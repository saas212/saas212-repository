/* French initialisation for the $ UI date picker plugin. */
/* Written by Keith Wood (kbwood{at}iinet.com.au) and St�phane Nahmani (sholby@sholby.net). */

$(document).ready(function () {
    $(".datepicker").datetimepicker({
        showOn: "button",
        buttonImage: "images/ico_calendrier_new.JPG",
        closeText: "Fermer",
        currentText: "Aujourd\'hui",
        monthNames: ["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin",
            "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"],
        monthNamesShort: ["janv.", "f�vr.", "mars", "avr.", "mai", "juin",
            "juil.", "ao�t", "sept.", "oct.", "nov.", "d�c."],
        dayNames: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
        dayNamesShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
        dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
        dateFormat: "yy-M-dd",
        showWeek: true,
        firstDay: 1,
        changeMonth: true,
        changeYear: true,
        weekHeader: "W",
        buttonImageOnly: true,
        inline: true
    });
    $(".datepickerF").datetimepicker({
        showOn: "button",
        buttonImage: "images/ico_calendrier_new.JPG",
        closeText: "Fermer",
        currentText: "Aujourd\'hui",
        monthNames: ["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin",
            "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"],
        monthNamesShort: ["janv.", "f�vr.", "mars", "avr.", "mai", "juin",
            "juil.", "ao�t", "sept.", "oct.", "nov.", "d�c."],
        dayNames: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
        dayNamesShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
        dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
        dateFormat: "yy-mm-dd",
        showWeek: true,
        firstDay: 1,
        changeMonth: true,
        changeYear: true,
        weekHeader: "W",
        buttonImageOnly: true,
        inline: true
    });
    $(".datepickerS").datepicker({
        showOn: "button",
        buttonImage: "images/ico_calendrier_new.JPG",
        closeText: "Fermer",
        currentText: "Aujourd\'hui",
        monthNames: ["Janvier", "F�vrier", "Mars", "Avril", "Mai", "Juin",
            "Juillet", "Ao�t", "Septembre", "Octobre", "Novembre", "D�cembre"],
        monthNamesShort: ["janv.", "f�vr.", "mars", "avr.", "mai", "juin",
            "juil.", "ao�t", "sept.", "oct.", "nov.", "d�c."],
        dayNames: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
        dayNamesShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
        dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
        dateFormat: "yy-M-dd",
        showWeek: true,
        firstDay: 1,
        changeMonth: true,
        changeYear: true,
        weekHeader: "W",
        buttonImageOnly: true,
        inline: true
    });
    $(".datepickerSF").datepicker({
        showOn: "both",
        buttonImage: "images/ico_calendrier_new.JPG",
        closeText: "Fermer",
        currentText: "Aujourd\'hui",
        monthNames: ["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin",
            "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"],
        monthNamesShort: ["janv.", "f�vr.", "mars", "avr.", "mai", "juin",
            "juil.", "ao�t", "sept.", "oct.", "nov.", "d�c."],
        dayNames: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
        dayNamesShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
        dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
        dateFormat: "yy-mm-dd",
        showWeek: true,
        firstDay: 1,
        changeMonth: true,
        changeYear: true,
        weekHeader: "W",
        buttonImageOnly: true,
        inline: true
    });
    $(".datepickerSFC").datepicker({
        showOn: "button",
        buttonImage: "images/ico_calendrier_new.JPG",
        closeText: "Fermer",
        currentText: "Aujourd\'hui",
        monthNames: ["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin",
            "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"],
        monthNamesShort: ["janv.", "f�vr.", "mars", "avr.", "mai", "juin",
            "juil.", "ao�t", "sept.", "oct.", "nov.", "d�c."],
        dayNames: ["Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"],
        dayNamesShort: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
        dayNamesMin: ["Di", "Lu", "Ma", "Me", "Je", "Ve", "Sa"],
        dateFormat: "yy-mm-dd",
        showWeek: true,
        firstDay: 1,
        changeMonth: true,
        changeYear: true,
        weekHeader: "W",
        buttonImageOnly: true,
        inline: true,
        showButtonPanel: true,
        beforeShow: function (input) {
            setTimeout(function () {
                var buttonPane = $(input)
                        .datepicker("widget")
                        .find(".ui-datepicker-buttonpane");
                $("<button>", {
                    text: "Vider",
                    click: function () {
                        $(input).val('');
                    }
                }).addClass('ui-state-default ui-corner-all')
                        .appendTo(buttonPane);
            }, 1);
        }
    });
});