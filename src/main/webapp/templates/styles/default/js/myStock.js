/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $(".integerInput").keypress(function (e) {

        var charEnum = {ZERO: 48, NEUF: 57};
        var keyEnum = {ENTER: 13, TAB: 9, BACKSPACE: 8, RIGHT: 37, LEFT: 39, DELETE: 46};

        var charCode = e.charCode ? e.charCode : 0;
        var keyCode = e.keyCode ? e.keyCode : 0;

        if (((charCode >= charEnum.ZERO) && (charCode <= charEnum.NEUF) && (keyCode == 0))
                || ((keyCode >= charEnum.ZERO) && (keyCode <= charEnum.NEUF) && (charCode == 0))
                || ((keyCode >= charEnum.ZERO) && (keyCode <= charEnum.NEUF) && (charCode == keyCode))
                || ((keyCode == keyEnum.ENTER)
                        || (keyCode == keyEnum.TAB)
                        || (keyCode == keyEnum.BACKSPACE)
                        || (keyCode == keyEnum.RIGHT) || (keyCode == keyEnum.LEFT)
                        ) && (charCode == 0)) {
            return true;
        } else {
            return false;
        }
    });
});

$('.doubleInput').keypress(function (event) {

    if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {

        event.preventDefault();
    }
});