
function autoCompate(selector, hidden, type, before,after) {

    var servlet = "http://localhost:8083/portal/servlet/autoComplate?type=" + type;
    
    function split(val) {
        return val.split(/,\s*/);
    }
    function extractLast(term) {
        return term;//split(term).pop();
    }
    
    $(selector).autocomplete({
        source: function (request, response) {
            $.getJSON(servlet, {
                term: extractLast(request.term)
            }, response);
        },
        select: function (event, ui) {
            before();
            //alert("update " + ui.item.value + " - " + ui.item.label + " - " + this.value);
            var label = ui.item.label;
            $(hidden).val(ui.item.value);
            //alert(hidden + " - " + ui.item.value);
            this.value = label;
            
            after();
            
            return false;
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        minLength: 3,
        delay: 0,
        scrol: true,
        minChars: 3,
        width: 424,
        multiple: false,
        matchContains: true,
        max: 100
    });
    
    
}