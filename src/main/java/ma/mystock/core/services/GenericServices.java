package ma.mystock.core.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import ma.mystock.core.dao.GenericDAO;
import ma.mystock.core.dao.entities.CltClient;
import ma.mystock.core.dao.entities.CltModule;
import ma.mystock.core.dao.entities.CltUser;
import ma.mystock.core.dao.entities.CtaEntityEmail;
import ma.mystock.core.dao.entities.CtaEntityFax;
import ma.mystock.core.dao.entities.CtaEntityLocation;
import ma.mystock.core.dao.entities.CtaEntityPhone;
import ma.mystock.core.dao.entities.CtaEntityWeb;
import ma.mystock.core.dao.entities.SmProduct;
import ma.mystock.core.dao.entities.views.VBiSmOrderLineByDay;
import ma.mystock.core.dao.entities.views.VBiSmReceptionLineByDay;
import ma.mystock.core.dao.entities.views.VCltClient;
import ma.mystock.core.dao.entities.views.VCltUser;
import ma.mystock.core.dao.entities.views.VCltUserCategory;
import ma.mystock.core.dao.entities.views.VCltUserDetails;
import ma.mystock.core.dao.entities.views.VCltUserModule;
import ma.mystock.core.dao.entities.views.VCtaEmailType;
import ma.mystock.core.dao.entities.views.VCtaEntityEmail;
import ma.mystock.core.dao.entities.views.VCtaEntityFax;
import ma.mystock.core.dao.entities.views.VCtaEntityLocation;
import ma.mystock.core.dao.entities.views.VCtaEntityPhone;
import ma.mystock.core.dao.entities.views.VCtaEntityWeb;
import ma.mystock.core.dao.entities.views.VCtaFaxType;
import ma.mystock.core.dao.entities.views.VCtaLocationType;
import ma.mystock.core.dao.entities.views.VCtaPhoneType;
import ma.mystock.core.dao.entities.views.VCtaWebType;
import ma.mystock.core.dao.entities.views.VInfCity;
import ma.mystock.core.dao.entities.views.VInfCountry;
import ma.mystock.core.dao.entities.views.VInfCurrency;
import ma.mystock.core.dao.entities.views.VInfMonth;
import ma.mystock.core.dao.entities.views.VPmPage;
import ma.mystock.core.dao.entities.views.VPmPageAttributeModule;
import ma.mystock.core.dao.entities.views.VSmOrder;
import ma.mystock.core.dao.entities.views.VSmOrderDetails;
import ma.mystock.core.dao.entities.views.VSmOrderSupplier;
import ma.mystock.core.dao.entities.views.VSmProduct;
import ma.mystock.core.dao.entities.views.VSmProductAlert;
import ma.mystock.core.dao.entities.views.VSmProductDepartment;
import ma.mystock.core.dao.entities.views.VSmProductNotReceived;
import ma.mystock.core.dao.entities.views.VSmProductUnit;
import ma.mystock.core.dao.entities.views.VSmReception;
import ma.mystock.core.dao.entities.views.dto.VPmPagesDto;
import ma.mystock.web.utils.globals.GlobalsAttributs;
import ma.mystock.web.utils.sessions.SessionsUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author abdou
 *
 */
@Service("GenericServices")
@Transactional
public class GenericServices {

    @Autowired
    GenericDAO<Object, Long> genericDao;

    private final Map<String, Object> paramQuery = new HashMap<>();

    private String getLanguage() {
        return SessionsUtils.getString(GlobalsAttributs.LANGUAGE);
    }

    /**
     * <!--
     * ###############################################################################
     * ###
     * ###                         Module INF : Infrastrucutre
     * ###
     * ###############################################################################
     * -->
     *
     */
    /* ################### VInfCountry ################### */
    public List<VInfCountry> findVInfCountryByActive(String active) {
        paramQuery.clear();
        paramQuery.put("active", active);
        List<VInfCountry> vInfCountryList = genericDao.executeQuery(VInfCountry.findByActive, paramQuery);

        return vInfCountryList;
    }

    /* ################### VInfCountry ################### */
    public List<VInfCity> findVInfCityByCountryIdAndActive(Long countryId, String active) {
        paramQuery.clear();
        paramQuery.put("countryId", countryId);
        paramQuery.put("active", active);
        List<VInfCity> vInfCityList = genericDao.executeQuery(VInfCity.findByCountryIdAndActive, paramQuery);

        return vInfCityList;
    }

    /* ################### VInfCurrency ################### */
    public List<VInfCurrency> findVInfCurrencyByActive(String active) {
        paramQuery.clear();
        paramQuery.put("active", active);
        List<VInfCurrency> vInfCurrencyList = genericDao.executeQuery(VInfCurrency.findByActive, paramQuery);

        return vInfCurrencyList;
    }


    /* ################### VInfMonth ################### */
    public VInfMonth findVInfMonthById(Long id) {
        paramQuery.clear();
        paramQuery.put("id", id);
        VInfMonth vInfMonth = (VInfMonth) genericDao.executeSingleQuery(VInfMonth.findById, paramQuery);

        return vInfMonth;
    }

    /**
     * <!--
     * ###############################################################################
     * ###
     * ###                         Module CLT : Gestion Client
     * ###
     * ###############################################################################
     * -->
     *
     */
    /* ################### VCltUserDetails ################### */
    public VCltUserDetails findVCltUserDetailsByUsernameAndPasswordAndClientId(String username, String password, Long clientId) {
        paramQuery.clear();
        paramQuery.put("username", username);
        paramQuery.put("password", password);
        paramQuery.put("clientId", clientId);

        VCltUserDetails vCltUserDetails = (VCltUserDetails) genericDao.executeSingleQuery(VCltUserDetails.findByUsernameAndPasswordAndClientId, paramQuery);

        return vCltUserDetails;
    }
    
    public VCltUserDetails findVCltUserDetailsByUsernameAndPassword(String username, String password) {
        paramQuery.clear();
        paramQuery.put("username", username);
        paramQuery.put("password", password);

        VCltUserDetails vCltUserDetails = (VCltUserDetails) genericDao.executeSingleQuery(VCltUserDetails.findByUsernameAndPassword, paramQuery);

        return vCltUserDetails;
    }

    /* ################### VCltUser ################### */
    public List<VCltUser> findVCltUserByClientId(Long id) {
        paramQuery.clear();
        paramQuery.put("clientId", id);

        List<VCltUser> vCltUserList = genericDao.executeQuery(VCltUser.findByClientId, paramQuery);

        return vCltUserList;
    }

    /* ################### VCltUser ################### */
    public CltUser findCltUserById(Long id) {
        paramQuery.clear();
        paramQuery.put("id", id);

        CltUser cltUser = (CltUser) genericDao.executeSingleQuery(CltUser.findById, paramQuery);

        return cltUser;
    }

    /* ################### VCltUser ################### */
    public List<VCltUserModule> findVCltUserModuleByUserId(Long userId) {

        paramQuery.clear();
        paramQuery.put("userId", userId);
        List<VCltUserModule> VCltUserModuleList = genericDao.executeQuery(VCltUserModule.findByUserId, paramQuery);

        return VCltUserModuleList;
    }

    /* ################### VCltClient ################### */
    public VCltClient findVCltClientById(Long id) {

        paramQuery.clear();
        paramQuery.put("id", id);

        VCltClient vCltClient = (VCltClient) genericDao.executeSingleQuery(VCltClient.findById, paramQuery);

        return vCltClient;
    }

    /* ################### CltClient ################### */
    public CltClient findCltClientById(Long id) {

        paramQuery.clear();
        paramQuery.put("id", id);

        CltClient cltClient = (CltClient) genericDao.executeSingleQuery(CltClient.findById, paramQuery);

        return cltClient;
    }

    /* ################### CltClient ################### */
    public List<VCltUserCategory> findVCltUserCategoryByCltModuleId(Long cltModuleId) {

        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);

        List<VCltUserCategory> vCltUserCategoryList = genericDao.executeQuery(VCltUserCategory.findByCltModuleId, paramQuery);

        return vCltUserCategoryList;
    }

    /* ################### CltClient ################### */
    public List<CltModule> findCltModuleByClientId(Long clientId) {

        paramQuery.clear();
        paramQuery.put("clientId", clientId);

        List<CltModule> cltModuleList = genericDao.executeQuery(CltModule.findByClientId, paramQuery);

        return cltModuleList;
    }

    /**
     * <!--
     * ###############################################################################
     * ###
     * ###                         Module PM : Page managment
     * ###
     * ###############################################################################
     * -->
     *
     */
    /* ################### VPmPage ################### */
    public VPmPage findVPmPageById(Long id) {
        paramQuery.clear();
        paramQuery.put("id", id);
        VPmPage vPmPage = (VPmPage) genericDao.executeSingleQuery(VPmPage.findById, paramQuery);
        return vPmPage;
    }

    /* ################### VPmPagesDto ################### */
    public VPmPagesDto findVPmPagesDtoById(Long id) {
        paramQuery.clear();
        paramQuery.put("id", id);
        VPmPagesDto vPmPagesDto = (VPmPagesDto) genericDao.executeSingleQuery(VPmPagesDto.findById, paramQuery);
        return vPmPagesDto;
    }

    /* ################### VPmPageAttributeModule ################### */
    public List<VPmPageAttributeModule> findVPmPageAttributeModuleByIsRequired() {
        List<VPmPageAttributeModule> vPmPageAttributeModuleList = genericDao.executeQuery(VPmPageAttributeModule.findByIsRequired);
        return vPmPageAttributeModuleList;
    }

    public List<VPmPageAttributeModule> findVPmPageAttributeModuleByIsReadonly() {
        List<VPmPageAttributeModule> vPmPageAttributeModuleList = genericDao.executeQuery(VPmPageAttributeModule.findByIsReadonly);
        return vPmPageAttributeModuleList;
    }

    public List<VPmPageAttributeModule> findVPmPageAttributeModuleByIsHidden() {
        List<VPmPageAttributeModule> vPmPageAttributeModuleList = genericDao.executeQuery(VPmPageAttributeModule.findByIsHidden);
        return vPmPageAttributeModuleList;
    }

    public List<VPmPageAttributeModule> findVPmPageAttributeModuleByDataTypeId() {
        List<VPmPageAttributeModule> vPmPageAttributeModuleList = genericDao.executeQuery(VPmPageAttributeModule.findByDataTypeId);
        return vPmPageAttributeModuleList;
    }

    public List<VPmPageAttributeModule> findVPmPageAttributeModuleByFormatTypeId() {
        List<VPmPageAttributeModule> vPmPageAttributeModuleList = genericDao.executeQuery(VPmPageAttributeModule.findByFormatTypeId);
        return vPmPageAttributeModuleList;
    }

    public List<VPmPageAttributeModule> findVPmPageAttributeModuleByMaxLenght() {
        List<VPmPageAttributeModule> vPmPageAttributeModuleList = genericDao.executeQuery(VPmPageAttributeModule.findByMaxLenght);
        return vPmPageAttributeModuleList;
    }

    public List<VPmPageAttributeModule> findVPmPageAttributeModuleByMaxWord() {
        List<VPmPageAttributeModule> vPmPageAttributeModuleList = genericDao.executeQuery(VPmPageAttributeModule.findByMaxWord);
        return vPmPageAttributeModuleList;
    }

    /**
     * <!--
     * ###############################################################################
     * ###
     * ###                         Module SM : Stock managment
     * ###
     * ###############################################################################
     * -->
     *
     */
    
    
    /* ################### VSmReception ################### */
          
    public List<VSmReception> findVSmReceptionByReferenceAnCltModuleId(String reference, Long cltModuleId) {
        paramQuery.clear();
        paramQuery.put("reference", reference);
        paramQuery.put("cltModuleId", cltModuleId);
        List<VSmReception> vSmReceptionList = genericDao.executeQuery(VSmReception.findByReferenceAnCltModuleId, paramQuery);
        return vSmReceptionList;
    }
    
    
            
    /* ################### VSmOrderSupplier ################### */
          
    public List<VSmOrderSupplier> findVSmOrderSupplierByReferenceAnCltModuleId(String reference, Long cltModuleId) {
        paramQuery.clear();
        paramQuery.put("reference", reference);
        paramQuery.put("cltModuleId", cltModuleId);
        List<VSmOrderSupplier> vSmOrderSupplierList = genericDao.executeQuery(VSmOrderSupplier.findByReferenceAnCltModuleId, paramQuery);
        return vSmOrderSupplierList;
    }
    
    
    /* ################### VSmOrder ################### */
          
    public List<VSmOrder> findVSmOrderByReferenceAnCltModuleId(String reference, Long cltModuleId) {
        paramQuery.clear();
        paramQuery.put("reference", reference);
        paramQuery.put("cltModuleId", cltModuleId);
        List<VSmOrder> vSmOrderList = genericDao.executeQuery(VSmOrder.findByReferenceAnCltModuleId, paramQuery);
        return vSmOrderList;
    }
    
    /* ################### vSmProductNotReceivedList ################### */
    public List<VSmProductNotReceived> findVSmProductNotReceivedByCltModuleId(Long cltModuleId) {
        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        List<VSmProductNotReceived> vSmProductNotReceivedList = genericDao.executeQuery(VSmProductNotReceived.findByCltModuleId, paramQuery);
        return vSmProductNotReceivedList;
    }

    public List<VSmProductNotReceived> findVSmProductNotReceivedByLikeReferenceOrSupplierIdOrOrderSupplierReferenceOrDesignationAndCltModuleId(String reference, Long supplierId, String orderSupplierReference, String productDesignation, Long cltModuleId) {
        paramQuery.clear();
        paramQuery.put("productReference", toLowerAndPercentage(reference));
        paramQuery.put("productDesignation", toLowerAndPercentage(productDesignation));
        paramQuery.put("orderSupplierReference", toLowerAndPercentage(orderSupplierReference));
        System.out.println(" > orderSupplierReference " + orderSupplierReference);
        paramQuery.put("supplierId", supplierId);
        paramQuery.put("cltModuleId", cltModuleId);
        List<VSmProductNotReceived> vSmProductNotReceivedList = genericDao.executeQuery(VSmProductNotReceived.findByLikeReferenceOrSupplierIdAndCltModuleId, paramQuery);
        return vSmProductNotReceivedList;
    }

    /* ################### VSmProductAlert ################### */
    public List<VSmProductAlert> findVSmProductAlertByCltModuleId(Long cltModuleId) {
        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        List<VSmProductAlert> vSmProductAlertList = genericDao.executeQuery(VSmProductAlert.findByCltModuleId, paramQuery);
        return vSmProductAlertList;
    }

    public List<VSmProductAlert> findVSmProductAlertByLikeReferenceOrDesignationAndCltModuleId(String reference, String designation, Long cltModuleId) {
        paramQuery.clear();
        paramQuery.put("reference", toLowerAndPercentage(reference));
        paramQuery.put("designation", toLowerAndPercentage(designation));
        paramQuery.put("cltModuleId", cltModuleId);
        List<VSmProductAlert> vSmProductAlertList = genericDao.executeQuery(VSmProductAlert.findByLikeReferenceOrDesignationAndCltModuleId, paramQuery);
        return vSmProductAlertList;
    }

    /* ################### VSmProductDepartment ################### */
    public List<VSmProductDepartment> findVSmProductDepartmentByCltModuleId(Long cltModuleId) {
        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        List<VSmProductDepartment> vSmProductDepartmentList = genericDao.executeQuery(VSmProductDepartment.findByCltModuleId, paramQuery);
        return vSmProductDepartmentList;
    }

    public List<VSmProductDepartment> findVSmProductDepartmentByActiveAndCltModuleId(String active, Long cltModuleId) {
        paramQuery.clear();
        paramQuery.put("active", active);
        paramQuery.put("cltModuleId", cltModuleId);
        List<VSmProductDepartment> vSmProductDepartmentList = genericDao.executeQuery(VSmProductDepartment.findByActiveAndCltModuleId, paramQuery);
        return vSmProductDepartmentList;
    }

    /* ################### VSmProduct ################### */
    public List<VSmProduct> findVSmProductByLikeReferenceOrLikeDesignationOrFamilyIdOrTypeIdOrDepartmentIdOrSizeIdOrColorId(String reference, String designation, Long productFamilyId, Long productTypeId, Long productDepartmentId, Long productSizeId, Long productColorId, Long quantity, Long cltModuleId) {

        paramQuery.clear();
        paramQuery.put("reference", toLowerAndPercentage(reference));
        paramQuery.put("designation", toLowerAndPercentage(designation));
        paramQuery.put("productFamilyId", productFamilyId);
        paramQuery.put("productTypeId", productTypeId);
        paramQuery.put("productDepartmentId", productDepartmentId);
        paramQuery.put("productSizeId", productSizeId);
        paramQuery.put("productColorId", productColorId);
        paramQuery.put("quantity", quantity);
        paramQuery.put("cltModuleId", cltModuleId);

        List<VSmProduct> vSmProductList = genericDao.executeQuery(VSmProduct.findByLikeReferenceOrLikeDesignationOrFamilyIdOrTypeIdOrDepartmentIdOrSizeIdOrColorIdOrQuantityAndCltModuleId, paramQuery);
        return vSmProductList;
    }

    public List<VSmProduct> findVSmProductByActiveAndCltModuleIdOrderByIdDescAndLimit(String active, Long cltModuleId, int limit) {

        paramQuery.clear();
        paramQuery.put("active", active);
        paramQuery.put("cltModuleId", cltModuleId);

        List<VSmProduct> vSmProductList = genericDao.executeQuery(VSmProduct.findByActiveAndCltModuleIdOrderByIdDesc, paramQuery, limit);
        return vSmProductList;
    }
    
    public List<VSmProduct> findVSmProductByReferenceAnCltModuleId(String reference, Long cltModuleId){
        paramQuery.clear();
        paramQuery.put("reference", reference);
        paramQuery.put("cltModuleId", cltModuleId);
        List<VSmProduct> vSmProductList =  genericDao.executeQuery(VSmProduct.findByReferenceAnCltModuleId, paramQuery);
        
        return vSmProductList;
    }

    /* ################### SmProduct ################### */
    public SmProduct findSmProductById(Long productId) {

        paramQuery.clear();
        paramQuery.put("id", productId);
        SmProduct smProduct = (SmProduct) genericDao.executeSingleQuery(SmProduct.findById, paramQuery);

        return smProduct;
    }

    /* ################### VSmProductUnit ################### */
    public List<VSmProductUnit> findVSmProductUnitByCltModuleId(Long cltModuleId) {
        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        List<VSmProductUnit> vSmProductUnitList = genericDao.executeQuery(VSmProductUnit.findByCltModuleId, paramQuery);
        return vSmProductUnitList;
    }

    public List<VSmProductUnit> findVSmProductUnitByActiveAndCltModuleId(String active, Long cltModuleId) {
        paramQuery.clear();
        paramQuery.put("active", active);
        paramQuery.put("cltModuleId", cltModuleId);
        List<VSmProductUnit> vSmProductUnitList = genericDao.executeQuery(VSmProductUnit.findByActiveAndCltModuleId, paramQuery);
        return vSmProductUnitList;
    }

    /* ################### VSmOrderDetails ################### */
    public VSmOrderDetails findVSmOrderDetailsByCltModuleId(Long cltModuleId) {
        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        VSmOrderDetails vSmOrderDetails = (VSmOrderDetails) genericDao.executeSingleQuery(VSmOrderDetails.findByCltModuleId, paramQuery);
        return vSmOrderDetails;
    }

    /* ################### VBiSmOrderLineByDay ################### */
    public List<VBiSmOrderLineByDay> findVBiSmOrderLineByDayByCltModuleId(Long cltModuleId) {
        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        List<VBiSmOrderLineByDay> vBiSmOrderLineByDayList = genericDao.executeQuery(VBiSmOrderLineByDay.findByCltModuleId, paramQuery);
        return vBiSmOrderLineByDayList;
    }

    public List<VBiSmOrderLineByDay> findVBiSmOrderLineByDayByCltModuleIdAndMonthAndYear(Long month, Long year, Long cltModuleId) {
        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        paramQuery.put("month", month);
        paramQuery.put("year", year);
        List<VBiSmOrderLineByDay> vBiSmOrderLineByDayList = genericDao.executeQuery(VBiSmOrderLineByDay.findByCltModuleIdAndMonthAndYear, paramQuery);
        return vBiSmOrderLineByDayList;
    }

    /* ################### VBiSmReceptionLineByDay ################### */
    public List<VBiSmReceptionLineByDay> findVBiSmReceptionLineByDayByCltModuleId(Long cltModuleId) {
        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        List<VBiSmReceptionLineByDay> vBiSmReceptionLineByDayList = genericDao.executeQuery(VBiSmReceptionLineByDay.findByCltModuleId, paramQuery);
        return vBiSmReceptionLineByDayList;
    }

    public List<VBiSmReceptionLineByDay> findVBiSmReceptionLineByDayByCltModuleIdAndMonthAndYear(Long month, Long year, Long cltModuleId) {
        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        paramQuery.put("month", month);
        paramQuery.put("year", year);
        List<VBiSmReceptionLineByDay> vBiSmOrderLineByDayList = genericDao.executeQuery(VBiSmReceptionLineByDay.findByCltModuleIdAndMonthAndYear, paramQuery);
        return vBiSmOrderLineByDayList;
    }

    /**
     * <!--
     * ###############################################################################
     * ###
     * ###                         Module CTA : Contact managment
     * ###
     * ###############################################################################
     * -->
     *
     */
    /* ################### VCtaEntityWeb ################### */
    public List<VCtaEntityWeb> findVCtaEntityWebByEntityId(Long entityId) {
        paramQuery.clear();
        paramQuery.put("entityId", entityId);
        List<VCtaEntityWeb> vCtaEntityWebList = genericDao.executeQuery(VCtaEntityWeb.findByEntityId, paramQuery);

        return vCtaEntityWebList;
    }

    /* ################### CtaEntityWeb ################### */
    public CtaEntityWeb findCtaEntityWebById(Long id) {
        paramQuery.clear();
        paramQuery.put("id", id);
        CtaEntityWeb ctaEntityWeb = (CtaEntityWeb) genericDao.executeSingleQuery(CtaEntityWeb.findById, paramQuery);

        return ctaEntityWeb;
    }

    /* ################### VCtaWebType ################### */
    public List<VCtaWebType> findVCtaWebTypeByCltModuleId(Long cltModuleId) {
        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        List<VCtaWebType> vCtaWebTypeList = genericDao.executeQuery(VCtaWebType.findByCltModuleId, paramQuery);

        return vCtaWebTypeList;
    }

    /* ################### VCtaEntityLocation ################### */
    public List<VCtaEntityLocation> findVCtaEntityLocationByEntityId(Long entityId) {
        paramQuery.clear();
        paramQuery.put("entityId", entityId);
        List<VCtaEntityLocation> vCtaEntityLocationList = genericDao.executeQuery(VCtaEntityLocation.findByEntityId, paramQuery);

        return vCtaEntityLocationList;
    }

    /* ################### CtaEntityWeb ################### */
    public CtaEntityLocation findCtaEntityLocationById(Long id) {
        paramQuery.clear();
        paramQuery.put("id", id);
        CtaEntityLocation ctaEntityLocation = (CtaEntityLocation) genericDao.executeSingleQuery(CtaEntityLocation.findById, paramQuery);

        return ctaEntityLocation;
    }

    /* ################### VCtaWebType ################### */
    public List<VCtaLocationType> findVCtaLocationTypeByCltModuleId(Long cltModuleId) {
        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        List<VCtaLocationType> vCtaLocationTypeList = genericDao.executeQuery(VCtaLocationType.findByCltModuleId, paramQuery);

        return vCtaLocationTypeList;
    }

    /* ################### CtaEntityPhone ################### */
    public CtaEntityPhone findCtaEntityPhoneById(Long id) {

        paramQuery.clear();
        paramQuery.put("id", id);
        CtaEntityPhone ctaEntityPhone = (CtaEntityPhone) genericDao.executeSingleQuery(CtaEntityPhone.findById, paramQuery);

        return ctaEntityPhone;
    }

    /* ################### VCtaPhoneType ################### */
    public List<VCtaPhoneType> findVCtaPhoneTypeByCltModuleId(Long cltModuleId) {

        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        paramQuery.put("active", "Y");
        List<VCtaPhoneType> vCtaPhoneTypeList = genericDao.executeQuery(VCtaPhoneType.findByCltModuleIdAndActive, paramQuery);

        return vCtaPhoneTypeList;
    }

    /* ################### VCtaEntityPhone ################### */
    public List<VCtaEntityPhone> findVCtaEntityPhoneByEntityId(Long entityId) {

        paramQuery.clear();
        paramQuery.put("entityId", entityId);
        List<VCtaEntityPhone> vCtaEntityPhoneList = genericDao.executeQuery(VCtaEntityPhone.findByEntityId, paramQuery);

        return vCtaEntityPhoneList;
    }

    /* ################### VCtaEntityEmail ################### */
    public List<VCtaEntityEmail> findVCtaEntityEmailByEntityId(Long entityId) {
        paramQuery.clear();
        paramQuery.put("entityId", entityId);
        List<VCtaEntityEmail> vCtaEntityEmailList = genericDao.executeQuery(VCtaEntityEmail.findByEntityId, paramQuery);

        return vCtaEntityEmailList;
    }

    /* ################### CtaEntityEmail ################### */
    public CtaEntityEmail findCtaEntityEmailById(Long id) {
        paramQuery.clear();
        paramQuery.put("id", id);
        CtaEntityEmail ctaEntityEmail = (CtaEntityEmail) genericDao.executeSingleQuery(CtaEntityEmail.findById, paramQuery);

        return ctaEntityEmail;
    }

    /* ################### VCtaEmailType ################### */
    public List<VCtaEmailType> findVCtaEmailTypeByCltModuleId(Long cltModuleId) {
        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        List<VCtaEmailType> vCtaEmailType = genericDao.executeQuery(VCtaEmailType.findByCltModuleId, paramQuery);

        return vCtaEmailType;
    }

    /* ################### VCtaEntityFax ################### */
    public List<VCtaEntityFax> findVCtaEntityFaxByEntityId(Long entityId) {
        paramQuery.clear();
        paramQuery.put("entityId", entityId);
        List<VCtaEntityFax> vCtaEntityFaxList = genericDao.executeQuery(VCtaEntityFax.findByEntityId, paramQuery);

        return vCtaEntityFaxList;
    }

    /* ################### CtaEntityFax ################### */
    public CtaEntityFax findCtaEntityFaxById(Long id) {
        paramQuery.clear();
        paramQuery.put("id", id);
        CtaEntityFax ctaEntityFax = (CtaEntityFax) genericDao.executeSingleQuery(CtaEntityFax.findById, paramQuery);

        return ctaEntityFax;
    }

    /* ################### VCtaFaxType ################### */
    public List<VCtaFaxType> findVCtaFaxTypeByCltModuleId(Long cltModuleId) {
        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        List<VCtaFaxType> vCtaFaxType = genericDao.executeQuery(VCtaFaxType.findByCltModuleId, paramQuery);

        return vCtaFaxType;
    }

    /**
     * <!--
     * ###############################################################################
     * ###
     * ###                         Module Generic : All
     * ###
     * ###############################################################################
     * -->
     *
     */
    /* ################### Globals ################### */
    public Object genericPersiste(Object o) {
        return genericDao.executeMerge(o);

    }

    public void genericDelete(Object o) {
        genericDao.executeDelete(o);
    }

    public String toLowerAndPercentage(String param) {
        String value = "";
        if (!"".equalsIgnoreCase(param)) {
            value = "%" + param.toLowerCase() + "%";
        }
        return value;
    }
    
    
}
