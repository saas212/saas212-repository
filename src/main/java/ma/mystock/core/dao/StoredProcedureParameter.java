/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao;

import javax.persistence.ParameterMode;

/**
 *
 * @author Abdessamad HALLAL
 */
public class StoredProcedureParameter {

    private String name;
    private Class classe;
    private ParameterMode Mode;
    private Object value;

    public StoredProcedureParameter() {
    }

    public StoredProcedureParameter(String name, Class classe, ParameterMode Mode, Object value) {
        this.name = name;
        this.classe = classe;
        this.Mode = Mode;
        this.value = value;
    }

    public StoredProcedureParameter(Class classe, ParameterMode Mode) {
        this.classe = classe;
        this.Mode = Mode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Class getClasse() {
        return classe;
    }

    public void setClasse(Class classe) {
        this.classe = classe;
    }

    public ParameterMode getMode() {
        return Mode;
    }

    public void setMode(ParameterMode Mode) {
        this.Mode = Mode;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

}
