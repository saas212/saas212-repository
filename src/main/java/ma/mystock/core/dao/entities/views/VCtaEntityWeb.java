package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_CTA_ENTITY_WEB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCtaEntityWeb.findAll", query = "SELECT v FROM VCtaEntityWeb v")
    ,
    @NamedQuery(name = "VCtaEntityWeb.findById", query = "SELECT v FROM VCtaEntityWeb v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VCtaEntityWeb.findByUrl", query = "SELECT v FROM VCtaEntityWeb v WHERE v.url = :url")
    ,
    @NamedQuery(name = "VCtaEntityWeb.findByPriority", query = "SELECT v FROM VCtaEntityWeb v WHERE v.priority = :priority")
    ,
    @NamedQuery(name = "VCtaEntityWeb.findByEntityId", query = "SELECT v FROM VCtaEntityWeb v WHERE v.entityId = :entityId")
    ,
    @NamedQuery(name = "VCtaEntityWeb.findByWebTypeId", query = "SELECT v FROM VCtaEntityWeb v WHERE v.webTypeId = :webTypeId")
    ,
    @NamedQuery(name = "VCtaEntityWeb.findByDateCreation", query = "SELECT v FROM VCtaEntityWeb v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VCtaEntityWeb.findByUserCreation", query = "SELECT v FROM VCtaEntityWeb v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VCtaEntityWeb.findByDateUpdate", query = "SELECT v FROM VCtaEntityWeb v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VCtaEntityWeb.findByUserUpdate", query = "SELECT v FROM VCtaEntityWeb v WHERE v.userUpdate = :userUpdate")})
public class VCtaEntityWeb implements Serializable {

    public static final String findByEntityId = "SELECT v FROM VCtaEntityWeb v WHERE v.entityId = :entityId";

    private static final Long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @Id
    private Long id;

    @Column(name = "WEB_TYPE_NAME")
    private String webTypeName;

    @Column(name = "URL")
    private String url;

    @Column(name = "EXTERNAL_URL")
    private String externalUrl;

    @Column(name = "PRIORITY")
    private Long priority;

    @Column(name = "ENTITY_ID")
    private Long entityId;

    @Column(name = "WEB_TYPE_ID")
    private Long webTypeId;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public VCtaEntityWeb() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Long getWebTypeId() {
        return webTypeId;
    }

    public void setWebTypeId(Long webTypeId) {
        this.webTypeId = webTypeId;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getWebTypeName() {
        return webTypeName;
    }

    public void setWebTypeName(String webTypeName) {
        this.webTypeName = webTypeName;
    }

    public String getExternalUrl() {
        return externalUrl;
    }

    public void setExternalUrl(String externalUrl) {
        this.externalUrl = externalUrl;
    }

}
