package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_PM_COMPOSITION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VPmComposition.findAll", query = "SELECT v FROM VPmComposition v")
    ,
    @NamedQuery(name = "VPmComposition.findById", query = "SELECT v FROM VPmComposition v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VPmComposition.findByGroupId", query = "SELECT v FROM VPmComposition v WHERE v.groupId = :groupId")
    ,
    @NamedQuery(name = "VPmComposition.findByCategoryId", query = "SELECT v FROM VPmComposition v WHERE v.categoryId = :categoryId")
    ,
    @NamedQuery(name = "VPmComposition.findByMenuId", query = "SELECT v FROM VPmComposition v WHERE v.menuId = :menuId")
    ,
    @NamedQuery(name = "VPmComposition.findByPageId", query = "SELECT v FROM VPmComposition v WHERE v.pageId = :pageId")})
public class VPmComposition implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static String findByModelIdAndIndexShow = "SELECT v FROM VPmComposition v WHERE v.modelId = :modelId and v.indexShow = 'Y' order by v.indexSort";

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;

    //@Column(name = "CLT_MODULE_ID")
    //private Long cltModuleId;
    @Column(name = "GROUP_ID")
    private Long groupId;

    @Column(name = "CATEGORY_ID")
    private Long categoryId;

    @Column(name = "MENU_ID")
    private Long menuId;

    @Column(name = "PAGE_ID")
    private Long pageId;

    @Column(name = "INDEX_SHOW")
    private String indexShow;

    @Column(name = "INDEX_SORT")
    private Long indexSort;

    @Column(name = "MODEL_ID")
    private Long modelId;

    public VPmComposition() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getModelId() {
        return modelId;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    //public Long getCltModuleId() {
    //    return cltModuleId;
    //}
    //public void setCltModuleId(Long cltModuleId) {
    //    this.cltModuleId = cltModuleId;
    //}
    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public Long getPageId() {
        return pageId;
    }

    public void setPageId(Long pageId) {
        this.pageId = pageId;
    }

    public String getIndexShow() {
        return indexShow;
    }

    public void setIndexShow(String indexShow) {
        this.indexShow = indexShow;
    }

    public Long getIndexSort() {
        return indexSort;
    }

    public void setIndexSort(Long indexSort) {
        this.indexSort = indexSort;
    }

}
