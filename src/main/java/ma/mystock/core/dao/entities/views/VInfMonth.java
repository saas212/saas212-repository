/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_INF_MONTH")
@XmlRootElement

@NamedQueries({
    @NamedQuery(name = "VInfMonth.findAll", query = "SELECT v FROM VInfMonth v")
    ,
    @NamedQuery(name = "VInfMonth.findById", query = "SELECT v FROM VInfMonth v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VInfMonth.findByName", query = "SELECT v FROM VInfMonth v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VInfMonth.findByNumberOfDays", query = "SELECT v FROM VInfMonth v WHERE v.numberOfDays = :numberOfDays")
    ,
    @NamedQuery(name = "VInfMonth.findBySortKey", query = "SELECT v FROM VInfMonth v WHERE v.sortKey = :sortKey")
    ,
    @NamedQuery(name = "VInfMonth.findByDateCreation", query = "SELECT v FROM VInfMonth v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VInfMonth.findByUserCreation", query = "SELECT v FROM VInfMonth v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VInfMonth.findByDateUpdate", query = "SELECT v FROM VInfMonth v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VInfMonth.findByUserUpdate", query = "SELECT v FROM VInfMonth v WHERE v.userUpdate = :userUpdate")})
public class VInfMonth implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findById = "SELECT v FROM VInfMonth v WHERE v.id = :id";

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @Id
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "NUMBER_OF_DAYS")
    private Long numberOfDays;

    @Column(name = "SORT_KEY")
    private Long sortKey;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public VInfMonth() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(Long numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

}
