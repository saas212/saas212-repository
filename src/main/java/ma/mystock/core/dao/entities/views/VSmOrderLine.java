/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_SM_ORDER_LINE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmOrderLine.findAll", query = "SELECT v FROM VSmOrderLine v")
    ,
    @NamedQuery(name = "VSmOrderLine.findById", query = "SELECT v FROM VSmOrderLine v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VSmOrderLine.findByProductId", query = "SELECT v FROM VSmOrderLine v WHERE v.productId = :productId")
    ,
    @NamedQuery(name = "VSmOrderLine.findByOrderId", query = "SELECT v FROM VSmOrderLine v WHERE v.orderId = :orderId")
    ,
    @NamedQuery(name = "VSmOrderLine.findByPromotionId", query = "SELECT v FROM VSmOrderLine v WHERE v.promotionId = :promotionId")
    ,
    @NamedQuery(name = "VSmOrderLine.findByNegotiatePriceSale", query = "SELECT v FROM VSmOrderLine v WHERE v.negotiatePriceSale = :negotiatePriceSale")
    ,
    @NamedQuery(name = "VSmOrderLine.findByQuantity", query = "SELECT v FROM VSmOrderLine v WHERE v.quantity = :quantity")
    ,
    @NamedQuery(name = "VSmOrderLine.findByActive", query = "SELECT v FROM VSmOrderLine v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VSmOrderLine.findByDateCreation", query = "SELECT v FROM VSmOrderLine v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VSmOrderLine.findByUserCreation", query = "SELECT v FROM VSmOrderLine v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VSmOrderLine.findByDateUpdate", query = "SELECT v FROM VSmOrderLine v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VSmOrderLine.findByUserUpdate", query = "SELECT v FROM VSmOrderLine v WHERE v.userUpdate = :userUpdate")
    ,
    @NamedQuery(name = "VSmOrderLine.findByReference", query = "SELECT v FROM VSmOrderLine v WHERE v.reference = :reference")
    ,
    @NamedQuery(name = "VSmOrderLine.findByTotalNegotiatePriceSale", query = "SELECT v FROM VSmOrderLine v WHERE v.totalNegotiatePriceSale = :totalNegotiatePriceSale")})

public class VSmOrderLine implements Serializable {

    private static final Long serialVersionUID = 1L;
    public static final String findByOrderId = "SELECT v FROM VSmOrderLine v WHERE v.orderId = :orderId order by v.id";
    public static final String findById = "SELECT v FROM VSmOrderLine v WHERE v.id = :id";

    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "PRODUCT_ID")
    private Long productId;

    @Column(name = "DESIGNATION")
    private String designation;

    @Column(name = "ORDER_ID")
    private Long orderId;

    @Column(name = "PROMOTION_ID")
    private Long promotionId;

    @Column(name = "NEGOTIATE_PRICE_SALE")
    private Double negotiatePriceSale;

    @Column(name = "QUANTITY")
    private Long quantity;

    @Column(name = "ACTIVE")
    private String active;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "REFERENCE")
    private String reference;

    @Column(name = "TOTAL_NEGOTIATE_PRICE_SALE")
    private Double totalNegotiatePriceSale;

    public VSmOrderLine() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Long promotionId) {
        this.promotionId = promotionId;
    }

    public Double getNegotiatePriceSale() {
        return negotiatePriceSale;
    }

    public void setNegotiatePriceSale(Double negotiatePriceSale) {
        this.negotiatePriceSale = negotiatePriceSale;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Double getTotalNegotiatePriceSale() {
        return totalNegotiatePriceSale;
    }

    public void setTotalNegotiatePriceSale(Double totalNegotiatePriceSale) {
        this.totalNegotiatePriceSale = totalNegotiatePriceSale;
    }

}
