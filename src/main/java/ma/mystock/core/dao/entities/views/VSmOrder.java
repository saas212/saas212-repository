/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_SM_ORDER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmOrder.findAll", query = "SELECT v FROM VSmOrder v")
    ,
    @NamedQuery(name = "VSmOrder.findById", query = "SELECT v FROM VSmOrder v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VSmOrder.findByNoSeq", query = "SELECT v FROM VSmOrder v WHERE v.noSeq = :noSeq")
    ,
    @NamedQuery(name = "VSmOrder.findByReference", query = "SELECT v FROM VSmOrder v WHERE v.reference = :reference")
    ,
    @NamedQuery(name = "VSmOrder.findByCustomerId", query = "SELECT v FROM VSmOrder v WHERE v.customerId = :customerId")
    ,
    @NamedQuery(name = "VSmOrder.findByPaymentMethodId", query = "SELECT v FROM VSmOrder v WHERE v.paymentMethodId = :paymentMethodId")
    ,
    @NamedQuery(name = "VSmOrder.findByCheckId", query = "SELECT v FROM VSmOrder v WHERE v.checkId = :checkId")
    ,
    @NamedQuery(name = "VSmOrder.findByDelivery", query = "SELECT v FROM VSmOrder v WHERE v.delivery = :delivery")
    ,
    @NamedQuery(name = "VSmOrder.findByOrderStatusId", query = "SELECT v FROM VSmOrder v WHERE v.orderStatusId = :orderStatusId")
    ,
    @NamedQuery(name = "VSmOrder.findByCltModuleId", query = "SELECT v FROM VSmOrder v WHERE v.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "VSmOrder.findByActive", query = "SELECT v FROM VSmOrder v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VSmOrder.findByDateCreation", query = "SELECT v FROM VSmOrder v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VSmOrder.findByUserCreation", query = "SELECT v FROM VSmOrder v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VSmOrder.findByDateUpdate", query = "SELECT v FROM VSmOrder v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VSmOrder.findByUserUpdate", query = "SELECT v FROM VSmOrder v WHERE v.userUpdate = :userUpdate")
    ,
    @NamedQuery(name = "VSmOrder.findByCustomerCompanyName", query = "SELECT v FROM VSmOrder v WHERE v.customerCompanyName = :customerCompanyName")
    ,
    @NamedQuery(name = "VSmOrder.findByTotalHorsTaxe", query = "SELECT v FROM VSmOrder v WHERE v.totalHorsTaxe = :totalHorsTaxe")
    ,
    @NamedQuery(name = "VSmOrder.findByTotalTtc", query = "SELECT v FROM VSmOrder v WHERE v.totalTtc = :totalTtc")
    ,
    @NamedQuery(name = "VSmOrder.findBySumProduct", query = "SELECT v FROM VSmOrder v WHERE v.sumProduct = :sumProduct")
    ,
    @NamedQuery(name = "VSmOrder.findByOrderStatusName", query = "SELECT v FROM VSmOrder v WHERE v.orderStatusName = :orderStatusName")})
public class VSmOrder implements Serializable {

    public static String findById = "SELECT v FROM VSmOrder v WHERE v.id = :id";
    public static String findByCltModuleId = "SELECT v FROM VSmOrder v WHERE v.cltModuleId = :cltModuleId";
    public static String findByCltModuleIdAndOrderStatusId = "SELECT v FROM VSmOrder v WHERE v.cltModuleId = :cltModuleId and v.orderStatusId = :orderStatusId";
    public static String findMaxNoSeqByCltModuleId = "SELECT max(v.noSeq) FROM VSmOrder v WHERE v.cltModuleId = :cltModuleId";
    public static String findByReferenceAnCltModuleId = "SELECT v FROM VSmOrder v WHERE v.reference = :reference and v.cltModuleId = :cltModuleId";
    
    
    private static final Long serialVersionUID = 1L;

    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "NO_SEQ")
    private Long noSeq;

    @Column(name = "REFERENCE")
    private String reference;

    @Column(name = "CUSTOMER_ID")
    private Long customerId;

    @Column(name = "PAYMENT_METHOD_ID")
    private Long paymentMethodId;

    @Column(name = "CHECK_ID")
    private Long checkId;

    @Column(name = "DELIVERY")
    private String delivery;

    @Column(name = "NOTE")
    private String note;

    @Column(name = "ORDER_STATUS_ID")
    private Long orderStatusId;

    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    @Column(name = "ACTIVE")
    private String active;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "CUSTOMER_COMPANY_NAME")
    private String customerCompanyName;

    @Column(name = "TOTAL_HORS_TAXE")
    private Double totalHorsTaxe;

    @Column(name = "TOTAL_TTC")
    private Double totalTtc;

    @Column(name = "SUM_PRODUCT")
    private Long sumProduct;

    @Column(name = "ORDER_STATUS_NAME")
    private String orderStatusName;

    public VSmOrder() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNoSeq() {
        return noSeq;
    }

    public void setNoSeq(Long noSeq) {
        this.noSeq = noSeq;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public Long getCheckId() {
        return checkId;
    }

    public void setCheckId(Long checkId) {
        this.checkId = checkId;
    }

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getOrderStatusId() {
        return orderStatusId;
    }

    public void setOrderStatusId(Long orderStatusId) {
        this.orderStatusId = orderStatusId;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getCustomerCompanyName() {
        return customerCompanyName;
    }

    public void setCustomerCompanyName(String customerCompanyName) {
        this.customerCompanyName = customerCompanyName;
    }

    public Double getTotalHorsTaxe() {
        return totalHorsTaxe;
    }

    public void setTotalHorsTaxe(Double totalHorsTaxe) {
        this.totalHorsTaxe = totalHorsTaxe;
    }

    public Double getTotalTtc() {
        return totalTtc;
    }

    public void setTotalTtc(Double totalTtc) {
        this.totalTtc = totalTtc;
    }

    public Long getSumProduct() {
        return sumProduct;
    }

    public void setSumProduct(Long sumProduct) {
        this.sumProduct = sumProduct;
    }

    public String getOrderStatusName() {
        return orderStatusName;
    }

    public void setOrderStatusName(String orderStatusName) {
        this.orderStatusName = orderStatusName;
    }

}
