/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views.dto;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_PM_PAGES_DTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VPmPagesDto.findAll", query = "SELECT v FROM VPmPagesDto v")
    ,
    @NamedQuery(name = "VPmPagesDto.findByMenuId", query = "SELECT v FROM VPmPagesDto v WHERE v.menuId = :menuId")
    ,
    @NamedQuery(name = "VPmPagesDto.findByCode", query = "SELECT v FROM VPmPagesDto v WHERE v.code = :code")
    ,
    @NamedQuery(name = "VPmPagesDto.findByName", query = "SELECT v FROM VPmPagesDto v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VPmPagesDto.findByPageTypeId", query = "SELECT v FROM VPmPagesDto v WHERE v.pageTypeId = :pageTypeId")
    ,
    @NamedQuery(name = "VPmPagesDto.findByPageSort", query = "SELECT v FROM VPmPagesDto v WHERE v.pageSort = :pageSort")
    ,
    @NamedQuery(name = "VPmPagesDto.findByGroupId", query = "SELECT v FROM VPmPagesDto v WHERE v.groupId = :groupId")
    ,
    @NamedQuery(name = "VPmPagesDto.findByCategoryId", query = "SELECT v FROM VPmPagesDto v WHERE v.categoryId = :categoryId")})

public class VPmPagesDto implements Serializable {

    public static String findAll = "SELECT v FROM VPmPagesDto v";

    public static String findById = "SELECT v FROM VPmPagesDto v WHERE v.id = :id";

    public static String findByModelIdAndGroupIdAndCategoryIdAndMenuId = "SELECT v FROM VPmPagesDto v WHERE v.modelId = :modelId and v.groupId = :groupId and v.categoryId = :categoryId and v.menuId = :menuId order by v.pageSort";

    private static final Long serialVersionUID = 1L;

    @Id
    @Column(name = "CODE")
    private String code;

    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    transient private String classe;

    transient private String jsp;

    @Column(name = "PAGE_TYPE_ID")
    private Long pageTypeId;

    @Column(name = "PAGE_SORT")
    private Long pageSort;

    @Column(name = "MODEL_ID")
    private Long modelId;

    @Column(name = "GROUP_ID")
    private Long groupId;

    @Column(name = "CATEGORY_ID")
    private Long categoryId;

    @Column(name = "MENU_ID")
    private Long menuId;

    @Column(name = "IN_DEV")
    private String inDev;

    public VPmPagesDto() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInDev() {
        return inDev;
    }

    public void setInDev(String inDev) {
        this.inDev = inDev;
    }

    public Long getPageTypeId() {
        return pageTypeId;
    }

    public void setPageTypeId(Long pageTypeId) {
        this.pageTypeId = pageTypeId;
    }

    public Long getPageSort() {
        return pageSort;
    }

    public void setPageSort(Long pageSort) {
        this.pageSort = pageSort;
    }

    public Long getModelId() {
        return modelId;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    /**
     * For Web Page
     */
    public String getClasse() {

        // ma.mystock.web.beans.compositions.pages.p001.P001
        String newClasse;
        if (id < 10) {
            newClasse = "ma.mystock.web.beans.compositions.pages.p00" + id + ".P00" + id;
        } else if (id < 100) {
            newClasse = "ma.mystock.web.beans.compositions.pages.p0" + id + ".P0" + id;
        } else {
            newClasse = "ma.mystock.web.beans.compositions.pages.p" + id + ".P" + id;
        }

        return newClasse;

    }

    public String getJsp() {
        String newJsp;

        if ("Y".equalsIgnoreCase(inDev)) {
            newJsp = "encours/encours.xhtml";
        } else {
            if (id < 10) {
                newJsp = "p00" + id + "/p00" + id + ".xhtml";
            } else if (id < 100) {
                newJsp = "p0" + id + "/p0" + id + ".xhtml";
            } else {
                newJsp = "p" + id + "/p" + id + ".xhtml";
            }

        }

        return newJsp;
    }

    /**
     * For PDF *
     */
    public String getPdfClasse() {

        // ma.mystock.web.beans.compositions.pages.p001.P001
        String newClasse = "ma.mystock.web.beans.compositions.pdf.pdf" + id + ".PDF" + id;
        System.out.println(" => newClasse : " + newClasse);

        return newClasse;

    }

    public String getPdfJsp() {
        String newJsp;

        if ("Y".equalsIgnoreCase(inDev)) {
            newJsp = "encours/encours.xhtml";
        } else {
            newJsp = "pdf" + id + "/pdf" + id + ".xhtml";

        }
        System.out.println(" => newJsp : " + newJsp);
        return newJsp;
    }

    public void setJsp(String jsp) {
        this.jsp = jsp;
    }

}
