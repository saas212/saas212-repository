/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * Abdou
 */
@Entity
@Table(name = "CLT_MODULE_PARAMETER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CltModuleParameter.findAll", query = "SELECT c FROM CltModuleParameter c")
    ,
    @NamedQuery(name = "CltModuleParameter.findById", query = "SELECT c FROM CltModuleParameter c WHERE c.id = :id")
    ,
    @NamedQuery(name = "CltModuleParameter.findByName", query = "SELECT c FROM CltModuleParameter c WHERE c.name = :name")
    ,
    @NamedQuery(name = "CltModuleParameter.findByModuleParameterTypeId", query = "SELECT c FROM CltModuleParameter c WHERE c.moduleParameterTypeId = :moduleParameterTypeId")
    ,
    @NamedQuery(name = "CltModuleParameter.findByActive", query = "SELECT c FROM CltModuleParameter c WHERE c.active = :active")
    ,
    @NamedQuery(name = "CltModuleParameter.findByDateCreation", query = "SELECT c FROM CltModuleParameter c WHERE c.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "CltModuleParameter.findByUserCreation", query = "SELECT c FROM CltModuleParameter c WHERE c.userCreation = :userCreation")
    ,
    @NamedQuery(name = "CltModuleParameter.findByDateUpdate", query = "SELECT c FROM CltModuleParameter c WHERE c.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "CltModuleParameter.findByUserUpdate", query = "SELECT c FROM CltModuleParameter c WHERE c.userUpdate = :userUpdate")})
public class CltModuleParameter implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String findById = "SELECT c FROM CltModuleParameter c WHERE c.id = :id";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "DESCRIPTION")
    private String description;
    @Lob
    @Size(max = 65535)
    @Column(name = "DEFAULT_VALUE")
    private String defaultValue;
    @Column(name = "MODULE_PARAMETER_TYPE_ID")
    private Long moduleParameterTypeId;
    @JoinColumn(name = "MODULE_PARAMETER_TYPE_ID", referencedColumnName = "ID", updatable = false, insertable = false)
    @ManyToOne
    private CltModuleParameterType cltModuleParameterTypeId;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public CltModuleParameter() {
    }

    public CltModuleParameter(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public Long getModuleParameterTypeId() {
        return moduleParameterTypeId;
    }

    public void setModuleParameterTypeId(Long moduleParameterTypeId) {
        this.moduleParameterTypeId = moduleParameterTypeId;
    }

    public CltModuleParameterType getCltModuleParameterTypeId() {
        return cltModuleParameterTypeId;
    }

    public void setCltModuleParameterTypeId(CltModuleParameterType cltModuleParameterTypeId) {
        this.cltModuleParameterTypeId = cltModuleParameterTypeId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CltModuleParameter)) {
            return false;
        }
        CltModuleParameter other = (CltModuleParameter) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entitys.CltModuleParameter[ id=" + id + " ]";
    }

}
