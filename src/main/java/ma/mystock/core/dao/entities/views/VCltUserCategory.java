package ma.mystock.core.dao.entities.views;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdessamad HALLAL
 */
@Entity
@Table(name = "V_CLT_USER_CATEGORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCltUserCategory.findAll", query = "SELECT v FROM VCltUserCategory v")
    ,
    @NamedQuery(name = "VCltUserCategory.findById", query = "SELECT v FROM VCltUserCategory v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VCltUserCategory.findByName", query = "SELECT v FROM VCltUserCategory v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VCltUserCategory.findBySortKey", query = "SELECT v FROM VCltUserCategory v WHERE v.sortKey = :sortKey")
    ,
    @NamedQuery(name = "VCltUserCategory.findByActive", query = "SELECT v FROM VCltUserCategory v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VCltUserCategory.findByDateCreation", query = "SELECT v FROM VCltUserCategory v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VCltUserCategory.findByUserCreation", query = "SELECT v FROM VCltUserCategory v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VCltUserCategory.findByDateUpdate", query = "SELECT v FROM VCltUserCategory v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VCltUserCategory.findByUserUpdate", query = "SELECT v FROM VCltUserCategory v WHERE v.userUpdate = :userUpdate")})
public class VCltUserCategory implements Serializable {

    public final static String findAll = "SELECT v FROM VCltUserCategory v";
    public final static String findByCltModuleId = "SELECT v FROM VCltUserCategory v WHERE v.cltModuleId = :cltModuleId";

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private long id;

    @Column(name = "NAME")
    private String name;

    @Lob
    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "SORT_KEY")
    private Long sortKey;

    @Column(name = "ACTIVE")
    private String active;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    public VCltUserCategory() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

}
