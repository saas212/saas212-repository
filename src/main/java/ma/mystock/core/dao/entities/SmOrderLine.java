/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "SM_ORDER_LINE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SmOrderLine.findAll", query = "SELECT s FROM SmOrderLine s")
    ,
    @NamedQuery(name = "SmOrderLine.findById", query = "SELECT s FROM SmOrderLine s WHERE s.id = :id")
    ,
    @NamedQuery(name = "SmOrderLine.findByPromotionId", query = "SELECT s FROM SmOrderLine s WHERE s.promotionId = :promotionId")
    ,
    @NamedQuery(name = "SmOrderLine.findByNegotiatePriceSale", query = "SELECT s FROM SmOrderLine s WHERE s.negotiatePriceSale = :negotiatePriceSale")
    ,
    @NamedQuery(name = "SmOrderLine.findByQuantity", query = "SELECT s FROM SmOrderLine s WHERE s.quantity = :quantity")
    ,
    @NamedQuery(name = "SmOrderLine.findByActive", query = "SELECT s FROM SmOrderLine s WHERE s.active = :active")
    ,
    @NamedQuery(name = "SmOrderLine.findByDateCreation", query = "SELECT s FROM SmOrderLine s WHERE s.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "SmOrderLine.findByUserCreation", query = "SELECT s FROM SmOrderLine s WHERE s.userCreation = :userCreation")
    ,
    @NamedQuery(name = "SmOrderLine.findByDateUpdate", query = "SELECT s FROM SmOrderLine s WHERE s.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "SmOrderLine.findByUserUpdate", query = "SELECT s FROM SmOrderLine s WHERE s.userUpdate = :userUpdate")})
public class SmOrderLine implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String findByOrderId = "SELECT s FROM SmOrderLine s WHERE s.orderId = :orderId";
    public static final String findById = "SELECT s FROM SmOrderLine s WHERE s.id = :id";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;

    @Column(name = "DESIGNATION")
    private String designation;

    @Column(name = "PROMOTION_ID")
    private Long promotionId;

    @Column(name = "NEGOTIATE_PRICE_SALE")
    private Double negotiatePriceSale;

    @Column(name = "QUANTITY")
    private Long quantity;

    @Column(name = "ACTIVE")
    private Character active;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "ORDER_ID")
    private Long orderId;

    @Column(name = "PRODUCT_ID")
    private Long productId;

    public SmOrderLine() {
    }

    public SmOrderLine(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Long getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Long promotionId) {
        this.promotionId = promotionId;
    }

    public Double getNegotiatePriceSale() {
        return negotiatePriceSale;
    }

    public void setNegotiatePriceSale(Double negotiatePriceSale) {
        this.negotiatePriceSale = negotiatePriceSale;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SmOrderLine)) {
            return false;
        }
        SmOrderLine other = (SmOrderLine) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entitys.SmOrderLine[ id=" + id + " ]";
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

}
