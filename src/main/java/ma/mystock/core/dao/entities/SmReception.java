/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "SM_RECEPTION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SmReception.findAll", query = "SELECT s FROM SmReception s")
    ,
    @NamedQuery(name = "SmReception.findById", query = "SELECT s FROM SmReception s WHERE s.id = :id")
    ,
    @NamedQuery(name = "SmReception.findByNoSeq", query = "SELECT s FROM SmReception s WHERE s.noSeq = :noSeq")
    ,
    @NamedQuery(name = "SmReception.findBySouche", query = "SELECT s FROM SmReception s WHERE s.souche = :souche")
    ,
    @NamedQuery(name = "SmReception.findByDepositId", query = "SELECT s FROM SmReception s WHERE s.depositId = :depositId")
    ,
    @NamedQuery(name = "SmReception.findByDeadline", query = "SELECT s FROM SmReception s WHERE s.deadline = :deadline")
    ,
    @NamedQuery(name = "SmReception.findByTva", query = "SELECT s FROM SmReception s WHERE s.tva = :tva")
    ,
    @NamedQuery(name = "SmReception.findByDelivery", query = "SELECT s FROM SmReception s WHERE s.delivery = :delivery")
    ,
    @NamedQuery(name = "SmReception.findByActive", query = "SELECT s FROM SmReception s WHERE s.active = :active")
    ,
    @NamedQuery(name = "SmReception.findByExpirationDate", query = "SELECT s FROM SmReception s WHERE s.expirationDate = :expirationDate")
    ,
    @NamedQuery(name = "SmReception.findByCltModuleId", query = "SELECT s FROM SmReception s WHERE s.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "SmReception.findByDateCreation", query = "SELECT s FROM SmReception s WHERE s.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "SmReception.findByUserCreation", query = "SELECT s FROM SmReception s WHERE s.userCreation = :userCreation")
    ,
    @NamedQuery(name = "SmReception.findByDateUpdate", query = "SELECT s FROM SmReception s WHERE s.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "SmReception.findByUserUpdate", query = "SELECT s FROM SmReception s WHERE s.userUpdate = :userUpdate")})
public class SmReception implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findById = "SELECT s FROM SmReception s WHERE s.id = :id";
    public static final String findByCltModuleIdAndOrderSupplierStatusId = "SELECT s FROM SmReception s WHERE s.cltModuleId = :cltModuleId and s.receptionStatusId = :receptionStatusId";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "NO_SEQ")
    private Long noSeq;
    @Size(max = 255)
    @Column(name = "SOUCHE")
    private String souche;
    @Column(name = "DEPOSIT_ID")
    private Long depositId;
    @Column(name = "DEADLINE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deadline;
    @Lob
    @Size(max = 65535)
    @Column(name = "NOTE")
    private String note;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "TVA")
    private Double tva;
    @Size(max = 255)
    @Column(name = "DELIVERY")
    private String delivery;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "EXPIRATION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expirationDate;
    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "REFERENCE")
    private String reference;

    @Column(name = "SUPPLIER_ID")
    private Long supplierId;

    @Column(name = "RECEPTION_STATUS_ID")
    private Long receptionStatusId;

    public SmReception() {
    }

    public SmReception(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNoSeq() {
        return noSeq;
    }

    public void setNoSeq(Long noSeq) {
        this.noSeq = noSeq;
    }

    public String getSouche() {
        return souche;
    }

    public void setSouche(String souche) {
        this.souche = souche;
    }

    public Long getDepositId() {
        return depositId;
    }

    public void setDepositId(Long depositId) {
        this.depositId = depositId;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Double getTva() {
        return tva;
    }

    public void setTva(Double tva) {
        this.tva = tva;
    }

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SmReception)) {
            return false;
        }
        SmReception other = (SmReception) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entitys.views.SmReception[ id=" + id + " ]";
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public Long getReceptionStatusId() {
        return receptionStatusId;
    }

    public void setReceptionStatusId(Long receptionStatusId) {
        this.receptionStatusId = receptionStatusId;
    }

}
