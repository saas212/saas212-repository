/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_SM_RECEPTION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmReception.findAll", query = "SELECT v FROM VSmReception v")
    ,
    @NamedQuery(name = "VSmReception.findById", query = "SELECT v FROM VSmReception v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VSmReception.findByNoSeq", query = "SELECT v FROM VSmReception v WHERE v.noSeq = :noSeq")
    ,
    @NamedQuery(name = "VSmReception.findBySouche", query = "SELECT v FROM VSmReception v WHERE v.souche = :souche")
    ,
    @NamedQuery(name = "VSmReception.findBySupplierId", query = "SELECT v FROM VSmReception v WHERE v.supplierId = :supplierId")
    ,
    @NamedQuery(name = "VSmReception.findByDepositId", query = "SELECT v FROM VSmReception v WHERE v.depositId = :depositId")
    ,
    @NamedQuery(name = "VSmReception.findByReceptionStatusId", query = "SELECT v FROM VSmReception v WHERE v.receptionStatusId = :receptionStatusId")
    ,
    @NamedQuery(name = "VSmReception.findByDeadline", query = "SELECT v FROM VSmReception v WHERE v.deadline = :deadline")
    ,
    @NamedQuery(name = "VSmReception.findByTva", query = "SELECT v FROM VSmReception v WHERE v.tva = :tva")
    ,
    @NamedQuery(name = "VSmReception.findByDelivery", query = "SELECT v FROM VSmReception v WHERE v.delivery = :delivery")
    ,
    @NamedQuery(name = "VSmReception.findByOrderSupplierId", query = "SELECT v FROM VSmReception v WHERE v.orderSupplierId = :orderSupplierId")
    ,
    @NamedQuery(name = "VSmReception.findByActive", query = "SELECT v FROM VSmReception v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VSmReception.findByExpirationDate", query = "SELECT v FROM VSmReception v WHERE v.expirationDate = :expirationDate")
    ,
    @NamedQuery(name = "VSmReception.findByCltModuleId", query = "SELECT v FROM VSmReception v WHERE v.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "VSmReception.findByDateCreation", query = "SELECT v FROM VSmReception v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VSmReception.findByUserCreation", query = "SELECT v FROM VSmReception v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VSmReception.findByDateUpdate", query = "SELECT v FROM VSmReception v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VSmReception.findByUserUpdate", query = "SELECT v FROM VSmReception v WHERE v.userUpdate = :userUpdate")
    ,
    @NamedQuery(name = "VSmReception.findBySupplierCompanyName", query = "SELECT v FROM VSmReception v WHERE v.supplierCompanyName = :supplierCompanyName")
    ,
    @NamedQuery(name = "VSmReception.findByTotalHorsTaxe", query = "SELECT v FROM VSmReception v WHERE v.totalHorsTaxe = :totalHorsTaxe")
    ,
    @NamedQuery(name = "VSmReception.findByTotalTtc", query = "SELECT v FROM VSmReception v WHERE v.totalTtc = :totalTtc")
    ,
    @NamedQuery(name = "VSmReception.findBySumProduct", query = "SELECT v FROM VSmReception v WHERE v.sumProduct = :sumProduct")
    ,
    @NamedQuery(name = "VSmReception.findByOrderSupplierStatusName", query = "SELECT v FROM VSmReception v WHERE v.orderSupplierStatusName = :orderSupplierStatusName")})
public class VSmReception implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findByCltModuleIdAndReceptionStatusId = "SELECT v FROM VSmReception v WHERE v.cltModuleId = :cltModuleId and v.receptionStatusId = :receptionStatusId";
    public static final String findById = "SELECT v FROM VSmReception v WHERE v.id = :id";
    public static final String findMaxNoSeqByCltModuleId = "SELECT max(v.noSeq) FROM VSmReception v WHERE v.cltModuleId = :cltModuleId";
    public static final String findByCltModuleId = "SELECT v FROM VSmReception v WHERE v.cltModuleId = :cltModuleId";
    public static final String findByReferenceAnCltModuleId = "SELECT v FROM VSmReception v WHERE v.reference = :reference and v.cltModuleId = :cltModuleId";

    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "NO_SEQ")
    private Long noSeq;

    @Size(max = 255)
    @Column(name = "SOUCHE")
    private String souche;

    @Column(name = "SUPPLIER_ID")
    private Long supplierId;

    @Column(name = "DEPOSIT_ID")
    private Long depositId;

    @Column(name = "RECEPTION_STATUS_ID")
    private Long receptionStatusId;

    @Column(name = "DEADLINE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deadline;

    @Lob

    @Column(name = "NOTE")
    private String note;

    @Column(name = "TVA")
    private Double tva;

    @Column(name = "DELIVERY")
    private String delivery;

    @Column(name = "ORDER_SUPPLIER_ID")
    private Long orderSupplierId;

    @Column(name = "ACTIVE")
    private String active;

    @Column(name = "EXPIRATION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expirationDate;

    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "SUPPLIER_COMPANY_NAME")
    private String supplierCompanyName;

    @Column(name = "TOTAL_HORS_TAXE")
    private Double totalHorsTaxe;

    @Column(name = "TOTAL_TTC")
    private Double totalTtc;

    @Column(name = "SUM_PRODUCT")
    private Long sumProduct;

    @Column(name = "ORDER_SUPPLIER_STATUS_NAME")
    private String orderSupplierStatusName;

    @Column(name = "REFERENCE")
    private String reference;

    public VSmReception() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNoSeq() {
        return noSeq;
    }

    public void setNoSeq(Long noSeq) {
        this.noSeq = noSeq;
    }

    public String getSouche() {
        return souche;
    }

    public void setSouche(String souche) {
        this.souche = souche;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public Long getDepositId() {
        return depositId;
    }

    public void setDepositId(Long depositId) {
        this.depositId = depositId;
    }

    public Long getReceptionStatusId() {
        return receptionStatusId;
    }

    public void setReceptionStatusId(Long receptionStatusId) {
        this.receptionStatusId = receptionStatusId;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Double getTva() {
        return tva;
    }

    public void setTva(Double tva) {
        this.tva = tva;
    }

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public Long getOrderSupplierId() {
        return orderSupplierId;
    }

    public void setOrderSupplierId(Long orderSupplierId) {
        this.orderSupplierId = orderSupplierId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getSupplierCompanyName() {
        return supplierCompanyName;
    }

    public void setSupplierCompanyName(String supplierCompanyName) {
        this.supplierCompanyName = supplierCompanyName;
    }

    public Double getTotalHorsTaxe() {
        return totalHorsTaxe;
    }

    public void setTotalHorsTaxe(Double totalHorsTaxe) {
        this.totalHorsTaxe = totalHorsTaxe;
    }

    public Double getTotalTtc() {
        return totalTtc;
    }

    public void setTotalTtc(Double totalTtc) {
        this.totalTtc = totalTtc;
    }

    public Long getSumProduct() {
        return sumProduct;
    }

    public void setSumProduct(Long sumProduct) {
        this.sumProduct = sumProduct;
    }

    public String getOrderSupplierStatusName() {
        return orderSupplierStatusName;
    }

    public void setOrderSupplierStatusName(String orderSupplierStatusName) {
        this.orderSupplierStatusName = orderSupplierStatusName;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

}
