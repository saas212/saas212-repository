/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdou
 */
@Entity
@Table(name = "PM_PAGE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PmPage.findAll", query = "SELECT p FROM PmPage p")
    ,
    @NamedQuery(name = "PmPage.findById", query = "SELECT p FROM PmPage p WHERE p.id = :id")
    ,
    @NamedQuery(name = "PmPage.findByName", query = "SELECT p FROM PmPage p WHERE p.name = :name")
    ,
    @NamedQuery(name = "PmPage.findBySortKey", query = "SELECT p FROM PmPage p WHERE p.sortKey = :sortKey")
    ,
    @NamedQuery(name = "PmPage.findByActive", query = "SELECT p FROM PmPage p WHERE p.active = :active")
    ,
    @NamedQuery(name = "PmPage.findByUserCreation", query = "SELECT p FROM PmPage p WHERE p.userCreation = :userCreation")
    ,
    @NamedQuery(name = "PmPage.findByDateCreation", query = "SELECT p FROM PmPage p WHERE p.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "PmPage.findByUserUpdate", query = "SELECT p FROM PmPage p WHERE p.userUpdate = :userUpdate")
    ,
    @NamedQuery(name = "PmPage.findByDateUpdate", query = "SELECT p FROM PmPage p WHERE p.dateUpdate = :dateUpdate")})
public class PmPage implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String findAllActive = "SELECT p FROM PmPage p WHERE p.active = :active";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "IN_DEV")
    private String inDev;

    @Column(name = "SORT_KEY")
    private Long sortKey;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "PAGE_TYPE_ID")
    private Long pmPageTypeId;
    @JoinColumn(name = "PAGE_TYPE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne
    private PmPageType pmPageType;

    public PmPage() {
    }

    public PmPage(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getPmPageTypeId() {
        return pmPageTypeId;
    }

    public void setPmPageTypeId(Long pmPageTypeId) {
        this.pmPageTypeId = pmPageTypeId;
    }

    public PmPageType getPmPageType() {
        return pmPageType;
    }

    public void setPmPageType(PmPageType pmPageType) {
        this.pmPageType = pmPageType;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PmPage)) {
            return false;
        }
        PmPage other = (PmPage) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.PmPage[ id=" + id + " ]";
    }

    public String getInDev() {
        return inDev;
    }

    public void setInDev(String inDev) {
        this.inDev = inDev;
    }

}
