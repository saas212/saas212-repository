/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_SM_RETURN_RECEIPT_LINE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmReturnReceiptLine.findAll", query = "SELECT v FROM VSmReturnReceiptLine v")
    ,
    @NamedQuery(name = "VSmReturnReceiptLine.findById", query = "SELECT v FROM VSmReturnReceiptLine v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VSmReturnReceiptLine.findByProductId", query = "SELECT v FROM VSmReturnReceiptLine v WHERE v.productId = :productId")
    ,
    @NamedQuery(name = "VSmReturnReceiptLine.findByReturnReceiptId", query = "SELECT v FROM VSmReturnReceiptLine v WHERE v.returnReceiptId = :returnReceiptId")
    ,
    @NamedQuery(name = "VSmReturnReceiptLine.findByQuantity", query = "SELECT v FROM VSmReturnReceiptLine v WHERE v.quantity = :quantity")
    ,
    @NamedQuery(name = "VSmReturnReceiptLine.findByPromotionId", query = "SELECT v FROM VSmReturnReceiptLine v WHERE v.promotionId = :promotionId")
    ,
    @NamedQuery(name = "VSmReturnReceiptLine.findByNegotiatePriceSale", query = "SELECT v FROM VSmReturnReceiptLine v WHERE v.negotiatePriceSale = :negotiatePriceSale")
    ,
    @NamedQuery(name = "VSmReturnReceiptLine.findByActive", query = "SELECT v FROM VSmReturnReceiptLine v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VSmReturnReceiptLine.findByDateCreation", query = "SELECT v FROM VSmReturnReceiptLine v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VSmReturnReceiptLine.findByUserCreation", query = "SELECT v FROM VSmReturnReceiptLine v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VSmReturnReceiptLine.findByDateUpdate", query = "SELECT v FROM VSmReturnReceiptLine v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VSmReturnReceiptLine.findByUserUpdate", query = "SELECT v FROM VSmReturnReceiptLine v WHERE v.userUpdate = :userUpdate")
    ,
    @NamedQuery(name = "VSmReturnReceiptLine.findByReference", query = "SELECT v FROM VSmReturnReceiptLine v WHERE v.reference = :reference")
    ,
    @NamedQuery(name = "VSmReturnReceiptLine.findByTotalPriceSale", query = "SELECT v FROM VSmReturnReceiptLine v WHERE v.totalPriceSale = :totalPriceSale")})
public class VSmReturnReceiptLine implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findByReturnReceiptId = "SELECT v FROM VSmReturnReceiptLine v WHERE v.returnReceiptId = :returnReceiptId";

    public static final String findById = "SELECT v FROM VSmReturnReceiptLine v WHERE v.id = :id";

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Id
    @Column(name = "PRODUCT_ID")
    private Long productId;

    @Column(name = "DESIGNATION")
    private String designation;
    @Column(name = "RETURN_RECEIPT_ID")
    private Long returnReceiptId;
    @Column(name = "QUANTITY")
    private Long quantity;
    @Column(name = "PROMOTION_ID")
    private Long promotionId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "NEGOTIATE_PRICE_SALE")
    private Double negotiatePriceSale;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "REFERENCE")
    private String reference;
    @Column(name = "TOTAL_PRICE_SALE")
    private Double totalPriceSale;

    public VSmReturnReceiptLine() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Long getReturnReceiptId() {
        return returnReceiptId;
    }

    public void setReturnReceiptId(Long returnReceiptId) {
        this.returnReceiptId = returnReceiptId;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Long promotionId) {
        this.promotionId = promotionId;
    }

    public Double getNegotiatePriceSale() {
        return negotiatePriceSale;
    }

    public void setNegotiatePriceSale(Double negotiatePriceSale) {
        this.negotiatePriceSale = negotiatePriceSale;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Double getTotalPriceSale() {
        return totalPriceSale;
    }

    public void setTotalPriceSale(Double totalPriceSale) {
        this.totalPriceSale = totalPriceSale;
    }

}
