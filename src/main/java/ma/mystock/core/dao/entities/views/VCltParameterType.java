/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * Abdou
 */
@Entity
@Table(name = "V_CLT_PARAMETER_TYPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCltParameterType.findAll", query = "SELECT v FROM VCltParameterType v")
    ,
    @NamedQuery(name = "VCltParameterType.findById", query = "SELECT v FROM VCltParameterType v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VCltParameterType.findByName", query = "SELECT v FROM VCltParameterType v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VCltParameterType.findBySortKey", query = "SELECT v FROM VCltParameterType v WHERE v.sortKey = :sortKey")
    ,
    @NamedQuery(name = "VCltParameterType.findByActive", query = "SELECT v FROM VCltParameterType v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VCltParameterType.findByDateCreation", query = "SELECT v FROM VCltParameterType v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VCltParameterType.findByUserCreation", query = "SELECT v FROM VCltParameterType v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VCltParameterType.findByDateUpdate", query = "SELECT v FROM VCltParameterType v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VCltParameterType.findByUserUpdate", query = "SELECT v FROM VCltParameterType v WHERE v.userUpdate = :userUpdate")})
public class VCltParameterType implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String findAllActive = "SELECT v FROM VCltParameterType v WHERE v.active = :active";
    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "SORT_KEY")
    private Long sortKey;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public VCltParameterType() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

}
