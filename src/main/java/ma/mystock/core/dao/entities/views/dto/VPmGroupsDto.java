/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_PM_GROUPS_DTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VPmGroupsDto.findAll", query = "SELECT v FROM VPmGroupsDto v")
    ,
    @NamedQuery(name = "VPmGroupsDto.findByCode", query = "SELECT v FROM VPmGroupsDto v WHERE v.code = :code")
    ,
    @NamedQuery(name = "VPmGroupsDto.findById", query = "SELECT v FROM VPmGroupsDto v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VPmGroupsDto.findByName", query = "SELECT v FROM VPmGroupsDto v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VPmGroupsDto.findByGroupTypeId", query = "SELECT v FROM VPmGroupsDto v WHERE v.groupTypeId = :groupTypeId")
    ,
    @NamedQuery(name = "VPmGroupsDto.findBySortKey", query = "SELECT v FROM VPmGroupsDto v WHERE v.sortKey = :sortKey")})
public class VPmGroupsDto implements Serializable {

    public static String findAll = "SELECT v FROM VPmGroupsDto v";
    public static String findByModelId = "SELECT v FROM VPmGroupsDto v WHERE v.modelId = :modelId order by v.sortKey";
    private static final Long serialVersionUID = 1L;

    @Id
    @Column(name = "CODE")
    private String code;
    @Basic(optional = false)

    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "GROUP_TYPE_ID")
    private Long groupTypeId;

    @Column(name = "SORT_KEY")
    private Long sortKey;

    @Column(name = "MODEL_ID")
    private Long modelId;

    transient List<VPmCategorysDto> vPmCategorysDtoList;

    ;
    
    public VPmGroupsDto() {
        this.vPmCategorysDtoList = new ArrayList<>();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getGroupTypeId() {
        return groupTypeId;
    }

    public void setGroupTypeId(Long groupTypeId) {
        this.groupTypeId = groupTypeId;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public Long getModelId() {
        return modelId;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    public List<VPmCategorysDto> getvPmCategorysDtoList() {
        return vPmCategorysDtoList;
    }

    public void setvPmCategorysDtoList(List<VPmCategorysDto> vPmCategorysDtoList) {
        this.vPmCategorysDtoList = vPmCategorysDtoList;
    }

}
