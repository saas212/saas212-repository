package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_INF_PRIVILEGES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VInfPrivileges.findAll", query = "SELECT v FROM VInfPrivileges v")
    ,
    @NamedQuery(name = "VInfPrivileges.findByCode", query = "SELECT v FROM VInfPrivileges v WHERE v.code = :code")
    ,
    @NamedQuery(name = "VInfPrivileges.findByItemCode", query = "SELECT v FROM VInfPrivileges v WHERE v.itemCode = :itemCode")
    ,
    @NamedQuery(name = "VInfPrivileges.findByRoleId", query = "SELECT v FROM VInfPrivileges v WHERE v.roleId = :roleId")
    ,
    @NamedQuery(name = "VInfPrivileges.findByRoleName", query = "SELECT v FROM VInfPrivileges v WHERE v.roleName = :roleName")
    ,
    @NamedQuery(name = "VInfPrivileges.findByGroupId", query = "SELECT v FROM VInfPrivileges v WHERE v.groupId = :groupId")
    ,
    @NamedQuery(name = "VInfPrivileges.findByGroupName", query = "SELECT v FROM VInfPrivileges v WHERE v.groupName = :groupName")
    ,
    @NamedQuery(name = "VInfPrivileges.findByUserId", query = "SELECT v FROM VInfPrivileges v WHERE v.userId = :userId")
    ,
    @NamedQuery(name = "VInfPrivileges.findByModuleId", query = "SELECT v FROM VInfPrivileges v WHERE v.moduleId = :moduleId")
    ,
    @NamedQuery(name = "VInfPrivileges.findByClientId", query = "SELECT v FROM VInfPrivileges v WHERE v.clientId = :clientId")})
public class VInfPrivileges implements Serializable {

    public static String findAll = "SELECT v FROM VInfPrivileges v";

    private static final Long serialVersionUID = 1L;

    @Column(name = "CODE")
    @Id
    private String code;

    @Column(name = "ITEM_CODE")
    private String itemCode;

    @Column(name = "ROLE_ID")
    private Long roleId;

    @Column(name = "ROLE_NAME")
    private String roleName;

    @Column(name = "GROUP_ID")
    private Long groupId;

    @Column(name = "GROUP_NAME")
    private String groupName;

    @Column(name = "USER_ID")
    private Long userId;

    @Column(name = "MODULE_ID")
    private Long moduleId;

    @Column(name = "CLIENT_ID")
    private Long clientId;

    public VInfPrivileges() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

}
