/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Yamine Abdelmounaim <yaminea@evision.ca>
 */
@Entity
@Table(name = "CLT_MODULE_PARAMETER_CLIENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CltModuleParameterClient.findAll", query = "SELECT c FROM CltModuleParameterClient c")
    ,
    @NamedQuery(name = "CltModuleParameterClient.findById", query = "SELECT c FROM CltModuleParameterClient c WHERE c.id = :id")
    ,
    @NamedQuery(name = "CltModuleParameterClient.findByModuleParameterId", query = "SELECT c FROM CltModuleParameterClient c WHERE c.moduleParameterId = :moduleParameterId")
    ,
    @NamedQuery(name = "CltModuleParameterClient.findByModuleId", query = "SELECT c FROM CltModuleParameterClient c WHERE c.moduleId = :moduleId")
    ,
    @NamedQuery(name = "CltModuleParameterClient.findByActive", query = "SELECT c FROM CltModuleParameterClient c WHERE c.active = :active")
    ,
    @NamedQuery(name = "CltModuleParameterClient.findByDateCreation", query = "SELECT c FROM CltModuleParameterClient c WHERE c.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "CltModuleParameterClient.findByUserCreation", query = "SELECT c FROM CltModuleParameterClient c WHERE c.userCreation = :userCreation")
    ,
    @NamedQuery(name = "CltModuleParameterClient.findByDateUpdate", query = "SELECT c FROM CltModuleParameterClient c WHERE c.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "CltModuleParameterClient.findByUserUpdate", query = "SELECT c FROM CltModuleParameterClient c WHERE c.userUpdate = :userUpdate")})
public class CltModuleParameterClient implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String findById = "SELECT c FROM CltModuleParameterClient c WHERE c.id = :id";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "MODULE_PARAMETER_ID")
    private Long moduleParameterId;
    @JoinColumn(name = "MODULE_PARAMETER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne
    private CltModuleParameter cltModuleParameter;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "VALUE")
    private String value;
    @Column(name = "MODULE_ID")
    private Long moduleId;
    @JoinColumn(name = "MODULE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne
    private CltModule cltModule;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public CltModuleParameterClient() {
    }

    public CltModuleParameterClient(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getModuleParameterId() {
        return moduleParameterId;
    }

    public void setModuleParameterId(Long moduleParameterId) {
        this.moduleParameterId = moduleParameterId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CltModuleParameterClient)) {
            return false;
        }
        CltModuleParameterClient other = (CltModuleParameterClient) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entitys.CltModuleParameterClient[ id=" + id + " ]";
    }

}
