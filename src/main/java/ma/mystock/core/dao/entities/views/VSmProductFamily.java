/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_SM_PRODUCT_FAMILY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmProductFamily.findAll", query = "SELECT v FROM VSmProductFamily v")
    ,
    @NamedQuery(name = "VSmProductFamily.findById", query = "SELECT v FROM VSmProductFamily v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VSmProductFamily.findByName", query = "SELECT v FROM VSmProductFamily v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VSmProductFamily.findByProductGroupId", query = "SELECT v FROM VSmProductFamily v WHERE v.productGroupId = :productGroupId")
    ,
    @NamedQuery(name = "VSmProductFamily.findBySortKey", query = "SELECT v FROM VSmProductFamily v WHERE v.sortKey = :sortKey")
    ,
    @NamedQuery(name = "VSmProductFamily.findByActive", query = "SELECT v FROM VSmProductFamily v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VSmProductFamily.findByCltModuleId", query = "SELECT v FROM VSmProductFamily v WHERE v.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "VSmProductFamily.findByDateCreation", query = "SELECT v FROM VSmProductFamily v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VSmProductFamily.findByUserCreation", query = "SELECT v FROM VSmProductFamily v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VSmProductFamily.findByDateUpdate", query = "SELECT v FROM VSmProductFamily v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VSmProductFamily.findByUserUpdate", query = "SELECT v FROM VSmProductFamily v WHERE v.userUpdate = :userUpdate")})
public class VSmProductFamily implements Serializable {

    public static final String findByCltModuleIdAndActive = "SELECT v FROM VSmProductFamily v WHERE v.cltModuleId = :cltModuleId and v.active = :active";
    public static final String findByCltModuleIdAndProductGroupIdAndActive = "SELECT v FROM VSmProductFamily v WHERE v.cltModuleId = :cltModuleId and v.productGroupId = :productGroupId and v.active = :active";

    private static final Long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @Id
    private Long id;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "PRODUCT_GROUP_ID")
    private Long productGroupId;
    @Column(name = "SORT_KEY")
    private Long sortKey;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public VSmProductFamily() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getProductGroupId() {
        return productGroupId;
    }

    public void setProductGroupId(Long productGroupId) {
        this.productGroupId = productGroupId;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

}
