package ma.mystock.core.dao.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "CTA_ENTITY_PHONE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CtaEntityPhone.findAll", query = "SELECT c FROM CtaEntityPhone c")
    ,
    @NamedQuery(name = "CtaEntityPhone.findById", query = "SELECT c FROM CtaEntityPhone c WHERE c.id = :id")
    ,
    @NamedQuery(name = "CtaEntityPhone.findByCountryCode", query = "SELECT c FROM CtaEntityPhone c WHERE c.countryCode = :countryCode")
    ,
    @NamedQuery(name = "CtaEntityPhone.findByNumber", query = "SELECT c FROM CtaEntityPhone c WHERE c.number = :number")
    ,
    @NamedQuery(name = "CtaEntityPhone.findByDateCreation", query = "SELECT c FROM CtaEntityPhone c WHERE c.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "CtaEntityPhone.findByUserCreation", query = "SELECT c FROM CtaEntityPhone c WHERE c.userCreation = :userCreation")
    ,
    @NamedQuery(name = "CtaEntityPhone.findByDateUpdate", query = "SELECT c FROM CtaEntityPhone c WHERE c.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "CtaEntityPhone.findByUserUpdate", query = "SELECT c FROM CtaEntityPhone c WHERE c.userUpdate = :userUpdate")})
public class CtaEntityPhone implements Serializable {

    public static final String findById = "SELECT c FROM CtaEntityPhone c WHERE c.id = :id";

    private static final Long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;

    @Column(name = "COUNTRY_CODE")
    private String countryCode;

    @Column(name = "NUMBER")
    private String number;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "PRIORITY")
    private Long priority;

    @Column(name = "ENTITY_ID")
    private Long entityId;

    @Column(name = "PHONE_TYPE_ID")
    private Long phoneTypeId;

    public CtaEntityPhone() {
    }

    public CtaEntityPhone(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CtaEntityPhone)) {
            return false;
        }
        CtaEntityPhone other = (CtaEntityPhone) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.dao.entities.CtaEntityPhone[ id=" + id + " ]";
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Long getPhoneTypeId() {
        return phoneTypeId;
    }

    public void setPhoneTypeId(Long phoneTypeId) {
        this.phoneTypeId = phoneTypeId;
    }

}
