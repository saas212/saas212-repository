/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * Abdou
 */
@Entity
@Table(name = "V_CLT_MODULE_PARAMETER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCltModuleParameter.findAll", query = "SELECT v FROM VCltModuleParameter v")
    ,
    @NamedQuery(name = "VCltModuleParameter.findById", query = "SELECT v FROM VCltModuleParameter v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VCltModuleParameter.findByName", query = "SELECT v FROM VCltModuleParameter v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VCltModuleParameter.findByModuleParameterTypeId", query = "SELECT v FROM VCltModuleParameter v WHERE v.moduleParameterTypeId = :moduleParameterTypeId")
    ,
    @NamedQuery(name = "VCltModuleParameter.findByCltModuleParameterTypeName", query = "SELECT v FROM VCltModuleParameter v WHERE v.cltModuleParameterTypeName = :cltModuleParameterTypeName")
    ,
    @NamedQuery(name = "VCltModuleParameter.findByActive", query = "SELECT v FROM VCltModuleParameter v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VCltModuleParameter.findByCanBeDelete", query = "SELECT v FROM VCltModuleParameter v WHERE v.canBeDelete = :canBeDelete")})

public class VCltModuleParameter implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String findById = "SELECT v FROM VCltModuleParameter v WHERE v.id = :id";
    public static final String findAll = "SELECT v FROM VCltModuleParameter v";
    public static final String findAllActive = "SELECT v FROM VCltModuleParameter v WHERE v.active = :active";
    public static final String findAllActiveAndId = "SELECT v FROM VCltModuleParameter v WHERE v.active = :active and ((v.id = :id) or v.id not in (select cv.cltModuleParameterId from VCltModuleParameterClient cv where  cv.moduleId = :moduleId ))";

    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "DESCRIPTION")
    private String description;
    @Lob
    @Size(max = 65535)
    @Column(name = "DEFAULT_VALUE")
    private String defaultValue;
    @Column(name = "MODULE_PARAMETER_TYPE_ID")
    private Long moduleParameterTypeId;
    @Size(max = 255)
    @Column(name = "CLT_MODULE_PARAMETER_TYPE_NAME")
    private String cltModuleParameterTypeName;
    @Column(name = "ACTIVE")
    private String active;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "CAN_BE_DELETE")
    private boolean canBeDelete;

    public VCltModuleParameter() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public Long getModuleParameterTypeId() {
        return moduleParameterTypeId;
    }

    public void setModuleParameterTypeId(Long moduleParameterTypeId) {
        this.moduleParameterTypeId = moduleParameterTypeId;
    }

    public String getCltModuleParameterTypeName() {
        return cltModuleParameterTypeName;
    }

    public void setCltModuleParameterTypeName(String cltModuleParameterTypeName) {
        this.cltModuleParameterTypeName = cltModuleParameterTypeName;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public boolean getCanBeDelete() {
        return canBeDelete;
    }

    public void setCanBeDelete(boolean canBeDelete) {
        this.canBeDelete = canBeDelete;
    }

}
