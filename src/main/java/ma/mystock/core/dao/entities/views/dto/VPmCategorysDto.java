package ma.mystock.core.dao.entities.views.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_PM_CATEGORYS_DTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VPmCategorysDto.findAll", query = "SELECT v FROM VPmCategorysDto v")
    ,
    @NamedQuery(name = "VPmCategorysDto.findByGroupId", query = "SELECT v FROM VPmCategorysDto v WHERE v.groupId = :groupId")
    ,
    @NamedQuery(name = "VPmCategorysDto.findByCode", query = "SELECT v FROM VPmCategorysDto v WHERE v.code = :code")
    ,
    @NamedQuery(name = "VPmCategorysDto.findById", query = "SELECT v FROM VPmCategorysDto v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VPmCategorysDto.findByName", query = "SELECT v FROM VPmCategorysDto v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VPmCategorysDto.findByCategoryTypeId", query = "SELECT v FROM VPmCategorysDto v WHERE v.categoryTypeId = :categoryTypeId")
    ,
    @NamedQuery(name = "VPmCategorysDto.findByCategorySort", query = "SELECT v FROM VPmCategorysDto v WHERE v.categorySort = :categorySort")})

public class VPmCategorysDto implements Serializable {

    public static String findAll = "SELECT v FROM VPmCategorysDto v";
    public static String findByGroupId = "SELECT v FROM VPmCategorysDto v WHERE v.groupId = :groupId"; // groupId
    public static String findByModelIdAndGroupId = "SELECT v FROM VPmCategorysDto v WHERE v.groupId = :groupId and v.modelId = :modelId order by v.categorySort";

    private static final Long serialVersionUID = 1L;
    @Id
    @Size(max = 62)
    @Column(name = "CODE")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "CATEGORY_TYPE_ID")
    private Long categoryTypeId;
    @Column(name = "CATEGORY_SORT")
    private Long categorySort;
    @Column(name = "MODEL_ID")
    private Long modelId;
    @Column(name = "GROUP_ID")
    private Long groupId;

    @Column(name = "IMAGE_PATH")
    private String imagePath;

    transient List<VPmMenusDto> vPmMenusDtoList;

    ;
    
    public VPmCategorysDto() {
        this.vPmMenusDtoList = new ArrayList<>();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCategoryTypeId() {
        return categoryTypeId;
    }

    public void setCategoryTypeId(Long categoryTypeId) {
        this.categoryTypeId = categoryTypeId;
    }

    public Long getCategorySort() {
        return categorySort;
    }

    public void setCategorySort(Long categorySort) {
        this.categorySort = categorySort;
    }

    public Long getModelId() {
        return modelId;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public List<VPmMenusDto> getvPmMenusDtoList() {
        return vPmMenusDtoList;
    }

    public void setvPmMenusDtoList(List<VPmMenusDto> vPmMenusDtoList) {
        this.vPmMenusDtoList = vPmMenusDtoList;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

}
