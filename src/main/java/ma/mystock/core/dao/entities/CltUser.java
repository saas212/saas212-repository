package ma.mystock.core.dao.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "CLT_USER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CltUser.findAll", query = "SELECT c FROM CltUser c")
    ,
    @NamedQuery(name = "CltUser.findById", query = "SELECT c FROM CltUser c WHERE c.id = :id")
    ,
    @NamedQuery(name = "CltUser.findByFirstName", query = "SELECT c FROM CltUser c WHERE c.firstName = :firstName")
    ,
    @NamedQuery(name = "CltUser.findByLastName", query = "SELECT c FROM CltUser c WHERE c.lastName = :lastName")
    ,
    @NamedQuery(name = "CltUser.findByUsername", query = "SELECT c FROM CltUser c WHERE c.username = :username")
    ,
    @NamedQuery(name = "CltUser.findByPassword", query = "SELECT c FROM CltUser c WHERE c.password = :password")
    ,
    @NamedQuery(name = "CltUser.findByActive", query = "SELECT c FROM CltUser c WHERE c.active = :active")
    ,
    @NamedQuery(name = "CltUser.findByDateCreation", query = "SELECT c FROM CltUser c WHERE c.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "CltUser.findByUserCreation", query = "SELECT c FROM CltUser c WHERE c.userCreation = :userCreation")
    ,
    @NamedQuery(name = "CltUser.findByDateUpdate", query = "SELECT c FROM CltUser c WHERE c.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "CltUser.findByUserUpdate", query = "SELECT c FROM CltUser c WHERE c.userUpdate = :userUpdate")})
public class CltUser implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findById = "SELECT c FROM CltUser c WHERE c.id = :id";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "ACTIVE")
    private String active;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "ENTITY_ID")
    private Long entityId;

    @Column(name = "CATEGORY_ID")
    private Long categoryId;

    @Column(name = "CLIENT_ID")
    private Long clientId;

    @Column(name = "USER_STATUS_ID")
    private Long userStatusId;

    public CltUser() {
    }

    public CltUser(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CltUser)) {
            return false;
        }
        CltUser other = (CltUser) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entitys.CltUser[ id=" + id + " ]";
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getUserStatusId() {
        return userStatusId;
    }

    public void setUserStatusId(Long userStatusId) {
        this.userStatusId = userStatusId;
    }

}
