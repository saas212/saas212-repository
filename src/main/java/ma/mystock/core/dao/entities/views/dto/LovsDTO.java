/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views.dto;

import java.io.Serializable;
import ma.mystock.core.dao.entities.views.VSmLovs;

/**
 *
 * @author abdou
 */
public class LovsDTO implements Serializable {

    private Long id;
    private String entity;
    private String name;
    private String description;
    private String active;
    private Long sortKey;
    private Long cltModuleId;
    private String value;

    // Ajouter d'autre champs ... 
    public LovsDTO(Long id, String entity, String name, String description, String value, String active, Long sortKey, Long cltModuleId) {

        this.id = id;
        this.entity = entity;
        this.name = name;
        this.description = description;
        this.value = value;
        this.active = active;
        this.sortKey = sortKey;
        this.cltModuleId = cltModuleId;

    }

    public LovsDTO(VSmLovs o) {

        this.id = o.getId();
        this.name = o.getName();
        this.description = o.getDescription();
        this.value = o.getValue();
        this.active = o.getActive();
        this.sortKey = o.getSortKey();
        this.cltModuleId = o.getCltModuleId();

        //Method m = null;
        //try{ m = o.getClass().getMethod("getId",null); this.id =  (Long) m.invoke(o,null);}catch(Exception ex1){ ex1.printStackTrace(); }
        //VSmSupplierType lov = new VSmSupplierType();
        //lov = (VSmSupplierType) o;
        //this.name = lov.getName();
        //this.description = lov.getDescription();
        //this.value = lov.getValue();
        //this.active = lov.getActive();
        //this.sortKey = lov.getSortKey();
        //this.cltModuleId = lov.getCltModuleId();
    }

    public LovsDTO() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

}
