/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdessamad HALLAL
 */
@Entity
@Table(name = "V_PM_PAGE_ATTRIBUTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VPmPageAttribute.findAll", query = "SELECT v FROM VPmPageAttribute v")
    ,
    @NamedQuery(name = "VPmPageAttribute.findById", query = "SELECT v FROM VPmPageAttribute v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VPmPageAttribute.findByPageId", query = "SELECT v FROM VPmPageAttribute v WHERE v.pageId = :pageId")
    ,
    @NamedQuery(name = "VPmPageAttribute.findByInfItemCode", query = "SELECT v FROM VPmPageAttribute v WHERE v.infItemCode = :infItemCode")
    ,
    @NamedQuery(name = "VPmPageAttribute.findByPmComponentId", query = "SELECT v FROM VPmPageAttribute v WHERE v.pmComponentId = :pmComponentId")
    ,
    @NamedQuery(name = "VPmPageAttribute.findBySortKey", query = "SELECT v FROM VPmPageAttribute v WHERE v.sortKey = :sortKey")
    ,
    @NamedQuery(name = "VPmPageAttribute.findByActive", query = "SELECT v FROM VPmPageAttribute v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VPmPageAttribute.findByIsRequired", query = "SELECT v FROM VPmPageAttribute v WHERE v.isRequired = :isRequired")
    ,
    @NamedQuery(name = "VPmPageAttribute.findByIsReadonly", query = "SELECT v FROM VPmPageAttribute v WHERE v.isReadonly = :isReadonly")
    ,
    @NamedQuery(name = "VPmPageAttribute.findByIsHidden", query = "SELECT v FROM VPmPageAttribute v WHERE v.isHidden = :isHidden")
    ,
    @NamedQuery(name = "VPmPageAttribute.findByDataTypeId", query = "SELECT v FROM VPmPageAttribute v WHERE v.dataTypeId = :dataTypeId")
    ,
    @NamedQuery(name = "VPmPageAttribute.findByFormatTypeId", query = "SELECT v FROM VPmPageAttribute v WHERE v.formatTypeId = :formatTypeId")
    ,
    @NamedQuery(name = "VPmPageAttribute.findByMaxLenght", query = "SELECT v FROM VPmPageAttribute v WHERE v.maxLenght = :maxLenght")
    ,
    @NamedQuery(name = "VPmPageAttribute.findByMaxWord", query = "SELECT v FROM VPmPageAttribute v WHERE v.maxWord = :maxWord")
    ,
    @NamedQuery(name = "VPmPageAttribute.findByDateCreation", query = "SELECT v FROM VPmPageAttribute v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VPmPageAttribute.findByDateUpdate", query = "SELECT v FROM VPmPageAttribute v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VPmPageAttribute.findByUserCreation", query = "SELECT v FROM VPmPageAttribute v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VPmPageAttribute.findByUserUpdate", query = "SELECT v FROM VPmPageAttribute v WHERE v.userUpdate = :userUpdate")})
public class VPmPageAttribute implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String findByPageId = "SELECT v FROM VPmPageAttribute v WHERE v.pageId = :pageId";

    @NotNull
    @Id
    @Column(name = "ID")
    private long id;

    @Column(name = "PAGE_ID")
    private Long pageId;

    @Column(name = "INF_ITEM_CODE")
    private String infItemCode;

    @Column(name = "PM_COMPONENT_ID")
    private Long pmComponentId;

    @Column(name = "SORT_KEY")
    private Long sortKey;

    @Column(name = "ACTIVE")
    private String active;

    @Column(name = "IS_REQUIRED")
    private String isRequired;

    @Column(name = "IS_READONLY")
    private String isReadonly;

    @Column(name = "IS_HIDDEN")
    private String isHidden;

    @Column(name = "DATA_TYPE_ID")
    private Long dataTypeId;

    @Column(name = "FORMAT_TYPE_ID")
    private Long formatTypeId;

    @Column(name = "MAX_LENGHT")
    private Long maxLenght;

    @Column(name = "MAX_WORD")
    private Long maxWord;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "DATE_UPDATE")
    private String dateUpdate;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public VPmPageAttribute() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getPageId() {
        return pageId;
    }

    public void setPageId(Long pageId) {
        this.pageId = pageId;
    }

    public String getInfItemCode() {
        return infItemCode;
    }

    public void setInfItemCode(String infItemCode) {
        this.infItemCode = infItemCode;
    }

    public Long getPmComponentId() {
        return pmComponentId;
    }

    public void setPmComponentId(Long pmComponentId) {
        this.pmComponentId = pmComponentId;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(String isRequired) {
        this.isRequired = isRequired;
    }

    public String getIsReadonly() {
        return isReadonly;
    }

    public void setIsReadonly(String isReadonly) {
        this.isReadonly = isReadonly;
    }

    public String getIsHidden() {
        return isHidden;
    }

    public void setIsHidden(String isHidden) {
        this.isHidden = isHidden;
    }

    public Long getDataTypeId() {
        return dataTypeId;
    }

    public void setDataTypeId(Long dataTypeId) {
        this.dataTypeId = dataTypeId;
    }

    public Long getFormatTypeId() {
        return formatTypeId;
    }

    public void setFormatTypeId(Long formatTypeId) {
        this.formatTypeId = formatTypeId;
    }

    public Long getMaxLenght() {
        return maxLenght;
    }

    public void setMaxLenght(Long maxLenght) {
        this.maxLenght = maxLenght;
    }

    public Long getMaxWord() {
        return maxWord;
    }

    public void setMaxWord(Long maxWord) {
        this.maxWord = maxWord;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(String dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

}
