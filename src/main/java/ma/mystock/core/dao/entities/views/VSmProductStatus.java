/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_SM_PRODUCT_STATUS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmProductStatus.findAll", query = "SELECT v FROM VSmProductStatus v")
    ,
    @NamedQuery(name = "VSmProductStatus.findById", query = "SELECT v FROM VSmProductStatus v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VSmProductStatus.findByName", query = "SELECT v FROM VSmProductStatus v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VSmProductStatus.findByActive", query = "SELECT v FROM VSmProductStatus v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VSmProductStatus.findBySortKey", query = "SELECT v FROM VSmProductStatus v WHERE v.sortKey = :sortKey")})
public class VSmProductStatus implements Serializable {

    public static final String findByActive = "SELECT v FROM VSmProductStatus v WHERE v.active = :active";

    private static final Long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @Id
    private Long id;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "SORT_KEY")
    private Long sortKey;

    public VSmProductStatus() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

}
