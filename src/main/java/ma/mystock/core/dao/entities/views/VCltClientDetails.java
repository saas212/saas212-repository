/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_CLT_CLIENT_DETAILS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCltClientDetails.findAll", query = "SELECT v FROM VCltClientDetails v")
    ,
    @NamedQuery(name = "VCltClientDetails.findById", query = "SELECT v FROM VCltClientDetails v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VCltClientDetails.findByFirstName", query = "SELECT v FROM VCltClientDetails v WHERE v.firstName = :firstName")
    ,
    @NamedQuery(name = "VCltClientDetails.findByLastName", query = "SELECT v FROM VCltClientDetails v WHERE v.lastName = :lastName")
    ,
    @NamedQuery(name = "VCltClientDetails.findByCompanyName", query = "SELECT v FROM VCltClientDetails v WHERE v.companyName = :companyName")
    ,
    @NamedQuery(name = "VCltClientDetails.findByClientStatusId", query = "SELECT v FROM VCltClientDetails v WHERE v.clientStatusId = :clientStatusId")
    ,
    @NamedQuery(name = "VCltClientDetails.findByActive", query = "SELECT v FROM VCltClientDetails v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VCltClientDetails.findByDateCreation", query = "SELECT v FROM VCltClientDetails v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VCltClientDetails.findByUserCreation", query = "SELECT v FROM VCltClientDetails v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VCltClientDetails.findByDateUpdate", query = "SELECT v FROM VCltClientDetails v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VCltClientDetails.findByUserUpdate", query = "SELECT v FROM VCltClientDetails v WHERE v.userUpdate = :userUpdate")})
public class VCltClientDetails implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static String findById = "SELECT v FROM VCltClientDetails v WHERE v.id = :id";

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @Id
    private Long id;
    @Size(max = 255)
    @Column(name = "FIRST_NAME")
    private String firstName;
    @Size(max = 255)
    @Column(name = "LAST_NAME")
    private String lastName;
    @Size(max = 255)
    @Column(name = "COMPANY_NAME")
    private String companyName;

    @Column(name = "CLIENT_STATUS_ID")
    private Long clientStatusId;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public VCltClientDetails() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Long getClientStatusId() {
        return clientStatusId;
    }

    public void setClientStatusId(Long clientStatusId) {
        this.clientStatusId = clientStatusId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

}
