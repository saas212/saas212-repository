package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_SM_PRODUCT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmProduct.findAll", query = "SELECT v FROM VSmProduct v")
    ,
    @NamedQuery(name = "VSmProduct.findById", query = "SELECT v FROM VSmProduct v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VSmProduct.findByReference", query = "SELECT v FROM VSmProduct v WHERE v.reference = :reference")
    ,
    @NamedQuery(name = "VSmProduct.findByDesignation", query = "SELECT v FROM VSmProduct v WHERE v.designation = :designation")
    ,
    @NamedQuery(name = "VSmProduct.findByQuantity", query = "SELECT v FROM VSmProduct v WHERE v.quantity = :quantity")
    ,
    @NamedQuery(name = "VSmProduct.findByPriceSale", query = "SELECT v FROM VSmProduct v WHERE v.priceSale = :priceSale")
    ,
    @NamedQuery(name = "VSmProduct.findByPriceBuy", query = "SELECT v FROM VSmProduct v WHERE v.priceBuy = :priceBuy")
    ,
    @NamedQuery(name = "VSmProduct.findByActive", query = "SELECT v FROM VSmProduct v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VSmProduct.findByCltModuleId", query = "SELECT v FROM VSmProduct v WHERE v.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "VSmProduct.findByProductGroupId", query = "SELECT v FROM VSmProduct v WHERE v.productGroupId = :productGroupId")
    ,
    @NamedQuery(name = "VSmProduct.findByProductGroupName", query = "SELECT v FROM VSmProduct v WHERE v.productGroupName = :productGroupName")
    ,
    @NamedQuery(name = "VSmProduct.findByProductFamilyId", query = "SELECT v FROM VSmProduct v WHERE v.productFamilyId = :productFamilyId")
    ,
    @NamedQuery(name = "VSmProduct.findByProductFamilyName", query = "SELECT v FROM VSmProduct v WHERE v.productFamilyName = :productFamilyName")
    ,
    @NamedQuery(name = "VSmProduct.findByProductSizeId", query = "SELECT v FROM VSmProduct v WHERE v.productSizeId = :productSizeId")
    ,
    @NamedQuery(name = "VSmProduct.findByProductSizeName", query = "SELECT v FROM VSmProduct v WHERE v.productSizeName = :productSizeName")
    ,
    @NamedQuery(name = "VSmProduct.findByProductStatusId", query = "SELECT v FROM VSmProduct v WHERE v.productStatusId = :productStatusId")
    ,
    @NamedQuery(name = "VSmProduct.findByProductStatusName", query = "SELECT v FROM VSmProduct v WHERE v.productStatusName = :productStatusName")
    ,
    @NamedQuery(name = "VSmProduct.findByProductTypeId", query = "SELECT v FROM VSmProduct v WHERE v.productTypeId = :productTypeId")})
public class VSmProduct implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findAll = "SELECT v FROM VSmProduct v";
    public static final String findById = "SELECT v FROM VSmProduct v WHERE v.id = :id";
    public static final String findByCltModuleIdAndActive = "SELECT v FROM VSmProduct v WHERE v.cltModuleId = :cltModuleId and v.active = :active";
    public static final String findByCltModuleId = "SELECT v FROM VSmProduct v WHERE v.cltModuleId = :cltModuleId";
    public static final String findByReference = "SELECT v FROM VSmProduct v WHERE v.reference = :reference";
    public static final String findByReferenceAnCltModuleId = "SELECT v FROM VSmProduct v WHERE v.reference = :reference and v.cltModuleId = :cltModuleId";
    public static final String findByReferenceAndProductSizeIdAndProductColorId = "SELECT v FROM VSmProduct v WHERE v.reference = :reference and v.productSizeId = :productSizeId and v.productColorId = :productColorId";
    public static final String findByCltModuleIdAndReferenceAndActive = "SELECT v FROM VSmProduct v WHERE v.cltModuleId = :cltModuleId and v.reference = :reference and v.active = :active";
    public static final String findMaxNoSeqByCltModuleId = "select max(v.noSeq) as noSeq from VSmProduct v where v.cltModuleId = :cltModuleId";
    public static final String findLikeDesignationAndCltModuleId = "SELECT v FROM VSmProduct v WHERE lower(v.designation) like :designation and v.cltModuleId = :cltModuleId";
    public static final String findLikeReferenceAndCltModuleId = "SELECT v FROM VSmProduct v WHERE lower(v.reference) like lower(:reference) and v.cltModuleId = :cltModuleId";
    public static final String findByDesignationAnCltModuleId = "SELECT v FROM VSmProduct v WHERE v.designation = :designation and v.cltModuleId = :cltModuleId";

    public static final String findByLikeReferenceOrLikeDesignationOrFamilyIdOrTypeIdOrDepartmentIdOrSizeIdOrColorIdOrQuantityAndCltModuleId = ""
            + "SELECT v FROM VSmProduct v WHERE "
            + "     (:reference is null or :reference = '' or lower(v.reference) like :reference) "
            + " and (:designation is null or :designation = '' or lower(v.designation) like :designation) "
            + " and (:productFamilyId is null or v.productFamilyId = :productFamilyId) "
            + " and (:productTypeId is null or v.productTypeId = :productTypeId) "
            + " and (:productDepartmentId is null or v.productDepartmentId = :productDepartmentId) "
            + " and (:productSizeId is null or v.productSizeId = :productSizeId) "
            + " and (:productColorId is null   or v.productColorId = :productColorId) "
            + " and (:quantity is null  or v.quantity = :quantity) "
            + " and (v.cltModuleId = :cltModuleId)";

    public static final String findByActiveAndCltModuleIdOrderByIdDesc = "SELECT v FROM VSmProduct v WHERE v.cltModuleId = :cltModuleId and v.active = :active order by v.id desc";

    @Column(name = "id")
    @Id
    private Long id;

    @Column(name = "NO_SEQ")
    private Long noSeq;

    @Column(name = "REFERENCE")
    private String reference;

    @Column(name = "DESIGNATION")
    private String designation;

    @Column(name = "QUANTITY")
    private Long quantity;

    @Column(name = "PRICE_SALE")
    private Double priceSale;

    @Column(name = "PRICE_BUY")
    private Double priceBuy;

    @Lob
    @Column(name = "NOTE")
    private String note;

    @Column(name = "ACTIVE")
    private String active;

    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    @Column(name = "PRODUCT_GROUP_ID")
    private Long productGroupId;

    @Column(name = "PRODUCT_GROUP_NAME")
    private String productGroupName;

    @Column(name = "PRODUCT_FAMILY_ID")
    private Long productFamilyId;

    @Column(name = "PRODUCT_FAMILY_NAME")
    private String productFamilyName;

    @Column(name = "PRODUCT_SIZE_ID")
    private Long productSizeId;

    @Column(name = "PRODUCT_SIZE_NAME")
    private String productSizeName;

    @Column(name = "PRODUCT_STATUS_ID")
    private Long productStatusId;

    @Column(name = "PRODUCT_STATUS_NAME")
    private String productStatusName;

    @Column(name = "PRODUCT_TYPE_ID")
    private Long productTypeId;

    @Column(name = "PRODUCT_TYPE_NAME")
    private String productTypeName;

    @Column(name = "PRODUCT_COLOR_ID")
    private Long productColorId;

    @Column(name = "PRODUCT_COLOR_NAME")
    private String productColorName;

    @Column(name = "THRESHOLD")
    private Long threshold;

    @Column(name = "PRODUCT_DEPARTMENT_ID")
    private Long productDepartmentId;

    @Column(name = "PRODUCT_DEPARTMENT_NAME")
    private String productDepartmentName;

    @Column(name = "PRODUCT_UNIT_ID")
    private Long productUnitId;

    @Column(name = "PRODUCT_UNIT_NAME")
    private String productUnitName;

    @Column(name = "CAN_BE_DELETED")
    private String canBeDeleted;

    @Override
    public String toString() {
        return designation;
    }

    public VSmProduct() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Long getQuantity() {
        if (quantity == null || quantity == 0) {
            return 0L;
        } else {
            return quantity;
        }
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getPriceSale() {
        return priceSale;
    }

    public void setPriceSale(Double priceSale) {
        this.priceSale = priceSale;
    }

    public Double getPriceBuy() {
        return priceBuy;
    }

    public void setPriceBuy(Double priceBuy) {
        this.priceBuy = priceBuy;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public Long getProductGroupId() {
        return productGroupId;
    }

    public void setProductGroupId(Long productGroupId) {
        this.productGroupId = productGroupId;
    }

    public String getProductGroupName() {
        return productGroupName;
    }

    public void setProductGroupName(String productGroupName) {
        this.productGroupName = productGroupName;
    }

    public Long getProductFamilyId() {
        return productFamilyId;
    }

    public void setProductFamilyId(Long productFamilyId) {
        this.productFamilyId = productFamilyId;
    }

    public String getProductFamilyName() {
        return productFamilyName;
    }

    public void setProductFamilyName(String productFamilyName) {
        this.productFamilyName = productFamilyName;
    }

    public Long getProductSizeId() {
        return productSizeId;
    }

    public void setProductSizeId(Long productSizeId) {
        this.productSizeId = productSizeId;
    }

    public String getProductSizeName() {
        return productSizeName;
    }

    public void setProductSizeName(String productSizeName) {
        this.productSizeName = productSizeName;
    }

    public Long getProductStatusId() {
        return productStatusId;
    }

    public void setProductStatusId(Long productStatusId) {
        this.productStatusId = productStatusId;
    }

    public String getProductStatusName() {
        return productStatusName;
    }

    public void setProductStatusName(String productStatusName) {
        this.productStatusName = productStatusName;
    }

    public Long getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Long productTypeId) {
        this.productTypeId = productTypeId;
    }

    public String getProductTypeName() {
        return productTypeName;
    }

    public void setProductTypeName(String productTypeName) {
        this.productTypeName = productTypeName;
    }

    public Long getProductColorId() {
        return productColorId;
    }

    public void setProductColorId(Long productColorId) {
        this.productColorId = productColorId;
    }

    public String getProductColorName() {
        return productColorName;
    }

    public void setProductColorName(String productColorName) {
        this.productColorName = productColorName;
    }

    public Long getNoSeq() {
        return noSeq;
    }

    public void setNoSeq(Long noSeq) {
        this.noSeq = noSeq;
    }

    public Long getThreshold() {
        return threshold;
    }

    public void setThreshold(Long threshold) {
        this.threshold = threshold;
    }

    public Long getProductDepartmentId() {
        return productDepartmentId;
    }

    public void setProductDepartmentId(Long productDepartmentId) {
        this.productDepartmentId = productDepartmentId;
    }

    public String getProductDepartmentName() {
        return productDepartmentName;
    }

    public void setProductDepartmentName(String productDepartmentName) {
        this.productDepartmentName = productDepartmentName;
    }

    public Long getProductUnitId() {
        return productUnitId;
    }

    public void setProductUnitId(Long productUnitId) {
        this.productUnitId = productUnitId;
    }

    public String getProductUnitName() {
        return productUnitName;
    }

    public void setProductUnitName(String productUnitName) {
        this.productUnitName = productUnitName;
    }

    public String getCanBeDeleted() {
        return canBeDeleted;
    }

    public void setCanBeDeleted(String canBeDeleted) {
        this.canBeDeleted = canBeDeleted;
    }

}
