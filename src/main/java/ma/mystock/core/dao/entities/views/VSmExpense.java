/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_SM_EXPENSE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmExpense.findAll", query = "SELECT v FROM VSmExpense v")
    ,
    @NamedQuery(name = "VSmExpense.findById", query = "SELECT v FROM VSmExpense v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VSmExpense.findByName", query = "SELECT v FROM VSmExpense v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VSmExpense.findByAmount", query = "SELECT v FROM VSmExpense v WHERE v.amount = :amount")
    ,
    @NamedQuery(name = "VSmExpense.findByExpenseTypeId", query = "SELECT v FROM VSmExpense v WHERE v.expenseTypeId = :expenseTypeId")
    ,
    @NamedQuery(name = "VSmExpense.findByCltModuleId", query = "SELECT v FROM VSmExpense v WHERE v.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "VSmExpense.findByDateCreation", query = "SELECT v FROM VSmExpense v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VSmExpense.findByUserCreation", query = "SELECT v FROM VSmExpense v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VSmExpense.findByDateUpdate", query = "SELECT v FROM VSmExpense v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VSmExpense.findByUserUpdate", query = "SELECT v FROM VSmExpense v WHERE v.userUpdate = :userUpdate")
    ,
    @NamedQuery(name = "VSmExpense.findByCltModuleName", query = "SELECT v FROM VSmExpense v WHERE v.cltModuleName = :cltModuleName")})
public class VSmExpense implements Serializable {

// Requête SQL : 
    public static String findAll = "SELECT v FROM VSmExpense v";
    public static String findByCltModule = "SELECT v FROM VSmExpense v WHERE v.cltModuleId = :cltModuleId";
    public static String findById = "SELECT v FROM VSmExpense v WHERE v.id = :id";

// Attributs : 
    private static final Long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @Id
    private Long id;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "DESCRIPTION")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AMOUNT")
    private double amount;
    @Column(name = "EXPENSE_TYPE_ID")
    private Long expenseTypeId;
    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;
    @Size(max = 255)
    @Column(name = "CLT_MODULE_NAME")
    private String cltModuleName;
    @Size(max = 255)

    @Column(name = "EXPENSE_TYPE_NAME")
    private String expenseTypeName;

    // Constructor :
    public VSmExpense() {
    }

    // Getters and Setters :
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Long getExpenseTypeId() {
        return expenseTypeId;
    }

    public void setExpenseTypeId(Long expenseTypeId) {
        this.expenseTypeId = expenseTypeId;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getCltModuleName() {
        return cltModuleName;
    }

    public void setCltModuleName(String cltModuleName) {
        this.cltModuleName = cltModuleName;
    }

    public String getExpenseTypeName() {
        return expenseTypeName;
    }

    public void setExpenseTypeName(String expenseTypeName) {
        this.expenseTypeName = expenseTypeName;
    }

}
