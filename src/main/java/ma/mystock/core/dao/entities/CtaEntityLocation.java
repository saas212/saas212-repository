/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "CTA_ENTITY_LOCATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CtaEntityLocation.findAll", query = "SELECT c FROM CtaEntityLocation c")
    ,
    @NamedQuery(name = "CtaEntityLocation.findById", query = "SELECT c FROM CtaEntityLocation c WHERE c.id = :id")
    ,
    @NamedQuery(name = "CtaEntityLocation.findByAddressLine1", query = "SELECT c FROM CtaEntityLocation c WHERE c.addressLine1 = :addressLine1")
    ,
    @NamedQuery(name = "CtaEntityLocation.findByAddressLine2", query = "SELECT c FROM CtaEntityLocation c WHERE c.addressLine2 = :addressLine2")
    ,
    @NamedQuery(name = "CtaEntityLocation.findByAddressLine3", query = "SELECT c FROM CtaEntityLocation c WHERE c.addressLine3 = :addressLine3")
    ,
    @NamedQuery(name = "CtaEntityLocation.findByInfCountryId", query = "SELECT c FROM CtaEntityLocation c WHERE c.infCountryId = :infCountryId")
    ,
    @NamedQuery(name = "CtaEntityLocation.findByInfCityId", query = "SELECT c FROM CtaEntityLocation c WHERE c.infCityId = :infCityId")
    ,
    @NamedQuery(name = "CtaEntityLocation.findByPostalCode", query = "SELECT c FROM CtaEntityLocation c WHERE c.postalCode = :postalCode")
    ,
    @NamedQuery(name = "CtaEntityLocation.findByLocationTypeId", query = "SELECT c FROM CtaEntityLocation c WHERE c.locationTypeId = :locationTypeId")
    ,
    @NamedQuery(name = "CtaEntityLocation.findByDateCreation", query = "SELECT c FROM CtaEntityLocation c WHERE c.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "CtaEntityLocation.findByUserCreation", query = "SELECT c FROM CtaEntityLocation c WHERE c.userCreation = :userCreation")
    ,
    @NamedQuery(name = "CtaEntityLocation.findByDateUpdate", query = "SELECT c FROM CtaEntityLocation c WHERE c.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "CtaEntityLocation.findByUserUpdate", query = "SELECT c FROM CtaEntityLocation c WHERE c.userUpdate = :userUpdate")})
public class CtaEntityLocation implements Serializable {

    public static final String findById = "SELECT c FROM CtaEntityLocation c WHERE c.id = :id";

    private static final Long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;

    @Column(name = "ADDRESS_LINE_1")
    private String addressLine1;

    @Column(name = "ADDRESS_LINE_2")
    private String addressLine2;

    @Column(name = "ADDRESS_LINE_3")
    private String addressLine3;

    @Column(name = "INF_COUNTRY_ID")
    private Long infCountryId;

    @Column(name = "INF_CITY_ID")
    private Long infCityId;

    @Column(name = "POSTAL_CODE")
    private String postalCode;

    @Column(name = "PRIORITY")
    private Long priority;

    @Column(name = "LOCATION_TYPE_ID")
    private Long locationTypeId;

    @Column(name = "ENTITY_ID")
    private Long entityId;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public CtaEntityLocation() {
    }

    public CtaEntityLocation(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3() {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    public Long getInfCountryId() {
        return infCountryId;
    }

    public void setInfCountryId(Long infCountryId) {
        this.infCountryId = infCountryId;
    }

    public Long getInfCityId() {
        return infCityId;
    }

    public void setInfCityId(Long infCityId) {
        this.infCityId = infCityId;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Long getLocationTypeId() {
        return locationTypeId;
    }

    public void setLocationTypeId(Long locationTypeId) {
        this.locationTypeId = locationTypeId;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CtaEntityLocation)) {
            return false;
        }
        CtaEntityLocation other = (CtaEntityLocation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.dao.entities.CtaEntityLocation[ id=" + id + " ]";
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

}
