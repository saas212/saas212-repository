/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * Abdou
 */
@Entity
@Table(name = "V_CLT_PARAMETER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCltParameter.findAll", query = "SELECT v FROM VCltParameter v")
    ,
    @NamedQuery(name = "VCltParameter.findById", query = "SELECT v FROM VCltParameter v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VCltParameter.findByName", query = "SELECT v FROM VCltParameter v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VCltParameter.findByDescription", query = "SELECT v FROM VCltParameter v WHERE v.description = :description")
    ,
    @NamedQuery(name = "VCltParameter.findByParameterTypeId", query = "SELECT v FROM VCltParameter v WHERE v.parameterTypeId = :parameterTypeId")
    ,
    @NamedQuery(name = "VCltParameter.findByCltParameterTypeName", query = "SELECT v FROM VCltParameter v WHERE v.cltParameterTypeName = :cltParameterTypeName")
    ,
    @NamedQuery(name = "VCltParameter.findByActive", query = "SELECT v FROM VCltParameter v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VCltParameter.findByCanBeDelete", query = "SELECT v FROM VCltParameter v WHERE v.canBeDelete = :canBeDelete")})
public class VCltParameter implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String findById = "SELECT v FROM VCltParameter v WHERE v.id = :id";
    public static final String findAll = "SELECT v FROM VCltParameter v";
    public static final String findAllActive = "SELECT v FROM VCltParameter v WHERE v.active = :active";

    public static final String findAllActiveAndId = "SELECT v FROM VCltParameter v WHERE v.active = :active and ((v.id = :id) or v.id not in (select cv.parameterId from VCltParameterClient cv where  cv.clinetId = :clinetId ))";
    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Size(max = 255)
    @Column(name = "DESCRIPTION")
    private String description;
    @Lob
    @Size(max = 65535)
    @Column(name = "DEFAULT_VALUE")
    private String defaultValue;
    @Column(name = "PARAMETER_TYPE_ID")
    private Long parameterTypeId;
    @Size(max = 255)
    @Column(name = "CLT_PARAMETER_TYPE_NAME")
    private String cltParameterTypeName;
    @Column(name = "ACTIVE")
    private String active;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "CAN_BE_DELETE")
    private boolean canBeDelete;

    public VCltParameter() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public Long getParameterTypeId() {
        return parameterTypeId;
    }

    public void setParameterTypeId(Long parameterTypeId) {
        this.parameterTypeId = parameterTypeId;
    }

    public String getCltParameterTypeName() {
        return cltParameterTypeName;
    }

    public void setCltParameterTypeName(String cltParameterTypeName) {
        this.cltParameterTypeName = cltParameterTypeName;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public boolean isCanBeDelete() {
        return canBeDelete;
    }

    public void setCanBeDelete(boolean canBeDelete) {
        this.canBeDelete = canBeDelete;
    }

}
