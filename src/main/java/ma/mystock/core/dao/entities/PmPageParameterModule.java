/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Yamine Abdelmounaim <yaminea@evision.ca>
 */
@Entity
@Table(name = "PM_PAGE_PARAMETER_MODULE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PmPageParameterModule.findAll", query = "SELECT p FROM PmPageParameterModule p")
    ,
    @NamedQuery(name = "PmPageParameterModule.findById", query = "SELECT p FROM PmPageParameterModule p WHERE p.id = :id")
    ,
    @NamedQuery(name = "PmPageParameterModule.findByPageParameterId", query = "SELECT p FROM PmPageParameterModule p WHERE p.pageParameterId = :pageParameterId")
    ,
    @NamedQuery(name = "PmPageParameterModule.findByCltModuleId", query = "SELECT p FROM PmPageParameterModule p WHERE p.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "PmPageParameterModule.findByActive", query = "SELECT p FROM PmPageParameterModule p WHERE p.active = :active")
    ,
    @NamedQuery(name = "PmPageParameterModule.findByDateCreation", query = "SELECT p FROM PmPageParameterModule p WHERE p.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "PmPageParameterModule.findByUserCreation", query = "SELECT p FROM PmPageParameterModule p WHERE p.userCreation = :userCreation")
    ,
    @NamedQuery(name = "PmPageParameterModule.findByDateUpdate", query = "SELECT p FROM PmPageParameterModule p WHERE p.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "PmPageParameterModule.findByUserUpdate", query = "SELECT p FROM PmPageParameterModule p WHERE p.userUpdate = :userUpdate")})
public class PmPageParameterModule implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String findById = "SELECT p FROM PmPageParameterModule p WHERE p.id = :id";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "PAGE_PARAMETER_ID")
    private Long pageParameterId;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "VALUE")
    private String value;
    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public PmPageParameterModule() {
    }

    public PmPageParameterModule(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPageParameterId() {
        return pageParameterId;
    }

    public void setPageParameterId(Long pageParameterId) {
        this.pageParameterId = pageParameterId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PmPageParameterModule)) {
            return false;
        }
        PmPageParameterModule other = (PmPageParameterModule) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entitys.PmPageParameterModule[ id=" + id + " ]";
    }

}
