/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_CLT_CLIENT_LANGUAGE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCltClientLanguage.findAll", query = "SELECT v FROM VCltClientLanguage v")
    ,
    @NamedQuery(name = "VCltClientLanguage.findByCode", query = "SELECT v FROM VCltClientLanguage v WHERE v.code = :code")
    ,
    @NamedQuery(name = "VCltClientLanguage.findByClientId", query = "SELECT v FROM VCltClientLanguage v WHERE v.clientId = :clientId")
    ,
    @NamedQuery(name = "VCltClientLanguage.findByInfLanguageId", query = "SELECT v FROM VCltClientLanguage v WHERE v.infLanguageId = :infLanguageId")
    ,
    @NamedQuery(name = "VCltClientLanguage.findByInfLanguageCode", query = "SELECT v FROM VCltClientLanguage v WHERE v.infLanguageCode = :infLanguageCode")
    ,
    @NamedQuery(name = "VCltClientLanguage.findByInfPrefixId", query = "SELECT v FROM VCltClientLanguage v WHERE v.infPrefixId = :infPrefixId")
    ,
    @NamedQuery(name = "VCltClientLanguage.findByInfPrefixCode", query = "SELECT v FROM VCltClientLanguage v WHERE v.infPrefixCode = :infPrefixCode")})
public class VCltClientLanguage implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static String findByClientIdAndInfLanguageId = "SELECT v FROM VCltClientLanguage v WHERE v.clientId = :clientId and v.infLanguageId = :infLanguageId";

    @Size(max = 41)
    @Id
    @Column(name = "CODE")
    private String code;
    @Column(name = "CLIENT_ID")
    private Long clientId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "INF_LANGUAGE_ID")
    private Long infLanguageId;
    @Size(max = 255)
    @Column(name = "INF_LANGUAGE_CODE")
    private String infLanguageCode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "INF_PREFIX_ID")
    private Long infPrefixId;
    @Size(max = 255)
    @Column(name = "INF_PREFIX_CODE")
    private String infPrefixCode;

    public VCltClientLanguage() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getInfLanguageId() {
        return infLanguageId;
    }

    public void setInfLanguageId(Long infLanguageId) {
        this.infLanguageId = infLanguageId;
    }

    public String getInfLanguageCode() {
        return infLanguageCode;
    }

    public void setInfLanguageCode(String infLanguageCode) {
        this.infLanguageCode = infLanguageCode;
    }

    public Long getInfPrefixId() {
        return infPrefixId;
    }

    public void setInfPrefixId(Long infPrefixId) {
        this.infPrefixId = infPrefixId;
    }

    public String getInfPrefixCode() {
        return infPrefixCode;
    }

    public void setInfPrefixCode(String infPrefixCode) {
        this.infPrefixCode = infPrefixCode;
    }

}
