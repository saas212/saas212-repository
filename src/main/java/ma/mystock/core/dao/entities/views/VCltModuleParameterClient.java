/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @Important cette les querys + fileds + tous à vérifier
 */
@Entity
@Table(name = "V_CLT_MODULE_PARAMETER_CLIENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCltModuleParameterClient.findAll", query = "SELECT v FROM VCltModuleParameterClient v")
    ,
    @NamedQuery(name = "VCltModuleParameterClient.findByModuleParameterName", query = "SELECT v FROM VCltModuleParameterClient v WHERE v.moduleParameterName = :moduleParameterName")
    ,
    @NamedQuery(name = "VCltModuleParameterClient.findByModuleParameterTypeName", query = "SELECT v FROM VCltModuleParameterClient v WHERE v.moduleParameterTypeName = :moduleParameterTypeName")
    ,
    @NamedQuery(name = "VCltModuleParameterClient.findByActive", query = "SELECT v FROM VCltModuleParameterClient v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VCltModuleParameterClient.findByModuleId", query = "SELECT v FROM VCltModuleParameterClient v WHERE v.moduleId = :moduleId")})
public class VCltModuleParameterClient implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String findById = "SELECT v FROM VCltModuleParameterClient v WHERE v.id = :id";
    public static final String findAllActive = "SELECT v FROM VCltModuleParameterClient v WHERE v.active = :active";
    public static final String findByClientAndActive = "SELECT v FROM VCltModuleParameterClient v where v.clientId = :clientId and v.active = :active";
    public static final String findByModuleId = "SELECT v FROM VCltModuleParameterClient v WHERE v.moduleId = :moduleId  and v.active = :active";
    /*
     @Basic(optional = false)
     @NotNull
     @Id
     @Column(name = "ID")
     private Long id;
     */

    @Id
    @Column(name = "CODE")
    private String code;

    @Column(name = "MODULE_PARAMETER_NAME")
    private String moduleParameterName;

    @Column(name = "MODULE_PARAMETER_TYPE_NAME")
    private String moduleParameterTypeName;
    /*
     @Lob
     @Size(max = 65535)
     @Column(name = "DEFAULT_VALUE")
     private String defaultValue;
     @Lob
     @Size(max = 2147483647)
     */
    @Column(name = "VALUE")
    private String value;
    //@Column(name = "CLIENT_ID")
    //private Long clientId;
    @Column(name = "MODULE_ID")
    private Long moduleId;

    @Column(name = "MODULE_PARAMETER_ID")
    private Long moduleParameterId;

    @Column(name = "ACTIVE")
    private String active;

    //@Column(name = "can_be_delete")
    //private String canBeDelete;
    public VCltModuleParameterClient() {
    }

    /*
     public Long getId() {
     return id;
     }

     public void setId(Long id) {
     this.id = id;
     }
     */
    public Long getModuleParameterId() {
        return moduleParameterId;
    }

    public void setModuleParameterId(Long moduleParameterId) {
        this.moduleParameterId = moduleParameterId;
    }

    public String getModuleParameterName() {
        return moduleParameterName;
    }

    public void setModuleParameterName(String moduleParameterName) {
        this.moduleParameterName = moduleParameterName;
    }

    public String getModuleParameterTypeName() {
        return moduleParameterTypeName;
    }

    public void setModuleParameterTypeName(String moduleParameterTypeName) {
        this.moduleParameterTypeName = moduleParameterTypeName;
    }

    /*
     public String getDefaultValue() {
     return defaultValue;
     }

     public void setDefaultValue(String defaultValue) {
     this.defaultValue = defaultValue;
     }
     */
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    /*
     public Long getClientId() {
     return clientId;
     }

     public void setClientId(Long clientId) {
     this.clientId = clientId;
     }

     public Long getCltModuleParameterId() {
     return cltModuleParameterId;
     }

     public void setCltModuleParameterId(Long cltModuleParameterId) {
     this.cltModuleParameterId = cltModuleParameterId;
     }
     */
    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    /*
     public String getCanBeDelete() {
     return canBeDelete;
     }

     public void setCanBeDelete(String canBeDelete) {
     this.canBeDelete = canBeDelete;
     }
     */
    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
