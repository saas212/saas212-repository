/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdessamad HALLAL
 */
@Entity
@Table(name = "V_SM_CUSTOMER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmCustomer.findAll", query = "SELECT v FROM VSmCustomer v")
    ,
    @NamedQuery(name = "VSmCustomer.findById", query = "SELECT v FROM VSmCustomer v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VSmCustomer.findByFirstName", query = "SELECT v FROM VSmCustomer v WHERE v.firstName = :firstName")
    ,
    @NamedQuery(name = "VSmCustomer.findByLastName", query = "SELECT v FROM VSmCustomer v WHERE v.lastName = :lastName")
    ,
    @NamedQuery(name = "VSmCustomer.findByCltModuleId", query = "SELECT v FROM VSmCustomer v WHERE v.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "VSmCustomer.findByCustomerTypeId", query = "SELECT v FROM VSmCustomer v WHERE v.customerTypeId = :customerTypeId")
    ,
    @NamedQuery(name = "VSmCustomer.findByCustomerCategoryId", query = "SELECT v FROM VSmCustomer v WHERE v.customerCategoryId = :customerCategoryId")
    ,
    @NamedQuery(name = "VSmCustomer.findBySortKey", query = "SELECT v FROM VSmCustomer v WHERE v.sortKey = :sortKey")
    ,
    @NamedQuery(name = "VSmCustomer.findByCompanyName", query = "SELECT v FROM VSmCustomer v WHERE v.companyName = :companyName")
    ,
    @NamedQuery(name = "VSmCustomer.findByActive", query = "SELECT v FROM VSmCustomer v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VSmCustomer.findByShortLabel", query = "SELECT v FROM VSmCustomer v WHERE v.shortLabel = :shortLabel")
    ,
    @NamedQuery(name = "VSmCustomer.findByDateCreation", query = "SELECT v FROM VSmCustomer v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VSmCustomer.findByUserCreation", query = "SELECT v FROM VSmCustomer v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VSmCustomer.findByIdentification", query = "SELECT v FROM VSmCustomer v WHERE v.identification = :identification")
    ,
    @NamedQuery(name = "VSmCustomer.findByDateUpdate", query = "SELECT v FROM VSmCustomer v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VSmCustomer.findByUserUpdate", query = "SELECT v FROM VSmCustomer v WHERE v.userUpdate = :userUpdate")
    ,
    @NamedQuery(name = "VSmCustomer.findByCustomerTypeName", query = "SELECT v FROM VSmCustomer v WHERE v.customerTypeName = :customerTypeName")
    ,
    @NamedQuery(name = "VSmCustomer.findByCustomerCategoryName", query = "SELECT v FROM VSmCustomer v WHERE v.customerCategoryName = :customerCategoryName")})
public class VSmCustomer implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findAll = "SELECT v FROM VSmCustomer v";
    public static final String findById = "SELECT v FROM VSmCustomer v WHERE v.id = :id";
    public static final String findByCltModuleId = "SELECT v FROM VSmCustomer v WHERE v.cltModuleId = :cltModuleId";
    public static final String findByCltModuleIdAndActive = "SELECT v FROM VSmCustomer v WHERE v.cltModuleId = :cltModuleId and v.active = :active";

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    @Column(name = "CUSTOMER_TYPE_ID")
    private Long customerTypeId;

    @Column(name = "CUSTOMER_CATEGORY_ID")
    private Long customerCategoryId;

    @Column(name = "SORT_KEY")
    private Long sortKey;

    @Column(name = "COMPANY_NAME")
    private String companyName;

    @Column(name = "ACTIVE")
    private String active;

    @Column(name = "SHORT_LABEL")
    private String shortLabel;

    @Column(name = "FULL_LABEL")
    private String fullLabel;

    @Column(name = "NOTE")
    private String note;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "IDENTIFICATION")
    private String identification;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "CUSTOMER_TYPE_NAME")
    private String customerTypeName;

    @Column(name = "CUSTOMER_CATEGORY_NAME")
    private String customerCategoryName;

    @Column(name = "CAN_BE_BELETE")
    private String canBeDeleted;

    @Column(name = "REPRESENTATIVE")
    private String representative;

    @Column(name = "ENTITY_PHONE")
    private String entityPhone;

    @Column(name = "ENTITY_EMAIL")
    private String entityEmail;

    @Column(name = "ENTITY_FAX")
    private String entityFax;

    @Column(name = "ENTITY_LOCATION_ADDRESS")
    private String entityLocationAddress;

    @Column(name = "ENTITY_LOCATION_CITY_NAME")
    private String entityLocationCityName;

    @Column(name = "ENTITY_LOCATION_COUNTRY_NAME")
    private String entityLocationCountryName;

    @Column(name = "ENTITY_WEB")
    private String entityWeb;

    @Column(name = "ENTITY_ID")
    private Long entityId;

    @Column(name = "CUSTOMER_NATURE_ID")
    private Long customerNatureId;

    @Transient
    private String isSociety;

    @Transient
    private String isParticular;

    public String getIsSociety() {

        if ("1".equalsIgnoreCase(customerNatureId + "")) {
            return "Y";
        } else {
            return "N";
        }

    }

    public String getIsParticular() {
        if ("2".equalsIgnoreCase(customerNatureId + "")) {
            return "Y";
        } else {
            return "N";
        }
    }

    public VSmCustomer() {
    }

    public String getRepresentative() {
        return representative;
    }

    public void setRepresentative(String representative) {
        this.representative = representative;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public Long getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(Long customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public Long getCustomerCategoryId() {
        return customerCategoryId;
    }

    public void setCustomerCategoryId(Long customerCategoryId) {
        this.customerCategoryId = customerCategoryId;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getShortLabel() {
        return shortLabel;
    }

    public void setShortLabel(String shortLabel) {
        this.shortLabel = shortLabel;
    }

    public String getFullLabel() {
        return fullLabel;
    }

    public void setFullLabel(String fullLabel) {
        this.fullLabel = fullLabel;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getCustomerTypeName() {
        return customerTypeName;
    }

    public void setCustomerTypeName(String customerTypeName) {
        this.customerTypeName = customerTypeName;
    }

    public String getCustomerCategoryName() {
        return customerCategoryName;
    }

    public void setCustomerCategoryName(String customerCategoryName) {
        this.customerCategoryName = customerCategoryName;
    }

    public String getCanBeDeleted() {
        return canBeDeleted;
    }

    public void setCanBeDeleted(String canBeDeleted) {
        this.canBeDeleted = canBeDeleted;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getEntityPhone() {
        return entityPhone;
    }

    public void setEntityPhone(String entityPhone) {
        this.entityPhone = entityPhone;
    }

    public String getEntityEmail() {
        return entityEmail;
    }

    public void setEntityEmail(String entityEmail) {
        this.entityEmail = entityEmail;
    }

    public String getEntityFax() {
        return entityFax;
    }

    public void setEntityFax(String entityFax) {
        this.entityFax = entityFax;
    }

    public String getEntityWeb() {
        return entityWeb;
    }

    public void setEntityWeb(String entityWeb) {
        this.entityWeb = entityWeb;
    }

    public String getEntityLocationAddress() {
        return entityLocationAddress;
    }

    public void setEntityLocationAddress(String entityLocationAddress) {
        this.entityLocationAddress = entityLocationAddress;
    }

    public String getEntityLocationCityName() {
        return entityLocationCityName;
    }

    public void setEntityLocationCityName(String entityLocationCityName) {
        this.entityLocationCityName = entityLocationCityName;
    }

    public String getEntityLocationCountryName() {
        return entityLocationCountryName;
    }

    public void setEntityLocationCountryName(String entityLocationCountryName) {
        this.entityLocationCountryName = entityLocationCountryName;
    }

    public Long getCustomerNatureId() {
        return customerNatureId;
    }

    public void setCustomerNatureId(Long customerNatureId) {
        this.customerNatureId = customerNatureId;
    }

}
