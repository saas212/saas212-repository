package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_INF_LOVS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VInfLovs.findAll", query = "SELECT v FROM VInfLovs v")
    ,
    @NamedQuery(name = "VInfLovs.findById", query = "SELECT v FROM VInfLovs v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VInfLovs.findByName", query = "SELECT v FROM VInfLovs v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VInfLovs.findByTablePrefix", query = "SELECT v FROM VInfLovs v WHERE v.tablePrefix = :tablePrefix")
    ,
    @NamedQuery(name = "VInfLovs.findByTable", query = "SELECT v FROM VInfLovs v WHERE v.table = :table")
    ,
    @NamedQuery(name = "VInfLovs.findByView", query = "SELECT v FROM VInfLovs v WHERE v.view = :view")
    ,
    @NamedQuery(name = "VInfLovs.findByItemCode", query = "SELECT v FROM VInfLovs v WHERE v.itemCode = :itemCode")
    ,
    @NamedQuery(name = "VInfLovs.findBySortKey", query = "SELECT v FROM VInfLovs v WHERE v.sortKey = :sortKey")
    ,
    @NamedQuery(name = "VInfLovs.findByActive", query = "SELECT v FROM VInfLovs v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VInfLovs.findByDateCreation", query = "SELECT v FROM VInfLovs v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VInfLovs.findByUserCreation", query = "SELECT v FROM VInfLovs v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VInfLovs.findByDateUpdate", query = "SELECT v FROM VInfLovs v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VInfLovs.findByUserUpdate", query = "SELECT v FROM VInfLovs v WHERE v.userUpdate = :userUpdate")})

public class VInfLovs implements Serializable {

    public static String findAll = "SELECT v FROM VInfLovs v";
    public static String findByCltModuleId = "SELECT v FROM VInfLovs v where v.cltModuleId = :cltModuleId";
    public static String findById = "SELECT v FROM VInfLovs v WHERE v.id = :id";

    private static final Long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @Id
    private Long id;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "DESCRIPTION")
    private String description;
    @Size(max = 255)
    @Column(name = "TABLE_PREFIX")
    private String tablePrefix;
    @Size(max = 255)
    @Column(name = "TABLE")
    private String table;
    @Size(max = 255)
    @Column(name = "VIEW")
    private String view;
    @Size(max = 255)
    @Column(name = "ITEM_CODE")
    private String itemCode;
    @Column(name = "SORT_KEY")
    private Long sortKey;
    @Size(max = 20)
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    @Column(name = "ENTITY")
    private String entity;

    public VInfLovs() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTablePrefix() {
        return tablePrefix;
    }

    public void setTablePrefix(String tablePrefix) {
        this.tablePrefix = tablePrefix;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

}
