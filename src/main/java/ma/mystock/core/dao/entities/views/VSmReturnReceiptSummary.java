/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_SM_RETURN_RECEIPT_SUMMARY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmReturnReceiptSummary.findAll", query = "SELECT v FROM VSmReturnReceiptSummary v")
    ,
    @NamedQuery(name = "VSmReturnReceiptSummary.findByReturnReceiptId", query = "SELECT v FROM VSmReturnReceiptSummary v WHERE v.returnReceiptId = :returnReceiptId")
    ,
    @NamedQuery(name = "VSmReturnReceiptSummary.findBySummaryTotalQuantity", query = "SELECT v FROM VSmReturnReceiptSummary v WHERE v.summaryTotalQuantity = :summaryTotalQuantity")
    ,
    @NamedQuery(name = "VSmReturnReceiptSummary.findBySummaryTotalHt", query = "SELECT v FROM VSmReturnReceiptSummary v WHERE v.summaryTotalHt = :summaryTotalHt")
    ,
    @NamedQuery(name = "VSmReturnReceiptSummary.findBySummaryTvaAmount", query = "SELECT v FROM VSmReturnReceiptSummary v WHERE v.summaryTvaAmount = :summaryTvaAmount")
    ,
    @NamedQuery(name = "VSmReturnReceiptSummary.findBySummaryTotalTtc", query = "SELECT v FROM VSmReturnReceiptSummary v WHERE v.summaryTotalTtc = :summaryTotalTtc")})
public class VSmReturnReceiptSummary implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findByReturnReceiptId = "SELECT v FROM VSmReturnReceiptSummary v WHERE v.returnReceiptId = :returnReceiptId";

    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "RETURN_RECEIPT_ID")
    private Long returnReceiptId;
    @Column(name = "SUMMARY_TOTAL_QUANTITY")
    private Long summaryTotalQuantity;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "SUMMARY_TOTAL_HT")
    private Double summaryTotalHt;
    @Column(name = "SUMMARY_TVA_AMOUNT")
    private Double summaryTvaAmount;
    @Column(name = "SUMMARY_TOTAL_TTC")
    private Double summaryTotalTtc;

    public VSmReturnReceiptSummary() {
    }

    public Long getReturnReceiptId() {
        return returnReceiptId;
    }

    public void setReturnReceiptId(Long returnReceiptId) {
        this.returnReceiptId = returnReceiptId;
    }

    public Long getSummaryTotalQuantity() {
        return summaryTotalQuantity;
    }

    public void setSummaryTotalQuantity(Long summaryTotalQuantity) {
        this.summaryTotalQuantity = summaryTotalQuantity;
    }

    public Double getSummaryTotalHt() {
        return summaryTotalHt;
    }

    public void setSummaryTotalHt(Double summaryTotalHt) {
        this.summaryTotalHt = summaryTotalHt;
    }

    public Double getSummaryTvaAmount() {
        return summaryTvaAmount;
    }

    public void setSummaryTvaAmount(Double summaryTvaAmount) {
        this.summaryTvaAmount = summaryTvaAmount;
    }

    public Double getSummaryTotalTtc() {
        return summaryTotalTtc;
    }

    public void setSummaryTotalTtc(Double summaryTotalTtc) {
        this.summaryTotalTtc = summaryTotalTtc;
    }

}
