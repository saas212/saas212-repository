/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_CLT_PARAMETERS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCltParameters.findAll", query = "SELECT v FROM VCltParameters v")
    ,
    @NamedQuery(name = "VCltParameters.findByCode", query = "SELECT v FROM VCltParameters v WHERE v.code = :code")
    ,
    @NamedQuery(name = "VCltParameters.findById", query = "SELECT v FROM VCltParameters v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VCltParameters.findByParameterTypeId", query = "SELECT v FROM VCltParameters v WHERE v.parameterTypeId = :parameterTypeId")
    ,
    @NamedQuery(name = "VCltParameters.findByClinetId", query = "SELECT v FROM VCltParameters v WHERE v.clinetId = :clinetId")})
public class VCltParameters implements Serializable {

    public static String findAll = "SELECT v FROM VCltParameters v";
    private static final Long serialVersionUID = 1L;
    @Id
    @Size(max = 41)
    @Column(name = "CODE")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Column(name = "PARAMETER_TYPE_ID")
    private Long parameterTypeId;
    @Column(name = "CLINET_ID")
    private Long clinetId;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "VAL")
    private String val;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "VALUE")
    private String value;
    @Lob
    @Size(max = 65535)
    @Column(name = "DEFAULT_VALUE")
    private String defaultValue;

    public VCltParameters() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParameterTypeId() {
        return parameterTypeId;
    }

    public void setParameterTypeId(Long parameterTypeId) {
        this.parameterTypeId = parameterTypeId;
    }

    public Long getClinetId() {
        return clinetId;
    }

    public void setClinetId(Long clinetId) {
        this.clinetId = clinetId;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

}
