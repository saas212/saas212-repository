package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_SM_PRODUCT_ALERT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmProductAlert.findAll", query = "SELECT v FROM VSmProductAlert v")
    ,
    @NamedQuery(name = "VSmProductAlert.findByCltModuleId", query = "SELECT v FROM VSmProductAlert v WHERE v.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "VSmProductAlert.findByReference", query = "SELECT v FROM VSmProductAlert v WHERE v.reference = :reference")
    ,
    @NamedQuery(name = "VSmProductAlert.findById", query = "SELECT v FROM VSmProductAlert v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VSmProductAlert.findByDesignation", query = "SELECT v FROM VSmProductAlert v WHERE v.designation = :designation")
    ,
    @NamedQuery(name = "VSmProductAlert.findByQuantity", query = "SELECT v FROM VSmProductAlert v WHERE v.quantity = :quantity")
    ,
    @NamedQuery(name = "VSmProductAlert.findByThreshold", query = "SELECT v FROM VSmProductAlert v WHERE v.threshold = :threshold")
    ,
    @NamedQuery(name = "VSmProductAlert.findByTotalCommanded", query = "SELECT v FROM VSmProductAlert v WHERE v.totalCommanded = :totalCommanded")
    ,
    @NamedQuery(name = "VSmProductAlert.findByTotalRecieved", query = "SELECT v FROM VSmProductAlert v WHERE v.totalRecieved = :totalRecieved")})
public class VSmProductAlert implements Serializable {

    public final static String findByCltModuleId = "SELECT v FROM VSmProductAlert v WHERE v.cltModuleId = :cltModuleId";
    public final static String findByLikeReferenceOrDesignationAndCltModuleId
            = "SELECT v FROM VSmProductAlert v WHERE "
            + "     (:reference is null or :reference = '' or lower(v.reference) like :reference) "
            + " and (:designation is null or :designation = '' or lower(v.designation) like :designation) "
            + " and (v.cltModuleId = :cltModuleId)";

    private static final Long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    @Column(name = "REFERENCE")
    private String reference;

    @Column(name = "DESIGNATION")
    private String designation;

    @Column(name = "QUANTITY")
    private Long quantity;

    @Column(name = "THRESHOLD")
    private Long threshold;

    @Column(name = "TOTAL_COMMANDED")
    private Long totalCommanded;

    @Column(name = "TOTAL_RECIEVED")
    private Long totalRecieved;

    public VSmProductAlert() {
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getThreshold() {
        return threshold;
    }

    public void setThreshold(Long threshold) {
        this.threshold = threshold;
    }

    public Long getTotalCommanded() {
        return totalCommanded;
    }

    public void setTotalCommanded(Long totalCommanded) {
        this.totalCommanded = totalCommanded;
    }

    public Long getTotalRecieved() {
        return totalRecieved;
    }

    public void setTotalRecieved(Long totalRecieved) {
        this.totalRecieved = totalRecieved;
    }

}
