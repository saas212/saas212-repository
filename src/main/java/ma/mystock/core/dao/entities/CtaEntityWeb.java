package ma.mystock.core.dao.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "CTA_ENTITY_WEB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CtaEntityWeb.findAll", query = "SELECT c FROM CtaEntityWeb c")
    ,
    @NamedQuery(name = "CtaEntityWeb.findById", query = "SELECT c FROM CtaEntityWeb c WHERE c.id = :id")
    ,
    @NamedQuery(name = "CtaEntityWeb.findByUrl", query = "SELECT c FROM CtaEntityWeb c WHERE c.url = :url")
    ,
    @NamedQuery(name = "CtaEntityWeb.findByPriority", query = "SELECT c FROM CtaEntityWeb c WHERE c.priority = :priority")
    ,
    @NamedQuery(name = "CtaEntityWeb.findByEntityId", query = "SELECT c FROM CtaEntityWeb c WHERE c.entityId = :entityId")
    ,
    @NamedQuery(name = "CtaEntityWeb.findByWebTypeId", query = "SELECT c FROM CtaEntityWeb c WHERE c.webTypeId = :webTypeId")
    ,
    @NamedQuery(name = "CtaEntityWeb.findByDateCreation", query = "SELECT c FROM CtaEntityWeb c WHERE c.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "CtaEntityWeb.findByUserCreation", query = "SELECT c FROM CtaEntityWeb c WHERE c.userCreation = :userCreation")
    ,
    @NamedQuery(name = "CtaEntityWeb.findByDateUpdate", query = "SELECT c FROM CtaEntityWeb c WHERE c.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "CtaEntityWeb.findByUserUpdate", query = "SELECT c FROM CtaEntityWeb c WHERE c.userUpdate = :userUpdate")})
public class CtaEntityWeb implements Serializable {

    public static final String findById = "SELECT c FROM CtaEntityWeb c WHERE c.id = :id";

    private static final Long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;

    @Column(name = "URL")
    private String url;

    @Column(name = "PRIORITY")
    private Long priority;

    @Column(name = "ENTITY_ID")
    private Long entityId;

    @Column(name = "WEB_TYPE_ID")
    private Long webTypeId;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public CtaEntityWeb() {
    }

    public CtaEntityWeb(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Long getWebTypeId() {
        return webTypeId;
    }

    public void setWebTypeId(Long webTypeId) {
        this.webTypeId = webTypeId;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CtaEntityWeb)) {
            return false;
        }
        CtaEntityWeb other = (CtaEntityWeb) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entities.CtaEntityWeb[ id=" + id + " ]";
    }

}
