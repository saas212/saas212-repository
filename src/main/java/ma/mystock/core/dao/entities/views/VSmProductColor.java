/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_SM_PRODUCT_COLOR")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmProductColor.findAll", query = "SELECT v FROM VSmProductColor v")
    ,
    @NamedQuery(name = "VSmProductColor.findById", query = "SELECT v FROM VSmProductColor v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VSmProductColor.findByName", query = "SELECT v FROM VSmProductColor v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VSmProductColor.findByHex", query = "SELECT v FROM VSmProductColor v WHERE v.hex = :hex")
    ,
    @NamedQuery(name = "VSmProductColor.findByRgb", query = "SELECT v FROM VSmProductColor v WHERE v.rgb = :rgb")
    ,
    @NamedQuery(name = "VSmProductColor.findBySortKey", query = "SELECT v FROM VSmProductColor v WHERE v.sortKey = :sortKey")
    ,
    @NamedQuery(name = "VSmProductColor.findByActive", query = "SELECT v FROM VSmProductColor v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VSmProductColor.findByDateCreation", query = "SELECT v FROM VSmProductColor v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VSmProductColor.findByCltModuleId", query = "SELECT v FROM VSmProductColor v WHERE v.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "VSmProductColor.findByUserCreation", query = "SELECT v FROM VSmProductColor v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VSmProductColor.findByDateUpdate", query = "SELECT v FROM VSmProductColor v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VSmProductColor.findByUserUpdate", query = "SELECT v FROM VSmProductColor v WHERE v.userUpdate = :userUpdate")})
public class VSmProductColor implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findByCltModuleId = "SELECT v FROM VSmProductColor v WHERE v.cltModuleId = :cltModuleId";
    public static final String findByNameAndCltModuleId = "SELECT v FROM VSmProductColor v WHERE v.name = :name and v.cltModuleId = :cltModuleId";
    public static final String findById = "SELECT v FROM VSmProductColor v WHERE v.id = :id";

    public static final String findByCltModuleIdAndActive = "SELECT v FROM VSmProductColor v WHERE v.cltModuleId = :cltModuleId and v.active = :active";

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @Id
    private Long id;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Size(max = 255)
    @Column(name = "HEX")
    private String hex;
    @Size(max = 255)
    @Column(name = "RGB")
    private String rgb;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "SORT_KEY")
    private Long sortKey;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public VSmProductColor() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHex() {
        return hex;
    }

    public void setHex(String hex) {
        this.hex = hex;
    }

    public String getRgb() {
        return rgb;
    }

    public void setRgb(String rgb) {
        this.rgb = rgb;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

}
