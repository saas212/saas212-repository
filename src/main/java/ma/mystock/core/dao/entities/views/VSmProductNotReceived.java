package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_SM_PRODUCT_NOT_RECEIVED")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmProductNotReceived.findAll", query = "SELECT v FROM VSmProductNotReceived v")
    ,
    @NamedQuery(name = "VSmProductNotReceived.findByCode", query = "SELECT v FROM VSmProductNotReceived v WHERE v.code = :code")
    ,
    @NamedQuery(name = "VSmProductNotReceived.findByOrderSupplierId", query = "SELECT v FROM VSmProductNotReceived v WHERE v.orderSupplierId = :orderSupplierId")
    ,
    @NamedQuery(name = "VSmProductNotReceived.findByOrderSupplierReference", query = "SELECT v FROM VSmProductNotReceived v WHERE v.orderSupplierReference = :orderSupplierReference")
    ,
    @NamedQuery(name = "VSmProductNotReceived.findByCltModuleId", query = "SELECT v FROM VSmProductNotReceived v WHERE v.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "VSmProductNotReceived.findByProductReference", query = "SELECT v FROM VSmProductNotReceived v WHERE v.productReference = :productReference")
    ,
    @NamedQuery(name = "VSmProductNotReceived.findByProductDesignation", query = "SELECT v FROM VSmProductNotReceived v WHERE v.productDesignation = :productDesignation")
    ,
    @NamedQuery(name = "VSmProductNotReceived.findBySupplierId", query = "SELECT v FROM VSmProductNotReceived v WHERE v.supplierId = :supplierId")
    ,
    @NamedQuery(name = "VSmProductNotReceived.findBySupplierFirstName", query = "SELECT v FROM VSmProductNotReceived v WHERE v.supplierFirstName = :supplierFirstName")
    ,
    @NamedQuery(name = "VSmProductNotReceived.findBySupplierLastName", query = "SELECT v FROM VSmProductNotReceived v WHERE v.supplierLastName = :supplierLastName")
    ,
    @NamedQuery(name = "VSmProductNotReceived.findBySupplierCompanyName", query = "SELECT v FROM VSmProductNotReceived v WHERE v.supplierCompanyName = :supplierCompanyName")
    ,
    @NamedQuery(name = "VSmProductNotReceived.findByProductId", query = "SELECT v FROM VSmProductNotReceived v WHERE v.productId = :productId")
    ,
    @NamedQuery(name = "VSmProductNotReceived.findByQteCommanded", query = "SELECT v FROM VSmProductNotReceived v WHERE v.qteCommanded = :qteCommanded")
    ,
    @NamedQuery(name = "VSmProductNotReceived.findByQteRecieved", query = "SELECT v FROM VSmProductNotReceived v WHERE v.qteRecieved = :qteRecieved")})
public class VSmProductNotReceived implements Serializable {

    public static final String findByCltModuleId = "SELECT v FROM VSmProductNotReceived v WHERE v.cltModuleId = :cltModuleId";

    public static final String findByLikeReferenceOrSupplierIdAndCltModuleId
            = "SELECT v FROM VSmProductNotReceived v WHERE ("
            + "     (:productReference is null or :productReference = '' or lower(v.productReference) like :productReference) "
            + " and (:orderSupplierReference is null or :orderSupplierReference = '' or lower(v.orderSupplierReference) like :orderSupplierReference) "
            + " and (:productDesignation is null or :productDesignation = '' or lower(v.productDesignation) like :productDesignation) "
            + " and (:supplierId is null or (v.supplierId = :supplierId))) "
            + " and (v.cltModuleId = :cltModuleId)";

    private static final Long serialVersionUID = 1L;

    @Id
    @Column(name = "CODE")
    private String code;

    @Column(name = "ORDER_SUPPLIER_ID")
    private Long orderSupplierId;

    @Column(name = "ORDER_SUPPLIER_REFERENCE")
    private String orderSupplierReference;

    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    @Column(name = "PRODUCT_REFERENCE")
    private String productReference;

    @Column(name = "PRODUCT_DESIGNATION")
    private String productDesignation;

    @Column(name = "SUPPLIER_ID")
    private Long supplierId;

    @Column(name = "SUPPLIER_FIRST_NAME")
    private String supplierFirstName;

    @Column(name = "SUPPLIER_LAST_NAME")
    private String supplierLastName;

    @Column(name = "SUPPLIER_COMPANY_NAME")
    private String supplierCompanyName;

    @Column(name = "PRODUCT_ID")
    private Long productId;

    @Column(name = "QTE_COMMANDED")
    private String qteCommanded;

    @Column(name = "QTE_RECIEVED")
    private Long qteRecieved;

    public VSmProductNotReceived() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getOrderSupplierId() {
        return orderSupplierId;
    }

    public void setOrderSupplierId(Long orderSupplierId) {
        this.orderSupplierId = orderSupplierId;
    }

    public String getOrderSupplierReference() {
        return orderSupplierReference;
    }

    public void setOrderSupplierReference(String orderSupplierReference) {
        this.orderSupplierReference = orderSupplierReference;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public String getProductReference() {
        return productReference;
    }

    public void setProductReference(String productReference) {
        this.productReference = productReference;
    }

    public String getProductDesignation() {
        return productDesignation;
    }

    public void setProductDesignation(String productDesignation) {
        this.productDesignation = productDesignation;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplierFirstName() {
        return supplierFirstName;
    }

    public void setSupplierFirstName(String supplierFirstName) {
        this.supplierFirstName = supplierFirstName;
    }

    public String getSupplierLastName() {
        return supplierLastName;
    }

    public void setSupplierLastName(String supplierLastName) {
        this.supplierLastName = supplierLastName;
    }

    public String getSupplierCompanyName() {
        return supplierCompanyName;
    }

    public void setSupplierCompanyName(String supplierCompanyName) {
        this.supplierCompanyName = supplierCompanyName;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getQteCommanded() {
        return qteCommanded;
    }

    public void setQteCommanded(String qteCommanded) {
        this.qteCommanded = qteCommanded;
    }

    public Long getQteRecieved() {
        return qteRecieved;
    }

    public void setQteRecieved(Long qteRecieved) {
        this.qteRecieved = qteRecieved;
    }

}
