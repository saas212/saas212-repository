/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdessamad HALLAL
 */
@Entity
@Table(name = "V_SM_SUPPLIER_CATEGORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmSupplierCategory.findAll", query = "SELECT v FROM VSmSupplierCategory v")
    ,
    @NamedQuery(name = "VSmSupplierCategory.findById", query = "SELECT v FROM VSmSupplierCategory v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VSmSupplierCategory.findByName", query = "SELECT v FROM VSmSupplierCategory v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VSmSupplierCategory.findByDescription", query = "SELECT v FROM VSmSupplierCategory v WHERE v.description = :description")
    ,
    @NamedQuery(name = "VSmSupplierCategory.findByCltModuleId", query = "SELECT v FROM VSmSupplierCategory v WHERE v.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "VSmSupplierCategory.findBySortKey", query = "SELECT v FROM VSmSupplierCategory v WHERE v.sortKey = :sortKey")
    ,
    @NamedQuery(name = "VSmSupplierCategory.findByActive", query = "SELECT v FROM VSmSupplierCategory v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VSmSupplierCategory.findByDateCreation", query = "SELECT v FROM VSmSupplierCategory v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VSmSupplierCategory.findByUserCreation", query = "SELECT v FROM VSmSupplierCategory v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VSmSupplierCategory.findByDateUpdate", query = "SELECT v FROM VSmSupplierCategory v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VSmSupplierCategory.findByUserUpdate", query = "SELECT v FROM VSmSupplierCategory v WHERE v.userUpdate = :userUpdate")})
public class VSmSupplierCategory implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findByCltModuleIdAndActive = "SELECT v FROM VSmSupplierCategory v WHERE v.cltModuleId = :cltModuleId and v.active = :active";

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @Id
    private Long id;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Size(max = 45)
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;
    @Column(name = "SORT_KEY")
    private Long sortKey;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public VSmSupplierCategory() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

}
