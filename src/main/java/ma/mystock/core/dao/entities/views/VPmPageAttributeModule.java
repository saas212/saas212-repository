/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_PM_PAGE_ATTRIBUTE_MODULE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VPmPageAttributeModule.findAll", query = "SELECT v FROM VPmPageAttributeModule v")
    ,
    @NamedQuery(name = "VPmPageAttributeModule.findByCode", query = "SELECT v FROM VPmPageAttributeModule v WHERE v.code = :code")
    ,
    @NamedQuery(name = "VPmPageAttributeModule.findByCltModuleId", query = "SELECT v FROM VPmPageAttributeModule v WHERE v.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "VPmPageAttributeModule.findByPageAttributeId", query = "SELECT v FROM VPmPageAttributeModule v WHERE v.pageAttributeId = :pageAttributeId")
    ,
    @NamedQuery(name = "VPmPageAttributeModule.findByActive", query = "SELECT v FROM VPmPageAttributeModule v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VPmPageAttributeModule.findByPageId", query = "SELECT v FROM VPmPageAttributeModule v WHERE v.pageId = :pageId")
    ,
    @NamedQuery(name = "VPmPageAttributeModule.findByInfItemCode", query = "SELECT v FROM VPmPageAttributeModule v WHERE v.infItemCode = :infItemCode")
    ,
    @NamedQuery(name = "VPmPageAttributeModule.findByPmComponentId", query = "SELECT v FROM VPmPageAttributeModule v WHERE v.pmComponentId = :pmComponentId")
    ,
    @NamedQuery(name = "VPmPageAttributeModule.findByIsRequired", query = "SELECT v FROM VPmPageAttributeModule v WHERE v.isRequired = :isRequired")
    ,
    @NamedQuery(name = "VPmPageAttributeModule.findByIsReadonly", query = "SELECT v FROM VPmPageAttributeModule v WHERE v.isReadonly = :isReadonly")
    ,
    @NamedQuery(name = "VPmPageAttributeModule.findByIsHidden", query = "SELECT v FROM VPmPageAttributeModule v WHERE v.isHidden = :isHidden")
    ,
    @NamedQuery(name = "VPmPageAttributeModule.findByDataTypeId", query = "SELECT v FROM VPmPageAttributeModule v WHERE v.dataTypeId = :dataTypeId")
    ,
    @NamedQuery(name = "VPmPageAttributeModule.findByFormatTypeId", query = "SELECT v FROM VPmPageAttributeModule v WHERE v.formatTypeId = :formatTypeId")
    ,
    @NamedQuery(name = "VPmPageAttributeModule.findByMaxLenght", query = "SELECT v FROM VPmPageAttributeModule v WHERE v.maxLenght = :maxLenght")
    ,
    @NamedQuery(name = "VPmPageAttributeModule.findByMaxWord", query = "SELECT v FROM VPmPageAttributeModule v WHERE v.maxWord = :maxWord")})
public class VPmPageAttributeModule implements Serializable {

    // Map module id, page attribute id , InfItemCode value where <> null
    // Required 
    public static final String findByIsRequired = "SELECT v FROM VPmPageAttributeModule v WHERE upper(v.isRequired )= 'Y'";
    public static final String findByIsReadonly = "SELECT v FROM VPmPageAttributeModule v WHERE  upper(v.isReadonly )= 'Y'";
    public static final String findByIsHidden = "SELECT v FROM VPmPageAttributeModule v WHERE   upper(v.isHidden )= 'Y' ";
    public static final String findByDataTypeId = "SELECT v FROM VPmPageAttributeModule v WHERE   v.dataTypeId is not null";
    public static final String findByFormatTypeId = "SELECT v FROM VPmPageAttributeModule v WHERE   v.dataTypeId is not null";
    public static final String findByMaxLenght = "SELECT v FROM VPmPageAttributeModule v WHERE   v.maxLenght is not null";
    public static final String findByMaxWord = "SELECT v FROM VPmPageAttributeModule v WHERE  v.maxWord is not null ";

    private static final Long serialVersionUID = 1L;

    @Id
    @Column(name = "CODE")
    private String code;

    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    @Column(name = "PAGE_ATTRIBUTE_ID")
    private Long pageAttributeId;
    @Basic(optional = false)

    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "PAGE_ID")
    private Long pageId;

    @Column(name = "INF_ITEM_CODE")
    private String infItemCode;
    @Column(name = "PM_COMPONENT_ID")
    private Long pmComponentId;

    @Column(name = "IS_REQUIRED")
    private String isRequired;

    @Column(name = "IS_READONLY")
    private String isReadonly;

    @Column(name = "IS_HIDDEN")
    private String isHidden;

    @Column(name = "DATA_TYPE_ID")
    private Long dataTypeId;

    @Column(name = "FORMAT_TYPE_ID")
    private Long formatTypeId;

    @Column(name = "MAX_LENGHT")
    private Long maxLenght;

    @Column(name = "MAX_WORD")
    private Long maxWord;

    public VPmPageAttributeModule() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public Long getPageAttributeId() {
        return pageAttributeId;
    }

    public void setPageAttributeId(Long pageAttributeId) {
        this.pageAttributeId = pageAttributeId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Long getPageId() {
        return pageId;
    }

    public void setPageId(Long pageId) {
        this.pageId = pageId;
    }

    public String getInfItemCode() {
        return infItemCode;
    }

    public void setInfItemCode(String infItemCode) {
        this.infItemCode = infItemCode;
    }

    public Long getPmComponentId() {
        return pmComponentId;
    }

    public void setPmComponentId(Long pmComponentId) {
        this.pmComponentId = pmComponentId;
    }

    public String getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(String isRequired) {
        this.isRequired = isRequired;
    }

    public String getIsReadonly() {
        return isReadonly;
    }

    public void setIsReadonly(String isReadonly) {
        this.isReadonly = isReadonly;
    }

    public String getIsHidden() {
        return isHidden;
    }

    public void setIsHidden(String isHidden) {
        this.isHidden = isHidden;
    }

    public Long getDataTypeId() {
        return dataTypeId;
    }

    public void setDataTypeId(Long dataTypeId) {
        this.dataTypeId = dataTypeId;
    }

    public Long getFormatTypeId() {
        return formatTypeId;
    }

    public void setFormatTypeId(Long formatTypeId) {
        this.formatTypeId = formatTypeId;
    }

    public Long getMaxLenght() {
        return maxLenght;
    }

    public void setMaxLenght(Long maxLenght) {
        this.maxLenght = maxLenght;
    }

    public Long getMaxWord() {
        return maxWord;
    }

    public void setMaxWord(Long maxWord) {
        this.maxWord = maxWord;
    }

}
