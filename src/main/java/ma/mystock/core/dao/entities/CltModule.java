/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "CLT_MODULE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CltModule.findAll", query = "SELECT c FROM CltModule c")
    ,
    @NamedQuery(name = "CltModule.findById", query = "SELECT c FROM CltModule c WHERE c.id = :id")
    ,
    @NamedQuery(name = "CltModule.findByName", query = "SELECT c FROM CltModule c WHERE c.name = :name")
    ,
    @NamedQuery(name = "CltModule.findBySortKey", query = "SELECT c FROM CltModule c WHERE c.sortKey = :sortKey")
    ,
    @NamedQuery(name = "CltModule.findByActive", query = "SELECT c FROM CltModule c WHERE c.active = :active")
    ,
    @NamedQuery(name = "CltModule.findByDateCreation", query = "SELECT c FROM CltModule c WHERE c.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "CltModule.findByUserCreation", query = "SELECT c FROM CltModule c WHERE c.userCreation = :userCreation")
    ,
    @NamedQuery(name = "CltModule.findByDateUpdate", query = "SELECT c FROM CltModule c WHERE c.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "CltModule.findByUserUpdate", query = "SELECT c FROM CltModule c WHERE c.userUpdate = :userUpdate")})
public class CltModule implements Serializable {

    public static String findAll = "SELECT c FROM CltModule c";
    public static String findById = "SELECT c FROM CltModule c WHERE c.id = :id";
    public static String findByClientId = "SELECT c FROM CltModule c WHERE c.clientId = :clientId";
    public static String findByIdAndModuleTypeId = "SELECT c FROM CltModule c WHERE c.id = :id and c.moduleTypeId = :moduleTypeId";

    private static final Long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;

    @Column(name = "CLIENT_ID")
    private Long clientId;

    @Column(name = "MODULE_TYPE_ID")
    private Long moduleTypeId;

    @Column(name = "PARENT_MODULE_ID")
    private Long parentModuleId;

    @Column(name = "NAME")
    private String name;

    @Lob
    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "SORT_KEY")
    private Long sortKey;

    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "PM_MODEL_ID")
    private Long pmModelId;

    transient private Long moduleTypeIdToManager;

    public CltModule() {

    }

    public CltModule(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getModuleTypeId() {
        return moduleTypeId;
    }

    public void setModuleTypeId(Long moduleTypeId) {
        this.moduleTypeId = moduleTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CltModule)) {
            return false;
        }
        CltModule other = (CltModule) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.models.entitys.CltModule[ id=" + id + " ]";
    }

    //@XmlTransient
    //public List<SmExpense> getSmExpenseList() {
    //    return smExpenseList;
    //}
    //public void setSmExpenseList(List<SmExpense> smExpenseList) {
    //    this.smExpenseList = smExpenseList;
    //}
    public Long getParentModuleId() {
        return parentModuleId;
    }

    public void setParentModuleId(Long parentModuleId) {
        this.parentModuleId = parentModuleId;
    }

    public Long getModuleTypeIdToManager() {
        return moduleTypeIdToManager;
    }

    public void setModuleTypeIdToManager(Long moduleTypeIdToManager) {
        this.moduleTypeIdToManager = moduleTypeIdToManager;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getPmModelId() {
        return pmModelId;
    }

    public void setPmModelId(Long pmModelId) {
        this.pmModelId = pmModelId;
    }

}
