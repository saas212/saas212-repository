package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_PM_PAGE_PARAMETER_MODULE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VPmPageParameterModule.findAll", query = "SELECT v FROM VPmPageParameterModule v")
    ,
    @NamedQuery(name = "VPmPageParameterModule.findByCode", query = "SELECT v FROM VPmPageParameterModule v WHERE v.code = :code")
    ,
    @NamedQuery(name = "VPmPageParameterModule.findByCltModuleId", query = "SELECT v FROM VPmPageParameterModule v WHERE v.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "VPmPageParameterModule.findByPageParameterId", query = "SELECT v FROM VPmPageParameterModule v WHERE v.pageParameterId = :pageParameterId")
    ,
    @NamedQuery(name = "VPmPageParameterModule.findByPageParameterTypeName", query = "SELECT v FROM VPmPageParameterModule v WHERE v.pageParameterTypeName = :pageParameterTypeName")
    ,
    @NamedQuery(name = "VPmPageParameterModule.findByActive", query = "SELECT v FROM VPmPageParameterModule v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VPmPageParameterModule.findByPageParameterName", query = "SELECT v FROM VPmPageParameterModule v WHERE v.pageParameterName = :pageParameterName")})
public class VPmPageParameterModule implements Serializable {

    public static String findAll = "SELECT v FROM VPmPageParameterModule v";
    public static String findByCode = "SELECT v FROM VPmPageParameterModule v WHERE v.code = :code";

    private static final Long serialVersionUID = 1L;

    @Id
    @Column(name = "CODE")
    private String code;

    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    @Column(name = "PAGE_PARAMETER_ID")
    private Long pageParameterId;

    @Column(name = "PAGE_PARAMETER_TYPE_NAME")
    private String pageParameterTypeName;

    @Column(name = "ACTIVE")
    private String active;

    @Column(name = "PAGE_PARAMETER_NAME")
    private String pageParameterName;

    @Column(name = "VALUE")
    private String value;

    public VPmPageParameterModule() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public Long getPageParameterId() {
        return pageParameterId;
    }

    public void setPageParameterId(Long pageParameterId) {
        this.pageParameterId = pageParameterId;
    }

    public String getPageParameterTypeName() {
        return pageParameterTypeName;
    }

    public void setPageParameterTypeName(String pageParameterTypeName) {
        this.pageParameterTypeName = pageParameterTypeName;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getPageParameterName() {
        return pageParameterName;
    }

    public void setPageParameterName(String pageParameterName) {
        this.pageParameterName = pageParameterName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
