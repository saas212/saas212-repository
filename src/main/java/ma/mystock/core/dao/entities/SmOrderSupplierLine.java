/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "SM_ORDER_SUPPLIER_LINE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SmOrderSupplierLine.findAll", query = "SELECT s FROM SmOrderSupplierLine s")
    ,
    @NamedQuery(name = "SmOrderSupplierLine.findById", query = "SELECT s FROM SmOrderSupplierLine s WHERE s.id = :id")
    ,
    @NamedQuery(name = "SmOrderSupplierLine.findByProductId", query = "SELECT s FROM SmOrderSupplierLine s WHERE s.productId = :productId")
    ,
    @NamedQuery(name = "SmOrderSupplierLine.findByUnitPriceSale", query = "SELECT s FROM SmOrderSupplierLine s WHERE s.unitPriceSale = :unitPriceSale")
    ,
    @NamedQuery(name = "SmOrderSupplierLine.findByQuantity", query = "SELECT s FROM SmOrderSupplierLine s WHERE s.quantity = :quantity")
    ,
    @NamedQuery(name = "SmOrderSupplierLine.findByTva", query = "SELECT s FROM SmOrderSupplierLine s WHERE s.tva = :tva")
    ,
    @NamedQuery(name = "SmOrderSupplierLine.findByRemise", query = "SELECT s FROM SmOrderSupplierLine s WHERE s.remise = :remise")
    ,
    @NamedQuery(name = "SmOrderSupplierLine.findByPromotionId", query = "SELECT s FROM SmOrderSupplierLine s WHERE s.promotionId = :promotionId")
    ,
    @NamedQuery(name = "SmOrderSupplierLine.findByActive", query = "SELECT s FROM SmOrderSupplierLine s WHERE s.active = :active")
    ,
    @NamedQuery(name = "SmOrderSupplierLine.findByDateCreation", query = "SELECT s FROM SmOrderSupplierLine s WHERE s.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "SmOrderSupplierLine.findByUserCreation", query = "SELECT s FROM SmOrderSupplierLine s WHERE s.userCreation = :userCreation")
    ,
    @NamedQuery(name = "SmOrderSupplierLine.findByDateUpdate", query = "SELECT s FROM SmOrderSupplierLine s WHERE s.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "SmOrderSupplierLine.findByUserUpdate", query = "SELECT s FROM SmOrderSupplierLine s WHERE s.userUpdate = :userUpdate")})
public class SmOrderSupplierLine implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String findById = "SELECT v FROM SmOrderSupplierLine v WHERE v.id = :id";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;

    @Column(name = "PRODUCT_ID")
    private Long productId;

    @Column(name = "DESIGNATION")
    private String designation;

    @Column(name = "UNIT_PRICE_SALE")
    private Double unitPriceSale;

    @Column(name = "QUANTITY")
    private Long quantity;

    @Column(name = "ORDER_SUPPLIER_ID")
    private Long orderSupplierId;

    @Column(name = "TVA")
    private Double tva;
    @Column(name = "REMISE")
    private Double remise;

    @Column(name = "PROMOTION_ID")
    private Long promotionId;

    @Column(name = "ACTIVE")
    private String active;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public SmOrderSupplierLine() {
    }

    public SmOrderSupplierLine(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Double getUnitPriceSale() {
        return unitPriceSale;
    }

    public void setUnitPriceSale(Double unitPriceSale) {
        this.unitPriceSale = unitPriceSale;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getTva() {
        return tva;
    }

    public void setTva(Double tva) {
        this.tva = tva;
    }

    public Double getRemise() {
        return remise;
    }

    public void setRemise(Double remise) {
        this.remise = remise;
    }

    public Long getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Long promotionId) {
        this.promotionId = promotionId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public Long getOrderSupplierId() {
        return orderSupplierId;
    }

    public void setOrderSupplierId(Long orderSupplierId) {
        this.orderSupplierId = orderSupplierId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SmOrderSupplierLine)) {
            return false;
        }
        SmOrderSupplierLine other = (SmOrderSupplierLine) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entitys.SmOrderSupplierLine[ id=" + id + " ]";
    }

}
