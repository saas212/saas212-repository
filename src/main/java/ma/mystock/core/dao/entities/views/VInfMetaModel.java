package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_INF_META_MODEL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VInfMetaModel.findAll", query = "SELECT v FROM VInfMetaModel v")
    ,
    @NamedQuery(name = "VInfMetaModel.findByPrefixId", query = "SELECT v FROM VInfMetaModel v WHERE v.prefixId = :prefixId")
    ,
    @NamedQuery(name = "VInfMetaModel.findByPrefixCode", query = "SELECT v FROM VInfMetaModel v WHERE v.prefixCode = :prefixCode")
    ,
    @NamedQuery(name = "VInfMetaModel.findByItemCode", query = "SELECT v FROM VInfMetaModel v WHERE v.itemCode = :itemCode")
    ,
    @NamedQuery(name = "VInfMetaModel.findByTextTypeId", query = "SELECT v FROM VInfMetaModel v WHERE v.textTypeId = :textTypeId")
    ,
    @NamedQuery(name = "VInfMetaModel.findByTextTypeCode", query = "SELECT v FROM VInfMetaModel v WHERE v.textTypeCode = :textTypeCode")
    ,
    @NamedQuery(name = "VInfMetaModel.findByLanguageId", query = "SELECT v FROM VInfMetaModel v WHERE v.languageId = :languageId")
    ,
    @NamedQuery(name = "VInfMetaModel.findByLanguageCode", query = "SELECT v FROM VInfMetaModel v WHERE v.languageCode = :languageCode")
    ,
    @NamedQuery(name = "VInfMetaModel.findByCode", query = "SELECT v FROM VInfMetaModel v WHERE v.code = :code")})
public class VInfMetaModel implements Serializable {

    public static String findAll = "SELECT v FROM VInfMetaModel v";

    private static final Long serialVersionUID = 1L;

    @Column(name = "CODE")
    @Id
    private String code;

    @Column(name = "PREFIX_ID")
    private Long prefixId;

    @Column(name = "PREFIX_CODE")
    private String prefixCode;

    @Column(name = "ITEM_CODE")
    private String itemCode;

    @Column(name = "VALUE")
    private String value;

    @Column(name = "TEXT_TYPE_ID")
    private Long textTypeId;

    @Column(name = "TEXT_TYPE_CODE")
    private String textTypeCode;

    @Column(name = "LANGUAGE_ID")
    private Long languageId;

    @Column(name = "LANGUAGE_CODE")
    private String languageCode;

    public VInfMetaModel() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getPrefixId() {
        return prefixId;
    }

    public void setPrefixId(Long prefixId) {
        this.prefixId = prefixId;
    }

    public String getPrefixCode() {
        return prefixCode;
    }

    public void setPrefixCode(String prefixCode) {
        this.prefixCode = prefixCode;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getTextTypeId() {
        return textTypeId;
    }

    public void setTextTypeId(Long textTypeId) {
        this.textTypeId = textTypeId;
    }

    public String getTextTypeCode() {
        return textTypeCode;
    }

    public void setTextTypeCode(String textTypeCode) {
        this.textTypeCode = textTypeCode;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

}
