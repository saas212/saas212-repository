package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_CTA_ENTITY_PHONE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCtaEntityPhone.findAll", query = "SELECT v FROM VCtaEntityPhone v")
    ,
    @NamedQuery(name = "VCtaEntityPhone.findById", query = "SELECT v FROM VCtaEntityPhone v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VCtaEntityPhone.findByCountryCode", query = "SELECT v FROM VCtaEntityPhone v WHERE v.countryCode = :countryCode")
    ,
    @NamedQuery(name = "VCtaEntityPhone.findByNumber", query = "SELECT v FROM VCtaEntityPhone v WHERE v.number = :number")
    ,
    @NamedQuery(name = "VCtaEntityPhone.findByEntityId", query = "SELECT v FROM VCtaEntityPhone v WHERE v.entityId = :entityId")
    ,
    @NamedQuery(name = "VCtaEntityPhone.findByPhoneTypeId", query = "SELECT v FROM VCtaEntityPhone v WHERE v.phoneTypeId = :phoneTypeId")
    ,
    @NamedQuery(name = "VCtaEntityPhone.findByDateCreation", query = "SELECT v FROM VCtaEntityPhone v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VCtaEntityPhone.findByUserCreation", query = "SELECT v FROM VCtaEntityPhone v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VCtaEntityPhone.findByDateUpdate", query = "SELECT v FROM VCtaEntityPhone v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VCtaEntityPhone.findByUserUpdate", query = "SELECT v FROM VCtaEntityPhone v WHERE v.userUpdate = :userUpdate")
    ,
    @NamedQuery(name = "VCtaEntityPhone.findByPhoneTypeName", query = "SELECT v FROM VCtaEntityPhone v WHERE v.phoneTypeName = :phoneTypeName")})
public class VCtaEntityPhone implements Serializable {

    public static final String findByEntityId = "SELECT v FROM VCtaEntityPhone v WHERE v.entityId = :entityId";

    private static final Long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "COUNTRY_CODE")
    private String countryCode;

    @Column(name = "NUMBER")
    private String number;

    @Column(name = "ENTITY_ID")
    private Long entityId;

    @Column(name = "PRIORITY")
    private Long priority;

    @Column(name = "PHONE_TYPE_ID")
    private Long phoneTypeId;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "PHONE_TYPE_NAME")
    private String phoneTypeName;

    public VCtaEntityPhone() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Long getPhoneTypeId() {
        return phoneTypeId;
    }

    public void setPhoneTypeId(Long phoneTypeId) {
        this.phoneTypeId = phoneTypeId;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getPhoneTypeName() {
        return phoneTypeName;
    }

    public void setPhoneTypeName(String phoneTypeName) {
        this.phoneTypeName = phoneTypeName;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

}
