/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdou
 */
@Entity
@Table(name = "V_PM_PAGE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VPmPage.findAll", query = "SELECT v FROM VPmPage v")
    ,
    @NamedQuery(name = "VPmPage.findById", query = "SELECT v FROM VPmPage v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VPmPage.findByName", query = "SELECT v FROM VPmPage v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VPmPage.findBySortKey", query = "SELECT v FROM VPmPage v WHERE v.sortKey = :sortKey")
    ,
    @NamedQuery(name = "VPmPage.findByPageTypeId", query = "SELECT v FROM VPmPage v WHERE v.pageTypeId = :pageTypeId")
    ,
    @NamedQuery(name = "VPmPage.findByActive", query = "SELECT v FROM VPmPage v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VPmPage.findByUserCreation", query = "SELECT v FROM VPmPage v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VPmPage.findByDateCreation", query = "SELECT v FROM VPmPage v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VPmPage.findByUserUpdate", query = "SELECT v FROM VPmPage v WHERE v.userUpdate = :userUpdate")
    ,
    @NamedQuery(name = "VPmPage.findByDateUpdate", query = "SELECT v FROM VPmPage v WHERE v.dateUpdate = :dateUpdate")})
public class VPmPage implements Serializable {

    public static final String findAllActive = "SELECT v FROM VPmPage v WHERE v.active = :active";
    public static final String findAll = "SELECT v FROM VPmPage v";
    public static final String findById = "SELECT v FROM VPmPage v WHERE v.id = :id";

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "IN_DEV")
    private String inDev;

    @Column(name = "SORT_KEY")
    private Long sortKey;
    @Column(name = "PAGE_TYPE_ID")
    private Long pageTypeId;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    //@Size(max = 255)
    //@Column(name = "PACKAGE_JAVA")
    //private String packageJava;
    //@Size(max = 255)
    //@Column(name = "FOLDER_JSF")
    //private String folderJsf;

    public VPmPage() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public Long getPageTypeId() {
        return pageTypeId;
    }

    public void setPageTypeId(Long pageTypeId) {
        this.pageTypeId = pageTypeId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    /*
     public String getPackageJava() {
     return packageJava;
     }

     public void setPackageJava(String packageJava) {
     this.packageJava = packageJava;
     }

     public String getFolderJsf() {
     return folderJsf;
     }

     public void setFolderJsf(String folderJsf) {
     this.folderJsf = folderJsf;
     }
     */
    public String getInDev() {
        return inDev;
    }

    public void setInDev(String inDev) {
        this.inDev = inDev;
    }

}
