package ma.mystock.core.dao.entities.views.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_PM_MODEL_DTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VPmModelDto.findAll", query = "SELECT v FROM VPmModelDto v")
    ,
    @NamedQuery(name = "VPmModelDto.findById", query = "SELECT v FROM VPmModelDto v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VPmModelDto.findByName", query = "SELECT v FROM VPmModelDto v WHERE v.name = :name")})
public class VPmModelDto implements Serializable {

    public static String findAll = "SELECT v FROM VPmModelDto v";
    public static String findById = "SELECT v FROM VPmModelDto v WHERE v.id = :id";

    private static final Long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    transient private List<VPmGroupsDto> vPmGroupsDtoList;

    public VPmModelDto() {
        this.vPmGroupsDtoList = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<VPmGroupsDto> getvPmGroupsDtoList() {
        return vPmGroupsDtoList;
    }

    public void setvPmGroupsDtoList(List<VPmGroupsDto> vPmGroupsDtoList) {
        this.vPmGroupsDtoList = vPmGroupsDtoList;
    }

}
