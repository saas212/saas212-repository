package ma.mystock.core.dao.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "CTA_ENTITY_EMAIL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CtaEntityEmail.findAll", query = "SELECT c FROM CtaEntityEmail c")
    ,
    @NamedQuery(name = "CtaEntityEmail.findById", query = "SELECT c FROM CtaEntityEmail c WHERE c.id = :id")
    ,
    @NamedQuery(name = "CtaEntityEmail.findByEmailAddress", query = "SELECT c FROM CtaEntityEmail c WHERE c.emailAddress = :emailAddress")
    ,
    @NamedQuery(name = "CtaEntityEmail.findByIsForNotification", query = "SELECT c FROM CtaEntityEmail c WHERE c.isForNotification = :isForNotification")
    ,
    @NamedQuery(name = "CtaEntityEmail.findByDateCreation", query = "SELECT c FROM CtaEntityEmail c WHERE c.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "CtaEntityEmail.findByUserCreation", query = "SELECT c FROM CtaEntityEmail c WHERE c.userCreation = :userCreation")
    ,
    @NamedQuery(name = "CtaEntityEmail.findByDateUpdate", query = "SELECT c FROM CtaEntityEmail c WHERE c.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "CtaEntityEmail.findByUserUpdate", query = "SELECT c FROM CtaEntityEmail c WHERE c.userUpdate = :userUpdate")})
public class CtaEntityEmail implements Serializable {

    public static final String findById = "SELECT c FROM CtaEntityEmail c WHERE c.id = :id";

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;

    @Column(name = "EMAIL_ADDRESS")
    private String emailAddress;

    @Column(name = "IS_FOR_NOTIFICATION")
    private String isForNotification;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "ENTITY_ID")
    private Long entityId;

    @Column(name = "EMAIL_TYPE_ID")
    private Long emailTypeId;

    @Column(name = "PRIORITY")
    private Long priority;

    public CtaEntityEmail() {
    }

    public CtaEntityEmail(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getIsForNotification() {
        return isForNotification;
    }

    public void setIsForNotification(String isForNotification) {
        this.isForNotification = isForNotification;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CtaEntityEmail)) {
            return false;
        }
        CtaEntityEmail other = (CtaEntityEmail) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.dao.entities.CtaEntityEmail[ id=" + id + " ]";
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Long getEmailTypeId() {
        return emailTypeId;
    }

    public void setEmailTypeId(Long emailTypeId) {
        this.emailTypeId = emailTypeId;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

}
