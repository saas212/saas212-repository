/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_INF_BASIC_PARAMETER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VInfBasicParameter.findAll", query = "SELECT v FROM VInfBasicParameter v")
    ,
    @NamedQuery(name = "VInfBasicParameter.findById", query = "SELECT v FROM VInfBasicParameter v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VInfBasicParameter.findByName", query = "SELECT v FROM VInfBasicParameter v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VInfBasicParameter.findByBasicParameterTypeId", query = "SELECT v FROM VInfBasicParameter v WHERE v.basicParameterTypeId = :basicParameterTypeId")})
public class VInfBasicParameter implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findById = "SELECT v FROM VInfBasicParameter v where v.id = :id";
    public static final String findAll = "SELECT v FROM VInfBasicParameter v";
    public static final String findAllActive = "SELECT v FROM VInfBasicParameter v where v.active = :active";

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "VALUE")
    private String value;

    @Column(name = "BASIC_PARAMETER_TYPE_ID")
    private Long basicParameterTypeId;

    @Column(name = "ACTIVE")
    private String active;

    @Column(name = "BASIC_PARAMETER_TYPE_NAME")
    private String basicParameterTypeName;

    @Column(name = "CAN_BE_DELETE")
    private String canBeDelete;

    public VInfBasicParameter() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getBasicParameterTypeId() {
        return basicParameterTypeId;
    }

    public void setBasicParameterTypeId(Long basicParameterTypeId) {
        this.basicParameterTypeId = basicParameterTypeId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getBasicParameterTypeName() {
        return basicParameterTypeName;
    }

    public void setBasicParameterTypeName(String basicParameterTypeName) {
        this.basicParameterTypeName = basicParameterTypeName;
    }

    public String isCanBeDelete() {
        return canBeDelete;
    }

    public void setCanBeDelete(String canBeDelete) {
        this.canBeDelete = canBeDelete;
    }

}
