/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities;

import java.io.Serializable;

import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * Abdou
 */
@Entity
@Table(name = "CLT_PARAMETER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CltParameter.findAll", query = "SELECT c FROM CltParameter c")
    ,
    @NamedQuery(name = "CltParameter.findById", query = "SELECT c FROM CltParameter c WHERE c.id = :id")
    ,
    @NamedQuery(name = "CltParameter.findByName", query = "SELECT c FROM CltParameter c WHERE c.name = :name")
    ,
    @NamedQuery(name = "CltParameter.findByActive", query = "SELECT c FROM CltParameter c WHERE c.active = :active")
    ,
    @NamedQuery(name = "CltParameter.findByDateCreation", query = "SELECT c FROM CltParameter c WHERE c.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "CltParameter.findByUserCreation", query = "SELECT c FROM CltParameter c WHERE c.userCreation = :userCreation")
    ,
    @NamedQuery(name = "CltParameter.findByDateUpdate", query = "SELECT c FROM CltParameter c WHERE c.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "CltParameter.findByUserUpdate", query = "SELECT c FROM CltParameter c WHERE c.userUpdate = :userUpdate")
    ,
    @NamedQuery(name = "CltParameter.findByDescription", query = "SELECT c FROM CltParameter c WHERE c.description = :description")})
public class CltParameter implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String findById = "SELECT c FROM CltParameter c WHERE c.id = :id";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "DEFAULT_VALUE")
    private String defaultValue;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;
    @Size(max = 255)
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "PARAMETER_TYPE_ID")
    private Long parameterTypeId;
    @JoinColumn(name = "PARAMETER_TYPE_ID", referencedColumnName = "ID", updatable = false, insertable = false)
    @ManyToOne
    private CltParameterType cltParameterTypeId;

    @Column(name = "ACTIVE")
    private String active;

    public CltParameter() {
    }

    public CltParameter(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getParameterTypeId() {
        return parameterTypeId;
    }

    public void setParameterTypeId(Long parameterTypeId) {
        this.parameterTypeId = parameterTypeId;
    }

    public CltParameterType getCltParameterTypeId() {
        return cltParameterTypeId;
    }

    public void setCltParameterTypeId(CltParameterType cltParameterTypeId) {
        this.cltParameterTypeId = cltParameterTypeId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CltParameter)) {
            return false;
        }
        CltParameter other = (CltParameter) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entitys.CltParameter[ id=" + id + " ]";
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

}
