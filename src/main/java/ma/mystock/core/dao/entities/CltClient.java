package ma.mystock.core.dao.entities;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdou
 */
@Entity
@Table(name = "CLT_CLIENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CltClient.findAll", query = "SELECT c FROM CltClient c")
    ,
    @NamedQuery(name = "CltClient.findById", query = "SELECT c FROM CltClient c WHERE c.id = :id")
    ,
    @NamedQuery(name = "CltClient.findByFirstName", query = "SELECT c FROM CltClient c WHERE c.firstName = :firstName")
    ,
    @NamedQuery(name = "CltClient.findByLastName", query = "SELECT c FROM CltClient c WHERE c.lastName = :lastName")
    ,
    @NamedQuery(name = "CltClient.findByCompanyName", query = "SELECT c FROM CltClient c WHERE c.companyName = :companyName")
    ,
    @NamedQuery(name = "CltClient.findByActive", query = "SELECT c FROM CltClient c WHERE c.active = :active")
    ,
    @NamedQuery(name = "CltClient.findByDateCreation", query = "SELECT c FROM CltClient c WHERE c.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "CltClient.findByUserCreation", query = "SELECT c FROM CltClient c WHERE c.userCreation = :userCreation")
    ,
    @NamedQuery(name = "CltClient.findByDateUpdate", query = "SELECT c FROM CltClient c WHERE c.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "CltClient.findByUserUpdate", query = "SELECT c FROM CltClient c WHERE c.userUpdate = :userUpdate")})
public class CltClient implements Serializable {

    public static final String findByActive = "SELECT c FROM CltClient c WHERE c.active = :active";
    public static final String findById = "SELECT c FROM CltClient c WHERE c.id = :id";

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;

    @Column(name = "CODE")
    private String code;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "COMPANY_NAME")
    private String companyName;

    @Column(name = "ACTIVE")
    private String active;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "ENTITY_ID")
    private Long entityId;

    //@OneToMany(mappedBy = "clinetId")
    //private Collection<CltParameterClient> cltParameterClientCollection;
    //@JoinColumn(name = "CLIENT_STATUS_ID", referencedColumnName = "ID")
    //@ManyToOne
    //private CltClientStatus clientStatusId;
    public CltClient() {
    }

    public CltClient(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /*
    public Long getDefaultLanguageId() {
        return defaultLanguageId;
    }

    public void setDefaultLanguageId(Long defaultLanguageId) {
        this.defaultLanguageId = defaultLanguageId;
    }
    
     */
    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    /*
     @XmlTransient
     public Collection<CltParameterClient> getCltParameterClientCollection() {
     return cltParameterClientCollection;
     }

     public void setCltParameterClientCollection(Collection<CltParameterClient> cltParameterClientCollection) {
     this.cltParameterClientCollection = cltParameterClientCollection;
     }
     */
 /*
     public CltClientStatus getClientStatusId() {
     return clientStatusId;
     }

     public void setClientStatusId(CltClientStatus clientStatusId) {
     this.clientStatusId = clientStatusId;
     }
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CltClient)) {
            return false;
        }
        CltClient other = (CltClient) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entitys.CltClient[ id=" + id + " ]";
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

}
