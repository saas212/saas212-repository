/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 *
 */
@Entity
@Table(name = "V_PM_VALIDATION")
@XmlRootElement
public class VPmValidation implements Serializable {

    private static final Long serialVersionUID = 1L;
    public static String findAll = "SELECT v FROM VPmValidation v";
    public static String findByPageId = "SELECT v FROM VPmValidation v WHERE v.pageId = :pageId";
    public static String findByPageIdAndItemCode = "SELECT v FROM VPmValidation v WHERE v.pageId = :pageId and v.itemCode = :itemCode";

    @Id
    @Column(name = "CODE")
    private String code;

    @Column(name = "PAGE_ID")
    private Long pageId;

    @Column(name = "ITEM_CODE")
    private String itemCode;

    @Column(name = "VALIDATION_ID")
    private Long validationId;

    @Lob
    @Column(name = "PARAMS")
    private String params;

    @Column(name = "PARAM_NUMBER")
    private Long paramNumber;

    @Lob
    @Column(name = "ERROR_MESSAGE")
    private String errorMessage;

    @Lob
    @Column(name = "HELP")
    private String help;

    @Lob
    @Column(name = "CUSTOM_ERROR")
    private String customError;

    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    public VPmValidation() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getPageId() {
        return pageId;
    }

    public void setPageId(Long pageId) {
        this.pageId = pageId;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public Long getValidationId() {
        return validationId;
    }

    public void setValidationId(Long validationId) {
        this.validationId = validationId;
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public Long getParamNumber() {
        return paramNumber;
    }

    public void setParamNumber(Long paramNumber) {
        this.paramNumber = paramNumber;
    }

    public String getHelp() {
        return help;
    }

    public void setHelp(String help) {
        this.help = help;
    }

    public String getCustomError() {
        return customError;
    }

    public void setCustomError(String customError) {
        this.customError = customError;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
