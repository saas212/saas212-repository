/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import ma.mystock.core.dao.entities.views.dto.LovsDTO;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_SM_EXPENSE_TYPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmExpenseType.findAll", query = "SELECT v FROM VSmExpenseType v")
    ,
    @NamedQuery(name = "VSmExpenseType.findById", query = "SELECT v FROM VSmExpenseType v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VSmExpenseType.findByName", query = "SELECT v FROM VSmExpenseType v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VSmExpenseType.findByActive", query = "SELECT v FROM VSmExpenseType v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VSmExpenseType.findBySortKey", query = "SELECT v FROM VSmExpenseType v WHERE v.sortKey = :sortKey")
    ,
    @NamedQuery(name = "VSmExpenseType.findByCltModuleId", query = "SELECT v FROM VSmExpenseType v WHERE v.cltModuleId = :cltModuleId")})
public class VSmExpenseType extends LovsDTO implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findByCltModuleId = "SELECT v FROM VSmExpenseType v WHERE v.cltModuleId = :cltModuleId";

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "SORT_KEY")
    private Long sortKey;
    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    public VSmExpenseType() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

}
