/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Yamine Abdelmounaim <yaminea@evision.ca>
 */
@Entity
@Table(name = "V_PM_PAGE_MODULE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VPmPageModule.findAll", query = "SELECT v FROM VPmPageModule v")
    ,
    @NamedQuery(name = "VPmPageModule.findById", query = "SELECT v FROM VPmPageModule v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VPmPageModule.findByIdPage", query = "SELECT v FROM VPmPageModule v WHERE v.idPage = :idPage")
    ,
    @NamedQuery(name = "VPmPageModule.findByCltModuleId", query = "SELECT v FROM VPmPageModule v WHERE v.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "VPmPageModule.findByPageName", query = "SELECT v FROM VPmPageModule v WHERE v.pageName = :pageName")
    ,
    @NamedQuery(name = "VPmPageModule.findByPageTypeName", query = "SELECT v FROM VPmPageModule v WHERE v.pageTypeName = :pageTypeName")
    ,
    @NamedQuery(name = "VPmPageModule.findByActive", query = "SELECT v FROM VPmPageModule v WHERE v.active = :active")})
public class VPmPageModule implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String findByCltModuleId = "SELECT v FROM VPmPageModule v WHERE v.cltModuleId = :cltModuleId and v.active = :active";
    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_PAGE")
    private Long idPage;
    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;
    @Size(max = 255)
    @Column(name = "PAGE_NAME")
    private String pageName;
    @Lob
    @Size(max = 65535)
    @Column(name = "DESCRIPTION")
    private String description;
    @Size(max = 255)
    @Column(name = "PAGE_TYPE_NAME")
    private String pageTypeName;
    @Column(name = "ACTIVE")
    private String active;

    public VPmPageModule() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdPage() {
        return idPage;
    }

    public void setIdPage(Long idPage) {
        this.idPage = idPage;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPageTypeName() {
        return pageTypeName;
    }

    public void setPageTypeName(String pageTypeName) {
        this.pageTypeName = pageTypeName;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

}
