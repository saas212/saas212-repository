/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_INF_CITY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VInfCity.findAll", query = "SELECT v FROM VInfCity v")
    ,
    @NamedQuery(name = "VInfCity.findById", query = "SELECT v FROM VInfCity v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VInfCity.findByCode", query = "SELECT v FROM VInfCity v WHERE v.code = :code")
    ,
    @NamedQuery(name = "VInfCity.findByName", query = "SELECT v FROM VInfCity v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VInfCity.findBySortKey", query = "SELECT v FROM VInfCity v WHERE v.sortKey = :sortKey")
    ,
    @NamedQuery(name = "VInfCity.findByCountryId", query = "SELECT v FROM VInfCity v WHERE v.countryId = :countryId")
    ,
    @NamedQuery(name = "VInfCity.findByActive", query = "SELECT v FROM VInfCity v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VInfCity.findByDateCreation", query = "SELECT v FROM VInfCity v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VInfCity.findByUserCreation", query = "SELECT v FROM VInfCity v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VInfCity.findByDateUpdate", query = "SELECT v FROM VInfCity v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VInfCity.findByUserUpdate", query = "SELECT v FROM VInfCity v WHERE v.userUpdate = :userUpdate")})
public class VInfCity implements Serializable {

    private static final Long serialVersionUID = 1L;

    //public static final String findByCltModuleId = "SELECT v FROM VInfCity v WHERE v.cltModuleId = :cltModuleId";
    //public static final String findByCltModuleIdAndActive = "SELECT v FROM VInfCity v WHERE v.cltModuleId = :cltModuleId and v.active = :active";
    public static final String findById = "SELECT v FROM VInfCity v WHERE v.id = :id";
    public static final String findByCountryIdAndActive = "SELECT v FROM VInfCity v WHERE v.countryId = :countryId and v.active = :active";

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @Id
    private Long id;
    @Size(max = 255)
    @Column(name = "CODE")
    private String code;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Column(name = "SORT_KEY")
    private Long sortKey;
    @Column(name = "COUNTRY_ID")
    private Long countryId;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    //@Column(name = "CLT_MODULE_ID")
    //private Long cltModuleId;
    public VInfCity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }
    /*
    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }
     */

}
