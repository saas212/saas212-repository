/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "SM_RETURN_RECEIPT_STATUS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SmReturnReceiptStatus.findAll", query = "SELECT s FROM SmReturnReceiptStatus s")
    ,
    @NamedQuery(name = "SmReturnReceiptStatus.findById", query = "SELECT s FROM SmReturnReceiptStatus s WHERE s.id = :id")
    ,
    @NamedQuery(name = "SmReturnReceiptStatus.findByName", query = "SELECT s FROM SmReturnReceiptStatus s WHERE s.name = :name")
    ,
    @NamedQuery(name = "SmReturnReceiptStatus.findBySortKey", query = "SELECT s FROM SmReturnReceiptStatus s WHERE s.sortKey = :sortKey")
    ,
    @NamedQuery(name = "SmReturnReceiptStatus.findByActive", query = "SELECT s FROM SmReturnReceiptStatus s WHERE s.active = :active")
    ,
    @NamedQuery(name = "SmReturnReceiptStatus.findByDateCreation", query = "SELECT s FROM SmReturnReceiptStatus s WHERE s.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "SmReturnReceiptStatus.findByUserCreation", query = "SELECT s FROM SmReturnReceiptStatus s WHERE s.userCreation = :userCreation")
    ,
    @NamedQuery(name = "SmReturnReceiptStatus.findByDateUpdate", query = "SELECT s FROM SmReturnReceiptStatus s WHERE s.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "SmReturnReceiptStatus.findByUserUpdate", query = "SELECT s FROM SmReturnReceiptStatus s WHERE s.userUpdate = :userUpdate")})
public class SmReturnReceiptStatus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "SORT_KEY")
    private Long sortKey;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public SmReturnReceiptStatus() {
    }

    public SmReturnReceiptStatus(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SmReturnReceiptStatus)) {
            return false;
        }
        SmReturnReceiptStatus other = (SmReturnReceiptStatus) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entitys.views.SmReturnReceiptStatus[ id=" + id + " ]";
    }

}
