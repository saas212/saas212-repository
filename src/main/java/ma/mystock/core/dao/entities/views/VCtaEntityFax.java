package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_CTA_ENTITY_FAX")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCtaEntityFax.findAll", query = "SELECT v FROM VCtaEntityFax v")
    ,
    @NamedQuery(name = "VCtaEntityFax.findById", query = "SELECT v FROM VCtaEntityFax v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VCtaEntityFax.findByCountryCode", query = "SELECT v FROM VCtaEntityFax v WHERE v.countryCode = :countryCode")
    ,
    @NamedQuery(name = "VCtaEntityFax.findByNumber", query = "SELECT v FROM VCtaEntityFax v WHERE v.number = :number")
    ,
    @NamedQuery(name = "VCtaEntityFax.findByPriority", query = "SELECT v FROM VCtaEntityFax v WHERE v.priority = :priority")
    ,
    @NamedQuery(name = "VCtaEntityFax.findByEntityId", query = "SELECT v FROM VCtaEntityFax v WHERE v.entityId = :entityId")
    ,
    @NamedQuery(name = "VCtaEntityFax.findByFaxTypeId", query = "SELECT v FROM VCtaEntityFax v WHERE v.faxTypeId = :faxTypeId")
    ,
    @NamedQuery(name = "VCtaEntityFax.findByDateCreation", query = "SELECT v FROM VCtaEntityFax v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VCtaEntityFax.findByUserCreation", query = "SELECT v FROM VCtaEntityFax v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VCtaEntityFax.findByDateUpdate", query = "SELECT v FROM VCtaEntityFax v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VCtaEntityFax.findByUserUpdate", query = "SELECT v FROM VCtaEntityFax v WHERE v.userUpdate = :userUpdate")
    ,
    @NamedQuery(name = "VCtaEntityFax.findByFaxTypeName", query = "SELECT v FROM VCtaEntityFax v WHERE v.faxTypeName = :faxTypeName")})
public class VCtaEntityFax implements Serializable {

    public static final String findByEntityId = "SELECT v FROM VCtaEntityFax v WHERE v.entityId = :entityId";

    private static final Long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "COUNTRY_CODE")
    private String countryCode;

    @Column(name = "NUMBER")
    private String number;

    @Column(name = "PRIORITY")
    private Long priority;

    @Column(name = "ENTITY_ID")
    private Long entityId;

    @Column(name = "FAX_TYPE_ID")
    private Long faxTypeId;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "FAX_TYPE_NAME")
    private String faxTypeName;

    public VCtaEntityFax() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Long getFaxTypeId() {
        return faxTypeId;
    }

    public void setFaxTypeId(Long faxTypeId) {
        this.faxTypeId = faxTypeId;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getFaxTypeName() {
        return faxTypeName;
    }

    public void setFaxTypeName(String faxTypeName) {
        this.faxTypeName = faxTypeName;
    }

}
