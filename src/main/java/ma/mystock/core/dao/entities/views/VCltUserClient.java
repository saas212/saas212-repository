/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Yamine Abdelmounaim <yaminea@evision.ca>
 */
@Entity
@Table(name = "V_CLT_USER_CLIENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCltUserClient.findAll", query = "SELECT v FROM VCltUserClient v")
    ,
    @NamedQuery(name = "VCltUserClient.findById", query = "SELECT v FROM VCltUserClient v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VCltUserClient.findByUserId", query = "SELECT v FROM VCltUserClient v WHERE v.userId = :userId")
    ,
    @NamedQuery(name = "VCltUserClient.findByClientId", query = "SELECT v FROM VCltUserClient v WHERE v.clientId = :clientId")
    ,
    @NamedQuery(name = "VCltUserClient.findByActive", query = "SELECT v FROM VCltUserClient v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VCltUserClient.findByFirstNameClient", query = "SELECT v FROM VCltUserClient v WHERE v.firstNameClient = :firstNameClient")
    ,
    @NamedQuery(name = "VCltUserClient.findByUserIdAndActive", query = "SELECT v FROM VCltUserClient v where v.userId = :userId and v.active = :active")
    ,
    @NamedQuery(name = "VCltUserClient.findByLastNameClient", query = "SELECT v FROM VCltUserClient v WHERE v.lastNameClient = :lastNameClient")})
public class VCltUserClient implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String findAll = "SELECT v FROM VCltUserClient v";
    public static final String findById = "SELECT v FROM VCltUserClient v WHERE v.id = :id";
    public static final String findAllActive = "SELECT v FROM VCltUserClient v WHERE v.active = :active";
    public static final String findByUserIdAndActive = "SELECT v FROM VCltUserClient v where v.userId = :userId and v.active = :active";
    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;
    @Column(name = "USER_ID")
    private Long userId;
    @Column(name = "CLIENT_ID")
    private Long clientId;
    @Column(name = "ACTIVE")
    private String active;
    @Size(max = 255)
    @Column(name = "FIRST_NAME_CLIENT")
    private String firstNameClient;
    @Size(max = 255)
    @Column(name = "LAST_NAME_CLIENT")
    private String lastNameClient;
    @Column(name = "COMPANY_NAME")
    private String companyName;
    @Column(name = "EMAIL")
    private String email;

    public VCltUserClient() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getFirstNameClient() {
        return firstNameClient;
    }

    public void setFirstNameClient(String firstNameClient) {
        this.firstNameClient = firstNameClient;
    }

    public String getLastNameClient() {
        return lastNameClient;
    }

    public void setLastNameClient(String lastNameClient) {
        this.lastNameClient = lastNameClient;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
