/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_BI_SM_RECEPTION_LINE_BY_DAY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VBiSmReceptionLineByDay.findAll", query = "SELECT v FROM VBiSmReceptionLineByDay v")
    ,
    @NamedQuery(name = "VBiSmReceptionLineByDay.findByPk", query = "SELECT v FROM VBiSmReceptionLineByDay v WHERE v.pk = :pk")
    ,
    @NamedQuery(name = "VBiSmReceptionLineByDay.findByCltModuleId", query = "SELECT v FROM VBiSmReceptionLineByDay v WHERE v.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "VBiSmReceptionLineByDay.findByOperationDate", query = "SELECT v FROM VBiSmReceptionLineByDay v WHERE v.operationDate = :operationDate")
    ,
    @NamedQuery(name = "VBiSmReceptionLineByDay.findByDay", query = "SELECT v FROM VBiSmReceptionLineByDay v WHERE v.day = :day")
    ,
    @NamedQuery(name = "VBiSmReceptionLineByDay.findByMonth", query = "SELECT v FROM VBiSmReceptionLineByDay v WHERE v.month = :month")
    ,
    @NamedQuery(name = "VBiSmReceptionLineByDay.findByYear", query = "SELECT v FROM VBiSmReceptionLineByDay v WHERE v.year = :year")
    ,
    @NamedQuery(name = "VBiSmReceptionLineByDay.findBySumPrice", query = "SELECT v FROM VBiSmReceptionLineByDay v WHERE v.sumPrice = :sumPrice")
    ,
    @NamedQuery(name = "VBiSmReceptionLineByDay.findBySumQuantity", query = "SELECT v FROM VBiSmReceptionLineByDay v WHERE v.sumQuantity = :sumQuantity")})
public class VBiSmReceptionLineByDay implements Serializable {

    public static final String findByCltModuleId = "SELECT v FROM VBiSmReceptionLineByDay v WHERE v.cltModuleId = :cltModuleId";
    public static final String findByCltModuleIdAndMonthAndYear = "SELECT v FROM VBiSmReceptionLineByDay v WHERE v.month = :month and v.year = :year and v.cltModuleId = :cltModuleId";

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    @Column(name = "OPERATION_DATE")
    private String operationDate;

    @Column(name = "DAY")
    private Long day;

    @Column(name = "MONTH")
    private Long month;

    @Column(name = "YEAR")
    private Long year;

    @Column(name = "SUM_PRICE")
    private Double sumPrice;

    @Column(name = "SUM_QUANTITY")
    private Long sumQuantity;

    public VBiSmReceptionLineByDay() {
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public String getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(String operationDate) {
        this.operationDate = operationDate;
    }

    public Long getDay() {
        return day;
    }

    public void setDay(Long day) {
        this.day = day;
    }

    public Long getMonth() {
        return month;
    }

    public void setMonth(Long month) {
        this.month = month;
    }

    public Long getYear() {
        return year;
    }

    public void setYear(Long year) {
        this.year = year;
    }

    public Double getSumPrice() {
        return sumPrice;
    }

    public void setSumPrice(Double sumPrice) {
        this.sumPrice = sumPrice;
    }

    public Long getSumQuantity() {
        return sumQuantity;
    }

    public void setSumQuantity(Long sumQuantity) {
        this.sumQuantity = sumQuantity;
    }

}
