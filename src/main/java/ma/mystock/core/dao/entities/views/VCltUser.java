/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_CLT_USER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCltUser.findAll", query = "SELECT v FROM VCltUser v")
    ,
    @NamedQuery(name = "VCltUser.findById", query = "SELECT v FROM VCltUser v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VCltUser.findByFirstName", query = "SELECT v FROM VCltUser v WHERE v.firstName = :firstName")
    ,
    @NamedQuery(name = "VCltUser.findByLastName", query = "SELECT v FROM VCltUser v WHERE v.lastName = :lastName")
    ,
    @NamedQuery(name = "VCltUser.findByUsername", query = "SELECT v FROM VCltUser v WHERE v.username = :username")
    ,
    @NamedQuery(name = "VCltUser.findByPassword", query = "SELECT v FROM VCltUser v WHERE v.password = :password")
    ,
    @NamedQuery(name = "VCltUser.findByCategoryId", query = "SELECT v FROM VCltUser v WHERE v.categoryId = :categoryId")
    ,
    @NamedQuery(name = "VCltUser.findByClientId", query = "SELECT v FROM VCltUser v WHERE v.clientId = :clientId")
    ,
    @NamedQuery(name = "VCltUser.findByUserStatusId", query = "SELECT v FROM VCltUser v WHERE v.userStatusId = :userStatusId")
    ,
    @NamedQuery(name = "VCltUser.findByActive", query = "SELECT v FROM VCltUser v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VCltUser.findByDateCreation", query = "SELECT v FROM VCltUser v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VCltUser.findByUserCreation", query = "SELECT v FROM VCltUser v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VCltUser.findByDateUpdate", query = "SELECT v FROM VCltUser v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VCltUser.findByUserUpdate", query = "SELECT v FROM VCltUser v WHERE v.userUpdate = :userUpdate")})
public class VCltUser implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findById = "SELECT v FROM VCltUser v WHERE v.id = :id";
    public static final String findByClientId = "SELECT v FROM VCltUser v WHERE v.clientId = :clientId";

    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "CATEGORY_ID")
    private Long categoryId;

    @Column(name = "CLIENT_ID")
    private Long clientId;

    @Column(name = "USER_STATUS_ID")
    private Long userStatusId;

    @Column(name = "ACTIVE")
    private String active;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "ENTITY_ID")
    private Long entityId;

    @Column(name = "ENTITY_PHONE")
    private String entityPhone;

    @Column(name = "ENTITY_EMAIL")
    private String entityEmail;

    @Column(name = "ENTITY_FAX")
    private String entityFax;

    @Column(name = "ENTITY_LOCATION_ADDRESS")
    private String entityLocationAddress;

    @Column(name = "ENTITY_LOCATION_CITY_NAME")
    private String entityLocationCityName;

    @Column(name = "ENTITY_LOCATION_COUNTRY_NAME")
    private String entityLocationCountryName;

    @Column(name = "ENTITY_WEB")
    private String entityWeb;

    @Column(name = "CATEGORY_NAME")
    private String categoryName;

    public VCltUser() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getUserStatusId() {
        return userStatusId;
    }

    public void setUserStatusId(Long userStatusId) {
        this.userStatusId = userStatusId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getEntityPhone() {
        return entityPhone;
    }

    public void setEntityPhone(String entityPhone) {
        this.entityPhone = entityPhone;
    }

    public String getEntityEmail() {
        return entityEmail;
    }

    public void setEntityEmail(String entityEmail) {
        this.entityEmail = entityEmail;
    }

    public String getEntityFax() {
        return entityFax;
    }

    public void setEntityFax(String entityFax) {
        this.entityFax = entityFax;
    }

    public String getEntityLocationAddress() {
        return entityLocationAddress;
    }

    public void setEntityLocationAddress(String entityLocationAddress) {
        this.entityLocationAddress = entityLocationAddress;
    }

    public String getEntityLocationCityName() {
        return entityLocationCityName;
    }

    public void setEntityLocationCityName(String entityLocationCityName) {
        this.entityLocationCityName = entityLocationCityName;
    }

    public String getEntityLocationCountryName() {
        return entityLocationCountryName;
    }

    public void setEntityLocationCountryName(String entityLocationCountryName) {
        this.entityLocationCountryName = entityLocationCountryName;
    }

    public String getEntityWeb() {
        return entityWeb;
    }

    public void setEntityWeb(String entityWeb) {
        this.entityWeb = entityWeb;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

}
