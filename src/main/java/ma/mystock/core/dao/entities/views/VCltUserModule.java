/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_CLT_USER_MODULE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCltUserModule.findAll", query = "SELECT v FROM VCltUserModule v")
    ,
    @NamedQuery(name = "VCltUserModule.findById", query = "SELECT v FROM VCltUserModule v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VCltUserModule.findByUserId", query = "SELECT v FROM VCltUserModule v WHERE v.userId = :userId")
    ,
    @NamedQuery(name = "VCltUserModule.findByModuleId", query = "SELECT v FROM VCltUserModule v WHERE v.moduleId = :moduleId")
})
public class VCltUserModule implements Serializable {

    private static final Long serialVersionUID = 1L;
    public static String findByUserId = "SELECT v FROM VCltUserModule v WHERE v.userId = :userId";
    public static String findByUserIdAndModuleTypeId = "SELECT v FROM VCltUserModule v WHERE v.userId = :userId and v.moduleTypeId = :moduleTypeId";

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "USER_ID")
    private Long userId;

    @Column(name = "MODULE_ID")
    private Long moduleId;

    @Column(name = "MODULE_TYPE_ID")
    private Long moduleTypeId;

    @Column(name = "MODULE_ID_TO_MANAGER")
    private Long moduleIdToManager;

    @Column(name = "MODULE_NAME")
    private String moduleName;

    @Column(name = "MODULE_IMAGE_PATH")
    private String moduleImagePath;

    public VCltUserModule() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

    public Long getModuleTypeId() {
        return moduleTypeId;
    }

    public void setModuleTypeId(Long moduleTypeId) {
        this.moduleTypeId = moduleTypeId;
    }

    public Long getModuleIdToManager() {
        return moduleIdToManager;
    }

    public void setModuleIdToManager(Long moduleIdToManager) {
        this.moduleIdToManager = moduleIdToManager;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getModuleImagePath() {
        return moduleImagePath;
    }

    public void setModuleImagePath(String moduleImagePath) {
        this.moduleImagePath = moduleImagePath;
    }

}
