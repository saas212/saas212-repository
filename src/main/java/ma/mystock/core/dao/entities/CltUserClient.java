/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Yamine Abdelmounaim <yaminea@evision.ca>
 */
@Entity
@Table(name = "CLT_USER_CLIENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CltUserClient.findAll", query = "SELECT c FROM CltUserClient c")
    ,
    @NamedQuery(name = "CltUserClient.findById", query = "SELECT c FROM CltUserClient c WHERE c.id = :id")
    ,
    @NamedQuery(name = "CltUserClient.findByActive", query = "SELECT c FROM CltUserClient c WHERE c.active = :active")
    ,
    @NamedQuery(name = "CltUserClient.findByDateCreation", query = "SELECT c FROM CltUserClient c WHERE c.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "CltUserClient.findByUserCreation", query = "SELECT c FROM CltUserClient c WHERE c.userCreation = :userCreation")
    ,
    @NamedQuery(name = "CltUserClient.findByDateUpdate", query = "SELECT c FROM CltUserClient c WHERE c.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "CltUserClient.findByUserUpdate", query = "SELECT c FROM CltUserClient c WHERE c.userUpdate = :userUpdate")})
public class CltUserClient implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String findById = "SELECT c FROM CltUserClient c WHERE c.id = :id";
    public static final String findAllActive = "SELECT c FROM CltUserClient c WHERE c.active = :active";
    public static final String findByClientAndActive = "SELECT c FROM CltUserClient c where c.userId = :userId and c.active = :active";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "USER_ID")
    private Long userId;
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne
    private CltUser cltUser;
    @Column(name = "CLIENT_ID")
    private Long clinetId;
    @JoinColumn(name = "CLINET_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne
    private CltClient cltClient;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public CltUserClient() {
    }

    public CltUserClient(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CltUser getCltUser() {
        return cltUser;
    }

    public void setCltUser(CltUser cltUser) {
        this.cltUser = cltUser;
    }

    public CltClient getCltClient() {
        return cltClient;
    }

    public void setCltClient(CltClient cltClient) {
        this.cltClient = cltClient;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getClinetId() {
        return clinetId;
    }

    public void setClinetId(Long clinetId) {
        this.clinetId = clinetId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CltUserClient)) {
            return false;
        }
        CltUserClient other = (CltUserClient) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entitys.CltUserClient[ id=" + id + " ]";
    }

}
