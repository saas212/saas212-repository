/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_PM_COMPOS_DETAILS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VPmComposDetails.findAll", query = "SELECT v FROM VPmComposDetails v")
    ,
    @NamedQuery(name = "VPmComposDetails.findByCompositionId", query = "SELECT v FROM VPmComposDetails v WHERE v.compositionId = :compositionId")
    ,
    @NamedQuery(name = "VPmComposDetails.findByCltModuleId", query = "SELECT v FROM VPmComposDetails v WHERE v.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "VPmComposDetails.findByCltModuleName", query = "SELECT v FROM VPmComposDetails v WHERE v.cltModuleName = :cltModuleName")
    ,
    @NamedQuery(name = "VPmComposDetails.findByGroupId", query = "SELECT v FROM VPmComposDetails v WHERE v.groupId = :groupId")
    ,
    @NamedQuery(name = "VPmComposDetails.findByGroupName", query = "SELECT v FROM VPmComposDetails v WHERE v.groupName = :groupName")
    ,
    @NamedQuery(name = "VPmComposDetails.findByCategoryId", query = "SELECT v FROM VPmComposDetails v WHERE v.categoryId = :categoryId")
    ,
    @NamedQuery(name = "VPmComposDetails.findByCategoryName", query = "SELECT v FROM VPmComposDetails v WHERE v.categoryName = :categoryName")
    ,
    @NamedQuery(name = "VPmComposDetails.findByMenuId", query = "SELECT v FROM VPmComposDetails v WHERE v.menuId = :menuId")
    ,
    @NamedQuery(name = "VPmComposDetails.findByMenuName", query = "SELECT v FROM VPmComposDetails v WHERE v.menuName = :menuName")
    ,
    @NamedQuery(name = "VPmComposDetails.findByPageId", query = "SELECT v FROM VPmComposDetails v WHERE v.pageId = :pageId")
    ,
    @NamedQuery(name = "VPmComposDetails.findByPageName", query = "SELECT v FROM VPmComposDetails v WHERE v.pageName = :pageName")})
public class VPmComposDetails implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findByCltModuleId = "SELECT v FROM VPmComposDetails v WHERE v.cltModuleId = :cltModuleId";
    public static final String findByCltModuleIdAndGroupId = "SELECT v FROM VPmComposDetails v WHERE v.cltModuleId = :cltModuleId and v.groupId = :groupId";

    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "COMPOSITION_ID")
    private Long compositionId;
    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    @Column(name = "CLT_MODULE_NAME")
    private String cltModuleName;
    @Column(name = "GROUP_ID")
    private Long groupId;

    @Column(name = "GROUP_NAME")
    private String groupName;
    @Column(name = "CATEGORY_ID")
    private Long categoryId;

    @Column(name = "CATEGORY_NAME")
    private String categoryName;
    @Column(name = "MENU_ID")
    private Long menuId;

    @Column(name = "MENU_NAME")
    private String menuName;
    @Column(name = "PAGE_ID")
    private Long pageId;

    @Column(name = "PAGE_NAME")
    private String pageName;

    public VPmComposDetails() {
    }

    public Long getCompositionId() {
        return compositionId;
    }

    public void setCompositionId(Long compositionId) {
        this.compositionId = compositionId;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public String getCltModuleName() {
        return cltModuleName;
    }

    public void setCltModuleName(String cltModuleName) {
        this.cltModuleName = cltModuleName;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public Long getPageId() {
        return pageId;
    }

    public void setPageId(Long pageId) {
        this.pageId = pageId;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

}
