/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_SM_RETURN_RECEIPT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmReturnReceipt.findAll", query = "SELECT v FROM VSmReturnReceipt v")
    ,
    @NamedQuery(name = "VSmReturnReceipt.findById", query = "SELECT v FROM VSmReturnReceipt v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VSmReturnReceipt.findByNoSeq", query = "SELECT v FROM VSmReturnReceipt v WHERE v.noSeq = :noSeq")
    ,
    @NamedQuery(name = "VSmReturnReceipt.findByReference", query = "SELECT v FROM VSmReturnReceipt v WHERE v.reference = :reference")
    ,
    @NamedQuery(name = "VSmReturnReceipt.findByCustomerId", query = "SELECT v FROM VSmReturnReceipt v WHERE v.customerId = :customerId")
    ,
    @NamedQuery(name = "VSmReturnReceipt.findByOrderId", query = "SELECT v FROM VSmReturnReceipt v WHERE v.orderId = :orderId")
    ,
    @NamedQuery(name = "VSmReturnReceipt.findByReturnReceiptStatusId", query = "SELECT v FROM VSmReturnReceipt v WHERE v.returnReceiptStatusId = :returnReceiptStatusId")
    ,
    @NamedQuery(name = "VSmReturnReceipt.findByActive", query = "SELECT v FROM VSmReturnReceipt v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VSmReturnReceipt.findByCltModuleId", query = "SELECT v FROM VSmReturnReceipt v WHERE v.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "VSmReturnReceipt.findByDateCreation", query = "SELECT v FROM VSmReturnReceipt v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VSmReturnReceipt.findByUserCreation", query = "SELECT v FROM VSmReturnReceipt v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VSmReturnReceipt.findByDateUpdate", query = "SELECT v FROM VSmReturnReceipt v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VSmReturnReceipt.findByUserUpdate", query = "SELECT v FROM VSmReturnReceipt v WHERE v.userUpdate = :userUpdate")
    ,
    @NamedQuery(name = "VSmReturnReceipt.findByCustomerCompanyName", query = "SELECT v FROM VSmReturnReceipt v WHERE v.customerCompanyName = :customerCompanyName")
    ,
    @NamedQuery(name = "VSmReturnReceipt.findByTotalHorsTaxe", query = "SELECT v FROM VSmReturnReceipt v WHERE v.totalHorsTaxe = :totalHorsTaxe")
    ,
    @NamedQuery(name = "VSmReturnReceipt.findByTotalTtc", query = "SELECT v FROM VSmReturnReceipt v WHERE v.totalTtc = :totalTtc")
    ,
    @NamedQuery(name = "VSmReturnReceipt.findBySumProduct", query = "SELECT v FROM VSmReturnReceipt v WHERE v.sumProduct = :sumProduct")
    ,
    @NamedQuery(name = "VSmReturnReceipt.findBySmReturnReceiptStatusName", query = "SELECT v FROM VSmReturnReceipt v WHERE v.smReturnReceiptStatusName = :smReturnReceiptStatusName")})
public class VSmReturnReceipt implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findByCltModuleIdAndReturnReceiptStatusId = "SELECT v FROM VSmReturnReceipt v WHERE v.cltModuleId = :cltModuleId and v.returnReceiptStatusId = :returnReceiptStatusId";

    public static final String findMaxNoSeqByCltModuleId = "SELECT max(v.noSeq) FROM VSmReturnReceipt v WHERE v.cltModuleId = :cltModuleId";

    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;
    @Column(name = "NO_SEQ")
    private Long noSeq;

    @Column(name = "REFERENCE")
    private String reference;
    @Column(name = "CUSTOMER_ID")
    private Long customerId;
    @Column(name = "ORDER_ID")
    private Long orderId;
    @Column(name = "RETURN_RECEIPT_STATUS_ID")
    private Long returnReceiptStatusId;

    @Column(name = "NOTE")
    private String note;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "CUSTOMER_COMPANY_NAME")
    private String customerCompanyName;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "TOTAL_HORS_TAXE")
    private Double totalHorsTaxe;
    @Column(name = "TOTAL_TTC")
    private Double totalTtc;
    @Column(name = "SUM_PRODUCT")
    private Long sumProduct;

    @Column(name = "SM_RETURN_RECEIPT_STATUS_NAME")
    private String smReturnReceiptStatusName;

    public VSmReturnReceipt() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNoSeq() {
        return noSeq;
    }

    public void setNoSeq(Long noSeq) {
        this.noSeq = noSeq;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getReturnReceiptStatusId() {
        return returnReceiptStatusId;
    }

    public void setReturnReceiptStatusId(Long returnReceiptStatusId) {
        this.returnReceiptStatusId = returnReceiptStatusId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getCustomerCompanyName() {
        return customerCompanyName;
    }

    public void setCustomerCompanyName(String customerCompanyName) {
        this.customerCompanyName = customerCompanyName;
    }

    public Double getTotalHorsTaxe() {
        return totalHorsTaxe;
    }

    public void setTotalHorsTaxe(Double totalHorsTaxe) {
        this.totalHorsTaxe = totalHorsTaxe;
    }

    public Double getTotalTtc() {
        return totalTtc;
    }

    public void setTotalTtc(Double totalTtc) {
        this.totalTtc = totalTtc;
    }

    public Long getSumProduct() {
        return sumProduct;
    }

    public void setSumProduct(Long sumProduct) {
        this.sumProduct = sumProduct;
    }

    public String getSmReturnReceiptStatusName() {
        return smReturnReceiptStatusName;
    }

    public void setSmReturnReceiptStatusName(String smReturnReceiptStatusName) {
        this.smReturnReceiptStatusName = smReturnReceiptStatusName;
    }

}
