/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "INF_COUNTRY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InfCountry.findAll", query = "SELECT i FROM InfCountry i")
    ,
    @NamedQuery(name = "InfCountry.findById", query = "SELECT i FROM InfCountry i WHERE i.id = :id")
    ,
    @NamedQuery(name = "InfCountry.findByCode", query = "SELECT i FROM InfCountry i WHERE i.code = :code")
    ,
    @NamedQuery(name = "InfCountry.findByName", query = "SELECT i FROM InfCountry i WHERE i.name = :name")
    ,
    @NamedQuery(name = "InfCountry.findBySortKey", query = "SELECT i FROM InfCountry i WHERE i.sortKey = :sortKey")
    ,
    @NamedQuery(name = "InfCountry.findByActive", query = "SELECT i FROM InfCountry i WHERE i.active = :active")
    ,
    @NamedQuery(name = "InfCountry.findByDateCreation", query = "SELECT i FROM InfCountry i WHERE i.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "InfCountry.findByUserCreation", query = "SELECT i FROM InfCountry i WHERE i.userCreation = :userCreation")
    ,
    @NamedQuery(name = "InfCountry.findByDateUpdate", query = "SELECT i FROM InfCountry i WHERE i.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "InfCountry.findByUserUpdate", query = "SELECT i FROM InfCountry i WHERE i.userUpdate = :userUpdate")})
public class InfCountry implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findById = "SELECT i FROM InfCountry i WHERE i.id = :id";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Size(max = 255)
    @Column(name = "CODE")
    private String code;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Column(name = "SORT_KEY")
    private Long sortKey;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    //@OneToMany(mappedBy = "countryId")
    //private List<InfCity> infCityList;
    //@Column(name = "CLT_MODULE_ID")
    //private Long cltModuleId;
    public InfCountry() {
    }

    public InfCountry(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    /*
     @XmlTransient
     public List<InfCity> getInfCityList() {
     return infCityList;
     }

     public void setInfCityList(List<InfCity> infCityList) {
     this.infCityList = infCityList;
     }
    
     */
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InfCountry)) {
            return false;
        }
        InfCountry other = (InfCountry) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entitys.InfCountry[ id=" + id + " ]";
    }
    /*
     public Long getCltModuleId() {
     return cltModuleId;
     }

     public void setCltModuleId(Long cltModuleId) {
     this.cltModuleId = cltModuleId;
     }
     */

}
