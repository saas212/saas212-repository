/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_CTA_ENTITY_LOCATION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCtaEntityLocation.findAll", query = "SELECT v FROM VCtaEntityLocation v")
    ,
    @NamedQuery(name = "VCtaEntityLocation.findById", query = "SELECT v FROM VCtaEntityLocation v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VCtaEntityLocation.findByAddressLine1", query = "SELECT v FROM VCtaEntityLocation v WHERE v.addressLine1 = :addressLine1")
    ,
    @NamedQuery(name = "VCtaEntityLocation.findByAddressLine2", query = "SELECT v FROM VCtaEntityLocation v WHERE v.addressLine2 = :addressLine2")
    ,
    @NamedQuery(name = "VCtaEntityLocation.findByAddressLine3", query = "SELECT v FROM VCtaEntityLocation v WHERE v.addressLine3 = :addressLine3")
    ,
    @NamedQuery(name = "VCtaEntityLocation.findByInfCountryId", query = "SELECT v FROM VCtaEntityLocation v WHERE v.infCountryId = :infCountryId")
    ,
    @NamedQuery(name = "VCtaEntityLocation.findByInfCityId", query = "SELECT v FROM VCtaEntityLocation v WHERE v.infCityId = :infCityId")
    ,
    @NamedQuery(name = "VCtaEntityLocation.findByPostalCode", query = "SELECT v FROM VCtaEntityLocation v WHERE v.postalCode = :postalCode")
    ,
    @NamedQuery(name = "VCtaEntityLocation.findByLocationTypeId", query = "SELECT v FROM VCtaEntityLocation v WHERE v.locationTypeId = :locationTypeId")
    ,
    @NamedQuery(name = "VCtaEntityLocation.findByDateCreation", query = "SELECT v FROM VCtaEntityLocation v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VCtaEntityLocation.findByUserCreation", query = "SELECT v FROM VCtaEntityLocation v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VCtaEntityLocation.findByDateUpdate", query = "SELECT v FROM VCtaEntityLocation v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VCtaEntityLocation.findByUserUpdate", query = "SELECT v FROM VCtaEntityLocation v WHERE v.userUpdate = :userUpdate")
    ,
    @NamedQuery(name = "VCtaEntityLocation.findByLocationTypeName", query = "SELECT v FROM VCtaEntityLocation v WHERE v.locationTypeName = :locationTypeName")})
public class VCtaEntityLocation implements Serializable {

    public static final String findByEntityId = "SELECT v FROM VCtaEntityLocation v WHERE v.entityId = :entityId";

    private static final Long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "ADDRESS_LINE_1")
    private String addressLine1;

    @Column(name = "ADDRESS_LINE_2")
    private String addressLine2;

    @Column(name = "ADDRESS_LINE_3")
    private String addressLine3;

    @Column(name = "INF_COUNTRY_ID")
    private Long infCountryId;

    @Column(name = "INF_CITY_ID")
    private Long infCityId;

    @Column(name = "POSTAL_CODE")
    private String postalCode;

    @Column(name = "PRIORITY")
    private Long priority;

    @Column(name = "ENTITY_ID")
    private Long entityId;

    @Column(name = "LOCATION_TYPE_ID")
    private Long locationTypeId;

    @Column(name = "LOCATION_TYPE_NAME")
    private String locationTypeName;

    @Column(name = "INF_COUNTRY_NAME")
    private String infCountryName;

    @Column(name = "INF_CITY_NAME")
    private String infCityName;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public VCtaEntityLocation() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3() {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    public Long getInfCountryId() {
        return infCountryId;
    }

    public void setInfCountryId(Long infCountryId) {
        this.infCountryId = infCountryId;
    }

    public Long getInfCityId() {
        return infCityId;
    }

    public void setInfCityId(Long infCityId) {
        this.infCityId = infCityId;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Long getLocationTypeId() {
        return locationTypeId;
    }

    public void setLocationTypeId(Long locationTypeId) {
        this.locationTypeId = locationTypeId;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getLocationTypeName() {
        return locationTypeName;
    }

    public void setLocationTypeName(String locationTypeName) {
        this.locationTypeName = locationTypeName;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    public String getInfCountryName() {
        return infCountryName;
    }

    public void setInfCountryName(String infCountryName) {
        this.infCountryName = infCountryName;
    }

    public String getInfCityName() {
        return infCityName;
    }

    public void setInfCityName(String infCityName) {
        this.infCityName = infCityName;
    }

}
