package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_SM_ORDER_DETAILS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmOrderDetails.findAll", query = "SELECT v FROM VSmOrderDetails v")
    ,
    @NamedQuery(name = "VSmOrderDetails.findByOrderSupplierInProgressCount", query = "SELECT v FROM VSmOrderDetails v WHERE v.orderSupplierInProgressCount = :orderSupplierInProgressCount")
    ,
    @NamedQuery(name = "VSmOrderDetails.findByOrderSupplierTransmittedCount", query = "SELECT v FROM VSmOrderDetails v WHERE v.orderSupplierTransmittedCount = :orderSupplierTransmittedCount")
    ,
    @NamedQuery(name = "VSmOrderDetails.findByOrderSupplierValidatedCount", query = "SELECT v FROM VSmOrderDetails v WHERE v.orderSupplierValidatedCount = :orderSupplierValidatedCount")
    ,
    @NamedQuery(name = "VSmOrderDetails.findByOrderSupplierRefusedCount", query = "SELECT v FROM VSmOrderDetails v WHERE v.orderSupplierRefusedCount = :orderSupplierRefusedCount")
    ,
    @NamedQuery(name = "VSmOrderDetails.findByOrderSupplierRejectedCount", query = "SELECT v FROM VSmOrderDetails v WHERE v.orderSupplierRejectedCount = :orderSupplierRejectedCount")
    ,
    @NamedQuery(name = "VSmOrderDetails.findByReceptionInProgressCount", query = "SELECT v FROM VSmOrderDetails v WHERE v.receptionInProgressCount = :receptionInProgressCount")
    ,
    @NamedQuery(name = "VSmOrderDetails.findByReceptionTransmittedCount", query = "SELECT v FROM VSmOrderDetails v WHERE v.receptionTransmittedCount = :receptionTransmittedCount")
    ,
    @NamedQuery(name = "VSmOrderDetails.findByReceptionValidatedCount", query = "SELECT v FROM VSmOrderDetails v WHERE v.receptionValidatedCount = :receptionValidatedCount")
    ,
    @NamedQuery(name = "VSmOrderDetails.findByReceptionRefusedCount", query = "SELECT v FROM VSmOrderDetails v WHERE v.receptionRefusedCount = :receptionRefusedCount")
    ,
    @NamedQuery(name = "VSmOrderDetails.findByReceptionRejectedCount", query = "SELECT v FROM VSmOrderDetails v WHERE v.receptionRejectedCount = :receptionRejectedCount")
    ,
    @NamedQuery(name = "VSmOrderDetails.findByOrderInProgressCount", query = "SELECT v FROM VSmOrderDetails v WHERE v.orderInProgressCount = :orderInProgressCount")
    ,
    @NamedQuery(name = "VSmOrderDetails.findByOrderTransmittedCount", query = "SELECT v FROM VSmOrderDetails v WHERE v.orderTransmittedCount = :orderTransmittedCount")
    ,
    @NamedQuery(name = "VSmOrderDetails.findByOrderValidatedCount", query = "SELECT v FROM VSmOrderDetails v WHERE v.orderValidatedCount = :orderValidatedCount")
    ,
    @NamedQuery(name = "VSmOrderDetails.findByOrderdRefusedCount", query = "SELECT v FROM VSmOrderDetails v WHERE v.orderdRefusedCount = :orderdRefusedCount")
    ,
    @NamedQuery(name = "VSmOrderDetails.findByOrderdRejectedCount", query = "SELECT v FROM VSmOrderDetails v WHERE v.orderdRejectedCount = :orderdRejectedCount")
    ,
    @NamedQuery(name = "VSmOrderDetails.findByReturnReceiptInProgressCount", query = "SELECT v FROM VSmOrderDetails v WHERE v.returnReceiptInProgressCount = :returnReceiptInProgressCount")
    ,
    @NamedQuery(name = "VSmOrderDetails.findByReturnReceiptTransmittedCount", query = "SELECT v FROM VSmOrderDetails v WHERE v.returnReceiptTransmittedCount = :returnReceiptTransmittedCount")
    ,
    @NamedQuery(name = "VSmOrderDetails.findByReturnReceiptValidatedCount", query = "SELECT v FROM VSmOrderDetails v WHERE v.returnReceiptValidatedCount = :returnReceiptValidatedCount")
    ,
    @NamedQuery(name = "VSmOrderDetails.findByReturnReceiptRefusedCount", query = "SELECT v FROM VSmOrderDetails v WHERE v.returnReceiptRefusedCount = :returnReceiptRefusedCount")
    ,
    @NamedQuery(name = "VSmOrderDetails.findByReturnReceiptRejectedCount", query = "SELECT v FROM VSmOrderDetails v WHERE v.returnReceiptRejectedCount = :returnReceiptRejectedCount")})
public class VSmOrderDetails implements Serializable {

    public static final String findByCltModuleId = "SELECT v FROM VSmOrderDetails v WHERE v.cltModuleId = :cltModuleId";

    private static final Long serialVersionUID = 1L;

    @Id
    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;
    @Column(name = "ORDER_SUPPLIER_IN_PROGRESS_COUNT")
    private Long orderSupplierInProgressCount;
    @Column(name = "ORDER_SUPPLIER_TRANSMITTED_COUNT")
    private Long orderSupplierTransmittedCount;
    @Column(name = "ORDER_SUPPLIER_VALIDATED_COUNT")
    private Long orderSupplierValidatedCount;
    @Column(name = "ORDER_SUPPLIER_REFUSED_COUNT")
    private Long orderSupplierRefusedCount;
    @Column(name = "ORDER_SUPPLIER_REJECTED_COUNT")
    private Long orderSupplierRejectedCount;
    @Column(name = "RECEPTION_IN_PROGRESS_COUNT")
    private Long receptionInProgressCount;
    @Column(name = "RECEPTION_TRANSMITTED_COUNT")
    private Long receptionTransmittedCount;
    @Column(name = "RECEPTION_VALIDATED_COUNT")
    private Long receptionValidatedCount;
    @Column(name = "RECEPTION_REFUSED_COUNT")
    private Long receptionRefusedCount;
    @Column(name = "RECEPTION_REJECTED_COUNT")
    private Long receptionRejectedCount;
    @Column(name = "ORDER_IN_PROGRESS_COUNT")
    private Long orderInProgressCount;
    @Column(name = "ORDER_TRANSMITTED_COUNT")
    private Long orderTransmittedCount;
    @Column(name = "ORDER_VALIDATED_COUNT")
    private Long orderValidatedCount;
    @Column(name = "ORDERD_REFUSED_COUNT")
    private Long orderdRefusedCount;
    @Column(name = "ORDERD_REJECTED_COUNT")
    private Long orderdRejectedCount;
    @Column(name = "RETURN_RECEIPT_IN_PROGRESS_COUNT")
    private Long returnReceiptInProgressCount;
    @Column(name = "RETURN_RECEIPT_TRANSMITTED_COUNT")
    private Long returnReceiptTransmittedCount;
    @Column(name = "RETURN_RECEIPT_VALIDATED_COUNT")
    private Long returnReceiptValidatedCount;
    @Column(name = "RETURN_RECEIPT_REFUSED_COUNT")
    private Long returnReceiptRefusedCount;
    @Column(name = "RETURN_RECEIPT_REJECTED_COUNT")
    private Long returnReceiptRejectedCount;

    public VSmOrderDetails() {
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public Long getOrderSupplierInProgressCount() {
        return orderSupplierInProgressCount;
    }

    public void setOrderSupplierInProgressCount(Long orderSupplierInProgressCount) {
        this.orderSupplierInProgressCount = orderSupplierInProgressCount;
    }

    public Long getOrderSupplierTransmittedCount() {
        return orderSupplierTransmittedCount;
    }

    public void setOrderSupplierTransmittedCount(Long orderSupplierTransmittedCount) {
        this.orderSupplierTransmittedCount = orderSupplierTransmittedCount;
    }

    public Long getOrderSupplierValidatedCount() {
        return orderSupplierValidatedCount;
    }

    public void setOrderSupplierValidatedCount(Long orderSupplierValidatedCount) {
        this.orderSupplierValidatedCount = orderSupplierValidatedCount;
    }

    public Long getOrderSupplierRefusedCount() {
        return orderSupplierRefusedCount;
    }

    public void setOrderSupplierRefusedCount(Long orderSupplierRefusedCount) {
        this.orderSupplierRefusedCount = orderSupplierRefusedCount;
    }

    public Long getOrderSupplierRejectedCount() {
        return orderSupplierRejectedCount;
    }

    public void setOrderSupplierRejectedCount(Long orderSupplierRejectedCount) {
        this.orderSupplierRejectedCount = orderSupplierRejectedCount;
    }

    public Long getReceptionInProgressCount() {
        return receptionInProgressCount;
    }

    public void setReceptionInProgressCount(Long receptionInProgressCount) {
        this.receptionInProgressCount = receptionInProgressCount;
    }

    public Long getReceptionTransmittedCount() {
        return receptionTransmittedCount;
    }

    public void setReceptionTransmittedCount(Long receptionTransmittedCount) {
        this.receptionTransmittedCount = receptionTransmittedCount;
    }

    public Long getReceptionValidatedCount() {
        return receptionValidatedCount;
    }

    public void setReceptionValidatedCount(Long receptionValidatedCount) {
        this.receptionValidatedCount = receptionValidatedCount;
    }

    public Long getReceptionRefusedCount() {
        return receptionRefusedCount;
    }

    public void setReceptionRefusedCount(Long receptionRefusedCount) {
        this.receptionRefusedCount = receptionRefusedCount;
    }

    public Long getReceptionRejectedCount() {
        return receptionRejectedCount;
    }

    public void setReceptionRejectedCount(Long receptionRejectedCount) {
        this.receptionRejectedCount = receptionRejectedCount;
    }

    public Long getOrderInProgressCount() {
        return orderInProgressCount;
    }

    public void setOrderInProgressCount(Long orderInProgressCount) {
        this.orderInProgressCount = orderInProgressCount;
    }

    public Long getOrderTransmittedCount() {
        return orderTransmittedCount;
    }

    public void setOrderTransmittedCount(Long orderTransmittedCount) {
        this.orderTransmittedCount = orderTransmittedCount;
    }

    public Long getOrderValidatedCount() {
        return orderValidatedCount;
    }

    public void setOrderValidatedCount(Long orderValidatedCount) {
        this.orderValidatedCount = orderValidatedCount;
    }

    public Long getOrderdRefusedCount() {
        return orderdRefusedCount;
    }

    public void setOrderdRefusedCount(Long orderdRefusedCount) {
        this.orderdRefusedCount = orderdRefusedCount;
    }

    public Long getOrderdRejectedCount() {
        return orderdRejectedCount;
    }

    public void setOrderdRejectedCount(Long orderdRejectedCount) {
        this.orderdRejectedCount = orderdRejectedCount;
    }

    public Long getReturnReceiptInProgressCount() {
        return returnReceiptInProgressCount;
    }

    public void setReturnReceiptInProgressCount(Long returnReceiptInProgressCount) {
        this.returnReceiptInProgressCount = returnReceiptInProgressCount;
    }

    public Long getReturnReceiptTransmittedCount() {
        return returnReceiptTransmittedCount;
    }

    public void setReturnReceiptTransmittedCount(Long returnReceiptTransmittedCount) {
        this.returnReceiptTransmittedCount = returnReceiptTransmittedCount;
    }

    public Long getReturnReceiptValidatedCount() {
        return returnReceiptValidatedCount;
    }

    public void setReturnReceiptValidatedCount(Long returnReceiptValidatedCount) {
        this.returnReceiptValidatedCount = returnReceiptValidatedCount;
    }

    public Long getReturnReceiptRefusedCount() {
        return returnReceiptRefusedCount;
    }

    public void setReturnReceiptRefusedCount(Long returnReceiptRefusedCount) {
        this.returnReceiptRefusedCount = returnReceiptRefusedCount;
    }

    public Long getReturnReceiptRejectedCount() {
        return returnReceiptRejectedCount;
    }

    public void setReturnReceiptRejectedCount(Long returnReceiptRejectedCount) {
        this.returnReceiptRejectedCount = returnReceiptRejectedCount;
    }

}
