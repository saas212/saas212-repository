package ma.mystock.core.dao.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "CTA_WEB_TYPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CtaWebType.findAll", query = "SELECT c FROM CtaWebType c")
    ,
    @NamedQuery(name = "CtaWebType.findById", query = "SELECT c FROM CtaWebType c WHERE c.id = :id")
    ,
    @NamedQuery(name = "CtaWebType.findByName", query = "SELECT c FROM CtaWebType c WHERE c.name = :name")
    ,
    @NamedQuery(name = "CtaWebType.findBySortKey", query = "SELECT c FROM CtaWebType c WHERE c.sortKey = :sortKey")
    ,
    @NamedQuery(name = "CtaWebType.findByActive", query = "SELECT c FROM CtaWebType c WHERE c.active = :active")
    ,
    @NamedQuery(name = "CtaWebType.findByCltModuleId", query = "SELECT c FROM CtaWebType c WHERE c.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "CtaWebType.findByDateCreation", query = "SELECT c FROM CtaWebType c WHERE c.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "CtaWebType.findByUserCreation", query = "SELECT c FROM CtaWebType c WHERE c.userCreation = :userCreation")
    ,
    @NamedQuery(name = "CtaWebType.findByDateUpdate", query = "SELECT c FROM CtaWebType c WHERE c.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "CtaWebType.findByUserUpdate", query = "SELECT c FROM CtaWebType c WHERE c.userUpdate = :userUpdate")})
public class CtaWebType implements Serializable {

    private static final Long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "SORT_KEY")
    private Long sortKey;

    @Column(name = "ACTIVE")
    private Character active;

    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public CtaWebType() {
    }

    public CtaWebType(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CtaWebType)) {
            return false;
        }
        CtaWebType other = (CtaWebType) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entities.CtaWebType[ id=" + id + " ]";
    }

}
