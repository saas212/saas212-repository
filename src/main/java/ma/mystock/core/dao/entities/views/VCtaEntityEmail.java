package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_CTA_ENTITY_EMAIL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCtaEntityEmail.findAll", query = "SELECT v FROM VCtaEntityEmail v")
    ,
    @NamedQuery(name = "VCtaEntityEmail.findById", query = "SELECT v FROM VCtaEntityEmail v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VCtaEntityEmail.findByEmailAddress", query = "SELECT v FROM VCtaEntityEmail v WHERE v.emailAddress = :emailAddress")
    ,
    @NamedQuery(name = "VCtaEntityEmail.findByIsForNotification", query = "SELECT v FROM VCtaEntityEmail v WHERE v.isForNotification = :isForNotification")
    ,
    @NamedQuery(name = "VCtaEntityEmail.findByEntityId", query = "SELECT v FROM VCtaEntityEmail v WHERE v.entityId = :entityId")
    ,
    @NamedQuery(name = "VCtaEntityEmail.findByEmailTypeId", query = "SELECT v FROM VCtaEntityEmail v WHERE v.emailTypeId = :emailTypeId")
    ,
    @NamedQuery(name = "VCtaEntityEmail.findByDateCreation", query = "SELECT v FROM VCtaEntityEmail v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VCtaEntityEmail.findByUserCreation", query = "SELECT v FROM VCtaEntityEmail v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VCtaEntityEmail.findByDateUpdate", query = "SELECT v FROM VCtaEntityEmail v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VCtaEntityEmail.findByUserUpdate", query = "SELECT v FROM VCtaEntityEmail v WHERE v.userUpdate = :userUpdate")
    ,
    @NamedQuery(name = "VCtaEntityEmail.findByEmailTypeName", query = "SELECT v FROM VCtaEntityEmail v WHERE v.emailTypeName = :emailTypeName")})
public class VCtaEntityEmail implements Serializable {

    public static final String findByEntityId = "SELECT v FROM VCtaEntityEmail v WHERE v.entityId = :entityId";

    private static final Long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "EMAIL_ADDRESS")
    private String emailAddress;

    @Column(name = "IS_FOR_NOTIFICATION")
    private String isForNotification;

    @Column(name = "ENTITY_ID")
    private Long entityId;

    @Column(name = "EMAIL_TYPE_ID")
    private Long emailTypeId;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "EMAIL_TYPE_NAME")
    private String emailTypeName;

    @Column(name = "PRIORITY")
    private Long priority;

    public VCtaEntityEmail() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getIsForNotification() {
        return isForNotification;
    }

    public void setIsForNotification(String isForNotification) {
        this.isForNotification = isForNotification;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Long getEmailTypeId() {
        return emailTypeId;
    }

    public void setEmailTypeId(Long emailTypeId) {
        this.emailTypeId = emailTypeId;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getEmailTypeName() {
        return emailTypeName;
    }

    public void setEmailTypeName(String emailTypeName) {
        this.emailTypeName = emailTypeName;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

}
