/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import ma.mystock.core.dao.entities.CltClient;

/**
 *
 * @author Yamine Abdelmounaim <yaminea@evision.ca>
 */
@Entity
@Table(name = "V_CLT_MODULE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCltModule.findAll", query = "SELECT v FROM VCltModule v")
    ,
    @NamedQuery(name = "VCltModule.findById", query = "SELECT v FROM VCltModule v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VCltModule.findByClientId", query = "SELECT v FROM VCltModule v WHERE v.clientId = :clientId")
    ,
    @NamedQuery(name = "VCltModule.findByModuleName", query = "SELECT v FROM VCltModule v WHERE v.moduleName = :moduleName")
    ,
    @NamedQuery(name = "VCltModule.findByModuleTypeName", query = "SELECT v FROM VCltModule v WHERE v.moduleTypeName = :moduleTypeName")
    ,
    @NamedQuery(name = "VCltModule.findByActive", query = "SELECT v FROM VCltModule v WHERE v.active = :active")
})

public class VCltModule implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String findById = "SELECT v FROM VCltModule v WHERE v.id = :id";
    public static final String findByClientId = "SELECT v FROM VCltModule v WHERE v.clientId = :clientId";
    public static final String findByClientIdAndNotAssign = "SELECT v FROM VCltModule v WHERE v.active = :active and ((v.id = :id) or v.id not in (select cv.moduleId from VCltModuleParameterClient cv where  cv.clinetId = :clinetId ))";
    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "MODULE_NAME")
    private String moduleName;

    @Lob
    @Column(name = "MODULE_DESCRIPTION")
    private String moduleDescription;

    @Column(name = "MODULE_TYPE_NAME")
    private String moduleTypeName;

    @JoinColumn(name = "CLIENT_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne
    private CltClient client;

    @Column(name = "CLIENT_ID")
    private Long clientId;

    @Column(name = "ACTIVE")
    private String active;

    public VCltModule() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getModuleDescription() {
        return moduleDescription;
    }

    public void setModuleDescription(String moduleDescription) {
        this.moduleDescription = moduleDescription;
    }

    public String getModuleTypeName() {
        return moduleTypeName;
    }

    public void setModuleTypeName(String moduleTypeName) {
        this.moduleTypeName = moduleTypeName;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public CltClient getClient() {
        return client;
    }

    public void setClient(CltClient client) {
        this.client = client;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

}
