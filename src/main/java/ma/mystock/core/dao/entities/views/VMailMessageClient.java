/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_MAIL_MESSAGE_CLIENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VMailMessageClient.findAll", query = "SELECT v FROM VMailMessageClient v")
    ,
    @NamedQuery(name = "VMailMessageClient.findById", query = "SELECT v FROM VMailMessageClient v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VMailMessageClient.findBySubject", query = "SELECT v FROM VMailMessageClient v WHERE v.subject = :subject")
    ,
    @NamedQuery(name = "VMailMessageClient.findByContent", query = "SELECT v FROM VMailMessageClient v WHERE v.content = :content")
    ,
    @NamedQuery(name = "VMailMessageClient.findBySender", query = "SELECT v FROM VMailMessageClient v WHERE v.sender = :sender")
    ,
    @NamedQuery(name = "VMailMessageClient.findByActive", query = "SELECT v FROM VMailMessageClient v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VMailMessageClient.findByIsSendToSuperAdmin", query = "SELECT v FROM VMailMessageClient v WHERE v.isSendToSuperAdmin = :isSendToSuperAdmin")
    ,
    @NamedQuery(name = "VMailMessageClient.findByIsSendToCurrentUser", query = "SELECT v FROM VMailMessageClient v WHERE v.isSendToCurrentUser = :isSendToCurrentUser")
    ,
    @NamedQuery(name = "VMailMessageClient.findByMessageId", query = "SELECT v FROM VMailMessageClient v WHERE v.messageId = :messageId")
    ,
    @NamedQuery(name = "VMailMessageClient.findByInfClientId", query = "SELECT v FROM VMailMessageClient v WHERE v.infClientId = :infClientId")
    ,
    @NamedQuery(name = "VMailMessageClient.findByDateCreation", query = "SELECT v FROM VMailMessageClient v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VMailMessageClient.findByUserCreation", query = "SELECT v FROM VMailMessageClient v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VMailMessageClient.findByDateUpdate", query = "SELECT v FROM VMailMessageClient v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VMailMessageClient.findByUserUpdate", query = "SELECT v FROM VMailMessageClient v WHERE v.userUpdate = :userUpdate")})
public class VMailMessageClient implements Serializable {

    private static final Long serialVersionUID = 1L;

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @Id
    private Long id;

    @Column(name = "SUBJECT")
    private String subject;

    @Column(name = "CONTENT")
    private String content;

    @Column(name = "SENDER")
    private String sender;

    @Column(name = "ACTIVE")
    private String active;

    @Column(name = "IS_SEND_TO_SUPER_ADMIN")
    private String isSendToSuperAdmin;

    @Column(name = "IS_SEND_TO_CURRENT_USER")
    private String isSendToCurrentUser;

    @Column(name = "MESSAGE_ID")
    private Long messageId;

    @Column(name = "INF_CLIENT_ID")
    private Long infClientId;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public VMailMessageClient() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getIsSendToSuperAdmin() {
        return isSendToSuperAdmin;
    }

    public void setIsSendToSuperAdmin(String isSendToSuperAdmin) {
        this.isSendToSuperAdmin = isSendToSuperAdmin;
    }

    public String getIsSendToCurrentUser() {
        return isSendToCurrentUser;
    }

    public void setIsSendToCurrentUser(String isSendToCurrentUser) {
        this.isSendToCurrentUser = isSendToCurrentUser;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public Long getInfClientId() {
        return infClientId;
    }

    public void setInfClientId(Long infClientId) {
        this.infClientId = infClientId;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

}
