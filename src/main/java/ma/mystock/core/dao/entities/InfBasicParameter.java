/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdou
 */
@Entity
@Table(name = "INF_BASIC_PARAMETER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InfBasicParameter.findAll", query = "SELECT i FROM InfBasicParameter i")
    ,
    @NamedQuery(name = "InfBasicParameter.findById", query = "SELECT i FROM InfBasicParameter i WHERE i.id = :id")
    ,
    @NamedQuery(name = "InfBasicParameter.findByName", query = "SELECT i FROM InfBasicParameter i WHERE i.name = :name")
    ,
    @NamedQuery(name = "InfBasicParameter.findByBasicParameterTypeId", query = "SELECT i FROM InfBasicParameter i WHERE i.basicParameterTypeId = :basicParameterTypeId")
    ,
    @NamedQuery(name = "InfBasicParameter.findByActive", query = "SELECT i FROM InfBasicParameter i WHERE i.active = :active")
    ,
    @NamedQuery(name = "InfBasicParameter.findByDateCreation", query = "SELECT i FROM InfBasicParameter i WHERE i.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "InfBasicParameter.findByUserCreation", query = "SELECT i FROM InfBasicParameter i WHERE i.userCreation = :userCreation")
    ,
    @NamedQuery(name = "InfBasicParameter.findByDateUpdate", query = "SELECT i FROM InfBasicParameter i WHERE i.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "InfBasicParameter.findByUserUpdate", query = "SELECT i FROM InfBasicParameter i WHERE i.userUpdate = :userUpdate")})
public class InfBasicParameter implements Serializable {

    private static final Long serialVersionUID = 1L;
    public static final String findById = "SELECT v FROM InfBasicParameter v where v.id = :id";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "VALUE")
    private String value;

    @Column(name = "BASIC_PARAMETER_TYPE_ID")
    private Long basicParameterTypeId;

    @Column(name = "ACTIVE")
    private String active;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public InfBasicParameter() {
    }

    public InfBasicParameter(Long id) {
        this.id = id;
    }

    public InfBasicParameter(Long id, Long basicParameterTypeId) {
        this.id = id;
        this.basicParameterTypeId = basicParameterTypeId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getBasicParameterTypeId() {
        return basicParameterTypeId;
    }

    public void setBasicParameterTypeId(Long basicParameterTypeId) {
        this.basicParameterTypeId = basicParameterTypeId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InfBasicParameter)) {
            return false;
        }
        InfBasicParameter other = (InfBasicParameter) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entitys.InfBasicParameter[ id=" + id + " ]";
    }

}
