/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "SM_RETURN_RECEIPT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SmReturnReceipt.findAll", query = "SELECT s FROM SmReturnReceipt s")
    ,
    @NamedQuery(name = "SmReturnReceipt.findById", query = "SELECT s FROM SmReturnReceipt s WHERE s.id = :id")
    ,
    @NamedQuery(name = "SmReturnReceipt.findByNoSeq", query = "SELECT s FROM SmReturnReceipt s WHERE s.noSeq = :noSeq")
    ,
    @NamedQuery(name = "SmReturnReceipt.findByReference", query = "SELECT s FROM SmReturnReceipt s WHERE s.reference = :reference")
    ,
    @NamedQuery(name = "SmReturnReceipt.findByCustomerId", query = "SELECT s FROM SmReturnReceipt s WHERE s.customerId = :customerId")
    ,
    @NamedQuery(name = "SmReturnReceipt.findByActive", query = "SELECT s FROM SmReturnReceipt s WHERE s.active = :active")
    ,
    @NamedQuery(name = "SmReturnReceipt.findByCltModuleId", query = "SELECT s FROM SmReturnReceipt s WHERE s.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "SmReturnReceipt.findByDateCreation", query = "SELECT s FROM SmReturnReceipt s WHERE s.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "SmReturnReceipt.findByUserCreation", query = "SELECT s FROM SmReturnReceipt s WHERE s.userCreation = :userCreation")
    ,
    @NamedQuery(name = "SmReturnReceipt.findByDateUpdate", query = "SELECT s FROM SmReturnReceipt s WHERE s.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "SmReturnReceipt.findByUserUpdate", query = "SELECT s FROM SmReturnReceipt s WHERE s.userUpdate = :userUpdate")})
public class SmReturnReceipt implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findById = "SELECT s FROM SmReturnReceipt s WHERE s.id = :id";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "NO_SEQ")
    private Long noSeq;

    @Column(name = "REFERENCE")
    private String reference;
    @Column(name = "CUSTOMER_ID")
    private Long customerId;

    @Column(name = "NOTE")
    private String note;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "RETURN_RECEIPT_STATUS_ID")
    private Long returnReceiptStatusId;

    public SmReturnReceipt() {
    }

    public SmReturnReceipt(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNoSeq() {
        return noSeq;
    }

    public void setNoSeq(Long noSeq) {
        this.noSeq = noSeq;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SmReturnReceipt)) {
            return false;
        }
        SmReturnReceipt other = (SmReturnReceipt) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entitys.views.SmReturnReceipt[ id=" + id + " ]";
    }

    public Long getReturnReceiptStatusId() {
        return returnReceiptStatusId;
    }

    public void setReturnReceiptStatusId(Long returnReceiptStatusId) {
        this.returnReceiptStatusId = returnReceiptStatusId;
    }

}
