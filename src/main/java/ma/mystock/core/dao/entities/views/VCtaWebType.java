/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_CTA_WEB_TYPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCtaWebType.findAll", query = "SELECT v FROM VCtaWebType v")
    ,
    @NamedQuery(name = "VCtaWebType.findById", query = "SELECT v FROM VCtaWebType v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VCtaWebType.findByName", query = "SELECT v FROM VCtaWebType v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VCtaWebType.findBySortKey", query = "SELECT v FROM VCtaWebType v WHERE v.sortKey = :sortKey")
    ,
    @NamedQuery(name = "VCtaWebType.findByActive", query = "SELECT v FROM VCtaWebType v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VCtaWebType.findByCltModuleId", query = "SELECT v FROM VCtaWebType v WHERE v.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "VCtaWebType.findByDateCreation", query = "SELECT v FROM VCtaWebType v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VCtaWebType.findByUserCreation", query = "SELECT v FROM VCtaWebType v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VCtaWebType.findByDateUpdate", query = "SELECT v FROM VCtaWebType v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VCtaWebType.findByUserUpdate", query = "SELECT v FROM VCtaWebType v WHERE v.userUpdate = :userUpdate")})
public class VCtaWebType implements Serializable {

    public static final String findByCltModuleId = "SELECT v FROM VCtaWebType v WHERE v.cltModuleId = :cltModuleId";
    private static final Long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @Id
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "SORT_KEY")
    private Long sortKey;

    @Column(name = "ACTIVE")
    private Character active;

    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public VCtaWebType() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

}
