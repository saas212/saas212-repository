/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdessamad HALLAL
 */
@Entity
@Table(name = "SM_SUPPLIER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SmSupplier.findAll", query = "SELECT s FROM SmSupplier s")
    ,
    @NamedQuery(name = "SmSupplier.findById", query = "SELECT s FROM SmSupplier s WHERE s.id = :id")
    ,
    @NamedQuery(name = "SmSupplier.findByFirstName", query = "SELECT s FROM SmSupplier s WHERE s.firstName = :firstName")
    ,
    @NamedQuery(name = "SmSupplier.findByLastName", query = "SELECT s FROM SmSupplier s WHERE s.lastName = :lastName")
    ,
    @NamedQuery(name = "SmSupplier.findByCompanyName", query = "SELECT s FROM SmSupplier s WHERE s.companyName = :companyName")
    ,
    @NamedQuery(name = "SmSupplier.findByCltModuleId", query = "SELECT s FROM SmSupplier s WHERE s.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "SmSupplier.findBySortKey", query = "SELECT s FROM SmSupplier s WHERE s.sortKey = :sortKey")
    ,
    @NamedQuery(name = "SmSupplier.findByActive", query = "SELECT s FROM SmSupplier s WHERE s.active = :active")
    ,
    @NamedQuery(name = "SmSupplier.findByDateCreation", query = "SELECT s FROM SmSupplier s WHERE s.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "SmSupplier.findByUserCreation", query = "SELECT s FROM SmSupplier s WHERE s.userCreation = :userCreation")
    ,
    @NamedQuery(name = "SmSupplier.findByDateUpdate", query = "SELECT s FROM SmSupplier s WHERE s.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "SmSupplier.findByUserUpdate", query = "SELECT s FROM SmSupplier s WHERE s.userUpdate = :userUpdate")
    ,
    @NamedQuery(name = "SmSupplier.findByShortLabel", query = "SELECT s FROM SmSupplier s WHERE s.shortLabel = :shortLabel")
    ,
    @NamedQuery(name = "SmSupplier.findBySupplierCategoryId", query = "SELECT s FROM SmSupplier s WHERE s.supplierCategoryId = :supplierCategoryId")})
public class SmSupplier implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static String findAll = "SELECT s FROM SmSupplier s";
    public static String findById = "SELECT s FROM SmSupplier s WHERE s.id = :id";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "COMPANY_NAME")
    private String companyName;

    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;
    @Column(name = "SORT_KEY")

    private Long sortKey;
    @Column(name = "ACTIVE")

    private String active;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "NOTE")
    private String note;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "SHORT_LABEL")
    private String shortLabel;

    @Column(name = "FULL_LABEL")
    private String fullLabel;

    @Column(name = "SUPPLIER_TYPE_ID")
    private Long supplierTypeId;

    @Column(name = "SUPPLIER_CATEGORY_ID")
    private Long supplierCategoryId;

    @Column(name = "ENTITY_ID")
    private Long entityId;

    @Column(name = "REPRESENTATIVE")
    private String representative;

    @Column(name = "SUPPLIER_NATURE_ID")
    private Long supplierNatureId;

    public SmSupplier() {
    }

    public String getRepresentative() {
        return representative;
    }

    public void setRepresentative(String representative) {
        this.representative = representative;
    }

    public Long getSupplierCategoryId() {
        return supplierCategoryId;
    }

    public void setSupplierCategoryId(Long supplierCategoryId) {
        this.supplierCategoryId = supplierCategoryId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SmSupplier)) {
            return false;
        }
        SmSupplier other = (SmSupplier) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entitys.SmSupplier[ id=" + id + " ]";
    }

    public Long getSupplierTypeId() {
        return supplierTypeId;
    }

    public void setSupplierTypeId(Long supplierTypeId) {
        this.supplierTypeId = supplierTypeId;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getShortLabel() {
        return shortLabel;
    }

    public void setShortLabel(String shortLabel) {
        this.shortLabel = shortLabel;
    }

    public String getFullLabel() {
        return fullLabel;
    }

    public void setFullLabel(String fullLabel) {
        this.fullLabel = fullLabel;
    }

    public Long getSupplierNatureId() {
        return supplierNatureId;
    }

    public void setSupplierNatureId(Long supplierNatureId) {
        this.supplierNatureId = supplierNatureId;
    }

}
