package ma.mystock.core.dao.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "SM_PRODUCT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SmProduct.findAll", query = "SELECT s FROM SmProduct s")
    ,
    @NamedQuery(name = "SmProduct.findById", query = "SELECT s FROM SmProduct s WHERE s.id = :id")
    ,
    @NamedQuery(name = "SmProduct.findByDesignation", query = "SELECT s FROM SmProduct s WHERE s.designation = :designation")
    ,
    @NamedQuery(name = "SmProduct.findByQuantity", query = "SELECT s FROM SmProduct s WHERE s.quantity = :quantity")
    ,
    @NamedQuery(name = "SmProduct.findByReference", query = "SELECT s FROM SmProduct s WHERE s.reference = :reference")
    ,
    @NamedQuery(name = "SmProduct.findByPriceSale", query = "SELECT s FROM SmProduct s WHERE s.priceSale = :priceSale")
    ,
    @NamedQuery(name = "SmProduct.findByActive", query = "SELECT s FROM SmProduct s WHERE s.active = :active")
    ,
    @NamedQuery(name = "SmProduct.findByDateCreation", query = "SELECT s FROM SmProduct s WHERE s.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "SmProduct.findByUserCreation", query = "SELECT s FROM SmProduct s WHERE s.userCreation = :userCreation")
    ,
    @NamedQuery(name = "SmProduct.findByDateUpdate", query = "SELECT s FROM SmProduct s WHERE s.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "SmProduct.findByUserUpdate", query = "SELECT s FROM SmProduct s WHERE s.userUpdate = :userUpdate")})
public class SmProduct implements Serializable {

    public static String findAll = "SELECT s FROM SmProduct s";
    public static String findById = "SELECT s FROM SmProduct s WHERE s.id = :id";
    public static String findByReferenceAndProductSizeIdAndProductColorId = "SELECT s FROM SmProduct s WHERE s.reference = :reference and s.productSizeId = :productSizeId and s.productColorId = :productColorId";
    public static String findMaxNoSeqByCltModuleId = "select max(s.noSeq) as noSeq from SmProduct s where s.cltModuleId = :cltModuleId";

    private static final Long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NO_SEQ")
    private Long noSeq;

    @Column(name = "DESIGNATION")
    private String designation;

    @Column(name = "QUANTITY")
    private Long quantity;

    @Column(name = "REFERENCE")
    private String reference;

    @Column(name = "PRICE_SALE")
    private Double priceSale;

    @Column(name = "PRICE_BUY")
    private Double priceBuy;

    @Column(name = "ACTIVE")
    private String active;

    @Column(name = "NOTE")
    private String note;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "PRODUCT_STATUS_ID")
    private Long productStatusId;

    @Column(name = "PRODUCT_SIZE_ID")
    private Long productSizeId;

    @Column(name = "PRODUCT_FAMILY_ID")
    private Long productFamilyId;

    @Column(name = "PRODUCT_COLOR_ID")
    private Long productColorId;

    @Column(name = "PRODUCT_GROUP_ID")
    private Long productGroupId;

    @Column(name = "PRODUCT_TYPE_ID")
    private Long productTypeId;

    @JoinColumn(name = "PRODUCT_STATUS_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne
    private SmProductStatus productStatus;

    @JoinColumn(name = "PRODUCT_SIZE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne
    private SmProductSize productSize;

    @JoinColumn(name = "PRODUCT_FAMILY_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne
    private SmProductFamily productFamily;

    @JoinColumn(name = "PRODUCT_COLOR_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne
    private SmProductColor productColor;

    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    @Column(name = "THRESHOLD")
    private Long threshold;

    @Column(name = "PRODUCT_DEPARTMENT_ID")
    private Long productDepartmentId;

    @Column(name = "PRODUCT_UNIT_ID")
    private Long productUnitId;

    public SmProduct() {
    }

    public void addQuantity(Long q) {
        if (this.quantity == null || this.quantity == 0) {
            this.quantity = q;
        } else {
            this.quantity = this.quantity + q;
        }
    }

    public SmProduct(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Double getPriceSale() {
        return priceSale;
    }

    public void setPriceSale(Double priceSale) {
        this.priceSale = priceSale;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public Long getProductStatusId() {
        return productStatusId;
    }

    public void setProductStatusId(Long productStatusId) {
        this.productStatusId = productStatusId;
    }

    public Long getProductSizeId() {
        return productSizeId;
    }

    public void setProductSizeId(Long productSizeId) {
        this.productSizeId = productSizeId;
    }

    public Long getProductFamilyId() {
        return productFamilyId;
    }

    public void setProductFamilyId(Long productFamilyId) {
        this.productFamilyId = productFamilyId;
    }

    public Long getProductColorId() {
        return productColorId;
    }

    public void setProductColorId(Long productColorId) {
        this.productColorId = productColorId;
    }

    public SmProductStatus getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(SmProductStatus productStatus) {
        this.productStatus = productStatus;
    }

    public SmProductSize getProductSize() {
        return productSize;
    }

    public void setProductSize(SmProductSize productSize) {
        this.productSize = productSize;
    }

    public SmProductFamily getProductFamily() {
        return productFamily;
    }

    public void setProductFamily(SmProductFamily productFamily) {
        this.productFamily = productFamily;
    }

    public SmProductColor getProductColor() {
        return productColor;
    }

    public void setProductColor(SmProductColor productColor) {
        this.productColor = productColor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SmProduct)) {
            return false;
        }
        SmProduct other = (SmProduct) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entitys.SmProduct[ id=" + id + " ]";
    }

    public Double getPriceBuy() {
        return priceBuy;
    }

    public void setPriceBuy(Double priceBuy) {
        this.priceBuy = priceBuy;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getProductGroupId() {
        return productGroupId;
    }

    public void setProductGroupId(Long productGroupId) {
        this.productGroupId = productGroupId;
    }

    public Long getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Long productTypeId) {
        this.productTypeId = productTypeId;
    }

    public Long getNoSeq() {
        return noSeq;
    }

    public void setNoSeq(Long noSeq) {
        this.noSeq = noSeq;
    }

    public Long getThreshold() {
        return threshold;
    }

    public void setThreshold(Long threshold) {
        this.threshold = threshold;
    }

    public Long getProductDepartmentId() {
        return productDepartmentId;
    }

    public void setProductDepartmentId(Long productDepartmentId) {
        this.productDepartmentId = productDepartmentId;
    }

    public Long getProductUnitId() {
        return productUnitId;
    }

    public void setProductUnitId(Long productUnitId) {
        this.productUnitId = productUnitId;
    }

}
