/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdessamd HALLAL
 */
@Entity
@Table(name = "v_inf_currency")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VInfCurrency.findAll", query = "SELECT v FROM VInfCurrency v")
    ,
    @NamedQuery(name = "VInfCurrency.findById", query = "SELECT v FROM VInfCurrency v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VInfCurrency.findByCode", query = "SELECT v FROM VInfCurrency v WHERE v.code = :code")
    ,
    @NamedQuery(name = "VInfCurrency.findByName", query = "SELECT v FROM VInfCurrency v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VInfCurrency.findBySortKey", query = "SELECT v FROM VInfCurrency v WHERE v.sortKey = :sortKey")
    ,
    @NamedQuery(name = "VInfCurrency.findByActive", query = "SELECT v FROM VInfCurrency v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VInfCurrency.findBySymbol", query = "SELECT v FROM VInfCurrency v WHERE v.symbol = :symbol")
    ,
    @NamedQuery(name = "VInfCurrency.findByFormat", query = "SELECT v FROM VInfCurrency v WHERE v.format = :format")
    ,
    @NamedQuery(name = "VInfCurrency.findByDateCreation", query = "SELECT v FROM VInfCurrency v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VInfCurrency.findByUserCreation", query = "SELECT v FROM VInfCurrency v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VInfCurrency.findByDateUpdate", query = "SELECT v FROM VInfCurrency v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VInfCurrency.findByUserUpdate", query = "SELECT v FROM VInfCurrency v WHERE v.userUpdate = :userUpdate")})
public class VInfCurrency implements Serializable {

    public static final String findByActive = "SELECT v FROM VInfCurrency v WHERE v.active = :active";

    private static final Long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @Id
    private Long id;
    @Size(max = 255)
    @Column(name = "CODE")
    private String code;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "SORT_KEY")
    private Long sortKey;
    @Column(name = "ACTIVE")
    private String active;
    @Size(max = 255)
    @Column(name = "SYMBOL")
    private String symbol;
    @Size(max = 255)
    @Column(name = "FORMAT")
    private String format;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public VInfCurrency() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

}
