/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_SM_RECEPTION_LINE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmReceptionLine.findAll", query = "SELECT v FROM VSmReceptionLine v")
    ,
    @NamedQuery(name = "VSmReceptionLine.findById", query = "SELECT v FROM VSmReceptionLine v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VSmReceptionLine.findByProductId", query = "SELECT v FROM VSmReceptionLine v WHERE v.productId = :productId")
    ,
    @NamedQuery(name = "VSmReceptionLine.findByDesignation", query = "SELECT v FROM VSmReceptionLine v WHERE v.designation = :designation")
    ,
    @NamedQuery(name = "VSmReceptionLine.findByUnitPriceBuy", query = "SELECT v FROM VSmReceptionLine v WHERE v.unitPriceBuy = :unitPriceBuy")
    ,
    @NamedQuery(name = "VSmReceptionLine.findByRemise", query = "SELECT v FROM VSmReceptionLine v WHERE v.remise = :remise")
    ,
    @NamedQuery(name = "VSmReceptionLine.findByQuantity", query = "SELECT v FROM VSmReceptionLine v WHERE v.quantity = :quantity")
    ,
    @NamedQuery(name = "VSmReceptionLine.findByTva", query = "SELECT v FROM VSmReceptionLine v WHERE v.tva = :tva")
    ,
    @NamedQuery(name = "VSmReceptionLine.findByActive", query = "SELECT v FROM VSmReceptionLine v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VSmReceptionLine.findByReceptionId", query = "SELECT v FROM VSmReceptionLine v WHERE v.receptionId = :receptionId")
    ,
    @NamedQuery(name = "VSmReceptionLine.findByDateCreation", query = "SELECT v FROM VSmReceptionLine v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VSmReceptionLine.findByUserCreation", query = "SELECT v FROM VSmReceptionLine v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VSmReceptionLine.findByDateUpdate", query = "SELECT v FROM VSmReceptionLine v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VSmReceptionLine.findByUserUpdate", query = "SELECT v FROM VSmReceptionLine v WHERE v.userUpdate = :userUpdate")
    ,
    @NamedQuery(name = "VSmReceptionLine.findByReference", query = "SELECT v FROM VSmReceptionLine v WHERE v.reference = :reference")
    ,
    @NamedQuery(name = "VSmReceptionLine.findByTotalPriceBuy", query = "SELECT v FROM VSmReceptionLine v WHERE v.totalPriceBuy = :totalPriceBuy")})
public class VSmReceptionLine implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findById = "SELECT v FROM VSmReceptionLine v WHERE v.id = :id";
    public static final String findByReceptionId = "SELECT v FROM VSmReceptionLine v WHERE v.receptionId = :receptionId";
    public static final String findByOrderSupplierIdAndProductId = "SELECT v FROM VSmReceptionLine v WHERE v.receptionId = :receptionId and v.productId = :productId";

    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;
    @Column(name = "PRODUCT_ID")
    private Long productId;
    @Size(max = 255)
    @Column(name = "DESIGNATION")
    private String designation;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "UNIT_PRICE_BUY")
    private Double unitPriceBuy;
    @Column(name = "REMISE")
    private Double remise;
    @Column(name = "QUANTITY")
    private Long quantity;
    @Column(name = "TVA")
    private Double tva;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "RECEPTION_ID")
    private Long receptionId;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;
    @Size(max = 255)
    @Column(name = "REFERENCE")
    private String reference;
    @Column(name = "TOTAL_PRICE_BUY")
    private Double totalPriceBuy;

    public VSmReceptionLine() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Double getUnitPriceBuy() {
        return unitPriceBuy;
    }

    public void setUnitPriceBuy(Double unitPriceBuy) {
        this.unitPriceBuy = unitPriceBuy;
    }

    public Double getRemise() {
        return remise;
    }

    public void setRemise(Double remise) {
        this.remise = remise;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getTva() {
        return tva;
    }

    public void setTva(Double tva) {
        this.tva = tva;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Long getReceptionId() {
        return receptionId;
    }

    public void setReceptionId(Long receptionId) {
        this.receptionId = receptionId;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Double getTotalPriceBuy() {
        return totalPriceBuy;
    }

    public void setTotalPriceBuy(Double totalPriceBuy) {
        this.totalPriceBuy = totalPriceBuy;
    }

}
