package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_CLT_PARAMETER_CLIENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCltParameterClient.findAll", query = "SELECT v FROM VCltParameterClient v")
    ,
    @NamedQuery(name = "VCltParameterClient.findByCode", query = "SELECT v FROM VCltParameterClient v WHERE v.code = :code")
    ,
    @NamedQuery(name = "VCltParameterClient.findByClientId", query = "SELECT v FROM VCltParameterClient v WHERE v.clientId = :clientId")
    ,
    @NamedQuery(name = "VCltParameterClient.findByParameterId", query = "SELECT v FROM VCltParameterClient v WHERE v.parameterId = :parameterId")
    ,
    @NamedQuery(name = "VCltParameterClient.findByParameterTypeName", query = "SELECT v FROM VCltParameterClient v WHERE v.parameterTypeName = :parameterTypeName")
    ,
    @NamedQuery(name = "VCltParameterClient.findByActive", query = "SELECT v FROM VCltParameterClient v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VCltParameterClient.findByParameterName", query = "SELECT v FROM VCltParameterClient v WHERE v.parameterName = :parameterName")})
public class VCltParameterClient implements Serializable {

    public static final String findAll = "SELECT v FROM VCltParameterClient v";

    private static final Long serialVersionUID = 1L;

    @Id
    @Column(name = "CODE")
    private String code;

    @Column(name = "CLIENT_ID")
    private Long clientId;

    @Column(name = "PARAMETER_ID")
    private Long parameterId;

    @Column(name = "PARAMETER_TYPE_NAME")
    private String parameterTypeName;
    @Basic(optional = false)

    @Column(name = "ACTIVE")
    private String active;

    @Column(name = "PARAMETER_NAME")
    private String parameterName;

    @Column(name = "VALUE")
    private String value;

    public VCltParameterClient() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getParameterId() {
        return parameterId;
    }

    public void setParameterId(Long parameterId) {
        this.parameterId = parameterId;
    }

    public String getParameterTypeName() {
        return parameterTypeName;
    }

    public void setParameterTypeName(String parameterTypeName) {
        this.parameterTypeName = parameterTypeName;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getParameterName() {
        return parameterName;
    }

    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
