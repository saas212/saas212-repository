/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_SM_ORDER_SUPPLIER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmOrderSupplier.findAll", query = "SELECT v FROM VSmOrderSupplier v")
    ,
    @NamedQuery(name = "VSmOrderSupplier.findById", query = "SELECT v FROM VSmOrderSupplier v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VSmOrderSupplier.findByNoSeq", query = "SELECT v FROM VSmOrderSupplier v WHERE v.noSeq = :noSeq")
    ,
    @NamedQuery(name = "VSmOrderSupplier.findByReference", query = "SELECT v FROM VSmOrderSupplier v WHERE v.reference = :reference")
    ,
    @NamedQuery(name = "VSmOrderSupplier.findByOrderSupplierStatusId", query = "SELECT v FROM VSmOrderSupplier v WHERE v.orderSupplierStatusId = :orderSupplierStatusId")
    ,
    @NamedQuery(name = "VSmOrderSupplier.findBySupplierId", query = "SELECT v FROM VSmOrderSupplier v WHERE v.supplierId = :supplierId")
    ,
    @NamedQuery(name = "VSmOrderSupplier.findByActive", query = "SELECT v FROM VSmOrderSupplier v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VSmOrderSupplier.findByCltModuleId", query = "SELECT v FROM VSmOrderSupplier v WHERE v.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "VSmOrderSupplier.findByDateCreation", query = "SELECT v FROM VSmOrderSupplier v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VSmOrderSupplier.findByUserCreation", query = "SELECT v FROM VSmOrderSupplier v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VSmOrderSupplier.findByDateUpdate", query = "SELECT v FROM VSmOrderSupplier v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VSmOrderSupplier.findByUserUpdate", query = "SELECT v FROM VSmOrderSupplier v WHERE v.userUpdate = :userUpdate")
    ,
    @NamedQuery(name = "VSmOrderSupplier.findBySupplierCompanyName", query = "SELECT v FROM VSmOrderSupplier v WHERE v.supplierCompanyName = :supplierCompanyName")
    ,
    @NamedQuery(name = "VSmOrderSupplier.findByTotalHorsTaxe", query = "SELECT v FROM VSmOrderSupplier v WHERE v.totalHorsTaxe = :totalHorsTaxe")
    ,
    @NamedQuery(name = "VSmOrderSupplier.findByTotalTtc", query = "SELECT v FROM VSmOrderSupplier v WHERE v.totalTtc = :totalTtc")
    ,
    @NamedQuery(name = "VSmOrderSupplier.findBySumProduct", query = "SELECT v FROM VSmOrderSupplier v WHERE v.sumProduct = :sumProduct")})
public class VSmOrderSupplier implements Serializable {

    private static final Long serialVersionUID = 1L;
    
    public static final String findByCltModuleIdAndOrderSupplierStatusId = "SELECT v FROM VSmOrderSupplier v WHERE v.cltModuleId = :cltModuleId and v.orderSupplierStatusId = :orderSupplierStatusId";
    public static final String findById = "SELECT v FROM VSmOrderSupplier v WHERE v.id = :id";
    public static final String findMaxNoSeqByCltModuleId = "SELECT max(v.noSeq) FROM VSmOrderSupplier v WHERE v.cltModuleId = :cltModuleId";
    public static final String findByCltModuleId = "SELECT v FROM VSmOrderSupplier v WHERE v.cltModuleId = :cltModuleId";
    public static final String findByReferenceAnCltModuleId = "SELECT v FROM VSmOrderSupplier v WHERE v.cltModuleId = :cltModuleId and v.reference = :reference";

    @Basic(optional = false)
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "NO_SEQ")
    private Long noSeq;

    @Column(name = "REFERENCE")
    private String reference;

    @Column(name = "NOTE")
    private String note;

    @Column(name = "ORDER_SUPPLIER_STATUS_ID")
    private Long orderSupplierStatusId;

    @Column(name = "SUPPLIER_ID")
    private Long supplierId;

    @Column(name = "ACTIVE")
    private String active;

    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Basic(optional = false)

    @Column(name = "SUPPLIER_COMPANY_NAME")
    private String supplierCompanyName;
    @Basic(optional = false)

    @Column(name = "TOTAL_HORS_TAXE")
    private Double totalHorsTaxe;
    @Basic(optional = false)

    @Column(name = "TOTAL_TTC")
    private Double totalTtc;

    @Basic(optional = false)
    @Column(name = "SUM_PRODUCT")
    private Double sumProduct;

    @Column(name = "ORDER_SUPPLIER_STATUS_NAME")
    private String orderSupplierStatusName;

    public VSmOrderSupplier() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNoSeq() {
        return noSeq;
    }

    public void setNoSeq(Long noSeq) {
        this.noSeq = noSeq;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getOrderSupplierStatusId() {
        return orderSupplierStatusId;
    }

    public void setOrderSupplierStatusId(Long orderSupplierStatusId) {
        this.orderSupplierStatusId = orderSupplierStatusId;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getSupplierCompanyName() {
        return supplierCompanyName;
    }

    public void setSupplierCompanyName(String supplierCompanyName) {
        this.supplierCompanyName = supplierCompanyName;
    }

    public Double getTotalHorsTaxe() {
        return totalHorsTaxe;
    }

    public void setTotalHorsTaxe(Double totalHorsTaxe) {
        this.totalHorsTaxe = totalHorsTaxe;
    }

    public Double getTotalTtc() {
        return totalTtc;
    }

    public void setTotalTtc(Double totalTtc) {
        this.totalTtc = totalTtc;
    }

    public Double getSumProduct() {
        return sumProduct;
    }

    public void setSumProduct(Double sumProduct) {
        this.sumProduct = sumProduct;
    }

    public String getOrderSupplierStatusName() {
        return orderSupplierStatusName;
    }

    public void setOrderSupplierStatusName(String orderSupplierStatusName) {
        this.orderSupplierStatusName = orderSupplierStatusName;
    }

}
