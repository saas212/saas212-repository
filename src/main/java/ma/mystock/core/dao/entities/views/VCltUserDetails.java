package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdessamad HALLAL
 */
@Entity
@Table(name = "V_CLT_USER_DETAILS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCltUserDetails.findAll", query = "SELECT v FROM VCltUserDetails v")
    ,
    @NamedQuery(name = "VCltUserDetails.findById", query = "SELECT v FROM VCltUserDetails v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VCltUserDetails.findByFirstName", query = "SELECT v FROM VCltUserDetails v WHERE v.firstName = :firstName")
    ,
    @NamedQuery(name = "VCltUserDetails.findByLastName", query = "SELECT v FROM VCltUserDetails v WHERE v.lastName = :lastName")
    ,
    @NamedQuery(name = "VCltUserDetails.findByUsername", query = "SELECT v FROM VCltUserDetails v WHERE v.username = :username")
    ,
    @NamedQuery(name = "VCltUserDetails.findByPassword", query = "SELECT v FROM VCltUserDetails v WHERE v.password = :password")
    ,
    @NamedQuery(name = "VCltUserDetails.findByCategoryId", query = "SELECT v FROM VCltUserDetails v WHERE v.categoryId = :categoryId")
    ,
    @NamedQuery(name = "VCltUserDetails.findByClientId", query = "SELECT v FROM VCltUserDetails v WHERE v.clientId = :clientId")
    ,
    @NamedQuery(name = "VCltUserDetails.findByUserStatusId", query = "SELECT v FROM VCltUserDetails v WHERE v.userStatusId = :userStatusId")
    ,
    @NamedQuery(name = "VCltUserDetails.findByActive", query = "SELECT v FROM VCltUserDetails v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VCltUserDetails.findByDateCreation", query = "SELECT v FROM VCltUserDetails v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VCltUserDetails.findByUserCreation", query = "SELECT v FROM VCltUserDetails v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VCltUserDetails.findByDateUpdate", query = "SELECT v FROM VCltUserDetails v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VCltUserDetails.findByUserUpdate", query = "SELECT v FROM VCltUserDetails v WHERE v.userUpdate = :userUpdate")})
public class VCltUserDetails implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static String findByUsernameAndPassword = "SELECT v FROM VCltUserDetails v where v.username = :username and v.password = :password";
    public static String findByUsernameAndPasswordAndClientId = "SELECT v FROM VCltUserDetails v where v.username = :username and v.password = :password and v.clientId = :clientId";

    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "CATEGORY_ID")
    private Long categoryId;

    @Column(name = "CLIENT_ID")
    private Long clientId;

    @Column(name = "USER_STATUS_ID")
    private Long userStatusId;

    //@Column(name = "DEFAULT_LANGUAGE_ID")
    //private Long defaultLanguageId;
    @Column(name = "ACTIVE")
    private String active;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "IMAGE_PATH")
    private String imagePath;

    public VCltUserDetails() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getUserStatusId() {
        return userStatusId;
    }

    public void setUserStatusId(Long userStatusId) {
        this.userStatusId = userStatusId;
    }

    /*
    public Long getDefaultLanguageId() {
        return defaultLanguageId;
    }

    public void setDefaultLanguageId(Long defaultLanguageId) {
        this.defaultLanguageId = defaultLanguageId;
    }
     */
    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

}
