package ma.mystock.core.dao.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdessamad HALLAL
 */
@Entity
@Table(name = "SM_CUSTOMER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SmCustomer.findAll", query = "SELECT s FROM SmCustomer s")
    ,
    @NamedQuery(name = "SmCustomer.findById", query = "SELECT s FROM SmCustomer s WHERE s.id = :id")
    ,
    @NamedQuery(name = "SmCustomer.findByFirstName", query = "SELECT s FROM SmCustomer s WHERE s.firstName = :firstName")
    ,
    @NamedQuery(name = "SmCustomer.findByLastName", query = "SELECT s FROM SmCustomer s WHERE s.lastName = :lastName")
    ,
    @NamedQuery(name = "SmCustomer.findByCustomerCategoryId", query = "SELECT s FROM SmCustomer s WHERE s.customerCategoryId = :customerCategoryId")
    ,
    @NamedQuery(name = "SmCustomer.findBySortKey", query = "SELECT s FROM SmCustomer s WHERE s.sortKey = :sortKey")
    ,
    @NamedQuery(name = "SmCustomer.findByCompanyName", query = "SELECT s FROM SmCustomer s WHERE s.companyName = :companyName")
    ,
    @NamedQuery(name = "SmCustomer.findByActive", query = "SELECT s FROM SmCustomer s WHERE s.active = :active")
    ,
    @NamedQuery(name = "SmCustomer.findByShortLabel", query = "SELECT s FROM SmCustomer s WHERE s.shortLabel = :shortLabel")
    ,
    @NamedQuery(name = "SmCustomer.findByDateCreation", query = "SELECT s FROM SmCustomer s WHERE s.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "SmCustomer.findByUserCreation", query = "SELECT s FROM SmCustomer s WHERE s.userCreation = :userCreation")
    ,
    @NamedQuery(name = "SmCustomer.findByIdentification", query = "SELECT s FROM SmCustomer s WHERE s.identification = :identification")
    ,
    @NamedQuery(name = "SmCustomer.findByDateUpdate", query = "SELECT s FROM SmCustomer s WHERE s.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "SmCustomer.findByUserUpdate", query = "SELECT s FROM SmCustomer s WHERE s.userUpdate = :userUpdate")})
public class SmCustomer implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findById = "SELECT s FROM SmCustomer s WHERE s.id = :id";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @Column(name = "CUSTOMER_CATEGORY_ID")
    private Long customerCategoryId;

    @Column(name = "CUSTOMER_TYPE_ID")
    private Long customerTypeId;

    @Column(name = "SORT_KEY")
    private Long sortKey;

    @Column(name = "COMPANY_NAME")
    private String companyName;
    @Column(name = "ACTIVE")
    private String active;

    @Column(name = "SHORT_LABEL")
    private String shortLabel;

    @Column(name = "FULL_LABEL")
    private String fullLabel;

    @Column(name = "NOTE")
    private String note;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "IDENTIFICATION")
    private String identification;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    @Column(name = "ENTITY_ID")
    private Long entityId;

    @Column(name = "REPRESENTATIVE")
    private String representative;

    @Column(name = "CUSTOMER_NATURE_ID")
    private Long customerNatureId;

    public SmCustomer() {
    }

    public String getRepresentative() {
        return representative;
    }

    public void setRepresentative(String representative) {
        this.representative = representative;
    }

    public SmCustomer(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getCustomerCategoryId() {
        return customerCategoryId;
    }

    public void setCustomerCategoryId(Long customerCategoryId) {
        this.customerCategoryId = customerCategoryId;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getShortLabel() {
        return shortLabel;
    }

    public void setShortLabel(String shortLabel) {
        this.shortLabel = shortLabel;
    }

    public String getFullLabel() {
        return fullLabel;
    }

    public void setFullLabel(String fullLabel) {
        this.fullLabel = fullLabel;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SmCustomer)) {
            return false;
        }
        SmCustomer other = (SmCustomer) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entitys.SmCustomer[ id=" + id + " ]";
    }

    public Long getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(Long customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Long getCustomerNatureId() {
        return customerNatureId;
    }

    public void setCustomerNatureId(Long customerNatureId) {
        this.customerNatureId = customerNatureId;
    }

}
