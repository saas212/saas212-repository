package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdessamad HALLAL
 */
@Entity
@Table(name = "V_SM_SUPPLIER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmSupplier.findAll", query = "SELECT v FROM VSmSupplier v")
    ,
    @NamedQuery(name = "VSmSupplier.findById", query = "SELECT v FROM VSmSupplier v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VSmSupplier.findByFirstName", query = "SELECT v FROM VSmSupplier v WHERE v.firstName = :firstName")
    ,
    @NamedQuery(name = "VSmSupplier.findByLastName", query = "SELECT v FROM VSmSupplier v WHERE v.lastName = :lastName")
    ,
    @NamedQuery(name = "VSmSupplier.findBySupplierTypeId", query = "SELECT v FROM VSmSupplier v WHERE v.supplierTypeId = :supplierTypeId")
    ,
    @NamedQuery(name = "VSmSupplier.findByCompanyName", query = "SELECT v FROM VSmSupplier v WHERE v.companyName = :companyName")
    ,
    @NamedQuery(name = "VSmSupplier.findByCltModuleId", query = "SELECT v FROM VSmSupplier v WHERE v.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "VSmSupplier.findBySortKey", query = "SELECT v FROM VSmSupplier v WHERE v.sortKey = :sortKey")
    ,
    @NamedQuery(name = "VSmSupplier.findByActive", query = "SELECT v FROM VSmSupplier v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VSmSupplier.findByDateCreation", query = "SELECT v FROM VSmSupplier v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VSmSupplier.findByUserCreation", query = "SELECT v FROM VSmSupplier v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VSmSupplier.findByDateUpdate", query = "SELECT v FROM VSmSupplier v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VSmSupplier.findByUserUpdate", query = "SELECT v FROM VSmSupplier v WHERE v.userUpdate = :userUpdate")
    ,
    @NamedQuery(name = "VSmSupplier.findByShortLabel", query = "SELECT v FROM VSmSupplier v WHERE v.shortLabel = :shortLabel")
    ,
    @NamedQuery(name = "VSmSupplier.findBySupplierCategoryId", query = "SELECT v FROM VSmSupplier v WHERE v.supplierCategoryId = :supplierCategoryId")
    ,
    @NamedQuery(name = "VSmSupplier.findBySupplierTypeName", query = "SELECT v FROM VSmSupplier v WHERE v.supplierTypeName = :supplierTypeName")
    ,
    @NamedQuery(name = "VSmSupplier.findBySupplierCategoryName", query = "SELECT v FROM VSmSupplier v WHERE v.supplierCategoryName = :supplierCategoryName")})
public class VSmSupplier implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static String findAll = "SELECT v FROM VSmSupplier v order by v.dateCreation, v.dateUpdate";
    public static String findById = "SELECT v FROM VSmSupplier v WHERE v.id = :id";
    public static String findByCltModuleId = "SELECT v FROM VSmSupplier v WHERE v.cltModuleId = :cltModuleId";
    public static String findByCltModuleIdAndActive = "SELECT v FROM VSmSupplier v WHERE v.cltModuleId = :cltModuleId and  v.active = :active";

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @Id
    private Long id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;
    @Column(name = "SUPPLIER_TYPE_ID")
    private Long supplierTypeId;

    @Column(name = "COMPANY_NAME")
    private String companyName;

    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;
    @Column(name = "SORT_KEY")
    private Long sortKey;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "NOTE")
    private String note;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "SHORT_LABEL")
    private String shortLabel;

    @Column(name = "FULL_LABEL")
    private String fullLabel;

    @Column(name = "SUPPLIER_CATEGORY_ID")
    private Long supplierCategoryId;

    @Column(name = "SUPPLIER_TYPE_NAME")
    private String supplierTypeName;

    @Column(name = "SUPPLIER_CATEGORY_NAME")
    private String supplierCategoryName;

    @Column(name = "CAN_BE_BELETE")
    private String canBeDeleted;

    @Column(name = "ENTITY_ID")
    private Long entityId;

    @Column(name = "REPRESENTATIVE")
    private String representative;

    // ENTITY_LOCATION_COUNTRY_NAME ENTITY_LOCATION_CITY_NAME
    @Column(name = "ENTITY_PHONE")
    private String entityPhone;

    @Column(name = "ENTITY_EMAIL")
    private String entityEmail;

    @Column(name = "ENTITY_FAX")
    private String entityFax;

    @Column(name = "ENTITY_LOCATION_ADDRESS")
    private String entityLocationAddress;

    @Column(name = "ENTITY_LOCATION_CITY_NAME")
    private String entityLocationCityName;

    @Column(name = "ENTITY_LOCATION_COUNTRY_NAME")
    private String entityLocationCountryName;

    @Column(name = "ENTITY_WEB")
    private String entityWeb;

    @Column(name = "SUPPLIER_NATURE_ID")
    private Long supplierNatureId;

    @Transient
    private String isSociety;

    @Transient
    private String isParticular;

    public String getIsSociety() {

        if ("1".equalsIgnoreCase(supplierNatureId + "")) {
            return "Y";
        } else {
            return "N";
        }

    }

    public String getIsParticular() {
        if ("2".equalsIgnoreCase(supplierNatureId + "")) {
            return "Y";
        } else {
            return "N";
        }
    }

    public VSmSupplier() {
    }

    public String getRepresentative() {
        return representative;
    }

    public void setRepresentative(String representative) {
        this.representative = representative;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getSupplierTypeId() {
        return supplierTypeId;
    }

    public void setSupplierTypeId(Long supplierTypeId) {
        this.supplierTypeId = supplierTypeId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getShortLabel() {
        return shortLabel;
    }

    public void setShortLabel(String shortLabel) {
        this.shortLabel = shortLabel;
    }

    public String getFullLabel() {
        return fullLabel;
    }

    public void setFullLabel(String fullLabel) {
        this.fullLabel = fullLabel;
    }

    public Long getSupplierCategoryId() {
        return supplierCategoryId;
    }

    public void setSupplierCategoryId(Long supplierCategoryId) {
        this.supplierCategoryId = supplierCategoryId;
    }

    public String getSupplierTypeName() {
        return supplierTypeName;
    }

    public void setSupplierTypeName(String supplierTypeName) {
        this.supplierTypeName = supplierTypeName;
    }

    public String getSupplierCategoryName() {
        return supplierCategoryName;
    }

    public void setSupplierCategoryName(String supplierCategoryName) {
        this.supplierCategoryName = supplierCategoryName;
    }

    public String getCanBeDeleted() {
        return canBeDeleted;
    }

    public void setCanBeDeleted(String canBeDeleted) {
        this.canBeDeleted = canBeDeleted;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getEntityPhone() {
        return entityPhone;
    }

    public void setEntityPhone(String entityPhone) {
        this.entityPhone = entityPhone;
    }

    public String getEntityEmail() {
        return entityEmail;
    }

    public void setEntityEmail(String entityEmail) {
        this.entityEmail = entityEmail;
    }

    public String getEntityFax() {
        return entityFax;
    }

    public void setEntityFax(String entityFax) {
        this.entityFax = entityFax;
    }

    public String getEntityLocationAddress() {
        return entityLocationAddress;
    }

    public void setEntityLocationAddress(String entityLocationAddress) {
        this.entityLocationAddress = entityLocationAddress;
    }

    public String getEntityLocationCityName() {
        return entityLocationCityName;
    }

    public void setEntityLocationCityName(String entityLocationCityName) {
        this.entityLocationCityName = entityLocationCityName;
    }

    public String getEntityLocationCountryName() {
        return entityLocationCountryName;
    }

    public void setEntityLocationCountryName(String entityLocationCountryName) {
        this.entityLocationCountryName = entityLocationCountryName;
    }

    public String getEntityWeb() {
        return entityWeb;
    }

    public void setEntityWeb(String entityWeb) {
        this.entityWeb = entityWeb;
    }

    public Long getSupplierNatureId() {
        return supplierNatureId;
    }

    public void setSupplierNatureId(Long supplierNatureId) {
        this.supplierNatureId = supplierNatureId;
    }

}
