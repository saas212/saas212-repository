/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_SM_ORDER_SUMMARY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmOrderSummary.findAll", query = "SELECT v FROM VSmOrderSummary v")
    ,
    @NamedQuery(name = "VSmOrderSummary.findByOrderId", query = "SELECT v FROM VSmOrderSummary v WHERE v.orderId = :orderId")
    ,
    @NamedQuery(name = "VSmOrderSummary.findBySummaryTotalQuantity", query = "SELECT v FROM VSmOrderSummary v WHERE v.summaryTotalQuantity = :summaryTotalQuantity")
    ,
    @NamedQuery(name = "VSmOrderSummary.findBySummaryTotalHt", query = "SELECT v FROM VSmOrderSummary v WHERE v.summaryTotalHt = :summaryTotalHt")
    ,
    @NamedQuery(name = "VSmOrderSummary.findBySummaryTvaAmount", query = "SELECT v FROM VSmOrderSummary v WHERE v.summaryTvaAmount = :summaryTvaAmount")
    ,
    @NamedQuery(name = "VSmOrderSummary.findBySummaryTotalTtc", query = "SELECT v FROM VSmOrderSummary v WHERE v.summaryTotalTtc = :summaryTotalTtc")})
public class VSmOrderSummary implements Serializable {

    private static final Long serialVersionUID = 1L;
    public static final String findByOrderId = "SELECT v FROM VSmOrderSummary v WHERE v.orderId = :orderId";

    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ORDER_ID")
    private Long orderId;

    @Column(name = "SUMMARY_TOTAL_QUANTITY")
    private Long summaryTotalQuantity;

    @Column(name = "SUMMARY_TOTAL_HT")
    private Double summaryTotalHt;

    @Column(name = "SUMMARY_TVA_AMOUNT")
    private Double summaryTvaAmount;

    @Column(name = "SUMMARY_TOTAL_TTC")
    private Double summaryTotalTtc;

    public VSmOrderSummary() {
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getSummaryTotalQuantity() {
        return summaryTotalQuantity;
    }

    public void setSummaryTotalQuantity(Long summaryTotalQuantity) {
        this.summaryTotalQuantity = summaryTotalQuantity;
    }

    public Double getSummaryTotalHt() {
        return summaryTotalHt;
    }

    public void setSummaryTotalHt(Double summaryTotalHt) {
        this.summaryTotalHt = summaryTotalHt;
    }

    public Double getSummaryTvaAmount() {
        return summaryTvaAmount;
    }

    public void setSummaryTvaAmount(Double summaryTvaAmount) {
        this.summaryTvaAmount = summaryTvaAmount;
    }

    public Double getSummaryTotalTtc() {
        return summaryTotalTtc;
    }

    public void setSummaryTotalTtc(Double summaryTotalTtc) {
        this.summaryTotalTtc = summaryTotalTtc;
    }

}
