/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_SM_SUPPLIER_TYPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmSupplierType.findAll", query = "SELECT v FROM VSmSupplierType v")
    ,
    @NamedQuery(name = "VSmSupplierType.findById", query = "SELECT v FROM VSmSupplierType v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VSmSupplierType.findByName", query = "SELECT v FROM VSmSupplierType v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VSmSupplierType.findByActive", query = "SELECT v FROM VSmSupplierType v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VSmSupplierType.findBySortKey", query = "SELECT v FROM VSmSupplierType v WHERE v.sortKey = :sortKey")
    ,
    @NamedQuery(name = "VSmSupplierType.findByCltModuleId", query = "SELECT v FROM VSmSupplierType v WHERE v.cltModuleId = :cltModuleId")})
public class VSmSupplierType implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findByCltModuleId = "SELECT v FROM VSmSupplierType v WHERE v.cltModuleId = :cltModuleId";
    public static final String findByCltModuleIdAndActive = "SELECT v FROM VSmSupplierType v WHERE v.cltModuleId = :cltModuleId and v.active = :active";
    public static final String findById = "SELECT v FROM VSmSupplierType v WHERE v.id = :id";

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    @Id
    private Long id;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "SORT_KEY")
    private Long sortKey;
    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    public VSmSupplierType() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

}
