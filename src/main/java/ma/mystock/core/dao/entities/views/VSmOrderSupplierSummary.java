/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_SM_ORDER_SUPPLIER_SUMMARY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmOrderSupplierSummary.findAll", query = "SELECT v FROM VSmOrderSupplierSummary v")
    ,
    @NamedQuery(name = "VSmOrderSupplierSummary.findByOrderSupplierId", query = "SELECT v FROM VSmOrderSupplierSummary v WHERE v.orderSupplierId = :orderSupplierId")
    ,
    @NamedQuery(name = "VSmOrderSupplierSummary.findBySummaryTotalQuantity", query = "SELECT v FROM VSmOrderSupplierSummary v WHERE v.summaryTotalQuantity = :summaryTotalQuantity")
    ,
    @NamedQuery(name = "VSmOrderSupplierSummary.findBySummaryTotalHt", query = "SELECT v FROM VSmOrderSupplierSummary v WHERE v.summaryTotalHt = :summaryTotalHt")
    ,
    @NamedQuery(name = "VSmOrderSupplierSummary.findBySummaryTvaAmount", query = "SELECT v FROM VSmOrderSupplierSummary v WHERE v.summaryTvaAmount = :summaryTvaAmount")
    ,
    @NamedQuery(name = "VSmOrderSupplierSummary.findBySummaryTotalTtc", query = "SELECT v FROM VSmOrderSupplierSummary v WHERE v.summaryTotalTtc = :summaryTotalTtc")})
public class VSmOrderSupplierSummary implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findByOrderSupplierId = "SELECT v FROM VSmOrderSupplierSummary v WHERE v.orderSupplierId = :orderSupplierId";

    @Id
    @NotNull
    @Column(name = "ORDER_SUPPLIER_ID")
    private Long orderSupplierId;

    @Column(name = "SUMMARY_TOTAL_QUANTITY")
    private Long summaryTotalQuantity;

    @Column(name = "SUMMARY_TOTAL_HT")
    private Double summaryTotalHt;

    @Column(name = "SUMMARY_TVA_AMOUNT")
    private Double summaryTvaAmount;

    @Column(name = "SUMMARY_TOTAL_TTC")
    private Double summaryTotalTtc;

    public VSmOrderSupplierSummary() {
    }

    public Long getOrderSupplierId() {
        return orderSupplierId;
    }

    public void setOrderSupplierId(Long orderSupplierId) {
        this.orderSupplierId = orderSupplierId;
    }

    public Long getSummaryTotalQuantity() {
        return summaryTotalQuantity;
    }

    public void setSummaryTotalQuantity(Long summaryTotalQuantity) {
        this.summaryTotalQuantity = summaryTotalQuantity;
    }

    public Double getSummaryTotalHt() {
        return summaryTotalHt;
    }

    public void setSummaryTotalHt(Double summaryTotalHt) {
        this.summaryTotalHt = summaryTotalHt;
    }

    public Double getSummaryTvaAmount() {
        return summaryTvaAmount;
    }

    public void setSummaryTvaAmount(Double summaryTvaAmount) {
        this.summaryTvaAmount = summaryTvaAmount;
    }

    public Double getSummaryTotalTtc() {
        return summaryTotalTtc;
    }

    public void setSummaryTotalTtc(Double summaryTotalTtc) {
        this.summaryTotalTtc = summaryTotalTtc;
    }

}
