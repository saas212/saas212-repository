/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_PM_MENUS_DTO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VPmMenusDto.findAll", query = "SELECT v FROM VPmMenusDto v")
    ,
    @NamedQuery(name = "VPmMenusDto.findByCategoryId", query = "SELECT v FROM VPmMenusDto v WHERE v.categoryId = :categoryId")
    ,
    @NamedQuery(name = "VPmMenusDto.findByCode", query = "SELECT v FROM VPmMenusDto v WHERE v.code = :code")
    ,
    @NamedQuery(name = "VPmMenusDto.findById", query = "SELECT v FROM VPmMenusDto v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VPmMenusDto.findByName", query = "SELECT v FROM VPmMenusDto v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VPmMenusDto.findByMenuTypeId", query = "SELECT v FROM VPmMenusDto v WHERE v.menuTypeId = :menuTypeId")
    ,
    @NamedQuery(name = "VPmMenusDto.findByMenuSort", query = "SELECT v FROM VPmMenusDto v WHERE v.menuSort = :menuSort")
    ,
    @NamedQuery(name = "VPmMenusDto.findByGroupId", query = "SELECT v FROM VPmMenusDto v WHERE v.groupId = :groupId")})
public class VPmMenusDto implements Serializable {

    public static String findAll = "SELECT v FROM VPmMenusDto v";
    public static String findByCategoryId = "SELECT v FROM VPmMenusDto v WHERE v.categoryId = :categoryId";
    public static String findByModelIdAndGroupIdAndCategoryId = "SELECT v FROM VPmMenusDto v WHERE v.modelId = :modelId and v.groupId = :groupId  and v.categoryId = :categoryId order by v.menuSort";

    private static final Long serialVersionUID = 1L;
    @Id
    @Size(max = 83)
    @Column(name = "CODE")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "MENU_TYPE_ID")
    private Long menuTypeId;
    @Column(name = "MENU_SORT")
    private Long menuSort;
    @Column(name = "MODEL_ID")
    private Long modelId;
    @Column(name = "GROUP_ID")
    private Long groupId;
    @Column(name = "CATEGORY_ID")
    private Long categoryId;

    @Column(name = "IMAGE_PATH")
    private String imagePath;

    transient List<VPmPagesDto> vPmPagesDtoList;

    public VPmMenusDto() {
        this.vPmPagesDtoList = new ArrayList<>();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getMenuTypeId() {
        return menuTypeId;
    }

    public void setMenuTypeId(Long menuTypeId) {
        this.menuTypeId = menuTypeId;
    }

    public Long getMenuSort() {
        return menuSort;
    }

    public void setMenuSort(Long menuSort) {
        this.menuSort = menuSort;
    }

    public Long getModelId() {
        return modelId;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public List<VPmPagesDto> getvPmPagesDtoList() {
        return vPmPagesDtoList;
    }

    public void setvPmPagesDtoList(List<VPmPagesDto> vPmPagesDtoList) {
        this.vPmPagesDtoList = vPmPagesDtoList;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

}
