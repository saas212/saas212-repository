/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "SM_RETURN_RECEIPT_LINE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SmReturnReceiptLine.findAll", query = "SELECT s FROM SmReturnReceiptLine s")
    ,
    @NamedQuery(name = "SmReturnReceiptLine.findById", query = "SELECT s FROM SmReturnReceiptLine s WHERE s.id = :id")
    ,
    @NamedQuery(name = "SmReturnReceiptLine.findByQuantity", query = "SELECT s FROM SmReturnReceiptLine s WHERE s.quantity = :quantity")
    ,
    @NamedQuery(name = "SmReturnReceiptLine.findByPromotionId", query = "SELECT s FROM SmReturnReceiptLine s WHERE s.promotionId = :promotionId")
    ,
    @NamedQuery(name = "SmReturnReceiptLine.findByNegotiatePriceSale", query = "SELECT s FROM SmReturnReceiptLine s WHERE s.negotiatePriceSale = :negotiatePriceSale")
    ,
    @NamedQuery(name = "SmReturnReceiptLine.findByActive", query = "SELECT s FROM SmReturnReceiptLine s WHERE s.active = :active")
    ,
    @NamedQuery(name = "SmReturnReceiptLine.findByDateCreation", query = "SELECT s FROM SmReturnReceiptLine s WHERE s.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "SmReturnReceiptLine.findByUserCreation", query = "SELECT s FROM SmReturnReceiptLine s WHERE s.userCreation = :userCreation")
    ,
    @NamedQuery(name = "SmReturnReceiptLine.findByDateUpdate", query = "SELECT s FROM SmReturnReceiptLine s WHERE s.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "SmReturnReceiptLine.findByUserUpdate", query = "SELECT s FROM SmReturnReceiptLine s WHERE s.userUpdate = :userUpdate")})
public class SmReturnReceiptLine implements Serializable {

    public static final String findById = "SELECT s FROM SmReturnReceiptLine s WHERE s.id = :id";

    private static final Long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;

    @Column(name = "DESIGNATION")
    private String designation;
    @Column(name = "QUANTITY")
    private Long quantity;
    @Column(name = "PROMOTION_ID")
    private Long promotionId;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "NEGOTIATE_PRICE_SALE")
    private Double negotiatePriceSale;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "RETURN_RECEIPT_ID")
    private Long returnReceiptId;

    @Column(name = "PRODUCT_ID")
    private Long productId;

    public SmReturnReceiptLine() {
    }

    public SmReturnReceiptLine(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Long promotionId) {
        this.promotionId = promotionId;
    }

    public Double getNegotiatePriceSale() {
        return negotiatePriceSale;
    }

    public void setNegotiatePriceSale(Double negotiatePriceSale) {
        this.negotiatePriceSale = negotiatePriceSale;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public Long getReturnReceiptId() {
        return returnReceiptId;
    }

    public void setReturnReceiptId(Long returnReceiptId) {
        this.returnReceiptId = returnReceiptId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SmReturnReceiptLine)) {
            return false;
        }
        SmReturnReceiptLine other = (SmReturnReceiptLine) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entitys.views.SmReturnReceiptLine[ id=" + id + " ]";
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

}
