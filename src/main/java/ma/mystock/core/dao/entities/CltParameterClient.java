/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdou
 */
@Entity
@Table(name = "CLT_PARAMETER_CLIENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CltParameterClient.findAll", query = "SELECT c FROM CltParameterClient c")
    ,
    @NamedQuery(name = "CltParameterClient.findById", query = "SELECT c FROM CltParameterClient c WHERE c.id = :id")
    ,
    @NamedQuery(name = "CltParameterClient.findByActive", query = "SELECT c FROM CltParameterClient c WHERE c.active = :active")
    ,
    @NamedQuery(name = "CltParameterClient.findByDateCreation", query = "SELECT c FROM CltParameterClient c WHERE c.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "CltParameterClient.findByUserCreation", query = "SELECT c FROM CltParameterClient c WHERE c.userCreation = :userCreation")
    ,
    @NamedQuery(name = "CltParameterClient.findByDateUpdate", query = "SELECT c FROM CltParameterClient c WHERE c.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "CltParameterClient.findByUserUpdate", query = "SELECT c FROM CltParameterClient c WHERE c.userUpdate = :userUpdate")})
public class CltParameterClient implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String findById = "SELECT c FROM CltParameterClient c WHERE c.id = :id";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Lob
    @Size(max = 2147483647)
    @Column(name = "VALUE")
    private String value;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;
    @JoinColumn(name = "PARAMETER_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne
    private CltParameter parameter;
    @Column(name = "PARAMETER_ID")
    private Long parameterId;
    @JoinColumn(name = "CLINET_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne
    private CltClient clinet;
    @Column(name = "CLINET_ID")
    private Long clinetId;

    public CltParameterClient() {
    }

    public CltParameterClient(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public CltParameter getParameter() {
        return parameter;
    }

    public void setParameter(CltParameter parameter) {
        this.parameter = parameter;
    }

    public Long getParameterId() {
        return parameterId;
    }

    public void setParameterId(Long parameterId) {
        this.parameterId = parameterId;
    }

    public CltClient getClinet() {
        return clinet;
    }

    public void setClinet(CltClient clinet) {
        this.clinet = clinet;
    }

    public Long getClinetId() {
        return clinetId;
    }

    public void setClinetId(Long clinetId) {
        this.clinetId = clinetId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CltParameterClient)) {
            return false;
        }
        CltParameterClient other = (CltParameterClient) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entitys.CltParameterClient[ id=" + id + " ]";
    }

}
