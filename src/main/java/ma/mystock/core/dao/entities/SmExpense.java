/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "SM_EXPENSE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SmExpense.findAll", query = "SELECT s FROM SmExpense s")
    ,
    @NamedQuery(name = "SmExpense.findById", query = "SELECT s FROM SmExpense s WHERE s.id = :id")
    ,
    @NamedQuery(name = "SmExpense.findByName", query = "SELECT s FROM SmExpense s WHERE s.name = :name")
    ,
    @NamedQuery(name = "SmExpense.findByAmount", query = "SELECT s FROM SmExpense s WHERE s.amount = :amount")
    ,
    @NamedQuery(name = "SmExpense.findByDateCreation", query = "SELECT s FROM SmExpense s WHERE s.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "SmExpense.findByUserCreation", query = "SELECT s FROM SmExpense s WHERE s.userCreation = :userCreation")
    ,
    @NamedQuery(name = "SmExpense.findByDateUpdate", query = "SELECT s FROM SmExpense s WHERE s.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "SmExpense.findByUserUpdate", query = "SELECT s FROM SmExpense s WHERE s.userUpdate = :userUpdate")})
public class SmExpense implements Serializable {

    // Requête SQL : 
    public static String findAll = "SELECT s FROM SmExpense s";
    public static String findByCltModule = "SELECT s FROM SmExpense s WHERE s.cltModuleId = :cltModuleId";
    public static String findById = "SELECT s FROM SmExpense s WHERE s.id = :id";

    // Attributs : 
    private static final Long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "DESCRIPTION")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "AMOUNT")
    private double amount;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    // Jointure Attributs 
    @Column(name = "EXPENSE_TYPE_ID")
    private Long expenseTypeId;

    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    // Jointure Attributs Object :
    @ManyToOne
    @JoinColumn(name = "EXPENSE_TYPE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    private SmExpenseType smExpenseType;

    @ManyToOne
    @JoinColumn(name = "CLT_MODULE_ID", referencedColumnName = "ID", insertable = false, updatable = false)
    private CltModule cltModule;

    // Constructor :
    public SmExpense() {
    }

    // Getters and Setters :
    public SmExpense(Long id) {
        this.id = id;
    }

    public SmExpense(Long id, double amount) {
        this.id = id;
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public Long getExpenseTypeId() {
        return expenseTypeId;
    }

    public void setExpenseTypeId(Long expenseTypeId) {
        this.expenseTypeId = expenseTypeId;
    }

    public SmExpenseType getSmExpenseType() {
        return smExpenseType;
    }

    public void setSmExpenseType(SmExpenseType smExpenseType) {
        this.smExpenseType = smExpenseType;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public CltModule getCltModule() {
        return cltModule;
    }

    public void setCltModule(CltModule cltModule) {
        this.cltModule = cltModule;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SmExpense)) {
            return false;
        }
        SmExpense other = (SmExpense) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entitys.SmExpense[ id=" + id + " ]";
    }

}
