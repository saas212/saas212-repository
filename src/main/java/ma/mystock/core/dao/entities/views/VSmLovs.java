package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import ma.mystock.core.dao.entities.views.dto.LovsDTO;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_SM_LOVS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmLovs.findAll", query = "SELECT v FROM VSmLovs v")
    ,
    @NamedQuery(name = "VSmLovs.findByCode", query = "SELECT v FROM VSmLovs v WHERE v.code = :code")
    ,
    @NamedQuery(name = "VSmLovs.findByTableCode", query = "SELECT v FROM VSmLovs v WHERE v.tableCode = :tableCode")
    ,
    @NamedQuery(name = "VSmLovs.findById", query = "SELECT v FROM VSmLovs v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VSmLovs.findByName", query = "SELECT v FROM VSmLovs v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VSmLovs.findByActive", query = "SELECT v FROM VSmLovs v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VSmLovs.findBySortKey", query = "SELECT v FROM VSmLovs v WHERE v.sortKey = :sortKey")
    ,
    @NamedQuery(name = "VSmLovs.findByCltModuleId", query = "SELECT v FROM VSmLovs v WHERE v.cltModuleId = :cltModuleId")})
public class VSmLovs extends LovsDTO implements Serializable {

    private static final Long serialVersionUID = 1L;

    final public static String findAll = "SELECT v FROM VSmLovs v";
    final public static String findByTableCode = "SELECT v FROM VSmLovs v WHERE v.tableCode = :tableCode";
    final public static String finAllDistinctTableCode = "select distinct new java.lang.String(v.tableCode) from VSmLovs v";

    @Id
    @Column(name = "CODE")
    private String code;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 17)
    @Column(name = "TABLE_CODE")
    private String tableCode;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "SORT_KEY")
    private Long sortKey;
    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    transient private String value;

    public VSmLovs() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTableCode() {
        return tableCode;
    }

    public void setTableCode(String tableCode) {
        this.tableCode = tableCode;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
