/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_SM_ORDER_SUPPLIER_LINE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmOrderSupplierLine.findAll", query = "SELECT v FROM VSmOrderSupplierLine v")
    ,
    @NamedQuery(name = "VSmOrderSupplierLine.findById", query = "SELECT v FROM VSmOrderSupplierLine v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VSmOrderSupplierLine.findByProductId", query = "SELECT v FROM VSmOrderSupplierLine v WHERE v.productId = :productId")
    ,
    @NamedQuery(name = "VSmOrderSupplierLine.findByUnitPriceSale", query = "SELECT v FROM VSmOrderSupplierLine v WHERE v.unitPriceSale = :unitPriceSale")
    ,
    @NamedQuery(name = "VSmOrderSupplierLine.findByQuantity", query = "SELECT v FROM VSmOrderSupplierLine v WHERE v.quantity = :quantity")
    ,
    @NamedQuery(name = "VSmOrderSupplierLine.findByTva", query = "SELECT v FROM VSmOrderSupplierLine v WHERE v.tva = :tva")
    ,
    @NamedQuery(name = "VSmOrderSupplierLine.findByRemise", query = "SELECT v FROM VSmOrderSupplierLine v WHERE v.remise = :remise")
    ,
    @NamedQuery(name = "VSmOrderSupplierLine.findByPromotionId", query = "SELECT v FROM VSmOrderSupplierLine v WHERE v.promotionId = :promotionId")
    ,
    @NamedQuery(name = "VSmOrderSupplierLine.findByOrderSupplierId", query = "SELECT v FROM VSmOrderSupplierLine v WHERE v.orderSupplierId = :orderSupplierId")
    ,
    @NamedQuery(name = "VSmOrderSupplierLine.findByActive", query = "SELECT v FROM VSmOrderSupplierLine v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VSmOrderSupplierLine.findByDateCreation", query = "SELECT v FROM VSmOrderSupplierLine v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VSmOrderSupplierLine.findByUserCreation", query = "SELECT v FROM VSmOrderSupplierLine v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VSmOrderSupplierLine.findByDateUpdate", query = "SELECT v FROM VSmOrderSupplierLine v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VSmOrderSupplierLine.findByUserUpdate", query = "SELECT v FROM VSmOrderSupplierLine v WHERE v.userUpdate = :userUpdate")})
public class VSmOrderSupplierLine implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findById = "SELECT v FROM VSmOrderSupplierLine v WHERE v.id = :id";
    public static final String findByOrderSupplierId = "SELECT v FROM VSmOrderSupplierLine v WHERE v.orderSupplierId = :orderSupplierId";
    public static final String findByOrderSupplierIdAndProductId = "SELECT v FROM VSmOrderSupplierLine v WHERE v.orderSupplierId = :orderSupplierId and v.productId = :productId";

    @Id
    @NotNull
    @Column(name = "ID")
    private Long id;

    @Column(name = "PRODUCT_ID")
    private Long productId;

    @Column(name = "REFERENCE")
    private String reference;

    @Column(name = "DESIGNATION")
    private String designation;

    @Column(name = "UNIT_PRICE_SALE")
    private Double unitPriceSale;

    @Column(name = "QUANTITY")
    private Long quantity;

    @Column(name = "TVA")
    private Double tva;

    @Column(name = "REMISE")
    private Double remise;

    @Column(name = "PROMOTION_ID")
    private Long promotionId;

    @Column(name = "TOTAL_PRICE_SALE")
    private Double totalPriceSale;

    @Column(name = "ORDER_SUPPLIER_ID")
    private Long orderSupplierId;

    @Column(name = "ACTIVE")
    private String active;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public VSmOrderSupplierLine() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Double getUnitPriceSale() {
        return unitPriceSale;
    }

    public void setUnitPriceSale(Double unitPriceSale) {
        this.unitPriceSale = unitPriceSale;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getTva() {
        return tva;
    }

    public void setTva(Double tva) {
        this.tva = tva;
    }

    public Double getRemise() {
        return remise;
    }

    public void setRemise(Double remise) {
        this.remise = remise;
    }

    public Long getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Long promotionId) {
        this.promotionId = promotionId;
    }

    public Double getTotalPriceSale() {
        return totalPriceSale;
    }

    public void setTotalPriceSale(Double totalPriceSale) {
        this.totalPriceSale = totalPriceSale;
    }

    public Long getOrderSupplierId() {
        return orderSupplierId;
    }

    public void setOrderSupplierId(Long orderSupplierId) {
        this.orderSupplierId = orderSupplierId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

}
