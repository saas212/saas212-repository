package ma.mystock.core.dao.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "CTA_ENTITY_FAX")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CtaEntityFax.findAll", query = "SELECT c FROM CtaEntityFax c")
    ,
    @NamedQuery(name = "CtaEntityFax.findById", query = "SELECT c FROM CtaEntityFax c WHERE c.id = :id")
    ,
    @NamedQuery(name = "CtaEntityFax.findByCountryCode", query = "SELECT c FROM CtaEntityFax c WHERE c.countryCode = :countryCode")
    ,
    @NamedQuery(name = "CtaEntityFax.findByNumber", query = "SELECT c FROM CtaEntityFax c WHERE c.number = :number")
    ,
    @NamedQuery(name = "CtaEntityFax.findByPriority", query = "SELECT c FROM CtaEntityFax c WHERE c.priority = :priority")
    ,
    @NamedQuery(name = "CtaEntityFax.findByDateCreation", query = "SELECT c FROM CtaEntityFax c WHERE c.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "CtaEntityFax.findByUserCreation", query = "SELECT c FROM CtaEntityFax c WHERE c.userCreation = :userCreation")
    ,
    @NamedQuery(name = "CtaEntityFax.findByDateUpdate", query = "SELECT c FROM CtaEntityFax c WHERE c.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "CtaEntityFax.findByUserUpdate", query = "SELECT c FROM CtaEntityFax c WHERE c.userUpdate = :userUpdate")})
public class CtaEntityFax implements Serializable {

    public static final String findById = "SELECT c FROM CtaEntityFax c WHERE c.id = :id";

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;

    @Column(name = "COUNTRY_CODE")
    private String countryCode;

    @Column(name = "NUMBER")
    private String number;

    @Column(name = "PRIORITY")
    private Long priority;

    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column(name = "USER_CREATION")
    private Long userCreation;

    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;

    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "ENTITY_ID")
    private Long entityId;

    @Column(name = "FAX_TYPE_ID")
    private Long faxTypeId;

    public CtaEntityFax() {
    }

    public CtaEntityFax(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CtaEntityFax)) {
            return false;
        }
        CtaEntityFax other = (CtaEntityFax) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.dao.entities.CtaEntityFax[ id=" + id + " ]";
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public Long getFaxTypeId() {
        return faxTypeId;
    }

    public void setFaxTypeId(Long faxTypeId) {
        this.faxTypeId = faxTypeId;
    }

}
