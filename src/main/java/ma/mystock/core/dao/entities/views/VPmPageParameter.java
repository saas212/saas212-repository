/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdou
 */
@Entity
@Table(name = "V_PM_PAGE_PARAMETER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VPmPageParameter.findAll", query = "SELECT v FROM VPmPageParameter v")
    ,
    @NamedQuery(name = "VPmPageParameter.findById", query = "SELECT v FROM VPmPageParameter v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VPmPageParameter.findByName", query = "SELECT v FROM VPmPageParameter v WHERE v.name = :name")
    ,
    @NamedQuery(name = "VPmPageParameter.findByPageParameterTypeId", query = "SELECT v FROM VPmPageParameter v WHERE v.pageParameterTypeId = :pageParameterTypeId")
    ,
    @NamedQuery(name = "VPmPageParameter.findByPmPageParameterTypeName", query = "SELECT v FROM VPmPageParameter v WHERE v.pmPageParameterTypeName = :pmPageParameterTypeName")
    ,
    @NamedQuery(name = "VPmPageParameter.findByActive", query = "SELECT v FROM VPmPageParameter v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VPmPageParameter.findByCanBeDelete", query = "SELECT v FROM VPmPageParameter v WHERE v.canBeDelete = :canBeDelete")})
public class VPmPageParameter implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String findById = "SELECT v FROM VPmPageParameter v WHERE v.id = :id";
    public static final String findAll = "SELECT v FROM VPmPageParameter v";
    public static final String findAllActive = "SELECT v FROM VPmPageParameter v WHERE v.active = :active";
    public static final String findAllActiveAndId = "SELECT v FROM VPmPageParameter v WHERE v.active = :active and v.pageId = :pageId and ((v.id = :id) or v.id not in (select cv.pageParameterId from VPmPageParameterModule cv where  cv.cltModuleId = :cltModuleId ))";
    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "DESCRIPTION")
    private String description;
    @Lob
    @Size(max = 65535)
    @Column(name = "DEFAULT_VALUE")
    private String defaultValue;
    @Column(name = "PAGE_PARAMETER_TYPE_ID")
    private Long pageParameterTypeId;
    @Size(max = 255)
    @Column(name = "PM_PAGE_PARAMETER_TYPE_NAME")
    private String pmPageParameterTypeName;
    @Column(name = "PAGE_ID")
    private Long pageId;
    @Size(max = 255)
    @Column(name = "PM_PAGE_NAME")
    private String pmPageName;
    @Column(name = "ACTIVE")
    private String active;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "CAN_BE_DELETE")
    private boolean canBeDelete;

    public VPmPageParameter() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public Long getPageParameterTypeId() {
        return pageParameterTypeId;
    }

    public void setPageParameterTypeId(Long pageParameterTypeId) {
        this.pageParameterTypeId = pageParameterTypeId;
    }

    public String getPmPageParameterTypeName() {
        return pmPageParameterTypeName;
    }

    public void setPmPageParameterTypeName(String pmPageParameterTypeName) {
        this.pmPageParameterTypeName = pmPageParameterTypeName;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public boolean isCanBeDelete() {
        return canBeDelete;
    }

    public void setCanBeDelete(boolean canBeDelete) {
        this.canBeDelete = canBeDelete;
    }

    public Long getPageId() {
        return pageId;
    }

    public void setPageId(Long pageId) {
        this.pageId = pageId;
    }

    public String getPmPageName() {
        return pmPageName;
    }

    public void setPmPageName(String pmPageName) {
        this.pmPageName = pmPageName;
    }

}
