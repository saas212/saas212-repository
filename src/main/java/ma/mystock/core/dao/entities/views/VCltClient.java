package ma.mystock.core.dao.entities.views;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Yamine Abdelmounaim <yaminea@evision.ca>
 */
@Entity
@Table(name = "V_CLT_CLIENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCltClient.findAll", query = "SELECT v FROM VCltClient v")
    ,
    @NamedQuery(name = "VCltClient.findById", query = "SELECT v FROM VCltClient v WHERE v.id = :id")
    ,
    @NamedQuery(name = "VCltClient.findByFirstName", query = "SELECT v FROM VCltClient v WHERE v.firstName = :firstName")
    ,
    @NamedQuery(name = "VCltClient.findByLastName", query = "SELECT v FROM VCltClient v WHERE v.lastName = :lastName")
    ,
    @NamedQuery(name = "VCltClient.findByCompanyName", query = "SELECT v FROM VCltClient v WHERE v.companyName = :companyName")
    ,
    @NamedQuery(name = "VCltClient.findByClientStatusId", query = "SELECT v FROM VCltClient v WHERE v.clientStatusId = :clientStatusId")
    ,
    @NamedQuery(name = "VCltClient.findByActive", query = "SELECT v FROM VCltClient v WHERE v.active = :active")
    ,
    @NamedQuery(name = "VCltClient.findByDateCreation", query = "SELECT v FROM VCltClient v WHERE v.dateCreation = :dateCreation")
    ,
    @NamedQuery(name = "VCltClient.findByUserCreation", query = "SELECT v FROM VCltClient v WHERE v.userCreation = :userCreation")
    ,
    @NamedQuery(name = "VCltClient.findByDateUpdate", query = "SELECT v FROM VCltClient v WHERE v.dateUpdate = :dateUpdate")
    ,
    @NamedQuery(name = "VCltClient.findByUserUpdate", query = "SELECT v FROM VCltClient v WHERE v.userUpdate = :userUpdate")})
public class VCltClient implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String findAllActive = "SELECT v FROM VCltClient v WHERE v.active = :active";
    public static final String findByCode = "SELECT v FROM VCltClient v WHERE lower(v.code) = :code";
    public static final String findById = "SELECT v FROM VCltClient v WHERE v.id = :id";

    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "CODE")
    private String code;

    @Size(max = 255)
    @Column(name = "FIRST_NAME")
    private String firstName;
    @Size(max = 255)
    @Column(name = "LAST_NAME")
    private String lastName;
    @Size(max = 255)
    @Column(name = "COMPANY_NAME")
    private String companyName;
    @Column(name = "CLIENT_STATUS_ID")
    private Long clientStatusId;

    //@Column(name = "DEFAULT_LANGUAGE_ID")
    //private Long defaultLanguageId;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "ENTITY_ID")
    private Long entityId;

    public VCltClient() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Long getClientStatusId() {
        return clientStatusId;
    }

    public void setClientStatusId(Long clientStatusId) {
        this.clientStatusId = clientStatusId;
    }

    /*

    public Long getDefaultLanguageId() {
        return defaultLanguageId;
    }

    public void setDefaultLanguageId(Long defaultLanguageId) {
        this.defaultLanguageId = defaultLanguageId;
    }
     */
    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

}
