/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "PM_MODEL")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PmModel.findAll", query = "SELECT p FROM PmModel p")
    ,
    @NamedQuery(name = "PmModel.findById", query = "SELECT p FROM PmModel p WHERE p.id = :id")
    ,
    @NamedQuery(name = "PmModel.findByName", query = "SELECT p FROM PmModel p WHERE p.name = :name")
    ,
    @NamedQuery(name = "PmModel.findByActive", query = "SELECT p FROM PmModel p WHERE p.active = :active")})
public class PmModel implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String findById = "SELECT p FROM PmModel p WHERE p.id = :id";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "ACTIVE")
    private String active;

    @Column(name = "INF_PACKAGE_ID")
    private Long infPackageId;

    public PmModel() {
    }

    public PmModel(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PmModel)) {
            return false;
        }
        PmModel other = (PmModel) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.entitys.PmModel[ id=" + id + " ]";
    }

    public Long getInfPackageId() {
        return infPackageId;
    }

    public void setInfPackageId(Long infPackageId) {
        this.infPackageId = infPackageId;
    }

}
