package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_BI_SM_ORDER_LINE_BY_DAY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VBiSmOrderLineByDay.findAll", query = "SELECT v FROM VBiSmOrderLineByDay v")
    ,
    @NamedQuery(name = "VBiSmOrderLineByDay.findByPk", query = "SELECT v FROM VBiSmOrderLineByDay v WHERE v.pk = :pk")
    ,
    @NamedQuery(name = "VBiSmOrderLineByDay.findByCltModuleId", query = "SELECT v FROM VBiSmOrderLineByDay v WHERE v.cltModuleId = :cltModuleId")
    ,
    @NamedQuery(name = "VBiSmOrderLineByDay.findByOperationDate", query = "SELECT v FROM VBiSmOrderLineByDay v WHERE v.operationDate = :operationDate")
    ,
    @NamedQuery(name = "VBiSmOrderLineByDay.findByMonth", query = "SELECT v FROM VBiSmOrderLineByDay v WHERE v.month = :month")
    ,
    @NamedQuery(name = "VBiSmOrderLineByDay.findByYear", query = "SELECT v FROM VBiSmOrderLineByDay v WHERE v.year = :year")
    ,
    @NamedQuery(name = "VBiSmOrderLineByDay.findBySumPrice", query = "SELECT v FROM VBiSmOrderLineByDay v WHERE v.sumPrice = :sumPrice")
    ,
    @NamedQuery(name = "VBiSmOrderLineByDay.findBySumQuantity", query = "SELECT v FROM VBiSmOrderLineByDay v WHERE v.sumQuantity = :sumQuantity")})
public class VBiSmOrderLineByDay implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final String findByCltModuleId = "SELECT v FROM VBiSmOrderLineByDay v WHERE v.cltModuleId = :cltModuleId";
    public static final String findByCltModuleIdAndMonthAndYear = "SELECT v FROM VBiSmOrderLineByDay v WHERE v.month = :month and v.year = :year and v.cltModuleId = :cltModuleId";

    @Id
    @Column(name = "PK")
    private String pk;

    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    @Column(name = "OPERATION_DATE")
    private String operationDate;

    @Column(name = "DAY")
    private Long day;

    @Column(name = "MONTH")
    private Long month;

    @Column(name = "YEAR")
    private Long year;

    @Column(name = "SUM_PRICE")
    private Double sumPrice;

    @Column(name = "SUM_QUANTITY")
    private Long sumQuantity;

    public VBiSmOrderLineByDay() {
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public String getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(String operationDate) {
        this.operationDate = operationDate;
    }

    public Long getMonth() {
        return month;
    }

    public void setMonth(Long month) {
        this.month = month;
    }

    public Long getYear() {
        return year;
    }

    public void setYear(Long year) {
        this.year = year;
    }

    public Double getSumPrice() {
        return sumPrice;
    }

    public void setSumPrice(Double sumPrice) {
        this.sumPrice = sumPrice;
    }

    public Long getSumQuantity() {
        return sumQuantity;
    }

    public void setSumQuantity(Long sumQuantity) {
        this.sumQuantity = sumQuantity;
    }

    public Long getDay() {
        return day;
    }

    public void setDay(Long day) {
        this.day = day;
    }

}
