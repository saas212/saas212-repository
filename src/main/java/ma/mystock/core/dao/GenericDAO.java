package ma.mystock.core.dao;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author abdou
 * @param <T> le mon de le class d'entity
 * @param <PrimaryKey> le type de Clé primaire
 *
 */
/*  
 (named, nativ, creteria, ...)
 */
// query.setFirstResult(1); // les parameter dans les namedQuery ?1 // @WebFilter(urlPatterns = {"*.xhtml"})
@Repository("GenericDAO")
@Transactional
public class GenericDAO<T, PrimaryKey extends Serializable> {

    // ############################ EntityManger ############################
    @PersistenceContext(name = "Saas212PU")
    protected EntityManager entityManager;
    protected static Logger log = Logger.getLogger(GenericDAO.class);

    public List<Class> getLov(Class lov, Long moduleId) {

        // requete
        String querys = "SELECT v FROM " + lov.getSimpleName() + " v WHERE v.cltModuleId = :cltModuleId";

        log.info("iiii : " + querys);

        // params 
        Map<String, Object> m = new HashMap<>();
        m.put("cltModuleId", moduleId);

        // request
        Query query = entityManager.createQuery(querys);
        for (String key : m.keySet()) {
            query.setParameter(key, m.get(key));
        }
        query.setHint("eclipselink.refresh", "true");

        // result
        List<Class> list;
        list = query.getResultList();
        return list;

        //return executeQuery(querys,m);
    }

    /* ###############################  Select Simple  List ############################## */
    public List executeQuery(String querys) {

        List<T> list = null;
        Query query = entityManager.createQuery(querys);
        query.setHint("eclipselink.refresh", "true");
        list = query.getResultList();
        return list;
    }

    public List executeQuery(String querys, Map<String, Object> parameters) {
        List<T> list;
        Query query = entityManager.createQuery(querys);
        for (String key : parameters.keySet()) {
            query.setParameter(key, parameters.get(key));
        }
        query.setHint("eclipselink.refresh", "true");
        list = query.getResultList();
        return list;
    }

    public List executeQuery(String querys, Map<String, Object> parameters, int limit) {
        List<T> list;
        Query query = entityManager.createQuery(querys);
        for (String key : parameters.keySet()) {
            query.setParameter(key, parameters.get(key));
        }
        query.setHint("eclipselink.refresh", "true");
        query.setMaxResults(limit);
        list = query.getResultList();
        return list;
    }

    public T executeSingleQuery(String querys) {
        T object;
        Query query = entityManager.createQuery(querys);
        query.setHint("eclipselink.refresh", "true");
        try {
            object = (T) query.getSingleResult();
        } catch (Exception e) {
            object = null;
        }

        return object;
    }

    public T executeSingleQuery(String querys, Map<String, Object> parameters) {
        T object;
        Query query = entityManager.createQuery(querys);
        for (String key : parameters.keySet()) {
            query.setParameter(key, parameters.get(key));
        }
        query.setHint("eclipselink.refresh", "true");
        try {
            object = (T) query.getSingleResult();
        } catch (Exception e) {
            object = null;
        }
        return object;
    }

    public Long executeLongQuery(String querys, Map<String, Object> parameters) {
        Long value;
        Query query = entityManager.createQuery(querys);
        for (String key : parameters.keySet()) {
            query.setParameter(key, parameters.get(key));
        }
        query.setHint("eclipselink.refresh", "true");
        try {
            value = (Long) query.getSingleResult();
        } catch (Exception e) {
            value = null;
        }
        return value;
    }

    public List<T> executeNamedQuery(String namedQuery) {
        List<T> list;
        Query query = entityManager.createNamedQuery(namedQuery);
        query.setHint("eclipselink.refresh", "true");
        list = query.getResultList();
        return list;
    }

    public List<T> executeNamedQuery(String namedQuery, Map<String, Object> parameters) {
        List<T> list;
        Query query = entityManager.createNamedQuery(namedQuery);
        for (String key : parameters.keySet()) {
            query.setParameter(key, parameters.get(key));
        }
        query.setHint("eclipselink.refresh", "true");
        list = query.getResultList();
        return list;
    }

    public List<T> executeNativeQuery(String nativeQuery) {
        List<T> list;
        Query query = entityManager.createNativeQuery(nativeQuery);
        query.setHint("eclipselink.refresh", "true");
        list = query.getResultList();
        return list;
    }

    public List<T> executeNativeQuery(String nativeQuery, Class clazz, Map<Integer, Object> parameters) {
        List<T> list;
        Query query = entityManager.createNativeQuery(nativeQuery, clazz);
        for (Integer key : parameters.keySet()) {
            query.setParameter(key, parameters.get(key));
            log.info("=> " + key + " - " + parameters.get(key));
        }
        query.setHint("eclipselink.refresh", "true");
        list = query.getResultList();
        return list;
    }

    public List<T> executeNativeQuery(String nativeQuery, Class clazz, Object... parameters) {
        List<T> list;
        Query query = entityManager.createNativeQuery(nativeQuery, clazz);
        int i = 1;
        for (Object param : parameters) {
            query.setParameter(i, param);
            i++;
        }
        query.setHint("eclipselink.refresh", "true");
        list = query.getResultList();
        return list;
    }

    public List<T> createCreteriaList() {
        /*
         CriteriaBuilder cb = entityManager.getCriteriaBuilder();
 
         // Query for a List of objects.
         CriteriaQuery cq = cb.createQuery();
         Root e = cq.from(Table1.class);
         cq.where(cb.greaterThan(e.get("salary"), 100000));
         Query query = entityManager.createQuery(cq);
         List<Table1> result = query.getResultList();

         // Query for a single object.
         cq.where(cb.equal(e.get("id"), cb.parameter(Long.class, "id")));
         query.setParameter("id", 1);
         Table1 result2 = (Table1)query.getSingleResult();

         // Query for a single data element.
         cq.select(cb.max(e.get("salary")));
         BigDecimal result3 = (BigDecimal)query.getSingleResult();

         // Query for a List of data elements.
         cq.select(e.get("firstName"));
         List<String> result4 = query.getResultList();
            
            
         // Query for a List of element arrays.
         cq.multiselect(e.get("firstName"), e.get("lastName"));
         List<Object[]> result5 = query.getResultList();
            
            
            
         // TEST deux table 
            
         // Select the employees and the mailing addresses that have the same address.
         Root e1 = cq.from(Table1.class);
         Root e2 = cq.from(String.class);
         cq.multiselect(e1, e2);
         cq.where(cb.equal(e1.get("address"), e2.get("address")));
         List<Object[]> results = query.getResultList();

         // http://en.wikibooks.org/wiki/Java_Persistence/Criteria
         */
        return null;
    }

    /* ###############################  Select Simple Object ############################## */
    public T executeSingleNamedQuery(String namedQuery) {
        T object;
        Query query = entityManager.createNamedQuery(namedQuery);
        query.setHint("eclipselink.refresh", "true");
        try {
            object = (T) query.getSingleResult();
        } catch (Exception e) {
            object = null;
        }

        return object;
    }

    public T createCreteriaObject() {
        /*
         CriteriaBuilder cb = entityManager.getCriteriaBuilder();
 
         // Query for a List of objects.
         CriteriaQuery cq = cb.createQuery();
         Root e = cq.from(Table1.class);
         cq.where(cb.greaterThan(e.get("salary"), 100000));
         Query query = entityManager.createQuery(cq);
         List<Table1> result = query.getResultList();

         // Query for a single object.
         cq.where(cb.equal(e.get("id"), cb.parameter(Long.class, "id")));
         query.setParameter("id", 1);
         Table1 result2 = (Table1)query.getSingleResult();

         // Query for a single data element.
         cq.select(cb.max(e.get("salary")));
         BigDecimal result3 = (BigDecimal)query.getSingleResult();

         // Query for a List of data elements.
         cq.select(e.get("firstName"));
         List<String> result4 = query.getResultList();
            
            
         // Query for a List of element arrays.
         cq.multiselect(e.get("firstName"), e.get("lastName"));
         List<Object[]> result5 = query.getResultList();
            
            
            
         // TEST deux table 
            
         // Select the employees and the mailing addresses that have the same address.
         Root e1 = cq.from(Table1.class);
         Root e2 = cq.from(String.class);
         cq.multiselect(e1, e2);
         cq.where(cb.equal(e1.get("address"), e2.get("address")));
         List<Object[]> results = query.getResultList();

         // http://en.wikibooks.org/wiki/Java_Persistence/Criteria
         */
        return null;
    }

    public T executeSingleNamedQuery(String namedQuery, Map<String, Object> parameters) {
        T object;
        Query query = entityManager.createNamedQuery(namedQuery);
        for (String key : parameters.keySet()) {
            query.setParameter(key, parameters.get(key));
        }
        query.setHint("eclipselink.refresh", "true");
        try {
            object = (T) query.getSingleResult();
        } catch (Exception e) {
            object = null;
        }
        return object;
    }

    public T executeSingleNativeQuery(String nativeQuery) {
        return null;
    }

    public T executeSingleNativeQuery(String nativeQuery, Class clazz, Object... parameters) {
        return null;
    }

    public T executeSingleNativeQuery(String nativeQuery, Class clazz, Map<Integer, Object> parameters) {
        return null;
    }

    /* ##############################  Select Max result  ############################### */
    public List<T> executeNamedQueryMaxResult(String namedQuery, int maxResult) {
        return null;
    }

    public List<T> executeNamedQueryMaxResult(String namedQuery, Map<String, Object> parameters, int maxResult) {
        return null;
    }

    public List<T> executeNativeQueryMaxResult(String nativeQuery, int maxResult) {
        return null;
    }

    public List<T> executeNativeQueryMaxResult(String nativeQuery, Class clazz, Map<Integer, Object> parameters, int maxResult) {
        return null;
    }

    /* ##############################  Select With parameters  ############################### */

 /* #####################################  Select count  ################################## */
    public Long executeCountNativeQuery(String nativeQuery) {
        return null;
    }

    public Long executeCountNativeQuery(String nativeQuery, Class clazz, Map<Integer, Object> parameters) {
        return null;
    }

    public Long executeCountNativeQuery(String nativeQuery, Class clazz, Object... parameters) {
        return null;
    }

    public Long executeCount(Class clazz) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        cq.select(cb.count(cq.from(clazz)));
        //cq.where(//your stuff/);
        return entityManager.createQuery(cq).getSingleResult();
    }

    /* ########################################  Update  #################################### */
    public int executeNamedUpdate(String nativeQuery, Map<Integer, Object> parameters) {

        return 1;
    }

    public int executeNamedUpdate(String nativeQuery, Object... parameters) {

        return 1;
    }

    public int executeNamedUpdate(String nativeQuery) {

        return 1;
    }

    public void CreateUpdate() {
        /*
         CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
         // create update
         CriteriaUpdate<Table1> update = cb.createCriteriaUpdate(Table1.class);
         // set the root class
         Root e = update.from(Table1.class);
         // set update and where clause
         update.set("name", "abdou");
         update.where(cb.greaterThanOrEqualTo(e.get("id"), 3));

         // perform update
         this.entityManager.createQuery(update).executeUpdate();
         */
    }

    // ######################### Update query  #############################
    public int executeNativeUpdate(String nativeQuery) {

        return 1;
    }

    public int executeNativeUpdate(String nativeQuery, Map<Integer, Object> parameters) {

        return 1;
    }

    public int executeNativeUpdate(String nativeQuery, Object... parameters) {

        return 1;
    }

    // ######################### Stored Procedure  #############################
    public int executeStoredProcedureQueryGeneric(String name, Map<String, StoredProcedureParameter> parameters) {

        // Prepare storedProcedure
        StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery(name);

        // Set parameters
        for (String key : parameters.keySet()) {

            StoredProcedureParameter parameter = parameters.get(key);
            storedProcedure.registerStoredProcedureParameter(key, parameter.getClasse(), parameter.getMode());

            if (ParameterMode.IN.equals(parameter.getMode())) {
                storedProcedure.setParameter(key, parameter.getValue());
            }
        }

        // Execute call 
        storedProcedure.execute();

        // Get result
        return (int) storedProcedure.getOutputParameterValue("RESULT_STATUS");

    }

    public int executeStoredProcedureUpdate(String nativeQuery) {

        return 1;
    }

    public int executeStoredProcedureUpdate(String nativeQuery, Map<Integer, Object> parameters) {

        return 1;
    }

    public int executeStoredProcedureUpdate(String nativeQuery, Object... parameters) {

        return 1;
    }

    // #########################  Insert  ###########################
    public T executeMerge(T entity) {
        return entityManager.merge(entity);
    }

    public void executePersist(T entity) {
        entityManager.persist(entity);
    }
    // ######################### Remove   ###########################

    public void executeDelete(T entity) {
        entityManager.remove(entityManager.merge(entity));
    }

    public void deleteOrder() {
        /*
         CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();

         // create delete
         CriteriaDelete<Table1> delete = cb.createCriteriaDelete(Table1.class);

         // set the root class
         Root e = delete.from(Table1.class);

         // set where clause
         delete.where(cb.lessThanOrEqualTo(e.get("id"), 10));

         // perform update
         this.entityManager.createQuery(delete).executeUpdate();
         */
    }

    public EntityManager getEm() {
        return entityManager;
    }

}
