/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao;

/**
 *
 * @author Abdessamad HALLAL
 */
public class StoredProcedures {

    public static final String SP_SM_VALIDATE_RECEPTION = "SP_SM_VALIDATE_RECEPTION";
    public static final String PARAM_SM_RECEPTION_ID = "SM_RECEPTION_ID";

    public static final String SP_SM_VALIDATE_ORDER = "SP_SM_VALIDATE_ORDER";
    public static final String PARAM_ORDER_ID = "ORDER_ID";

    public static final String PARAM_RESULT_STATUS = "RESULT_STATUS";

}
