package ma.mystock.module.pageManagement.components;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 *
 * @author pro
 */
@Component
@Scope(value = WebApplicationContext.SCOPE_APPLICATION, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class ApplicationContent {

    private static final Logger logger = Logger.getLogger(ApplicationContent.class);

    private boolean isDebugEnabled;

    public ApplicationContent() {
        isDebugEnabled = false;
    }

    public boolean isIsDebugEnabled() {
        return isDebugEnabled;
    }

    public void setIsDebugEnabled(boolean isDebugEnabled) {
        this.isDebugEnabled = isDebugEnabled;
    }

}
