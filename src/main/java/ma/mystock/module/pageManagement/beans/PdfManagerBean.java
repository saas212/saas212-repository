package ma.mystock.module.pageManagement.beans;

import ma.mystock.web.utils.springBeans.GloablsServices;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import ma.mystock.core.dao.entities.views.VPmPage;
import ma.mystock.core.dao.entities.views.VSmProduct;
import ma.mystock.core.dao.entities.views.dto.VPmPagesDto;
import ma.mystock.web.utils.datatype.MyLong;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.web.utils.jsf.FacesUtils;
import static ma.mystock.web.utils.springBeans.GloablsServices.genericServices;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author abdou
 *
 */
@Component("pdfManagerBean")
@Scope("view")
public class PdfManagerBean extends GloablsServices implements Serializable {

    protected static Logger log = Logger.getLogger(PdfManagerBean.class);
    protected Map<String, Object> paramQuery = new HashMap<>();

    private VPmPage vPmPdf;

    private PdfInterface selectedPdf;
    private String jsp;

    private Long vPmPdfId;

    private String outputTypeParam;
    private String pdfIdParam;
    private String fileNameParam;

    @PostConstruct
    public void onInit() {

        outputTypeParam = FacesUtils.getRequestParameter("outputType");
        pdfIdParam = FacesUtils.getRequestParameter("pdfId");
        fileNameParam = FacesUtils.getRequestParameter("fileName");

        System.out.println("outputTypeParam :  " + outputTypeParam);
        System.out.println("pdfIdParam :  " + pdfIdParam);
        System.out.println("fileNameParam :  " + fileNameParam);

        vPmPdfId = MyLong.toLong(pdfIdParam);
        vPmPdfId = vPmPdfId + 1000L;

        vPmPdf = genericServices.findVPmPageById(vPmPdfId);
        System.out.println(" ......vPmPdf : " + vPmPdf);
        if (MyIs.isNull(vPmPdf)) {
            //RedirectUtils.goTo404();
        }

        String classe = "ma.mystock.web.beans.compositions.pdfs.pdf" + vPmPdfId + ".Pdf" + vPmPdfId;

        jsp = "pdfs/pdf" + vPmPdfId + "/pdf" + vPmPdfId + ".xhtml";

        System.out.println(" classe : " + classe);
        System.out.println(" jsp : " + jsp);

        selectedPdf = getPdf(classe);

        if (selectedPdf != null) {
            //selectedVPmPdf.onInit();
        }

        log.info("PdfManagerBean @ onInit");
    }

    public PdfInterface getPdf(String clazz) {
        try {
            return (PdfInterface) Class.forName(clazz).newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            return null;
        }
    }

    public static Logger getLog() {
        return log;
    }

    public static void setLog(Logger log) {
        PdfManagerBean.log = log;
    }

    public Map<String, Object> getParamQuery() {
        return paramQuery;
    }

    public void setParamQuery(Map<String, Object> paramQuery) {
        this.paramQuery = paramQuery;
    }

    public Long getvPmPdfId() {
        return vPmPdfId;
    }

    public void setvPmPdfId(Long vPmPdfId) {
        this.vPmPdfId = vPmPdfId;
    }

    public VPmPage getvPmPdf() {
        return vPmPdf;
    }

    public void setvPmPdf(VPmPage vPmPdf) {
        this.vPmPdf = vPmPdf;
    }

    public String getJsp() {
        return jsp;
    }

    public void setJsp(String jsp) {
        this.jsp = jsp;
    }

    public String getOutputTypeParam() {
        return outputTypeParam;
    }

    public void setOutputTypeParam(String outputTypeParam) {
        this.outputTypeParam = outputTypeParam;
    }

    public String getPdfIdParam() {
        return pdfIdParam;
    }

    public void setPdfIdParam(String pdfIdParam) {
        this.pdfIdParam = pdfIdParam;
    }

    public String getFileNameParam() {
        return fileNameParam;
    }

    public void setFileNameParam(String fileNameParam) {
        this.fileNameParam = fileNameParam;
    }

    public PdfInterface getSelectedPdf() {
        return selectedPdf;
    }

    public void setSelectedPdf(PdfInterface selectedPdf) {
        this.selectedPdf = selectedPdf;
    }

}
