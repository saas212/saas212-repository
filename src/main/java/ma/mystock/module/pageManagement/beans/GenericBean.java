package ma.mystock.module.pageManagement.beans;

import java.io.Serializable;
import ma.mystock.web.utils.springBeans.GloablsServices;
import ma.mystock.web.utils.values.MapsGetter;
import java.util.*;
import javax.faces.model.SelectItem;
import ma.mystock.core.dao.entities.views.dto.LovsDTO;
import ma.mystock.web.utils.sessions.SessionsUtils;
import ma.mystock.module.pageManagement.messages.MessageList;
import ma.mystock.web.utils.functionUtils.DateUtils;
import ma.mystock.web.utils.sessions.SessionsGetter;
import org.apache.log4j.Logger;

/**
 *
 * @author abdou
 * @desc cette classe globale pour gérer :
 *
 * Message de validation List messages map queryParam
 *
 */
public class GenericBean extends GloablsServices implements Serializable {

    protected MessageList messages = new MessageList(); // Gestion des messages (attr, +  les 4 methods setError, setInfo, ...)
    protected static Logger log = Logger.getLogger(GenericBean.class);
    protected Long cltModuleId = SessionsGetter.getCltModuleId();

    protected Long currentDay = DateUtils.getCurrentMonth();
    protected Long currentMonth = DateUtils.getCurrentMonth();
    protected Long currentYear = DateUtils.getCurrentDay();

    public Map<String, Object> paramQuery = new HashMap<>();
    public Map<String, String> paramPdf = new HashMap<>();

    public MessageList getMessages() {
        return messages;
    }

    public void setMessages(MessageList messages) {
        this.messages = messages;
    }

    public Map<String, Object> getParamQuery() {
        return paramQuery;
    }

    public void setParamQuery(Map<String, Object> paramQuery) {
        this.paramQuery = paramQuery;
    }

    // ###### USE VALUES 
    public String getParam(String key) {
        return MapsGetter.getParameterClientMap().get(key);
    }

    public String getPageParam(String key) {
        return MapsGetter.getPageParameterMap().get(key);
    }

    public String getTxt(String key) {
        return MapsGetter.getMetaModelMap().get(key);
    }

    public String getAttrHidden(String key) {
        return MapsGetter.getAttributHiddenMap().get(key);
    }

    public List<LovsDTO> getLov(String table) {
        return MapsGetter.getLoadLovMap().get(table);
    }

    public String getConfig(String key) {
        return MapsGetter.getConfigBasicMap().get(key);
    }

    public String getPriv(String key) {
        return MapsGetter.getPrivilegesMap().get(key);
    }

    public Object getSession(String key) {
        return SessionsUtils.get(key);
    }

    public void setSession(String key, String val) {
        SessionsUtils.set(key, val);
    }

    public List<SelectItem> getLovSelectItem1(String table) {

        List<SelectItem> list = new ArrayList<>();

        //list.add(new SelectItem(1L, "val with Id = 1 "));
        for (LovsDTO o : getLov(table)) {
            list.add(new SelectItem(o.getId(), o.getName()));
        }
        return list;
    }

}
