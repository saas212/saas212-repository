package ma.mystock.module.pageManagement.beans;

import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import ma.mystock.module.pageManagement.messages.MessageDetail;
import ma.mystock.module.pageManagement.messages.MessageList;
import ma.mystock.web.utils.sessions.SessionsGetter;
import org.apache.log4j.Logger;

/**
 *
 * @author abdou
 *
 */
public abstract class AbstractPage extends GenericBean implements PageInterface {

    // info sur le le client
    protected Long cltclientId;

    // info sur le user 
    protected Long cltUserId;

    // info sur le module 
    protected Long cltModuleId;
    protected Long cltModuleTypeId;

    // info sur la page 
    protected Long pageId;
    protected boolean showForm;
    protected String operation;
    protected String subOperation;
    protected boolean isEditOperation;
    protected boolean isAddOperation;

    // info générale
    protected static Logger log = Logger.getLogger(AbstractPage.class);
    protected PageManagerBean pageManager;
    protected String languageCode;

    protected Long cltModuleIdToManager;
    protected Long cltClientIdToManager;

    protected String printUrl;
    protected String previewUrl;

    public AbstractPage() {
        try {
            initGeneric();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initGeneric() throws Exception {

        cltclientId = SessionsGetter.getCltClientId();
        cltUserId = SessionsGetter.getCltUserId();
        cltModuleId = SessionsGetter.getCltModuleId();
        cltModuleTypeId = SessionsGetter.getCltModuleTypeId();
        cltModuleIdToManager = 1L;//SessionsValues.getCltModule().getModuleTypeIdToManager();
        cltClientIdToManager = SessionsGetter.getCltModule().getClientId();

        System.out.println("cltModuleIdToManager : " + cltModuleIdToManager);
        System.out.println("cltClientIdToManager : " + cltClientIdToManager);
        languageCode = "";
    }

    public final void init() throws Exception {
        onInit();
    }

    /* à developper prochainement
     public final void add() {
     onAdd();
     }
     public final void edit() {
     onEdit();
     }
     public final void cancel() {
     onCancel();
     }
     */
    @Override
    public final void beforeRequest() {
        onBeforeRequest();
    }

    @Override
    public final void afterRrequest() {
        onAfterRrequest();
        messages.clear();
    }

    @Override
    public final void changelanguage(ActionEvent e) {
        onChangelanguage(e);
    }

    // Simple
    @Override
    public final void validateBeforeSave(ActionEvent e) {
        onValidateBeforeSave(e);
    }

    @Override
    public final void save(ActionEvent e) {
        log.info("save pbean");
        onSave(e);
    }

    @Override
    public final void delete(ActionEvent e) {
        onDelete(e);
    }

    @Override
    public final void cancel(ActionEvent e) {
        log.info("cancel event");
        onCancel(e);
    }

    //  AJAX 
    @Override
    public final void validateBeforeSavePartial(AjaxBehaviorEvent e) {
        onValidateBeforeSavePartial(e);
    }

    @Override
    public final void savePartial(AjaxBehaviorEvent e) {
        onSavePartial(e);
    }

    @Override
    public final void deletePartial(AjaxBehaviorEvent e) {
        onDeletePartial(e);
    }

    @Override
    public final void cancelPartial(AjaxBehaviorEvent e) {
        onCancelPartial(e);
    }

    // Getters & Setters
    @Override
    public MessageList getMessages() {
        return messages;
    }

    @Override
    public void setMessages(MessageList messages) {
        this.messages = messages;
    }

    @Override
    public Long getPageId() {
        return pageId;
    }

    @Override
    public void setPageId(Long pageId) {
        this.pageId = pageId;
    }

    public static Logger getLog() {
        return log;
    }

    public static void setLog(Logger log) {
        AbstractPage.log = log;
    }

    @Override
    public PageManagerBean getPageManager() {
        return pageManager;
    }

    @Override
    public void setPageManager(PageManagerBean pageManager) {
        this.pageManager = pageManager;
    }

    // Overrid les methodes qui sont pas obligatoire.
    @Override
    public void onInit() {
    }

    @Override
    public void onBeforeRequest() {
    }

    @Override
    public void onAfterRrequest() {
    }

    @Override
    public void onChangelanguage(ActionEvent e) {
    }

    @Override
    public void onValidateBeforeSave(ActionEvent e) {
    }

    @Override
    public void onSave(ActionEvent e) {
    }

    @Override
    public void onDelete(ActionEvent e) {
    }

    @Override
    public void onCancel(ActionEvent e) {
    }

    @Override
    public void onValidateBeforeSavePartial(AjaxBehaviorEvent e) {
    }

    @Override
    public void onSavePartial(AjaxBehaviorEvent e) {
    }

    @Override
    public void onDeletePartial(AjaxBehaviorEvent e) {
    }

    @Override
    public void onCancelPartial(AjaxBehaviorEvent e) {
    }

    public boolean isShowForm() {
        return showForm;
    }

    public void setShowForm(boolean showForm) {
        this.showForm = showForm;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public Long getCltModuleTypeId() {
        return cltModuleTypeId;
    }

    public void setCltModuleTypeId(Long cltModuleTypeId) {
        this.cltModuleTypeId = cltModuleTypeId;
    }

    public Long getCltUserId() {
        return cltUserId;
    }

    public void setCltUserId(Long cltUserId) {
        this.cltUserId = cltUserId;
    }

    public Long getCltclientId() {
        return cltclientId;
    }

    public void setCltclientId(Long cltclientId) {
        this.cltclientId = cltclientId;
    }

    public String getSubOperation() {
        return subOperation;
    }

    public void setSubOperation(String subOperation) {
        this.subOperation = subOperation;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public Long getCltModuleIdToManager() {
        return cltModuleIdToManager;
    }

    public void setCltModuleIdToManager(Long cltModuleIdToManager) {
        this.cltModuleIdToManager = cltModuleIdToManager;
    }

    public Long getCltClientIdToManager() {
        return cltClientIdToManager;
    }

    public void setCltClientIdToManager(Long cltClientIdToManager) {
        this.cltClientIdToManager = cltClientIdToManager;
    }

    public String getPrintUrl() {
        return printUrl;
    }

    public void setPrintUrl(String printUrl) {
        this.printUrl = printUrl;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public void setPreviewUrl(String previewUrl) {
        this.previewUrl = previewUrl;
    }

    public boolean isIsEditOperation() {
        return isEditOperation;
    }

    public void setIsEditOperation(boolean isEditOperation) {
        this.isEditOperation = isEditOperation;
    }

    public boolean isIsAddOperation() {
        return isAddOperation;
    }

    public void setIsAddOperation(boolean isAddOperation) {
        this.isAddOperation = isAddOperation;
    }

    public void addError(String itemCode, String value) {
        messages.add(new MessageDetail(1, itemCode, value));
    }

    public void addInfo(String itemCode, String value) {
        messages.add(new MessageDetail(2, itemCode, value));
    }

    public void addWarning(String itemCode, String value) {
        messages.add(new MessageDetail(3, itemCode, value));
    }

    public void addSuccess(String itemCode, String value) {
        messages.add(new MessageDetail(4, itemCode, value));
    }

    public void addNote(String itemCode, String value) {
        messages.add(new MessageDetail(5, itemCode, value));
    }

}
