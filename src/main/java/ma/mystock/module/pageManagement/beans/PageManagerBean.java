package ma.mystock.module.pageManagement.beans;

import ma.mystock.web.utils.springBeans.GloablsServices;
import ma.mystock.web.utils.springBeans.GloablsServices;
import ma.mystock.module.pageManagement.messages.MessageList;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;
import ma.mystock.core.dao.entities.views.VPmComposition;
import ma.mystock.core.dao.entities.views.dto.VPmModelDto;
import ma.mystock.core.dao.entities.views.dto.VPmCategorysDto;
import ma.mystock.core.dao.entities.views.dto.VPmGroupsDto;
import ma.mystock.core.dao.entities.views.dto.VPmMenusDto;
import ma.mystock.core.dao.entities.views.dto.VPmPagesDto;
import ma.mystock.web.utils.sessions.SessionsUtils;
import ma.mystock.module.pageManagement.messages.MessageDetail;
import ma.mystock.module.pageManagement.messages.Messages;
import ma.mystock.module.pageManagement.validations.Validator;
import ma.mystock.web.utils.routing.RedirectUtils;
import ma.mystock.web.utils.values.MapsGetter;
import ma.mystock.web.utils.sessions.SessionsGetter;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author abdou
 *
 */
@Component("pageManagerBean")
@Scope("session")
public class PageManagerBean extends GloablsServices implements Serializable {

    protected static Logger log = Logger.getLogger(PageManagerBean.class);

    /* ****************************** ATTRIBUTES ********************************/
    // Object Module  (DTO contient les categories, groupe et tous ...)
    private VPmModelDto selectedmodule;

    // Selected Menu (Object DTO ou une liste)
    private VPmMenusDto selectedMenu = new VPmMenusDto();

    // Selected Pages (DTO)
    private VPmPagesDto selectedPage;
    private List<VPmPagesDto> pages = new ArrayList<>();

    // Map pour toutes les pages
    private HashMap<String, PageInterface> beansMap = new HashMap<>(); // current pages 

    // Attributes utils
    private List<VPmMenusDto> PagesHeader = new ArrayList<>();
    private List<VPmMenusDto> PagesSubHeader = new ArrayList<>();
    private List<VPmCategorysDto> PagesPrincipal = new ArrayList<>();
    private List<VPmMenusDto> PagesFooter = new ArrayList<>();

    private Map<String, Object> paramQuery = new HashMap<>();

    // Attributes et funcion au niveau d'affichage // en cours 
    // Gesrion des langes ex (langChanged)
    // Gestion des messages selon les types 
    private boolean showSavedSuccess = false;
    private boolean showDeleteSuccess = false;

    private boolean showDashboard;
    // String (bocks) - Message (la liste des messages, type itemCode, value)
    private Messages messages = new Messages();

    /* ****************************** Event **** ****************************/
    @PostConstruct
    public void onInit() {

        // Redirection vers la page modules s'il y a un module non séléctionné
        if (SessionsGetter.getPmModelId() == null) {
            RedirectUtils.goToModule();
        }

        // header subheader menu footer ...
        initModel();

        // Afficher la liste des blocks dans la page Index.
        paramQuery.clear();
        paramQuery.put("modelId", SessionsGetter.getPmModelId());
        List<VPmComposition> pagesIndex = vPmCompositionEJB.executeQuery(VPmComposition.findByModelIdAndIndexShow, paramQuery);

        // Initialize selectedMenu par les pages index
        for (VPmComposition o : pagesIndex) {
            paramQuery.clear();
            paramQuery.put("id", o.getPageId());
            VPmPagesDto vPmPagesDto = vPmPagesDtoEJB.executeSingleQuery(VPmPagesDto.findById, paramQuery);
            pages.add(vPmPagesDto);
        }

        selectedMenu.setvPmPagesDtoList(pages);

        // Init Bean oue les pages qui sont dans selectedMenu
        initBeansMap();

        showDashboard = true;
        log.info("PageManagerBean : onInit");
    }

    public void goToDashBoard(ActionEvent e) {
        showDashboard = true;
    }

    public void onDestroy() {

        PagesHeader = new ArrayList<>();
        PagesSubHeader = new ArrayList<>();
        PagesPrincipal = new ArrayList<>();
        PagesFooter = new ArrayList<>();
        paramQuery = new HashMap<>();

        selectedMenu = new VPmMenusDto();
        pages = new ArrayList<>();

        beansMap = new HashMap<>();

        log.info("PageManagerBean : onDestroy");
    }

    public void onBeforeRequest() {

        if (selectedMenu != null) {

            for (VPmPagesDto pDto : selectedMenu.getvPmPagesDtoList()) {

                PageInterface page = beansMap.get(pDto.getId().toString());
                page.onBeforeRequest();

            }

        }

        // setter les messages error dans la session pour stylisé les inputs
        SessionsUtils.set("getErrorMessages", getErrorMessages());

        log.info("PageManagerBean : onBeforeRequest");
    }

    public void onAfterRequest() {

        if (selectedMenu != null) {
            for (VPmPagesDto pDto : selectedMenu.getvPmPagesDtoList()) {

                PageInterface page = beansMap.get(pDto.getId().toString());
                page.onAfterRrequest();

            }
        }

        messages.clear();
        showSavedSuccess = false;
        showDeleteSuccess = false;

        log.info("PageManagerBean : onAfterRequest");

    }

    public void onChangeMenu(Long id) {

        log.info("PageManagerBean : onChangeMenu");

    }

    public String onSelectMenu() {

        pages = selectedMenu.getvPmPagesDtoList();
        beansMap.clear();
        messages = new Messages();

        initBeansMap();

        log.info(" selectedMenu.id : " + selectedMenu.getId() + " selectedMenu.name : " + selectedMenu.getName());

        onChangeMenu(selectedMenu.getId());

        log.info("PageManagerBean : onSelectMenu");

        showDashboard = false;

        return "";

    }

    public String onSelectPage() {

        log.info("PageManagerBean : onSelectPage");

        return "";
    }

    /**
     * ***************************** UTILS *******************************
     */
    // change Menu ou change Page // faire qlq intruction
    public void initBeansMap() {

        if (pages != null) {

            // on se basent sur la liste pages 
            log.info("initializeBeansMap ...");

            for (VPmPagesDto o : pages) {
                //log.info("instances ... " + o.getId() + " || " + o.getClasse());
                try {
                    PageInterface p = getPage(o.getClasse());
                    if (p != null) {
                        //p.setLabel(o.getLabel());
                        p.setPageId(o.getId());
                        p.setPageManager(this);
                        beansMap.put("" + o.getId(), p);
                        messages.put("p" + p.getPageId(), p.getMessages());
                    }
                } catch (Exception ex) {

                    log.info("Error Loding page : " + o.getId() + " :  ");
                }

            }
        }
    }

    // set SelectedMenu & selectedPage // faire qlq choses dans cette methodes
    public void setSelectedPage(VPmPagesDto selectedPage) {
        this.selectedPage = selectedPage;
    }

    public void setSelectedMenu(VPmMenusDto selectedMenu) {
        this.selectedMenu = selectedMenu;
    }

    public void myException(String title, String message) {

        SessionsUtils.set("EXP_TITLE", title);
        SessionsUtils.set("EXP_MSG", message);

        // refirect to Exception
        RedirectUtils.goToException();

    }

    public void initModel() {
        //TODO
        /**
         * ######################### LOAD Modél au lieu de module
         * ################################
         */
        //System.out.println("1111");
        System.out.println(" initAllGroups for model :  " + SessionsGetter.getPmModelId());

        selectedmodule = MapsGetter.getModelMap().get(SessionsGetter.getPmModelId()); // get By Session ou by attributes

        if (SessionsGetter.getPmModelId() != null) {

            //System.out.println("    33333 ");
            //log.info("selectedmodule " + selectedmodule.getId());
            if (selectedmodule.getvPmGroupsDtoList() != null && !selectedmodule.getvPmGroupsDtoList().isEmpty()) {
                //log.info("selectedmodule.getvPmGroupsDtoList() " + selectedmodule.getvPmGroupsDtoList().size());
            } else {
                String title = "No Composition for This Module";
                String message = "il faut créer des compositions pour le module N° : " + selectedmodule.getId();
                //log.info("Redirect");
                myException(title, message);
                //log.info("after");
            }
            //log.info("No methode");

            //if (selectedmodule != null && selectedmodule.getvPmGroupsDtoList() != null && !selectedmodule.getvPmGroupsDtoList().isEmpty()) {
            for (VPmGroupsDto group : selectedmodule.getvPmGroupsDtoList()) {
                //System.out.println(" => group.getId()");
                if (group.getId().equals(1L)) {
                    //System.out.println(" Oui " + group.getvPmCategorysDtoList().size());
                    for (VPmCategorysDto category : group.getvPmCategorysDtoList()) {
                        //System.out.println(" entred 1");
                        for (VPmMenusDto menu : category.getvPmMenusDtoList()) {
                            //System.out.println("entred 2");
                            PagesHeader.add(menu);
                        }
                    }

                } else if (group.getId().equals(2L)) {

                    for (VPmCategorysDto category : group.getvPmCategorysDtoList()) {

                        for (VPmMenusDto menu : category.getvPmMenusDtoList()) {

                            PagesSubHeader.add(menu);
                        }
                    }

                } else if (group.getId().equals(3L)) {

                    for (VPmCategorysDto category : group.getvPmCategorysDtoList()) {
                        PagesPrincipal.add(category);
                    }

                } else if (group.getId().equals(4L)) {

                    for (VPmCategorysDto category : group.getvPmCategorysDtoList()) {
                        for (VPmMenusDto menu : category.getvPmMenusDtoList()) {
                            PagesFooter.add(menu);
                        }
                    }

                }

            }
        }

    }

    /* ****************************** ACTIONS ********************************/
    public void savePartial(AjaxBehaviorEvent e) throws Exception {

        System.out.println("pageManager  saveParial .... ");

        messages.clear();
        MessageList msg = new MessageList();

        for (VPmPagesDto pDto : selectedMenu.getvPmPagesDtoList()) {

            PageInterface page = beansMap.get(pDto.getId().toString());

            // vider all message ( NB : il faut supprimer que les messages d'erreurs)
            page.setMessages(new MessageList());
            
            // validate bean From DataBase
            Validator.validateBeanFromDataBase(page, msg);

            // validate Bean Before Save
            page.validateBeforeSavePartial(e);

            showSavedSuccess = false;

            page.savePartial(e);

            // copier les message form bean to map messages
            msg.addAll(page.getMessages());

            if (msg.getErrors().isEmpty()) {
                showSavedSuccess = true;
            }
            messages.put("" + pDto.getId(), msg);
        }
    }

    public void save(ActionEvent e) throws Exception {

        messages.clear();

        for (VPmPagesDto pDto : selectedMenu.getvPmPagesDtoList()) {
            PageInterface page = beansMap.get(pDto.getId().toString());
            // copier les message form bean to map messages
            MessageList msg = new MessageList();
            if (page.getMessages() != null && !page.getMessages().isEmpty()) {
                msg.addAll(page.getMessages());
            }
            if (page == null) {
                log.info("page is null");
            }
            //log.info("avant : validateBeanBeforeSave");
            Validator.validateBeanFromDataBase(page, msg);
            //log.info("après : validateBeanBeforeSave");
            page.validateBeforeSave(e);
            showSavedSuccess = false;
            page.save(e);

            //messages.put("", page.getMessages());
            if (msg.size() == 0) {
                showSavedSuccess = true;
            }
            messages.put("" + pDto.getId(), msg);
        }
    }

    private HashMap<String, HashMap<String, String>> validateBeforSave(ActionEvent e) {
        return null;
    }

    /* ****************************** MESSAGES ********************************/
    public MessageList getWorningMessages() {
        MessageList list = new MessageList();
        for (String o : messages.keySet()) {
            for (MessageDetail m : messages.get(o)) {
                if (MessageList.ID_MESSGAE_WARNING == m.getLevel()) {
                    list.add(m);
                }
            }
        }
        return list;
    }

    public MessageList getNoteMessages() {
        MessageList list = new MessageList();
        for (String o : messages.keySet()) {
            for (MessageDetail m : messages.get(o)) {
                if (MessageList.ID_MESSGAE_NOTE == m.getLevel()) {
                    list.add(m);
                }
            }
        }
        return list;
    }

    public MessageList getInfoMessages() {
        MessageList list = new MessageList();
        for (String o : messages.keySet()) {
            for (MessageDetail m : messages.get(o)) {
                if (MessageList.ID_MESSGAE_INFO == m.getLevel()) {
                    list.add(m);
                }
            }
        }
        return list;
    }

    public MessageList getSuccessMessages() {
        MessageList list = new MessageList();
        for (String o : messages.keySet()) {
            for (MessageDetail m : messages.get(o)) {
                if (MessageList.ID_MESSGAE_SUCCESS == m.getLevel()) {
                    list.add(m);
                }
            }
        }
        return list;
    }

    public MessageList getErrorMessages() {
        MessageList list = new MessageList();
        for (String o : messages.keySet()) {
            for (MessageDetail m : messages.get(o)) {
                if (MessageList.ID_MESSGAE_ERROR == m.getLevel()) {
                    list.add(m);
                }
            }
        }
        return list;
    }

    public void initBeanMap() throws Exception { // à supprimer

        /*
         // par les pages pardéafut .....
        
         // filtre by // clt module, group category menu 
         List<VPmComposition> l = vPmCompositionEJB.executeQuery(null);

         for (VPmComposition compos : l) {
         // get page add add to beanMap

         List<VPmPagesDto> list = vPmPagesDtoEJB.executeQuery(VPmPagesDto.findById);
         for (VPmPagesDto o : list) {
         log.info("Load pages ... " + o.getId() + " || " + o.getLabel());

         try {
         Page p = getPage(o.getClasse());
         if (p != null) {
         p.setLabel(o.getLabel());
         p.setPageId(o.getId());
         beansMap.put("" + o.getId(), p);
         }
         } catch (Exception ex) {
         log.info("Error Loding page : " + o.getId() + " :  " + o.getLabel());
         }
         }
         }

         // fin first 
         selectedMenu = null;
         */
        log.info("initBeanMap ...");
        List<VPmPagesDto> list = vPmPagesDtoEJB.executeQuery(VPmPagesDto.findAll);
        for (VPmPagesDto o : list) {
            log.info(o.getId() + " || ");
            try {
                PageInterface p = getPage(o.getClasse());
                if (p != null) {
                    //p.setLabel(o.getLabel());
                    p.setPageId(o.getId());
                    beansMap.put("" + o.getId(), p);
                }
            } catch (Exception ex) {
                log.info("Error Loding page : " + o.getId() + " :  ");
            }

        }
    }

    public PageInterface getPage(String clazz) {
        try {
            return (PageInterface) Class.forName(clazz).newInstance();
        } catch (Exception ex) {

            // set value  
            SessionsUtils.set("EXP_TITLE", "" + ex.getClass());

            SessionsUtils.set("EXP_MSG", "vous avez fait une errors lors la definition de la classe : " + clazz);

            // refirect to Exception
            RedirectUtils.goToException();

            log.info("Error getPage : -" + clazz + "- : attestion ne pas ajouter . java au path");
            ex.printStackTrace();
            return null;

        }
    }

    /* ****************************** GETTERS /SETTERS ********************************/
    public HashMap<String, PageInterface> getBeansMap() {

        if (beansMap == null || beansMap.isEmpty()) {
            initBeansMap();
        }

        return beansMap;
    }

    public void setBeansMap(HashMap<String, PageInterface> beansMap) {
        this.beansMap = beansMap;
    }

    public VPmPagesDto getSelectedPage() {
        return selectedPage;
    }

    public VPmMenusDto getSelectedMenu() {
        return selectedMenu;
    }

    public List<VPmMenusDto> getPagesHeader() {
        return PagesHeader;
    }

    public void setPagesHeader(List<VPmMenusDto> PagesHeader) {
        this.PagesHeader = PagesHeader;
    }

    public List<VPmMenusDto> getPagesSubHeader() {
        return PagesSubHeader;
    }

    public void setPagesSubHeader(List<VPmMenusDto> PagesSubHeader) {
        this.PagesSubHeader = PagesSubHeader;
    }

    public List<VPmCategorysDto> getPagesPrincipal() throws Exception {
        return PagesPrincipal;
    }

    public void setPagesPrincipal(List<VPmCategorysDto> PagesPrincipal) {
        this.PagesPrincipal = PagesPrincipal;
    }

    public List<VPmMenusDto> getPagesFooter() {
        return PagesFooter;
    }

    public void setPagesFooter(List<VPmMenusDto> PagesFooter) {
        this.PagesFooter = PagesFooter;
    }

    public VPmModelDto getSelectedmodule() {
        return selectedmodule;
    }

    public void setSelectedmodule(VPmModelDto selectedmodule) {
        this.selectedmodule = selectedmodule;
    }

    public Messages getMessages() {

        //MessageList messageList = new MessageList();
        //messageList.add(new MessageDetail(2, "itemCode", "value1111"));
        //messages.put("page1", messageList);
        //log.info(" ++++++++++++++++++++++++++++ Messages ++++++++++++++++++++++++++++ ");
        for (String page : messages.keySet()) {
            //log.info("      => Messages Page : " + page);
            for (MessageDetail msg : messages.get(page)) {
                //log.info("            => Messages value : " + msg.getValue());
            }
        }
        //log.info(" ++++++++++++++++++++++++++++ Messages ++++++++++++++++++++++++++++ ");

        return messages;
    }

    public void setMessages(Messages messages) {
        this.messages = messages;
    }

    public boolean isShowSavedSuccess() {
        return showSavedSuccess;
    }

    public void setShowSavedSuccess(boolean showSavedSuccess) {
        this.showSavedSuccess = showSavedSuccess;
    }

    public List<VPmPagesDto> getPages() {
        return pages;
    }

    public void setPages(List<VPmPagesDto> pages) {
        this.pages = pages;
    }

    public String getBeforeRequest() {
        onBeforeRequest();
        return "";
    }

    public String getAfterRequest() {

        onAfterRequest();
        getMessages();
        return "";
    }

    public boolean isShowDeleteSuccess() {
        return showDeleteSuccess;
    }

    public void setShowDeleteSuccess(boolean showDeleteSuccess) {
        this.showDeleteSuccess = showDeleteSuccess;
    }

    public static Logger getLog() {
        return log;
    }

    public static void setLog(Logger log) {
        PageManagerBean.log = log;
    }

    public Map<String, Object> getParamQuery() {
        return paramQuery;
    }

    public void setParamQuery(Map<String, Object> paramQuery) {
        this.paramQuery = paramQuery;
    }

    public boolean isShowDashboard() {
        return showDashboard;
    }

    public void setShowDashboard(boolean showDashboard) {
        this.showDashboard = showDashboard;
    }

}
