/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.module.pageManagement.beans;

import ma.mystock.web.utils.sessions.SessionsGetter;
import org.apache.log4j.Logger;

/**
 *
 * @author abdou
 *
 */
public abstract class AbstractPdf extends GenericBean implements PdfInterface {

    protected Long cltclientId;

    protected Long cltUserId;

    protected Long cltModuleId;
    protected Long cltModuleTypeId;

    protected Long pageId;
    protected boolean showForm;
    protected String operation;
    protected String subOperation;

    protected static Logger log = Logger.getLogger(AbstractPdf.class);

    protected String languageCode;

    protected Long cltModuleIdToManager;
    protected Long cltClientIdToManager;

    public AbstractPdf() {
        try {
            initGeneric();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initGeneric() throws Exception {

        cltclientId = SessionsGetter.getCltClientId();
        cltUserId = SessionsGetter.getCltUserId();
        cltModuleId = SessionsGetter.getCltModuleId();
        cltModuleTypeId = SessionsGetter.getCltModuleTypeId();
        cltModuleIdToManager = 1L;
        cltClientIdToManager = 1L;

        languageCode = "";
    }

    @Override
    public void onInit() {
    }

    public final void init() throws Exception {
        onInit();
    }

    @Override
    public Long getPageId() {
        return pageId;
    }

    @Override
    public void setPageId(Long pageId) {
        this.pageId = pageId;
    }

    public static Logger getLog() {
        return log;
    }

    public static void setLog(Logger log) {
        AbstractPdf.log = log;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public Long getCltModuleTypeId() {
        return cltModuleTypeId;
    }

    public void setCltModuleTypeId(Long cltModuleTypeId) {
        this.cltModuleTypeId = cltModuleTypeId;
    }

    public Long getCltUserId() {
        return cltUserId;
    }

    public void setCltUserId(Long cltUserId) {
        this.cltUserId = cltUserId;
    }

    public Long getCltclientId() {
        return cltclientId;
    }

    public void setCltclientId(Long cltclientId) {
        this.cltclientId = cltclientId;
    }

    public String getSubOperation() {
        return subOperation;
    }

    public void setSubOperation(String subOperation) {
        this.subOperation = subOperation;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public Long getCltModuleIdToManager() {
        return cltModuleIdToManager;
    }

    public void setCltModuleIdToManager(Long cltModuleIdToManager) {
        this.cltModuleIdToManager = cltModuleIdToManager;
    }

    public Long getCltClientIdToManager() {
        return cltClientIdToManager;
    }

    public void setCltClientIdToManager(Long cltClientIdToManager) {
        this.cltClientIdToManager = cltClientIdToManager;
    }

    @Override
    public void beforeRequest() {
    }

    @Override
    public void afterRrequest() {

    }

    @Override
    public void onBeforeRequest() {

    }

    @Override
    public void onAfterRrequest() {

    }

}
