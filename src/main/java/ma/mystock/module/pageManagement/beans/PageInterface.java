/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.module.pageManagement.beans;

import java.io.Serializable;
import ma.mystock.module.pageManagement.messages.MessageList;
import javax.faces.event.ActionEvent;
import javax.faces.event.AjaxBehaviorEvent;

/**
 *
 * @author abdou
 */
public interface PageInterface extends Serializable {

    /*
     ################### USE BY Abstruct PAGE ###################
     */
    // All
    public void beforeRequest();

    public void afterRrequest();

    public void changelanguage(ActionEvent e);

    // Simple
    public void validateBeforeSave(ActionEvent e);

    public void save(ActionEvent e);

    public void delete(ActionEvent e);

    public void cancel(ActionEvent e);

    //Ajax
    public void validateBeforeSavePartial(AjaxBehaviorEvent e);

    public void savePartial(AjaxBehaviorEvent e);

    public void deletePartial(AjaxBehaviorEvent e);

    public void cancelPartial(AjaxBehaviorEvent e);

    // Getters & Setters
    public MessageList getMessages();

    public void setMessages(MessageList messages);

    public Long getPageId();

    public void setPageId(Long pageId);

    public PageManagerBean getPageManager();

    public void setPageManager(PageManagerBean pageManager);

    /*
     ################### USE BY ALL PAGES ###################
     */
    //All
    public void onInit();

    public void onBeforeRequest();

    public void onAfterRrequest();

    public void onChangelanguage(ActionEvent e);

    // Simple 
    public void onValidateBeforeSave(ActionEvent e);

    public void onSave(ActionEvent e);

    public void onDelete(ActionEvent e);

    public void onCancel(ActionEvent e);

    // Ajax
    public void onValidateBeforeSavePartial(AjaxBehaviorEvent e);

    public void onSavePartial(AjaxBehaviorEvent e);

    public void onDeletePartial(AjaxBehaviorEvent e);

    public void onCancelPartial(AjaxBehaviorEvent e);

}
