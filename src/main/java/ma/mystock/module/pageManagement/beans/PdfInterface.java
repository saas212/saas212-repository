/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.module.pageManagement.beans;

import ma.mystock.module.pageManagement.messages.MessageList;

/**
 *
 * @author abdou
 */
public interface PdfInterface {

    /*
     ################### USE BY Abstruct PAGE ###################
     */
    public void beforeRequest();

    public void afterRrequest();

    // Getters & Setters
    public MessageList getMessages();

    public void setMessages(MessageList messages);

    public Long getPageId();

    public void setPageId(Long pageId);

    /*
     ################### USE BY ALL PAGES ###################
     */
    //All
    public void onInit();

    public void onBeforeRequest();

    public void onAfterRrequest();

}
