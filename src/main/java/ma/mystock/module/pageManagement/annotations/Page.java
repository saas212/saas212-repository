/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.module.pageManagement.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 *
 * @author abdou
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Page {

    public String id() default "";

    public String description() default "";
}
