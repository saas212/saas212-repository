package ma.mystock.module.pageManagement.validations;

public interface ValidatorInterface {

    public String getId();

    public void setParams(String params);

    public boolean validate(Object o) throws Exception;

}
