/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.module.pageManagement.validations;

import ma.mystock.module.pageManagement.annotations.Attribute;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import ma.mystock.core.dao.entities.views.VPmValidation;
import ma.mystock.module.pageManagement.beans.PageInterface;
import ma.mystock.module.pageManagement.messages.MessageList;
import ma.mystock.web.utils.values.MapsGetter;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author abdou
 */
public class Validator {

    protected static Logger log = Logger.getLogger(Validator.class);

    private static ValidatorsFactory validatorsFactory;

    static {
        validatorsFactory = ValidatorsFactory.getInstance();
    }

    public static void validateBeanBySectionId(PageInterface page, MessageList msg, int section) {

        Long id = page.getPageId();

        log.info("validating before save for : " + "page_" + id + " " + page.getClass());

        HashMap<String, List<VPmValidation>> blockMap = MapsGetter.getPageValidationMap().get(id.toString());
        // pageId + liste 

        if (blockMap == null) {
            return;
        }

        if (page != null && page.getClass() != null) {
            Field[] fields = page.getClass().getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(Attribute.class)) {
                    Attribute annotation = field.getAnnotation(Attribute.class);
                    if (annotation != null) {

                        //if()
                        //codes.add(annotation.code());
                        if (section != 0) {
                            log.info("validate section s" + section);
                            if (!annotation.itemCode().contains("s" + section)) {
                                //log.info("continue : " + annotation.itemCode());
                                continue;
                            }
                        } else {
                            //log.info("All validation");
                        }

                        log.info("s" + section + " validation for annotation :" + annotation.itemCode());

                        List<VPmValidation> validatorsList = blockMap.get(annotation.itemCode());

                        if (validatorsList != null) {
                            for (VPmValidation fbBlockFieldValidTypeDTO : validatorsList) {
                                log.info(annotation.itemCode() + " : " + fbBlockFieldValidTypeDTO.getValidationId());
                                //log.info("Avant basic");
                                ValidatorInterface basicValidator = validatorsFactory.getValidator("" + fbBlockFieldValidTypeDTO.getValidationId());
                                //log.info("Après validato c => " + fbBlockFieldValidTypeDTO.getValidationId());

                                if (basicValidator != null) {

                                    basicValidator.setParams(fbBlockFieldValidTypeDTO.getParams());

                                    Object fieldValue = null;
                                    try {
                                        fieldValue = PropertyUtils.getProperty(page, field.getName());
                                    } catch (Exception ex) {
                                        log.info("ex :  = PropertyUtils.getProperty(page, field.getName());");
                                    }

                                    log.info("params : " + fbBlockFieldValidTypeDTO.getParams() + "   field value : " + fieldValue);

                                    try {

                                        if (hasValidate(fbBlockFieldValidTypeDTO)) {

                                            if (!basicValidator.validate(fieldValue)) {

                                                msg.setTxtError(annotation.itemCode(), constructErrorMessage(fbBlockFieldValidTypeDTO.getErrorMessage(), annotation.itemCode(), fbBlockFieldValidTypeDTO.getParams()));

                                            }

                                        }

                                    } catch (Exception ex) {
                                        log.info("Ecep validation ");
                                        ex.printStackTrace();
                                    }
                                }

                            }
                        }

                    }

                }
            }

        }

    }

    public static void validateBeanFromDataBase(PageInterface page, MessageList msg) {
        Long id = page.getPageId();

        log.info("vvalidateBeanBeforeSave for : " + "page_" + id);

        HashMap<String, List<VPmValidation>> pageValidationMap = MapsGetter.getPageValidationMap().get(id.toString());

        if (pageValidationMap == null) {
            return;
        }

        log.info("page_" + id + " (" + pageValidationMap.size() + ") et class :" + page.getClass());
        if (page != null && page.getClass() != null) {
            Field[] fields = page.getClass().getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(Attribute.class)) {
                    Attribute annotation = field.getAnnotation(Attribute.class);
                    if (annotation != null) {

                        List<VPmValidation> validatorsList = pageValidationMap.get(annotation.itemCode());

                        if (validatorsList != null) {
                            log.info("validate annotation :" + annotation.itemCode());

                            for (VPmValidation validation : validatorsList) {

                                ValidatorInterface basicValidator = validatorsFactory.getValidator("" + validation.getValidationId());

                                log.info("  validationId : " + validation.getValidationId() + " et params : " + validation.getParams());

                                if (basicValidator != null) {

                                    basicValidator.setParams(validation.getParams());

                                    Object fieldValue = null;
                                    try {
                                        fieldValue = PropertyUtils.getProperty(page, field.getName());
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }

                                    log.info("field value : " + fieldValue);

                                    try {

                                        if (!basicValidator.validate(fieldValue)) {

                                            msg.setTxtError(annotation.itemCode(), constructErrorMessage(validation.getErrorMessage(), annotation.itemCode(), validation.getParams()));

                                        }

                                    } catch (Exception ex) {
                                        log.info("Ecep validation ");
                                        ex.printStackTrace();
                                    }
                                }

                            }
                        } else {
                            log.info("no validate for : " + annotation.itemCode());
                        }

                    }

                }
            }

        }
    }

    // Vérifier est ce que le module est null ou égal à le module courrant 
    public static boolean hasValidate(VPmValidation vPmValidation) {

        return true;
        /*
         boolean drapo = false;
         Long validationModule = vPmValidation.getCltModuleId();
         log.info(SessionsValues.getCltModuleId() + " - validationModule === +" + validationModule);
         if (validationModule == null) {
         validationModule = 0L;
         }

         if (validationModule == 0 || SessionsValues.getCltModuleId().equals(validationModule)) {
         drapo = true;
         }
         return drapo;
         */
    }

    private static String constructErrorMessage(String eitemCodeValidation, String eitemCodeField, String params) {

        String messageToFormat = MapsGetter.getMetaModelMap().get(eitemCodeValidation + ".error");

        System.out.println("eitemCodeValidation" + " - " + messageToFormat);
        String[] paramArray1 = null;

        try {
            paramArray1 = params.split(",");
        } catch (Exception ex) {
            paramArray1 = new String[(1)];
        }

        String[] paramArray = new String[(paramArray1.length != 0 ? 1 + paramArray1.length : 1)];
        paramArray[0] = MapsGetter.getMetaModelMap().get(eitemCodeField + ".label");

        String message = messageToFormat.replace("{0}", paramArray[0]);

        try {
            for (int i = 0; i < paramArray1.length; i++) {
                paramArray[i + 1] = paramArray1[i];
                message = message.replace("{" + String.valueOf(i + 1) + "}", paramArray[i + 1]);
            }
        } catch (Exception ex) {
        }

        return message;

    }

}
