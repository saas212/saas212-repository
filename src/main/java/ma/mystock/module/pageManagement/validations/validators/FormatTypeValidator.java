package ma.mystock.module.pageManagement.validations.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ma.mystock.module.pageManagement.validations.AbstractRuleValidator;

public class FormatTypeValidator extends AbstractRuleValidator {

    public static String EmailFormatValidator_ID = "5";

    public FormatTypeValidator() {
        super(EmailFormatValidator_ID);
    }

    @Override
    public boolean validIfNullOrEmpty() {
        return true;
    }

    @Override
    public boolean validate(Object o) throws Exception {
        String value = (String) o;

        if (value == null || "".equals(value)) {
            return super.validate(o);
        }
        boolean isValid = false;
        String expression;

        if ("mail".equalsIgnoreCase(value)) {
            expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            isValid = checkException(params, expression);
        } else if ("phone".equalsIgnoreCase(value)) {
            expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            isValid = checkException(params, expression);
        } else if ("fax".equalsIgnoreCase(value)) {
            expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            isValid = checkException(params, expression);
        } else if ("codePostale".equalsIgnoreCase(value)) {
            expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            isValid = checkException(params, expression);
        } else if ("date".equalsIgnoreCase(value)) {
            expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            isValid = checkException(params, expression);
        } else if ("time".equalsIgnoreCase(value)) {
            expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            isValid = checkException(params, expression);
        } else if ("datetime".equalsIgnoreCase(value)) {
            expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            isValid = checkException(params, expression);
        }
        return isValid;
    }

    public boolean checkException(String value, String expression) {
        if (value.length() > 0) {
            CharSequence inputStr = value;
            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(inputStr);
            if (matcher.matches()) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

}
