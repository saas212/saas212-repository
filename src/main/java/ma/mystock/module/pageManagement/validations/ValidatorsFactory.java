package ma.mystock.module.pageManagement.validations;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import ma.mystock.module.pageManagement.validations.validators.DataTypeValidator;
import ma.mystock.module.pageManagement.validations.validators.FormatTypeValidator;
import ma.mystock.module.pageManagement.validations.validators.MaxLengthValidator;
import ma.mystock.module.pageManagement.validations.validators.MaxWordValidator;
import ma.mystock.module.pageManagement.validations.validators.RequiredValidator;

public class ValidatorsFactory {

    private static ValidatorsFactory _instance;

    private List<AbstractRuleValidator> availableValidators;

    private HashMap<String, ValidatorInterface> availableValidatorsMap = new HashMap<>();

    private ValidatorsFactory() {
        availableValidators = Arrays.asList(new RequiredValidator(),
                new MaxLengthValidator(),
                new MaxWordValidator(),
                new DataTypeValidator(),
                new FormatTypeValidator());

        for (ValidatorInterface validatorInterface : availableValidators) {
            availableValidatorsMap.put(validatorInterface.getId(), validatorInterface);
        }

    }

    public static synchronized ValidatorsFactory getInstance() {
        if (_instance == null) {
            _instance = new ValidatorsFactory();
        }
        return _instance;
    }

    public ValidatorInterface getValidator(String id) {
        return availableValidatorsMap.get(id);
    }

}
