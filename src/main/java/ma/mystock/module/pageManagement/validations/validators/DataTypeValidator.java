package ma.mystock.module.pageManagement.validations.validators;

import java.util.Date;
import ma.mystock.module.pageManagement.validations.AbstractRuleValidator;

public class DataTypeValidator extends AbstractRuleValidator {

    public static String EmailFormatValidator_ID = "4";

    public DataTypeValidator() {
        super(EmailFormatValidator_ID);
    }

    @Override
    public boolean validIfNullOrEmpty() {
        return true;
    }

    @Override
    public boolean validate(Object o) throws Exception {
        String str = (String) o;

        if (str == null || "".equals(str)) {
            return super.validate(o);
        }
        String value = (String) o;

        if ("Char".equalsIgnoreCase(params)) {

            try {
                return value.length() == 1;
            } catch (Exception e) {
                return false;
            }
        } else if ("Integer".equalsIgnoreCase(params)) {
            try {
                Long.parseLong(value);
            } catch (Exception e) {
                return false;
            }

        } else if ("Double".equalsIgnoreCase(params)) {
            try {
                Double.parseDouble(value);
            } catch (Exception e) {
                return false;
            }

        } else if ("DateTime".equalsIgnoreCase(params)) {
            try {
                //Date.parse(value);
            } catch (Exception e) {
                return false;
            }

        }

        return true;
    }

    public boolean checkDataType(String str, String type) {

        log.info("Check data type (" + str + ", " + type + ")");

        boolean isValid = false;

        if ("string".equalsIgnoreCase(type)) {
            isValid = true;
        } else if ("integer".equalsIgnoreCase(type)) {

            try {
                Long.valueOf(str);
                isValid = true;
            } catch (NumberFormatException ex) {
                isValid = false;
            }

        } else if ("double".equalsIgnoreCase(type)) {

            try {
                Double.valueOf(str);
                isValid = true;
            } catch (NumberFormatException ex) {
                isValid = false;
            }

        } else if ("char".equalsIgnoreCase(type)) {

            try {
                isValid = str.length() == 1;
            } catch (Exception ex) {
                isValid = false;
            }

        } else if ("date".equalsIgnoreCase(type)) {

        } else if ("time".equalsIgnoreCase(type)) {

        } else if ("datetime".equalsIgnoreCase(type)) {

        }

        return isValid;
    }

}
