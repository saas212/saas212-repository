package ma.mystock.module.pageManagement.validations.validators;

import ma.mystock.module.pageManagement.validations.AbstractRuleValidator;

/**
 *
 * @author abdou
 */
public class MaxLengthValidator extends AbstractRuleValidator {

    public static String maxLengthValidatorId = "2";

    public MaxLengthValidator() {
        super(maxLengthValidatorId);
    }

    @Override
    public boolean validate(Object o) throws Exception {

        String val = (String) o;
        boolean isValid = false;

        if (val == null || "".equals(val)) {
            return super.validate(o);
        }

        try {

            long value = ((String) o).length();
            long lenght = Long.valueOf(params);

            if (value <= lenght) {
                isValid = true;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }

        return isValid;

    }

    @Override
    public boolean validIfNullOrEmpty() {
        return true;
    }

}
