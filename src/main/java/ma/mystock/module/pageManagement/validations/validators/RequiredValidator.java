package ma.mystock.module.pageManagement.validations.validators;

import ma.mystock.module.pageManagement.validations.AbstractRuleValidator;

public class RequiredValidator extends AbstractRuleValidator {

    public static String requiredValidatorId = "1";

    public RequiredValidator() {
        super(requiredValidatorId);
    }

    @Override
    public boolean validate(Object o) {

        String str = (String) o;
        return !(str == null || "".equals(str));
    }

    @Override
    public boolean validIfNullOrEmpty() {
        return false;
    }

}
