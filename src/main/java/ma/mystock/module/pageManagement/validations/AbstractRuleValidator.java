package ma.mystock.module.pageManagement.validations;

import org.apache.log4j.Logger;

public abstract class AbstractRuleValidator implements ValidatorInterface {

    protected String id;
    protected String params;

    protected static Logger log = Logger.getLogger(AbstractRuleValidator.class);

    public AbstractRuleValidator(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setParams(String _params) {
        params = _params;
    }

    public abstract boolean validIfNullOrEmpty();

    public boolean validate(Object o) throws Exception {
        String str = (String) o;
        if (str == null || "".equals(str)) {
            return validIfNullOrEmpty();
        }
        return validIfNullOrEmpty();
    }
}
