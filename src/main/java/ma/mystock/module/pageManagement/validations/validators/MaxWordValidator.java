package ma.mystock.module.pageManagement.validations.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import ma.mystock.module.pageManagement.validations.AbstractRuleValidator;

/**
 *
 * @author abdou
 */
public class MaxWordValidator extends AbstractRuleValidator {

    public static String maxWordValidatorId = "3";

    public MaxWordValidator() {
        super(maxWordValidatorId);
    }

    @Override
    public boolean validate(Object o) throws Exception {

        String val = (String) o;
        boolean isValid = false;

        if (val == null || "".equals(val)) {
            return super.validate(o);
        }

        try {

            long value = wordCounter((String) o);
            long lenght = Long.valueOf(params);

            if (value <= lenght) {
                isValid = true;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }

        return isValid;

    }

    private long wordCounter(String s) {

        if (s == null || "".equals(s)) {
            return 0;
        }
        long count = 0;

        Pattern pattern = Pattern.compile("[a-zA-Z0-9éèêëàáâäóòôöíìîïçÉÈÊËÀÁÂÄÒÓÔÖÌÍÎÏÇ-]+");
        Matcher matcher = pattern.matcher(s);
        while (matcher.find()) {
            count++;
        }
        return count;
    }

    @Override
    public boolean validIfNullOrEmpty() {
        return true;
    }

}
