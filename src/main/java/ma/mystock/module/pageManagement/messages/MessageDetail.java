package ma.mystock.module.pageManagement.messages;

/**
 *
 * @author abdou
 */
public class MessageDetail {

    private String itemCode;
    private String value;
    private int level; // 1 success, 2 errors, 3 wrans, 4 infos  ... voir Message List
    private int scope; // request, session, enum 

    public MessageDetail(int level, String itemCode, String value) {
        this.itemCode = itemCode;
        this.value = value;
        this.level = level;
    }

    public MessageDetail() {

    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getScope() {
        return scope;
    }

    public void setScope(int scope) {
        this.scope = scope;
    }

}
