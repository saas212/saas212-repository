package ma.mystock.module.pageManagement.messages;

import java.util.ArrayList;
import ma.mystock.web.utils.values.MapsGetter;

/**
 *
 * @author abdou
 */
public class MessageList extends ArrayList<MessageDetail> {

    public static int ID_MESSGAE_ERROR = 1;
    public static int ID_MESSGAE_INFO = 2;
    public static int ID_MESSGAE_WARNING = 3;
    public static int ID_MESSGAE_SUCCESS = 4;
    public static int ID_MESSGAE_NOTE = 5;

    public ArrayList<MessageDetail> getErrors() {
        MessageList list = new MessageList();
        for (MessageDetail m : this.subList(0, this.size())) {
            if (ID_MESSGAE_ERROR == m.getLevel()) {
                list.add(m);
            }
        }
        return list;
    }

    public ArrayList<MessageDetail> getInfos() {
        MessageList list = new MessageList();
        for (MessageDetail m : this.subList(0, this.size())) {
            if (ID_MESSGAE_INFO == m.getLevel()) {
                list.add(m);
            }
        }
        return list;
    }

    public ArrayList<MessageDetail> getWarnings() {
        MessageList list = new MessageList();
        for (MessageDetail m : this.subList(0, this.size())) {
            if (ID_MESSGAE_WARNING == m.getLevel()) {
                list.add(m);
            }
        }
        return list;
    }

    public ArrayList<MessageDetail> getSuccess() {
        MessageList list = new MessageList();
        for (MessageDetail m : this.subList(0, this.size())) {
            if (ID_MESSGAE_SUCCESS == m.getLevel()) {
                list.add(m);
            }
        }
        return list;
    }

    public ArrayList<MessageDetail> getNotes() {
        MessageList list = new MessageList();
        for (MessageDetail m : this.subList(0, this.size())) {
            if (ID_MESSGAE_NOTE == m.getLevel()) {
                list.add(m);
            }
        }
        return list;
    }

    public void setError(String item, String msg) {
        super.add(new MessageDetail(ID_MESSGAE_ERROR, item, MapsGetter.getMetaModelMap().get(msg)));
    }

    public void setInfo(String item, String msg) {
        super.add(new MessageDetail(ID_MESSGAE_INFO, item, MapsGetter.getMetaModelMap().get(msg)));
    }

    public void setWarn(String item, String msg) {
        super.add(new MessageDetail(ID_MESSGAE_WARNING, item, MapsGetter.getMetaModelMap().get(msg)));
    }

    public void setSuccess(String item, String msg) {
        super.add(new MessageDetail(ID_MESSGAE_SUCCESS, item, MapsGetter.getMetaModelMap().get(msg)));
    }

    public void setNote(String item, String msg) {
        super.add(new MessageDetail(ID_MESSGAE_NOTE, item, MapsGetter.getMetaModelMap().get(msg)));
    }

    public void setTxtError(String item, String msg) {
        add(new MessageDetail(ID_MESSGAE_ERROR, item, msg));
    }

    public void setTxtInfo(String item, String msg) {
        add(new MessageDetail(ID_MESSGAE_INFO, item, msg));
    }

    public void setTxtWarn(String item, String msg) {
        add(new MessageDetail(ID_MESSGAE_WARNING, item, msg));
    }

    public void setTxtSuccess(String item, String msg) {
        add(new MessageDetail(ID_MESSGAE_SUCCESS, item, msg));
    }

    public void setTxtNote(String item, String msg) {
        add(new MessageDetail(ID_MESSGAE_NOTE, item, msg));
    }
}
