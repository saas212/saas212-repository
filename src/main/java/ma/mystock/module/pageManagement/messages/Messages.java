package ma.mystock.module.pageManagement.messages;

import java.util.HashMap;

/**
 *
 * @author abdou
 */
public class Messages extends HashMap<String, MessageList> {

    @Override
    public MessageList get(Object key) {
        return super.get(key);
    }

    public MessageList getErrors(Object key) {
        return super.get(key);
    }

    public MessageList getSuccess(Object key) {
        return super.get(key);
    }

    // for all messgaes    id du page  message
    //Messages extends HashMap<String, MessageList extends ArrayList<Object>>
}
