/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock;

import java.awt.Image;
import java.io.InputStream;
import ma.mystock.web.utils.jsf.FacesUtils;
import org.xhtmlrenderer.layout.SharedContext;
import org.xhtmlrenderer.pdf.ITextOutputDevice;
import org.xhtmlrenderer.pdf.ITextUserAgent;
import org.xhtmlrenderer.resource.CSSResource;
import org.xhtmlrenderer.resource.ImageResource;
import org.xhtmlrenderer.resource.XMLResource;

/**
 *
 * @author Anni
 */
public class FlyingUserAgent extends ITextUserAgent {

    private static final int IMAGE_CACHE_CAPACITY = 32;
    private SharedContext _sharedContext;
    private ITextOutputDevice _outputDevice;
    private String proxyUrl;
    private String httpUrl;

    public FlyingUserAgent(ITextOutputDevice outputDevice, String proxyUrl, String httpUrl) {
        super(outputDevice);
        this.proxyUrl = proxyUrl;
        this.httpUrl = httpUrl;
    }

    public FlyingUserAgent(ITextOutputDevice outputDevice) {
        super(outputDevice);
    }

    @Override
    public ImageResource getImageResource(String uri) {
        if (uri != null) {
            String newUri = decodeUri(uri);
            //if(uri!=null && uri.startsWith(proxyUrl)){
            return super.getImageResource(newUri);
            // }
        }
        return super.getImageResource(uri); //To change body of generated methods, choose Tools | Templates.

    }

    @Override
    public SharedContext getSharedContext() {
        return super.getSharedContext(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setSharedContext(SharedContext sharedContext) {
        super.setSharedContext(sharedContext); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void shrinkImageCache() {
        super.shrinkImageCache(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clearImageCache() {
        super.clearImageCache(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected InputStream resolveAndOpenStream(String uri) {
        if (uri != null) {
            String newUri = decodeUri(uri);
            //if(uri!=null && uri.startsWith(proxyUrl)){
            return super.resolveAndOpenStream(newUri);
            // }
        }

        return super.resolveAndOpenStream(uri); //To change body of generated methods, choose Tools | Templates.

    }

    @Override
    public CSSResource getCSSResource(String uri) {
        if (uri != null) {
            String newUri = decodeUri(uri);
            //if(uri!=null && uri.startsWith(proxyUrl)){
            return super.getCSSResource(newUri);
            // }
        }
//         if(uri!=null && uri.startsWith(proxyUrl)){
//            return super.getCSSResource(uri.replace(proxyUrl, httpUrl));
//        }

        return super.getCSSResource(uri); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected ImageResource createImageResource(String uri, Image img) {
        if (uri != null) {
            String newUri = decodeUri(uri);
            //if(uri!=null && uri.startsWith(proxyUrl)){
            return super.createImageResource(newUri, img);
            // }
        }

        return super.createImageResource(uri, img);

    }

    @Override
    public XMLResource getXMLResource(String uri) {
        if (uri != null) {
            String newUri = decodeUri(uri);
            //if(uri!=null && uri.startsWith(proxyUrl)){
            return super.getXMLResource(newUri);
            // }
        }
//         if(uri!=null && uri.startsWith(proxyUrl)){
//            return super.getXMLResource(uri.replace(proxyUrl, httpUrl));
//        }

        return super.getXMLResource(uri);

    }

    @Override
    public byte[] getBinaryResource(String uri) {
        if (uri != null) {
            String newUri = decodeUri(uri);
            //if(uri!=null && uri.startsWith(proxyUrl)){
            return super.getBinaryResource(newUri);
            // }
        }

        return super.getBinaryResource(uri);

    }

    @Override
    public boolean isVisited(String uri) {
        if (uri != null) {
            String newUri = decodeUri(uri);
            //if(uri!=null && uri.startsWith(proxyUrl)){
            return super.isVisited(newUri);
            // }
        }

        return super.isVisited(uri);
        // return super.isVisited(uri); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setBaseURL(String url) {

        super.setBaseURL(url); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String resolveURI(String uri) {
        if (uri != null) {
            String newUri = decodeUri(uri);
            //if(uri!=null && uri.startsWith(proxyUrl)){
            return super.resolveURI(newUri);
            // }
        }
        return super.resolveURI(uri); //To change body of generated methods, choose Tools | Templates.

    }

    @Override
    public String getBaseURL() {
        return super.getBaseURL(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void documentStarted() {
        super.documentStarted(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void documentLoaded() {
        super.documentLoaded(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onLayoutException(Throwable t) {
        super.onLayoutException(t); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void onRenderException(Throwable t) {
        super.onRenderException(t); //To change body of generated methods, choose Tools | Templates.
    }

    private String decodeUri(String uri) {
        System.out.println("-----------------------------------------------------------------------");
        System.out.println("Old uri " + uri);
        System.out.println("-----------------------------------------------------------------------");
        if (uri.contains("evision.ca")) {
            String url = uri.substring(uri.indexOf("evision.ca") + (FacesUtils.getServletContext().getContextPath().length() + 11));
            System.out.println("url = :" + url);

            System.out.println("uri = :" + httpUrl + "/" + url);
            return httpUrl + "/" + url;
        }
        return uri;
    }
}
