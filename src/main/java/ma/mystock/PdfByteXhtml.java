/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock;

/**
 *
 * @author admin
 */
public class PdfByteXhtml {

    private byte[] pdfByte;

    private String pdfXhtml;

    public byte[] getPdfByte() {
        return pdfByte;
    }

    public void setPdfByte(byte[] pdfByte) {
        this.pdfByte = pdfByte;
    }

    public String getPdfXhtml() {
        return pdfXhtml;
    }

    public void setPdfXhtml(String pdfXhtml) {
        this.pdfXhtml = pdfXhtml;
    }

}
