/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.log4j.RollingFileAppender;
import org.w3c.tidy.Tidy;

/**
 *
 * @author Driss HAIDOUR/ ISMAIL ZARKOUN
 */
public class HtmlToXHtml {

    static final Logger logger = Logger.getLogger(HtmlToXHtml.class.getName());
    static final String FILE_LOG = "FILE";
    private String xhtmlString; 

    public String getXhtmlString() {
        return xhtmlString;
    }

    public void setXhtmlString(String xhtmlString) {
        this.xhtmlString = xhtmlString;
    }
    public StringReader convertHtmlToXHtml(String htmlContent) throws IOException {

        RollingFileAppender appender = (RollingFileAppender) logger.getAppender(FILE_LOG);
        logger.addAppender(appender);

        htmlContent = htmlContent.replaceAll("rel=\"stylesheet\"", "rel=\"stylesheet\" media=\"print\"");

        StringReader contentReader = new StringReader(htmlContent);
        StringWriter out = new StringWriter();

        try {
            Tidy tidy = new Tidy();
            //Tell Tidy to convert HTML to XML
            tidy.setXmlOut(true);
            //Convert files
            tidy.parse(contentReader, out);
            return new StringReader(out.toString());
        } catch (Exception e) {
            logger.debug(e.getMessage());
            return null;
        } finally {
            //Clean up
            contentReader.close();
            out.close();
        }
    }

    public StringReader convertHtmlToXHtmlWithoutDTD(String htmlContent) throws IOException {
        RollingFileAppender appender = (RollingFileAppender) logger.getAppender(FILE_LOG);
        logger.addAppender(appender);

        htmlContent = htmlContent.replaceAll("rel=\"stylesheet\"", "rel=\"stylesheet\" media=\"print\"");

        StringReader contentReader = new StringReader(htmlContent);
        StringWriter out = new StringWriter();

        try {
            Tidy tidy = new Tidy();
            //Tell Tidy to convert HTML to XML
            tidy.setXmlOut(true);
            tidy.setDocType("omit");
            //Convert files
            tidy.parse(contentReader, out);
            xhtmlString = out.toString();
               xhtmlString=   xhtmlString.replaceAll("https:-test", "https://uottawa-test");
            return new StringReader(out.toString());
        } catch (Exception e) {
            logger.debug(e.getMessage());
            return null;
        } finally {
            //Clean up
            contentReader.close();
            out.close();
        }
    }

    public StringReader convertHtmlToXHtmlWithoutDTD(String htmlContent, String context, String absoluteUrl) throws IOException {
        RollingFileAppender appender = (RollingFileAppender) logger.getAppender(FILE_LOG);
        logger.addAppender(appender);

        htmlContent = htmlContent.replaceAll("rel=\"stylesheet\"", "rel=\"stylesheet\" media=\"print\"");
        htmlContent = htmlContent.replaceAll("/" + context, absoluteUrl);

        StringReader contentReader = new StringReader(htmlContent);
        StringWriter out = new StringWriter();

        try {
            Tidy tidy = new Tidy();
            //Tell Tidy to convert HTML to XML
           tidy.setXmlOut(true);
            tidy.setDocType("omit");
           
            //Convert files
            tidy.parse(contentReader, out);
            xhtmlString = out.toString();
         xhtmlString=   xhtmlString.replaceAll("https:-test", "https://uottawa-test");
            return new StringReader(xhtmlString);
        } catch (Exception e) {
            logger.debug(e.getMessage());
            return null;
        } finally {
            //Clean up
            contentReader.close();
            out.close();
        }
        
        
        
    }

    
      public StringReader convertHtmlToXHtmlWithoutDTD(String htmlContent, String context, String absoluteUrl, List<StudySection> sections, MessagesResolver messagesResolver) throws IOException {
        RollingFileAppender appender = (RollingFileAppender) logger.getAppender(FILE_LOG);
        logger.addAppender(appender);

        htmlContent = htmlContent.replaceAll("rel=\"stylesheet\"", "rel=\"stylesheet\" media=\"print\"");
        htmlContent = htmlContent.replaceAll("/" + context, absoluteUrl);

        StringReader contentReader = new StringReader(htmlContent);
        StringWriter out = new StringWriter();

        try {
            Tidy tidy = new Tidy();
            //Tell Tidy to convert HTML to XML
           tidy.setXmlOut(true);
            tidy.setDocType("omit");
           
            //Convert files
            tidy.parse(contentReader, out);
            xhtmlString = out.toString();
            StringBuilder builder=new StringBuilder();
            builder.append("<bookmarks>");
            for (StudySection studySection : sections) {
              builder.append("<bookmark name=\"").append(messagesResolver.get(studySection.getSectionId().getItemCode().concat(".label"))).append("\" href=\"#section").append(studySection.getSectionId().getId()).append("\"/>");   
            }
             builder.append("</bookmarks>");
             xhtmlString=   xhtmlString.replaceAll("https:-test", "https://uottawa-test");
    String    bookmarksString=     xhtmlString.replace("<head>", "<head>"+builder.toString());
            return new StringReader(bookmarksString);
        } catch (Exception e) {
            logger.debug(e.getMessage());
            return null;
        } finally {
            //Clean up
            contentReader.close();
            out.close();
        }
        
        
        
    }
    public String convertHtmlToXHtmlWithoutDTDS(String htmlContent) throws IOException {
        RollingFileAppender appender = (RollingFileAppender) logger.getAppender(FILE_LOG);
        logger.addAppender(appender);

        //traitement sur le string en input
        htmlContent = htmlContent.replaceAll("rel=\"stylesheet\"", "rel=\"stylesheet\" media=\"print\"");

        StringReader contentReader = new StringReader(htmlContent);
        StringWriter out = new StringWriter();

        try {
            Tidy tidy = new Tidy();
            //Tell Tidy to convert HTML to XML
            tidy.setXmlOut(true);
            tidy.setDocType("omit");
            
            //Convert files
            tidy.parse(contentReader, out);
            return out.toString();
        } catch (Exception e) {
            logger.debug(e.getMessage());
            return null;
        } finally {
            //Clean up
            contentReader.close();
            out.close();
        }
    }

    public static void main(String[] args) throws IOException {
        HtmlToXHtml h = new HtmlToXHtml();
        h.convertHtmlToXHtml("<html><head><title>Driss</title></head><body></body></html>");
    }
}