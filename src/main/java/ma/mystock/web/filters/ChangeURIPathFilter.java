package ma.mystock.web.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.Filter;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import ma.mystock.core.dao.entities.views.VCltClient;
import ma.mystock.core.dao.entities.views.VCltClientDetails;
import ma.mystock.core.dao.entities.views.VCltClientLanguage;
import ma.mystock.web.utils.functionUtils.StringUtils;
import ma.mystock.web.utils.functionUtils.params.BasicParameterUtils;
import ma.mystock.web.utils.pdf.ContentCaptureServletResponse;
import ma.mystock.web.utils.pdf.HtmlToXHtml;
import ma.mystock.web.utils.sessions.SessionsGetter;
import ma.mystock.web.utils.springBeans.GloablsServices;
import static ma.mystock.web.utils.springBeans.GloablsServices.vCltClientDetailsEJB;
import static ma.mystock.web.utils.springBeans.GloablsServices.vCltClientLanguageEJB;




import ma.mystock.web.utils.values.MapsGetter;
import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextRenderer;
import org.xml.sax.InputSource;

/**
 *
 * @author Abdessamad HALLAL
 */
//@WebFilter(urlPatterns = {"*.xhtml"})
public class ChangeURIPathFilter implements Filter {

    private FilterConfig config;
    private DocumentBuilder documentBuilder;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println(" init ChangeURIPathFilter ");
        try {
            this.config = config;
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            documentBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new ServletException(e);
        }
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpServleRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        String contextPath = httpServleRequest.getContextPath();
        String currentRequestURI = httpServleRequest.getRequestURI();
        currentRequestURI = currentRequestURI.replace(contextPath, "");
        String queryString = httpServleRequest.getQueryString();

        System.out.println(" => currentRequestURIcurrentRequestURI : " + currentRequestURI);

        if (currentRequestURI.contains("home.xhtml")) {

        } else if (!currentRequestURI.contains("home.xhtml") && currentRequestURI.contains("indexPDF.xhtml")) {
            /**
             * Start PDF
             */
            String outputTypeParam = httpServleRequest.getParameter("outputType");
            String fileNameParam = httpServleRequest.getParameter("fileName");

            System.out.println(" RenderPdfFilter doFilter : " + outputTypeParam + " - fileNameParam " + fileNameParam);

            if ("pdf".equalsIgnoreCase(outputTypeParam)) {

                ContentCaptureServletResponse capContent = new ContentCaptureServletResponse(httpServletResponse);

                httpServleRequest.setCharacterEncoding("UTF-8");
                chain.doFilter(httpServleRequest, capContent);
                String contents = capContent.getContent();

                try {

                    if ("".equalsIgnoreCase(fileNameParam) || fileNameParam == null) {
                        fileNameParam = "document";
                    }

                    String context = httpServleRequest.getContextPath();
                    System.out.println(" => 1 context " + context);
                    //context = deleteClientCodeFormUrl(context);
                    System.out.println(" => 2 context " + context);
                    String absoluteUrl = MapsGetter.getConfigBasicMap().get("1"); //"http://localhost:8083/" + context;
                    StringReader sourceReader = HtmlToXHtml.convertHtmlToXHtmlWithoutDTD(contents, context, absoluteUrl);

                    InputSource source = new InputSource(sourceReader);
                    Document xhtmlContent = documentBuilder.parse(source);

                    ITextRenderer renderer = new ITextRenderer();
                    System.out.println(" => absoluteUrl " + absoluteUrl);
                    renderer.setDocument(xhtmlContent, absoluteUrl); // java.io.IOException: Stream closed
                    renderer.layout();
                    httpServletResponse.setContentType("application/pdf");
                    httpServletResponse.setHeader("Content-Disposition", "attachment; filename=" + fileNameParam + ".pdf");
                    httpServletResponse.setHeader("Content-Description", fileNameParam + ".pdf");
                    OutputStream browserStream = httpServletResponse.getOutputStream();
                    renderer.createPDF(browserStream);
                    browserStream.flush();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return;

            /**
             * End PDF
             */
        } else if (!currentRequestURI.contains("javax.faces.resource")) {

            System.out.println("doFilter ChangeURIPathFilter : " + currentRequestURI);
            System.out.println("codeClient session : " + getClientCodeFromSession(httpServleRequest));
            System.out.println("codeClient url  : " + getClientCodeFormUrl(currentRequestURI));
            System.out.println("queryString : " + queryString);
            if (!urlHasClientCode(currentRequestURI) && !sessionHasClientCode(httpServleRequest)) {
                // redirct si aucun clientCode existe
                goToPortal(httpServletResponse);

            } else if (urlHasClientCode(currentRequestURI)) {

                String clientCodeUrl = getClientCodeFormUrl(currentRequestURI);
                System.out.println("Oui existe in URL ");

                if (!StringUtils.isEmpty(getClientCodeFromSession(httpServleRequest))) {

                    System.out.println(" Oui existe in session ");

                    if (!getClientCodeFromSession(httpServleRequest).equalsIgnoreCase(clientCodeUrl)) {

                        System.out.println(" sont pas egeaux ");
                        if (checkClientCodeFromDataBase(clientCodeUrl)) {

                            System.out.println(" checkClientCodeFromDataBase = true");
                            // change client dans session
                            changeClientCode(httpServleRequest, clientCodeUrl);

                        } else {
                            System.out.println(" checkClientCodeFromDataBase = false");
                            goToPortal(httpServletResponse);
                        }

                    } else {
                        System.out.println(" sont egeaux ");
                    }
                } else {
                    System.out.println(" Not existe in session ");

                    if (checkClientCodeFromDataBase(clientCodeUrl)) {

                        setClientCode(httpServleRequest, clientCodeUrl);

                    } else {
                        goToPortal(httpServletResponse);
                    }
                }

            } else if (!StringUtils.isEmpty(getClientCodeFromSession(httpServleRequest))) {
                // ajouter le client dans 

                System.out.println("Non not existe in URL ");
                currentRequestURI = addClientCodeToUrl(currentRequestURI, getClientCodeFromSession(httpServleRequest));

            } else {
                goToPortal(httpServletResponse);
            }

            currentRequestURI = removeSlashes(currentRequestURI);
            System.out.println(" after removeSlashes currentRequestURI : " + currentRequestURI);
            currentRequestURI = deleteClientCodeFormUrl(currentRequestURI);
            System.out.println(" after deleteClientCodeFormUrl clientCode : " + currentRequestURI);
            currentRequestURI = currentRequestURI + "?" + queryString;
            System.out.println(" after add  queryString currentRequestURI + " + currentRequestURI);
            request.getRequestDispatcher(currentRequestURI).forward(request, response);
            return;

        } else {
            chain.doFilter(request, response);
        }
    }

    public static String getClientCodeFormUrl(String requestURI) {

        if (BasicParameterUtils.isLOCAL()) {

            try {
                return requestURI.split("/")[1];
            } catch (Exception ex) {
                return "";
            }

        } else {

            try {
                return requestURI.split("/")[2];
            } catch (Exception ex) {
                return "";
            }
        }

        /*
         try {

         if (true) { //BasicParameterUtils.isLOCAL()
         return requestURI.split("/")[2];
         } else {
         return requestURI.split("/")[1];
         }

         } catch (Exception ex) {
         return "";
         }
        
         */
    }

    public static String addClientCodeToUrl(String requestURI, String codeClient) {
        try {
            return "/" + codeClient + "/" + requestURI;
        } catch (Exception e) {
            return requestURI;
        }
    }

    public static String deleteClientCodeFormUrl(String requestURI) {
        try {
            return requestURI = requestURI.replace("/" + getClientCodeFormUrl(requestURI), "");
        } catch (Exception e) {
            return requestURI;
        }
    }

    public static String getClientCodeFromSession(HttpServletRequest httpServletRequest) {
        return (String) httpServletRequest.getSession().getAttribute(SessionsGetter.CLT_CLIENT_CODE);
    }

    public static boolean checkClientCodeFromDataBase(String clientCode) {
        Map<String, Object> paramQuery = new HashMap<>();
        paramQuery.put("code", clientCode.toLowerCase());
        VCltClient vCltClient = GloablsServices.vCltClientEJB.executeSingleQuery(VCltClient.findByCode, paramQuery);
        return (vCltClient != null);
    }

    public static void goToPortal(HttpServletResponse httpServletResponse) {
        try {
            httpServletResponse.sendRedirect("https://www.google.com/");
        } catch (Exception e) {

        }
    }

    public static void setClientCode(HttpServletRequest httpServletRequest, String clientCode) {
        System.out.println(" Set client in session " + clientCode.toLowerCase());
        Map<String, Object> paramQuery = new HashMap<>();
        paramQuery.put("code", clientCode.toLowerCase());
        VCltClient vCltClient = GloablsServices.vCltClientEJB.executeSingleQuery(VCltClient.findByCode, paramQuery);
        if (vCltClient != null) {
            System.out.println(" start ! ");
            // setter dans la session inforamtion sur client 
            httpServletRequest.getSession().setAttribute(SessionsGetter.CLT_CLIENT_CODE, vCltClient.getCode());
            httpServletRequest.getSession().setAttribute(SessionsGetter.CLT_CLIENT_ID, vCltClient.getId());
            httpServletRequest.getSession().setAttribute(SessionsGetter.V_CLT_CLIENT, vCltClient);

            // setter dans la session detail de client 
            paramQuery.clear();
            paramQuery.put("id", vCltClient.getId());
            VCltClientDetails vCltClientDetails = vCltClientDetailsEJB.executeSingleQuery(VCltClientDetails.findById, paramQuery);
            httpServletRequest.getSession().setAttribute(SessionsGetter.V_CLT_CLIENT_DETAILS, vCltClientDetails);

            System.out.println(" vCltClient.getDefaultLanguageId() : " + MapsGetter.getParameterClientMap().get("3"));
            // setter dans la session les détails de client et language
            paramQuery.clear();
            paramQuery.put("clientId", vCltClient.getId());
            paramQuery.put("infLanguageId", 1L); //MapsGetter.getParameterClientMap().get("3")
            VCltClientLanguage vCltClientLanguage = vCltClientLanguageEJB.executeSingleQuery(VCltClientLanguage.findByClientIdAndInfLanguageId, paramQuery);
            httpServletRequest.getSession().setAttribute(SessionsGetter.V_CLT_CLIENT_LANGUAGE, vCltClientLanguage);

            if (vCltClientLanguage != null) {

                // setter dans la session le language de client
                httpServletRequest.getSession().setAttribute(SessionsGetter.INF_LANGUAGE_ID, vCltClientLanguage.getInfLanguageId());
                httpServletRequest.getSession().setAttribute(SessionsGetter.INF_LANGUAGE_CODE, vCltClientLanguage.getInfLanguageCode());

                // setter dans la session le priefix de client
                httpServletRequest.getSession().setAttribute(SessionsGetter.INF_PREFIX_ID, vCltClientLanguage.getInfPrefixId());
                httpServletRequest.getSession().setAttribute(SessionsGetter.INF_PREFIX_CODE, vCltClientLanguage.getInfPrefixCode());
            }
        } else {
            System.out.println(" client is null");
        }

    }

    public static void changeClientCode(HttpServletRequest httpServletRequest, String clientCode) {
        setClientCode(httpServletRequest, clientCode);
    }

    public static boolean urlHasClientCode(String requestURI) {
        /*
         if (true) { // BasicParameterUtils.isLOCAL()
         return requestURI.split("/").length == 3;
         } else {
         return requestURI.split("/").length == 4;
         }
         */
        if (BasicParameterUtils.isLOCAL()) {

            return requestURI.split("/").length == 3;

        } else {

            return requestURI.split("/").length == 4;
        }
    }

    public static boolean sessionHasClientCode(HttpServletRequest httpServletRequest) {
        return !StringUtils.isEmpty(getClientCodeFromSession(httpServletRequest));
    }

    public static String removeSlashes(String str) {
        return str.replaceAll("//", "/").replaceAll("///", "/");
    }

    @Override
    public void destroy() {
        System.out.println(" destroy ChangeURIPathFilter ");
    }
}
