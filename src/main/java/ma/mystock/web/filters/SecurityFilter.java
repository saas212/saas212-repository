package ma.mystock.web.filters;

import java.io.IOException;
import java.util.Locale;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ma.mystock.web.utils.sessions.SessionsGetter;
import ma.mystock.web.utils.values.MapsGetter;

/**
 *
 * @author abdou
 */
@WebFilter(urlPatterns = {"*.xhtml"})
public  class SecurityFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;

        if (!isPathAllowed(httpServletRequest.getRequestURL().toString())) {

            boolean connected = false;

            try {
                connected = (boolean) httpServletRequest.getSession().getAttribute(SessionsGetter.IS_CONNECTED);
            } catch (Exception e) {
            }

            if (!connected) {

                String url = MapsGetter.getConfigBasicMap().get("1") + "servlet/logout";
                httpServletResponse.sendRedirect(url);

            }

        }

        chain.doFilter(request, response);
    }

    private boolean isPathAllowed(String path) {
        if (path.endsWith("/login.xhtml") || path.endsWith("/home.xhtml") || path.endsWith("/") || path.contains("javax.faces.resource")) {
            return true;
        } else {
            return false;
        }
    }

    private void setLocaleAndLanguage(HttpServletRequest httpServletRequest, ServletResponse response, String language) {

        Locale locale = new Locale(language);
        response.setLocale(locale);

        httpServletRequest.getSession().setAttribute("", language);
        httpServletRequest.getSession().setAttribute("", locale);

    }

    
    @Override
    public void destroy() {

    }

}
