/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Abdessamad HALLAL
 */
@WebFilter(urlPatterns = {"/*"})
public class GlobalFilter implements Filter {

    @Override
    public void init(FilterConfig fc) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest httpServleRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;

        String currentRequestURI = httpServleRequest.getRequestURI();
        String contextPath = httpServleRequest.getContextPath();
        currentRequestURI = currentRequestURI.replace(contextPath, "");

        //if ("/".equalsIgnoreCase(currentRequestURI)) {
        //    request.getRequestDispatcher("/home.xhtml").forward(request, response);
        //} else {
        //    chain.doFilter(request, response);
        //}
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }

}
