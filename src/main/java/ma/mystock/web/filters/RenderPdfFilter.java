package ma.mystock.web.filters;

import ma.mystock.web.utils.pdf.ContentCaptureServletResponse;
import ma.mystock.web.utils.pdf.HtmlToXHtml;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xhtmlrenderer.pdf.ITextRenderer;
import com.lowagie.text.DocumentException;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.servlet.annotation.WebFilter;
import ma.mystock.web.utils.values.MapsGetter;

@WebFilter(urlPatterns = {"/indexPDF.xhtml"})
//@WebFilter(urlPatterns = {"*.xhtml"})
public class RenderPdfFilter implements Filter {

    private FilterConfig config;
    private DocumentBuilder documentBuilder;

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

//        System.out.println(" request.getRequestURI(); ====> " + request.getRequestURI());
        String contextPath = request.getContextPath();
        String currentRequestURI = request.getRequestURI();
        //currentRequestURI = currentRequestURI.replace(contextPath, "");
        String queryString = request.getQueryString();

        //  System.out.println("RenderPdfFilter + " + currentRequestURI);
        
        if (currentRequestURI.contains("indexPDF")) {

            String outputTypeParam = request.getParameter("outputType");
            String fileNameParam = request.getParameter("fileName");

            System.out.println(" RenderPdfFilter doFilter : " + outputTypeParam + " - fileNameParam " + fileNameParam);

            if ("".equalsIgnoreCase(fileNameParam) || fileNameParam == null) {
                fileNameParam = "document";
            }
            if ("pdf".equalsIgnoreCase(outputTypeParam)) {

                ContentCaptureServletResponse capContent = new ContentCaptureServletResponse(response);
                request.setCharacterEncoding("UTF-8");
                chain.doFilter(request, capContent);
                String contents = capContent.getContent();

                try {
                    String context = request.getContextPath();

                    String absoluteUrl = MapsGetter.getConfigBasicMap().get("1"); //"http://localhost:8083/" + context;
                    StringReader sourceReader = HtmlToXHtml.convertHtmlToXHtmlWithoutDTD(contents, context, absoluteUrl);

                    InputSource source = new InputSource(sourceReader);
                    Document xhtmlContent = documentBuilder.parse(source);

                    ITextRenderer renderer = new ITextRenderer();
                    renderer.setDocument(xhtmlContent, absoluteUrl);
                    renderer.layout();
                    response.setContentType("application/pdf");
                    response.setHeader("Content-Disposition", "attachment; filename=" + fileNameParam + ".pdf");
                    response.setHeader("Content-Description", fileNameParam + ".pdf");
                    OutputStream browserStream = response.getOutputStream();
                    renderer.createPDF(browserStream);
                    browserStream.flush();

                } catch (SAXException | DocumentException e) {
                    e.printStackTrace();
                }
            }

            return;

        } else {
            System.out.println("ELSE  RenderPdfFilter doFilter  currentRequestURI + " + currentRequestURI);
            chain.doFilter(request, response);
        }
        //chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig config) throws ServletException {
        try {
            this.config = config;
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            documentBuilder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new ServletException(e);
        }
    }

    @Override
    public void destroy() {

    }

}
