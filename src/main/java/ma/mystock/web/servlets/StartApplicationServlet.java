package ma.mystock.web.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ma.mystock.core.dao.entities.CltClient;
import ma.mystock.web.utils.jsf.facelet.Functions;
import ma.mystock.web.utils.springBeans.GloablsServices;

/**
 *
 * @author Abdessamad HALLAL
 */
@WebServlet(name = "StartApplicationServlet", urlPatterns = {"/servlet/startApplication"})
public class StartApplicationServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        HttpSession session = (HttpSession) request.getSession();
        /*
        String url = "";

        Map<String, Object> paramQuery = new HashMap<>();
        paramQuery.put("active", "Y");
        List<CltClient> cltClientList = GloablsServices.cltClientEJB.executeQuery(CltClient.findByActive, paramQuery);

        if (cltClientList != null && cltClientList.size() == 1) {
            System.out.println(" StartApplicationServlet : cltClientList.size() " + cltClientList.size());
            CltClient cltClient = cltClientList.get(0);
            url = Functions.hostAndContext + "/" + cltClient.getCode() + "/login.xhtml";
        } else {
            url = Functions.hostAndContext;
        }

        // Action  : redirections 
        System.out.println(" StartApplicationServlet url : " + url);
        response.sendRedirect(url);
        
         */

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
