package ma.mystock.web.servlets;

import java.io.IOException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ma.mystock.web.listners.LoadContextListener;
import ma.mystock.web.utils.functionUtils.StringUtils;
import ma.mystock.web.utils.jsf.facelet.Functions;
import ma.mystock.web.utils.routing.RedirectUtils;
import ma.mystock.web.utils.sessions.SessionsGetter;
import org.apache.log4j.Logger;

/**
 *
 * @author abdou
 */
@WebServlet(name = "RefreshServlet", urlPatterns = {"/servlet/refresh"})
public class RefreshServlet extends HttpServlet {

    protected static Logger log = Logger.getLogger(RefreshServlet.class);

    LoadContextListener loadContextListener;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        /*
         Params :
         -> username : user 
         -> password : pass
         -> item     : all (validation, composition, )
         -> client   : x
         -> module   : x
         */
        response.setContentType("text/html");

        log.info("----------------------  RefreshServlet (" + request.getParameter("username") + "," + request.getParameter("password") + ") ----------------------");

        HttpServletRequest httpServleRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        HttpSession session = (HttpSession) request.getSession();
        String clientCode = (String) session.getAttribute(SessionsGetter.CLT_CLIENT_CODE);
        // contruct URL 

        String url = "";

        if (!StringUtils.isEmpty(clientCode)) {
            url = Functions.hostAndContext + "/" + clientCode + "/module.xhtml";
        } else {
            url = Functions.hostAndContext;
        }

        if ("user".equals(request.getParameter("username")) && "pass".equals(request.getParameter("password"))) {
            ServletContextEvent servletContextEvent = new ServletContextEvent(request.getServletContext());
            loadContextListener = new LoadContextListener();
            loadContextListener.contextInitialized(servletContextEvent);
        }

        log.info("----------------------  FIN ----------------------");

        response.sendRedirect(url);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RedirectUtils.goToFacesIndex();
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
