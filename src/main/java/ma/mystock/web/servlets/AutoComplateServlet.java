/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ma.mystock.web.utils.autocomplete.AutoComplateType;
import ma.mystock.web.utils.autocomplete.AutocompleteUtils;
import ma.mystock.web.utils.sessions.SessionsGetter;
import static ma.mystock.web.utils.sessions.SessionsGetter.CLT_MODULE_ID;
import org.primefaces.json.JSONObject;

/**
 *
 * @author Abdessamad HALLAL
 * @note String(q.getBytes("UTF-8"), "UTF-8");
 */
@WebServlet(name = "AutoComplateServlet", urlPatterns = {"/servlet/autoComplate"})
public class AutoComplateServlet extends HttpServlet {

    private static final String CONTENT_TYPE_TEXT_HTML_UTF8 = "text/html; charset=utf-8";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpServletRequest httpServleRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        String contextPath = httpServleRequest.getContextPath();
        String currentRequestURI = httpServleRequest.getRequestURI();
        currentRequestURI = currentRequestURI.replace(contextPath, "");

        response.setContentType(CONTENT_TYPE_TEXT_HTML_UTF8);
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");

        Long cltModuleId = (Long) httpServleRequest.getSession().getAttribute(SessionsGetter.CLT_MODULE_ID);

        PrintWriter out = response.getWriter();
        JSONObject list = new JSONObject();
        String term = request.getParameter("term");
        String type = request.getParameter("type");

        System.out.println("AutoComplateServlet / " + type + " / " + term);

        if (AutoComplateType.vSmProductLikeDesignation.equalsIgnoreCase(type)) {
            list = AutocompleteUtils.findVSmProductLikeDesignationAndCltModuleId(term, cltModuleId);
        } else if (AutoComplateType.vSmProductLikeReference.equalsIgnoreCase(type)) {
            list = AutocompleteUtils.findVSmProductLikeReferenceAndCltModuleId(term, cltModuleId);
        }

        out.print(list);
        out.flush();
        out.close();
    }

}
