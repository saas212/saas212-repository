package ma.mystock.web.servlets;

import java.io.IOException;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ma.mystock.web.utils.jsf.facelet.Functions;
import ma.mystock.web.utils.sessions.SessionsGetter;

/**
 *
 * @author abdou
 */
@WebServlet(name = "LogoutServlet", urlPatterns = {"/servlet/logout"}, description = "une servlet pour se déconnecter")
public class LogoutServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpServletRequest httpServleRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        HttpSession session = (HttpSession) request.getSession();

        //String clientCode = (String) session.getAttribute(SessionsGetter.CLT_CLIENT_CODE);
        // contruct URL 
        String url = Functions.hostAndContext;

        /*
        if (!StringUtils.isEmpty(clientCode)) {
            url = Functions.hostAndContext + "/" + clientCode + "/login.xhtml";
        } else {
            url = Functions.hostAndContext;
        }
         */
        for (Enumeration attributeEnum = request.getSession().getAttributeNames(); attributeEnum.hasMoreElements();) {
            String attributeName = (String) attributeEnum.nextElement();

            //if (toBeDeleted(attributeName)) {
            System.out.println(" attributeName " + attributeName);
            request.getSession().removeAttribute(attributeName);
            //}

        }
        System.out.println(" url : .... " + url);
        response.sendRedirect(url);
    }

    private boolean toBeDeleted(String attributeName) {

        return !(SessionsGetter.CLT_CLIENT_ID.equalsIgnoreCase(attributeName)
                || SessionsGetter.CLT_CLIENT_CODE.equalsIgnoreCase(attributeName)
                || SessionsGetter.V_CLT_CLIENT.equalsIgnoreCase(attributeName)
                || SessionsGetter.V_CLT_CLIENT_DETAILS.equalsIgnoreCase(attributeName)
                || SessionsGetter.V_CLT_CLIENT_LANGUAGE.equalsIgnoreCase(attributeName)
                || SessionsGetter.INF_LANGUAGE_CODE.equalsIgnoreCase(attributeName)
                || SessionsGetter.INF_LANGUAGE_ID.equalsIgnoreCase(attributeName)
                || SessionsGetter.INF_PREFIX_CODE.equalsIgnoreCase(attributeName)
                || SessionsGetter.INF_PREFIX_ID.equalsIgnoreCase(attributeName));
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
