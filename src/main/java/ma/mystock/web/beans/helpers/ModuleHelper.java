/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.beans.helpers;

import java.io.Serializable;
import java.util.List;
import ma.mystock.core.dao.entities.views.VCltUserModule;

/**
 *
 * @author Abdessamad HALLAL
 */
public class ModuleHelper implements Serializable {

    private Long moduleTypeId;
    private String moduleTypeName;
    private String connectAs = "Vous êtes connecté en tant que";

    private List<VCltUserModule> vCltUserModuleList;

    public Long getModuleTypeId() {
        return moduleTypeId;
    }

    public void setModuleTypeId(Long moduleTypeId) {
        this.moduleTypeId = moduleTypeId;
    }

    public List<VCltUserModule> getvCltUserModuleList() {
        return vCltUserModuleList;
    }

    public void setvCltUserModuleList(List<VCltUserModule> vCltUserModuleList) {
        this.vCltUserModuleList = vCltUserModuleList;
    }

    public String getModuleTypeName() {
        return moduleTypeName;
    }

    public void setModuleTypeName(String moduleTypeName) {
        this.moduleTypeName = moduleTypeName;
    }

    public String getConnectAs() {
        return connectAs + " " + getModuleTypeName();
    }

    public void setConnectAs(String connectAs) {
        this.connectAs = connectAs;
    }

}
