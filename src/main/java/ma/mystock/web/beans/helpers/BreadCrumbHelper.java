/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.beans.helpers;

import java.io.Serializable;

/**
 *
 * @author abdou
 */
public class BreadCrumbHelper implements Serializable {

    private String label;
    private String url;
    private boolean selected;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public BreadCrumbHelper(String label, String url) {
        this.label = label;
        this.url = url;
    }

    public BreadCrumbHelper() {
    }

    public BreadCrumbHelper(String label, String url, boolean selected) {
        this.label = label;
        this.url = url;
        this.selected = selected;
    }

}
