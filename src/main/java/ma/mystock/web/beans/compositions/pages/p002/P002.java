package ma.mystock.web.beans.compositions.pages.p002;

import java.util.List;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import ma.mystock.core.dao.entities.SmExpenseType;
import ma.mystock.core.dao.entities.views.dto.LovsDTO;
import ma.mystock.module.pageManagement.annotations.Action;
import ma.mystock.module.pageManagement.annotations.Attribute;
import ma.mystock.module.pageManagement.annotations.Page;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import ma.mystock.module.pageManagement.messages.MessageList;

/**
 *
 * @author abdou
 */
@Page(id = "1")

public class P002 extends AbstractPage {

    @Attribute(itemCode = "page1.s1.name")
    private String name;

    @Attribute(itemCode = "page1.s1.type")
    private String type;

    private List<SelectItem> typeList;

    @Override
    public void onInit() {

        log.info("start TEST var : ");

        log.info("getTxt('expense.s1.title') : " + getTxt("expense.s1.title")); // jsp : txt
        log.info("getParam('2') : " + getParam("2")); // jsp : params
        log.info("getPriv('login.s1.username') : " + getPriv("login.s1.username")); // jsp : priv
        log.info("getConfig('1') : " + getConfig("1")); // jsp : config
        //log.info("getAttrExclud('1.ok') : " + getAttrExclud("ok")); // jsp : attrExclud
        log.info("getPageParam('1') : " + getPageParam("1")); // jsp : pageParam

        setSession("abc", "def");
        log.info("getSession('abc') : " + getSession("abc"));

        log.info("Fin TEST var ");
        // 
        log.info("Init Page 1");
        log.info("Init page param : !!!!!!0000" + getPageParam("2"));
        if ("yes".equals(getPageParam("2"))) {
            ///showFrme = false;
        }

        List<LovsDTO> list = getLov("SM_EXPENSE_TYPE");

        if (list != null) {
            log.info("list size SM_EXPENSE_TYPE : " + list.size());
        }

        // remplacer ça par attr dans var global
        //typeList = getLovSelectItem("SM_EXPENSE_TYPE");
        if (typeList != null) {
            log.info("=> .L/ : " + typeList.size());
        }

    }

    @Override
    public void onValidateBeforeSave(ActionEvent e) {

    }

    @Override
    @Action
    public void onSave(ActionEvent e) {

        log.info("Avant ");
        SmExpenseType o = new SmExpenseType();
        // o.setActive('T');
        // o.setCltModuleId(1L);
        // o.setName("name");
        // o.setSortKey(1L);

        o = smExpenseTypeEJB.executeMerge(o);

        log.info("Après");
    }

    public String getName() {

        //log.info("getCltUserId ====> : " + SessionsValues.getCltUserId());
        //log.info("getCltModuleId ====> : " + SessionsValues.getCltModuleId());
        //log.info("getCltUserId ====> : " + SessionsValues.getCltUserId());
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<SelectItem> getTypeList() {
        return typeList;
    }

    public void setTypeList(List<SelectItem> typeList) {
        this.typeList = typeList;
    }

}
