package ma.mystock.web.beans.compositions.pages.p036;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import javax.persistence.ParameterMode;
import ma.mystock.core.dao.StoredProcedureParameter;
import ma.mystock.core.dao.StoredProcedures;
import ma.mystock.core.dao.entities.SmReception;
import ma.mystock.core.dao.entities.SmReceptionLine;
import ma.mystock.core.dao.entities.views.VSmReception;
import ma.mystock.core.dao.entities.views.VSmReceptionLine;
import ma.mystock.core.dao.entities.views.VSmReceptionSummary;
import ma.mystock.core.dao.entities.views.VSmProduct;
import ma.mystock.web.utils.MyGenerator;
import ma.mystock.web.utils.datatype.MyDouble;
import ma.mystock.web.utils.datatype.MyLong;
import ma.mystock.web.utils.datatype.MyString;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.web.utils.jsf.FacesUtils;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import ma.mystock.web.utils.functionUtils.DateUtils;
import static ma.mystock.web.utils.springBeans.GloablsServices.smReceptionEJB;
import static ma.mystock.web.utils.springBeans.GloablsServices.smReceptionLineEJB;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmProductEJB;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmReceptionEJB;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmReceptionLineEJB;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmReceptionSummaryEJB;
import ma.mystock.web.utils.globals.GlobalsAttributs;
import ma.mystock.web.utils.lovs.LovsUtils;
import ma.mystock.web.utils.pdf.PdfGlobals;
import ma.mystock.web.utils.pdf.PdfUtils;
import ma.mystock.web.utils.sessions.SessionsGetter;

/**
 *
 * @author Abdessamad HALLAL
 * @desc : Commandes fournisseur (add, edit et delete)
 *
 */
public class P036 extends AbstractPage {

    private List<VSmReception> vSmReceptionList;
    private List<VSmReceptionLine> vSmReceptionLineList;

    private List<SelectItem> supplierList;

    private VSmProduct vSmProduct;
    private SmReceptionLine smReceptionLine;
    private VSmReceptionLine vSmReceptionLine;
    private SmReception smReception;
    private VSmReceptionSummary vSmReceptionSummary;

    private Long smReceptionLineId;
    private Long receptionId;
    private Long vSmProductId;
    private Long lastReceptionId;

    private String reference;
    private String supplier;
    private String note;

    private String pReference;
    private String pReferenceHidden;
    private String pDesignation;
    private String pDesignationHidden;
    private String pUnitPrice;
    private String pQuantity;
    private String pTotalHt;

    private String summaryTotalQuantity;
    private String summaryTotalHt;
    private String summaryTvaAmount;
    private String summaryTotalTtc;

    @Override
    public void onInit() {

        initList();
        initovs();
        initAttribute();
        log.info("onInit P036");
        setShowForm(false);

    }

    public void initList() {
        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        paramQuery.put("receptionStatusId", GlobalsAttributs.ORDER_SUPPLIER_STATUS_ID_SUBMIT);
        vSmReceptionList = vSmReceptionEJB.executeQuery(VSmReception.findByCltModuleIdAndReceptionStatusId, paramQuery);
    }

    public void initSubList() {
        paramQuery.clear();
        paramQuery.put("receptionId", receptionId);
        vSmReceptionLineList = vSmReceptionLineEJB.executeQuery(VSmReceptionLine.findByReceptionId, paramQuery);
    }

    public void initSummary() {

        paramQuery.clear();
        paramQuery.put("receptionId", receptionId);
        vSmReceptionSummary = vSmReceptionSummaryEJB.executeSingleQuery(VSmReceptionSummary.findByReceptionId, paramQuery);

        if (MyIs.isNotNull(vSmReceptionSummary)) {

            summaryTotalQuantity = MyLong.toString(vSmReceptionSummary.getSummaryTotalQuantity());
            summaryTotalHt = MyDouble.toString(vSmReceptionSummary.getSummaryTotalHt());
            summaryTvaAmount = MyDouble.toString(vSmReceptionSummary.getSummaryTvaAmount());
            summaryTotalTtc = MyDouble.toString(vSmReceptionSummary.getSummaryTotalTtc());

            if ("".equalsIgnoreCase(summaryTotalQuantity)) {
                summaryTotalQuantity = "0";
            }
            if ("".equalsIgnoreCase(summaryTotalHt)) {
                summaryTotalHt = "0";
            }
            if ("".equalsIgnoreCase(summaryTvaAmount)) {
                summaryTvaAmount = "0";
            }
            if ("".equalsIgnoreCase(summaryTotalTtc)) {
                summaryTotalTtc = "0";
            }
        }

    }

    public void initovs() {
        supplierList = LovsUtils.getSmSupplierItems();
    }

    public void initAttribute() {
        setReference("");
        setSupplier("");
        setNote("");
    }

    public void initPAttribute() {
        setpReference("");
        setpReferenceHidden("");
        setpDesignation("");
        setpDesignationHidden("");
        setpUnitPrice("");
        setpQuantity("");
        setpTotalHt("");
    }

    public void editReception(AjaxBehaviorEvent e) {

        receptionId = (Long) e.getComponent().getAttributes().get("receptionId");

        if (receptionId != null) {

            initAttribute();

            paramQuery.clear();
            paramQuery.put("id", receptionId);
            smReception = smReceptionEJB.executeSingleQuery(SmReception.findById, paramQuery);

            if (MyIs.isNotNull(smReception)) {

                reference = smReception.getReference();
                supplier = MyLong.toString(smReception.getSupplierId());
                note = smReception.getNote();

            }
            initSubList();
            initPAttribute();
        }
        initSummary();
        setShowForm(true);
    }

    public void cancelReception(AjaxBehaviorEvent e) {
        receptionId = null;
        initList();
        initAttribute();
        setShowForm(false);
    }

    @Override
    public void onSavePartial(AjaxBehaviorEvent e) {
        if (!MyIs.isNotEmpty(reference)) {
            return;
        }

        if (receptionId == null) {
            smReception = new SmReception();
            smReception.setCltModuleId(cltModuleId);
            smReception.setReceptionStatusId(GlobalsAttributs.ORDER_SUPPLIER_STATUS_ID_SUBMIT);
            smReception.setUserCreation(SessionsGetter.getCltUserId());
            smReception.setDateCreation(DateUtils.getCurrentDate());

        } else {
            paramQuery.clear();
            paramQuery.put("id", receptionId);
            smReception = smReceptionEJB.executeSingleQuery(SmReception.findById, paramQuery);
            smReception.setUserUpdate(SessionsGetter.getCltUserId());
            smReception.setDateUpdate(DateUtils.getCurrentDate());
        }

        smReception.setReference(reference);
        smReception.setSupplierId(MyLong.toLong(supplier));
        smReception.setNote(note);

        smReception = smReceptionEJB.executeMerge(smReception);

        receptionId = smReception.getId();

    }

    @Override
    public void onDeletePartial(AjaxBehaviorEvent e) {

        receptionId = (Long) e.getComponent().getAttributes().get("receptionId");

        if (receptionId != null) {
            paramQuery.clear();
            paramQuery.put("id", receptionId);
            smReception = smReceptionEJB.executeSingleQuery(SmReception.findById, paramQuery);

            if (MyIs.isNotNull(smReception)) {
                smReceptionEJB.executeDelete(smReception);
            }
        }
        initList();
    }

    public void editProduct(AjaxBehaviorEvent e) {

        smReceptionLineId = (Long) e.getComponent().getAttributes().get("smReceptionLineId");

        if (smReceptionLineId != null) {

            paramQuery.clear();
            paramQuery.put("id", smReceptionLineId);
            vSmReceptionLine = vSmReceptionLineEJB.executeSingleQuery(VSmReceptionLine.findById, paramQuery);

            if (vSmReceptionLine != null) {

                pReference = vSmReceptionLine.getReference();
                pReferenceHidden = MyLong.toString(vSmReceptionLine.getProductId());
                pDesignation = vSmReceptionLine.getDesignation();
                pDesignationHidden = vSmReceptionLine.getProductId().toString();
                pUnitPrice = MyDouble.toString(vSmReceptionLine.getUnitPriceBuy());
                pQuantity = MyLong.toString(vSmReceptionLine.getQuantity());
                pTotalHt = MyString.toString(vSmReceptionLine.getUnitPriceBuy() * vSmReceptionLine.getQuantity());
                vSmProductId = vSmReceptionLine.getProductId();
            }
            initSummary();
        }
    }

    public void deleteProduct(AjaxBehaviorEvent e) {

        smReceptionLineId = (Long) e.getComponent().getAttributes().get("smReceptionLineId");
        if (smReceptionLineId != null) {

            paramQuery.clear();
            paramQuery.put("id", smReceptionLineId);
            smReceptionLine = smReceptionLineEJB.executeSingleQuery(SmReceptionLine.findById, paramQuery);
            if (smReceptionLine != null) {

                smReceptionLineEJB.executeDelete(smReceptionLine);
            }
            smReceptionLineId = null;
            initSubList();
            initSummary();

        }
    }

    public void findProduct(AjaxBehaviorEvent e) {

        if (!"".equalsIgnoreCase(pDesignationHidden)) {
            vSmProductId = Long.valueOf(pDesignationHidden);
        } else if (!"".equalsIgnoreCase(pReferenceHidden)) {
            vSmProductId = Long.valueOf(pReferenceHidden);
        }
        if (MyIs.isNull(vSmProductId)) {
            return;
        }
        paramQuery.clear();
        paramQuery.put("id", vSmProductId);
        vSmProduct = vSmProductEJB.executeSingleQuery(VSmProduct.findById, paramQuery);

        showFieldProduct();

    }

    public void showFieldProduct() {
        if (vSmProduct != null) {
            pReference = vSmProduct.getReference();
            pReferenceHidden = vSmProduct.getId().toString();
            pDesignation = vSmProduct.getDesignation();
            pDesignationHidden = vSmProduct.getId().toString();
            pUnitPrice = MyDouble.toString(vSmProduct.getPriceSale());
            pQuantity = "1";
            pTotalHt = MyString.toString(vSmProduct.getPriceSale() * Long.valueOf(pQuantity));
        }
    }

    public void updateTotalTtc(AjaxBehaviorEvent e) {
        
        Long qte = MyString.stringToLong(pQuantity);

        if (qte == null) {
            qte = 1L;
        }
        
        pTotalHt = MyString.toString((MyDouble.toDouble(pUnitPrice) * qte));
    }

    public void saveProduct(AjaxBehaviorEvent e) {

        if (!MyIs.isNotEmpty(pReference, pDesignation, pUnitPrice, pQuantity)) {
            return;
        }

        onSavePartial(e);

        if (smReceptionLineId != null) {
            paramQuery.clear();
            paramQuery.put("id", smReceptionLineId);
            smReceptionLine = smReceptionLineEJB.executeSingleQuery(SmReceptionLine.findById, paramQuery);
            smReceptionLine.setUserUpdate(SessionsGetter.getCltUserId());
            smReceptionLine.setDateUpdate(DateUtils.getCurrentDate());

        } else {
            smReceptionLine = new SmReceptionLine();
            smReceptionLine.setReceptionId(receptionId);
            smReceptionLine.setUserCreation(SessionsGetter.getCltUserId());
            smReceptionLine.setDateCreation(DateUtils.getCurrentDate());
        }

        smReceptionLine.setProductId(vSmProductId);
        smReceptionLine.setDesignation(pDesignation);
        smReceptionLine.setUnitPriceBuy(MyDouble.toDouble(pUnitPrice));
        smReceptionLine.setQuantity(MyLong.toLong(pQuantity));

        smReceptionLine = smReceptionLineEJB.executeMerge(smReceptionLine);
        smReceptionLineId = null;

        initSubList();
        initPAttribute();
        initSummary();
    }

    public void cleanProduct(AjaxBehaviorEvent e) {
        initPAttribute();
    }

    public void onChangePReference(AjaxBehaviorEvent e) {
        if ("".equalsIgnoreCase(pReference)) {
            initPAttribute();
        } else {
            paramQuery.clear();
            paramQuery.put("reference", MyString.trim(pReference));
            paramQuery.put("cltModuleId", cltModuleId);
            vSmProduct = vSmProductEJB.executeSingleQuery(VSmProduct.findByReferenceAnCltModuleId, paramQuery);
            showFieldProduct();
        }
    }

    public void onChangePDesignation(AjaxBehaviorEvent e) {
        if ("".equalsIgnoreCase(pDesignation)) {
            initPAttribute();
        } else {
            paramQuery.clear();
            paramQuery.put("designation", MyString.trim(pDesignation));
            paramQuery.put("cltModuleId", cltModuleId);
            vSmProduct = vSmProductEJB.executeSingleQuery(VSmProduct.findByDesignationAnCltModuleId, paramQuery);
            showFieldProduct();
        }
    }

    public void validateReception(AjaxBehaviorEvent e) {
        System.out.println("validateReception ");
        onSavePartial(e);

        Map<String, StoredProcedureParameter> map = new HashMap<>();
        map.put("RESULT_STATUS", new StoredProcedureParameter("RESULT_STATUS", Integer.class, ParameterMode.OUT, null));
        map.put("SM_RECEPTION_ID", new StoredProcedureParameter("SM_RECEPTION_ID", Integer.class, ParameterMode.IN, Integer.valueOf(receptionId.toString())));

        int val = vSmProductEJB.executeStoredProcedureQueryGeneric(StoredProcedures.SP_SM_VALIDATE_RECEPTION, map);
        System.out.println(" val : " + val);

        receptionId = null;
        setShowForm(false);
        initList();
        System.out.println(" validateReception ");
    }

    public void printAllReception(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("fileName", "bon_de_reception_en_cours");
        paramPdf.put("pdfId", PdfGlobals.PRINT_ALL_RECEPTIONS_ID.toString());
        paramPdf.put("receptionStatusId", GlobalsAttributs.ORDER_SUPPLIER_STATUS_ID_SUBMIT.toString());
        printUrl = PdfUtils.getUrlDownloadPdf(paramPdf);
        FacesUtils.goTo(printUrl);
    }

    public void previewAllReception(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("pdfId", PdfGlobals.PRINT_ALL_RECEPTIONS_ID.toString());
        paramPdf.put("receptionStatusId", GlobalsAttributs.ORDER_SUPPLIER_STATUS_ID_SUBMIT.toString());
        previewUrl = PdfUtils.getUrlViewHTML(paramPdf);
        FacesUtils.newTab(previewUrl);
    }

    public void printReception(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("fileName", "bon_de_reception_" + receptionId.toString());
        paramPdf.put("pdfId", PdfGlobals.PRINT_RECEPTION_ID.toString());
        paramPdf.put("receptionId", receptionId.toString());
        printUrl = PdfUtils.getUrlDownloadPdf(paramPdf);
        FacesUtils.goTo(printUrl);
    }

    public void previewReception(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("pdfId", PdfGlobals.PRINT_RECEPTION_ID.toString());
        paramPdf.put("receptionId", receptionId.toString());
        previewUrl = PdfUtils.getUrlViewHTML(paramPdf);
        FacesUtils.newTab(previewUrl);
    }

    public List<VSmReception> getvSmReceptionList() {
        return vSmReceptionList;
    }

    public void setvSmReceptionList(List<VSmReception> vSmReceptionList) {
        this.vSmReceptionList = vSmReceptionList;
    }

    public List<VSmReceptionLine> getvSmReceptionLineList() {
        return vSmReceptionLineList;
    }

    public void setvSmReceptionLineList(List<VSmReceptionLine> vSmReceptionLineList) {
        this.vSmReceptionLineList = vSmReceptionLineList;
    }

    public List<SelectItem> getSupplierList() {
        return supplierList;
    }

    public void setSupplierList(List<SelectItem> supplierList) {
        this.supplierList = supplierList;
    }

    public VSmProduct getvSmProduct() {
        return vSmProduct;
    }

    public void setvSmProduct(VSmProduct vSmProduct) {
        this.vSmProduct = vSmProduct;
    }

    public SmReceptionLine getSmReceptionLine() {
        return smReceptionLine;
    }

    public void setSmReceptionLine(SmReceptionLine smReceptionLine) {
        this.smReceptionLine = smReceptionLine;
    }

    public VSmReceptionLine getvSmReceptionLine() {
        return vSmReceptionLine;
    }

    public void setvSmReceptionLine(VSmReceptionLine vSmReceptionLine) {
        this.vSmReceptionLine = vSmReceptionLine;
    }

    public SmReception getSmReception() {
        return smReception;
    }

    public void setSmReception(SmReception smReception) {
        this.smReception = smReception;
    }

    public VSmReceptionSummary getvSmReceptionSummary() {
        return vSmReceptionSummary;
    }

    public void setvSmReceptionSummary(VSmReceptionSummary vSmReceptionSummary) {
        this.vSmReceptionSummary = vSmReceptionSummary;
    }

    public Long getSmReceptionLineId() {
        return smReceptionLineId;
    }

    public void setSmReceptionLineId(Long smReceptionLineId) {
        this.smReceptionLineId = smReceptionLineId;
    }

    public Long getReceptionId() {
        return receptionId;
    }

    public void setReceptionId(Long receptionId) {
        this.receptionId = receptionId;
    }

    public Long getvSmProductId() {
        return vSmProductId;
    }

    public void setvSmProductId(Long vSmProductId) {
        this.vSmProductId = vSmProductId;
    }

    public Long getLastReceptionId() {
        return lastReceptionId;
    }

    public void setLastReceptionId(Long lastReceptionId) {
        this.lastReceptionId = lastReceptionId;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getpReference() {
        return pReference;
    }

    public void setpReference(String pReference) {
        this.pReference = pReference;
    }

    public String getpReferenceHidden() {
        return pReferenceHidden;
    }

    public void setpReferenceHidden(String pReferenceHidden) {
        this.pReferenceHidden = pReferenceHidden;
    }

    public String getpDesignation() {
        return pDesignation;
    }

    public void setpDesignation(String pDesignation) {
        this.pDesignation = pDesignation;
    }

    public String getpDesignationHidden() {
        return pDesignationHidden;
    }

    public void setpDesignationHidden(String pDesignationHidden) {
        this.pDesignationHidden = pDesignationHidden;
    }

    public String getpUnitPrice() {
        return pUnitPrice;
    }

    public void setpUnitPrice(String pUnitPrice) {
        this.pUnitPrice = pUnitPrice;
    }

    public String getpQuantity() {
        return pQuantity;
    }

    public void setpQuantity(String pQuantity) {
        this.pQuantity = pQuantity;
    }

    public String getpTotalHt() {
        return pTotalHt;
    }

    public void setpTotalHt(String pTotalHt) {
        this.pTotalHt = pTotalHt;
    }

    public String getSummaryTotalQuantity() {
        return summaryTotalQuantity;
    }

    public void setSummaryTotalQuantity(String summaryTotalQuantity) {
        this.summaryTotalQuantity = summaryTotalQuantity;
    }

    public String getSummaryTotalHt() {
        return summaryTotalHt;
    }

    public void setSummaryTotalHt(String summaryTotalHt) {
        this.summaryTotalHt = summaryTotalHt;
    }

    public String getSummaryTvaAmount() {
        return summaryTvaAmount;
    }

    public void setSummaryTvaAmount(String summaryTvaAmount) {
        this.summaryTvaAmount = summaryTvaAmount;
    }

    public String getSummaryTotalTtc() {
        return summaryTotalTtc;
    }

    public void setSummaryTotalTtc(String summaryTotalTtc) {
        this.summaryTotalTtc = summaryTotalTtc;
    }

}
