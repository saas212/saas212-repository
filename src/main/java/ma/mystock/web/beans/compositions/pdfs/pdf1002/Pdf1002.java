/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.beans.compositions.pdfs.pdf1002;

import java.util.List;
import ma.mystock.core.dao.entities.views.VSmOrderSupplier;
import ma.mystock.core.dao.entities.views.VSmOrderSupplierLine;
import ma.mystock.core.dao.entities.views.VSmOrderSupplierSummary;
import ma.mystock.web.utils.datatype.MyDouble;
import ma.mystock.web.utils.datatype.MyLong;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.web.utils.jsf.FacesUtils;
import ma.mystock.module.pageManagement.beans.AbstractPdf;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmOrderSupplierEJB;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmOrderSupplierLineEJB;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmOrderSupplierSummaryEJB;

/**
 *
 * @author Abdessamad HALLAL
 */
public class Pdf1002 extends AbstractPdf {

    // list
    private List<VSmOrderSupplierLine> vSmOrderSupplierLineList;
    private VSmOrderSupplier vSmOrderSupplier;
    private VSmOrderSupplierSummary vSmOrderSupplierSummary;

    // reception
    private String reference;
    private String supplier;
    private String status;
    private String note;

    // summary
    private String summaryTotalQuantity;
    private String summaryTotalHt;
    private String summaryTvaAmount;
    private String summaryTotalTtc;

    private Long orderSupplierId;

    @Override
    public void onInit() {
        orderSupplierId = MyLong.toLong(FacesUtils.getRequestParameter("orderSupplierId"));
        System.out.println(" orderSupplierId = " + orderSupplierId);
        InitList();
        initSubList();
        initSummary();
    }

    public void InitList() {
        paramQuery.clear();
        paramQuery.put("id", orderSupplierId);
        vSmOrderSupplier = vSmOrderSupplierEJB.executeSingleQuery(VSmOrderSupplier.findById, paramQuery);

        if (MyIs.isNotNull(vSmOrderSupplier)) {

            reference = vSmOrderSupplier.getReference();
            supplier = vSmOrderSupplier.getSupplierCompanyName();
            note = vSmOrderSupplier.getNote();
            status = vSmOrderSupplier.getOrderSupplierStatusName();
        }
    }

    public void initSubList() {

        paramQuery.clear();
        paramQuery.put("orderSupplierId", orderSupplierId);
        vSmOrderSupplierLineList = vSmOrderSupplierLineEJB.executeQuery(VSmOrderSupplierLine.findByOrderSupplierId, paramQuery);
    }

    public void initSummary() {
        if (MyIs.isNotNull(orderSupplierId)) {

            paramQuery.clear();
            paramQuery.put("orderSupplierId", orderSupplierId);
            vSmOrderSupplierSummary = vSmOrderSupplierSummaryEJB.executeSingleQuery(VSmOrderSupplierSummary.findByOrderSupplierId, paramQuery);

            if (MyIs.isNotNull(vSmOrderSupplierSummary)) {

                summaryTotalQuantity = MyLong.toString(vSmOrderSupplierSummary.getSummaryTotalQuantity());
                summaryTotalHt = MyDouble.toString(vSmOrderSupplierSummary.getSummaryTotalHt());
                summaryTvaAmount = MyDouble.toString(vSmOrderSupplierSummary.getSummaryTvaAmount());
                summaryTotalTtc = MyDouble.toString(vSmOrderSupplierSummary.getSummaryTotalTtc());

                if ("".equalsIgnoreCase(summaryTotalQuantity)) {
                    summaryTotalQuantity = "0";
                }
                if ("".equalsIgnoreCase(summaryTotalHt)) {
                    summaryTotalHt = "0";
                }
                if ("".equalsIgnoreCase(summaryTvaAmount)) {
                    summaryTvaAmount = "0";
                }
                if ("".equalsIgnoreCase(summaryTotalTtc)) {
                    summaryTotalTtc = "0";
                }
            }
        }
    }

    public List<VSmOrderSupplierLine> getvSmOrderSupplierLineList() {
        return vSmOrderSupplierLineList;
    }

    public void setvSmOrderSupplierLineList(List<VSmOrderSupplierLine> vSmOrderSupplierLineList) {
        this.vSmOrderSupplierLineList = vSmOrderSupplierLineList;
    }

    public VSmOrderSupplier getvSmOrderSupplier() {
        return vSmOrderSupplier;
    }

    public void setvSmOrderSupplier(VSmOrderSupplier vSmOrderSupplier) {
        this.vSmOrderSupplier = vSmOrderSupplier;
    }

    public VSmOrderSupplierSummary getvSmOrderSupplierSummary() {
        return vSmOrderSupplierSummary;
    }

    public void setvSmOrderSupplierSummary(VSmOrderSupplierSummary vSmOrderSupplierSummary) {
        this.vSmOrderSupplierSummary = vSmOrderSupplierSummary;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getSummaryTotalQuantity() {
        return summaryTotalQuantity;
    }

    public void setSummaryTotalQuantity(String summaryTotalQuantity) {
        this.summaryTotalQuantity = summaryTotalQuantity;
    }

    public String getSummaryTotalHt() {
        return summaryTotalHt;
    }

    public void setSummaryTotalHt(String summaryTotalHt) {
        this.summaryTotalHt = summaryTotalHt;
    }

    public String getSummaryTvaAmount() {
        return summaryTvaAmount;
    }

    public void setSummaryTvaAmount(String summaryTvaAmount) {
        this.summaryTvaAmount = summaryTvaAmount;
    }

    public String getSummaryTotalTtc() {
        return summaryTotalTtc;
    }

    public void setSummaryTotalTtc(String summaryTotalTtc) {
        this.summaryTotalTtc = summaryTotalTtc;
    }

    public Long getOrderSupplierId() {
        return orderSupplierId;
    }

    public void setOrderSupplierId(Long orderSupplierId) {
        this.orderSupplierId = orderSupplierId;
    }

}
