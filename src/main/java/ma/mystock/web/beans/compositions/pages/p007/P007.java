package ma.mystock.web.beans.compositions.pages.p007;

import java.util.List;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import ma.mystock.core.dao.entities.SmCustomer;
import ma.mystock.core.dao.entities.views.VSmCustomer;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.web.utils.datatype.MyLong;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import ma.mystock.module.pageManagement.annotations.Attribute;
import ma.mystock.web.utils.components.ContactManagementComponent;
import ma.mystock.web.utils.functionUtils.DateUtils;
import ma.mystock.web.utils.globals.GlobalsAttributs;
import ma.mystock.web.utils.lovs.LovsUtils;
import ma.mystock.web.utils.sessions.SessionsGetter;

/**
 *
 * @author abdou
 */
public class P007 extends AbstractPage {

    @Attribute(itemCode = "p007.s2.firstName")
    private String firstName;

    @Attribute(itemCode = "p007.s2.lastName")
    private String lastName;

    @Attribute(itemCode = "p007.s2.city")
    private String city;

    @Attribute(itemCode = "p007.s2.type")
    private String type;

    @Attribute(itemCode = "p007.s2.category")
    private String category;

    @Attribute(itemCode = "p007.s2.company")
    private String company;

    @Attribute(itemCode = "p007.s2.representative")
    private String representative;

    @Attribute(itemCode = "p007.s2.shortLabel")
    private String shortLabel;

    @Attribute(itemCode = "p007.s2.fullLabel")
    private String fullLabel;

    @Attribute(itemCode = "p007.s2.note")
    private String note;

    @Attribute(itemCode = "p007.s2.identification")
    private String identification;

    @Attribute(itemCode = "p007.s2.active")
    private String active;

    @Attribute(itemCode = "p007.s1")
    private List<VSmCustomer> vSmCustomerList;

    private List<SelectItem> customerTypeItems;
    private List<SelectItem> customerCategoryItems;

    private List<SelectItem> countryItems;
    private List<SelectItem> cityItems;

    private SmCustomer smCustomer;
    private Long customerId;

    private ContactManagementComponent contactManagementComponent;

    // 1 = Société 2 = Particuler
    private String nature;

    @Override
    public void onInit() {
        setShowForm(false);
        setActive(GlobalsAttributs.Y);
        nature = "1";
        initList();
    }

    public void initList() {

        System.out.println(" => " + cltModuleId);
        System.out.println(" => " + getCltModuleId());

        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        vSmCustomerList = vSmCustomerEJB.executeQuery(VSmCustomer.findByCltModuleId, paramQuery);

        customerTypeItems = LovsUtils.getSmCustomerTypeItem();
        countryItems = LovsUtils.getInfCountryItem();
        cityItems = LovsUtils.getInfCityItem();
        customerCategoryItems = LovsUtils.getSmCustomerCategoryItem();
    }

    public void add(AjaxBehaviorEvent e) {

        setFirstName("");
        setLastName("");
        setCompany("");
        setNature("1");
        setShortLabel("");
        setFullLabel("");
        setNote("");
        setCity("");
        setCategory("");
        setType("");
        setActive("");

        setShowForm(true);
        setOperation(GlobalsAttributs.OPERATION_ADD);
    }

    public void edit(AjaxBehaviorEvent e) {

        customerId = (Long) e.getComponent().getAttributes().get("customerId");

        paramQuery.clear();
        paramQuery.put("id", customerId);
        smCustomer = smCustomerEJB.executeSingleQuery(SmCustomer.findById, paramQuery);

        if (MyIs.isNull(smCustomer)) {
            return;
        }
        System.out.println(" init pentity id //// ... ");
        contactManagementComponent = new ContactManagementComponent();
        contactManagementComponent.init(smCustomer.getEntityId());

        firstName = smCustomer.getFirstName();
        lastName = smCustomer.getLastName();
        company = smCustomer.getCompanyName();
        shortLabel = smCustomer.getShortLabel();
        fullLabel = smCustomer.getFullLabel();
        active = smCustomer.getActive();
        note = smCustomer.getNote();
        representative = smCustomer.getRepresentative();
        nature = MyLong.toString(smCustomer.getCustomerNatureId());

        type = MyLong.toString(smCustomer.getCustomerTypeId());
        category = MyLong.toString(smCustomer.getCustomerCategoryId());

        setOperation(GlobalsAttributs.OPERATION_EDIT);
        setShowForm(true);
    }

    @Override
    public void onSavePartial(AjaxBehaviorEvent e) {

        if (!messages.isEmpty()) {
            return;
        }

        if (GlobalsAttributs.OPERATION_ADD.equals(getOperation())) {

            smCustomer = new SmCustomer();
            smCustomer.setUserCreation(SessionsGetter.getCltUserId());
            smCustomer.setDateCreation(DateUtils.getCurrentDate());
            smCustomer.setCltModuleId(cltModuleId);

        } else if (GlobalsAttributs.OPERATION_EDIT.equals(getOperation())) {

            paramQuery.clear();
            paramQuery.put("id", customerId);
            smCustomer = smCustomerEJB.executeSingleQuery(SmCustomer.findById, paramQuery);
            smCustomer.setUserUpdate(SessionsGetter.getCltUserId());
            smCustomer.setDateUpdate(DateUtils.getCurrentDate());
        }

        smCustomer.setFirstName(firstName);
        smCustomer.setLastName(lastName);
        smCustomer.setCompanyName(company);
        smCustomer.setShortLabel(shortLabel);
        smCustomer.setFullLabel(fullLabel);
        smCustomer.setNote(note);

        smCustomer.setActive(active);
        smCustomer.setRepresentative(representative);

        smCustomer.setCustomerNatureId(MyLong.toLong(nature));

        smCustomer.setCustomerTypeId(MyLong.toLong(type));
        smCustomer.setCustomerCategoryId(MyLong.toLong(category));

        smCustomer = smCustomerEJB.executeMerge(smCustomer);

        initList();

        setShowForm(false);
    }

    @Override
    public void onDeletePartial(AjaxBehaviorEvent e) {

        customerId = (Long) e.getComponent().getAttributes().get("customerId");

        paramQuery.clear();
        paramQuery.put("id", customerId);

        smCustomer = smCustomerEJB.executeSingleQuery(SmCustomer.findById, paramQuery);
        smCustomerEJB.executeDelete(smCustomer);

        initList();
    }

    @Override
    public void onCancelPartial(AjaxBehaviorEvent e) {
        showForm = false;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getShortLabel() {
        return shortLabel;
    }

    public void setShortLabel(String shortLabel) {
        this.shortLabel = shortLabel;
    }

    public String getFullLabel() {
        return fullLabel;
    }

    public void setFullLabel(String fullLabel) {
        this.fullLabel = fullLabel;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public List<VSmCustomer> getvSmCustomerList() {
        return vSmCustomerList;
    }

    public void setvSmCustomerList(List<VSmCustomer> vSmCustomerList) {
        this.vSmCustomerList = vSmCustomerList;
    }

    public List<SelectItem> getCountryItems() {
        return countryItems;
    }

    public void setCountryItems(List<SelectItem> countryItems) {
        this.countryItems = countryItems;
    }

    public List<SelectItem> getCityItems() {
        return cityItems;
    }

    public void setCityItems(List<SelectItem> cityItems) {
        this.cityItems = cityItems;
    }

    public SmCustomer getSmCustomer() {
        return smCustomer;
    }

    public void setSmCustomer(SmCustomer smCustomer) {
        this.smCustomer = smCustomer;
    }

    public List<SelectItem> getCustomerCategoryItems() {
        return customerCategoryItems;
    }

    public void setCustomerCategoryItems(List<SelectItem> customerCategoryItems) {
        this.customerCategoryItems = customerCategoryItems;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public List<SelectItem> getCustomerTypeItems() {
        return customerTypeItems;
    }

    public void setCustomerTypeItems(List<SelectItem> customerTypeItems) {
        this.customerTypeItems = customerTypeItems;
    }

    public ContactManagementComponent getContactManagementComponent() {
        return contactManagementComponent;
    }

    public void setContactManagementComponent(ContactManagementComponent contactManagementComponent) {
        this.contactManagementComponent = contactManagementComponent;
    }

    public String getRepresentative() {
        return representative;
    }

    public void setRepresentative(String representative) {
        this.representative = representative;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

}
