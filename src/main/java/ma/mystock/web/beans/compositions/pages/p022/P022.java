/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.beans.compositions.pages.p022;

import java.util.List;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import ma.mystock.core.dao.entities.CltParameter;
import ma.mystock.core.dao.entities.views.VCltParameter;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import ma.mystock.module.pageManagement.messages.MessageList;
import ma.mystock.module.pageManagement.annotations.Attribute;
import ma.mystock.web.utils.globals.GlobalsAttributs;
import ma.mystock.web.utils.lovs.LovsUtils;
import ma.mystock.web.utils.sessions.SessionsGetter;

/**
 *
 * Abdou
 */
public class P022 extends AbstractPage {

    @Attribute(itemCode = "parameter.s1")
    private List<VCltParameter> cltParameterList;

    @Attribute(itemCode = "parameter.s2.name")
    private String name;

    @Attribute(itemCode = "parameter.s2.description")
    private String description;

    @Attribute(itemCode = "parameter.s2.value")
    private String value;

    @Attribute(itemCode = "parameter.s2.parameterType")
    private String parameterType;
    private List<SelectItem> parameterTypeList;

    private VCltParameter vCltParameter;
    private CltParameter cltParameter;

    private Long cltParameterId;

    @Override
    public void onInit() {

        setShowForm(false);
        initListCltParameter();

    }

    public void initListCltParameter() {

        paramQuery.clear();
        paramQuery.put("active", GlobalsAttributs.Y);

        cltParameterList = vCltParameterEJB.executeQuery(VCltParameter.findAllActive, paramQuery);
        parameterTypeList = LovsUtils.getCltParameterTypeItem();

    }

    public void initAttribut() {
        setName("");
        setDescription("");
        setValue("");
        setParameterType("");
        setOperation("");
    }

    public void edit(AjaxBehaviorEvent e) {
        log.info("cltParameterId = " + e.getComponent().getAttributes().get("cltParameterId"));
        cltParameterId = (Long) e.getComponent().getAttributes().get("cltParameterId");

        paramQuery.clear();
        paramQuery.put("id", cltParameterId);
        vCltParameter = vCltParameterEJB.executeSingleQuery(VCltParameter.findById, paramQuery);

        if (MyIs.isNull(vCltParameter)) {
            return;
        }

        setName(vCltParameter.getName());
        setDescription(vCltParameter.getDescription());
        setValue(vCltParameter.getDefaultValue());
        setParameterType(String.valueOf(vCltParameter.getParameterTypeId()));

        setShowForm(true);
        operation = GlobalsAttributs.OPERATION_EDIT;

        log.info("Opération : " + operation + " - id : " + cltParameterId);
    }

    @Override
    public void onDeletePartial(AjaxBehaviorEvent e) {

        log.info("delete 1 : " + cltParameterId);

        cltParameterId = (Long) e.getComponent().getAttributes().get("cltParameterId");

        paramQuery.clear();
        paramQuery.put("id", cltParameterId);
        cltParameter = cltParameterEJB.executeSingleQuery(CltParameter.findById, paramQuery);

        if (MyIs.isNotNull(cltParameter)) {
            cltParameterEJB.executeDelete(cltParameter);
        }

        initListCltParameter();

    }

    public void addParameter(AjaxBehaviorEvent event) {
        showForm = true;
        cltParameterId = null;
        operation = GlobalsAttributs.OPERATION_ADD;
        log.info("show form true addParameter ");
    }

    public void cancelParameter(AjaxBehaviorEvent event) {
        showForm = false;
        log.info("show form false");
        initAttribut();
    }

    @Override
    public void onSavePartial(AjaxBehaviorEvent e) {

        if (!messages.isEmpty()) {
            return;
        }

        if (GlobalsAttributs.OPERATION_ADD.equals(operation)) {

            cltParameter = new CltParameter();
            cltParameter.setUserCreation(Long.valueOf(SessionsGetter.getCltUserId()));
            cltParameter.setActive(GlobalsAttributs.Y);

        } else if (GlobalsAttributs.OPERATION_EDIT.equals(operation)) {

            paramQuery.clear();
            paramQuery.put("id", cltParameterId);
            cltParameter = cltParameterEJB.executeSingleQuery(CltParameter.findById, paramQuery);
            cltParameter.setUserUpdate(Long.valueOf(SessionsGetter.getCltUserId()));
        }

        cltParameter.setName(name);
        cltParameter.setDescription(description);
        cltParameter.setDefaultValue(value);
        cltParameter.setParameterTypeId(Long.parseLong(parameterType));

        cltParameter = cltParameterEJB.executeMerge(cltParameter);

        setShowForm(false);
        initAttribut();
        initListCltParameter();

    }

    public void validateParameter(AjaxBehaviorEvent e) {

        if (!messages.isEmpty()) {
            return;
        }

        if (cltParameterId != null) {
            paramQuery.clear();
            paramQuery.put("id", cltParameterId);
            cltParameter = cltParameterEJB.executeSingleQuery(CltParameter.findById, paramQuery);
            cltParameter.setUserUpdate(Long.valueOf(SessionsGetter.getCltUserId()));
            cltParameter = cltParameterEJB.executeMerge(cltParameter);

        }

        setShowForm(false);
        initAttribut();
        initListCltParameter();

    }

    public List<VCltParameter> getCltParameterList() {
        return cltParameterList;
    }

    public void setCltParameterList(List<VCltParameter> cltParameterList) {
        this.cltParameterList = cltParameterList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getParameterType() {
        return parameterType;
    }

    public void setParameterType(String parameterType) {
        this.parameterType = parameterType;
    }

    public List<SelectItem> getParameterTypeList() {
        return parameterTypeList;
    }

    public void setParameterTypeList(List<SelectItem> parameterTypeList) {
        this.parameterTypeList = parameterTypeList;
    }

    public VCltParameter getvCltParameter() {
        return vCltParameter;
    }

    public void setvCltParameter(VCltParameter vCltParameter) {
        this.vCltParameter = vCltParameter;
    }

    public CltParameter getCltParameter() {
        return cltParameter;
    }

    public void setCltParameter(CltParameter cltParameter) {
        this.cltParameter = cltParameter;
    }

    public Long getCltParameterId() {
        return cltParameterId;
    }

    public void setCltParameterId(Long cltParameterId) {
        this.cltParameterId = cltParameterId;
    }

}
