/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.beans.compositions.pages.p024;

import java.util.List;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import ma.mystock.core.dao.entities.PmPageParameter;
import ma.mystock.core.dao.entities.views.VPmPageParameter;
import ma.mystock.web.utils.datatype.MyString;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import ma.mystock.module.pageManagement.messages.MessageList;
import ma.mystock.module.pageManagement.annotations.Attribute;
import ma.mystock.module.pageManagement.annotations.Page;
import ma.mystock.web.utils.globals.GlobalsAttributs;
import ma.mystock.web.utils.lovs.LovsUtils;
import ma.mystock.web.utils.sessions.SessionsGetter;

/**
 *
 * Abdou
 */
@Page(id = "1", description = "cette page pour garé les paramètre des clients")
public class P024 extends AbstractPage {

    @Attribute(itemCode = "pageParameter.s1")
    private List<VPmPageParameter> pmPageParameterList;

    @Attribute(itemCode = "pageParameter.s2.name")
    private String name;

    @Attribute(itemCode = "pageParameter.s2.description")
    private String description;

    @Attribute(itemCode = "pageParameter.s2.value")
    private String value;

    @Attribute(itemCode = "pageParameter.s2.parameterType")
    private String pageParameterType;
    private List<SelectItem> pageParameterTypeList;
    @Attribute(itemCode = "pageParameter.s2.page")
    private String page;
    private List<SelectItem> pmPageList;

    private VPmPageParameter vPmPageParameter;
    private PmPageParameter pmPageParameter;

    private Long pmPageParameterId;

    @Override
    public void onInit() {

        setShowForm(false);
        initListPmPageParameter();

    }

    public void initListPmPageParameter() {

        paramQuery.clear();
        paramQuery.put("active", GlobalsAttributs.Y);

        pmPageParameterList = vPmPageParameterEJB.executeQuery(VPmPageParameter.findAllActive, paramQuery);
        pmPageList = LovsUtils.getPmPageItem();
        pageParameterTypeList = LovsUtils.getPmPageParameterTypeItem();

    }

    public void initAttribut() {
        setName("");
        setDescription("");
        setValue("");
        setPageParameterType("");
        setPage("");
        setOperation("");
    }

    public void edit(AjaxBehaviorEvent e) {
        log.info("pmPageParameterId = " + e.getComponent().getAttributes().get("pmPageParameterId"));
        pmPageParameterId = (Long) e.getComponent().getAttributes().get("pmPageParameterId");

        paramQuery.clear();
        paramQuery.put("id", pmPageParameterId);
        vPmPageParameter = vPmPageParameterEJB.executeSingleQuery(VPmPageParameter.findById, paramQuery);

        if (MyIs.isNull(vPmPageParameter)) {
            return;
        }

        setName(vPmPageParameter.getName());
        setDescription(vPmPageParameter.getDescription());
        setValue(vPmPageParameter.getDefaultValue());
        setPageParameterType(String.valueOf(vPmPageParameter.getPageParameterTypeId()));
        setPage(String.valueOf(vPmPageParameter.getPageId()));
        setShowForm(true);
        operation = GlobalsAttributs.OPERATION_EDIT;

        log.info("Opération : " + operation + " - id : " + pmPageParameterId);
    }

    @Override
    public void onDeletePartial(AjaxBehaviorEvent e) {

        log.info("delete 1 : " + pmPageParameterId);

        pmPageParameterId = (Long) e.getComponent().getAttributes().get("cltParameterId");

        paramQuery.clear();
        paramQuery.put("id", pmPageParameterId);
        pmPageParameter = pmPageParameterEJB.executeSingleQuery(PmPageParameter.findById, paramQuery);

        if (MyIs.isNotNull(pmPageParameter)) {
            pmPageParameterEJB.executeDelete(pmPageParameter);
        }

        initListPmPageParameter();

    }

    public void addPageParameter(AjaxBehaviorEvent event) {
        showForm = true;
        pmPageParameterId = null;
        operation = GlobalsAttributs.OPERATION_ADD;
        log.info("show form true addParameter ");
    }

    public void cancelPageParameter(AjaxBehaviorEvent event) {
        showForm = false;
        log.info("show form false");
        initAttribut();
    }

    @Override
    public void onSavePartial(AjaxBehaviorEvent e) {

        if (!messages.isEmpty()) {
            return;
        }

        if (GlobalsAttributs.OPERATION_ADD.equals(operation)) {

            pmPageParameter = new PmPageParameter();
            pmPageParameter.setUserCreation(Long.valueOf(SessionsGetter.getCltUserId()));
            pmPageParameter.setActive(GlobalsAttributs.Y);

        } else if (GlobalsAttributs.OPERATION_EDIT.equals(operation)) {

            paramQuery.clear();
            paramQuery.put("id", pmPageParameterId);
            pmPageParameter = pmPageParameterEJB.executeSingleQuery(PmPageParameter.findById, paramQuery);
            pmPageParameter.setUserUpdate(Long.valueOf(SessionsGetter.getCltUserId()));
        }

        pmPageParameter.setName(name);
        pmPageParameter.setDescription(description);
        pmPageParameter.setDefaultValue(value);
        if (!MyString.isEmpty(pageParameterType)) {
            pmPageParameter.setPageParameterTypeId(Long.parseLong(pageParameterType));
        } else {
            pmPageParameter.setPageParameterTypeId(null);
        }

        if (!MyString.isEmpty(page)) {
            pmPageParameter.setPageId(Long.parseLong(page));
        } else {
            pmPageParameter.setPageId(null);
        }
        //nullpointer ex
        pmPageParameter = pmPageParameterEJB.executeMerge(pmPageParameter);

        setShowForm(false);
        initAttribut();
        initListPmPageParameter();

    }

    public void validatePageParameter(AjaxBehaviorEvent e) {

        if (!messages.isEmpty()) {
            return;
        }

        if (pmPageParameterId != null) {
            paramQuery.clear();
            paramQuery.put("id", pmPageParameterId);
            pmPageParameter = pmPageParameterEJB.executeSingleQuery(PmPageParameter.findById, paramQuery);
            pmPageParameter.setUserUpdate(Long.valueOf(SessionsGetter.getCltUserId()));
            pmPageParameter = pmPageParameterEJB.executeMerge(pmPageParameter);

        }

        setShowForm(false);
        initAttribut();
        initListPmPageParameter();

    }

    public List<VPmPageParameter> getPmPageParameterList() {
        return pmPageParameterList;
    }

    public void setPmPageParameterList(List<VPmPageParameter> pmPageParameterList) {
        this.pmPageParameterList = pmPageParameterList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPageParameterType() {
        return pageParameterType;
    }

    public void setPageParameterType(String pageParameterType) {
        this.pageParameterType = pageParameterType;
    }

    public List<SelectItem> getPageParameterTypeList() {
        return pageParameterTypeList;
    }

    public void setPageParameterTypeList(List<SelectItem> pageParameterTypeList) {
        this.pageParameterTypeList = pageParameterTypeList;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public List<SelectItem> getPmPageList() {
        return pmPageList;
    }

    public void setPmPageList(List<SelectItem> pmPageList) {
        this.pmPageList = pmPageList;
    }

    public VPmPageParameter getvPmPageParameter() {
        return vPmPageParameter;
    }

    public void setvPmPageParameter(VPmPageParameter vPmPageParameter) {
        this.vPmPageParameter = vPmPageParameter;
    }

    public PmPageParameter getPmPageParameter() {
        return pmPageParameter;
    }

    public void setPmPageParameter(PmPageParameter pmPageParameter) {
        this.pmPageParameter = pmPageParameter;
    }

    public Long getPmPageParameterId() {
        return pmPageParameterId;
    }

    public void setPmPageParameterId(Long pmPageParameterId) {
        this.pmPageParameterId = pmPageParameterId;
    }

}
