package ma.mystock.web.beans.compositions.pages.p006;

import java.util.List;
import javax.faces.model.SelectItem;
import ma.mystock.core.dao.entities.SmReception;
//import ma.mystock.core.dao.entities.SmReceptionProducts;
import ma.mystock.core.dao.entities.views.VSmProduct;
import ma.mystock.core.dao.entities.views.VSmReception;
//import ma.mystock.core.dao.entities.views.VSmReceptionProducts;
import ma.mystock.core.dao.entities.views.VSmReceptionSummary;
import ma.mystock.module.pageManagement.beans.AbstractPage;

/**
 *
 * @author abdou
 */
public class P006 extends AbstractPage {

    // reception
    private String codeSeq;
    private String supplier;
    private String souche;
    private String deadline;
    private String note;
    private String deposit;

    // produit
    private String productRef;
    private String productDesignation;
    private String productQte;
    private String productPuTtc;
    private String productRemise;
    private String productTotalHt;
    private String productTva;
    private String productTotalTtc;

    // summary
    private String summaryQuantityTotal;
    private String summaryTotalHtTotal;
    private String summaryTva;
    private String summaryAmount;
    private String summaryTotalTtcTotal;

    // others
    private List<VSmReception> vSmReceptionList;
    //private List<VSmReceptionProducts> vSmReceptionProductsList;
    private List<SelectItem> supplierList;
    private List<SelectItem> depositList;

    private SmReception smReception;
    private VSmReception vSmReception;
    private VSmProduct vSmProduct;
    //private SmReceptionProducts smReceptionProducts;
    //private VSmReceptionProducts vSmReceptionProducts;
    private VSmReceptionSummary vSmReceptionSummary;

    private Long receptionId;
    private Long productId;
    private Long lastProductId;
    private Long smReceptionProductsId;

    /*
     @Override
     public void onInit() {

     initVSmReceptionList();
     lastProductId = null;

     }

     public void initVSmReceptionList() {
     paramQuery.clear();
     paramQuery.put("cltModuleId", cltModuleId);
     paramQuery.put("receptionStatusId", GlobalsAttributs.RECEPTION_STATUS_ID_IN_PROGRESS);
     vSmReceptionList = vSmReceptionEJB.executeQuery(VSmReception.findByCltModuleIdAndReceptionStatusId, paramQuery);
     System.out.println(" ==> ");
     supplierList = LovsUtils.getSmSupplierItem();
     depositList = LovsUtils.getSmDepositItem();

     }

     public void initProductAttribut() {

     productDesignation = "";
     productQte = "";
     productPuTtc = "";
     productRemise = "";
     productTotalHt = "";
     productTva = "";
     productTotalTtc = "";
     }

     public void initReceptionAttribut() {

     codeSeq = "";
     supplier = "";
     souche = "";
     deadline = "";
     note = "";
     deposit = "";
     }

     public void initVSmReceptionProductsList() {
     paramQuery.clear();
     log.info(" = receptionId : " + receptionId);
     paramQuery.put("receptionId", receptionId);
     paramQuery.put("active", GlobalsAttributs.Y);
     vSmReceptionProductsList = vSmReceptionProductsEJB.executeQuery(VSmReceptionProducts.findByReceptionIdAndActive, paramQuery);
     }

     public void initReceptionSummary() {

     summaryQuantityTotal = "0";
     summaryTotalHtTotal = "0";
     summaryTva = MyDouble.toString(ModuleParameterUtils.getTvaDefaultValue());
     summaryAmount = "0";
     summaryTotalTtcTotal = "0";

     if (receptionId != null) {
     paramQuery.clear();
     paramQuery.put("receptionId", receptionId);
     vSmReceptionSummary = vSmReceptionSummaryEJB.executeSingleQuery(VSmReceptionSummary.findByReceptionId, paramQuery);

     if (vSmReceptionSummary != null) {
     summaryQuantityTotal = MyLong.toString(vSmReceptionSummary.getQuantityTotal());
     summaryTotalHtTotal = MyDouble.toString(vSmReceptionSummary.getTotalHtTotal());
     summaryTva = MyDouble.toString(vSmReceptionSummary.getTva());
     summaryAmount = MyDouble.toString(vSmReceptionSummary.getAmountTva());
     summaryTotalTtcTotal = MyDouble.toString(vSmReceptionSummary.getTotalTtcTotal());
     }
     }
     }

     /**
     * ############## Gestion des réceptions ##############
     *
     * @param e
     /
     public void addReception(AjaxBehaviorEvent e) {

     initProductAttribut();
     initReceptionAttribut();
     initVSmReceptionProductsList();
     initReceptionSummary();

     setOperation(GlobalsAttributs.OPERATION_ADD);
     setShowForm(true);

     productRef = "";
     receptionId = null;
     productId = null;
     lastProductId = null;
     smReceptionProductsId = null;

     paramQuery.clear();
     paramQuery.put("cltModuleId", cltModuleId);
     Long max = vSmReceptionEJB.executeLongQuery(VSmReception.findMaxTenantSeq, paramQuery);

     if (MyIs.isNotNull(max)) {
     codeSeq = "R" + (max + 1);
     } else {
     codeSeq = "R1";
     }

     log.info("P006 @ add");
     }

     public void editReception(AjaxBehaviorEvent e) {
     receptionId = (Long) e.getComponent().getAttributes().get("receptionId");
     setOperation(GlobalsAttributs.OPERATION_EDIT);
     log.info("receptionId : " + receptionId);
     setShowForm(true);
     paramQuery.clear();
     paramQuery.put("id", receptionId);
     vSmReception = vSmReceptionEJB.executeSingleQuery(VSmReception.findById, paramQuery);
     if (vSmReception != null) {
     codeSeq = vSmReception.getCodeSeq();
     supplier = MyString.toString(vSmReception.getSupplierId());
     souche = vSmReception.getSouche();
     deadline = MyString.toString(vSmReception.getDeadline());
     note = vSmReception.getNote();
     deposit = MyString.toString(vSmReception.getDepositId());
     }
     initVSmReceptionProductsList();
     initReceptionSummary();
     log.info("P006 @ edit");
     }

     @Override
     public void onDeletePartial(AjaxBehaviorEvent e) {
     receptionId = (Long) e.getComponent().getAttributes().get("receptionId");

     paramQuery.clear();
     paramQuery.put("id", receptionId);
     smReception = smReceptionEJB.executeSingleQuery(SmReception.findById, paramQuery);

     if (MyIs.isNotNull(smReception)) {
     smReceptionEJB.executeDelete(smReception);
     }

     initVSmReceptionList();
     log.info("P006 @ onDeletePartial");

     }

     public void cancelReception(AjaxBehaviorEvent e) {
     initVSmReceptionList();
     receptionId = null;
     setShowForm(false);
     log.info("P006 @ cancel");
     }

     public void submitReception(AjaxBehaviorEvent e) {

     if (vSmReceptionProductsList.isEmpty()) {
     messages.add(new MessageDetail(1, "ID_MESSGAE_ERROR", "vous n'avez pas le droit de soummeter une réception sans aucun produit"));
     } else {
     paramQuery.clear();
     paramQuery.put("id", receptionId);
     smReception = smReceptionEJB.executeSingleQuery(SmReception.findById, paramQuery);

     if (MyIs.isNotNull(smReception)) {
     smReception.setReceptionStatusId(GlobalsAttributs.RECEPTION_STATUS_ID_SUBMIT);
     smReception = smReceptionEJB.executeMerge(smReception);
     }

     initVSmReceptionList();
     receptionId = null;
     setShowForm(false);
     }
     // set Y
     log.info("P006 @ submitReception");
     }

     public void validateReception(AjaxBehaviorEvent e) {

     if (vSmReceptionProductsList.isEmpty()) {
     messages.add(new MessageDetail(1, "ID_MESSGAE_ERROR", "vous n'avez pas le droit de valider une réception sans aucun produit"));
     } else {
     log.info("1");
     paramQuery.clear();
     paramQuery.put("id", receptionId);
     smReception = smReceptionEJB.executeSingleQuery(SmReception.findById, paramQuery);
     log.info("2");
     if (MyIs.isNotNull(smReception)) {
     log.info("3");
     // augmenter la quantité
     for (VSmReceptionProducts o : vSmReceptionProductsList) {
     log.info("4");
     paramQuery.clear();
     paramQuery.put("id", o.getProductId());
     SmProduct smProduct = smProductEJB.executeSingleQuery(SmProduct.findById, paramQuery);
     if (smProduct != null) {
     smProduct.addQuantity(o.getQuantity());
     System.out.println(" 5 = smProduct : " + smProduct.getQuantity());
     smProduct = smProductEJB.executeMerge(smProduct);
     }
     }
     // update status
     smReception.setReceptionStatusId(GlobalsAttributs.RECEPTION_STATUS_ID_VALID);
     smReception = smReceptionEJB.executeMerge(smReception);
     }

     initVSmReceptionList();
     receptionId = null;
     setShowForm(false);
     }

     // set Y
     log.info("P006 @ validateReception");
     }

     @Override
     public void onSavePartial(AjaxBehaviorEvent e) {

     if (GlobalsAttributs.OPERATION_EDIT.equalsIgnoreCase(getOperation())) {
     paramQuery.clear();
     paramQuery.put("id", receptionId);
     smReception = smReceptionEJB.executeSingleQuery(SmReception.findById, paramQuery);
     } else if (GlobalsAttributs.OPERATION_ADD.equalsIgnoreCase(getOperation())) {
     smReception = new SmReception();
     smReception.setCltModuleId(cltModuleId);
     smReception.setReceptionStatusId(GlobalsAttributs.RECEPTION_STATUS_ID_IN_PROGRESS);
     smReception.setTva(ModuleParameterUtils.getTvaDefaultValue());
     }
     smReception.setSupplierId(MyLong.toLong(supplier));
     smReception.setSouche(souche);
     smReception.setDeadline(MyString.stringToDate(deadline));
     smReception.setNote(note);
     smReception.setDepositId(MyLong.toLong(deposit));

     smReception = smReceptionEJB.executeMerge(smReception);

     receptionId = smReception.getId();
     lastProductId = receptionId;

     setOperation(GlobalsAttributs.OPERATION_EDIT);
     }
    
    
    

     public void printAllReceptionsAction(AjaxBehaviorEvent e) {

     paramPdf.clear();
     paramPdf.put("fileName", "Receptions");
     paramPdf.put("pdfId", "1");

     printUrl = PdfUtils.getUrlDownloadPdf(paramPdf);
     MyFaces.goTo(printUrl);

     }

     public void previewAllReceptionsAction(AjaxBehaviorEvent e) {

     paramPdf.clear();
     paramPdf.put("pdfId", "1");

     previewUrl = PdfUtils.getUrlViewHTML(paramPdf);
     MyFaces.goTo(previewUrl);

     }

     /**
     * ############## Gestion des produits dans une réceptions ##############
     *
     * @param e
     /
     public void findProductByRef(AjaxBehaviorEvent e) {
     initProductAttribut();

     paramQuery.clear();
     paramQuery.put("cltModuleId", cltModuleId);
     paramQuery.put("reference", productRef);
     paramQuery.put("active", GlobalsAttributs.Y);
     vSmProduct = vSmProductEJB.executeSingleQuery(VSmProduct.findByCltModuleIdAndReferenceAndActive, paramQuery);

     if (vSmProduct != null) {
     productId = vSmProduct.getId();
     productDesignation = vSmProduct.getDesignation();
     productPuTtc = MyString.toString(vSmProduct.getPriceSale());
     productTotalHt = MyString.toString(vSmProduct.getPriceSale());
     productTotalTtc = MyString.toString(vSmProduct.getPriceSale());

     productQte = MyString.toString(BusinessTools.getQuantity());
     productRemise = MyString.toString(BusinessTools.getRemise());
     productTva = MyString.toString(BusinessTools.getTva());

     }
     }
     /* AutoComplet primefaces
     public void findProductByDesignation(SelectEvent event) {

     log.info("Up");
     try {
     log.info(" == " + event.getObject().toString());
     } catch (Exception ex) {
     log.info(" exp");
     }
     log.info("enf");
     }

     public List<String> completeText(String query) {
     log.info("1");
     List<String> results = new ArrayList<>();
     for (int i = 0; i < 10; i++) {
     results.add(query + i);
     }
     log.info("end");
     return results;
     }

     public List<VSmProduct> completeText1(String query) {
     log.info(" => query : " + query);

     List<VSmProduct> liste = vSmProductEJB.executeQuery(VSmProduct.findAll);

     log.info(" => " + liste.size());
     return liste;
     }
     /

     public void editProduct(AjaxBehaviorEvent e) {
     smReceptionProductsId = MyLong.toLong(e.getComponent().getAttributes().get("smReceptionProductsId"));

     paramQuery.clear();
     paramQuery.put("id", smReceptionProductsId);
     vSmReceptionProducts = vSmReceptionProductsEJB.executeSingleQuery(VSmReceptionProducts.findById, paramQuery);

     if (vSmReceptionProducts != null) {
     productRef = vSmReceptionProducts.getReference();
     productDesignation = vSmReceptionProducts.getDesignation();
     productQte = MyLong.toString(vSmReceptionProducts.getQuantity());
     productPuTtc = MyDouble.toString(vSmReceptionProducts.getUnitPriceBuy());
     productRemise = MyDouble.toString(vSmReceptionProducts.getRemise());
     productTotalHt = MyDouble.toString(vSmReceptionProducts.getTotalHt());
     productTva = MyDouble.toString(vSmReceptionProducts.getTva());
     productTotalTtc = MyDouble.toString(vSmReceptionProducts.getTotalTtc());
     }
     lastProductId = null;
     }

     public void deleteProduct(AjaxBehaviorEvent e) {
     smReceptionProductsId = MyLong.toLong(e.getComponent().getAttributes().get("smReceptionProductsId"));

     paramQuery.clear();
     paramQuery.put("id", smReceptionProductsId);
     smReceptionProducts = smReceptionProductsEJB.executeSingleQuery(SmReceptionProducts.findById, paramQuery);
     smReceptionProductsEJB.executeDelete(smReceptionProducts);

     lastProductId = null;
     productRef = "";
     initProductAttribut();
     initVSmReceptionProductsList();

     }

     public void cleanProduct(AjaxBehaviorEvent e) {
     productRef = "";
     productId = null;
     initProductAttribut();
     }

     public void saveProduct(AjaxBehaviorEvent e) {

     if (MyIs.isNull(receptionId)) {
     onSavePartial(e);
     }

     if (MyIs.isNotNull(smReceptionProductsId)) {
     paramQuery.clear();
     paramQuery.put("id", smReceptionProductsId);
     smReceptionProducts = smReceptionProductsEJB.executeSingleQuery(SmReceptionProducts.findById, paramQuery);
     log.info("edit");
     } else {
     smReceptionProducts = new SmReceptionProducts();
     log.info("add");
     }

     smReceptionProducts.setProductId(productId);
     smReceptionProducts.setDesignation(productDesignation);
     smReceptionProducts.setUnitPriceBuy(MyDouble.toDouble(productPuTtc));
     smReceptionProducts.setRemise(MyDouble.toDouble(productRemise));
     smReceptionProducts.setQuantity(MyLong.toLong(productQte));
     smReceptionProducts.setTva(MyDouble.toDouble(productTva));
     smReceptionProducts.setActive(GlobalsAttributs.Y);
     smReceptionProducts.setReceptionId(receptionId);
     smReceptionProducts = smReceptionProductsEJB.executeMerge(smReceptionProducts);

     lastProductId = smReceptionProducts.getId();
     productRef = "";
     initProductAttribut();
     initVSmReceptionProductsList();
     initReceptionSummary();
     }

     public void printReceptionAction(AjaxBehaviorEvent e) {

     paramPdf.clear();
     paramPdf.put("fileName", "Receptions_" + receptionId);
     paramPdf.put("pdfId", "2");
     paramPdf.put("receptionId", "" + receptionId);

     printUrl = PdfUtils.getUrlDownloadPdf(paramPdf);
     MyFaces.goTo(printUrl);

     }

     public void previewReceptionAction(AjaxBehaviorEvent e) {

     paramPdf.clear();
     paramPdf.put("pdfId", "2");
     paramPdf.put("receptionId", "" + receptionId);

     previewUrl = PdfUtils.getUrlViewHTML(paramPdf);

     System.out.println(" => + previewReceptionAction : " + previewUrl);
     MyFaces.goTo(previewUrl);

     }

     public void calculTotalTtc(AjaxBehaviorEvent e) {
     // calculer le total
     }
    
     */
    public List<VSmReception> getvSmReceptionList() {
        return vSmReceptionList;
    }

    public void setvSmReceptionList(List<VSmReception> vSmReceptionList) {
        this.vSmReceptionList = vSmReceptionList;
    }

    public Long getReceptionId() {
        return receptionId;
    }

    public void setReceptionId(Long receptionId) {
        this.receptionId = receptionId;
    }

    public SmReception getSmReception() {
        return smReception;
    }

    public void setSmReception(SmReception smReception) {
        this.smReception = smReception;
    }

    public String getCodeSeq() {
        return codeSeq;
    }

    public void setCodeSeq(String codeSeq) {
        this.codeSeq = codeSeq;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getSouche() {
        return souche;
    }

    public void setSouche(String souche) {
        this.souche = souche;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public List<SelectItem> getSupplierList() {
        return supplierList;
    }

    public void setSupplierList(List<SelectItem> supplierList) {
        this.supplierList = supplierList;
    }

    public List<SelectItem> getDepositList() {
        return depositList;
    }

    public void setDepositList(List<SelectItem> depositList) {
        this.depositList = depositList;
    }

    public String getProductRef() {
        return productRef;
    }

    public void setProductRef(String productRef) {
        this.productRef = productRef;
    }

    public String getProductDesignation() {
        return productDesignation;
    }

    public void setProductDesignation(String productDesignation) {
        this.productDesignation = productDesignation;
    }

    public String getProductQte() {
        return productQte;
    }

    public void setProductQte(String productQte) {
        this.productQte = productQte;
    }

    public String getProductPuTtc() {
        return productPuTtc;
    }

    public void setProductPuTtc(String productPuTtc) {
        this.productPuTtc = productPuTtc;
    }

    public String getProductRemise() {
        return productRemise;
    }

    public void setProductRemise(String productRemise) {
        this.productRemise = productRemise;
    }

    public String getProductTotalHt() {
        return productTotalHt;
    }

    public void setProductTotalHt(String productTotalHt) {
        this.productTotalHt = productTotalHt;
    }

    public String getProductTotalTtc() {
        return productTotalTtc;
    }

    public void setProductTotalTtc(String productTotalTtc) {
        this.productTotalTtc = productTotalTtc;
    }

    public String getProductTva() {
        return productTva;
    }

    public void setProductTva(String productTva) {
        this.productTva = productTva;
    }

    /*
     public List<VSmReceptionProducts> getvSmReceptionProductsList() {
     return vSmReceptionProductsList;
     }

     public void setvSmReceptionProductsList(List<VSmReceptionProducts> vSmReceptionProductsList) {
     this.vSmReceptionProductsList = vSmReceptionProductsList;
     }

     public VSmProduct getvSmProduct() {
     return vSmProduct;
     }

     public void setvSmProduct(VSmProduct vSmProduct) {
     this.vSmProduct = vSmProduct;
     }

     public SmReceptionProducts getSmReceptionProducts() {
     return smReceptionProducts;
     }

     public void setSmReceptionProducts(SmReceptionProducts smReceptionProducts) {
     this.smReceptionProducts = smReceptionProducts;
     }
     */
    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getLastProductId() {
        return lastProductId;
    }

    public void setLastProductId(Long lastProductId) {
        this.lastProductId = lastProductId;
    }

    /*
    public VSmReceptionProducts getvSmReceptionProducts() {
        return vSmReceptionProducts;
    }

    public void setvSmReceptionProducts(VSmReceptionProducts vSmReceptionProducts) {
        this.vSmReceptionProducts = vSmReceptionProducts;
    }
     */
    public Long getSmReceptionProductsId() {
        return smReceptionProductsId;
    }

    public void setSmReceptionProductsId(Long smReceptionProductsId) {
        this.smReceptionProductsId = smReceptionProductsId;
    }

    public VSmReception getvSmReception() {
        return vSmReception;
    }

    public void setvSmReception(VSmReception vSmReception) {
        this.vSmReception = vSmReception;
    }

    public String getSummaryQuantityTotal() {
        return summaryQuantityTotal;
    }

    public void setSummaryQuantityTotal(String summaryQuantityTotal) {
        this.summaryQuantityTotal = summaryQuantityTotal;
    }

    public String getSummaryTotalHtTotal() {
        return summaryTotalHtTotal;
    }

    public void setSummaryTotalHtTotal(String summaryTotalHtTotal) {
        this.summaryTotalHtTotal = summaryTotalHtTotal;
    }

    public String getSummaryTva() {
        return summaryTva;
    }

    public void setSummaryTva(String summaryTva) {
        this.summaryTva = summaryTva;
    }

    public String getSummaryAmount() {
        return summaryAmount;
    }

    public void setSummaryAmount(String summaryAmount) {
        this.summaryAmount = summaryAmount;
    }

    public String getSummaryTotalTtcTotal() {
        return summaryTotalTtcTotal;
    }

    public void setSummaryTotalTtcTotal(String summaryTotalTtcTotal) {
        this.summaryTotalTtcTotal = summaryTotalTtcTotal;
    }

    public VSmReceptionSummary getvSmReceptionSummary() {
        return vSmReceptionSummary;
    }

    public void setvSmReceptionSummary(VSmReceptionSummary vSmReceptionSummary) {
        this.vSmReceptionSummary = vSmReceptionSummary;
    }

}
