/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.beans.compositions.pages.p027;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import ma.mystock.core.dao.entities.CltModule;
import ma.mystock.core.dao.entities.PmPageParameterModule;
import ma.mystock.core.dao.entities.views.VCltModule;
import ma.mystock.core.dao.entities.views.VCltUserClient;
import ma.mystock.core.dao.entities.views.VPmPageModule;
import ma.mystock.core.dao.entities.views.VPmPageParameter;
import ma.mystock.core.dao.entities.views.VPmPageParameterModule;
import ma.mystock.web.utils.datatype.MyString;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import ma.mystock.module.pageManagement.messages.MessageList;
import ma.mystock.module.pageManagement.annotations.Attribute;
import ma.mystock.web.utils.globals.GlobalsAttributs;
import ma.mystock.web.utils.sessions.SessionsGetter;
import static ma.mystock.web.utils.sessions.SessionsGetter.getCltClientId;

/**
 *
 * @author Abdou
 */
public class P027 extends AbstractPage {

    @Attribute(itemCode = "pageParameterModule.s1")
    private List<VCltUserClient> cltUserClientList;
    @Attribute(itemCode = "pageParameterModule.s2")
    private List<VCltModule> cltModuleList;
    @Attribute(itemCode = "pageParameterModule.s3")
    private List<VPmPageModule> pageModuleList;
    @Attribute(itemCode = "pageParameterModule.s4")
    private List<VPmPageParameterModule> pmPageParameterModuleList;

    @Attribute(itemCode = "pageParameterModule.s5.module")
    private String moduleId;

    @Attribute(itemCode = "pageParameterModule.s5.pageParameter")
    private String pageParameterId;

    @Attribute(itemCode = "pageParameterModule.s5.value")
    private String value;

    //private List<SelectItem> clientList;
    private List<SelectItem> pageParameterList;

    private VPmPageParameterModule vPmPageParameterModule;
    private PmPageParameterModule pmPageParameterModule;

    private String pmPageParameterModuleId;

    private Long cltClientIdP;
    private Long cltModuleIdP;
    private Long pmPageIdP;

    private boolean rendredCltUserClientList;
    private boolean rendredCltModuleList;
    private boolean rendredPageModuleList;
    private boolean rendredPmPageParameterModuleList;

    private boolean rendredClient;
    private boolean rendredModule;
    private boolean rendredPage;
    private boolean rendredAddPageParameter;

    @Override
    public void onInit() {
        rendredClient = true;
        rendredAddPageParameter = true;
        initListPmPageParameterModule();

    }

    public void initListPmPageParameterModule() {

        paramQuery.clear();
        paramQuery.put("active", GlobalsAttributs.Y);
        log.info("getCltClientId() : " + getCltClientId());
        log.info("getCltModuleTypeId() : " + getCltModuleTypeId());
        log.info("getCltUserId() : " + getCltUserId());
        log.info("getCltModuleId() : " + getCltModuleId());
        Map<String, Object> paramModule = new HashMap<String, Object>();
        paramModule.put("id", getCltModuleId());
        CltModule cltModuleT = cltModuleEJB.executeSingleQuery(CltModule.findById, paramModule);
        setCltModuleTypeId(cltModuleT.getModuleTypeId());
        if (getCltModuleTypeId() != null && 1L == getCltModuleTypeId()) {
            rendredCltUserClientList = false;
            rendredCltModuleList = false;
            rendredPageModuleList = true;
            rendredPmPageParameterModuleList = false;
            showForm = false;
            rendredClient = false;
            rendredModule = false;
            rendredPage = false;
        } else if (getCltModuleTypeId() != null && 2L == getCltModuleTypeId()) {
            rendredCltUserClientList = false;
            rendredCltModuleList = true;
            rendredPageModuleList = false;
            rendredPmPageParameterModuleList = false;
            showForm = false;
            rendredClient = false;
            rendredModule = true;
            rendredPage = false;
        } else {
            rendredCltUserClientList = true;
            rendredCltModuleList = false;
            rendredPageModuleList = false;
            rendredPmPageParameterModuleList = false;
            showForm = false;
            rendredClient = true;
            rendredModule = true;
            rendredPage = true;
        }
        paramQuery.put("cltModuleId", getCltModuleId());
        pageModuleList = vPmPageModuleEJB.executeQuery(VPmPageModule.findByCltModuleId, paramQuery);

        Map<String, Object> param = new HashMap<String, Object>();
        param.put("active", GlobalsAttributs.Y);
        param.put("userId", getCltUserId());
        cltUserClientList = vCltUserClientEJB.executeQuery(VCltUserClient.findByUserIdAndActive, param);
        Map<String, Object> paramM = new HashMap<String, Object>();
        paramM.put("clientId", getCltClientId());
        cltModuleList = vCltModuleEJB.executeQuery(VCltModule.findByClientId, paramM);

        Map<String, Object> paramPm = new HashMap<String, Object>();

        paramPm.put("cltModuleId", cltModuleIdP);
        paramPm.put("pageId", pmPageIdP);
        //pmPageParameterModuleList = vPmPageParameterModuleEJB.executeQuery(VPmPageParameterModule.findByPageIdAndcltModuleId, paramPm);

        //getPmPageParameterItem(getCltModuleId(),null, null);
    }

    public void getPmPageParameterItem(Long pageId, Long moduleId, Long id) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("active", GlobalsAttributs.Y);
        param.put("pageId", pageId);
        param.put("id", id);
        param.put("cltModuleId", moduleId);
        List<VPmPageParameter> vPmPageParameterList = vPmPageParameterEJB.executeQuery(VPmPageParameter.findAllActiveAndId, param);
        if (vPmPageParameterList == null || vPmPageParameterList.size() == 0) {
            rendredAddPageParameter = false;
        } else {
            rendredAddPageParameter = true;
        }
        List<SelectItem> supplierTypeList = new ArrayList<>();

        for (VPmPageParameter o : vPmPageParameterList) {
            supplierTypeList.add(new SelectItem(o.getId(), o.getName()));
        }

        pageParameterList = supplierTypeList;
    }

    public void initAttribut() {
        setModuleId("");
        setPageParameterId("");
        setValue("");
    }

    public void edit(AjaxBehaviorEvent e) {
        log.info("pmPageParameterModuleId = " + e.getComponent().getAttributes().get("pmPageParameterModuleId"));
        pmPageParameterModuleId = (String) e.getComponent().getAttributes().get("pmPageParameterModuleId");

        paramQuery.clear();
        paramQuery.put("code", pmPageParameterModuleId);
        vPmPageParameterModule = vPmPageParameterModuleEJB.executeSingleQuery(VPmPageParameterModule.findByCode, paramQuery);

        if (MyIs.isNull(vPmPageParameterModule)) {
            return;
        }
        //getPmPageParameterItem(vPmPageParameterModule.getPageId(), vPmPageParameterModule.getCltModuleId(), vPmPageParameterModule.getPageParameterId());
        setModuleId(String.valueOf(vPmPageParameterModule.getCltModuleId()));
        setPageParameterId(String.valueOf(vPmPageParameterModule.getPageParameterId()));
        setValue(vPmPageParameterModule.getValue());
        rendredCltUserClientList = false;
        rendredCltModuleList = false;
        rendredPageModuleList = false;
        rendredPmPageParameterModuleList = false;
        showForm = true;
        operation = GlobalsAttributs.OPERATION_EDIT;

        log.info("Opération : " + operation + " - id : " + pmPageParameterModuleId);
    }

    @Override
    public void onDeletePartial(AjaxBehaviorEvent e) {

        log.info("delete 1 : " + pmPageParameterModuleId);

        pmPageParameterModuleId = (String) e.getComponent().getAttributes().get("pmPageParameterModuleId");

        paramQuery.clear();
        paramQuery.put("id", pmPageParameterModuleId);
        pmPageParameterModule = pmPageParameterModuleEJB.executeSingleQuery(PmPageParameterModule.findById, paramQuery);

        if (MyIs.isNotNull(pmPageParameterModule)) {
            pmPageParameterModuleEJB.executeDelete(pmPageParameterModule);
        }

        initListPmPageParameterModule();

        rendredCltUserClientList = false;
        rendredCltModuleList = false;
        rendredPageModuleList = false;
        rendredPmPageParameterModuleList = true;
        showForm = false;

    }

    public void goToListModule(AjaxBehaviorEvent event) {
        rendredCltUserClientList = false;
        rendredCltModuleList = true;
        rendredPageModuleList = false;
        rendredPmPageParameterModuleList = false;
        showForm = false;
        //rendredClient = true;
        //rendredModule = false;

        cltClientIdP = (Long) event.getComponent().getAttributes().get("cltClientIdParam");
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("clientId", cltClientIdP);
        cltModuleList = vCltModuleEJB.executeQuery(VCltModule.findByClientId, param);
        log.info("show form true goToListModule ");
    }

    public void goToListPage(AjaxBehaviorEvent event) {
        rendredCltUserClientList = false;
        rendredCltModuleList = false;
        rendredPageModuleList = true;
        rendredPmPageParameterModuleList = false;
        showForm = false;
        // rendredClient = false;
        //rendredModule = true;
        cltModuleIdP = (Long) event.getComponent().getAttributes().get("cltModuleIdParam");
        getPmPageParameterItem(pmPageIdP, cltModuleIdP, null);
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("cltModuleId", cltModuleIdP);
        param.put("active", GlobalsAttributs.Y);
        pageModuleList = vPmPageModuleEJB.executeQuery(VPmPageModule.findByCltModuleId, param);
        log.info("show form true goToListPage ");
    }

    public void goToListPageParameter(AjaxBehaviorEvent event) {
        rendredCltUserClientList = false;
        rendredCltModuleList = false;
        rendredPageModuleList = false;
        rendredPmPageParameterModuleList = true;
        showForm = false;
        // rendredClient = false;
        //rendredModule = true;
        pmPageIdP = (Long) event.getComponent().getAttributes().get("pmPageIdParam");
        cltModuleIdP = (Long) event.getComponent().getAttributes().get("cltModuleIdParam");
        getPmPageParameterItem(pmPageIdP, cltModuleIdP, null);
        Map<String, Object> param = new HashMap<String, Object>();

        param.put("cltModuleId", cltModuleIdP);
        param.put("pageId", pmPageIdP);
        //pmPageParameterModuleList = vPmPageParameterModuleEJB.executeQuery(VPmPageParameterModule.findByPageIdAndcltModuleId, param);
        log.info("show form true goToListModuleParameter ");
    }

    public void addPageParameterModule(AjaxBehaviorEvent event) {
        rendredCltUserClientList = false;
        rendredCltModuleList = false;
        rendredPageModuleList = false;
        rendredPmPageParameterModuleList = false;
        showForm = true;
        //rendredClient = false;

        pmPageParameterModuleId = null;
        pmPageIdP = (Long) event.getComponent().getAttributes().get("pmPageIdParam");
        cltModuleIdP = (Long) event.getComponent().getAttributes().get("cltModuleIdParam");
        getPmPageParameterItem(pmPageIdP, cltModuleIdP, null);

        operation = GlobalsAttributs.OPERATION_ADD;
        log.info("show form true addPageParameterModule ");
    }

    public void cancelPageParameterClient(AjaxBehaviorEvent event) {
        rendredCltUserClientList = false;
        rendredCltModuleList = false;
        rendredPageModuleList = false;
        rendredPmPageParameterModuleList = true;
        showForm = false;
        //rendredClient = false;
        pmPageIdP = (Long) event.getComponent().getAttributes().get("pmPageIdParam");
        cltModuleIdP = (Long) event.getComponent().getAttributes().get("cltModuleIdParam");
        getPmPageParameterItem(pmPageIdP, cltModuleIdP, null);

        log.info("show form false");
        initAttribut();
    }

    public void cancelClient(AjaxBehaviorEvent event) {
        rendredCltUserClientList = true;
        rendredCltModuleList = false;
        rendredPageModuleList = false;
        rendredPmPageParameterModuleList = false;
        showForm = false;
        //rendredClient = false;

        log.info("show form false");
        initAttribut();
    }

    public void cancelPage(AjaxBehaviorEvent event) {
        rendredCltUserClientList = false;
        rendredCltModuleList = false;
        rendredPageModuleList = true;
        rendredPmPageParameterModuleList = false;
        showForm = false;
        //rendredClient = false;

        log.info("show form false");
        initAttribut();
    }

    public void cancelModule(AjaxBehaviorEvent event) {
        rendredCltUserClientList = false;
        rendredCltModuleList = true;
        rendredPageModuleList = false;
        rendredPmPageParameterModuleList = false;
        showForm = false;
        //rendredClient = true;

        log.info("show form false");
        initAttribut();
    }

    @Override
    public void onSavePartial(AjaxBehaviorEvent e) {

        if (!messages.isEmpty()) {
            return;
        }

        if (GlobalsAttributs.OPERATION_ADD.equals(operation)) {

            pmPageParameterModule = new PmPageParameterModule();
            pmPageParameterModule.setUserCreation(Long.valueOf(SessionsGetter.getCltUserId()));
            pmPageParameterModule.setActive(GlobalsAttributs.Y);

        } else if (GlobalsAttributs.OPERATION_EDIT.equals(operation)) {

            paramQuery.clear();
            paramQuery.put("id", pmPageParameterModuleId);
            pmPageParameterModule = pmPageParameterModuleEJB.executeSingleQuery(PmPageParameterModule.findById, paramQuery);
            pmPageParameterModule.setUserUpdate(Long.valueOf(SessionsGetter.getCltUserId()));
        }
        pmPageParameterModule.setCltModuleId(cltModuleIdP);

        if (!MyString.isEmpty(pageParameterId)) {
            pmPageParameterModule.setPageParameterId(Long.parseLong(pageParameterId));
        } else {
            pmPageParameterModule.setPageParameterId(null);
        }

        pmPageParameterModule.setValue(value);

        pmPageParameterModule = pmPageParameterModuleEJB.executeMerge(pmPageParameterModule);

        initAttribut();
        initListPmPageParameterModule();

        rendredCltUserClientList = false;
        rendredCltModuleList = false;
        rendredPageModuleList = false;
        rendredPmPageParameterModuleList = true;
        showForm = false;
        //rendredClient = false;

    }

    public void validateParameterClient(AjaxBehaviorEvent e) {

        if (!messages.isEmpty()) {
            return;
        }

        if (pmPageParameterModule != null) {
            paramQuery.clear();
            paramQuery.put("id", pmPageParameterModuleId);
            pmPageParameterModule = pmPageParameterModuleEJB.executeSingleQuery(PmPageParameterModule.findById, paramQuery);
            pmPageParameterModule.setUserUpdate(Long.valueOf(SessionsGetter.getCltUserId()));
            pmPageParameterModule = pmPageParameterModuleEJB.executeMerge(pmPageParameterModule);

        }

        setShowForm(false);
        initAttribut();
        initListPmPageParameterModule();

    }

    public List<VCltUserClient> getCltUserClientList() {
        return cltUserClientList;
    }

    public void setCltUserClientList(List<VCltUserClient> cltUserClientList) {
        this.cltUserClientList = cltUserClientList;
    }

    public List<VCltModule> getCltModuleList() {
        return cltModuleList;
    }

    public void setCltModuleList(List<VCltModule> cltModuleList) {
        this.cltModuleList = cltModuleList;
    }

    public List<VPmPageModule> getPageModuleList() {
        return pageModuleList;
    }

    public void setPageModuleList(List<VPmPageModule> pageModuleList) {
        this.pageModuleList = pageModuleList;
    }

    public List<VPmPageParameterModule> getPmPageParameterModuleList() {
        return pmPageParameterModuleList;
    }

    public void setPmPageParameterModuleList(List<VPmPageParameterModule> pmPageParameterModuleList) {
        this.pmPageParameterModuleList = pmPageParameterModuleList;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getPageParameterId() {
        return pageParameterId;
    }

    public void setPageParameterId(String pageParameterId) {
        this.pageParameterId = pageParameterId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<SelectItem> getPageParameterList() {
        return pageParameterList;
    }

    public void setPageParameterList(List<SelectItem> pageParameterList) {
        this.pageParameterList = pageParameterList;
    }

    public VPmPageParameterModule getvPmPageParameterModule() {
        return vPmPageParameterModule;
    }

    public void setvPmPageParameterModule(VPmPageParameterModule vPmPageParameterModule) {
        this.vPmPageParameterModule = vPmPageParameterModule;
    }

    public PmPageParameterModule getPmPageParameterModule() {
        return pmPageParameterModule;
    }

    public void setPmPageParameterModule(PmPageParameterModule pmPageParameterModule) {
        this.pmPageParameterModule = pmPageParameterModule;
    }

    public String getPmPageParameterModuleId() {
        return pmPageParameterModuleId;
    }

    public void setPmPageParameterModuleId(String pmPageParameterModuleId) {
        this.pmPageParameterModuleId = pmPageParameterModuleId;
    }

    public Long getCltClientIdP() {
        return cltClientIdP;
    }

    public void setCltClientIdP(Long cltClientIdP) {
        this.cltClientIdP = cltClientIdP;
    }

    public boolean isRendredCltUserClientList() {
        return rendredCltUserClientList;
    }

    public void setRendredCltUserClientList(boolean rendredCltUserClientList) {
        this.rendredCltUserClientList = rendredCltUserClientList;
    }

    public boolean isRendredCltModuleList() {
        return rendredCltModuleList;
    }

    public void setRendredCltModuleList(boolean rendredCltModuleList) {
        this.rendredCltModuleList = rendredCltModuleList;
    }

    public boolean isRendredPageModuleList() {
        return rendredPageModuleList;
    }

    public void setRendredPageModuleList(boolean rendredPageModuleList) {
        this.rendredPageModuleList = rendredPageModuleList;
    }

    public boolean isRendredPmPageParameterModuleList() {
        return rendredPmPageParameterModuleList;
    }

    public void setRendredPmPageParameterModuleList(boolean rendredPmPageParameterModuleList) {
        this.rendredPmPageParameterModuleList = rendredPmPageParameterModuleList;
    }

    public boolean isRendredClient() {
        return rendredClient;
    }

    public void setRendredClient(boolean rendredClient) {
        this.rendredClient = rendredClient;
    }

    public boolean isRendredModule() {
        return rendredModule;
    }

    public void setRendredModule(boolean rendredModule) {
        this.rendredModule = rendredModule;
    }

    public boolean isRendredAddPageParameter() {
        return rendredAddPageParameter;
    }

    public void setRendredAddPageParameter(boolean rendredAddPageParameter) {
        this.rendredAddPageParameter = rendredAddPageParameter;
    }

    public Long getCltModuleIdP() {
        return cltModuleIdP;
    }

    public void setCltModuleIdP(Long cltModuleIdP) {
        this.cltModuleIdP = cltModuleIdP;
    }

    public Long getPmPageIdP() {
        return pmPageIdP;
    }

    public void setPmPageIdP(Long pmPageIdP) {
        this.pmPageIdP = pmPageIdP;
    }

    public boolean isRendredPage() {
        return rendredPage;
    }

    public void setRendredPage(boolean rendredPage) {
        this.rendredPage = rendredPage;
    }

}
