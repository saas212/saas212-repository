package ma.mystock.web.beans.compositions.pages.p034;

import java.util.List;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import ma.mystock.core.dao.entities.SmOrderSupplier;
import ma.mystock.core.dao.entities.SmOrderSupplierLine;
import ma.mystock.core.dao.entities.views.VSmOrderSupplier;
import ma.mystock.core.dao.entities.views.VSmOrderSupplierLine;
import ma.mystock.core.dao.entities.views.VSmOrderSupplierSummary;
import ma.mystock.core.dao.entities.views.VSmProduct;
import ma.mystock.web.utils.datatype.MyDouble;
import ma.mystock.web.utils.datatype.MyLong;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.web.utils.jsf.FacesUtils;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import ma.mystock.web.utils.globals.GlobalsAttributs;
import ma.mystock.web.utils.lovs.LovsUtils;
import ma.mystock.web.utils.pdf.PdfGlobals;
import ma.mystock.web.utils.pdf.PdfUtils;

/**
 *
 * @author Abdessamad HALLAL
 * @desc : Commandes fournisseur (add, edit et delete)
 *
 */
public class P034 extends AbstractPage {

    private List<VSmOrderSupplier> vSmOrderSupplierList;
    private List<VSmOrderSupplierLine> vSmOrderSupplierLineList;

    private List<SelectItem> supplierList;

    private VSmProduct vSmProduct;
    private SmOrderSupplierLine smOrderSupplierLine;
    private VSmOrderSupplierLine vSmOrderSupplierLine;
    private SmOrderSupplier smOrderSupplier;
    private VSmOrderSupplierSummary vSmOrderSupplierSummary;

    private Long smOrderSupplierLineId;
    private Long orderSupplierId;
    private Long vSmProductId;

    private String reference;
    private String supplier;
    private String note;

    private String summaryTotalQuantity;
    private String summaryTotalHt;
    private String summaryTvaAmount;
    private String summaryTotalTtc;

    @Override
    public void onInit() {

        initList();
        initovs();
        initAttribute();
        log.info("onInit P032");
        setShowForm(false);

    }

    public void initList() {
        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        paramQuery.put("orderSupplierStatusId", GlobalsAttributs.ORDER_SUPPLIER_STATUS_ID_VALID);
        vSmOrderSupplierList = vSmOrderSupplierEJB.executeQuery(VSmOrderSupplier.findByCltModuleIdAndOrderSupplierStatusId, paramQuery);
    }

    public void initSubList() {

        paramQuery.clear();
        paramQuery.put("orderSupplierId", orderSupplierId);
        vSmOrderSupplierLineList = vSmOrderSupplierLineEJB.executeQuery(VSmOrderSupplierLine.findByOrderSupplierId, paramQuery);
    }

    public void initSummary() {

        paramQuery.clear();
        paramQuery.put("orderSupplierId", orderSupplierId);
        vSmOrderSupplierSummary = vSmOrderSupplierSummaryEJB.executeSingleQuery(VSmOrderSupplierSummary.findByOrderSupplierId, paramQuery);

        if (MyIs.isNotNull(vSmOrderSupplierSummary)) {

            summaryTotalQuantity = MyLong.toString(vSmOrderSupplierSummary.getSummaryTotalQuantity());
            summaryTotalHt = MyDouble.toString(vSmOrderSupplierSummary.getSummaryTotalHt());
            summaryTvaAmount = MyDouble.toString(vSmOrderSupplierSummary.getSummaryTvaAmount());
            summaryTotalTtc = MyDouble.toString(vSmOrderSupplierSummary.getSummaryTotalTtc());

            if ("".equalsIgnoreCase(summaryTotalQuantity)) {
                summaryTotalQuantity = "0";
            }
            if ("".equalsIgnoreCase(summaryTotalHt)) {
                summaryTotalHt = "0";
            }
            if ("".equalsIgnoreCase(summaryTvaAmount)) {
                summaryTvaAmount = "0";
            }
            if ("".equalsIgnoreCase(summaryTotalTtc)) {
                summaryTotalTtc = "0";
            }
        }

    }

    public void initovs() {
        supplierList = LovsUtils.getSmSupplierItems();
    }

    public void initAttribute() {
        setReference("");
        setSupplier("");
        setNote("");
    }

    public void editOrderSupplier(AjaxBehaviorEvent e) {

        orderSupplierId = (Long) e.getComponent().getAttributes().get("orderSupplierId");

        if (orderSupplierId != null) {

            initAttribute();

            paramQuery.clear();
            paramQuery.put("id", orderSupplierId);
            smOrderSupplier = smOrderSupplierEJB.executeSingleQuery(SmOrderSupplier.findById, paramQuery);

            if (MyIs.isNotNull(smOrderSupplier)) {

                reference = smOrderSupplier.getReference();
                supplier = MyLong.toString(smOrderSupplier.getSupplierId());
                note = smOrderSupplier.getNote();

            }
            initSubList();
        }
        initSummary();
        setShowForm(true);
    }

    public void cancelOrderSupplier(AjaxBehaviorEvent e) {
        orderSupplierId = null;
        initList();
        initAttribute();
        setShowForm(false);
    }

    public void printAllOrderSupplier(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("fileName", "bon_de_commandes_validee");
        paramPdf.put("pdfId", PdfGlobals.PRINT_ALL_ORDER_SUPPLIERS_ID.toString());
        paramPdf.put("orderSupplierStatusId", GlobalsAttributs.ORDER_SUPPLIER_STATUS_ID_VALID.toString());
        printUrl = PdfUtils.getUrlDownloadPdf(paramPdf);
        FacesUtils.goTo(printUrl);
    }

    public void previewAllOrderSupplier(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("pdfId", PdfGlobals.PRINT_ALL_ORDER_SUPPLIERS_ID.toString());
        paramPdf.put("orderSupplierStatusId", GlobalsAttributs.ORDER_SUPPLIER_STATUS_ID_VALID.toString());
        previewUrl = PdfUtils.getUrlViewHTML(paramPdf);
        FacesUtils.newTab(previewUrl);
    }

    public void printOrderSupplier(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("fileName", "bon_de_commandes_" + orderSupplierId.toString());
        paramPdf.put("pdfId", PdfGlobals.PRINT_ORDER_SUPPLIER_ID.toString());
        paramPdf.put("orderSupplierId", orderSupplierId.toString());
        printUrl = PdfUtils.getUrlDownloadPdf(paramPdf);
        FacesUtils.goTo(printUrl);
    }

    public void previewOrderSupplier(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("pdfId", PdfGlobals.PRINT_ORDER_SUPPLIER_ID.toString());
        paramPdf.put("orderSupplierId", orderSupplierId.toString());
        previewUrl = PdfUtils.getUrlViewHTML(paramPdf);
        FacesUtils.newTab(previewUrl);
    }

    public List<VSmOrderSupplier> getvSmOrderSupplierList() {
        return vSmOrderSupplierList;
    }

    public void setvSmOrderSupplierList(List<VSmOrderSupplier> vSmOrderSupplierList) {
        this.vSmOrderSupplierList = vSmOrderSupplierList;
    }

    public List<VSmOrderSupplierLine> getvSmOrderSupplierLineList() {
        return vSmOrderSupplierLineList;
    }

    public void setvSmOrderSupplierLineList(List<VSmOrderSupplierLine> vSmOrderSupplierLineList) {
        this.vSmOrderSupplierLineList = vSmOrderSupplierLineList;
    }

    public List<SelectItem> getSupplierList() {
        return supplierList;
    }

    public void setSupplierList(List<SelectItem> supplierList) {
        this.supplierList = supplierList;
    }

    public VSmProduct getvSmProduct() {
        return vSmProduct;
    }

    public void setvSmProduct(VSmProduct vSmProduct) {
        this.vSmProduct = vSmProduct;
    }

    public SmOrderSupplierLine getSmOrderSupplierLine() {
        return smOrderSupplierLine;
    }

    public void setSmOrderSupplierLine(SmOrderSupplierLine smOrderSupplierLine) {
        this.smOrderSupplierLine = smOrderSupplierLine;
    }

    public VSmOrderSupplierLine getvSmOrderSupplierLine() {
        return vSmOrderSupplierLine;
    }

    public void setvSmOrderSupplierLine(VSmOrderSupplierLine vSmOrderSupplierLine) {
        this.vSmOrderSupplierLine = vSmOrderSupplierLine;
    }

    public SmOrderSupplier getSmOrderSupplier() {
        return smOrderSupplier;
    }

    public void setSmOrderSupplier(SmOrderSupplier smOrderSupplier) {
        this.smOrderSupplier = smOrderSupplier;
    }

    public VSmOrderSupplierSummary getvSmOrderSupplierSummary() {
        return vSmOrderSupplierSummary;
    }

    public void setvSmOrderSupplierSummary(VSmOrderSupplierSummary vSmOrderSupplierSummary) {
        this.vSmOrderSupplierSummary = vSmOrderSupplierSummary;
    }

    public Long getSmOrderSupplierLineId() {
        return smOrderSupplierLineId;
    }

    public void setSmOrderSupplierLineId(Long smOrderSupplierLineId) {
        this.smOrderSupplierLineId = smOrderSupplierLineId;
    }

    public Long getOrderSupplierId() {
        return orderSupplierId;
    }

    public void setOrderSupplierId(Long orderSupplierId) {
        this.orderSupplierId = orderSupplierId;
    }

    public Long getvSmProductId() {
        return vSmProductId;
    }

    public void setvSmProductId(Long vSmProductId) {
        this.vSmProductId = vSmProductId;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getSummaryTotalQuantity() {
        return summaryTotalQuantity;
    }

    public void setSummaryTotalQuantity(String summaryTotalQuantity) {
        this.summaryTotalQuantity = summaryTotalQuantity;
    }

    public String getSummaryTotalHt() {
        return summaryTotalHt;
    }

    public void setSummaryTotalHt(String summaryTotalHt) {
        this.summaryTotalHt = summaryTotalHt;
    }

    public String getSummaryTvaAmount() {
        return summaryTvaAmount;
    }

    public void setSummaryTvaAmount(String summaryTvaAmount) {
        this.summaryTvaAmount = summaryTvaAmount;
    }

    public String getSummaryTotalTtc() {
        return summaryTotalTtc;
    }

    public void setSummaryTotalTtc(String summaryTotalTtc) {
        this.summaryTotalTtc = summaryTotalTtc;
    }

}
