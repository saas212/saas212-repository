/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.beans.compositions.pages.p003;

import java.util.List;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;
import ma.mystock.core.dao.entities.SmExpense;
import ma.mystock.core.dao.entities.views.VSmExpense;
import ma.mystock.web.utils.datatype.MyDouble;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.web.utils.datatype.MyLong;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import ma.mystock.module.pageManagement.annotations.Attribute;
import ma.mystock.module.pageManagement.annotations.ParamPage;
import ma.mystock.module.pageManagement.messages.MessageDetail;
import ma.mystock.web.utils.globals.GlobalsAttributs;
import ma.mystock.web.utils.lovs.LovsUtils;

/**
 *
 * @author abdou
 */
public class P003 extends AbstractPage {

    @Attribute(itemCode = "p003.s2.name")
    private String name;

    @Attribute(itemCode = "p003.s2.description")
    private String description;

    @Attribute(itemCode = "p003.s2.amount")
    private String amount;

    @Attribute(itemCode = "p003.s2.type")
    private String expenseTypeId;

    @Attribute(itemCode = "expense.s1")
    private List<VSmExpense> expenseList;

    private List<SelectItem> expenseTypeList;

    private SmExpense expense;

    private String active;

    private Long expenseId;

    @ParamPage(id = "1")
    private Long param;

    @Override
    public void onInit() {
        log.info("use message info ");

        System.out.println("cltModuleIdToManager : " + cltModuleIdToManager);
        System.out.println("cltClientIdToManager : " + cltClientIdToManager);

        showForm = false;
        initList();
    }

    public void initList() {

        messages.add(new MessageDetail(1, "ID_MESSGAE_ERROR", "ID_MESSGAE_ERROR"));
        messages.add(new MessageDetail(2, "ID_MESSGAE_INFO", "ID_MESSGAE_INFO"));
        messages.add(new MessageDetail(1, "ID_MESSGAE_ERROR", "ID_MESSGAE_ERROR"));
        messages.add(new MessageDetail(2, "ID_MESSGAE_INFO", "ID_MESSGAE_INFO"));
        messages.add(new MessageDetail(3, "ID_MESSGAE_WARNING", "ID_MESSGAE_WARNING"));
        messages.add(new MessageDetail(4, "ID_MESSGAE_SUCCESS", "ID_MESSGAE_SUCCESS"));
        messages.add(new MessageDetail(5, "ID_MESSGAE_NOTE", "ID_MESSGAE_NOTE"));

        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);

        System.out.println(" ID_MESSGAE_ERROR : " + messages.getErrors().size());
        System.out.println(" ID_MESSGAE_INFO : " + messages.getInfos().size());
        System.out.println(" ID_MESSGAE_WARNING : " + messages.getWarnings().size());
        System.out.println(" ID_MESSGAE_SUCCESS : " + messages.getSuccess().size());
        System.out.println(" ID_MESSGAE_NOTE : " + messages.getNotes().size());

        expenseList = vSmExpenseEJB.executeQuery(VSmExpense.findByCltModule, paramQuery);
        expenseTypeList = LovsUtils.getSmExpenseTypeItem(); //getLovSelectItem("SM_EXPENSE_TYPE");
    }

    public void add(ActionEvent e) {
        setExpenseTypeId("");
        setName("");
        setDescription("");
        setAmount("");
        showForm = true;
        operation = "add";
    }

    @Override
    public void onValidateBeforeSave(ActionEvent e) {

    }

    @Override
    public void onSave(ActionEvent e) {

        if (!messages.isEmpty()) {
            return;
        }
        /*
         if (GlobalsAttributs.OPERATION_ADD.equals(operation)) {

         expense = new SmExpense();
         expense.setUserCreation(SessionsValues.getCltUserId());
         expense.setCltModuleId(cltModuleId);

         } else if (GlobalsAttributs.OPERATION_EDIT.equals(operation)) {

         paramQuery.clear();
         paramQuery.put("id", expenseId);
         expense = smExpenseEJB.executeSingleQuery(SmExpense.findById, paramQuery);
         expense.setUserUpdate(SessionsValues.getCltUserId());
         }

         if (MyIs.isNotNull(expense)) {

         expense.setName(name);
         expense.setDescription(description);
         expense.setAmount(MyDouble.toDouble(amount));
         expense.setExpenseTypeId(MyLong.toLong(expenseTypeId));

         expense = smExpenseEJB.executeMerge(expense);

         }

         initList();

         showForm = false;
        
         */
    }

    public void edit(ActionEvent e) {

        expenseId = (Long) e.getComponent().getAttributes().get("expenseId");
        paramQuery.clear();
        paramQuery.put("id", expenseId);
        expense = smExpenseEJB.executeSingleQuery(SmExpense.findById, paramQuery);

        if (MyIs.isNotNull(expense)) {
            name = expense.getName();
            description = expense.getDescription();
            amount = MyDouble.toString(expense.getAmount());
            expenseTypeId = MyLong.toString(expense.getExpenseTypeId());
        }

        operation = GlobalsAttributs.OPERATION_EDIT;
        showForm = true;
    }

    @Override
    public void onDelete(ActionEvent e) {

        expenseId = (Long) e.getComponent().getAttributes().get("expenseId");

        paramQuery.clear();
        paramQuery.put("id", expenseId);
        expense = smExpenseEJB.executeSingleQuery(SmExpense.findById, paramQuery);

        if (MyIs.isNotNull(expense)) {
            smExpenseEJB.executeDelete(expense);
            pageManager.setShowDeleteSuccess(true);
        }

        initList();
    }

    @Override
    public void onCancel(ActionEvent e) {
        showForm = false;
    }

    public List<VSmExpense> getExpenseList() {
        return expenseList;
    }

    public void setExpenseList(List<VSmExpense> expenseList) {
        this.expenseList = expenseList;
    }

    public SmExpense getExpense() {
        return expense;
    }

    public void setExpense(SmExpense expense) {
        this.expense = expense;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public List<SelectItem> getExpenseTypeList() {
        return expenseTypeList;
    }

    public void setExpenseTypeList(List<SelectItem> expenseTypeList) {
        this.expenseTypeList = expenseTypeList;
    }

    public String getExpenseTypeId() {
        return expenseTypeId;
    }

    public void setExpenseTypeId(String expenseTypeId) {
        this.expenseTypeId = expenseTypeId;
    }

}
