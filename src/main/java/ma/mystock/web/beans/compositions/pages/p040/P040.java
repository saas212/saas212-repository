package ma.mystock.web.beans.compositions.pages.p040;

import java.util.ArrayList;
import java.util.List;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import ma.mystock.core.dao.entities.SmOrder;
import ma.mystock.core.dao.entities.SmOrderLine;
import ma.mystock.core.dao.entities.views.VSmCustomer;
import ma.mystock.core.dao.entities.views.VSmOrder;
import ma.mystock.core.dao.entities.views.VSmOrderLine;
import ma.mystock.core.dao.entities.views.VSmOrderSummary;
import ma.mystock.core.dao.entities.views.VSmProduct;
import ma.mystock.web.utils.MyGenerator;
import ma.mystock.web.utils.datatype.MyDouble;
import ma.mystock.web.utils.datatype.MyLong;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.web.utils.jsf.FacesUtils;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import static ma.mystock.web.utils.springBeans.GloablsServices.smOrderEJB;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmCustomerEJB;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmOrderEJB;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmOrderLineEJB;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmOrderSummaryEJB;
import ma.mystock.web.utils.globals.GlobalsAttributs;
import ma.mystock.web.utils.lovs.LovsUtils;
import ma.mystock.web.utils.pdf.PdfGlobals;
import ma.mystock.web.utils.pdf.PdfUtils;

/**
 *
 * @author Abdessamad HALLAL
 * @desc : vente client (add, edit et delete)
 *
 */
public class P040 extends AbstractPage {

    private List<VSmOrder> vSmOrderList;
    private List<VSmOrderLine> vSmOrderLineList;

    private List<SelectItem> customerItems;
    private List<VSmCustomer> customerList;

    private VSmProduct vSmProduct;
    private SmOrderLine smOrderLine;
    private VSmOrderLine vSmOrderLine;
    private SmOrder smOrder;
    private VSmOrderSummary vSmOrderSummary;

    private Long smOrderLineId;
    private Long orderId;
    private Long vSmProductId;
    private Long lastOrderId;

    private String reference;
    private String customer;
    private String note;

    private String pReference;
    private String pReferenceHidden;
    private String pDesignation;
    private String pDesignationHidden;
    private String pUnitPrice;
    private String pQuantity;
    private String pTotalHt;

    private String summaryTotalQuantity;
    private String summaryTotalHt;
    private String summaryTvaAmount;
    private String summaryTotalTtc;

    @Override
    public void onInit() {

        initList();
        initovs();
        initAttribute();
        log.info("onInit P038");
        setShowForm(false);

    }

    public void initList() {
        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        paramQuery.put("orderStatusId", GlobalsAttributs.ORDER_STATUS_ID_VALID);
        vSmOrderList = vSmOrderEJB.executeQuery(VSmOrder.findByCltModuleIdAndOrderStatusId, paramQuery);
    }

    public void initSubList() {

        paramQuery.clear();
        paramQuery.put("orderId", orderId);
        vSmOrderLineList = vSmOrderLineEJB.executeQuery(VSmOrderLine.findByOrderId, paramQuery);

    }

    public void initSummary() {

        paramQuery.clear();
        paramQuery.put("orderId", orderId);
        vSmOrderSummary = vSmOrderSummaryEJB.executeSingleQuery(VSmOrderSummary.findByOrderId, paramQuery);

        if (MyIs.isNotNull(vSmOrderSummary)) {

            summaryTotalQuantity = MyLong.toString(vSmOrderSummary.getSummaryTotalQuantity());
            summaryTotalHt = MyDouble.toString(vSmOrderSummary.getSummaryTotalHt());
            summaryTvaAmount = MyDouble.toString(vSmOrderSummary.getSummaryTvaAmount());
            summaryTotalTtc = MyDouble.toString(vSmOrderSummary.getSummaryTotalTtc());

            if ("".equalsIgnoreCase(summaryTotalQuantity)) {
                summaryTotalQuantity = "0";
            }
            if ("".equalsIgnoreCase(summaryTotalHt)) {
                summaryTotalHt = "0";
            }
            if ("".equalsIgnoreCase(summaryTvaAmount)) {
                summaryTvaAmount = "0";
            }
            if ("".equalsIgnoreCase(summaryTotalTtc)) {
                summaryTotalTtc = "0";
            }
        }

    }

    public void initovs() {
        customerItems = LovsUtils.getSmCustomerItems();
    }

    public void initAttribute() {
        setReference("");
        setCustomer("");
        setNote("");
    }

    public void initPAttribute() {
        setpReference("");
        setpReferenceHidden("");
        setpDesignation("");
        setpDesignationHidden("");
        setpUnitPrice("");
        setpQuantity("");
        setpTotalHt("");
    }

    public void editOrder(AjaxBehaviorEvent e) {

        orderId = (Long) e.getComponent().getAttributes().get("orderId");

        if (orderId != null) {

            initAttribute();

            paramQuery.clear();
            paramQuery.put("id", orderId);
            smOrder = smOrderEJB.executeSingleQuery(SmOrder.findById, paramQuery);

            if (MyIs.isNotNull(smOrder)) {

                reference = smOrder.getReference();
                customer = MyLong.toString(smOrder.getCustomerId());
                note = smOrder.getNote();

            }
            initSubList();
            initPAttribute();
        }
        initSummary();
        setShowForm(true);
    }

    public void cancelOrder(AjaxBehaviorEvent e) {
        orderId = null;
        initList();
        initAttribute();
        setShowForm(false);
    }

    public void printAllOrder(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("fileName", "ventes_en_cours");
        paramPdf.put("pdfId", PdfGlobals.PRINT_ALL_ORDERS_ID.toString());
        paramPdf.put("orderStatusId", GlobalsAttributs.ORDER_STATUS_ID_VALID.toString());
        printUrl = PdfUtils.getUrlDownloadPdf(paramPdf);
        FacesUtils.goTo(printUrl);
    }

    public void previewAllOrder(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("pdfId", PdfGlobals.PRINT_ALL_ORDERS_ID.toString());
        paramPdf.put("orderStatusId", GlobalsAttributs.ORDER_STATUS_ID_VALID.toString());
        previewUrl = PdfUtils.getUrlViewHTML(paramPdf);
        FacesUtils.newTab(previewUrl);
    }

    public void printOrder(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("fileName", "vente_" + orderId.toString());
        paramPdf.put("pdfId", PdfGlobals.PRINT_ORDER_ID.toString());
        paramPdf.put("orderId", orderId.toString());
        printUrl = PdfUtils.getUrlDownloadPdf(paramPdf);
        FacesUtils.goTo(printUrl);
    }

    public void previewOrder(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("pdfId", PdfGlobals.PRINT_ORDER_ID.toString());
        paramPdf.put("orderId", orderId.toString());
        previewUrl = PdfUtils.getUrlViewHTML(paramPdf);
        FacesUtils.newTab(previewUrl);
    }

    public List<VSmOrder> getvSmOrderList() {
        return vSmOrderList;
    }

    public void setvSmOrderList(List<VSmOrder> vSmOrderList) {
        this.vSmOrderList = vSmOrderList;
    }

    public List<VSmOrderLine> getvSmOrderLineList() {
        return vSmOrderLineList;
    }

    public void setvSmOrderLineList(List<VSmOrderLine> vSmOrderLineList) {
        this.vSmOrderLineList = vSmOrderLineList;
    }

    public List<SelectItem> getCustomerItems() {
        return customerItems;
    }

    public void setCustomerItems(List<SelectItem> customerItems) {
        this.customerItems = customerItems;
    }

    public List<VSmCustomer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<VSmCustomer> customerList) {
        this.customerList = customerList;
    }

    public VSmProduct getvSmProduct() {
        return vSmProduct;
    }

    public void setvSmProduct(VSmProduct vSmProduct) {
        this.vSmProduct = vSmProduct;
    }

    public SmOrderLine getSmOrderLine() {
        return smOrderLine;
    }

    public void setSmOrderLine(SmOrderLine smOrderLine) {
        this.smOrderLine = smOrderLine;
    }

    public VSmOrderLine getvSmOrderLine() {
        return vSmOrderLine;
    }

    public void setvSmOrderLine(VSmOrderLine vSmOrderLine) {
        this.vSmOrderLine = vSmOrderLine;
    }

    public SmOrder getSmOrder() {
        return smOrder;
    }

    public void setSmOrder(SmOrder smOrder) {
        this.smOrder = smOrder;
    }

    public VSmOrderSummary getvSmOrderSummary() {
        return vSmOrderSummary;
    }

    public void setvSmOrderSummary(VSmOrderSummary vSmOrderSummary) {
        this.vSmOrderSummary = vSmOrderSummary;
    }

    public Long getSmOrderLineId() {
        return smOrderLineId;
    }

    public void setSmOrderLineId(Long smOrderLineId) {
        this.smOrderLineId = smOrderLineId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getvSmProductId() {
        return vSmProductId;
    }

    public void setvSmProductId(Long vSmProductId) {
        this.vSmProductId = vSmProductId;
    }

    public Long getLastOrderId() {
        return lastOrderId;
    }

    public void setLastOrderId(Long lastOrderId) {
        this.lastOrderId = lastOrderId;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getpReference() {
        return pReference;
    }

    public void setpReference(String pReference) {
        this.pReference = pReference;
    }

    public String getpReferenceHidden() {
        return pReferenceHidden;
    }

    public void setpReferenceHidden(String pReferenceHidden) {
        this.pReferenceHidden = pReferenceHidden;
    }

    public String getpDesignation() {
        return pDesignation;
    }

    public void setpDesignation(String pDesignation) {
        this.pDesignation = pDesignation;
    }

    public String getpDesignationHidden() {
        return pDesignationHidden;
    }

    public void setpDesignationHidden(String pDesignationHidden) {
        this.pDesignationHidden = pDesignationHidden;
    }

    public String getpUnitPrice() {
        return pUnitPrice;
    }

    public void setpUnitPrice(String pUnitPrice) {
        this.pUnitPrice = pUnitPrice;
    }

    public String getpQuantity() {
        return pQuantity;
    }

    public void setpQuantity(String pQuantity) {
        this.pQuantity = pQuantity;
    }

    public String getpTotalHt() {
        return pTotalHt;
    }

    public void setpTotalHt(String pTotalHt) {
        this.pTotalHt = pTotalHt;
    }

    public String getSummaryTotalQuantity() {
        return summaryTotalQuantity;
    }

    public void setSummaryTotalQuantity(String summaryTotalQuantity) {
        this.summaryTotalQuantity = summaryTotalQuantity;
    }

    public String getSummaryTotalHt() {
        return summaryTotalHt;
    }

    public void setSummaryTotalHt(String summaryTotalHt) {
        this.summaryTotalHt = summaryTotalHt;
    }

    public String getSummaryTvaAmount() {
        return summaryTvaAmount;
    }

    public void setSummaryTvaAmount(String summaryTvaAmount) {
        this.summaryTvaAmount = summaryTvaAmount;
    }

    public String getSummaryTotalTtc() {
        return summaryTotalTtc;
    }

    public void setSummaryTotalTtc(String summaryTotalTtc) {
        this.summaryTotalTtc = summaryTotalTtc;
    }

}
