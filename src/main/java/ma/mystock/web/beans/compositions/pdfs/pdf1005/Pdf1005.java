/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.beans.compositions.pdfs.pdf1005;

import java.util.List;
import ma.mystock.core.dao.entities.views.VSmOrder;
import ma.mystock.web.utils.datatype.MyLong;
import ma.mystock.web.utils.jsf.FacesUtils;
import ma.mystock.module.pageManagement.beans.AbstractPdf;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmOrderEJB;

/**
 *
 * @author Abdessamad HALLAL
 */
public class Pdf1005 extends AbstractPdf {

    private List<VSmOrder> vSmOrderList;
    private Long orderStatusId;

    @Override
    public void onInit() {
        InitList();
    }

    public void InitList() {

        orderStatusId = MyLong.toLong(FacesUtils.getRequestParameter("orderStatusId"));

        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        paramQuery.put("orderStatusId", orderStatusId);
        vSmOrderList = vSmOrderEJB.executeQuery(VSmOrder.findByCltModuleIdAndOrderStatusId, paramQuery);

    }

    public List<VSmOrder> getvSmOrderList() {
        return vSmOrderList;
    }

    public void setvSmOrderList(List<VSmOrder> vSmOrderList) {
        this.vSmOrderList = vSmOrderList;
    }

}
