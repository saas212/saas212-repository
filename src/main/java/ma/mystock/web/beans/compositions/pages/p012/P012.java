package ma.mystock.web.beans.compositions.pages.p012;

import ma.mystock.module.pageManagement.beans.AbstractPage;

import ma.mystock.core.dao.entities.views.VSmOrderDetails;

/**
 *
 * @author abdou
 */
public class P012 extends AbstractPage {

    private VSmOrderDetails vSmOrderDetails;

    @Override
    public void onInit() {
        vSmOrderDetails = genericServices.findVSmOrderDetailsByCltModuleId(cltModuleId);

        log.info("onInit StockState");
    }

    public VSmOrderDetails getvSmOrderDetails() {
        return vSmOrderDetails;
    }

    public void setvSmOrderDetails(VSmOrderDetails vSmOrderDetails) {
        this.vSmOrderDetails = vSmOrderDetails;
    }

}
