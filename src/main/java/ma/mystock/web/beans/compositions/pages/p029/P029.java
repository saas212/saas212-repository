package ma.mystock.web.beans.compositions.pages.p029;

import java.util.List;
import ma.mystock.core.dao.entities.CltModule;
import ma.mystock.module.pageManagement.beans.AbstractPage;

/**
 *
 * @author Abdou
 */
public class P029 extends AbstractPage {

    private List<CltModule> cltModuleList;

    @Override
    public void onInit() {

        cltModuleList = genericServices.findCltModuleByClientId(cltclientId);

    }

    public List<CltModule> getCltModuleList() {
        return cltModuleList;
    }

    public void setCltModuleList(List<CltModule> cltModuleList) {
        this.cltModuleList = cltModuleList;
    }

}
