package ma.mystock.web.beans.compositions.pages.p008;

import java.util.List;
import java.util.Map;
import javax.faces.event.AjaxBehaviorEvent;
import ma.mystock.core.dao.entities.views.VSmProductAlert;
import ma.mystock.module.pageManagement.beans.AbstractPage;

import java.io.Serializable;

/**
 *
 * @author abdou
 */
public class P008 extends AbstractPage implements Serializable {

    //@
    // ############# Section 1 #############
    private String reference;

    private String designation;
    private String designationHidden;

    // ############# Section 2 #############
    private List<VSmProductAlert> vSmProductAlertList;

    @Override
    public void onInit() {
        vSmProductAlertList = genericServices.findVSmProductAlertByCltModuleId(cltModuleId);

    }

    public void search(AjaxBehaviorEvent e) {

        vSmProductAlertList = genericServices.findVSmProductAlertByLikeReferenceOrDesignationAndCltModuleId(reference, designation, cltModuleId);
    }

    public List<VSmProductAlert> getvSmProductAlertList() {
        return vSmProductAlertList;
    }

    public void setvSmProductAlertList(List<VSmProductAlert> vSmProductAlertList) {
        this.vSmProductAlertList = vSmProductAlertList;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDesignationHidden() {
        return designationHidden;
    }

    public void setDesignationHidden(String designationHidden) {
        this.designationHidden = designationHidden;
    }

    public Map<String, String> getParamPdf() {
        return paramPdf;
    }

    public void setParamPdf(Map<String, String> paramPdf) {
        this.paramPdf = paramPdf;
    }

}
