package ma.mystock.web.beans.compositions.pages.p033;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import javax.persistence.ParameterMode;
import ma.mystock.core.dao.StoredProcedureParameter;
import ma.mystock.core.dao.entities.SmOrderSupplier;
import ma.mystock.core.dao.entities.SmOrderSupplierLine;
import ma.mystock.core.dao.entities.views.VSmOrderSupplier;
import ma.mystock.core.dao.entities.views.VSmOrderSupplierLine;
import ma.mystock.core.dao.entities.views.VSmOrderSupplierSummary;
import ma.mystock.core.dao.entities.views.VSmProduct;
import ma.mystock.web.utils.MyGenerator;
import ma.mystock.web.utils.datatype.MyDouble;
import ma.mystock.web.utils.datatype.MyLong;
import ma.mystock.web.utils.datatype.MyString;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.web.utils.jsf.FacesUtils;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import ma.mystock.web.utils.functionUtils.DateUtils;
import static ma.mystock.web.utils.springBeans.GloablsServices.smOrderSupplierEJB;
import static ma.mystock.web.utils.springBeans.GloablsServices.smOrderSupplierLineEJB;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmOrderSupplierEJB;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmOrderSupplierLineEJB;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmOrderSupplierSummaryEJB;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmProductEJB;
import ma.mystock.web.utils.globals.GlobalsAttributs;
import ma.mystock.web.utils.lovs.LovsUtils;
import ma.mystock.web.utils.pdf.PdfGlobals;
import ma.mystock.web.utils.pdf.PdfUtils;
import ma.mystock.web.utils.sessions.SessionsGetter;

/**
 *
 * @author Abdessamad HALLAL
 * @desc : Commandes fournisseur (add, edit et delete)
 *
 */
public class P033 extends AbstractPage {

    private List<VSmOrderSupplier> vSmOrderSupplierList;
    private List<VSmOrderSupplierLine> vSmOrderSupplierLineList;

    private List<SelectItem> supplierList;

    private VSmProduct vSmProduct;
    private SmOrderSupplierLine smOrderSupplierLine;
    private VSmOrderSupplierLine vSmOrderSupplierLine;
    private SmOrderSupplier smOrderSupplier;
    private VSmOrderSupplierSummary vSmOrderSupplierSummary;

    private Long smOrderSupplierLineId;
    private Long orderSupplierId;
    private Long vSmProductId;
    private Long lastOrderSupplierId;

    private String reference;
    private String supplier;
    private String note;

    private String pReference;
    private String pReferenceHidden;
    private String pDesignation;
    private String pDesignationHidden;
    private String pUnitPrice;
    private String pQuantity;
    private String pTotalHt;

    private String summaryTotalQuantity;
    private String summaryTotalHt;
    private String summaryTvaAmount;
    private String summaryTotalTtc;

    @Override
    public void onInit() {

        initList();
        initovs();
        initAttribute();
        log.info("onInit P032");
        setShowForm(false);

    }

    public void initList() {
        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        paramQuery.put("orderSupplierStatusId", GlobalsAttributs.ORDER_SUPPLIER_STATUS_ID_SUBMIT);
        vSmOrderSupplierList = vSmOrderSupplierEJB.executeQuery(VSmOrderSupplier.findByCltModuleIdAndOrderSupplierStatusId, paramQuery);
    }

    public void initSubList() {

        paramQuery.clear();
        paramQuery.put("orderSupplierId", orderSupplierId);
        vSmOrderSupplierLineList = vSmOrderSupplierLineEJB.executeQuery(VSmOrderSupplierLine.findByOrderSupplierId, paramQuery);
    }

    public void initSummary() {

        paramQuery.clear();
        paramQuery.put("orderSupplierId", orderSupplierId);
        vSmOrderSupplierSummary = vSmOrderSupplierSummaryEJB.executeSingleQuery(VSmOrderSupplierSummary.findByOrderSupplierId, paramQuery);

        if (MyIs.isNotNull(vSmOrderSupplierSummary)) {

            summaryTotalQuantity = MyLong.toString(vSmOrderSupplierSummary.getSummaryTotalQuantity());
            summaryTotalHt = MyDouble.toString(vSmOrderSupplierSummary.getSummaryTotalHt());
            summaryTvaAmount = MyDouble.toString(vSmOrderSupplierSummary.getSummaryTvaAmount());
            summaryTotalTtc = MyDouble.toString(vSmOrderSupplierSummary.getSummaryTotalTtc());

            if ("".equalsIgnoreCase(summaryTotalQuantity)) {
                summaryTotalQuantity = "0";
            }
            if ("".equalsIgnoreCase(summaryTotalHt)) {
                summaryTotalHt = "0";
            }
            if ("".equalsIgnoreCase(summaryTvaAmount)) {
                summaryTvaAmount = "0";
            }
            if ("".equalsIgnoreCase(summaryTotalTtc)) {
                summaryTotalTtc = "0";
            }
        }

    }

    public void initovs() {
        supplierList = LovsUtils.getSmSupplierItems();
    }

    public void initAttribute() {
        setReference("");
        setSupplier("");
        setNote("");
    }

    public void initPAttribute() {
        setpReference("");
        setpReferenceHidden("");
        setpDesignation("");
        setpDesignationHidden("");
        setpUnitPrice("");
        setpQuantity("");
        setpTotalHt("");
    }

    public void editOrderSupplier(AjaxBehaviorEvent e) {

        orderSupplierId = (Long) e.getComponent().getAttributes().get("orderSupplierId");

        if (orderSupplierId != null) {

            initAttribute();

            paramQuery.clear();
            paramQuery.put("id", orderSupplierId);
            smOrderSupplier = smOrderSupplierEJB.executeSingleQuery(SmOrderSupplier.findById, paramQuery);

            if (MyIs.isNotNull(smOrderSupplier)) {

                reference = smOrderSupplier.getReference();
                supplier = MyLong.toString(smOrderSupplier.getSupplierId());
                note = smOrderSupplier.getNote();

            }
            initSubList();
            initPAttribute();
        }
        initSummary();
        setShowForm(true);
    }

    public void cancelOrderSupplier(AjaxBehaviorEvent e) {
        orderSupplierId = null;
        initList();
        initAttribute();
        setShowForm(false);
    }

    @Override
    public void onSavePartial(AjaxBehaviorEvent e) {
        if (!MyIs.isNotEmpty(reference)) {
            return;
        }

        if (orderSupplierId == null) {
            smOrderSupplier = new SmOrderSupplier();
            smOrderSupplier.setCltModuleId(cltModuleId);
            smOrderSupplier.setOrderSupplierStatusId(GlobalsAttributs.ORDER_SUPPLIER_STATUS_ID_SUBMIT);
            smOrderSupplier.setUserCreation(SessionsGetter.getCltUserId());
            smOrderSupplier.setDateCreation(DateUtils.getCurrentDate());
        } else {
            paramQuery.clear();
            paramQuery.put("id", orderSupplierId);
            smOrderSupplier = smOrderSupplierEJB.executeSingleQuery(SmOrderSupplier.findById, paramQuery);
            smOrderSupplier.setUserUpdate(SessionsGetter.getCltUserId());
            smOrderSupplier.setDateUpdate(DateUtils.getCurrentDate());

        }

        smOrderSupplier.setReference(reference);
        smOrderSupplier.setSupplierId(MyLong.toLong(supplier));
        smOrderSupplier.setNote(note);

        smOrderSupplier = smOrderSupplierEJB.executeMerge(smOrderSupplier);

        orderSupplierId = smOrderSupplier.getId();

    }

    @Override
    public void onDeletePartial(AjaxBehaviorEvent e) {

        orderSupplierId = (Long) e.getComponent().getAttributes().get("orderSupplierId");

        if (orderSupplierId != null) {
            paramQuery.clear();
            paramQuery.put("id", orderSupplierId);
            smOrderSupplier = smOrderSupplierEJB.executeSingleQuery(SmOrderSupplier.findById, paramQuery);

            if (MyIs.isNotNull(smOrderSupplier)) {
                smOrderSupplierEJB.executeDelete(smOrderSupplier);
            }
        }
        initList();
    }

    public void editProduct(AjaxBehaviorEvent e) {

        smOrderSupplierLineId = (Long) e.getComponent().getAttributes().get("smOrderSupplierLineId");

        if (smOrderSupplierLineId != null) {

            paramQuery.clear();
            paramQuery.put("id", smOrderSupplierLineId);
            vSmOrderSupplierLine = vSmOrderSupplierLineEJB.executeSingleQuery(VSmOrderSupplierLine.findById, paramQuery);

            if (vSmOrderSupplierLine != null) {

                pReference = vSmOrderSupplierLine.getReference();
                pReferenceHidden = MyLong.toString(vSmOrderSupplierLine.getProductId());
                pDesignation = vSmOrderSupplierLine.getDesignation();
                pDesignationHidden = vSmOrderSupplierLine.getProductId().toString();
                pUnitPrice = MyDouble.toString(vSmOrderSupplierLine.getUnitPriceSale());
                pQuantity = MyLong.toString(vSmOrderSupplierLine.getQuantity());
                pTotalHt = MyString.toString(vSmOrderSupplierLine.getUnitPriceSale() * vSmOrderSupplierLine.getQuantity());
                vSmProductId = vSmOrderSupplierLine.getProductId();
            }
            initSummary();
        }
    }

    public void deleteProduct(AjaxBehaviorEvent e) {

        smOrderSupplierLineId = (Long) e.getComponent().getAttributes().get("smOrderSupplierLineId");
        if (smOrderSupplierLineId != null) {

            paramQuery.clear();
            paramQuery.put("id", smOrderSupplierLineId);
            smOrderSupplierLine = smOrderSupplierLineEJB.executeSingleQuery(SmOrderSupplierLine.findById, paramQuery);
            if (smOrderSupplierLine != null) {

                smOrderSupplierLineEJB.executeDelete(smOrderSupplierLine);
            }
            smOrderSupplierLineId = null;
            initSubList();
            initSummary();

        }
    }

    public void findProduct(AjaxBehaviorEvent e) {

        if (!"".equalsIgnoreCase(pDesignationHidden)) {
            vSmProductId = Long.valueOf(pDesignationHidden);
        } else if (!"".equalsIgnoreCase(pReferenceHidden)) {
            vSmProductId = Long.valueOf(pReferenceHidden);
        }
        if (MyIs.isNull(vSmProductId)) {
            return;
        }
        paramQuery.clear();
        paramQuery.put("id", vSmProductId);
        vSmProduct = vSmProductEJB.executeSingleQuery(VSmProduct.findById, paramQuery);

        showFieldProduct();

    }

    public void showFieldProduct() {
        if (vSmProduct != null) {
            pReference = vSmProduct.getReference();
            pReferenceHidden = vSmProduct.getId().toString();
            pDesignation = vSmProduct.getDesignation();
            pDesignationHidden = vSmProduct.getId().toString();
            pUnitPrice = MyDouble.toString(vSmProduct.getPriceSale());
            pQuantity = "1";
            pTotalHt = MyString.toString(vSmProduct.getPriceSale() * Long.valueOf(pQuantity));
        }
    }

    public void updateTotalTtc(AjaxBehaviorEvent e) {
        
        Long qte = MyString.stringToLong(pQuantity);

        if (qte == null) {
            qte = 1L;
        }
        
        pTotalHt = MyString.toString((MyDouble.toDouble(pUnitPrice) * qte));
    }

    public void saveProduct(AjaxBehaviorEvent e) {

        if (!MyIs.isNotEmpty(pReference, pDesignation, pUnitPrice, pQuantity)) {
            return;
        }

        onSavePartial(e);

        if (smOrderSupplierLineId != null) {
            paramQuery.clear();
            paramQuery.put("id", smOrderSupplierLineId);
            smOrderSupplierLine = smOrderSupplierLineEJB.executeSingleQuery(SmOrderSupplierLine.findById, paramQuery);
            smOrderSupplierLine.setUserUpdate(SessionsGetter.getCltUserId());
            smOrderSupplierLine.setDateUpdate(DateUtils.getCurrentDate());

        } else {

            smOrderSupplierLine = new SmOrderSupplierLine();
            smOrderSupplierLine.setOrderSupplierId(orderSupplierId);
            smOrderSupplierLine.setUserCreation(SessionsGetter.getCltUserId());
            smOrderSupplierLine.setDateCreation(DateUtils.getCurrentDate());
        }

        smOrderSupplierLine.setProductId(vSmProductId);
        smOrderSupplierLine.setDesignation(pDesignation);
        smOrderSupplierLine.setUnitPriceSale(MyDouble.toDouble(pUnitPrice));
        smOrderSupplierLine.setQuantity(MyLong.toLong(pQuantity));

        smOrderSupplierLine = smOrderSupplierLineEJB.executeMerge(smOrderSupplierLine);
        smOrderSupplierLineId = null;

        initSubList();
        initPAttribute();
        initSummary();
    }

    public void cleanProduct(AjaxBehaviorEvent e) {
        initPAttribute();
    }

    public void validateOrderSupplier(AjaxBehaviorEvent e) {

        onSavePartial(e);

        Map<String, StoredProcedureParameter> map = new HashMap<>();
        map.put("RESULT_STATUS", new StoredProcedureParameter("RESULT_STATUS", Integer.class, ParameterMode.OUT, null));
        map.put("ORDER_SUPPLIER_ID", new StoredProcedureParameter("ORDER_SUPPLIER_ID", Integer.class, ParameterMode.IN, Integer.valueOf(orderSupplierId.toString())));

        int val = vSmProductEJB.executeStoredProcedureQueryGeneric("SP_SM_VALIDATE_ORDER_SUPPLIER", map);
        System.out.println(" val : " + val);

        orderSupplierId = null;
        setShowForm(false);
        initList();

    }

    public void onChangePReference(AjaxBehaviorEvent e) {
        if ("".equalsIgnoreCase(pReference)) {
            initPAttribute();
        } else {
            paramQuery.clear();
            paramQuery.put("reference", MyString.trim(pReference));
            paramQuery.put("cltModuleId", cltModuleId);
            vSmProduct = vSmProductEJB.executeSingleQuery(VSmProduct.findByReferenceAnCltModuleId, paramQuery);
            showFieldProduct();
        }
    }

    public void onChangePDesignation(AjaxBehaviorEvent e) {
        if ("".equalsIgnoreCase(pDesignation)) {
            initPAttribute();
        } else {
            paramQuery.clear();
            paramQuery.put("designation", MyString.trim(pDesignation));
            paramQuery.put("cltModuleId", cltModuleId);
            vSmProduct = vSmProductEJB.executeSingleQuery(VSmProduct.findByDesignationAnCltModuleId, paramQuery);
            showFieldProduct();
        }
    }

    public void printAllOrderSupplier(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("fileName", "bon_de_commandes_soumise");
        paramPdf.put("pdfId", PdfGlobals.PRINT_ALL_ORDER_SUPPLIERS_ID.toString());
        paramPdf.put("orderSupplierStatusId", GlobalsAttributs.ORDER_SUPPLIER_STATUS_ID_SUBMIT.toString());
        printUrl = PdfUtils.getUrlDownloadPdf(paramPdf);
        FacesUtils.goTo(printUrl);
    }

    public void previewAllOrderSupplier(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("pdfId", PdfGlobals.PRINT_ALL_ORDER_SUPPLIERS_ID.toString());
        paramPdf.put("orderSupplierStatusId", GlobalsAttributs.ORDER_SUPPLIER_STATUS_ID_SUBMIT.toString());
        previewUrl = PdfUtils.getUrlViewHTML(paramPdf);
        FacesUtils.newTab(previewUrl);
    }

    public void printOrderSupplier(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("fileName", "bon_de_commandes_" + orderSupplierId.toString());
        paramPdf.put("pdfId", PdfGlobals.PRINT_ORDER_SUPPLIER_ID.toString());
        paramPdf.put("orderSupplierId", orderSupplierId.toString());
        printUrl = PdfUtils.getUrlDownloadPdf(paramPdf);
        FacesUtils.goTo(printUrl);
    }

    public void previewOrderSupplier(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("pdfId", PdfGlobals.PRINT_ORDER_SUPPLIER_ID.toString());
        paramPdf.put("orderSupplierId", orderSupplierId.toString());
        previewUrl = PdfUtils.getUrlViewHTML(paramPdf);
        FacesUtils.newTab(previewUrl);
    }

    public List<VSmOrderSupplier> getvSmOrderSupplierList() {
        return vSmOrderSupplierList;
    }

    public void setvSmOrderSupplierList(List<VSmOrderSupplier> vSmOrderSupplierList) {
        this.vSmOrderSupplierList = vSmOrderSupplierList;
    }

    public List<VSmOrderSupplierLine> getvSmOrderSupplierLineList() {
        return vSmOrderSupplierLineList;
    }

    public void setvSmOrderSupplierLineList(List<VSmOrderSupplierLine> vSmOrderSupplierLineList) {
        this.vSmOrderSupplierLineList = vSmOrderSupplierLineList;
    }

    public List<SelectItem> getSupplierList() {
        return supplierList;
    }

    public void setSupplierList(List<SelectItem> supplierList) {
        this.supplierList = supplierList;
    }

    public VSmProduct getvSmProduct() {
        return vSmProduct;
    }

    public void setvSmProduct(VSmProduct vSmProduct) {
        this.vSmProduct = vSmProduct;
    }

    public SmOrderSupplierLine getSmOrderSupplierLine() {
        return smOrderSupplierLine;
    }

    public void setSmOrderSupplierLine(SmOrderSupplierLine smOrderSupplierLine) {
        this.smOrderSupplierLine = smOrderSupplierLine;
    }

    public VSmOrderSupplierLine getvSmOrderSupplierLine() {
        return vSmOrderSupplierLine;
    }

    public void setvSmOrderSupplierLine(VSmOrderSupplierLine vSmOrderSupplierLine) {
        this.vSmOrderSupplierLine = vSmOrderSupplierLine;
    }

    public SmOrderSupplier getSmOrderSupplier() {
        return smOrderSupplier;
    }

    public void setSmOrderSupplier(SmOrderSupplier smOrderSupplier) {
        this.smOrderSupplier = smOrderSupplier;
    }

    public VSmOrderSupplierSummary getvSmOrderSupplierSummary() {
        return vSmOrderSupplierSummary;
    }

    public void setvSmOrderSupplierSummary(VSmOrderSupplierSummary vSmOrderSupplierSummary) {
        this.vSmOrderSupplierSummary = vSmOrderSupplierSummary;
    }

    public Long getSmOrderSupplierLineId() {
        return smOrderSupplierLineId;
    }

    public void setSmOrderSupplierLineId(Long smOrderSupplierLineId) {
        this.smOrderSupplierLineId = smOrderSupplierLineId;
    }

    public Long getOrderSupplierId() {
        return orderSupplierId;
    }

    public void setOrderSupplierId(Long orderSupplierId) {
        this.orderSupplierId = orderSupplierId;
    }

    public Long getvSmProductId() {
        return vSmProductId;
    }

    public void setvSmProductId(Long vSmProductId) {
        this.vSmProductId = vSmProductId;
    }

    public Long getLastOrderSupplierId() {
        return lastOrderSupplierId;
    }

    public void setLastOrderSupplierId(Long lastOrderSupplierId) {
        this.lastOrderSupplierId = lastOrderSupplierId;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getpReference() {
        return pReference;
    }

    public void setpReference(String pReference) {
        this.pReference = pReference;
    }

    public String getpReferenceHidden() {
        return pReferenceHidden;
    }

    public void setpReferenceHidden(String pReferenceHidden) {
        this.pReferenceHidden = pReferenceHidden;
    }

    public String getpDesignation() {
        return pDesignation;
    }

    public void setpDesignation(String pDesignation) {
        this.pDesignation = pDesignation;
    }

    public String getpDesignationHidden() {
        return pDesignationHidden;
    }

    public void setpDesignationHidden(String pDesignationHidden) {
        this.pDesignationHidden = pDesignationHidden;
    }

    public String getpUnitPrice() {
        return pUnitPrice;
    }

    public void setpUnitPrice(String pUnitPrice) {
        this.pUnitPrice = pUnitPrice;
    }

    public String getpQuantity() {
        return pQuantity;
    }

    public void setpQuantity(String pQuantity) {
        this.pQuantity = pQuantity;
    }

    public String getpTotalHt() {
        return pTotalHt;
    }

    public void setpTotalHt(String pTotalHt) {
        this.pTotalHt = pTotalHt;
    }

    public String getSummaryTotalQuantity() {
        return summaryTotalQuantity;
    }

    public void setSummaryTotalQuantity(String summaryTotalQuantity) {
        this.summaryTotalQuantity = summaryTotalQuantity;
    }

    public String getSummaryTotalHt() {
        return summaryTotalHt;
    }

    public void setSummaryTotalHt(String summaryTotalHt) {
        this.summaryTotalHt = summaryTotalHt;
    }

    public String getSummaryTvaAmount() {
        return summaryTvaAmount;
    }

    public void setSummaryTvaAmount(String summaryTvaAmount) {
        this.summaryTvaAmount = summaryTvaAmount;
    }

    public String getSummaryTotalTtc() {
        return summaryTotalTtc;
    }

    public void setSummaryTotalTtc(String summaryTotalTtc) {
        this.summaryTotalTtc = summaryTotalTtc;
    }

}
