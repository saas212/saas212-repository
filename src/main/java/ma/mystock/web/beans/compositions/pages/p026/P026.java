/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.beans.compositions.pages.p026;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import ma.mystock.core.dao.entities.CltModule;
import ma.mystock.core.dao.entities.CltModuleParameterClient;
import ma.mystock.core.dao.entities.views.VCltModule;
import ma.mystock.core.dao.entities.views.VCltModuleParameter;
import ma.mystock.core.dao.entities.views.VCltModuleParameterClient;
import ma.mystock.core.dao.entities.views.VCltUserClient;
import ma.mystock.web.utils.datatype.MyString;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import ma.mystock.module.pageManagement.messages.MessageList;
import ma.mystock.module.pageManagement.annotations.Attribute;
import ma.mystock.web.utils.globals.GlobalsAttributs;
import ma.mystock.web.utils.sessions.SessionsGetter;
import static ma.mystock.web.utils.sessions.SessionsGetter.getCltClientId;

/**
 *
 * @author Abdou
 */
public class P026 extends AbstractPage {

    @Attribute(itemCode = "moduleParameterClient.s1")
    private List<VCltUserClient> cltUserClientList;
    @Attribute(itemCode = "moduleParameterClient.s2")
    private List<VCltModule> cltModule;
    @Attribute(itemCode = "moduleParameterClient.s3")
    private List<VCltModuleParameterClient> cltModuleParameterClientList;

    @Attribute(itemCode = "moduleParameterClient.s4.module")
    private String moduleId;

    @Attribute(itemCode = "moduleParameterClient.s4.parameterModule")
    private String parameterModuleId;

    @Attribute(itemCode = "moduleParameterClient.s4.value")
    private String value;

    //private List<SelectItem> clientList;
    private List<SelectItem> moduleParameterList;

    private VCltModuleParameterClient vCltModuleParameterClient;
    private CltModuleParameterClient cltModuleParameterClient;

    private Long cltModuleParameterClientId;

    private Long cltClientIdP;

    private boolean rendredClientList;
    private boolean rendredModuleList;
    private boolean rendredModuleParameterClientList;

    private boolean rendredClient;
    private boolean rendredModule;
    private boolean rendredAddModuleParameter;

    @Override
    public void onInit() {
        rendredClient = true;
        rendredAddModuleParameter = true;
        initListCltModuleParameterClient();

    }

    public void initListCltModuleParameterClient() {

        paramQuery.clear();
        paramQuery.put("active", GlobalsAttributs.Y);
        log.info("getCltClientId() : " + getCltClientId());
        log.info("getCltModuleTypeId() : " + getCltModuleTypeId());
        log.info("getCltUserId() : " + getCltUserId());
        Map<String, Object> paramModule = new HashMap<String, Object>();
        paramModule.put("id", getCltModuleId());
        CltModule cltModuleT = cltModuleEJB.executeSingleQuery(CltModule.findById, paramModule);
        setCltModuleTypeId(cltModuleT.getModuleTypeId());
        if (getCltModuleTypeId() != null && 1L == getCltModuleTypeId()) {
            rendredClientList = false;
            rendredModuleList = false;
            rendredModuleParameterClientList = true;
            showForm = false;
            rendredClient = false;
            rendredModule = false;
        } else if (getCltModuleTypeId() != null && 2L == getCltModuleTypeId()) {
            rendredClientList = false;
            rendredModuleList = true;
            rendredModuleParameterClientList = false;
            showForm = false;
            rendredClient = false;
            rendredModule = true;
        } else {
            rendredClientList = true;
            rendredModuleList = false;
            rendredModuleParameterClientList = false;
            showForm = false;
            rendredClient = true;
            rendredModule = true;
        }
        paramQuery.put("moduleId", getCltModuleId());
        cltModuleParameterClientList = vCltModuleParameterClientEJB.executeQuery(VCltModuleParameterClient.findByModuleId, paramQuery);

        Map<String, Object> param = new HashMap<String, Object>();
        param.put("active", GlobalsAttributs.Y);
        param.put("userId", getCltUserId());
        cltUserClientList = vCltUserClientEJB.executeQuery(VCltUserClient.findByUserIdAndActive, param);
        Map<String, Object> paramM = new HashMap<String, Object>();
        paramM.put("clientId", getCltClientId());
        cltModule = vCltModuleEJB.executeQuery(VCltModule.findByClientId, paramM);
        getCltModuleParameterItem(getCltModuleId(), null);
    }

    public void getCltModuleParameterItem(Long moduleId, Long id) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("active", GlobalsAttributs.Y);
        param.put("moduleId", moduleId);
        param.put("id", id);
        List<VCltModuleParameter> vCltModuleParameterList = vCltModuleParameterEJB.executeQuery(VCltModuleParameter.findAllActiveAndId, param);
        if (vCltModuleParameterList == null || vCltModuleParameterList.size() == 0) {
            rendredAddModuleParameter = false;
        } else {
            rendredAddModuleParameter = true;
        }
        List<SelectItem> supplierTypeList = new ArrayList<>();

        for (VCltModuleParameter o : vCltModuleParameterList) {
            supplierTypeList.add(new SelectItem(o.getId(), o.getName()));
        }

        moduleParameterList = supplierTypeList;
    }

    public void initAttribut() {
        setModuleId("");
        setParameterModuleId("");
        setValue("");
    }

    public void edit(AjaxBehaviorEvent e) {
        log.info("cltModuleParameterClientId = " + e.getComponent().getAttributes().get("cltModuleParameterClientId"));
        cltModuleParameterClientId = (Long) e.getComponent().getAttributes().get("cltModuleParameterClientId");

        paramQuery.clear();
        paramQuery.put("id", cltModuleParameterClientId);
        vCltModuleParameterClient = vCltModuleParameterClientEJB.executeSingleQuery(VCltModuleParameterClient.findById, paramQuery);

        if (MyIs.isNull(vCltModuleParameterClient)) {
            return;
        }
        //MIG
        //getCltModuleParameterItem(vCltModuleParameterClient.getModuleId(), vCltModuleParameterClient.getCltModuleParameterId());
        //setModuleId(String.valueOf(vCltModuleParameterClient.getModuleId()));
        //setParameterModuleId(String.valueOf(vCltModuleParameterClient.getCltModuleParameterId()));
        setValue(vCltModuleParameterClient.getValue());
        rendredClientList = false;
        rendredModuleList = false;
        rendredModuleParameterClientList = false;
        showForm = true;
        operation = GlobalsAttributs.OPERATION_EDIT;

        log.info("Opération : " + operation + " - id : " + cltModuleParameterClientId);
    }

    @Override
    public void onDeletePartial(AjaxBehaviorEvent e) {

        log.info("delete 1 : " + cltModuleParameterClientId);

        cltModuleParameterClientId = (Long) e.getComponent().getAttributes().get("cltModuleParameterClientId");

        paramQuery.clear();
        paramQuery.put("id", cltModuleParameterClientId);
        cltModuleParameterClient = cltModuleParameterClientEJB.executeSingleQuery(CltModuleParameterClient.findById, paramQuery);

        if (MyIs.isNotNull(cltModuleParameterClient)) {
            cltModuleParameterClientEJB.executeDelete(cltModuleParameterClient);
        }

        initListCltModuleParameterClient();

        rendredClientList = false;
        rendredModuleList = false;
        rendredModuleParameterClientList = true;
        showForm = false;

    }

    public void goToListModule(AjaxBehaviorEvent event) {
        rendredClientList = false;
        rendredModuleList = true;
        rendredModuleParameterClientList = false;
        showForm = false;
        //rendredClient = true;
        //rendredModule = false;

        cltClientIdP = (Long) event.getComponent().getAttributes().get("cltClientIdParam");
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("clientId", cltClientIdP);
        cltModule = vCltModuleEJB.executeQuery(VCltModule.findByClientId, param);
        log.info("show form true goToListModule ");
    }

    public void goToListModuleParameter(AjaxBehaviorEvent event) {
        rendredClientList = false;
        rendredModuleList = false;
        rendredModuleParameterClientList = true;
        showForm = false;
        // rendredClient = false;
        //rendredModule = true;
        cltModuleId = (Long) event.getComponent().getAttributes().get("cltModuleIdParam");
        getCltModuleParameterItem(cltModuleId, null);
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("moduleId", cltModuleId);
        param.put("active", GlobalsAttributs.Y);
        cltModuleParameterClientList = vCltModuleParameterClientEJB.executeQuery(VCltModuleParameterClient.findByModuleId, param);
        log.info("show form true goToListModuleParameter ");
    }

    public void addModuleParameterClient(AjaxBehaviorEvent event) {
        rendredClientList = false;
        rendredModuleList = false;
        rendredModuleParameterClientList = false;
        showForm = true;
        //rendredClient = false;

        cltModuleParameterClientId = null;
        getCltModuleParameterItem(getCltModuleId(), null);
        operation = GlobalsAttributs.OPERATION_ADD;
        log.info("show form true addParameter ");
    }

    public void cancelModuleParameterClient(AjaxBehaviorEvent event) {
        rendredClientList = false;
        rendredModuleList = false;
        rendredModuleParameterClientList = true;
        showForm = false;
        //rendredClient = false;
        if (!MyString.isEmpty(getModuleId())) {
            getCltModuleParameterItem(Long.parseLong(getModuleId()), null);
        } else {
            getCltModuleParameterItem(getCltModuleId(), null);
        }

        log.info("show form false");
        initAttribut();
    }

    public void cancelClient(AjaxBehaviorEvent event) {
        rendredClientList = true;
        rendredModuleList = false;
        rendredModuleParameterClientList = false;
        showForm = false;
        //rendredClient = false;

        log.info("show form false");
        initAttribut();
    }

    public void cancelModule(AjaxBehaviorEvent event) {
        rendredClientList = false;
        rendredModuleList = true;
        rendredModuleParameterClientList = false;
        showForm = false;
        //rendredClient = true;

        log.info("show form false");
        initAttribut();
    }

    @Override
    public void onSavePartial(AjaxBehaviorEvent e) {

        if (!messages.isEmpty()) {
            return;
        }

        if (GlobalsAttributs.OPERATION_ADD.equals(operation)) {

            cltModuleParameterClient = new CltModuleParameterClient();
            cltModuleParameterClient.setUserCreation(Long.valueOf(SessionsGetter.getCltUserId()));
            cltModuleParameterClient.setActive(GlobalsAttributs.Y);

        } else if (GlobalsAttributs.OPERATION_EDIT.equals(operation)) {

            paramQuery.clear();
            paramQuery.put("id", cltModuleParameterClientId);
            cltModuleParameterClient = cltModuleParameterClientEJB.executeSingleQuery(CltModuleParameterClient.findById, paramQuery);
            cltModuleParameterClient.setUserUpdate(Long.valueOf(SessionsGetter.getCltUserId()));
        }
        if (!MyString.isEmpty(getModuleId())) {
            cltModuleParameterClient.setModuleId(Long.parseLong(getModuleId()));
        } else {
            cltModuleParameterClient.setModuleId(getCltModuleId());
        }

        if (!MyString.isEmpty(parameterModuleId)) {
            cltModuleParameterClient.setModuleParameterId(Long.parseLong(parameterModuleId));
        } else {
            cltModuleParameterClient.setModuleParameterId(null);
        }

        cltModuleParameterClient.setValue(value);

        cltModuleParameterClient = cltModuleParameterClientEJB.executeMerge(cltModuleParameterClient);

        initAttribut();
        initListCltModuleParameterClient();

        rendredClientList = false;
        rendredModuleList = false;
        rendredModuleParameterClientList = true;
        showForm = false;
        //rendredClient = false;

    }

    public void validateParameterClient(AjaxBehaviorEvent e) {

        if (!messages.isEmpty()) {
            return;
        }

        if (cltModuleParameterClientId != null) {
            paramQuery.clear();
            paramQuery.put("id", cltModuleParameterClientId);
            cltModuleParameterClient = cltModuleParameterClientEJB.executeSingleQuery(CltModuleParameterClient.findById, paramQuery);
            cltModuleParameterClient.setUserUpdate(Long.valueOf(SessionsGetter.getCltUserId()));
            cltModuleParameterClient = cltModuleParameterClientEJB.executeMerge(cltModuleParameterClient);

        }

        setShowForm(false);
        initAttribut();
        initListCltModuleParameterClient();

    }

    public List<VCltUserClient> getCltUserClientList() {
        return cltUserClientList;
    }

    public void setCltUserClientList(List<VCltUserClient> cltUserClientList) {
        this.cltUserClientList = cltUserClientList;
    }

    public List<VCltModule> getCltModule() {
        return cltModule;
    }

    public void setCltModule(List<VCltModule> cltModule) {
        this.cltModule = cltModule;
    }

    public List<VCltModuleParameterClient> getCltModuleParameterClientList() {
        return cltModuleParameterClientList;
    }

    public void setCltModuleParameterClientList(List<VCltModuleParameterClient> cltModuleParameterClientList) {
        this.cltModuleParameterClientList = cltModuleParameterClientList;
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    public String getParameterModuleId() {
        return parameterModuleId;
    }

    public void setParameterModuleId(String parameterModuleId) {
        this.parameterModuleId = parameterModuleId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<SelectItem> getModuleParameterList() {
        return moduleParameterList;
    }

    public void setModuleParameterList(List<SelectItem> moduleParameterList) {
        this.moduleParameterList = moduleParameterList;
    }

    public VCltModuleParameterClient getvCltModuleParameterClient() {
        return vCltModuleParameterClient;
    }

    public void setvCltModuleParameterClient(VCltModuleParameterClient vCltModuleParameterClient) {
        this.vCltModuleParameterClient = vCltModuleParameterClient;
    }

    public CltModuleParameterClient getCltModuleParameterClient() {
        return cltModuleParameterClient;
    }

    public void setCltModuleParameterClient(CltModuleParameterClient cltModuleParameterClient) {
        this.cltModuleParameterClient = cltModuleParameterClient;
    }

    public Long getCltModuleParameterClientId() {
        return cltModuleParameterClientId;
    }

    public void setCltModuleParameterClientId(Long cltModuleParameterClientId) {
        this.cltModuleParameterClientId = cltModuleParameterClientId;
    }

    public Long getCltClientIdP() {
        return cltClientIdP;
    }

    public void setCltClientIdP(Long cltClientIdP) {
        this.cltClientIdP = cltClientIdP;
    }

    public boolean isRendredClientList() {
        return rendredClientList;
    }

    public void setRendredClientList(boolean rendredClientList) {
        this.rendredClientList = rendredClientList;
    }

    public boolean isRendredModuleList() {
        return rendredModuleList;
    }

    public void setRendredModuleList(boolean rendredModuleList) {
        this.rendredModuleList = rendredModuleList;
    }

    public boolean isRendredModuleParameterClientList() {
        return rendredModuleParameterClientList;
    }

    public void setRendredModuleParameterClientList(boolean rendredModuleParameterClientList) {
        this.rendredModuleParameterClientList = rendredModuleParameterClientList;
    }

    public boolean isRendredClient() {
        return rendredClient;
    }

    public void setRendredClient(boolean rendredClient) {
        this.rendredClient = rendredClient;
    }

    public boolean isRendredAddModuleParameter() {
        return rendredAddModuleParameter;
    }

    public void setRendredAddModuleParameter(boolean rendredAddModuleParameter) {
        this.rendredAddModuleParameter = rendredAddModuleParameter;
    }

    public boolean isRendredModule() {
        return rendredModule;
    }

    public void setRendredModule(boolean rendredModule) {
        this.rendredModule = rendredModule;
    }

}
