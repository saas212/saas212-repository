package ma.mystock.web.beans.compositions.pages.p038;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import javax.persistence.ParameterMode;
import ma.mystock.core.dao.StoredProcedureParameter;
import ma.mystock.core.dao.StoredProcedures;
import ma.mystock.core.dao.entities.SmOrder;
import ma.mystock.core.dao.entities.SmOrderLine;
import ma.mystock.core.dao.entities.views.VSmCustomer;
import ma.mystock.core.dao.entities.views.VSmOrder;
import ma.mystock.core.dao.entities.views.VSmOrderLine;
import ma.mystock.core.dao.entities.views.VSmOrderSummary;
import ma.mystock.core.dao.entities.views.VSmProduct;
import ma.mystock.core.dao.entities.views.VSmReception;
import ma.mystock.web.utils.MyGenerator;
import ma.mystock.web.utils.datatype.MyDouble;
import ma.mystock.web.utils.datatype.MyLong;
import ma.mystock.web.utils.datatype.MyString;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.web.utils.jsf.FacesUtils;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import ma.mystock.web.utils.functionUtils.DateUtils;
import ma.mystock.web.utils.globals.GlobalsAttributs;
import ma.mystock.web.utils.lovs.LovsUtils;
import ma.mystock.web.utils.pdf.PdfGlobals;
import ma.mystock.web.utils.pdf.PdfUtils;
import ma.mystock.web.utils.sessions.SessionsGetter;
import static ma.mystock.web.utils.springBeans.GloablsServices.genericServices;

/**
 *
 * @author Abdessamad HALLAL
 * @desc : vente client (add, edit et delete)
 *
 */
public class P038 extends AbstractPage {

    private List<VSmOrder> vSmOrderList;
    private List<VSmOrderLine> vSmOrderLineList;

    private List<SelectItem> customerItems;
    private List<VSmCustomer> customerList;

    private VSmProduct vSmProduct;
    private SmOrderLine smOrderLine;
    private VSmOrderLine vSmOrderLine;
    private SmOrder smOrder;
    private VSmOrderSummary vSmOrderSummary;

    private Long smOrderLineId;
    private Long orderId;
    private Long vSmProductId;
    private Long lastOrderId;

    private String reference;
    private String customer;
    private String note;

    private String pReference;
    private String pReferenceHidden;
    private String pDesignation;
    private String pDesignationHidden;
    private String pUnitPrice;
    private String pQuantity;
    private String pTotalHt;

    private String summaryTotalQuantity;
    private String summaryTotalHt;
    private String summaryTvaAmount;
    private String summaryTotalTtc;

    @Override
    public void onInit() {

        initList();
        initovs();
        initAttribute();
        log.info("onInit P038");
        setShowForm(false);

    }

    public void initList() {
        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        paramQuery.put("orderStatusId", GlobalsAttributs.ORDER_STATUS_ID_IN_PROGRESS);
        vSmOrderList = vSmOrderEJB.executeQuery(VSmOrder.findByCltModuleIdAndOrderStatusId, paramQuery);
    }

    public void initSubList() {

        paramQuery.clear();
        paramQuery.put("orderId", orderId);
        vSmOrderLineList = vSmOrderLineEJB.executeQuery(VSmOrderLine.findByOrderId, paramQuery);

    }

    public void initSummary() {

        paramQuery.clear();
        paramQuery.put("orderId", orderId);
        vSmOrderSummary = vSmOrderSummaryEJB.executeSingleQuery(VSmOrderSummary.findByOrderId, paramQuery);

        if (MyIs.isNotNull(vSmOrderSummary)) {

            summaryTotalQuantity = MyLong.toString(vSmOrderSummary.getSummaryTotalQuantity());
            summaryTotalHt = MyDouble.toString(vSmOrderSummary.getSummaryTotalHt());
            summaryTvaAmount = MyDouble.toString(vSmOrderSummary.getSummaryTvaAmount());
            summaryTotalTtc = MyDouble.toString(vSmOrderSummary.getSummaryTotalTtc());

            if ("".equalsIgnoreCase(summaryTotalQuantity)) {
                summaryTotalQuantity = "0";
            }
            if ("".equalsIgnoreCase(summaryTotalHt)) {
                summaryTotalHt = "0";
            }
            if ("".equalsIgnoreCase(summaryTvaAmount)) {
                summaryTvaAmount = "0";
            }
            if ("".equalsIgnoreCase(summaryTotalTtc)) {
                summaryTotalTtc = "0";
            }
        }

    }

    public void initovs() {

        customerItems = LovsUtils.getSmCustomerItems();

        /*
         paramQuery.clear();
         paramQuery.put("cltModuleId", cltModuleId);
         paramQuery.put("active", GlobalsAttributs.Y);
         customerList = vSmCustomerEJB.executeQuery(VSmCustomer.findByCltModuleIdAndActive, paramQuery);

         customerItems = new ArrayList<>();
         for (VSmCustomer item : customerList) {
         customerItems.add(new SelectItem(item.getId(), item.getCompanyName()));
         }
        
         */
    }

    public void initAttribute() {
        setReference("");
        setCustomer("");
        setNote("");
    }

    public void initPAttribute() {
        setpReference("");
        setpReferenceHidden("");
        setpDesignation("");
        setpDesignationHidden("");
        setpUnitPrice("");
        setpQuantity("");
        setpTotalHt("");
    }

    public void addOrder(AjaxBehaviorEvent e) {
        initAttribute();
        initPAttribute();
        vSmOrderLineList = new ArrayList<>();
        genSeq(e);
        setShowForm(true);
        orderId = null;
    }

    public void genSeq(AjaxBehaviorEvent e) {

        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        Long maxNoSeq = vSmOrderEJB.executeLongQuery(VSmOrder.findMaxNoSeqByCltModuleId, paramQuery);

        if (maxNoSeq == null) {
            maxNoSeq = 0L;
        }
        setReference("V" + (maxNoSeq + 1));
    }

    public void genRand(AjaxBehaviorEvent e) {

        String rand = MyGenerator.generate(MyGenerator.SIZE_MEDIUM, MyGenerator.TYPE_ALPHANUMERIC);
        setReference(rand.toUpperCase());

    }

    public void editOrder(AjaxBehaviorEvent e) {

        orderId = (Long) e.getComponent().getAttributes().get("orderId");

        if (orderId != null) {

            initAttribute();

            paramQuery.clear();
            paramQuery.put("id", orderId);
            smOrder = smOrderEJB.executeSingleQuery(SmOrder.findById, paramQuery);

            if (MyIs.isNotNull(smOrder)) {

                reference = smOrder.getReference();
                customer = MyLong.toString(smOrder.getCustomerId());
                note = smOrder.getNote();

            }
            initSubList();
            initPAttribute();
        }
        initSummary();
        setShowForm(true);
    }

    public void cancelOrder(AjaxBehaviorEvent e) {
        orderId = null;
        initList();
        initAttribute();
        setShowForm(false);
    }

    public void submitOrder(AjaxBehaviorEvent e) {
        onSavePartial(e);
        smOrder.setOrderStatusId(GlobalsAttributs.ORDER_STATUS_ID_SUBMIT);
        smOrder = smOrderEJB.executeMerge(smOrder);
        orderId = null;
        initList();
        setShowForm(false);

    }

    @Override
    public void onValidateBeforeSavePartial(AjaxBehaviorEvent e) {

        if (orderId == null) {

            List<VSmOrder> vSmOrder1 = genericServices.findVSmOrderByReferenceAnCltModuleId(reference, cltModuleId);
            if (vSmOrder1 != null && !vSmOrder1.isEmpty()) {
                addError("item", "Reference déja existe");
            }
        }
    }

    @Override
    public void onSavePartial(AjaxBehaviorEvent e) {

        if (!messages.getErrors().isEmpty()) {
            return;
        }

        if (!MyIs.isNotEmpty(reference)) {
            return;
        }

        if (orderId == null) {
            smOrder = new SmOrder();
            smOrder.setCltModuleId(cltModuleId);
            smOrder.setOrderStatusId(GlobalsAttributs.ORDER_STATUS_ID_IN_PROGRESS);

            smOrder.setUserCreation(SessionsGetter.getCltUserId());
            smOrder.setDateCreation(DateUtils.getCurrentDate());
        } else {
            paramQuery.clear();
            paramQuery.put("id", orderId);
            smOrder = smOrderEJB.executeSingleQuery(SmOrder.findById, paramQuery);
            smOrder.setUserUpdate(SessionsGetter.getCltUserId());
            smOrder.setDateUpdate(DateUtils.getCurrentDate());
        }

        smOrder.setReference(reference);
        smOrder.setCustomerId(MyLong.toLong(customer));
        smOrder.setNote(note);

        smOrder = smOrderEJB.executeMerge(smOrder);

        orderId = smOrder.getId();

    }

    @Override
    public void onDeletePartial(AjaxBehaviorEvent e) {

        orderId = (Long) e.getComponent().getAttributes().get("orderId");

        if (orderId != null) {
            paramQuery.clear();
            paramQuery.put("id", orderId);
            smOrder = smOrderEJB.executeSingleQuery(SmOrder.findById, paramQuery);

            if (MyIs.isNotNull(smOrder)) {
                smOrderEJB.executeDelete(smOrder);
            }
        }
        initList();
    }

    public void editProduct(AjaxBehaviorEvent e) {

        smOrderLineId = (Long) e.getComponent().getAttributes().get("smOrderLineId");

        if (smOrderLineId != null) {

            paramQuery.clear();
            paramQuery.put("id", smOrderLineId);
            vSmOrderLine = vSmOrderLineEJB.executeSingleQuery(VSmOrderLine.findById, paramQuery);

            if (vSmOrderLine != null) {

                pReference = vSmOrderLine.getReference();
                pReferenceHidden = MyLong.toString(vSmOrderLine.getProductId());
                pDesignation = vSmOrderLine.getDesignation();
                pDesignationHidden = vSmOrderLine.getProductId().toString();
                pUnitPrice = MyDouble.toString(vSmOrderLine.getNegotiatePriceSale());
                pQuantity = MyLong.toString(vSmOrderLine.getQuantity());
                pTotalHt = MyString.toString(vSmOrderLine.getNegotiatePriceSale() * vSmOrderLine.getQuantity());
                vSmProductId = vSmOrderLine.getProductId();
            }
            initSummary();
        }
    }

    public void deleteProduct(AjaxBehaviorEvent e) {

        smOrderLineId = (Long) e.getComponent().getAttributes().get("smOrderLineId");
        if (smOrderLineId != null) {

            paramQuery.clear();
            paramQuery.put("id", smOrderLineId);
            smOrderLine = smOrderLineEJB.executeSingleQuery(SmOrderLine.findById, paramQuery);
            if (smOrderLine != null) {

                smOrderLineEJB.executeDelete(smOrderLine);
            }
            smOrderLineId = null;
            initSubList();
            initSummary();

        }
    }

    public void findProduct(AjaxBehaviorEvent e) {

        System.out.println(" X : pDesignationHidden : " + pDesignationHidden + " - " + pReferenceHidden);

        if (!"".equalsIgnoreCase(pDesignationHidden)) {
            vSmProductId = Long.valueOf(pDesignationHidden);
        } else if (!"".equalsIgnoreCase(pReferenceHidden)) {
            vSmProductId = Long.valueOf(pReferenceHidden);
        }
        if (MyIs.isNull(vSmProductId)) {
            return;
        }
        paramQuery.clear();
        paramQuery.put("id", vSmProductId);
        vSmProduct = vSmProductEJB.executeSingleQuery(VSmProduct.findById, paramQuery);

        showFieldProduct();

    }

    public void showFieldProduct() {
        if (vSmProduct != null) {
            pReference = vSmProduct.getReference();
            pReferenceHidden = vSmProduct.getId().toString();
            pDesignation = vSmProduct.getDesignation();
            pDesignationHidden = vSmProduct.getId().toString();
            pUnitPrice = MyDouble.toString(vSmProduct.getPriceSale());
            pQuantity = "1";
            pTotalHt = MyString.toString(vSmProduct.getPriceSale() * Long.valueOf(pQuantity));
        }
    }

    public void updateTotalTtc(AjaxBehaviorEvent e) {

        Long qte = MyString.stringToLong(pQuantity);

        if (qte == null) {
            qte = 1L;
        }

        pTotalHt = MyString.toString((MyDouble.toDouble(pUnitPrice) * qte));
    }

    public void saveProduct(AjaxBehaviorEvent e) {
        System.out.println(" 1 vSmProductId : " + vSmProductId);
        if (!MyIs.isNotEmpty(pReference, pDesignation, pUnitPrice, pQuantity)) {
            return;
        }
        System.out.println(" 2 vSmProductId : " + vSmProductId);

        onSavePartial(e);

        System.out.println(" 3 vSmProductId : " + vSmProductId);

        if (smOrderLineId != null) {
            paramQuery.clear();
            paramQuery.put("id", smOrderLineId);
            smOrderLine = smOrderLineEJB.executeSingleQuery(SmOrderLine.findById, paramQuery);
            smOrderLine.setUserUpdate(SessionsGetter.getCltUserId());
            smOrderLine.setDateUpdate(DateUtils.getCurrentDate());

        } else {
            smOrderLine = new SmOrderLine();
            smOrderLine.setOrderId(orderId);
            smOrderLine.setUserCreation(SessionsGetter.getCltUserId());
            smOrderLine.setDateCreation(DateUtils.getCurrentDate());

        }
        System.out.println(" 4 vSmProductId : " + vSmProductId);
        smOrderLine.setProductId(vSmProductId);
        smOrderLine.setDesignation(pDesignation);
        smOrderLine.setNegotiatePriceSale(MyDouble.toDouble(pUnitPrice));
        smOrderLine.setQuantity(MyLong.toLong(pQuantity));

        smOrderLine = smOrderLineEJB.executeMerge(smOrderLine);
        smOrderLineId = null;
        System.out.println(" 5 vSmProductId : " + vSmProductId);
        initSubList();
        initPAttribute();
        initSummary();
    }

    public void cleanProduct(AjaxBehaviorEvent e) {
        initPAttribute();
    }

    public void onChangePReference(AjaxBehaviorEvent e) {
        if ("".equalsIgnoreCase(pReference)) {
            initPAttribute();
        } else {
            paramQuery.clear();
            paramQuery.put("reference", MyString.trim(pReference));
            paramQuery.put("cltModuleId", cltModuleId);
            vSmProduct = vSmProductEJB.executeSingleQuery(VSmProduct.findByReferenceAnCltModuleId, paramQuery);
            showFieldProduct();
        }
    }

    public void onChangePDesignation(AjaxBehaviorEvent e) {
        if ("".equalsIgnoreCase(pDesignation)) {
            initPAttribute();
        } else {
            paramQuery.clear();
            paramQuery.put("designation", MyString.trim(pDesignation));
            paramQuery.put("cltModuleId", cltModuleId);
            vSmProduct = vSmProductEJB.executeSingleQuery(VSmProduct.findByDesignationAnCltModuleId, paramQuery);
            showFieldProduct();
        }
    }

    public void printAllOrder(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("fileName", "ventes_en_cours");
        paramPdf.put("pdfId", PdfGlobals.PRINT_ALL_ORDERS_ID.toString());
        paramPdf.put("orderStatusId", GlobalsAttributs.ORDER_STATUS_ID_IN_PROGRESS.toString());
        printUrl = PdfUtils.getUrlDownloadPdf(paramPdf);
        FacesUtils.goTo(printUrl);
    }

    public void previewAllOrder(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("pdfId", PdfGlobals.PRINT_ALL_ORDERS_ID.toString());
        paramPdf.put("orderStatusId", GlobalsAttributs.ORDER_STATUS_ID_IN_PROGRESS.toString());
        previewUrl = PdfUtils.getUrlViewHTML(paramPdf);
        FacesUtils.newTab(previewUrl);
    }

    public void printOrder(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("fileName", "vente_" + orderId.toString());
        paramPdf.put("pdfId", PdfGlobals.PRINT_ORDER_ID.toString());
        paramPdf.put("orderId", orderId.toString());
        printUrl = PdfUtils.getUrlDownloadPdf(paramPdf);
        FacesUtils.goTo(printUrl);
    }

    public void previewOrder(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("pdfId", PdfGlobals.PRINT_ORDER_ID.toString());
        paramPdf.put("orderId", orderId.toString());
        previewUrl = PdfUtils.getUrlViewHTML(paramPdf);
        FacesUtils.newTab(previewUrl);
    }

    public List<VSmOrder> getvSmOrderList() {
        return vSmOrderList;
    }

    public void setvSmOrderList(List<VSmOrder> vSmOrderList) {
        this.vSmOrderList = vSmOrderList;
    }

    public List<VSmOrderLine> getvSmOrderLineList() {
        return vSmOrderLineList;
    }

    public void setvSmOrderLineList(List<VSmOrderLine> vSmOrderLineList) {
        this.vSmOrderLineList = vSmOrderLineList;
    }

    public List<SelectItem> getCustomerItems() {
        return customerItems;
    }

    public void setCustomerItems(List<SelectItem> customerItems) {
        this.customerItems = customerItems;
    }

    public List<VSmCustomer> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<VSmCustomer> customerList) {
        this.customerList = customerList;
    }

    public VSmProduct getvSmProduct() {
        return vSmProduct;
    }

    public void setvSmProduct(VSmProduct vSmProduct) {
        this.vSmProduct = vSmProduct;
    }

    public SmOrderLine getSmOrderLine() {
        return smOrderLine;
    }

    public void setSmOrderLine(SmOrderLine smOrderLine) {
        this.smOrderLine = smOrderLine;
    }

    public VSmOrderLine getvSmOrderLine() {
        return vSmOrderLine;
    }

    public void setvSmOrderLine(VSmOrderLine vSmOrderLine) {
        this.vSmOrderLine = vSmOrderLine;
    }

    public SmOrder getSmOrder() {
        return smOrder;
    }

    public void setSmOrder(SmOrder smOrder) {
        this.smOrder = smOrder;
    }

    public VSmOrderSummary getvSmOrderSummary() {
        return vSmOrderSummary;
    }

    public void setvSmOrderSummary(VSmOrderSummary vSmOrderSummary) {
        this.vSmOrderSummary = vSmOrderSummary;
    }

    public Long getSmOrderLineId() {
        return smOrderLineId;
    }

    public void setSmOrderLineId(Long smOrderLineId) {
        this.smOrderLineId = smOrderLineId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getvSmProductId() {
        return vSmProductId;
    }

    public void setvSmProductId(Long vSmProductId) {
        this.vSmProductId = vSmProductId;
    }

    public Long getLastOrderId() {
        return lastOrderId;
    }

    public void setLastOrderId(Long lastOrderId) {
        this.lastOrderId = lastOrderId;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getpReference() {
        return pReference;
    }

    public void setpReference(String pReference) {
        this.pReference = pReference;
    }

    public String getpReferenceHidden() {
        return pReferenceHidden;
    }

    public void setpReferenceHidden(String pReferenceHidden) {
        this.pReferenceHidden = pReferenceHidden;
    }

    public String getpDesignation() {
        return pDesignation;
    }

    public void setpDesignation(String pDesignation) {
        this.pDesignation = pDesignation;
    }

    public String getpDesignationHidden() {
        return pDesignationHidden;
    }

    public void setpDesignationHidden(String pDesignationHidden) {
        this.pDesignationHidden = pDesignationHidden;
    }

    public String getpUnitPrice() {
        return pUnitPrice;
    }

    public void setpUnitPrice(String pUnitPrice) {
        this.pUnitPrice = pUnitPrice;
    }

    public String getpQuantity() {
        return pQuantity;
    }

    public void setpQuantity(String pQuantity) {
        this.pQuantity = pQuantity;
    }

    public String getpTotalHt() {
        return pTotalHt;
    }

    public void setpTotalHt(String pTotalHt) {
        this.pTotalHt = pTotalHt;
    }

    public String getSummaryTotalQuantity() {
        return summaryTotalQuantity;
    }

    public void setSummaryTotalQuantity(String summaryTotalQuantity) {
        this.summaryTotalQuantity = summaryTotalQuantity;
    }

    public String getSummaryTotalHt() {
        return summaryTotalHt;
    }

    public void setSummaryTotalHt(String summaryTotalHt) {
        this.summaryTotalHt = summaryTotalHt;
    }

    public String getSummaryTvaAmount() {
        return summaryTvaAmount;
    }

    public void setSummaryTvaAmount(String summaryTvaAmount) {
        this.summaryTvaAmount = summaryTvaAmount;
    }

    public String getSummaryTotalTtc() {
        return summaryTotalTtc;
    }

    public void setSummaryTotalTtc(String summaryTotalTtc) {
        this.summaryTotalTtc = summaryTotalTtc;
    }

}
