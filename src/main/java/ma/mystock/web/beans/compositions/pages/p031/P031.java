/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.beans.compositions.pages.p031;

import java.util.List;
import ma.mystock.core.dao.entities.views.VPmComposDetails;
import ma.mystock.module.pageManagement.beans.AbstractPage;

public class P031 extends AbstractPage {

    private List<VPmComposDetails> vPmComposDetailsList;

    @Override
    public void onInit() {

        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleIdToManager);
        paramQuery.put("groupId", 3L);
        vPmComposDetailsList = vPmComposDetailsEJB.executeQuery(VPmComposDetails.findByCltModuleIdAndGroupId, paramQuery);

        System.out.println("VPmComposDetailsList " + vPmComposDetailsList.size());
        System.out.println("Init");

    }

    public List<VPmComposDetails> getvPmComposDetailsList() {
        return vPmComposDetailsList;
    }

    public void setvPmComposDetailsList(List<VPmComposDetails> vPmComposDetailsList) {
        this.vPmComposDetailsList = vPmComposDetailsList;
    }

}
