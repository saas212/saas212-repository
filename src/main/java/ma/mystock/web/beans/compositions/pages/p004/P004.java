package ma.mystock.web.beans.compositions.pages.p004;

import java.util.List;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import ma.mystock.core.dao.entities.SmSupplier;
import ma.mystock.core.dao.entities.views.VSmSupplier;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.web.utils.datatype.MyLong;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import ma.mystock.module.pageManagement.annotations.Attribute;
import ma.mystock.web.utils.components.ContactManagementComponent;
import ma.mystock.web.utils.functionUtils.DateUtils;
import ma.mystock.web.utils.globals.GlobalsAttributs;
import ma.mystock.web.utils.lovs.LovsUtils;
import ma.mystock.web.utils.sessions.SessionsGetter;

/**
 *
 * @author abdou
 */
public class P004 extends AbstractPage {

    @Attribute(itemCode = "p004.s2.companyName")
    private String companyName;

    @Attribute(itemCode = "p004.s2.representative")
    private String representative;

    @Attribute(itemCode = "p004.s2.firstName")
    private String firstName;

    @Attribute(itemCode = "p004.s2.lastName")
    private String lastName;

    @Attribute(itemCode = "p004.s2.type")
    private String type;

    @Attribute(itemCode = "p004.s2.active")
    private String active;

    @Attribute(itemCode = "p004.s2.shortLabel")
    private String shortLabel;

    @Attribute(itemCode = "p004.s2.fullLabel")
    private String fullLabel;

    @Attribute(itemCode = "p004.s2.category")
    private String category;

    @Attribute(itemCode = "p004.s2.note")
    private String note;

    // 1 = Société 2 = Particuler
    private String nature;

    private List<SelectItem> supplierTypeItems;
    private List<SelectItem> supplierCategoryItems;

    private List<SelectItem> countryItems;
    private List<SelectItem> cityItems;

    private SmSupplier smSupplier;
    private Long supplierId;

    @Attribute(itemCode = "p004.s1")
    private List<VSmSupplier> supplierList;

    private ContactManagementComponent contactManagementComponent;

    @Override
    public void onInit() {
        setShowForm(false);
        setActive(GlobalsAttributs.Y);
        nature = "1";
        initList();
    }

    // ******************************* Opérations ****************************//
    public void initList() {

        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        supplierList = vSmSupplierEJB.executeQuery(VSmSupplier.findByCltModuleId, paramQuery);

        supplierTypeItems = LovsUtils.getSmSupplierTypeItem();
        countryItems = LovsUtils.getInfCountryItem();
        cityItems = LovsUtils.getInfCityItem();
        supplierCategoryItems = LovsUtils.getSmSupplierCategoryItem();
    }

    public void add(AjaxBehaviorEvent e) {

        setFirstName("");
        setLastName("");
        setActive("");
        setCompanyName("");
        setShortLabel("");
        setFullLabel("");
        setType("");
        setCategory("");
        setNote("");
        setNature("1");
        setShowForm(true);
        setOperation(GlobalsAttributs.OPERATION_ADD);
    }

    public void edit(AjaxBehaviorEvent e) {

        supplierId = (Long) e.getComponent().getAttributes().get("supplierId");

        paramQuery.clear();
        paramQuery.put("id", supplierId);

        smSupplier = smSupplierEJB.executeSingleQuery(SmSupplier.findById, paramQuery);

        if (MyIs.isNull(smSupplier)) {
            return;
        }
        System.out.println(" edite edite : " + smSupplier.getEntityId());
        contactManagementComponent = new ContactManagementComponent();
        contactManagementComponent.init(smSupplier.getEntityId());

        firstName = smSupplier.getFirstName();
        lastName = smSupplier.getLastName();
        companyName = smSupplier.getCompanyName();
        representative = smSupplier.getRepresentative();

        shortLabel = smSupplier.getShortLabel();
        fullLabel = smSupplier.getFullLabel();
        note = smSupplier.getNote();

        type = MyLong.toString(smSupplier.getSupplierTypeId());
        category = MyLong.toString(smSupplier.getSupplierCategoryId());
        nature = MyLong.toString(smSupplier.getSupplierNatureId());
        active = smSupplier.getActive();

        setOperation(GlobalsAttributs.OPERATION_EDIT);
        setShowForm(true);
    }

    @Override
    public void onSavePartial(AjaxBehaviorEvent e) {

        System.out.println("save partial pour " + pageId);

        if (!messages.isEmpty()) {
            return;
        }

        if (GlobalsAttributs.OPERATION_ADD.equals(getOperation())) {

            smSupplier = new SmSupplier();
            smSupplier.setUserCreation(SessionsGetter.getCltUserId());
            smSupplier.setDateCreation(DateUtils.getCurrentDate());
            smSupplier.setCltModuleId(cltModuleId);

        } else if (GlobalsAttributs.OPERATION_EDIT.equals(getOperation())) {

            paramQuery.clear();
            paramQuery.put("id", supplierId);
            smSupplier = smSupplierEJB.executeSingleQuery(SmSupplier.findById, paramQuery);
            smSupplier.setUserUpdate(SessionsGetter.getCltUserId());
            smSupplier.setDateUpdate(DateUtils.getCurrentDate());

        }

        smSupplier.setFirstName(firstName);
        smSupplier.setLastName(lastName);

        smSupplier.setShortLabel(shortLabel);
        smSupplier.setFullLabel(fullLabel);
        smSupplier.setActive(active);
        smSupplier.setCompanyName(companyName);
        smSupplier.setNote(note);
        smSupplier.setRepresentative(representative);
        smSupplier.setSupplierTypeId(MyLong.toLong(type));
        smSupplier.setSupplierCategoryId(MyLong.toLong(category));
        smSupplier.setSupplierNatureId(MyLong.toLong(nature));
        smSupplier = smSupplierEJB.executeMerge(smSupplier);

        initList();

        setShowForm(false);
    }

    @Override
    public void onDeletePartial(AjaxBehaviorEvent e) {
        supplierId = (Long) e.getComponent().getAttributes().get("supplierId");
        System.out.println(" supplierId " + supplierId);
        paramQuery.clear();
        paramQuery.put("id", supplierId);

        smSupplier = smSupplierEJB.executeSingleQuery(SmSupplier.findById, paramQuery);

        smSupplierEJB.executeDelete(smSupplier);

        initList();
    }

    @Override
    public void onCancelPartial(AjaxBehaviorEvent e) {
        showForm = false;

    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public List<VSmSupplier> getSupplierList() {
        return supplierList;
    }

    public void setSupplierList(List<VSmSupplier> supplierList) {
        this.supplierList = supplierList;
    }

    public SmSupplier getSmSupplier() {
        return smSupplier;
    }

    public void setSmSupplier(SmSupplier smSupplier) {
        this.smSupplier = smSupplier;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getShortLabel() {
        return shortLabel;
    }

    public void setShortLabel(String shortLabel) {
        this.shortLabel = shortLabel;
    }

    public String getFullLabel() {
        return fullLabel;
    }

    public void setFullLabel(String fullLabel) {
        this.fullLabel = fullLabel;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<SelectItem> getSupplierTypeItems() {
        return supplierTypeItems;
    }

    public void setSupplierTypeItems(List<SelectItem> supplierTypeItems) {
        this.supplierTypeItems = supplierTypeItems;
    }

    public List<SelectItem> getSupplierCategoryItems() {
        return supplierCategoryItems;
    }

    public void setSupplierCategoryItems(List<SelectItem> supplierCategoryItems) {
        this.supplierCategoryItems = supplierCategoryItems;
    }

    public List<SelectItem> getCountryItems() {
        return countryItems;
    }

    public void setCountryItems(List<SelectItem> countryItems) {
        this.countryItems = countryItems;
    }

    public List<SelectItem> getCityItems() {
        return cityItems;
    }

    public void setCityItems(List<SelectItem> cityItems) {
        this.cityItems = cityItems;
    }

    public ContactManagementComponent getContactManagementComponent() {
        return contactManagementComponent;
    }

    public void setContactManagementComponent(ContactManagementComponent contactManagementComponent) {
        this.contactManagementComponent = contactManagementComponent;
    }

    public String getRepresentative() {
        return representative;
    }

    public void setRepresentative(String representative) {
        this.representative = representative;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

}
