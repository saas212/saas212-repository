/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.beans.compositions.pdfs.pdf1006;

import java.util.List;
import ma.mystock.core.dao.entities.views.VSmOrder;
import ma.mystock.core.dao.entities.views.VSmOrderLine;
import ma.mystock.core.dao.entities.views.VSmOrderSummary;
import ma.mystock.web.utils.datatype.MyDouble;
import ma.mystock.web.utils.datatype.MyLong;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.web.utils.jsf.FacesUtils;
import ma.mystock.module.pageManagement.beans.AbstractPdf;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmOrderEJB;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmOrderLineEJB;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmOrderSummaryEJB;

/**
 *
 * @author Abdessamad HALLAL
 */
public class Pdf1006 extends AbstractPdf {

    // list
    private List<VSmOrderLine> vSmOrderLineList;
    private VSmOrder vSmOrder;
    private VSmOrderSummary vSmOrderSummary;

    // reception
    private String reference;
    private String customer;
    private String status;
    private String note;

    // summary
    private String summaryTotalQuantity;
    private String summaryTotalHt;
    private String summaryTvaAmount;
    private String summaryTotalTtc;

    private Long orderId;

    @Override
    public void onInit() {
        orderId = MyLong.toLong(FacesUtils.getRequestParameter("orderId"));
        System.out.println(" orderId = " + orderId);
        InitList();
        initSubList();
        initSummary();
    }

    public void InitList() {
        paramQuery.clear();
        paramQuery.put("id", orderId);
        vSmOrder = vSmOrderEJB.executeSingleQuery(VSmOrder.findById, paramQuery);

        if (MyIs.isNotNull(vSmOrder)) {

            reference = vSmOrder.getReference();
            customer = vSmOrder.getCustomerCompanyName();
            note = vSmOrder.getNote();
            status = vSmOrder.getOrderStatusName();
        }
    }

    public void initSubList() {

        paramQuery.clear();
        paramQuery.put("orderId", orderId);
        vSmOrderLineList = vSmOrderLineEJB.executeQuery(VSmOrderLine.findByOrderId, paramQuery);
    }

    public void initSummary() {
        if (MyIs.isNotNull(orderId)) {

            paramQuery.clear();
            paramQuery.put("orderId", orderId);
            vSmOrderSummary = vSmOrderSummaryEJB.executeSingleQuery(VSmOrderSummary.findByOrderId, paramQuery);

            if (MyIs.isNotNull(vSmOrderSummary)) {

                summaryTotalQuantity = MyLong.toString(vSmOrderSummary.getSummaryTotalQuantity());
                summaryTotalHt = MyDouble.toString(vSmOrderSummary.getSummaryTotalHt());
                summaryTvaAmount = MyDouble.toString(vSmOrderSummary.getSummaryTvaAmount());
                summaryTotalTtc = MyDouble.toString(vSmOrderSummary.getSummaryTotalTtc());

                if ("".equalsIgnoreCase(summaryTotalQuantity)) {
                    summaryTotalQuantity = "0";
                }
                if ("".equalsIgnoreCase(summaryTotalHt)) {
                    summaryTotalHt = "0";
                }
                if ("".equalsIgnoreCase(summaryTvaAmount)) {
                    summaryTvaAmount = "0";
                }
                if ("".equalsIgnoreCase(summaryTotalTtc)) {
                    summaryTotalTtc = "0";
                }
            }
        }
    }

    public List<VSmOrderLine> getvSmOrderLineList() {
        return vSmOrderLineList;
    }

    public void setvSmOrderLineList(List<VSmOrderLine> vSmOrderLineList) {
        this.vSmOrderLineList = vSmOrderLineList;
    }

    public VSmOrder getvSmOrder() {
        return vSmOrder;
    }

    public void setvSmOrder(VSmOrder vSmOrder) {
        this.vSmOrder = vSmOrder;
    }

    public VSmOrderSummary getvSmOrderSummary() {
        return vSmOrderSummary;
    }

    public void setvSmOrderSummary(VSmOrderSummary vSmOrderSummary) {
        this.vSmOrderSummary = vSmOrderSummary;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getSummaryTotalQuantity() {
        return summaryTotalQuantity;
    }

    public void setSummaryTotalQuantity(String summaryTotalQuantity) {
        this.summaryTotalQuantity = summaryTotalQuantity;
    }

    public String getSummaryTotalHt() {
        return summaryTotalHt;
    }

    public void setSummaryTotalHt(String summaryTotalHt) {
        this.summaryTotalHt = summaryTotalHt;
    }

    public String getSummaryTvaAmount() {
        return summaryTvaAmount;
    }

    public void setSummaryTvaAmount(String summaryTvaAmount) {
        this.summaryTvaAmount = summaryTvaAmount;
    }

    public String getSummaryTotalTtc() {
        return summaryTotalTtc;
    }

    public void setSummaryTotalTtc(String summaryTotalTtc) {
        this.summaryTotalTtc = summaryTotalTtc;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

}
