package ma.mystock.web.beans.compositions.pages.p009;

import java.util.List;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import ma.mystock.core.dao.entities.views.VSmProductNotReceived;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import ma.mystock.web.utils.lovs.LovsUtils;
import ma.mystock.web.utils.sessions.SessionsGetter;

/**
 *
 * @author abdou
 */
public class P009 extends AbstractPage {

    //@
    // ############# Section 1 #############
    private String reference;

    private String designation;
    private String designationHidden;

    private String supplier;
    private String supplierHidden;

    private String orderSupplierReference;

    private Long supplierId;

    private List<SelectItem> supplierItems;

    // ############# Section 2 #############
    private List<VSmProductNotReceived> vSmProductNotReceivedList;

    @Override
    public void onInit() {

        System.out.println(" = > " + SessionsGetter.getCltModuleId());
        supplierItems = LovsUtils.getSmSupplierItems();

        vSmProductNotReceivedList = genericServices.findVSmProductNotReceivedByCltModuleId(cltModuleId);

    }

    public void search(AjaxBehaviorEvent e) {
        System.out.println(" => ... ");

        try {
            supplierId = Long.valueOf(supplier);
        } catch (Exception ex) {
            supplierId = null;
        }

        System.out.println(" => supplierId " + supplierId);

        vSmProductNotReceivedList = genericServices.findVSmProductNotReceivedByLikeReferenceOrSupplierIdOrOrderSupplierReferenceOrDesignationAndCltModuleId(reference, supplierId, orderSupplierReference, designation, cltModuleId);

    }

    public List<VSmProductNotReceived> getvSmProductNotReceivedList() {
        return vSmProductNotReceivedList;
    }

    public void setvSmProductNotReceivedList(List<VSmProductNotReceived> vSmProductNotReceivedList) {
        this.vSmProductNotReceivedList = vSmProductNotReceivedList;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public List<SelectItem> getSupplierItems() {
        return supplierItems;
    }

    public void setSupplierItems(List<SelectItem> supplierItems) {
        this.supplierItems = supplierItems;
    }

    public String getSupplierHidden() {
        return supplierHidden;
    }

    public void setSupplierHidden(String supplierHidden) {
        this.supplierHidden = supplierHidden;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDesignationHidden() {
        return designationHidden;
    }

    public void setDesignationHidden(String designationHidden) {
        this.designationHidden = designationHidden;
    }

    public String getOrderSupplierReference() {
        return orderSupplierReference;
    }

    public void setOrderSupplierReference(String orderSupplierReference) {
        this.orderSupplierReference = orderSupplierReference;
    }

    public Long getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Long supplierId) {
        this.supplierId = supplierId;
    }

}
