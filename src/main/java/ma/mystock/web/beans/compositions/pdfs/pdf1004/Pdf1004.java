/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.beans.compositions.pdfs.pdf1004;

import java.util.List;
import ma.mystock.core.dao.entities.views.VSmReception;
import ma.mystock.core.dao.entities.views.VSmReceptionLine;
import ma.mystock.core.dao.entities.views.VSmReceptionSummary;
import ma.mystock.web.utils.datatype.MyDouble;
import ma.mystock.web.utils.datatype.MyLong;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.web.utils.jsf.FacesUtils;
import ma.mystock.module.pageManagement.beans.AbstractPdf;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmReceptionEJB;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmReceptionLineEJB;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmReceptionSummaryEJB;

/**
 *
 * @author Abdessamad HALLAL
 */
public class Pdf1004 extends AbstractPdf {

    // list
    private List<VSmReceptionLine> vSmReceptionLineList;
    private VSmReception vSmReception;
    private VSmReceptionSummary vSmReceptionSummary;

    // reception
    private String reference;
    private String supplier;
    private String status;
    private String note;

    // summary
    private String summaryTotalQuantity;
    private String summaryTotalHt;
    private String summaryTvaAmount;
    private String summaryTotalTtc;

    private Long receptionId;

    @Override
    public void onInit() {
        receptionId = MyLong.toLong(FacesUtils.getRequestParameter("receptionId"));
        System.out.println(" receptionId = " + receptionId);
        InitList();
        initSubList();
        initSummary();
    }

    public void InitList() {
        paramQuery.clear();
        paramQuery.put("id", receptionId);
        vSmReception = vSmReceptionEJB.executeSingleQuery(VSmReception.findById, paramQuery);

        if (MyIs.isNotNull(vSmReception)) {

            reference = vSmReception.getReference();
            supplier = vSmReception.getSupplierCompanyName();
            note = vSmReception.getNote();
            status = vSmReception.getOrderSupplierStatusName();
        }
    }

    public void initSubList() {

        paramQuery.clear();
        paramQuery.put("receptionId", receptionId);
        vSmReceptionLineList = vSmReceptionLineEJB.executeQuery(VSmReceptionLine.findByReceptionId, paramQuery);
    }

    public void initSummary() {
        if (MyIs.isNotNull(receptionId)) {

            paramQuery.clear();
            paramQuery.put("receptionId", receptionId);
            vSmReceptionSummary = vSmReceptionSummaryEJB.executeSingleQuery(VSmReceptionSummary.findByReceptionId, paramQuery);

            if (MyIs.isNotNull(vSmReceptionSummary)) {

                summaryTotalQuantity = MyLong.toString(vSmReceptionSummary.getSummaryTotalQuantity());
                summaryTotalHt = MyDouble.toString(vSmReceptionSummary.getSummaryTotalHt());
                summaryTvaAmount = MyDouble.toString(vSmReceptionSummary.getSummaryTvaAmount());
                summaryTotalTtc = MyDouble.toString(vSmReceptionSummary.getSummaryTotalTtc());

                if ("".equalsIgnoreCase(summaryTotalQuantity)) {
                    summaryTotalQuantity = "0";
                }
                if ("".equalsIgnoreCase(summaryTotalHt)) {
                    summaryTotalHt = "0";
                }
                if ("".equalsIgnoreCase(summaryTvaAmount)) {
                    summaryTvaAmount = "0";
                }
                if ("".equalsIgnoreCase(summaryTotalTtc)) {
                    summaryTotalTtc = "0";
                }
            }
        }
    }

    public List<VSmReceptionLine> getvSmReceptionLineList() {
        return vSmReceptionLineList;
    }

    public void setvSmReceptionLineList(List<VSmReceptionLine> vSmReceptionLineList) {
        this.vSmReceptionLineList = vSmReceptionLineList;
    }

    public VSmReception getvSmReception() {
        return vSmReception;
    }

    public void setvSmReception(VSmReception vSmReception) {
        this.vSmReception = vSmReception;
    }

    public VSmReceptionSummary getvSmReceptionSummary() {
        return vSmReceptionSummary;
    }

    public void setvSmReceptionSummary(VSmReceptionSummary vSmReceptionSummary) {
        this.vSmReceptionSummary = vSmReceptionSummary;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getSummaryTotalQuantity() {
        return summaryTotalQuantity;
    }

    public void setSummaryTotalQuantity(String summaryTotalQuantity) {
        this.summaryTotalQuantity = summaryTotalQuantity;
    }

    public String getSummaryTotalHt() {
        return summaryTotalHt;
    }

    public void setSummaryTotalHt(String summaryTotalHt) {
        this.summaryTotalHt = summaryTotalHt;
    }

    public String getSummaryTvaAmount() {
        return summaryTvaAmount;
    }

    public void setSummaryTvaAmount(String summaryTvaAmount) {
        this.summaryTvaAmount = summaryTvaAmount;
    }

    public String getSummaryTotalTtc() {
        return summaryTotalTtc;
    }

    public void setSummaryTotalTtc(String summaryTotalTtc) {
        this.summaryTotalTtc = summaryTotalTtc;
    }

    public Long getReceptionId() {
        return receptionId;
    }

    public void setReceptionId(Long receptionId) {
        this.receptionId = receptionId;
    }

}
