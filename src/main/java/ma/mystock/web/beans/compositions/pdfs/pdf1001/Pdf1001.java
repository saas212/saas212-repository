/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.beans.compositions.pdfs.pdf1001;

import java.util.List;
import ma.mystock.core.dao.entities.views.VSmOrderSupplier;
import ma.mystock.web.utils.datatype.MyLong;
import ma.mystock.web.utils.jsf.FacesUtils;
import ma.mystock.module.pageManagement.beans.AbstractPdf;
import static ma.mystock.web.utils.springBeans.GloablsServices.vSmOrderSupplierEJB;
import ma.mystock.web.utils.globals.GlobalsAttributs;

/**
 *
 * @author Abdessamad HALLAL
 */
public class Pdf1001 extends AbstractPdf {

    private List<VSmOrderSupplier> vSmOrderSupplierList;
    private Long orderSupplierStatusId;

    @Override
    public void onInit() {
        InitList();
    }

    public void InitList() {

        orderSupplierStatusId = MyLong.toLong(FacesUtils.getRequestParameter("orderSupplierStatusId"));

        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        paramQuery.put("orderSupplierStatusId", orderSupplierStatusId);
        vSmOrderSupplierList = vSmOrderSupplierEJB.executeQuery(VSmOrderSupplier.findByCltModuleIdAndOrderSupplierStatusId, paramQuery);

    }

    public List<VSmOrderSupplier> getvSmOrderSupplierList() {
        return vSmOrderSupplierList;
    }

    public void setvSmOrderSupplierList(List<VSmOrderSupplier> vSmOrderSupplierList) {
        this.vSmOrderSupplierList = vSmOrderSupplierList;
    }

}
