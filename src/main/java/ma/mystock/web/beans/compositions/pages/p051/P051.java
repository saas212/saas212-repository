package ma.mystock.web.beans.compositions.pages.p051;

import javax.faces.event.AjaxBehaviorEvent;
import ma.mystock.core.dao.entities.CltClient;
import ma.mystock.core.dao.entities.views.VCltClient;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import ma.mystock.web.utils.components.ContactManagementComponent;
import ma.mystock.web.utils.functionUtils.StringUtils;

public class P051 extends AbstractPage {

    private String code;
    private String firstName;
    private String lastName;
    private String companyName;
    private String entityId;

    private VCltClient vCltClient;
    private CltClient cltClient;

    private ContactManagementComponent contactManagementComponent;

    @Override
    public void onInit() {

        System.out.println("  P051 : ");

        System.out.println(" cltclientId : " + cltclientId);

        vCltClient = genericServices.findVCltClientById(cltclientId);

        if (vCltClient != null) {
            code = vCltClient.getCode();
            firstName = vCltClient.getFirstName();
            lastName = vCltClient.getLastName();
            companyName = vCltClient.getCompanyName();
            entityId = StringUtils.langToString(vCltClient.getEntityId());

            contactManagementComponent = new ContactManagementComponent();
            contactManagementComponent.init(vCltClient.getEntityId());

            System.out.println(" vCltClient.getEntityId() : " + vCltClient.getEntityId());

        }

    }

    @Override
    public void onSavePartial(AjaxBehaviorEvent e) {

        if (!messages.isEmpty()) {
            return;
        }

        cltClient = genericServices.findCltClientById(cltclientId);

        if (cltClient != null) {
            cltClient.setFirstName(firstName);
            cltClient.setLastName(lastName);
            cltClient.setCompanyName(companyName);

            cltClient = (CltClient) genericServices.genericPersiste(cltClient);
        }
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public VCltClient getvCltClient() {
        return vCltClient;
    }

    public void setvCltClient(VCltClient vCltClient) {
        this.vCltClient = vCltClient;
    }

    public ContactManagementComponent getContactManagementComponent() {
        return contactManagementComponent;
    }

    public void setContactManagementComponent(ContactManagementComponent contactManagementComponent) {
        this.contactManagementComponent = contactManagementComponent;
    }

}
