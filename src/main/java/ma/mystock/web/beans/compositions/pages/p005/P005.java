package ma.mystock.web.beans.compositions.pages.p005;

import java.util.List;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import ma.mystock.core.dao.entities.SmProduct;
import ma.mystock.core.dao.entities.views.VSmProduct;
import ma.mystock.web.utils.datatype.MyDouble;
import ma.mystock.web.utils.MyGenerator;
import ma.mystock.web.utils.datatype.MyLong;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import ma.mystock.module.pageManagement.annotations.Attribute;
import ma.mystock.module.pageManagement.messages.MessageDetail;
import ma.mystock.web.utils.functionUtils.DateUtils;
import ma.mystock.web.utils.functionUtils.StringUtils;
import ma.mystock.web.utils.globals.GlobalsAttributs;
import ma.mystock.web.utils.lovs.LovsUtils;
import ma.mystock.web.utils.sessions.SessionsGetter;

/**
 *
 * @author Abdessamad HALLAL
 */
public class P005 extends AbstractPage {

    /**
     * Section 1 : Multi Search
     */
    private String searchReference;
    private String searchDesignation;
    private String searchDesignationHidden;
    private String searchFamily;
    private String searchQte;
    private String searchType;
    private String searchDepartment;
    private String searchSize;
    private String searchColor;

    private String countResult;

    /**
     * Section 2 : Research Result
     */
    @Attribute(itemCode = "p005.s2")
    private List<VSmProduct> productList;

    /**
     * Section 3 : Add / Edit
     */
    @Attribute(itemCode = "p005.s3.designation")
    private String designation;

    @Attribute(itemCode = "p005.s3.reference")
    private String reference;

    @Attribute(itemCode = "p005.s3.qte")
    private String qte;

    @Attribute(itemCode = "p005.s3.priceSale")
    private String priceSale;

    @Attribute(itemCode = "p005.s3.priceBuy")
    private String priceBuy;

    @Attribute(itemCode = "p005.s3.active")
    private String active;

    @Attribute(itemCode = "p005.s3.size")
    private String size;

    @Attribute(itemCode = "p005.s3.family")
    private String family;

    @Attribute(itemCode = "p005.s3.color")
    private String color;

    @Attribute(itemCode = "p005.s3.group")
    private String group;

    @Attribute(itemCode = "p005.s3.type")
    private String type;

    @Attribute(itemCode = "p005.s3.note")
    private String note;

    @Attribute(itemCode = "p005.s3.threshold")
    private String threshold;

    @Attribute(itemCode = "p005.s3.depatment")
    private String department;

    @Attribute(itemCode = "p005.s3.unit")
    private String unit;

    private List<SelectItem> productStatusItems;
    private List<SelectItem> productSizeItems;
    private List<SelectItem> productFamilyItems;
    private List<SelectItem> productColorItems;
    private List<SelectItem> productGroupItems;
    private List<SelectItem> productTypeItems;
    private List<SelectItem> productDepartmentItems;
    private List<SelectItem> productUnitItems;

    private Long productId;
    private SmProduct smProduct;

    @Override
    public void onInit() {

        //addError("item1", "value1");
        //addInfo("item2", "value2");
        //addWarning("item1", "value3");
        //addSuccess("item1", "value4");
        //addNote("item1", "value5");
        setShowForm(false);
        setIsEditOperation(false);
        setIsAddOperation(false);
        initList();

        log.info("P005 @ onInit");
    }

    public void initList() {

        productList = genericServices.findVSmProductByActiveAndCltModuleIdOrderByIdDescAndLimit("Y", cltModuleId, 20);

        countResult = String.valueOf(productList.size());

        productStatusItems = LovsUtils.getSmProductStatusItem();
        productSizeItems = LovsUtils.getSmProductSizeItem();
        productFamilyItems = LovsUtils.getSmProductFamilyItem();
        productColorItems = LovsUtils.getSmProductColorItem();
        productGroupItems = LovsUtils.getSmProductGroupItem();
        productTypeItems = LovsUtils.getSmProductTypeItem();
        productDepartmentItems = LovsUtils.getSmProductDepartmentItems();
        productUnitItems = LovsUtils.getSmProductUnitItems();
    }

    public void search(AjaxBehaviorEvent e) {

        productList = genericServices.findVSmProductByLikeReferenceOrLikeDesignationOrFamilyIdOrTypeIdOrDepartmentIdOrSizeIdOrColorId(searchReference, searchDesignation, StringUtils.stringToLang(searchFamily), StringUtils.stringToLang(searchType), StringUtils.stringToLang(searchDepartment), StringUtils.stringToLang(searchSize), StringUtils.stringToLang(searchColor), StringUtils.stringToLang(searchQte), cltModuleId);

        countResult = String.valueOf(productList.size());

        log.info("P005 @ search");
    }

    public void onChangeGroup(AjaxBehaviorEvent e) {

        if (MyIs.isNotEmpty(group)) {
            productFamilyItems = LovsUtils.getSmProductFamilyItem(new Long(group));
        }

        log.info("P005 @ onChangeGroup");
    }

    public void genSeq(AjaxBehaviorEvent e) {

        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        Long maxNoSeq = vSmProductEJB.executeLongQuery(VSmProduct.findMaxNoSeqByCltModuleId, paramQuery);

        if (maxNoSeq == null) {
            maxNoSeq = 0L;
        }
        setReference("P" + (maxNoSeq + 1));
    }

    public void genRand(AjaxBehaviorEvent e) {

        String rand = MyGenerator.generate(MyGenerator.SIZE_MEDIUM, MyGenerator.TYPE_ALPHANUMERIC);
        setReference(rand.toUpperCase());

    }

    public void add(AjaxBehaviorEvent e) {

        setReference("");
        setDesignation("");
        setPriceBuy("");
        setPriceSale("");
        setNote("");
        setGroup("");
        setFamily("");
        setSize("");
        setColor("");
        setActive("Y");
        setOperation("add");
        setThreshold("");
        setShowForm(true);
        setIsAddOperation(true);
        setIsEditOperation(false);

        log.info("P005 @ add");

    }

    public void edit(AjaxBehaviorEvent e) {

        setOperation("edit");

        productId = (Long) e.getComponent().getAttributes().get("productId");

        smProduct = genericServices.findSmProductById(productId);

        if (smProduct != null) {

            reference = smProduct.getReference();
            designation = smProduct.getDesignation();
            note = smProduct.getNote();
            priceBuy = MyDouble.toString(smProduct.getPriceBuy());
            priceSale = MyDouble.toString(smProduct.getPriceSale());
            group = MyLong.toString(smProduct.getProductGroupId());
            family = MyLong.toString(smProduct.getProductFamilyId());
            size = MyLong.toString(smProduct.getProductSizeId());
            color = MyLong.toString(smProduct.getProductColorId());
            type = MyLong.toString(smProduct.getProductTypeId());
            threshold = MyLong.toString(smProduct.getThreshold());
            active = smProduct.getActive();
        }

        setShowForm(true);
        setIsEditOperation(true);
        setIsAddOperation(false);

        log.info("P005 @ edit");
    }

    @Override
    public void onValidateBeforeSavePartial(AjaxBehaviorEvent e) {

        if ("add".equals(getOperation())) {

            List<VSmProduct> vSmProductList = genericServices.findVSmProductByReferenceAnCltModuleId(reference, cltModuleId);
            if (vSmProductList != null && !vSmProductList.isEmpty()) {
                addError("item", "Reference déja existe");
            }
        }
    }

    @Override
    public void onSavePartial(AjaxBehaviorEvent e) {

        if (!messages.getErrors().isEmpty()) {
            return;
        }

        System.out.println(" return ;");
        smProduct = null;

        if ("add".equals(getOperation())) {

            smProduct = new SmProduct();
            smProduct.setUserCreation(SessionsGetter.getCltUserId());
            smProduct.setDateCreation(DateUtils.getCurrentDate());
            smProduct.setCltModuleId(cltModuleId);
            smProduct.setQuantity(GlobalsAttributs.DEFAULT_QUANTITYE);

        } else if ("edit".equals(getOperation())) {

            paramQuery.clear();
            paramQuery.put("id", productId);
            smProduct = smProductEJB.executeSingleQuery(SmProduct.findById, paramQuery);
            smProduct.setUserUpdate(SessionsGetter.getCltUserId());
            smProduct.setDateUpdate(DateUtils.getCurrentDate());

        }
        smProduct.setReference(reference);
        smProduct.setDesignation(designation);

        if (MyDouble.toDouble(priceBuy) != null) {
            smProduct.setPriceBuy(MyDouble.toDouble(priceBuy));
        } else {
            smProduct.setPriceBuy(GlobalsAttributs.DEFAULT_PRICE_BUY);
        }
        if (MyDouble.toDouble(priceSale) != null) {
            smProduct.setPriceSale(MyDouble.toDouble(priceSale));
        } else {
            smProduct.setPriceSale(GlobalsAttributs.DEFAULT_PRICE_SALE);
        }

        smProduct.setNote(note);
        smProduct.setProductGroupId(MyLong.toLong(group));
        smProduct.setProductFamilyId(MyLong.toLong(family));
        smProduct.setProductSizeId(MyLong.toLong(size));
        smProduct.setProductColorId(MyLong.toLong(color));
        smProduct.setProductTypeId(MyLong.toLong(type));
        smProduct.setActive(active);
        smProduct.setThreshold(MyLong.toLong(threshold));

        smProduct = smProductEJB.executeMerge(smProduct);
        initList();
        setShowForm(false);
        setIsEditOperation(false);
        setIsAddOperation(false);
        log.info("P005 @ onSavePartial");
    }

    @Override
    public void onDeletePartial(AjaxBehaviorEvent e) {

        productId = (Long) e.getComponent().getAttributes().get("productId");

        System.out.println(" ==> " + productId);
        paramQuery.clear();
        paramQuery.put("id", productId);

        SmProduct product = smProductEJB.executeSingleQuery(SmProduct.findById, paramQuery);

        smProductEJB.executeDelete(product);

        initList();
        log.info("P005 @ onDeletePartial");
    }

    @Override
    public void onCancelPartial(AjaxBehaviorEvent e) {
        setShowForm(false);
        setIsEditOperation(false);
        setIsAddOperation(false);
        log.info("P005 @ onCancelPartial");
    }

    public String getSearchReference() {
        return searchReference;
    }

    public void setSearchReference(String searchReference) {
        this.searchReference = searchReference;
    }

    public String getSearchDesignation() {
        return searchDesignation;
    }

    public void setSearchDesignation(String searchDesignation) {
        this.searchDesignation = searchDesignation;
    }

    public String getSearchDesignationHidden() {
        return searchDesignationHidden;
    }

    public void setSearchDesignationHidden(String searchDesignationHidden) {
        this.searchDesignationHidden = searchDesignationHidden;
    }

    public String getSearchFamily() {
        return searchFamily;
    }

    public void setSearchFamily(String searchFamily) {
        this.searchFamily = searchFamily;
    }

    public String getSearchQte() {
        return searchQte;
    }

    public void setSearchQte(String searchQte) {
        this.searchQte = searchQte;
    }

    public String getSearchType() {
        return searchType;
    }

    public void setSearchType(String searchType) {
        this.searchType = searchType;
    }

    public String getSearchDepartment() {
        return searchDepartment;
    }

    public void setSearchDepartment(String searchDepartment) {
        this.searchDepartment = searchDepartment;
    }

    public String getSearchSize() {
        return searchSize;
    }

    public void setSearchSize(String searchSize) {
        this.searchSize = searchSize;
    }

    public String getSearchColor() {
        return searchColor;
    }

    public void setSearchColor(String searchColor) {
        this.searchColor = searchColor;
    }

    public List<VSmProduct> getProductList() {
        return productList;
    }

    public void setProductList(List<VSmProduct> productList) {
        this.productList = productList;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getQte() {
        return qte;
    }

    public void setQte(String qte) {
        this.qte = qte;
    }

    public String getPriceSale() {
        return priceSale;
    }

    public void setPriceSale(String priceSale) {
        this.priceSale = priceSale;
    }

    public String getPriceBuy() {
        return priceBuy;
    }

    public void setPriceBuy(String priceBuy) {
        this.priceBuy = priceBuy;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getThreshold() {
        return threshold;
    }

    public void setThreshold(String threshold) {
        this.threshold = threshold;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public List<SelectItem> getProductStatusItems() {
        return productStatusItems;
    }

    public void setProductStatusItems(List<SelectItem> productStatusItems) {
        this.productStatusItems = productStatusItems;
    }

    public List<SelectItem> getProductSizeItems() {
        return productSizeItems;
    }

    public void setProductSizeItems(List<SelectItem> productSizeItems) {
        this.productSizeItems = productSizeItems;
    }

    public List<SelectItem> getProductFamilyItems() {
        return productFamilyItems;
    }

    public void setProductFamilyItems(List<SelectItem> productFamilyItems) {
        this.productFamilyItems = productFamilyItems;
    }

    public List<SelectItem> getProductColorItems() {
        return productColorItems;
    }

    public void setProductColorItems(List<SelectItem> productColorItems) {
        this.productColorItems = productColorItems;
    }

    public List<SelectItem> getProductGroupItems() {
        return productGroupItems;
    }

    public void setProductGroupItems(List<SelectItem> productGroupItems) {
        this.productGroupItems = productGroupItems;
    }

    public List<SelectItem> getProductTypeItems() {
        return productTypeItems;
    }

    public void setProductTypeItems(List<SelectItem> productTypeItems) {
        this.productTypeItems = productTypeItems;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public SmProduct getSmProduct() {
        return smProduct;
    }

    public void setSmProduct(SmProduct smProduct) {
        this.smProduct = smProduct;
    }

    public List<SelectItem> getProductDepartmentItems() {
        return productDepartmentItems;
    }

    public void setProductDepartmentItems(List<SelectItem> productDepartmentItems) {
        this.productDepartmentItems = productDepartmentItems;
    }

    public String getCountResult() {
        return countResult;
    }

    public void setCountResult(String countResult) {
        this.countResult = countResult;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public List<SelectItem> getProductUnitItems() {
        return productUnitItems;
    }

    public void setProductUnitItems(List<SelectItem> productUnitItems) {
        this.productUnitItems = productUnitItems;
    }

}
