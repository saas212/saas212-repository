/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.beans.compositions.pages.p023;

import java.util.List;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import ma.mystock.core.dao.entities.CltModuleParameter;
import ma.mystock.core.dao.entities.CltParameter;
import ma.mystock.core.dao.entities.views.VCltModuleParameter;
import ma.mystock.core.dao.entities.views.VCltParameter;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import ma.mystock.module.pageManagement.messages.MessageList;
import ma.mystock.module.pageManagement.annotations.Attribute;
import ma.mystock.web.utils.globals.GlobalsAttributs;
import ma.mystock.web.utils.lovs.LovsUtils;
import ma.mystock.web.utils.sessions.SessionsGetter;

/**
 *
 * @author Abdou
 */
public class P023 extends AbstractPage {

    @Attribute(itemCode = "moduleParameter.s1")
    private List<VCltModuleParameter> cltModuleParameterList;

    @Attribute(itemCode = "moduleParameter.s2.name")
    private String name;

    @Attribute(itemCode = "moduleParameter.s2.description")
    private String description;

    @Attribute(itemCode = "moduleParameter.s2.value")
    private String value;

    @Attribute(itemCode = "moduleParameter.s2.moduleParameterType")
    private String moduleParameterType;
    private List<SelectItem> moduleParameterTypeList;

    private VCltModuleParameter vCltModuleParameter;
    private CltModuleParameter cltModuleParameter;

    private Long cltModuleParameterId;

    @Override
    public void onInit() {

        setShowForm(false);
        initListCltModuleParameter();

    }

    public void initListCltModuleParameter() {

        paramQuery.clear();
        paramQuery.put("active", GlobalsAttributs.Y);

        cltModuleParameterList = vCltModuleParameterEJB.executeQuery(VCltModuleParameter.findAllActive, paramQuery);
        moduleParameterTypeList = LovsUtils.getCltModuleParameterTypeItem();

    }

    public void initAttribut() {
        setName("");
        setDescription("");
        setValue("");
        setModuleParameterType("");
        setOperation("");
    }

    public void edit(AjaxBehaviorEvent e) {
        log.info("cltModuleParameterId = " + e.getComponent().getAttributes().get("cltModuleParameterId"));
        cltModuleParameterId = (Long) e.getComponent().getAttributes().get("cltModuleParameterId");

        paramQuery.clear();
        paramQuery.put("id", cltModuleParameterId);
        vCltModuleParameter = vCltModuleParameterEJB.executeSingleQuery(VCltModuleParameter.findById, paramQuery);

        if (MyIs.isNull(vCltModuleParameter)) {
            return;
        }

        setName(vCltModuleParameter.getName());
        setDescription(vCltModuleParameter.getDescription());
        setValue(vCltModuleParameter.getDefaultValue());
        setModuleParameterType(String.valueOf(vCltModuleParameter.getModuleParameterTypeId()));

        setShowForm(true);
        operation = GlobalsAttributs.OPERATION_EDIT;

        log.info("Opération : " + operation + " - id : " + cltModuleParameterId);
    }

    @Override
    public void onDeletePartial(AjaxBehaviorEvent e) {

        log.info("delete 1 : " + cltModuleParameterId);

        cltModuleParameterId = (Long) e.getComponent().getAttributes().get("cltModuleParameterId");

        paramQuery.clear();
        paramQuery.put("id", cltModuleParameterId);
        cltModuleParameter = cltModuleParameterEJB.executeSingleQuery(CltModuleParameter.findById, paramQuery);

        if (MyIs.isNotNull(cltModuleParameter)) {
            cltModuleParameterEJB.executeDelete(cltModuleParameter);
        }

        initListCltModuleParameter();

    }

    public void addModuleParameter(AjaxBehaviorEvent event) {
        showForm = true;
        cltModuleParameterId = null;
        operation = GlobalsAttributs.OPERATION_ADD;
        log.info("show form true addParameter ");
    }

    public void cancelModuleParameter(AjaxBehaviorEvent event) {
        showForm = false;
        log.info("show form false");
        initAttribut();
    }

    @Override
    public void onSavePartial(AjaxBehaviorEvent e) {

        if (!messages.isEmpty()) {
            return;
        }

        if (GlobalsAttributs.OPERATION_ADD.equals(operation)) {

            cltModuleParameter = new CltModuleParameter();
            cltModuleParameter.setUserCreation(Long.valueOf(SessionsGetter.getCltUserId()));
            cltModuleParameter.setActive(GlobalsAttributs.Y);

        } else if (GlobalsAttributs.OPERATION_EDIT.equals(operation)) {

            paramQuery.clear();
            paramQuery.put("id", cltModuleParameterId);
            cltModuleParameter = cltModuleParameterEJB.executeSingleQuery(CltModuleParameter.findById, paramQuery);
            cltModuleParameter.setUserUpdate(Long.valueOf(SessionsGetter.getCltUserId()));
        }

        cltModuleParameter.setName(name);
        cltModuleParameter.setDescription(description);
        cltModuleParameter.setDefaultValue(value);
        cltModuleParameter.setModuleParameterTypeId(Long.parseLong(moduleParameterType));

        cltModuleParameter = cltModuleParameterEJB.executeMerge(cltModuleParameter);

        setShowForm(false);
        initAttribut();
        initListCltModuleParameter();

    }

    public void validateModuleParameter(AjaxBehaviorEvent e) {

        if (!messages.isEmpty()) {
            return;
        }

        if (cltModuleParameterId != null) {
            paramQuery.clear();
            paramQuery.put("id", cltModuleParameterId);
            cltModuleParameter = cltModuleParameterEJB.executeSingleQuery(CltModuleParameter.findById, paramQuery);
            cltModuleParameter.setUserUpdate(Long.valueOf(SessionsGetter.getCltUserId()));
            cltModuleParameter = cltModuleParameterEJB.executeMerge(cltModuleParameter);

        }

        setShowForm(false);
        initAttribut();
        initListCltModuleParameter();

    }

    public List<VCltModuleParameter> getCltModuleParameterList() {
        return cltModuleParameterList;
    }

    public void setCltModuleParameterList(List<VCltModuleParameter> cltModuleParameterList) {
        this.cltModuleParameterList = cltModuleParameterList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getModuleParameterType() {
        return moduleParameterType;
    }

    public void setModuleParameterType(String moduleParameterType) {
        this.moduleParameterType = moduleParameterType;
    }

    public List<SelectItem> getModuleParameterTypeList() {
        return moduleParameterTypeList;
    }

    public void setModuleParameterTypeList(List<SelectItem> moduleParameterTypeList) {
        this.moduleParameterTypeList = moduleParameterTypeList;
    }

    public VCltModuleParameter getvCltModuleParameter() {
        return vCltModuleParameter;
    }

    public void setvCltModuleParameter(VCltModuleParameter vCltModuleParameter) {
        this.vCltModuleParameter = vCltModuleParameter;
    }

    public CltModuleParameter getCltModuleParameter() {
        return cltModuleParameter;
    }

    public void setCltModuleParameter(CltModuleParameter cltModuleParameter) {
        this.cltModuleParameter = cltModuleParameter;
    }

    public Long getCltModuleParameterId() {
        return cltModuleParameterId;
    }

    public void setCltModuleParameterId(Long cltModuleParameterId) {
        this.cltModuleParameterId = cltModuleParameterId;
    }

}
