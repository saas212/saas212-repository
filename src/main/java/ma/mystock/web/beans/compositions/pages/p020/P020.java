package ma.mystock.web.beans.compositions.pages.p020;

import javax.faces.event.AjaxBehaviorEvent;
import ma.mystock.core.dao.entities.CltUser;
import ma.mystock.core.dao.entities.views.VCltUser;
import ma.mystock.web.utils.password.MyPassword;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import ma.mystock.module.pageManagement.messages.MessageDetail;
import ma.mystock.web.utils.components.ContactManagementComponent;
import ma.mystock.web.utils.functionUtils.DateUtils;
import ma.mystock.web.utils.sessions.SessionsGetter;

/**
 *
 * @author abdou
 *
 */
public class P020 extends AbstractPage {

    // firstName, lastName, email;password, replayRassword,cellPhone,fixedPhone,adress
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private String replayRassword;

    private VCltUser vCltUser;
    private CltUser cltUser;

    private ContactManagementComponent contactManagementComponent;

    @Override
    public void onInit() {

        paramQuery.clear();
        paramQuery.put("id", SessionsGetter.getCltUserId());
        vCltUser = vCltUserEJB.executeSingleQuery(VCltUser.findById, paramQuery);

        if (MyIs.isNotNull(vCltUser)) {
            firstName = vCltUser.getFirstName();
            lastName = vCltUser.getLastName();
            username = vCltUser.getUsername();
            //password = vCltUser.getPassword();
            //replayRassword = vCltUser.getPassword();

            contactManagementComponent = new ContactManagementComponent();
            System.out.println(" =====================> " + vCltUser.getEntityId());
            contactManagementComponent.init(vCltUser.getEntityId());
        }
    }

    @Override
    public void onSavePartial(AjaxBehaviorEvent e) {

        if (!messages.isEmpty()) {
            return;
        }

        paramQuery.clear();
        paramQuery.put("id", SessionsGetter.getCltUserId());
        cltUser = cltUserEJB.executeSingleQuery(CltUser.findById, paramQuery);
        log.info("1");
        if (MyIs.isNotNull(cltUser)) {
            log.info("2");
            cltUser.setFirstName(firstName);
            cltUser.setLastName(lastName);
            cltUser.setUsername(username);
            //cltUser.setPassword(MyPassword.toSHA1(password));
            cltUser.setUserUpdate(SessionsGetter.getCltUserId());
            cltUser.setDateUpdate(DateUtils.getCurrentDate());

            cltUser = cltUserEJB.executeMerge(cltUser);

        }

        log.info("Save");
    }

    @Override
    public void onValidateBeforeSavePartial(AjaxBehaviorEvent e) {

        /*
        messages.add(new MessageDetail(1, "ID_MESSGAE_ERROR", "ID_MESSGAE_ERROR"));
        messages.add(new MessageDetail(2, "ID_MESSGAE_INFO", "ID_MESSGAE_INFO"));
        messages.add(new MessageDetail(1, "ID_MESSGAE_ERROR", "ID_MESSGAE_ERROR"));
        messages.add(new MessageDetail(2, "ID_MESSGAE_INFO", "ID_MESSGAE_INFO"));
        messages.add(new MessageDetail(3, "ID_MESSGAE_WARNING", "ID_MESSGAE_WARNING"));
        messages.add(new MessageDetail(4, "ID_MESSGAE_SUCCESS", "ID_MESSGAE_SUCCESS"));
        messages.add(new MessageDetail(5, "ID_MESSGAE_NOTE", "ID_MESSGAE_NOTE"));
         */
        if (MyIs.isEmpty(username)) {
            messages.add(new MessageDetail(1, "E-mail est obligatoire", "E-mail est obligatoire"));

        }

        /*
        if (MyIs.isNotEmpty(password) && MyIs.isEmpty(replayRassword)) {

            messages.add(new MessageDetail(1, "Re mot de passe  est obligatoire", "Re mot de passe  est obligatoire"));

        } else if (MyIs.isNotEmpty(password) && MyIs.isNotEmpty(replayRassword)) {

            if (!password.equals(replayRassword)) {
                messages.add(new MessageDetail(1, "Les deux mot de passe ne sont pas identique", "Les deux mot de passe ne sont pas identique"));
            }
        }
        
         */
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getReplayRassword() {
        return replayRassword;
    }

    public void setReplayRassword(String replayRassword) {
        this.replayRassword = replayRassword;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public VCltUser getvCltUser() {
        return vCltUser;
    }

    public void setvCltUser(VCltUser vCltUser) {
        this.vCltUser = vCltUser;
    }

    public CltUser getCltUser() {
        return cltUser;
    }

    public void setCltUser(CltUser cltUser) {
        this.cltUser = cltUser;
    }

    public ContactManagementComponent getContactManagementComponent() {
        return contactManagementComponent;
    }

    public void setContactManagementComponent(ContactManagementComponent contactManagementComponent) {
        this.contactManagementComponent = contactManagementComponent;
    }

}
