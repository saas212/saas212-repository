package ma.mystock.web.beans.compositions.pages.p017;

import java.util.ArrayList;
import java.util.List;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import ma.mystock.core.dao.entities.InfCity;
import ma.mystock.core.dao.entities.InfCountry;
import ma.mystock.core.dao.entities.SmProductColor;
import ma.mystock.core.dao.entities.SmSupplierType;
import ma.mystock.core.dao.entities.views.VInfCity;
import ma.mystock.core.dao.entities.views.VInfCountry;
import ma.mystock.core.dao.entities.views.VInfLovs;
import ma.mystock.core.dao.entities.views.VSmProductColor;
import ma.mystock.core.dao.entities.views.VSmSupplierType;
import ma.mystock.core.dao.entities.views.dto.LovsDTO;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import ma.mystock.module.pageManagement.messages.MessageList;
import ma.mystock.web.utils.sessions.SessionsGetter;

/**
 *
 * @author abdou
 *
 */
public class P017 extends AbstractPage {

    private List<LovsDTO> lovsDTOList;

    private List<VInfLovs> vInfLovsList;
    private List<SelectItem> vInfLovsItem;
    private VInfLovs vInfLovs;

    private String lov;

    private String name;
    private String description;
    private String active;
    private Long itemId;
    private String entity;

    @Override
    public void onInit() {
        log.info("Init : Lovs");
        initList();

        setShowForm(false);
    }

    public void initList() {

        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        vInfLovsList = vInfLovsEJB.executeQuery(VInfLovs.findByCltModuleId, paramQuery);

        log.info("vInfLovsList : " + vInfLovsList.size());

        vInfLovsItem = new ArrayList<>();
        for (VInfLovs o : vInfLovsList) {
            vInfLovsItem.add(new SelectItem(o.getEntity(), o.getName()));
        }
    }

    public void showList(AjaxBehaviorEvent e) {
        log.info("  loooooooooooovvvv    :  " + lov);
        lovsDTOList = new ArrayList<>();

        log.info("  loooooooooooovvvv    :  " + lov);

        switch (lov) {

            case "SmSupplierType":
                smSupplierTypeInit();
                log.info("1");
                break;
            case "SmProductColor":
                log.info("2");
                smProductColorInit();
                break;
            case "InfCountry":
                log.info("3");
                infCountryInit();
                break;
            case "InfCity":
                log.info("4");
                infCityInit();
                break;
        }

    }

    @Override
    public void onSavePartial(AjaxBehaviorEvent e) {
        log.info("Save onSavePartial ");

        log.info("itemId :  " + itemId);
        log.info("lovId :  " + lov);

        switch (lov) {
            case "SmSupplierType":
                smSupplierTypeSave(itemId);
                break;
            case "SmProductColor":
                smProductColorSave(itemId);
                break;
            case "InfCountry":
                infCountrySave(itemId);
                break;
            case "InfCity":
                infCitySave(itemId);
                break;
        }

        setShowForm(false);
        showList(e);
    }

    public void delete(AjaxBehaviorEvent e) {

        itemId = (Long) e.getComponent().getAttributes().get("itemId");

    }

    public void edit(AjaxBehaviorEvent e) {

        name = "";
        description = "";
        active = "";

        itemId = (Long) e.getComponent().getAttributes().get("itemId");

        log.info("itemId :  " + itemId);
        log.info("lovId :  " + lov);

        switch (lov) {
            case "SmSupplierType":
                smSupplierTypeEdit(itemId);
                break;
            case "SmProductColor":
                smProductColorEdit(itemId);
                break;
            case "InfCountry":
                infCountryEdit(itemId);
                break;
            case "InfCity":
                infCityEdit(itemId);
                break;
        }

        setShowForm(true);

    }

    public void add(AjaxBehaviorEvent e) {

        name = "";
        description = "";
        active = "";
        itemId = null;

        log.info("itemId :  " + itemId);
        log.info("lovId :  " + lov);

        setShowForm(true);

    }

    public void cancel(AjaxBehaviorEvent e) {

        log.info("cancel");

        setShowForm(false);

    }

    public List<VInfLovs> getvInfLovsList() {
        return vInfLovsList;
    }

    public void setvInfLovsList(List<VInfLovs> vInfLovsList) {
        this.vInfLovsList = vInfLovsList;
    }

    public List<SelectItem> getvInfLovsItem() {
        return vInfLovsItem;
    }

    public void setvInfLovsItem(List<SelectItem> vInfLovsItem) {
        this.vInfLovsItem = vInfLovsItem;
    }

    public String getLov() {
        return lov;
    }

    public void setLov(String lov) {
        this.lov = lov;
    }

    public List<LovsDTO> getLovsDTOList() {
        return lovsDTOList;
    }

    public void setLovsDTOList(List<LovsDTO> lovsDTOList) {
        this.lovsDTOList = lovsDTOList;
    }

    public VInfLovs getvInfLovs() {
        return vInfLovs;
    }

    public void setvInfLovs(VInfLovs vInfLovs) {
        this.vInfLovs = vInfLovs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public void smSupplierTypeInit() {
        paramQuery.clear();
        paramQuery.put("cltModuleId", SessionsGetter.getCltModuleId());
        List<VSmSupplierType> vSmSupplierTypeList = vSmSupplierTypeEJB.executeQuery(VSmSupplierType.findByCltModuleId, paramQuery);

        for (VSmSupplierType o : vSmSupplierTypeList) {
            lovsDTOList.add(new LovsDTO(o.getId(), "SmSupplierType", o.getName(), o.getDescription(), o.getName(), o.getActive(), o.getSortKey(), o.getCltModuleId()));
        }
    }

    public void smSupplierTypeEdit(Long itemId) {
        paramQuery.clear();
        paramQuery.put("id", itemId);
        VSmSupplierType vSmSupplierType = vSmSupplierTypeEJB.executeSingleQuery(VSmSupplierType.findById, paramQuery);

        if (vSmSupplierType != null) {

            name = vSmSupplierType.getName();
            description = vSmSupplierType.getDescription();
            active = vSmSupplierType.getActive();

        }
    }

    public void smSupplierTypeSave(Long itemId) {

        SmSupplierType smSupplierType;

        if (MyIs.isNull(itemId)) {
            //add
            log.info("smSupplierTypeLov @ add");
            smSupplierType = new SmSupplierType();
            smSupplierType.setCltModuleId(cltModuleId);

        } else {
            // edit
            log.info("smSupplierTypeLov @ edit");
            paramQuery.clear();
            paramQuery.put("id", itemId);
            smSupplierType = smSupplierTypeEJB.executeSingleQuery(SmSupplierType.findById, paramQuery);

            if (MyIs.isNull(smSupplierType)) {
                smSupplierType = new SmSupplierType();
            }
        }

        smSupplierType.setName(name);
        smSupplierType.setDescription(description);
        smSupplierType.setActive(active);

        smSupplierTypeEJB.executeMerge(smSupplierType);
    }

    public void smProductColorInit() {
        paramQuery.clear();
        paramQuery.put("cltModuleId", SessionsGetter.getCltModuleId());
        List<VSmProductColor> vSmProductColorList = vSmProductColorEJB.executeQuery(VSmProductColor.findByCltModuleId, paramQuery);

        for (VSmProductColor o : vSmProductColorList) {
            lovsDTOList.add(new LovsDTO(o.getId(), "SmProductColor", o.getName(), "", o.getName(), o.getActive(), o.getSortKey(), o.getCltModuleId()));
        }
    }

    public void smProductColorEdit(Long itemId) {
        paramQuery.clear();
        paramQuery.put("id", itemId);
        VSmProductColor vSmProductColor = vSmProductColorEJB.executeSingleQuery(VSmProductColor.findById, paramQuery);

        if (vSmProductColor != null) {

            name = vSmProductColor.getName();
            description = "";
            active = vSmProductColor.getActive();
            log.info(" getActive getActive getActive getActive : " + vSmProductColor.getActive());
        }
    }

    public void smProductColorSave(Long itemId) {

        SmProductColor smProductColor;

        if (MyIs.isNull(itemId)) {
            //add
            smProductColor = new SmProductColor();
            smProductColor.setCltModuleId(cltModuleId);

        } else {
            // edit
            paramQuery.clear();
            paramQuery.put("id", itemId);
            smProductColor = smProductColorEJB.executeSingleQuery(SmProductColor.findById, paramQuery);

            if (MyIs.isNull(smProductColor)) {
                smProductColor = new SmProductColor();
            }
        }

        smProductColor.setName(name);
        smProductColor.setActive(active);

        smProductColorEJB.executeMerge(smProductColor);

    }

    public void infCountryInit() {
        paramQuery.clear();
        paramQuery.put("cltModuleId", SessionsGetter.getCltModuleId());
        List<VInfCountry> vInfCountryList = null;//vInfCountryEJB.executeQuery(VInfCountry.findByCltModuleId, paramQuery);

        for (VInfCountry o : vInfCountryList) {
            //lovsDTOList.add(new LovsDTO(o.getId(), "InfCountry", o.getName(), "", o.getName(), o.getActive(), o.getSortKey(), o.getCltModuleId()));
        }
    }

    public void infCountryEdit(Long itemId) {
        paramQuery.clear();
        paramQuery.put("id", itemId);
        VInfCountry vInfCountry = vInfCountryEJB.executeSingleQuery(VInfCountry.findById, paramQuery);

        if (vInfCountry != null) {

            name = vInfCountry.getName();
            description = "";
            active = vInfCountry.getActive();
            log.info(" getActive getActive getActive getActive : " + vInfCountry.getActive());
        }
    }

    public void infCountrySave(Long itemId) {

        InfCountry infCountry;

        if (MyIs.isNull(itemId)) {
            //add
            infCountry = new InfCountry();
            //infCountry.setCltModuleId(cltModuleId);

        } else {
            // edit
            paramQuery.clear();
            paramQuery.put("id", itemId);
            infCountry = infCountryEJB.executeSingleQuery(InfCountry.findById, paramQuery);

            if (MyIs.isNull(infCountry)) {
                infCountry = new InfCountry();
            }
        }

        infCountry.setName(name);
        infCountry.setActive(active);

        infCountryEJB.executeMerge(infCountry);

    }

    public void infCityInit() {
        paramQuery.clear();
        paramQuery.put("cltModuleId", SessionsGetter.getCltModuleId());
        List<VInfCity> vInfCityList = null;//vInfCityEJB.executeQuery(VInfCity.findByCltModuleId, paramQuery);

        for (VInfCity o : vInfCityList) {
            //lovsDTOList.add(new LovsDTO(o.getId(), "InfCity", o.getName(), "", o.getName(), o.getActive(), o.getSortKey(), o.getCltModuleId()));
        }
    }

    public void infCityEdit(Long itemId) {
        paramQuery.clear();
        paramQuery.put("id", itemId);
        VInfCity vInfCity = vInfCityEJB.executeSingleQuery(VInfCity.findById, paramQuery);

        if (vInfCity != null) {

            name = vInfCity.getName();
            description = "";
            active = vInfCity.getActive();
            log.info(" getActive getActive getActive getActive : " + vInfCity.getActive());
        }
    }

    public void infCitySave(Long itemId) {

        InfCity infCity;

        if (MyIs.isNull(itemId)) {
            //add
            infCity = new InfCity();
            //infCity.setCltModuleId(cltModuleId);

        } else {
            // edit
            paramQuery.clear();
            paramQuery.put("id", itemId);
            infCity = infCityEJB.executeSingleQuery(InfCity.findById, paramQuery);

            if (MyIs.isNull(infCity)) {
                infCity = new InfCity();
            }
        }

        infCity.setName(name);
        infCity.setActive(active);

        infCityEJB.executeMerge(infCity);

    }
}
