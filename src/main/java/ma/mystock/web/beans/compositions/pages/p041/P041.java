package ma.mystock.web.beans.compositions.pages.p041;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import ma.mystock.core.dao.entities.SmReturnReceipt;
import ma.mystock.core.dao.entities.SmReturnReceiptLine;
import ma.mystock.core.dao.entities.views.VSmOrder;
import ma.mystock.core.dao.entities.views.VSmOrderLine;
import ma.mystock.core.dao.entities.views.VSmReturnReceipt;
import ma.mystock.core.dao.entities.views.VSmReturnReceiptLine;
import ma.mystock.core.dao.entities.views.VSmReturnReceiptSummary;
import ma.mystock.core.dao.entities.views.VSmProduct;
import ma.mystock.web.utils.MyGenerator;
import ma.mystock.web.utils.datatype.MyDouble;
import ma.mystock.web.utils.datatype.MyLong;
import ma.mystock.web.utils.datatype.MyString;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.web.utils.jsf.FacesUtils;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import ma.mystock.web.utils.functionUtils.DateUtils;
import ma.mystock.web.utils.globals.GlobalsAttributs;
import ma.mystock.web.utils.lovs.LovsUtils;
import ma.mystock.web.utils.pdf.HtmlToXHtml;
import ma.mystock.web.utils.pdf.PdfGlobals;
import ma.mystock.web.utils.pdf.PdfUtils;
import ma.mystock.web.utils.sessions.SessionsGetter;

import org.primefaces.model.DefaultStreamedContent;

import org.primefaces.model.StreamedContent;

/**
 *
 * @author Abdessamad HALLAL
 * @desc : Commandes fournisseur (add, edit et delete)
 *
 */
public class P041 extends AbstractPage {

    private List<VSmReturnReceipt> vSmReturnReceiptList;
    private List<VSmReturnReceiptLine> vSmReturnReceiptLineList;
    private List<VSmOrder> vSmOrderList;
    private List<VSmOrderLine> vSmOrderLineList;

    private List<SelectItem> customerItems;
    private List<SelectItem> orderItems;

    private VSmProduct vSmProduct;
    private SmReturnReceiptLine smReturnReceiptLine;
    private VSmReturnReceiptLine vSmReturnReceiptLine;
    private SmReturnReceipt smReturnReceipt;
    private VSmReturnReceiptSummary vSmReturnReceiptSummary;

    private Long smReturnReceiptLineId;
    private Long returnReceiptId;
    private Long vSmProductId;
    private Long lastReturnReceiptId;

    private String reference;
    private String customer;
    private String note;
    private String order;

    private String pReference;
    private String pReferenceHidden;
    private String pDesignation;
    private String pDesignationHidden;
    private String pUnitPrice;
    private String pQuantity;
    private String pTotalHt;

    private String summaryTotalQuantity;
    private String summaryTotalHt;
    private String summaryTvaAmount;
    private String summaryTotalTtc;

    @Override
    public void onInit() {

        initList();
        initovs();
        initAttribute();
        log.info("onInit P041");
        setShowForm(false);

    }

    public void initList() {
        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        paramQuery.put("returnReceiptStatusId", GlobalsAttributs.RETURN_RECEIPT_STATUS_ID_IN_PROGRESS);
        vSmReturnReceiptList = vSmReturnReceiptEJB.executeQuery(VSmReturnReceipt.findByCltModuleIdAndReturnReceiptStatusId, paramQuery);
    }

    public void initSubList() {
        paramQuery.clear();
        paramQuery.put("returnReceiptId", returnReceiptId);
        vSmReturnReceiptLineList = vSmReturnReceiptLineEJB.executeQuery(VSmReturnReceiptLine.findByReturnReceiptId, paramQuery);
    }

    public StreamedContent download() {

        try {

            /*
            
             byte[] blobContent =  HtmlToXHtml.generatePDFContent("http://192.168.1.6:8083/MyStock/demo/index.xhtml").getPdfByte();

             InputStream inputStream = new ByteArrayInputStream(blobContent);

             return new DefaultStreamedContent(inputStream, "application/pdf", "ResearchProjectDescriptionFormPreview.pdf");
             */
            System.out.println(" return null ");
        } catch (Exception e) {

            e.printStackTrace();

        }

        return null;

    }

    public void initSummary() {

        paramQuery.clear();
        paramQuery.put("returnReceiptId", returnReceiptId);
        vSmReturnReceiptSummary = vSmReturnReceiptSummaryEJB.executeSingleQuery(VSmReturnReceiptSummary.findByReturnReceiptId, paramQuery);

        if (MyIs.isNotNull(vSmReturnReceiptSummary)) {

            summaryTotalQuantity = MyLong.toString(vSmReturnReceiptSummary.getSummaryTotalQuantity());
            summaryTotalHt = MyDouble.toString(vSmReturnReceiptSummary.getSummaryTotalHt());
            summaryTvaAmount = MyDouble.toString(vSmReturnReceiptSummary.getSummaryTvaAmount());
            summaryTotalTtc = MyDouble.toString(vSmReturnReceiptSummary.getSummaryTotalTtc());

            if ("".equalsIgnoreCase(summaryTotalQuantity)) {
                summaryTotalQuantity = "0";
            }
            if ("".equalsIgnoreCase(summaryTotalHt)) {
                summaryTotalHt = "0";
            }
            if ("".equalsIgnoreCase(summaryTvaAmount)) {
                summaryTvaAmount = "0";
            }
            if ("".equalsIgnoreCase(summaryTotalTtc)) {
                summaryTotalTtc = "0";
            }
        }

    }

    public void initovs() {
        customerItems = LovsUtils.getSmCustomerItems();

        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        paramQuery.put("orderStatusId", GlobalsAttributs.RETURN_RECEIPT_STATUS_ID_VALID);
        vSmOrderList = vSmOrderEJB.executeQuery(VSmOrder.findByCltModuleIdAndOrderStatusId, paramQuery);

        orderItems = new ArrayList<>();
        for (VSmOrder o : vSmOrderList) {
            orderItems.add(new SelectItem(o.getId(), o.getReference() + " - " + o.getCustomerCompanyName()));
        }
    }

    public void initAttribute() {
        setReference("");
        setCustomer("");
        setNote("");
    }

    public void initPAttribute() {
        setpReference("");
        setpReferenceHidden("");
        setpDesignation("");
        setpDesignationHidden("");
        setpUnitPrice("");
        setpQuantity("");
        setpTotalHt("");
    }

    public void addReturnReceipt(AjaxBehaviorEvent e) {
        initAttribute();
        initPAttribute();
        vSmReturnReceiptLineList = new ArrayList<>();
        genSeq(e);
        setShowForm(true);
        returnReceiptId = null;
    }

    public void genSeq(AjaxBehaviorEvent e) {

        paramQuery.clear();
        paramQuery.put("cltModuleId", cltModuleId);
        Long maxNoSeq = vSmReturnReceiptEJB.executeLongQuery(VSmReturnReceipt.findMaxNoSeqByCltModuleId, paramQuery);
        if (maxNoSeq == null) {
            maxNoSeq = 0L;
        }
        setReference("R" + (maxNoSeq + 1));
    }

    public void genRand(AjaxBehaviorEvent e) {

        String rand = MyGenerator.generate(MyGenerator.SIZE_MEDIUM, MyGenerator.TYPE_ALPHANUMERIC);
        setReference(rand.toUpperCase());

    }

    public void generateProduct(AjaxBehaviorEvent e) {

        onSavePartial(e);

        paramQuery.clear();
        paramQuery.put("orderId", Long.valueOf(order));
        vSmOrderLineList = vSmOrderLineEJB.executeQuery(VSmOrderLine.findByOrderId, paramQuery);

        for (VSmOrderLine o : vSmOrderLineList) {

            smReturnReceiptLine = new SmReturnReceiptLine();
            smReturnReceiptLine.setReturnReceiptId(returnReceiptId);
            smReturnReceiptLine.setProductId(o.getProductId());
            smReturnReceiptLine.setDesignation(o.getDesignation());
            smReturnReceiptLine.setNegotiatePriceSale(o.getNegotiatePriceSale());
            smReturnReceiptLine.setQuantity(o.getQuantity());

            smReturnReceiptLine = smReturnReceiptLineEJB.executeMerge(smReturnReceiptLine);

        }
        smReturnReceiptLineId = null;
        smReturnReceiptLine = null;
        initSubList();
        initPAttribute();
        initSummary();
        System.out.println(" ok generateProduct " + order);

    }

    public void editReturnReceipt(AjaxBehaviorEvent e) {

        returnReceiptId = (Long) e.getComponent().getAttributes().get("returnReceiptId");

        if (returnReceiptId != null) {

            initAttribute();

            paramQuery.clear();
            paramQuery.put("id", returnReceiptId);
            smReturnReceipt = smReturnReceiptEJB.executeSingleQuery(SmReturnReceipt.findById, paramQuery);

            if (MyIs.isNotNull(smReturnReceipt)) {

                reference = smReturnReceipt.getReference();
                customer = MyLong.toString(smReturnReceipt.getCustomerId());
                note = smReturnReceipt.getNote();

            }
            initSubList();
            initPAttribute();
        }
        initSummary();
        setShowForm(true);
    }

    public void cancelReturnReceipt(AjaxBehaviorEvent e) {
        returnReceiptId = null;
        initList();
        initAttribute();
        setShowForm(false);
    }

    public void submitReturnReceipt(AjaxBehaviorEvent e) {
        onSavePartial(e);
        smReturnReceipt.setReturnReceiptStatusId(GlobalsAttributs.RETURN_RECEIPT_STATUS_ID_SUBMIT);
        smReturnReceipt = smReturnReceiptEJB.executeMerge(smReturnReceipt);
        returnReceiptId = null;
        initList();
        setShowForm(false);

    }

    @Override
    public void onSavePartial(AjaxBehaviorEvent e) {
        if (!MyIs.isNotEmpty(reference)) {
            return;
        }

        if (returnReceiptId == null) {
            smReturnReceipt = new SmReturnReceipt();
            smReturnReceipt.setCltModuleId(cltModuleId);
            smReturnReceipt.setReturnReceiptStatusId(GlobalsAttributs.RETURN_RECEIPT_STATUS_ID_IN_PROGRESS);
            smReturnReceipt.setUserCreation(SessionsGetter.getCltUserId());
            smReturnReceipt.setDateCreation(DateUtils.getCurrentDate());

        } else {
            paramQuery.clear();
            paramQuery.put("id", returnReceiptId);
            smReturnReceipt = smReturnReceiptEJB.executeSingleQuery(SmReturnReceipt.findById, paramQuery);
            smReturnReceipt.setUserUpdate(SessionsGetter.getCltUserId());
            smReturnReceipt.setDateUpdate(DateUtils.getCurrentDate());

        }

        smReturnReceipt.setReference(reference);
        smReturnReceipt.setCustomerId(MyLong.toLong(customer));
        smReturnReceipt.setNote(note);

        smReturnReceipt = smReturnReceiptEJB.executeMerge(smReturnReceipt);

        returnReceiptId = smReturnReceipt.getId();

    }

    @Override
    public void onDeletePartial(AjaxBehaviorEvent e) {

        returnReceiptId = (Long) e.getComponent().getAttributes().get("returnReceiptId");

        if (returnReceiptId != null) {
            paramQuery.clear();
            paramQuery.put("id", returnReceiptId);
            smReturnReceipt = smReturnReceiptEJB.executeSingleQuery(SmReturnReceipt.findById, paramQuery);

            if (MyIs.isNotNull(smReturnReceipt)) {
                smReturnReceiptEJB.executeDelete(smReturnReceipt);
            }
        }
        initList();
    }

    public void editProduct(AjaxBehaviorEvent e) {

        smReturnReceiptLineId = (Long) e.getComponent().getAttributes().get("smReturnReceiptLineId");

        if (smReturnReceiptLineId != null) {

            paramQuery.clear();
            paramQuery.put("id", smReturnReceiptLineId);
            vSmReturnReceiptLine = vSmReturnReceiptLineEJB.executeSingleQuery(VSmReturnReceiptLine.findById, paramQuery);

            if (vSmReturnReceiptLine != null) {

                pReference = vSmReturnReceiptLine.getReference();
                pReferenceHidden = MyLong.toString(vSmReturnReceiptLine.getProductId());
                pDesignation = vSmReturnReceiptLine.getDesignation();
                pDesignationHidden = vSmReturnReceiptLine.getProductId().toString();
                pUnitPrice = MyDouble.toString(vSmReturnReceiptLine.getNegotiatePriceSale());
                pQuantity = MyLong.toString(vSmReturnReceiptLine.getQuantity());
                pTotalHt = MyString.toString(vSmReturnReceiptLine.getNegotiatePriceSale() * vSmReturnReceiptLine.getQuantity());
                vSmProductId = vSmReturnReceiptLine.getProductId();
            }
            initSummary();
        }
    }

    public void deleteProduct(AjaxBehaviorEvent e) {

        smReturnReceiptLineId = (Long) e.getComponent().getAttributes().get("smReturnReceiptLineId");
        if (smReturnReceiptLineId != null) {

            paramQuery.clear();
            paramQuery.put("id", smReturnReceiptLineId);
            smReturnReceiptLine = smReturnReceiptLineEJB.executeSingleQuery(SmReturnReceiptLine.findById, paramQuery);
            if (smReturnReceiptLine != null) {

                smReturnReceiptLineEJB.executeDelete(smReturnReceiptLine);
            }
            smReturnReceiptLineId = null;
            initSubList();
            initSummary();

        }
    }

    public void findProduct(AjaxBehaviorEvent e) {

        if (!"".equalsIgnoreCase(pDesignationHidden)) {
            vSmProductId = Long.valueOf(pDesignationHidden);
        } else if (!"".equalsIgnoreCase(pReferenceHidden)) {
            vSmProductId = Long.valueOf(pReferenceHidden);
        }
        if (MyIs.isNull(vSmProductId)) {
            return;
        }
        paramQuery.clear();
        paramQuery.put("id", vSmProductId);
        vSmProduct = vSmProductEJB.executeSingleQuery(VSmProduct.findById, paramQuery);

        showFieldProduct();

    }

    public void showFieldProduct() {
        if (vSmProduct != null) {
            pReference = vSmProduct.getReference();
            pReferenceHidden = vSmProduct.getId().toString();
            pDesignation = vSmProduct.getDesignation();
            pDesignationHidden = vSmProduct.getId().toString();
            pUnitPrice = MyDouble.toString(vSmProduct.getPriceSale());
            pQuantity = "1";
            pTotalHt = MyString.toString(vSmProduct.getPriceSale() * Long.valueOf(pQuantity));
        }
    }

    public void updateTotalTtc(AjaxBehaviorEvent e) {
        
        Long qte = MyString.stringToLong(pQuantity);

        if (qte == null) {
            qte = 1L;
        }
        
        pTotalHt = MyString.toString((MyDouble.toDouble(pUnitPrice) * qte));
    }

    public void saveProduct(AjaxBehaviorEvent e) {

        if (!MyIs.isNotEmpty(pReference, pDesignation, pUnitPrice, pQuantity)) {
            return;
        }

        onSavePartial(e);

        if (smReturnReceiptLineId != null) {
            paramQuery.clear();
            paramQuery.put("id", smReturnReceiptLineId);
            smReturnReceiptLine = smReturnReceiptLineEJB.executeSingleQuery(SmReturnReceiptLine.findById, paramQuery);
            smReturnReceiptLine.setUserUpdate(SessionsGetter.getCltUserId());
            smReturnReceiptLine.setDateUpdate(DateUtils.getCurrentDate());

        } else {
            smReturnReceiptLine = new SmReturnReceiptLine();
            smReturnReceiptLine.setReturnReceiptId(returnReceiptId);
            smReturnReceiptLine.setUserCreation(SessionsGetter.getCltUserId());
            smReturnReceiptLine.setDateCreation(DateUtils.getCurrentDate());

        }

        smReturnReceiptLine.setProductId(vSmProductId);
        smReturnReceiptLine.setDesignation(pDesignation);
        smReturnReceiptLine.setNegotiatePriceSale(MyDouble.toDouble(pUnitPrice));
        smReturnReceiptLine.setQuantity(MyLong.toLong(pQuantity));

        smReturnReceiptLine = smReturnReceiptLineEJB.executeMerge(smReturnReceiptLine);
        smReturnReceiptLineId = null;

        initSubList();
        initPAttribute();
        initSummary();
    }

    public void cleanProduct(AjaxBehaviorEvent e) {
        initPAttribute();
    }

    public void onChangePReference(AjaxBehaviorEvent e) {
        if ("".equalsIgnoreCase(pReference)) {
            initPAttribute();
        } else {
            paramQuery.clear();
            paramQuery.put("reference", MyString.trim(pReference));
            paramQuery.put("cltModuleId", cltModuleId);
            vSmProduct = vSmProductEJB.executeSingleQuery(VSmProduct.findByReferenceAnCltModuleId, paramQuery);
            showFieldProduct();
        }
    }

    public void onChangePDesignation(AjaxBehaviorEvent e) {
        if ("".equalsIgnoreCase(pDesignation)) {
            initPAttribute();
        } else {
            paramQuery.clear();
            paramQuery.put("designation", MyString.trim(pDesignation));
            paramQuery.put("cltModuleId", cltModuleId);
            vSmProduct = vSmProductEJB.executeSingleQuery(VSmProduct.findByDesignationAnCltModuleId, paramQuery);
            showFieldProduct();
        }
    }

    public void printAllReturnReceipt(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("fileName", "bon_de_returnReceipt_en_cours");
        paramPdf.put("pdfId", PdfGlobals.PRINT_ALL_RETURN_RECEIPTS_ID.toString());
        paramPdf.put("returnReceiptStatusId", GlobalsAttributs.RETURN_RECEIPT_STATUS_ID_IN_PROGRESS.toString());
        printUrl = PdfUtils.getUrlDownloadPdf(paramPdf);
        FacesUtils.goTo(printUrl);
    }

    public void previewAllReturnReceipt(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("pdfId", PdfGlobals.PRINT_ALL_RETURN_RECEIPTS_ID.toString());
        paramPdf.put("returnReceiptStatusId", GlobalsAttributs.RETURN_RECEIPT_STATUS_ID_IN_PROGRESS.toString());
        previewUrl = PdfUtils.getUrlViewHTML(paramPdf);
        FacesUtils.newTab(previewUrl);
    }

    public void printReturnReceipt(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("fileName", "bon_de_returnReceipt_" + returnReceiptId.toString());
        paramPdf.put("pdfId", PdfGlobals.PRINT_RETURN_RECEIPT_ID.toString());
        paramPdf.put("returnReceiptId", returnReceiptId.toString());
        printUrl = PdfUtils.getUrlDownloadPdf(paramPdf);
        FacesUtils.goTo(printUrl);
    }

    public void previewReturnReceipt(AjaxBehaviorEvent e) {
        paramPdf.clear();
        paramPdf.put("pdfId", PdfGlobals.PRINT_RETURN_RECEIPT_ID.toString());
        paramPdf.put("returnReceiptId", returnReceiptId.toString());
        previewUrl = PdfUtils.getUrlViewHTML(paramPdf);
        FacesUtils.newTab(previewUrl);
    }

    public List<VSmReturnReceipt> getvSmReturnReceiptList() {
        return vSmReturnReceiptList;
    }

    public void setvSmReturnReceiptList(List<VSmReturnReceipt> vSmReturnReceiptList) {
        this.vSmReturnReceiptList = vSmReturnReceiptList;
    }

    public List<VSmReturnReceiptLine> getvSmReturnReceiptLineList() {
        return vSmReturnReceiptLineList;
    }

    public void setvSmReturnReceiptLineList(List<VSmReturnReceiptLine> vSmReturnReceiptLineList) {
        this.vSmReturnReceiptLineList = vSmReturnReceiptLineList;
    }

    public List<VSmOrder> getvSmOrderList() {
        return vSmOrderList;
    }

    public void setvSmOrderList(List<VSmOrder> vSmOrderList) {
        this.vSmOrderList = vSmOrderList;
    }

    public List<VSmOrderLine> getvSmOrderLineList() {
        return vSmOrderLineList;
    }

    public void setvSmOrderLineList(List<VSmOrderLine> vSmOrderLineList) {
        this.vSmOrderLineList = vSmOrderLineList;
    }

    public List<SelectItem> getCustomerItems() {
        return customerItems;
    }

    public void setCustomerItems(List<SelectItem> customerItems) {
        this.customerItems = customerItems;
    }

    public List<SelectItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<SelectItem> orderItems) {
        this.orderItems = orderItems;
    }

    public VSmProduct getvSmProduct() {
        return vSmProduct;
    }

    public void setvSmProduct(VSmProduct vSmProduct) {
        this.vSmProduct = vSmProduct;
    }

    public SmReturnReceiptLine getSmReturnReceiptLine() {
        return smReturnReceiptLine;
    }

    public void setSmReturnReceiptLine(SmReturnReceiptLine smReturnReceiptLine) {
        this.smReturnReceiptLine = smReturnReceiptLine;
    }

    public VSmReturnReceiptLine getvSmReturnReceiptLine() {
        return vSmReturnReceiptLine;
    }

    public void setvSmReturnReceiptLine(VSmReturnReceiptLine vSmReturnReceiptLine) {
        this.vSmReturnReceiptLine = vSmReturnReceiptLine;
    }

    public SmReturnReceipt getSmReturnReceipt() {
        return smReturnReceipt;
    }

    public void setSmReturnReceipt(SmReturnReceipt smReturnReceipt) {
        this.smReturnReceipt = smReturnReceipt;
    }

    public VSmReturnReceiptSummary getvSmReturnReceiptSummary() {
        return vSmReturnReceiptSummary;
    }

    public void setvSmReturnReceiptSummary(VSmReturnReceiptSummary vSmReturnReceiptSummary) {
        this.vSmReturnReceiptSummary = vSmReturnReceiptSummary;
    }

    public Long getSmReturnReceiptLineId() {
        return smReturnReceiptLineId;
    }

    public void setSmReturnReceiptLineId(Long smReturnReceiptLineId) {
        this.smReturnReceiptLineId = smReturnReceiptLineId;
    }

    public Long getReturnReceiptId() {
        return returnReceiptId;
    }

    public void setReturnReceiptId(Long returnReceiptId) {
        this.returnReceiptId = returnReceiptId;
    }

    public Long getvSmProductId() {
        return vSmProductId;
    }

    public void setvSmProductId(Long vSmProductId) {
        this.vSmProductId = vSmProductId;
    }

    public Long getLastReturnReceiptId() {
        return lastReturnReceiptId;
    }

    public void setLastReturnReceiptId(Long lastReturnReceiptId) {
        this.lastReturnReceiptId = lastReturnReceiptId;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getpReference() {
        return pReference;
    }

    public void setpReference(String pReference) {
        this.pReference = pReference;
    }

    public String getpReferenceHidden() {
        return pReferenceHidden;
    }

    public void setpReferenceHidden(String pReferenceHidden) {
        this.pReferenceHidden = pReferenceHidden;
    }

    public String getpDesignation() {
        return pDesignation;
    }

    public void setpDesignation(String pDesignation) {
        this.pDesignation = pDesignation;
    }

    public String getpDesignationHidden() {
        return pDesignationHidden;
    }

    public void setpDesignationHidden(String pDesignationHidden) {
        this.pDesignationHidden = pDesignationHidden;
    }

    public String getpUnitPrice() {
        return pUnitPrice;
    }

    public void setpUnitPrice(String pUnitPrice) {
        this.pUnitPrice = pUnitPrice;
    }

    public String getpQuantity() {
        return pQuantity;
    }

    public void setpQuantity(String pQuantity) {
        this.pQuantity = pQuantity;
    }

    public String getpTotalHt() {
        return pTotalHt;
    }

    public void setpTotalHt(String pTotalHt) {
        this.pTotalHt = pTotalHt;
    }

    public String getSummaryTotalQuantity() {
        return summaryTotalQuantity;
    }

    public void setSummaryTotalQuantity(String summaryTotalQuantity) {
        this.summaryTotalQuantity = summaryTotalQuantity;
    }

    public String getSummaryTotalHt() {
        return summaryTotalHt;
    }

    public void setSummaryTotalHt(String summaryTotalHt) {
        this.summaryTotalHt = summaryTotalHt;
    }

    public String getSummaryTvaAmount() {
        return summaryTvaAmount;
    }

    public void setSummaryTvaAmount(String summaryTvaAmount) {
        this.summaryTvaAmount = summaryTvaAmount;
    }

    public String getSummaryTotalTtc() {
        return summaryTotalTtc;
    }

    public void setSummaryTotalTtc(String summaryTotalTtc) {
        this.summaryTotalTtc = summaryTotalTtc;
    }

}
