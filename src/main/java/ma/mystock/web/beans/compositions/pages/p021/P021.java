package ma.mystock.web.beans.compositions.pages.p021;

import java.util.List;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import ma.mystock.core.dao.entities.InfBasicParameter;
import ma.mystock.core.dao.entities.views.VInfBasicParameter;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import ma.mystock.module.pageManagement.messages.MessageList;
import ma.mystock.module.pageManagement.annotations.Attribute;
import ma.mystock.web.utils.globals.GlobalsAttributs;
import ma.mystock.web.utils.lovs.LovsUtils;
import ma.mystock.web.utils.sessions.SessionsGetter;

/**
 *
 * @author Abdou
 */
public class P021 extends AbstractPage {

    @Attribute(itemCode = "basicParameter.s1")
    private List<VInfBasicParameter> basicParameterList;

    @Attribute(itemCode = "basicParameter.s2.name")
    private String name;

    @Attribute(itemCode = "basicParameter.s2.description")
    private String description;

    @Attribute(itemCode = "basicParameter.s2.value")
    private String value;

    @Attribute(itemCode = "basicParameter.s2.basicParameterType")
    private String basicParameterType;

    private List<SelectItem> basicParameterTypeList;
    private VInfBasicParameter vInfBasicParameter;
    private InfBasicParameter basicParameter;

    private Long basicParameterId;

    @Override
    public void onInit() {

        setShowForm(false);
        initListInfBasicParameter();
        log.info("BasicParameter @ onInit");

    }

    public void initListInfBasicParameter() {

        paramQuery.clear();
        paramQuery.put("active", GlobalsAttributs.Y);

        basicParameterList = vInfBasicParameterEJB.executeQuery(VInfBasicParameter.findAllActive, paramQuery);
        basicParameterTypeList = LovsUtils.getInfBasicParameterTypeItem();
    }

    public void initAttribut() {
        setName("");
        setDescription("");
        setValue("");
        setBasicParameterType("");
        setOperation("");
    }

    public void edit(AjaxBehaviorEvent e) {
        String val = e.getComponent().getAttributes().get("basicParameterId").toString();
        basicParameterId = Long.valueOf(val);

        paramQuery.clear();
        paramQuery.put("id", basicParameterId);
        vInfBasicParameter = vInfBasicParameterEJB.executeSingleQuery(VInfBasicParameter.findById, paramQuery);

        if (MyIs.isNull(vInfBasicParameter)) {
            return;
        }

        setName(vInfBasicParameter.getName());
        setDescription(vInfBasicParameter.getDescription());
        setValue(vInfBasicParameter.getValue());

        if (MyIs.isNotNull(vInfBasicParameter.getBasicParameterTypeId())) {
            setBasicParameterType(vInfBasicParameter.getBasicParameterTypeId().toString());
        }

        setShowForm(true);
        operation = GlobalsAttributs.OPERATION_EDIT;

        log.info("BasicParameter @ edit : operation(" + operation + "), basicParameterId(" + basicParameterId + ")");
    }

    @Override
    public void onDeletePartial(AjaxBehaviorEvent e) {

        basicParameterId = (Long) e.getComponent().getAttributes().get("basicParameterId");

        paramQuery.clear();
        paramQuery.put("id", basicParameterId);
        basicParameter = infBasicParameterEJB.executeSingleQuery(InfBasicParameter.findById, paramQuery);

        if (MyIs.isNotNull(basicParameter)) {
            infBasicParameterEJB.executeDelete(basicParameter);
        }

        initListInfBasicParameter();
        log.info("onDeletePartial @ onDeletePartial : basicParameterId(" + basicParameterId + ")");

    }

    public void addBasicParameter(AjaxBehaviorEvent event) {
        showForm = true;
        basicParameterId = null;
        operation = GlobalsAttributs.OPERATION_ADD;
    }

    public void delete(AjaxBehaviorEvent event) {

    }

    public void cancelBasicParameter(AjaxBehaviorEvent event) {
        showForm = false;
        initAttribut();
    }

    @Override
    public void onSavePartial(AjaxBehaviorEvent e) {

        if (!messages.isEmpty()) {
            return;
        }

        if (GlobalsAttributs.OPERATION_ADD.equals(operation)) {

            basicParameter = new InfBasicParameter();
            basicParameter.setUserCreation(Long.valueOf(SessionsGetter.getCltUserId()));
            basicParameter.setActive(GlobalsAttributs.Y);

        } else if (GlobalsAttributs.OPERATION_EDIT.equals(operation)) {

            paramQuery.clear();
            paramQuery.put("id", basicParameterId);
            basicParameter = infBasicParameterEJB.executeSingleQuery(InfBasicParameter.findById, paramQuery);
            basicParameter.setUserUpdate(Long.valueOf(SessionsGetter.getCltUserId()));
        }

        basicParameter.setName(name);
        basicParameter.setDescription(description);
        basicParameter.setValue(value);
        basicParameter.setBasicParameterTypeId(Long.parseLong(basicParameterType));

        basicParameter = infBasicParameterEJB.executeMerge(basicParameter);

        setShowForm(false);
        initAttribut();
        initListInfBasicParameter();
        log.info("BasicParameter @ onSavePartial");

    }

    public void validateBasicParameter(AjaxBehaviorEvent e) {

        if (!messages.isEmpty()) {
            return;
        }

        if (basicParameterId != null) {
            paramQuery.clear();
            paramQuery.put("id", basicParameterId);
            basicParameter = infBasicParameterEJB.executeSingleQuery(InfBasicParameter.findById, paramQuery);
            basicParameter.setUserUpdate(SessionsGetter.getCltUserId());
            basicParameter = infBasicParameterEJB.executeMerge(basicParameter);

        }

        setShowForm(false);
        initAttribut();
        initListInfBasicParameter();

        log.info("BasicParameter @ validateBasicParameter");

    }

    public List<VInfBasicParameter> getBasicParameterList() {
        return basicParameterList;
    }

    public void setBasicParameterList(List<VInfBasicParameter> basicParameterList) {
        this.basicParameterList = basicParameterList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getBasicParameterType() {
        return basicParameterType;
    }

    public void setBasicParameterType(String basicParameterType) {
        this.basicParameterType = basicParameterType;
    }

    public List<SelectItem> getBasicParameterTypeList() {
        return basicParameterTypeList;
    }

    public void setBasicParameterTypeList(List<SelectItem> basicParameterTypeList) {
        this.basicParameterTypeList = basicParameterTypeList;
    }

    public VInfBasicParameter getvInfBasicParameter() {
        return vInfBasicParameter;
    }

    public void setvInfBasicParameter(VInfBasicParameter vInfBasicParameter) {
        this.vInfBasicParameter = vInfBasicParameter;
    }

    public InfBasicParameter getBasicParameter() {
        return basicParameter;
    }

    public void setBasicParameter(InfBasicParameter basicParameter) {
        this.basicParameter = basicParameter;
    }

    public Long getBasicParameterId() {
        return basicParameterId;
    }

    public void setBasicParameterId(Long basicParameterId) {
        this.basicParameterId = basicParameterId;
    }

}
