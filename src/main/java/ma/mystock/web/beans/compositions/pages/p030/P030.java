package ma.mystock.web.beans.compositions.pages.p030;

import java.util.List;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import ma.mystock.core.dao.entities.CltUser;
import ma.mystock.core.dao.entities.views.VCltUser;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import ma.mystock.module.pageManagement.messages.MessageDetail;
import ma.mystock.web.utils.components.ContactManagementComponent;
import ma.mystock.web.utils.functionUtils.StringUtils;
import ma.mystock.web.utils.globals.GlobalsAttributs;
import ma.mystock.web.utils.lovs.LovsUtils;
import ma.mystock.web.utils.password.MyPassword;
import ma.mystock.web.utils.sessions.SessionsGetter;

/**
 *
 * @author Abdou
 */
public class P030 extends AbstractPage {

    private List<VCltUser> vCltUserList;
    private Long userId;
    private CltUser cltUser;

    private ContactManagementComponent contactManagementComponent;

    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private String category;
    private String active;
    private String entity;

    private List<SelectItem> categoryItems;

    @Override
    public void onInit() {
        initList();
        setShowForm(false);

    }

    public void initList() {

        vCltUserList = genericServices.findVCltUserByClientId(cltclientId);

        categoryItems = LovsUtils.getCltUserCategoryItems();
    }

    public void initAttributs() {
        setFirstName("");
        setLastName("");
        setUsername("");
        setCategory("");
        setActive("");
        setEntity("");
    }

    public void edit(AjaxBehaviorEvent e) {

        userId = (Long) e.getComponent().getAttributes().get("userId");

        if (userId != null) {

            cltUser = genericServices.findCltUserById(userId);

            if (cltUser != null) {

                initAttributs();

                firstName = cltUser.getFirstName();
                lastName = cltUser.getLastName();
                username = cltUser.getUsername();
                category = StringUtils.langToString(cltUser.getCategoryId());
                active = cltUser.getActive();
                entity = StringUtils.langToString(cltUser.getEntityId());

                contactManagementComponent = new ContactManagementComponent();
                contactManagementComponent.init(cltUser.getEntityId());

                setShowForm(true);
                setOperation(GlobalsAttributs.OPERATION_EDIT);

            }
        }

    }

    public void add(AjaxBehaviorEvent e) {

        initAttributs();

        setShowForm(true);
        setOperation(GlobalsAttributs.OPERATION_ADD);

    }

    @Override
    public void onValidateBeforeSavePartial(AjaxBehaviorEvent e) {
        System.out.println("onValidateBeforeSavePartial");
        messages.getErrors().add(new MessageDetail(1, "user1", "user2"));
    }

    @Override
    public void onSavePartial(AjaxBehaviorEvent e) {

        if (!messages.isEmpty()) {
            return;
        }

        if (GlobalsAttributs.OPERATION_ADD.equals(getOperation())) {

            cltUser = new CltUser();
            cltUser.setUserCreation(SessionsGetter.getCltUserId());
            cltUser.setClientId(SessionsGetter.getCltClientId());

        } else if (GlobalsAttributs.OPERATION_EDIT.equals(getOperation())) {

            cltUser = genericServices.findCltUserById(userId);
            cltUser.setUserUpdate(SessionsGetter.getCltUserId());

        }

        cltUser.setFirstName(firstName);
        cltUser.setLastName(lastName);
        cltUser.setUsername(username);
        cltUser.setCategoryId(StringUtils.stringToLong(category));
        cltUser.setActive(active);

        if (!"".equalsIgnoreCase(password)) {
            cltUser.setPassword(MyPassword.encryptPasswort(password));
        }

        cltUser = (CltUser) genericServices.genericPersiste(cltUser);

        initList();

        setShowForm(false);
    }

    @Override
    public void onDeletePartial(AjaxBehaviorEvent e) {

        userId = (Long) e.getComponent().getAttributes().get("userId");

        System.out.println(" > : userId  + " + userId);

        if (userId != null) {

            cltUser = genericServices.findCltUserById(userId);

            if (cltUser != null) {
                System.out.println("delete 1");
                genericServices.genericDelete(cltUser);
            }
        }

        System.out.println(" delete 2");

        initList();

    }

    @Override
    public void onCancelPartial(AjaxBehaviorEvent e) {
        showForm = false;

    }

    public List<VCltUser> getvCltUserList() {
        return vCltUserList;
    }

    public void setvCltUserList(List<VCltUser> vCltUserList) {
        this.vCltUserList = vCltUserList;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public CltUser getCltUser() {
        return cltUser;
    }

    public void setCltUser(CltUser cltUser) {
        this.cltUser = cltUser;
    }

    public ContactManagementComponent getContactManagementComponent() {
        return contactManagementComponent;
    }

    public void setContactManagementComponent(ContactManagementComponent contactManagementComponent) {
        this.contactManagementComponent = contactManagementComponent;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public List<SelectItem> getCategoryItems() {
        return categoryItems;
    }

    public void setCategoryItems(List<SelectItem> categoryItems) {
        this.categoryItems = categoryItems;
    }

}
