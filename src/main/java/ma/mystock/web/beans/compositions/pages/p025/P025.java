package ma.mystock.web.beans.compositions.pages.p025;

import java.util.List;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import ma.mystock.module.pageManagement.beans.AbstractPage;
import ma.mystock.web.utils.functionUtils.StringUtils;
import ma.mystock.web.utils.functionUtils.params.ClientParameterUtils;
import ma.mystock.web.utils.lovs.LovsUtils;

/**
 *
 * Abdou
 */
public class P025 extends AbstractPage {

    // Nom de l'application 
    private String paramClient1_applicationName;

    //Titre de l'application
    private String paramClient2_applicationTitle;

    //La langue par défaut
    private String paramClient3_defaultLanguage;

    //Le préfixe par défaut
    private String paramClient4_defaultPrefix;

    //Activer les caches des ressources css, js et images
    private boolean paramClient5_enableCacheResources;

    //Le patterne de la date
    private String paramClient6_datePattern;

    //Le patterne de date et l'heure
    private String paramClient7_dateTimePattern;

    //Le patterne de l'heure
    private String paramClient8_TimePattern;

    //La devise (point sur inf_currency)
    private String paramClient9_currencyCode;

    //Le nom du locataire
    private String paramClient10_tenantName;

    private List<SelectItem> currencyItems;

    @Override
    public void onInit() {

        paramClient1_applicationName = ClientParameterUtils.getApplicationName();
        paramClient2_applicationTitle = ClientParameterUtils.getApplicationTitle();
        paramClient3_defaultLanguage = StringUtils.langToString(ClientParameterUtils.getDefaultLanguage());
        paramClient4_defaultPrefix = StringUtils.langToString(ClientParameterUtils.getDefaultPrefix());
        paramClient5_enableCacheResources = ClientParameterUtils.getEnableCacheResources();
        paramClient6_datePattern = ClientParameterUtils.getPatternDate();
        paramClient7_dateTimePattern = ClientParameterUtils.getPatternDateTime();
        paramClient8_TimePattern = ClientParameterUtils.getPatternTime();
        paramClient9_currencyCode = ClientParameterUtils.getCurrency();
        paramClient10_tenantName = ClientParameterUtils.getTenantName();

        currencyItems = LovsUtils.getInfCurrencyItems();

    }

    @Override
    public void onSavePartial(AjaxBehaviorEvent e) {
        System.out.println(" save");

        // update DB
        // refresh Maps
    }

    public String getParamClient1_applicationName() {
        return paramClient1_applicationName;
    }

    public void setParamClient1_applicationName(String paramClient1_applicationName) {
        this.paramClient1_applicationName = paramClient1_applicationName;
    }

    public String getParamClient2_applicationTitle() {
        return paramClient2_applicationTitle;
    }

    public void setParamClient2_applicationTitle(String paramClient2_applicationTitle) {
        this.paramClient2_applicationTitle = paramClient2_applicationTitle;
    }

    public String getParamClient3_defaultLanguage() {
        return paramClient3_defaultLanguage;
    }

    public void setParamClient3_defaultLanguage(String paramClient3_defaultLanguage) {
        this.paramClient3_defaultLanguage = paramClient3_defaultLanguage;
    }

    public String getParamClient4_defaultPrefix() {
        return paramClient4_defaultPrefix;
    }

    public void setParamClient4_defaultPrefix(String paramClient4_defaultPrefix) {
        this.paramClient4_defaultPrefix = paramClient4_defaultPrefix;
    }

    public boolean getParamClient5_enableCacheResources() {
        return paramClient5_enableCacheResources;
    }

    public void setParamClient5_enableCacheResources(boolean paramClient5_enableCacheResources) {
        this.paramClient5_enableCacheResources = paramClient5_enableCacheResources;
    }

    public String getParamClient6_datePattern() {
        return paramClient6_datePattern;
    }

    public void setParamClient6_datePattern(String paramClient6_datePattern) {
        this.paramClient6_datePattern = paramClient6_datePattern;
    }

    public String getParamClient7_dateTimePattern() {
        return paramClient7_dateTimePattern;
    }

    public void setParamClient7_dateTimePattern(String paramClient7_dateTimePattern) {
        this.paramClient7_dateTimePattern = paramClient7_dateTimePattern;
    }

    public String getParamClient8_TimePattern() {
        return paramClient8_TimePattern;
    }

    public void setParamClient8_TimePattern(String paramClient8_TimePattern) {
        this.paramClient8_TimePattern = paramClient8_TimePattern;
    }

    public String getParamClient9_currencyCode() {
        return paramClient9_currencyCode;
    }

    public void setParamClient9_currencyCode(String paramClient9_currencyCode) {
        this.paramClient9_currencyCode = paramClient9_currencyCode;
    }

    public String getParamClient10_tenantName() {
        return paramClient10_tenantName;
    }

    public void setParamClient10_tenantName(String paramClient10_tenantName) {
        this.paramClient10_tenantName = paramClient10_tenantName;
    }

    public List<SelectItem> getCurrencyItems() {
        return currencyItems;
    }

    public void setCurrencyItems(List<SelectItem> currencyItems) {
        this.currencyItems = currencyItems;
    }

}
