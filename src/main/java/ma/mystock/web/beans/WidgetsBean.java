package ma.mystock.web.beans;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import ma.mystock.core.dao.entities.views.VBiSmOrderLineByDay;
import ma.mystock.core.dao.entities.views.VBiSmReceptionLineByDay;
import ma.mystock.core.dao.entities.views.VInfMonth;
import ma.mystock.core.dao.entities.views.VSmOrderDetails;
import ma.mystock.core.dao.entities.views.VSmProductAlert;
import ma.mystock.core.services.GenericServices;
import ma.mystock.module.pageManagement.beans.GenericBean;
import ma.mystock.web.utils.functionUtils.DateUtils;
import ma.mystock.web.utils.generators.RandomGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author anasshajami
 */
@Component("widgetsBean")
@Scope("request")
public class WidgetsBean extends GenericBean implements Serializable {

    @Autowired
    private GenericServices genericServices;

    private String priceByMonthValue;
    private String priceByMonthBuy;
    private String priceByMonthSale;

    private String countProductByMonthValue;
    private String countProductByMonthBuy;
    private String countProductByMonthSale;

    private List<VSmProductAlert> vSmProductAlertList;

    private List<VBiSmOrderLineByDay> vBiSmOrderLineByDayList;

    private List<VBiSmReceptionLineByDay> vBiSmReceptionLineByDayList;

    private VInfMonth vInfMonth;

    private VSmOrderDetails vSmOrderDetails;

    @PostConstruct
    public void init() {

        /**
         * Count order
         */
        vSmOrderDetails = genericServices.findVSmOrderDetailsByCltModuleId(cltModuleId);

        /**
         * Alert stock
         */
        vSmProductAlertList = genericServices.findVSmProductAlertByCltModuleId(cltModuleId);

        /**
         * Statistique
         */
        vInfMonth = genericServices.findVInfMonthById(DateUtils.getCurrentMonth());

        vBiSmOrderLineByDayList = genericServices.findVBiSmOrderLineByDayByCltModuleIdAndMonthAndYear(DateUtils.getCurrentMonth(), DateUtils.getCurrentYear(), cltModuleId);

        Map<String, VBiSmOrderLineByDay> vBiSmOrderLineByDayMap = new HashMap<>();

        if (vBiSmOrderLineByDayList != null && !vBiSmOrderLineByDayList.isEmpty()) {
            for (VBiSmOrderLineByDay o : vBiSmOrderLineByDayList) {
                String key = o.getDay() + "-" + o.getMonth() + "-" + o.getYear();
                vBiSmOrderLineByDayMap.put(key, o);
            }
        }

        vBiSmReceptionLineByDayList = genericServices.findVBiSmReceptionLineByDayByCltModuleIdAndMonthAndYear(DateUtils.getCurrentMonth(), DateUtils.getCurrentYear(), cltModuleId);

        Map<String, VBiSmReceptionLineByDay> vBiSmReceptionLineByDayMap = new HashMap<>();

        if (vBiSmReceptionLineByDayList != null && !vBiSmReceptionLineByDayList.isEmpty()) {
            for (VBiSmReceptionLineByDay o : vBiSmReceptionLineByDayList) {
                String key = o.getDay() + "-" + o.getMonth() + "-" + o.getYear();
                vBiSmReceptionLineByDayMap.put(key, o);
            }
        }

        if (vInfMonth != null && vInfMonth.getNumberOfDays() != null) {

            priceByMonthValue = "";
            priceByMonthBuy = "";
            priceByMonthSale = "";

            countProductByMonthValue = "";
            countProductByMonthBuy = "";
            countProductByMonthSale = "";

            for (int i = 1; i <= vInfMonth.getNumberOfDays(); i++) {

                String key = i + "-" + DateUtils.getCurrentMonth() + "-" + DateUtils.getCurrentYear();

                priceByMonthValue = priceByMonthValue + "\"" + i + "/" + currentMonth + "/" + currentYear + "\",";
                countProductByMonthValue = countProductByMonthValue + "\"" + i + "/" + DateUtils.getCurrentMonth() + "/" + DateUtils.getCurrentYear() + "\",";

                priceByMonthBuy = priceByMonthBuy + RandomGenerator.randomInt(1, 30) + ",";
                /*  RandomGenerator.randomInt(1, 30) */

                countProductByMonthBuy = countProductByMonthBuy + RandomGenerator.randomInt(20, 300) + ","; // RandomGenerator.randomInt(1, 300)

                priceByMonthSale = priceByMonthSale + RandomGenerator.randomInt(20, 30) + ",";
                countProductByMonthSale = countProductByMonthSale + RandomGenerator.randomInt(10, 50) + ",";

                /*
                    
                 if (vBiSmOrderLineByDayMap.containsKey(key)) {
                    
                 String valuePrice = vBiSmOrderLineByDayMap.get(key).getSumPrice().toString();
                 String valueQte = vBiSmOrderLineByDayMap.get(key).getSumQuantity().toString();
                    
                 priceByMonthBuy = priceByMonthBuy + RandomGenerator.randomInt(1, 30) + ","; // RandomGenerator.randomInt(1, 30) 

                 countProductByMonthBuy = countProductByMonthBuy + RandomGenerator.randomInt(20, 300) + ","; // RandomGenerator.randomInt(1, 300)

                 } else {

                 priceByMonthBuy = priceByMonthBuy + "0" + ","; //  RandomGenerator.randomInt(1, 30) 

                 countProductByMonthBuy = countProductByMonthBuy + "0" + ",";

                 }

                 if (vBiSmReceptionLineByDayMap.containsKey(key)) {

                 String valuePrice = vBiSmReceptionLineByDayMap.get(key).getSumPrice().toString();
                 String valueQte = vBiSmReceptionLineByDayMap.get(key).getSumQuantity().toString();

                 priceByMonthSale = priceByMonthSale + RandomGenerator.randomInt(20, 30) + ",";
                 countProductByMonthSale = countProductByMonthSale + RandomGenerator.randomInt(10, 50) + ",";
                 } else {
                 priceByMonthSale = priceByMonthSale + "0" + ",";
                 countProductByMonthSale = countProductByMonthSale + "0" + ",";
                 }
                 */
            }

            priceByMonthValue = priceByMonthValue.substring(0, priceByMonthValue.length() - 1);
            priceByMonthBuy = priceByMonthBuy.substring(0, priceByMonthBuy.length() - 1);
            priceByMonthSale = priceByMonthSale.substring(0, priceByMonthSale.length() - 1);

            countProductByMonthValue = countProductByMonthValue.substring(0, countProductByMonthValue.length() - 1);
            countProductByMonthBuy = countProductByMonthBuy.substring(0, countProductByMonthBuy.length() - 1);
            countProductByMonthSale = countProductByMonthSale.substring(0, countProductByMonthSale.length() - 1);

            System.out.println("priceByMonthValue : " + priceByMonthValue);
            System.out.println("priceByMonthBuy : " + priceByMonthBuy);
            System.out.println("priceByMonthSale : " + priceByMonthSale);

            System.out.println("countByMonthValue : " + countProductByMonthValue);
            System.out.println("countByMonthBuy : " + countProductByMonthBuy);
            System.out.println("countByMonthSale : " + countProductByMonthSale);

        }

        System.out.println(" on init ......");
    }

    public GenericServices getGenericServices() {
        return genericServices;
    }

    public void setGenericServices(GenericServices genericServices) {
        this.genericServices = genericServices;
    }

    public List<VSmProductAlert> getvSmProductAlertList() {
        return vSmProductAlertList;
    }

    public void setvSmProductAlertList(List<VSmProductAlert> vSmProductAlertList) {
        this.vSmProductAlertList = vSmProductAlertList;
    }

    public String getPriceByMonthValue() {
        return priceByMonthValue;
    }

    public void setPriceByMonthValue(String priceByMonthValue) {
        this.priceByMonthValue = priceByMonthValue;
    }

    public String getPriceByMonthBuy() {
        return priceByMonthBuy;
    }

    public void setPriceByMonthBuy(String priceByMonthBuy) {
        this.priceByMonthBuy = priceByMonthBuy;
    }

    public String getPriceByMonthSale() {
        return priceByMonthSale;
    }

    public void setPriceByMonthSale(String priceByMonthSale) {
        this.priceByMonthSale = priceByMonthSale;
    }

    public String getCountProductByMonthValue() {
        return countProductByMonthValue;
    }

    public void setCountProductByMonthValue(String countProductByMonthValue) {
        this.countProductByMonthValue = countProductByMonthValue;
    }

    public String getCountProductByMonthBuy() {
        return countProductByMonthBuy;
    }

    public void setCountProductByMonthBuy(String countProductByMonthBuy) {
        this.countProductByMonthBuy = countProductByMonthBuy;
    }

    public String getCountProductByMonthSale() {
        return countProductByMonthSale;
    }

    public void setCountProductByMonthSale(String countProductByMonthSale) {
        this.countProductByMonthSale = countProductByMonthSale;
    }

    public VInfMonth getvInfMonth() {
        return vInfMonth;
    }

    public void setvInfMonth(VInfMonth vInfMonth) {
        this.vInfMonth = vInfMonth;
    }

    public VSmOrderDetails getvSmOrderDetails() {
        return vSmOrderDetails;
    }

    public void setvSmOrderDetails(VSmOrderDetails vSmOrderDetails) {
        this.vSmOrderDetails = vSmOrderDetails;
    }

}
