/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.beans;

import java.io.Serializable;
import ma.mystock.web.beans.helpers.BreadCrumbHelper;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import ma.mystock.module.pageManagement.beans.GenericBean;
import ma.mystock.web.utils.sessions.SessionsGetter;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author abdou
 */
//@ManagedBean
//@RequestScoped
@Component("breadCrumbBean")
@Scope("view")
public class BreadCrumbBean extends GenericBean implements Serializable {

    private String cltModuleName;
    private String cltModuleId;

    protected static Logger log = Logger.getLogger(BreadCrumbBean.class);

    Map<String, BreadCrumbHelper> map = new HashMap<>();

    @PostConstruct
    public void init() {

        try {
            cltModuleName = SessionsGetter.getCltModule().getName();
            cltModuleId = SessionsGetter.getCltModuleId().toString();
        } catch (Exception ex) {

        }

        //map.put("1", new  BreadCrumbHelper("Module 1", "Module 2"));
    }

    public Map<String, BreadCrumbHelper> getMap() {
        return map;
    }

    public void setMap(Map<String, BreadCrumbHelper> map) {
        this.map = map;
    }

    public String getCltModuleName() {
        return cltModuleName;
    }

    public void setCltModuleName(String cltModuleName) {
        this.cltModuleName = cltModuleName;
    }

    public String getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(String cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

}
