package ma.mystock.web.beans;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;
import ma.mystock.core.dao.entities.CltModule;
import ma.mystock.core.dao.entities.PmModel;
import ma.mystock.core.dao.entities.views.VCltClient;
import ma.mystock.core.dao.entities.views.VCltUserDetails;
import ma.mystock.core.dao.entities.views.VCltUserModule;
import ma.mystock.core.services.GenericServices;
import ma.mystock.module.pageManagement.beans.GenericBean;
import ma.mystock.module.pageManagement.beans.PageManagerBean;
import ma.mystock.module.pageManagement.components.ApplicationContent;
import ma.mystock.module.pageManagement.components.SessionContent;
import ma.mystock.module.pageManagement.messages.MessageDetail;
import ma.mystock.web.filters.ChangeURIPathFilter;
import ma.mystock.web.utils.datatype.MyString;
import ma.mystock.web.utils.functionUtils.params.ClientParameterUtils;
import ma.mystock.web.utils.jsf.FacesUtils;
import ma.mystock.web.utils.mail.MailGlobals;
import ma.mystock.web.utils.mail.MailManager;
import ma.mystock.web.utils.mail.MailParameters;
import ma.mystock.web.utils.password.MyPassword;
import ma.mystock.web.utils.routing.UrlsValues;
import ma.mystock.web.utils.sessions.SessionsGetter;
import static ma.mystock.web.utils.springBeans.GloablsServices.cltModuleEJB;
import static ma.mystock.web.utils.springBeans.GloablsServices.pmModelEJB;
import ma.mystock.web.utils.values.MapsGetter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author abdou
 *
 */
@Component("loginBean")
@Scope("request")
public class LoginBean extends GenericBean implements Serializable {

    /**
     * Session
     */
    @Autowired
    SessionContent sessionContent;
    
    @Autowired
    ApplicationContent applicationContent;
    
    
    private String username;
    private String password;

    private String titleOfApplication;

    @Autowired
    private GenericServices genericServices;

    private PageManagerBean pageManager = null;//(PageManagerBean) FacesContextUtils.getWebApplicationContext(FacesUtils.getFacesContext()).getBean("pageManagerBean");

    @PostConstruct
    public void init() {

        log.info("Init Bean : LoginBean ");

        try {
            // If user connected => forword to page Module 
            if (SessionsGetter.getIsConnected()) {
                //RedirectUtils.goToModule();
            }

        } catch (Exception ex) {
        }

        titleOfApplication = ClientParameterUtils.getApplicationTitle();

    }

    public void login(ActionEvent e) {
        /**
         * MESSAGE : Login
         */

        MailParameters parameters = new MailParameters();
        parameters.setMessageId(MailGlobals.MAIL_1_LOGIN);

        //MailManager.support("sujet", "content");

        if (validateForm()) {

            paramQuery.clear();
            paramQuery.put("username", username);
            paramQuery.put("password", MyPassword.encryptPasswort(password));
            paramQuery.put("clientId", SessionsGetter.getCltClientId());

            VCltUserDetails vCltUserDetails = genericServices.findVCltUserDetailsByUsernameAndPassword(username, MyString.trim(MyPassword.toSHA1(password)));

            System.out.println(" ");
            if (vCltUserDetails != null) {

                System.out.println(" => " + vCltUserDetails);
                System.out.println(" => " + vCltUserDetails.getId());

                // set User
                SessionsGetter.setVCltUserDetails(vCltUserDetails); // OK
                
                // set Client
                
                VCltClient vCltClient = genericServices.findVCltClientById(vCltUserDetails.getClientId());
                
                if(vCltClient != null){
                    
                    ChangeURIPathFilter.setClientCode(FacesUtils.getHttpServletRequest(), vCltClient.getCode());
                    
                }
                
                
                
                
                System.out.println(" Go to page Module");

                //RedirectUtils.goToModule();
                // to change 
                try {

                    String page = "";

                    List<VCltUserModule> vCltUserModuleList = genericServices.findVCltUserModuleByUserId(SessionsGetter.getCltUserId());

                    if (vCltUserModuleList != null && vCltUserModuleList.size() == 1) {

                        // forward to page index 
                        VCltUserModule vCltUserModule = vCltUserModuleList.get(0);

                        paramQuery.clear();
                        paramQuery.put("id", vCltUserModule.getModuleId());
                        CltModule cltModule = cltModuleEJB.executeSingleQuery(CltModule.findById, paramQuery);
                        SessionsGetter.setCltModule(cltModule);

                        paramQuery.clear();
                        paramQuery.put("id", cltModule.getPmModelId());
                        PmModel pmModel = pmModelEJB.executeSingleQuery(PmModel.findById, paramQuery);
                        SessionsGetter.setPmModel(pmModel);

                        System.out.println(" model selected :  " + pmModel.getId() + " : " + pmModel.getName());

                        // initalisé le progManager
                        pageManager.onDestroy();

                        pageManager.onInit();

                        page = UrlsValues.URL_INDEX_PAGE;

                    } else {

                        // forward to page module
                        page = UrlsValues.URL_MODULE_PAGE;
                    }

                    //String url = MapsGetter.getConfigBasicMap().get("1") + "/" + SessionsGetter.getCltClientCode() + "/" + page + ".xhtml";
                    String url = MapsGetter.getConfigBasicMap().get("1") + page + ".xhtml";
                    System.out.println(" redirect to : " + url);
                    FacesUtils.getExternalContext().redirect(url);

                } catch (Exception ex1) {
                    ex1.printStackTrace();
                }

            } else {
                messages.add(new MessageDetail(1, "login.s1.username", "E-mail ou mot de passe incorrect"));
            }

        }
    }

    public boolean validateForm() {

        // Appel la validation coté PM
        messages.clear();
        if ("".equals(username)) {
            messages.add(new MessageDetail(1, "login.s1.username", "le username est obligatoire"));
        }
        if ("".equals(password)) {
            messages.add(new MessageDetail(1, "login.s1.password", "le mot de passe est obligatoire"));
        }

        return messages.isEmpty();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTitleOfApplication() {
        return titleOfApplication;
    }

    public void setTitleOfApplication(String titleOfApplication) {
        this.titleOfApplication = titleOfApplication;
    }

}
