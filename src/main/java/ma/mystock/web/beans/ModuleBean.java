package ma.mystock.web.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import ma.mystock.core.dao.entities.CltModule;
import ma.mystock.core.dao.entities.PmModel;
import ma.mystock.core.dao.entities.views.VCltModuleType;
import ma.mystock.core.dao.entities.views.VCltUserModule;
import ma.mystock.web.utils.jsf.FacesUtils;
import ma.mystock.module.pageManagement.beans.GenericBean;
import ma.mystock.module.pageManagement.beans.PageManagerBean;
import ma.mystock.module.pageManagement.components.SessionContent;
import ma.mystock.web.beans.helpers.ModuleHelper;
import ma.mystock.web.utils.annotations.TODO;
import ma.mystock.web.utils.sessions.SessionsGetter;
import ma.mystock.web.utils.values.MapsGetter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.jsf.FacesContextUtils;

/**
 *
 * @author abdou
 *
 */
@Component("moduleBean")
@Scope("view")
@TODO(value = "Afficher que les types des modules autorisé pour l'utilisateur connecté filtre sur VCltModuleType")
public class ModuleBean extends GenericBean implements Serializable {

    private PageManagerBean pageManager = (PageManagerBean) FacesContextUtils.getWebApplicationContext(FacesUtils.getFacesContext()).getBean("pageManagerBean");
    private VCltUserModule vCltUserModule;
    private List<VCltModuleType> vCltModuleTypeList;
    private List<ModuleHelper> moduleHelperList = new ArrayList<>();

    /**
     * Session
     */
    //@Autowired
    //private SessionContent sessionContent;
    @PostConstruct
    public void init() {

        //System.out.println(" module ... : " + sessionContent.getAa());
        vCltModuleTypeList = vCltModuleTypeEJB.executeQuery(VCltModuleType.findAll);

        for (VCltModuleType o : vCltModuleTypeList) {

            System.out.println(" => userId : " + SessionsGetter.getCltUserId());
            System.out.println(" => moduleTypeId : " + o.getId());

            paramQuery.clear();
            paramQuery.put("userId", SessionsGetter.getCltUserId());
            paramQuery.put("moduleTypeId", o.getId());
            List<VCltUserModule> vCltUserModuleList = vCltUserModuleEJB.executeQuery(VCltUserModule.findByUserIdAndModuleTypeId, paramQuery);

            if (!vCltUserModuleList.isEmpty()) {
                ModuleHelper helper = new ModuleHelper();
                helper.setModuleTypeId(o.getId());
                helper.setModuleTypeName(o.getName());
                helper.setvCltUserModuleList(vCltUserModuleList);
                moduleHelperList.add(helper);
            }

        }
    }

    public void selectModule() {

        System.out.println(" module selected :  " + vCltUserModule.getModuleId() + " : " + vCltUserModule.getModuleName());

        paramQuery.clear();
        paramQuery.put("id", vCltUserModule.getModuleId());
        CltModule cltModule = cltModuleEJB.executeSingleQuery(CltModule.findById, paramQuery);
        SessionsGetter.setCltModule(cltModule);

        paramQuery.clear();
        paramQuery.put("id", cltModule.getPmModelId());
        PmModel pmModel = pmModelEJB.executeSingleQuery(PmModel.findById, paramQuery);
        SessionsGetter.setPmModel(pmModel);

        System.out.println(" model selected :  " + pmModel.getId() + " : " + pmModel.getName());

        // initalisé le progManager
        pageManager.onDestroy();

        pageManager.onInit();

        try {

            //String url = MapsGetter.getConfigBasicMap().get("1") + "/" + SessionsGetter.getCltClientCode() + "/index.xhtml";
            String url = MapsGetter.getConfigBasicMap().get("1") + "index.xhtml";

            System.out.println(" redirect to : " + url);
            FacesUtils.getExternalContext().redirect(url);
            return;
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public PageManagerBean getPageManager() {
        return pageManager;
    }

    public void setPageManager(PageManagerBean pageManager) {
        this.pageManager = pageManager;
    }

    public VCltUserModule getvCltUserModule() {
        return vCltUserModule;
    }

    public void setvCltUserModule(VCltUserModule vCltUserModule) {
        this.vCltUserModule = vCltUserModule;
    }

    public List<VCltModuleType> getvCltModuleTypeList() {
        return vCltModuleTypeList;
    }

    public void setvCltModuleTypeList(List<VCltModuleType> vCltModuleTypeList) {
        this.vCltModuleTypeList = vCltModuleTypeList;
    }

    public List<ModuleHelper> getModuleHelperList() {
        return moduleHelperList;
    }

    public void setModuleHelperList(List<ModuleHelper> moduleHelperList) {
        this.moduleHelperList = moduleHelperList;
    }

}
