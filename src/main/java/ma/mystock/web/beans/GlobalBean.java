/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.beans;

import java.io.Serializable;
import ma.mystock.module.pageManagement.beans.GenericBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author anasshajami
 */
@Component("globalBean")
@Scope("request")
public class GlobalBean extends GenericBean implements Serializable {

}
