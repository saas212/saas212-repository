package ma.mystock.web.beans;

import java.io.Serializable;
import ma.mystock.module.pageManagement.beans.GenericBean;
import ma.mystock.web.utils.functionUtils.params.ClientParameterUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author anasshajami
 *
 * Pour affiche la liste des inforamtions de client ex : Image de profile header
 * title, option ...
 */
@Component("tenantBean")
@Scope("request")
public class TenantBean extends GenericBean implements Serializable {

    private String tenantUser;

    private boolean isDebug;

    public void init() {
        isDebug = ClientParameterUtils.getIsDebug();
    }

    public String getTenantUser() {
        return tenantUser;
    }

    public void setTenantUser(String tenantUser) {
        this.tenantUser = tenantUser;
    }

    public boolean isIsDebug() {
        return isDebug;
    }

    public void setIsDebug(boolean isDebug) {
        this.isDebug = isDebug;
    }

}
