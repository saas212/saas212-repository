package ma.mystock.web.listners.helpers;

import java.util.HashMap;
import ma.mystock.web.utils.sessions.SessionsGetter;

/**
 *
 * @author abdou
 */
public class PageParameterMap extends HashMap<String, String> {

    @Override
    public String get(Object key) {
        System.out.println("PageParameterMap get  " + SessionsGetter.getCltModuleId() + "." + key);
        return super.get(SessionsGetter.getCltModuleId() + "." + key);
    }

}
