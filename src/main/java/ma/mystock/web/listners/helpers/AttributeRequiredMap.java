/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.listners.helpers;

import java.util.HashMap;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.web.utils.sessions.SessionsGetter;
import org.apache.log4j.Logger;

/**
 *
 * @author abdou
 */
public class AttributeRequiredMap extends HashMap<String, String> {

    protected static Logger log = Logger.getLogger(AttributeRequiredMap.class);

    @Override
    public String get(Object key) {

        String value = super.get(SessionsGetter.getCltModuleId() + "." + key);

        if (!MyIs.isEmpty(value)) {
            return value;
        } else {
            return super.get(key);
        }
    }

}
