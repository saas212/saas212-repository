package ma.mystock.web.listners.helpers;

import java.util.HashMap;
import ma.mystock.web.utils.sessions.SessionsGetter;

/**
 *
 * @author abdou
 *
 */
public class PrivilegesMap extends HashMap<String, String> {

    @Override
    public String get(Object key) {
        return super.get(SessionsGetter.getCltClientId() + "." + SessionsGetter.getCltModuleId() + "." + key);
    }

}
