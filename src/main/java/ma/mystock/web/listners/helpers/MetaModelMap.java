package ma.mystock.web.listners.helpers;

import java.util.HashMap;
import ma.mystock.web.utils.sessions.SessionsGetter;

/**
 *
 * @author abdou
 *
 */
public class MetaModelMap extends HashMap<String, String> {

    @Override
    public String get(Object key) {
        String newkey = SessionsGetter.getInfPrefixCode() + "." + SessionsGetter.getInfLanguageCode() + "." + key;
        String val = super.get(newkey);
        if ("".equals(val) || val == null) {
            val = "?" + key + " ?";
        }
        return val;
    }

    public String getTxtValue(Object key) {
        key = SessionsGetter.getInfPrefixCode() + "." + SessionsGetter.getInfLanguageCode() + "." + key;
        return super.get(key);
    }
}
