package ma.mystock.web.listners.helpers;

import java.util.HashMap;
import java.util.List;
import ma.mystock.core.dao.entities.views.dto.LovsDTO;

/**
 *
 * @author abdou
 */
public class LovMap extends HashMap<String, List<LovsDTO>> {

    @Override
    public List<LovsDTO> get(Object key) {
        // remove not actived & selected module 
        return super.get(key);
    }

    public List<LovsDTO> get(Object key, Long id) {
        // remove not actived sauf id & selected module 
        return super.get(key);
    }

}
