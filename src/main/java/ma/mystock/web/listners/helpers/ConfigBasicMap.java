/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.listners.helpers;

import java.util.HashMap;
import ma.mystock.web.utils.datatype.MyIs;

/**
 *
 * @author abdou
 */
public class ConfigBasicMap extends HashMap<String, String> {

    @Override
    public String get(Object key) {

        // Si le type est Long, il faut le converter vers String
        String res = super.get(key);
        if (MyIs.isEmpty(key)) {
            res = " ?? config[" + key + "] ??";
        }
        return res;
    }
}
