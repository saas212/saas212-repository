package ma.mystock.web.listners.helpers;

import java.util.HashMap;
import ma.mystock.web.utils.sessions.SessionsGetter;

/**
 *
 * @author abdou
 */
public class AttributeMaxLengthMap extends HashMap<String, String> {

    @Override
    public String get(Object key) {
        return super.get(SessionsGetter.getCltModuleId() + "." + key);
    }

}
