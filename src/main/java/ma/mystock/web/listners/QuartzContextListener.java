package ma.mystock.web.listners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import ma.mystock.web.utils.springBeans.GloablsServices;
import ma.mystock.web.utils.quartz.MyJob;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

//@WebListener
public class QuartzContextListener implements ServletContextListener {

    Scheduler scheduler = null;

    //final WebApplicationContext springContext = WebApplicationContextUtils.getWebApplicationContext(event.getServletContext());
    //final Properties props = (Properties)springContext.getBean("myProps");
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        System.out.println("Start QuartzContextListener");
        try {
            // Configurer cette application via la base de donner

            /**
             * *********** ce ode ça marche très très bien
             *
             * JobDetail job =
             * JobBuilder.newJob(MyJob.class).withIdentity("MyJob",
             * "group1").build();
             *
             * Trigger trigger =
             * TriggerBuilder.newTrigger().withIdentity("MyJob",
             * "group1").withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(5).repeatForever()).build();
             *
             ************
             */
            //Trigger trigger = TriggerBuilder.newTrigger().withIdentity("dummyTriggerName2", "group2").withSchedule(CronScheduleBuilder.cronSchedule("0/5 * * * * ?")).build();

            /*
             scheduler = new StdSchedulerFactory().getScheduler();
             scheduler.start();
             scheduler.scheduleJob(job, trigger);
             */
        } catch (Exception e) {
            System.out.println("Exception QuartzContextListener ");
            e.printStackTrace();
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        /*
         System.out.println("End QuartzContextListener");
         try {
         scheduler.shutdown();
         } catch (SchedulerException e) {
         //e.printStackTrace();
         }
        
         */
    }
}
