package ma.mystock.web.listners;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import ma.mystock.core.dao.GenericDAO;
import ma.mystock.core.dao.entities.views.VCltParameterClient;
import ma.mystock.core.dao.entities.views.VInfBasicParameter;
import ma.mystock.core.dao.entities.views.VInfMetaModel;
import ma.mystock.core.dao.entities.views.VInfPrivileges;
import ma.mystock.core.dao.entities.views.VPmPageAttribute;
import ma.mystock.core.dao.entities.views.VPmPageAttributeModule;
import ma.mystock.core.dao.entities.views.VPmPageParameterModule;
import ma.mystock.core.dao.entities.views.VPmValidation;
import ma.mystock.core.dao.entities.views.VSmLovs;
import ma.mystock.core.dao.entities.views.dto.VPmModelDto;
import ma.mystock.core.dao.entities.views.dto.VPmCategorysDto;
import ma.mystock.core.dao.entities.views.dto.VPmGroupsDto;
import ma.mystock.core.dao.entities.views.dto.VPmMenusDto;
import ma.mystock.core.dao.entities.views.dto.VPmPagesDto;
import ma.mystock.core.services.GenericServices;
import ma.mystock.web.listners.helpers.AttributHiddenMap;
import ma.mystock.web.listners.helpers.AttributeMaxLengthMap;
import ma.mystock.web.listners.helpers.AttributeMaxWordMap;
import ma.mystock.web.listners.helpers.AttributeReadOnlyMap;
import ma.mystock.web.listners.helpers.AttributeRequiredMap;
import ma.mystock.web.listners.helpers.ConfigBasicMap;
import ma.mystock.web.listners.helpers.ItemCodeValidationMap;
import ma.mystock.web.listners.helpers.LovMap;
import ma.mystock.web.listners.helpers.MetaModelMap;
import ma.mystock.web.listners.helpers.ModelMap;
import ma.mystock.web.listners.helpers.PageMap;
import ma.mystock.web.listners.helpers.PageParameterMap;
import ma.mystock.web.listners.helpers.PageValidationMap;
import ma.mystock.web.listners.helpers.ParameterClientMap;
import ma.mystock.web.listners.helpers.PrivilegesMap;
import ma.mystock.web.utils.annotations.TODO;
import ma.mystock.web.utils.jsf.facelet.Functions;
import ma.mystock.web.utils.routing.UrlsValues;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author abdou
 */
@TODO(value = "il faut changer l'emplacement de synconized")
public class LoadContextListener implements ServletContextListener {

    protected Logger log = Logger.getLogger(LoadContextListener.class);

    @Autowired
    private GenericDAO<VInfMetaModel, String> vInfMetaModelEJB;
    //@Autowired
    //private GenericDAO<VPmAttributeExclud, Long> vPmAttributExcludEJB;
    //@Autowired
    //private GenericDAO<VPmAttributeHidden, String> vPmAttributeHiddenEJB;
    //@Autowired
    //private GenericDAO<VPmAttributeMaxLength, String> vPmAttributeMaxLengthEJB;
    //@Autowired
    //private GenericDAO<VPmAttributeMaxWord, String> vPmAttributeMaxWordEJB;
    //@Autowired
    //private GenericDAO<VPmAttributeReadonly, String> vPmAttributeReadonlyEJB;
    //@Autowired
    //private GenericDAO<VPmAttributeRequired, String> vPmAttributeRequiredEJB;
    @Autowired
    private GenericDAO<VInfBasicParameter, String> vInfBasicParameterEJB;
    @Autowired
    private GenericDAO<VPmModelDto, String> vPmModelDtoEJB;
    @Autowired
    private GenericDAO<VPmGroupsDto, String> vPmGroupsDtoBeanEJB;
    @Autowired
    private GenericDAO<VPmCategorysDto, String> vPmCategorysDtoEJB;
    @Autowired
    private GenericDAO<VPmMenusDto, String> vPmMenusDtoEJB;
    @Autowired
    private GenericDAO<VPmPagesDto, String> vPmPagesDtoEJB;
    @Autowired
    private GenericDAO<VPmPageParameterModule, String> vPmPageParameterModuleEJB;
    @Autowired
    private GenericDAO<VPmPageAttribute, String> vPmPageAttributeEJB;
    @Autowired
    private GenericDAO<VPmValidation, String> vPmAttributeValidationEJB;
    @Autowired
    private GenericDAO<VCltParameterClient, String> vCltParameterClientEJB;
    @Autowired
    private GenericDAO<VInfPrivileges, String> vInfPrivilegesEJB;
    @Autowired
    private GenericDAO<VSmLovs, String> vSmLovsEJB;

    @Autowired
    private GenericServices genericServices;

    /**
     * Parmétrage
     */
    //1 - LoadConfigBasics
    private static final ConfigBasicMap configBasicMap = new ConfigBasicMap();
    private static final String configBasicVar = "config";

    // 2 - LoadParameters TODO ajouter les autres types de paramètres
    private static final ParameterClientMap parameterClientMap = new ParameterClientMap();
    private static final String parameterClientVar = "paramClient";

    //LoadPageParameters
    private static final PageParameterMap pageParameterMap = new PageParameterMap();
    private static final String pageParameterVar = "pageParam";

    // LoadAttributExcluds
    //private static final AttributExcludMap attributExcludMap = new AttributExcludMap();
    //private static final String attributExcludVar = "attrExclud";
    // LoadAttributHidden
    private static final AttributHiddenMap attributHiddenMap = new AttributHiddenMap();
    private static final String attributHiddenVar = "attributeHidden";

    // LoadAttributeMaxLengths
    private static final AttributeMaxLengthMap attributeMaxLengthMap = new AttributeMaxLengthMap();
    private static final String attributeMaxLengthVar = "attributeMaxLength";

    // LoadAttributeMaxWord
    private static final AttributeMaxWordMap attributeMaxWordMap = new AttributeMaxWordMap();
    private static final String attributeMaxWordVar = "attributeMaxWord";

    // LoadAttributeReadInly 
    private static final AttributeReadOnlyMap attributeReadOnlyMap = new AttributeReadOnlyMap();
    private static final String attributeReadOnlyVar = "attributeReadOnly";

    //LoadAttributeRequireds
    private static final AttributeRequiredMap attributeRequiredMap = new AttributeRequiredMap();
    private static final String attributeRequiredVar = "attributeRequired";

    // LoadMetaModels
    private static final MetaModelMap metaModelMap = new MetaModelMap();
    private static final String metaModelVar = "txt";

    // LoadModules
    private static final ModelMap modelMap = new ModelMap();
    private static final String modelVar = "model";

    //LoadPageValidations
    private static final PageValidationMap pageValidationMap = new PageValidationMap();

    //LoadPages
    private static final PageMap pageMap1 = new PageMap();
    private static final String pageVar = "page";

    //LoadPrivileges
    private static final PrivilegesMap privilegesMap = new PrivilegesMap();
    private static final String privilegesVar = "priv";

    // LoadLovs
    private static final LovMap lovsMap = new LovMap();

    // PageMap
    private static final List<VPmPagesDto> pageMap = new ArrayList<>();

    // Globals Attributs 
    private static Map<String, Object> paramQuery = new HashMap<>();

    // Methodes
    public static void constructMaps() {
        configBasicMap.clear();
        parameterClientMap.clear();
        pageParameterMap.clear();
        //attributExcludMap.clear();
        attributHiddenMap.clear();
        attributeMaxLengthMap.clear();
        attributeMaxWordMap.clear();
        attributeReadOnlyMap.clear();
        attributeRequiredMap.clear();
        metaModelMap.clear();
        modelMap.clear();
        pageValidationMap.clear();
        pageMap1.clear();
        privilegesMap.clear();
        lovsMap.clear();
        pageMap.clear();
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        /*
         WebApplicationContextUtils
         .getRequiredWebApplicationContext(sce.getServletContext())
         .getAutowireCapableBeanFactory()
         .autowireBean(this);
         */
        WebApplicationContextUtils.getRequiredWebApplicationContext(sce.getServletContext())
                .getAutowireCapableBeanFactory().autowireBean(this);

        //initEJB();
        showMsg("START");

        //loadAttributsExcluds(sce.getServletContext());
        loadMetaModels(sce.getServletContext());
        System.out.println(" ok ");
        loadAttributsHidden(sce.getServletContext());
        loadAttributeMaxLengths(sce.getServletContext());
        loadAttributeMaxWord(sce.getServletContext());
        loadAttributeReadOnly(sce.getServletContext());
        loadAttributeRequireds(sce.getServletContext());
        loadConfigBasics(sce.getServletContext());
        loadLovs(sce.getServletContext());
        loadModules(sce.getServletContext());
        loadPageParameters(sce.getServletContext());
        loadPageValidations(sce.getServletContext());
        loadPages(sce.getServletContext());
        loadParameterClient(sce.getServletContext());
        loadPrivileges(sce.getServletContext());

        initializeResources(sce.getServletContext());

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        showMsg("Fin");
    }

//public void loadAttributsExcluds(ServletContext sce) {
//
//    List<VPmPageAttributeModule> vPmPageAttributeModules = genericServices.findVPmPageAttributeModuleByIsHidden();
//
//    attributExcludMap.clear();
//    synchronized (attributExcludMap) {
//        for (VPmPageAttributeModule o : vPmPageAttributeModules) {
//            ////log.info("Exclude : " + o.getCode());
//            attributExcludMap.put(o.getCode(), "true");
//        }
//    }
//    if (sce != null) {
//        // faire qlq choses
//    }
//    sce.setAttribute(attributExcludVar, attributExcludMap);
//    log.info("Loading LoadAttributExcluds ... : size (" + attributExcludMap.size() + ")");
//}
    public void loadAttributsHidden(ServletContext sce) {

        List<VPmPageAttributeModule> vPmPageAttributeModules = genericServices.findVPmPageAttributeModuleByIsHidden();

        attributHiddenMap.clear();

        synchronized (attributHiddenMap) {
            for (VPmPageAttributeModule o : vPmPageAttributeModules) {
                log.info("hide : " + o.getCode());
                attributHiddenMap.put(o.getCode(), "true");
            }
        }
        if (sce != null) {
            // faire qlq choses
        }
        sce.setAttribute(attributHiddenVar, attributHiddenMap);
        log.info("Loading loadAttributsHidden ... : size (" + attributHiddenMap.size() + ")");
    }

    public void loadAttributeMaxLengths(ServletContext sce) {

        List<VPmPageAttributeModule> vPmPageAttributeModules = genericServices.findVPmPageAttributeModuleByMaxLenght();

        attributeMaxLengthMap.clear();
        synchronized (attributeMaxLengthMap) {

            for (VPmPageAttributeModule o : vPmPageAttributeModules) {
                log.info("maxlength : " + o.getCode() + " :  " + o.getMaxLenght());
                attributeMaxLengthMap.put(o.getCode(), "" + o.getMaxLenght());
            }

        }
        sce.setAttribute(attributeMaxLengthVar, attributeMaxLengthMap);
        log.info("Loading LoadAttributeMaxLengths ... : size (" + attributeMaxLengthMap.size() + ")");
    }

    public void loadAttributeMaxWord(ServletContext sce) {

        List<VPmPageAttributeModule> vPmPageAttributeModules = genericServices.findVPmPageAttributeModuleByMaxWord();

        attributeMaxWordMap.clear();
        synchronized (attributeMaxWordMap) {

            for (VPmPageAttributeModule o : vPmPageAttributeModules) {
                log.info("maxword : " + o.getCode() + " :  " + o.getMaxWord());
                attributeMaxWordMap.put(o.getCode(), "" + o.getMaxWord());
            }

        }
        sce.setAttribute(attributeMaxWordVar, attributeMaxWordMap);
        log.info("Loading loadAttributeMaxWord ... : size (" + attributeMaxWordMap.size() + ")");
    }

    public void loadAttributeReadOnly(ServletContext sce) {

        List<VPmPageAttributeModule> vPmPageAttributeModules = genericServices.findVPmPageAttributeModuleByIsReadonly();

        attributeReadOnlyMap.clear();
        synchronized (attributeReadOnlyMap) {

            for (VPmPageAttributeModule o : vPmPageAttributeModules) {
                log.info("readOnly : " + o.getCode() + " :  true");
                attributeReadOnlyMap.put(o.getCode(), "true");
            }

        }
        sce.setAttribute(attributeReadOnlyVar, attributeReadOnlyMap);
        log.info("Loading loadAttributeReadOnly ... : size (" + attributeReadOnlyMap.size() + ")");
    }

    public void loadAttributeRequireds(ServletContext sce) {

        List<VPmPageAttributeModule> vPmPageAttributeModules = genericServices.findVPmPageAttributeModuleByIsRequired();

        attributeRequiredMap.clear();
        synchronized (attributeRequiredMap) {

            for (VPmPageAttributeModule o : vPmPageAttributeModules) {
                log.info("attributeRequiredMap : " + o.getCode() + " :  " + o.getInfItemCode());
                attributeRequiredMap.put(o.getCode(), "true");
            }

        }
        sce.setAttribute(attributeRequiredVar, attributeRequiredMap);
        log.info("Loading attributeRequiredMap ... : size (" + attributeRequiredMap.size() + ")");
    }

    public void loadConfigBasics(ServletContext sce) {
        log.info(" statt : contextInitialized + LoadConfigBasics");

        configBasicMap.clear();
        synchronized (configBasicMap) {

            List<VInfBasicParameter> list = vInfBasicParameterEJB.executeQuery(VInfBasicParameter.findAll);
            for (VInfBasicParameter o : list) {
                log.info("config : " + o.getId() + " : " + o.getValue());
                configBasicMap.put(o.getId().toString(), o.getValue());

            }

        }
        sce.setAttribute(configBasicVar, configBasicMap);
        log.info("Loading LoadConfigBasics ... : size (" + configBasicMap.size() + ")");
    }

    public void loadLovs(ServletContext sce) {
        log.info(" statt : contextInitialized + LoadLovs");

        lovsMap.clear();

        /*
         synchronized (lovsMap) {

         lovsMap.clear();

         List<String> keyLovs = vSmLovsEJB.getEm().createQuery(VSmLovs.finAllDistinctTableCode).getResultList();

         List<LovsDTO> listLov;

         for (String key : keyLovs) {

         listLov = new ArrayList<>();

         paramQuery.clear();
         paramQuery.put("tableCode", key);
         List<VSmLovs> list = vSmLovsEJB.executeQuery(VSmLovs.findByTableCode, paramQuery);

         for (VSmLovs o : list) {
         listLov.add(new LovsDTO(o));

         }

         lovsMap.put(key, listLov);
         ////log.info(key + " ---+---+- " + listLov.size());

         }

         }
        
         */
        log.info("Loading LoadLovs ... : size (" + lovsMap.size() + ")");
    }

    public void loadMetaModels(ServletContext sce) {
        System.out.println(" vInfMetaModelEJB vInfMetaModelEJB vInfMetaModelEJB : " + vInfMetaModelEJB);
        metaModelMap.clear();
        synchronized (metaModelMap) {
            //List<VInfMetaModel> list =  infServices.findAllVInfMetaModel();
            List<VInfMetaModel> list = vInfMetaModelEJB.executeQuery(VInfMetaModel.findAll);
            for (VInfMetaModel o : list) {
                log.info("metamodel : " + o.getCode() + " :  " + o.getValue());
                metaModelMap.put(o.getCode(), o.getValue());
            }

        }
        sce.setAttribute(metaModelVar, metaModelMap);
        log.info("Loading MetaModel ... : size (" + metaModelMap.size() + ")");
    }

    public void loadModules(ServletContext sce) {
        try {

            modelMap.clear();
            synchronized (modelMap) {

                ////log.info("Loading Modules ... ");
                // Niveau 1 : Load Modules
                List<VPmModelDto> Modelslist = vPmModelDtoEJB.executeQuery(VPmModelDto.findAll);

                for (VPmModelDto model : Modelslist) {

                    ////log.info("-   Loading Module (" + module.getId() + ") ... ");
                    paramQuery.clear();
                    paramQuery.put("modelId", model.getId());
                    List<VPmGroupsDto> groupslist = vPmGroupsDtoBeanEJB.executeQuery(VPmGroupsDto.findByModelId, paramQuery);

                    for (VPmGroupsDto group : groupslist) {

                        ////log.info("-      Loading Group (" + group.getId() + ") ... ");
                        paramQuery.clear();
                        paramQuery.put("groupId", group.getId());
                        paramQuery.put("modelId", model.getId());
                        List<VPmCategorysDto> categorysList = vPmCategorysDtoEJB.executeQuery(VPmCategorysDto.findByModelIdAndGroupId, paramQuery);
                        for (VPmCategorysDto category : categorysList) {

                            ////log.info("-         Loading Category (" + category.getId() + ") ... ");
                            paramQuery.clear();
                            paramQuery.put("modelId", model.getId());
                            paramQuery.put("groupId", group.getId());
                            paramQuery.put("categoryId", category.getId());

                            List<VPmMenusDto> menusList = vPmMenusDtoEJB.executeQuery(VPmMenusDto.findByModelIdAndGroupIdAndCategoryId, paramQuery);

                            for (VPmMenusDto menu : menusList) {

                                ////log.info("-            Loading Menu (" + category.getId() + ") ... ");
                                paramQuery.put("modelId", model.getId());
                                paramQuery.put("groupId", group.getId());
                                paramQuery.put("categoryId", category.getId());
                                paramQuery.put("menuId", menu.getId());

                                List<VPmPagesDto> pagesList = vPmPagesDtoEJB.executeQuery(VPmPagesDto.findByModelIdAndGroupIdAndCategoryIdAndMenuId, paramQuery);

                                for (VPmPagesDto page : pagesList) {

                                    ////log.info("-                  Loading Page (" + category.getId() + ") ... ");
                                    menu.getvPmPagesDtoList().add(page);

                                    pageMap.add(page);
                                }
                                category.getvPmMenusDtoList().add(menu);
                            }
                            group.getvPmCategorysDtoList().add(category);
                        }
                        model.getvPmGroupsDtoList().add(group);
                    }
                    modelMap.put(model.getId(), model);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        log.info("Loading Modules ... : size (" + modelMap.size() + ")");
    }

    public void loadPageParameters(ServletContext sce) {

        pageParameterMap.clear();
        synchronized (pageParameterMap) {

            List<VPmPageParameterModule> list = vPmPageParameterModuleEJB.executeQuery(VPmPageParameterModule.findAll);
            for (VPmPageParameterModule o : list) {
                log.info("pageParam : " + o.getCode() + " : " + o.getValue());
                pageParameterMap.put(o.getCode(), o.getValue());
            }

        }
        sce.setAttribute(pageParameterVar, pageParameterMap);
        log.info("Loading LoadPageParameters ... : size (" + pageParameterMap.size() + ")");

    }

    public void loadPageValidations(ServletContext sce) {

        pageValidationMap.clear();
        synchronized (pageValidationMap) {

            List<VPmPagesDto> pages = vPmPagesDtoEJB.executeQuery(VPmPagesDto.findAll);

            for (VPmPagesDto o : pages) {

                ItemCodeValidationMap validation = loadPageValidationsFindVPmAttributeValidationByPageId(o.getId());

                pageValidationMap.put(o.getId().toString(), validation);

            }

        }
        log.info("Loading LoadPageValidations ... : size (" + pageValidationMap.size() + ")");
    }

    public ItemCodeValidationMap loadPageValidationsFindVPmAttributeValidationByPageId(Long pageId) {

        ItemCodeValidationMap itemCodeValidationMap = new ItemCodeValidationMap();

        paramQuery.clear();
        paramQuery.put("pageId", pageId);
        List<VPmPageAttribute> vPmPageAttributeList = vPmPageAttributeEJB.executeQuery(VPmPageAttribute.findByPageId, paramQuery);
        log.info("page : " + pageId + " - " + vPmPageAttributeList.size());

        for (VPmPageAttribute o : vPmPageAttributeList) {
            paramQuery.clear();
            paramQuery.put("pageId", pageId);
            paramQuery.put("itemCode", o.getInfItemCode());
            List<VPmValidation> vPmValidationList = vPmAttributeValidationEJB.executeQuery(VPmValidation.findByPageIdAndItemCode, paramQuery);
            itemCodeValidationMap.put(o.getInfItemCode(), vPmValidationList);
            log.info("  itemCode : " + o.getInfItemCode() + " - " + vPmValidationList.size());
        }
        return itemCodeValidationMap;
    }

    public ItemCodeValidationMap loadPageValidationsFindVPmAttributeValidationByPageIdOld(Long pageId) {

        ItemCodeValidationMap mapByEitemCode = new ItemCodeValidationMap();
        paramQuery.clear();
        paramQuery.put("pageId", pageId);
        List<VPmValidation> list = vPmAttributeValidationEJB.executeQuery(VPmValidation.findByPageId, paramQuery); // VPmAttributeValidation.findBlocksFieldValidTypeByBlockId(blockId);      

        System.out.println("loadPageValidationsFindVPmAttributeValidationByPageId : page " + pageId + " - " + list.size());

        String lastEitemCode = null;
        List<VPmValidation> fbBlockFieldValidTypeDTOListByEitemCode = null;

        for (VPmValidation vPmValidation : list) {
            ////log.info(" validate for itemCode : " + vPmValidation.getItemCode() + " : " + vPmValidation.getValidationId() + "(" + vPmValidation.getParams() + ")");
            if (!vPmValidation.getItemCode().equals(lastEitemCode)) {

                if (lastEitemCode != null) {
                    mapByEitemCode.put(lastEitemCode, fbBlockFieldValidTypeDTOListByEitemCode);
                    System.out.println("    loadPageValidationsFindVPmAttributeValidationByPageId -> item " + lastEitemCode + " - " + fbBlockFieldValidTypeDTOListByEitemCode.size());
                    System.out.println(" ");
                    try {
                        //log.info("val : lastEitemCode + " + +fbBlockFieldValidTypeDTOListByEitemCode.size());
                    } catch (Exception ex) {
                        log.info("Exp 1");
                    }

                }
                lastEitemCode = vPmValidation.getItemCode();      // lastEitemCode should be not null
                fbBlockFieldValidTypeDTOListByEitemCode = new ArrayList<>();

            }
            fbBlockFieldValidTypeDTOListByEitemCode.add(vPmValidation);
        }
        // the last one
        if (lastEitemCode != null) {
            mapByEitemCode.put(lastEitemCode, fbBlockFieldValidTypeDTOListByEitemCode);
            try {
                //log.info(".val : lastEitemCode + " + +fbBlockFieldValidTypeDTOListByEitemCode.size());
            } catch (Exception ex) {
                log.info("Exp 2");
            }
        }

        return mapByEitemCode;
    }

    public void loadPages(ServletContext sce) {

        ////log.info("LoadPages 1 : pageMap1");
        pageMap1.clear();
        synchronized (pageMap1) {
            List<VPmPagesDto> list = vPmPagesDtoEJB.executeQuery(VPmPagesDto.findAll);
            for (VPmPagesDto o : list) {
                ////log.info("page : " + o.getId() + " : " + o.getLabel());
                pageMap1.put(o.getId(), o);
            }

        }
        log.info("Loading Pages ... : size (" + pageMap1.size() + ")");
    }

    public void loadParameterClient(ServletContext sce) {
        System.out.println(" loadParameterClient ");

        parameterClientMap.clear();
        synchronized (parameterClientMap) {

            List<VCltParameterClient> list = vCltParameterClientEJB.executeQuery(VCltParameterClient.findAll);
            for (VCltParameterClient o : list) {
                log.info("param client : " + o.getCode() + " : " + o.getValue());
                parameterClientMap.put(o.getCode(), o.getValue());
            }
        }
        sce.setAttribute(parameterClientVar, parameterClientMap);
        log.info("Loading parameter Client ... : size (" + parameterClientMap.size() + ")");
    }

    public void loadPrivileges(ServletContext sce) {

        privilegesMap.clear();
        synchronized (privilegesMap) {
            //List<VInfPrivileges> list =  infServices.findAllVInfPrivileges();

            List<VInfPrivileges> list = vInfPrivilegesEJB.executeQuery(VInfPrivileges.findAll);
            for (VInfPrivileges o : list) {
                ////log.info("priv : " + o.getCode());
                privilegesMap.put(o.getCode(), "true");
            }
        }
        sce.setAttribute(privilegesVar, privilegesMap);
        log.info("Loading Privileges ... : size (" + privilegesMap.size() + ")");

    }

    public static void initializeResources(ServletContext sce) {
        Functions.onInit();
        UrlsValues.onInit();
    }

    // GETTERS & SETTERS
    /*
     public static AttributExcludMap getAttributExcludMap() {
     return attributExcludMap;
     }

     public static String getAttributExcludVar() {
     return attributExcludVar;
     }
     */
    public static AttributeMaxLengthMap getAttributeMaxLengthMap() {
        return attributeMaxLengthMap;
    }

    public static AttributHiddenMap getAttributHiddenMap() {
        return attributHiddenMap;
    }

    public static String getAttributeMaxLengthVar() {
        return attributeMaxLengthVar;
    }

    public static AttributeRequiredMap getAttributeRequiredMap() {
        return attributeRequiredMap;
    }

    public static String getAttributeRequiredVar() {
        return attributeRequiredVar;
    }

    public static ConfigBasicMap getConfigBasicMap() {
        return configBasicMap;
    }

    public static String getConfigBasicVar() {
        return configBasicVar;
    }

    public static LovMap getLovsMap() {
        return lovsMap;
    }

    public static MetaModelMap getMetaModelMap() {
        return metaModelMap;
    }

    public static String getMetaModelVar() {
        return metaModelVar;
    }

    public static ModelMap getModelMap() {
        return modelMap;
    }

    public static String getModelVar() {
        return modelVar;
    }

    public static List<VPmPagesDto> getPageMap() {
        return pageMap;
    }

    public static PageParameterMap getPageParameterMap() {
        return pageParameterMap;
    }

    public static String getPageParameterVar() {
        return pageParameterVar;
    }

    public static PageValidationMap getPageValidationMap() {
        return pageValidationMap;
    }

    public static PageMap getPageMap1() {
        return pageMap1;
    }

    public static String getPageVar() {
        return pageVar;
    }

    public static PrivilegesMap getPrivilegesMap() {
        return privilegesMap;
    }

    public static String getPrivilegesVar() {
        return privilegesVar;
    }

    public static ParameterClientMap getParameterClientMap() {
        return parameterClientMap;
    }

    public static String getParameterClientVar() {
        return parameterClientVar;
    }

    public void showMsg(String a) {
        log.info("                          **************************** ");
        log.info("              ********************************************************");
        log.info("   ***********************************************************************************");
        log.info("****************************************" + a + "****************************************************");
        log.info("   ***********************************************************************************");
        log.info("              ********************************************************");
        log.info("                          **************************** ");

    }

}
