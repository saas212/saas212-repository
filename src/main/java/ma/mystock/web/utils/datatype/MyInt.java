/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.datatype;

/**
 *
 * @author abdou
 */
public class MyInt {

    public static int toInt(String str) {
        int i = 0;
        try {
            i = Integer.valueOf(str);
        } catch (Exception ex) {
            i = 0;
        }
        return i;
    }

}
