/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.datatype;

import java.util.List;

/**
 *
 * @author abdou
 */
public class MyList {

    public static boolean isEmpty(List l) {
        boolean resultat = false;
        if (l == null) {
            resultat = true;
        } else if (l.isEmpty()) {
            resultat = true;
        }
        return resultat;
    }

    public static boolean isNotEmpty(List l) {
        return !(isEmpty(l));
    }

}
