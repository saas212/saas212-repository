/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.datatype;

/**
 *
 * @author abdou
 */
public class MyLong {

    public static Long toLong(String str) {
        if (MyIs.isLong(str)) {
            return Long.valueOf(str);
        }
        return null;
    }

    public static Long toLong(Object str) {
        if (MyIs.isLong(str)) {
            return Long.valueOf(str.toString());
        }
        return null;
    }

    public static Long toLong(int str) {
        if (MyIs.isEmpty(str)) {
            return Long.valueOf(str);
        }
        return null;
    }

    public static String toString(Long str) {
        String res = "";

        try {
            res = str.toString();
        } catch (Exception ex) {
            res = "";
        }

        return res;
    }

}
