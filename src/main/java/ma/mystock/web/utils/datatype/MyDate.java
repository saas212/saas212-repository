/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.datatype;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import ma.mystock.web.utils.globals.MyAttribute;

/**
 *
 * @author abdou
 */
public class MyDate {

    public static Date getSysDate() {
        return new Date();
    }

    public static String formatDate(Date d, String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(d);
    }

    public static String formatDateTime(Date d, String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(d);
    }

    public static String toString(Date d) {

        try {
            return d.toString();
        } catch (Exception ex) {
            return "";
        }

    }

    public static Date toDate(String str) {
        return MyString.stringToDate(str);

    }

    public Timestamp getSysTimestamp() {
        return new Timestamp(System.currentTimeMillis());
    }

    public static Timestamp getSysTimestamp(String format) {
        Timestamp currentDate = null;
        DateFormat dateFormat = new SimpleDateFormat(format);
        Date cDate = new Date();
        try {
            currentDate = stringToTimestamp(dateFormat.format(cDate));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return currentDate;
    }

    public static String timestampToString(Timestamp tDate) throws Exception {
        if (tDate != null) {
            Date d = new Date();
            d.setTime(tDate.getTime());
            DateFormat dateFormat = new SimpleDateFormat(MyAttribute.DATE_FORMAT);
            return dateFormat.format(d);
        } else {
            return "";
        }
    }

    public static Date timestampToDate(Timestamp tDate) throws Exception {
        return MyString.stringToDate(MyDate.timestampToString(tDate));
    }

    public static Timestamp stringToTimestamp(String sDate) throws Exception {
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(MyAttribute.DATE_FORMAT);
        try {
            if (sDate != null) {
                if (!sDate.equals("")) {
                    d = sdf.parse(sDate);
                    Timestamp myTimestamp = new Timestamp(d.getTime());
                    return myTimestamp;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            // e.printStackTrace();
            return null;
        }
    }

    public static String timestampToStringByFormat(Timestamp tDate, String pattern) throws Exception {
        if (tDate != null) {
            Date d = new Date();
            d.setTime(tDate.getTime());
            DateFormat dateFormat = new SimpleDateFormat(pattern);
            return dateFormat.format(d);
        } else {
            return "";
        }
    }

}
