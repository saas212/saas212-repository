/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.datatype;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.regex.Pattern;
import ma.mystock.web.utils.globals.MyAttribute;

/**
 *
 * @author abdou
 */
public class MyString {

    public static String trim(String str) {
        String result = "";
        if (!"".equals(str)) {
            result = str.trim();
        }
        return result;
    }

    public static String toString(Object o) {
        String val = "";

        if (o != null) {

            try {
                val = String.valueOf(o);
            } catch (Exception ex) {
            }

        }
        return val;
    }

    public static String toStringNoSense(String str) {
        if (MyIs.isNotEmpty(str)) {
            return str;
        } else {
            return null;
        }
    }

    public static String getRandom(int length) {
        final String charset = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFJHIJKLMNOPQRSTUVWXYZ";
        Random rand = new Random(System.currentTimeMillis());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < length; i++) {
            int pos = rand.nextInt(charset.length());
            sb.append(charset.charAt(pos));
        }

        return sb.toString();
    }

    public static Long stringToLong(String str) {
        if ((str == null) || (str.length() == 0)) {
            return null;
        }
        try {
            Long newLong = Long.parseLong(str);
            return newLong;
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    public static Double stringToDouble(String str) {
        if ((str == null) || (str.length() == 0)) {
            return null;
        }
        try {
            Double newDouble = Double.parseDouble(str);
            return newDouble;
        } catch (NumberFormatException ex) {
            return null;
        }

    }

    public static Date stringToDate(String sDate) {
        Date d = null;
        SimpleDateFormat sdf = new SimpleDateFormat(MyAttribute.DATE_TIME_FORMAT);
        try {
            d = sdf.parse(sDate);
        } catch (Exception ex) {
        }
        return d;
    }

    public static String removePreview(String in) {
        return in.replaceAll("\\s*\\bpreview\\.\\b\\s*", "");
    }

    public static String addPreview(String in) {
        String prefix = in.split("\\.")[0];
        String sansPrefix = in.replaceAll("\\s*\\b" + prefix + "\\b\\s*", "");
        return prefix + ".preview" + sansPrefix;
    }

    public static String removeCharAt(String s, int i) {
        StringBuffer buf = new StringBuffer(s.length() - 1);
        buf.append(s.substring(0, i)).append(s.substring(i + 1));
        return buf.toString();
    }

    public static String removeChar(String s, char c) {
        StringBuffer buf = new StringBuffer(s.length());
        buf.setLength(s.length());
        int current = 0;
        for (int i = 0; i < s.length(); i++) {
            char cur = s.charAt(i);
            if (cur != c) {
                buf.setCharAt(current++, cur);
            }
        }
        return buf.toString();
    }

    public static String replaceCharAt(String s, int i, char c) {
        StringBuffer buf = new StringBuffer(s);
        buf.setCharAt(i, c);
        return buf.toString();
    }

    public static String deleteAllNonDigit(String s) {
        String temp = s.replaceAll("\\D", "");
        return temp;
    }

    public static String repalceAllChar(String s, String f, String r) {
        String temp = s.replace(f, r);
        return temp;
    }

    public static String removeWord(String unwanted, String sentence) {
        StringTokenizer st = new StringTokenizer(sentence);
        String remainder = "";

        while (st.hasMoreTokens()) {
            String temp = st.nextToken();

            if (!temp.equals(unwanted)) {
                remainder += temp + " ";
            }
        }

        return remainder.trim();
    }

    public static String getPropertyByItemCode(String itemCode) {

        itemCode = itemCode.replace('.', '_');
        return itemCode.split("_")[itemCode.split("_").length - 1];

    }

    public static boolean like(String str, String expr) {

        // on peu utuliser  ; log.info("abc".contains("A"));
        expr = expr.replace(".", "\\."); // "\\" is escaped to "\" (thanks, Alan M)
        // ... escape any other potentially problematic Strings here
        expr = expr.replace("?", ".");
        expr = expr.replace("%", ".*");

        //str = str.toLowerCase(); // ignoring locale for now
        //expr = expr.toLowerCase(); // ignoring locale for now
        return str.matches(expr);
    }

    public static boolean likeIgnoreCase(String str, String expr) {

        expr = expr.replace(".", "\\."); // "\\" is escaped to "\" (thanks, Alan M)
        // ... escape any other potentially problematic Strings here
        expr = expr.replace("?", ".");
        expr = expr.replace("%", ".*");

        str = str.toLowerCase(); // ignoring locale for now
        expr = expr.toLowerCase(); // ignoring locale for now

        return str.matches(expr);
    }

    public static String upperFistLetter(String str) {
        return "";
    }

    public static String lowerFistLetter(String str) {
        return "";
    }

    public static String upper(String str) {
        if (MyIs.isNotEmpty(str)) {
            return str.toUpperCase();
        }
        return str;
    }

    public static String lower(String str) {
        if (MyIs.isNotEmpty(str)) {
            return str.toLowerCase();
        }
        return str;
    }

    public static boolean isEmpty(String o) {

        if ("".equals(o) || o == null) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean validateEmail(String value) {
        boolean valid = true;
        Pattern pattern;

        pattern = Pattern
                .compile("^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$");
        boolean res = pattern.matcher(value).matches();
        if (!res) {

            return res;
        } else {
            return valid;
        }

    }

    // format Number(8,2)
    public static boolean validateDecimal(String value) {
        String patternStr = "[0-9]+(\\.[0-9][0-9]?)?";
        Pattern pattern = Pattern.compile(patternStr);
        boolean valid = pattern.matcher(value).matches();
        return valid;
    }

}
