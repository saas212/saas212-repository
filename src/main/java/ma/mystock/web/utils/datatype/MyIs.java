/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.datatype;

import java.util.regex.Pattern;
import ma.mystock.web.utils.MyParent;

/**
 *
 * @author abdou
 */
public class MyIs extends MyParent {

    public static boolean isNull(Object o) {
        return o == null;
    }

    public static boolean isNotNull(Object o) {
        return !isNull(o);
    }

    public static boolean isLong(String value) {
        try {
            Long.parseLong(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isLong(Object value) {
        try {
            Long.parseLong(value.toString());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isEmpty(String s) {
        boolean empty = false;
        if (s == null || "".equals(s)) {
            empty = true;
        }
        return empty;
    }

    public static boolean isEmpty(String... s) {

        boolean empty = true;

        for (int i = 0; i < s.length; i++) {

            //log.info("===> : " + s[i]);
            if (isNotEmpty(s[i])) {
                empty = false;
                break;
            }
        }

        return empty;
    }

    public static boolean isNotEmpty(String... s) {
        boolean empty = true;

        for (int i = 0; i < s.length; i++) {

            log.info("===> : " + s[i]);

            if (isEmpty(s[i])) {
                empty = false;
                break;
            }
        }

        return empty;
    }

    public static boolean isNotEmpty(String s) {
        return !isEmpty(s);
    }

    public static boolean isEmpty(Object o) {
        return (o == null);
    }

    public static boolean isMail(String value) {
        boolean valid = true;
        Pattern pattern;

        pattern = Pattern.compile("^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$");
        boolean res = pattern.matcher(value).matches();
        if (!res) {

            return res;
        } else {
            return valid;
        }

    }

    public static boolean isDecimal(String value) {
        String patternStr = "[0-9]+(\\.[0-9][0-9]?)?";
        Pattern pattern = Pattern.compile(patternStr);
        boolean valid = pattern.matcher(value).matches();
        return valid;
    }

    public static boolean isNumeric(String str) {
        return str.matches("-?\\d+(\\.\\d+)?");
    }

    public static boolean isDistinct(String s1, String s2) {
        if (isEmpty(s1) && isEmpty(s2)) {
            return true;
        } else if (!isEmpty(s1) && !isEmpty(s2)) {
            return !s1.equalsIgnoreCase(s2);
        } else {
            return true;
        }

    }

    public static boolean isDouble(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
