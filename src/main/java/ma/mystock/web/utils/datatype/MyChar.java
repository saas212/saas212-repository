/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.datatype;

/**
 *
 * @author abdou
 */
public class MyChar {

    public static String charSpecToString(String x) {
        String a, b, c, d, e, f, m, n, o, p, q, l, h, g;

        a = String.valueOf(x.replace("’", "'"));
        b = String.valueOf(a.replace("…", "..."));
        c = String.valueOf(b.replace("ˆ", "^"));
        d = String.valueOf(c.replace("‹", "<"));
        e = String.valueOf(d.replace("›", ">"));
        f = String.valueOf(e.replace("›", ">"));
        m = String.valueOf(f.replace("–", "-"));
        n = String.valueOf(m.replace("’", "'"));
        o = String.valueOf(n.replace("‘", "'"));
        p = String.valueOf(o.replace("‚", ","));
        q = String.valueOf(p.replace("“", "'" + "'"));
        l = String.valueOf(q.replace("”", "'" + "'"));
        h = String.valueOf(l.replace("Œ", "Œ"));
        g = String.valueOf(h.replace("œ", "œ"));

        return g;

    }

}
