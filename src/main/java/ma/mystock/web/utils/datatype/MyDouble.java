/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.datatype;

import java.text.DecimalFormat;

/**
 *
 * @author abdou
 */
public class MyDouble {

    public static Double toDouble(String str) {

        if (MyIs.isDouble(str)) {
            return Double.valueOf(str);
        }
        return null;
    }

    public static Double toDouble(int str) {
        if (MyIs.isEmpty(str)) {
            return (double) str;
        }
        return null;
    }

    public static String toString(Double d) {
        String res = "";
        try {
            res = d.toString();
        } catch (Exception ex) {
            res = "";
        }

        return res;
    }

    public static String format(Double d) {

        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(d);

    }

}
