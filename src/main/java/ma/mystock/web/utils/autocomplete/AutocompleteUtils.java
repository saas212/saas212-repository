/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.autocomplete;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import ma.mystock.core.dao.entities.views.VSmProduct;
import ma.mystock.web.utils.datatype.MyString;
import ma.mystock.web.utils.springBeans.GloablsServices;
import ma.mystock.web.utils.sessions.SessionsGetter;
import org.primefaces.json.JSONObject;

/**
 *
 * @author Abdessamad HALLAL
 */
public class AutocompleteUtils extends GloablsServices {

    //################## FiltedBy Model Id is Included ##################//
    public static Map<String, Object> paramQuery = new HashMap<>();

    public static JSONObject findVSmProductLikeDesignationAndCltModuleId(String term, Long cltModuleId) {
        paramQuery.clear();
        paramQuery.put("designation", "%" + MyString.lower(term) + "%");
        paramQuery.put("cltModuleId", cltModuleId);

        List<VSmProduct> vSmProductList = vSmProductEJB.executeQuery(VSmProduct.findLikeDesignationAndCltModuleId, paramQuery);
        System.out.println(" => vSmProductList " + vSmProductList.size() + " - " + "%" + MyString.lower(term) + "%");
        JSONObject object = new JSONObject();

        for (VSmProduct o : vSmProductList) {
            JSONObject sub = new JSONObject();
            try {
                sub.put("value", o.getId());
                sub.put("label", o.getDesignation());
                object.put("" + o.getId(), sub);
            } catch (Exception e) {
            }
        }
        return object;
    }

    public static JSONObject findVSmProductLikeReferenceAndCltModuleId(String term, Long cltModuleId) {
        paramQuery.clear();
        paramQuery.put("reference", "%" + MyString.lower(term) + "%");
        paramQuery.put("cltModuleId", cltModuleId);
        List<VSmProduct> vSmProductList = vSmProductEJB.executeQuery(VSmProduct.findLikeReferenceAndCltModuleId, paramQuery);
        JSONObject object = new JSONObject();

        for (VSmProduct o : vSmProductList) {
            JSONObject sub = new JSONObject();
            try {
                sub.put("value", o.getId());
                sub.put("label", o.getReference());
                object.put("" + o.getId(), sub);
            } catch (Exception e) {
            }
        }
        return object;
    }

}
