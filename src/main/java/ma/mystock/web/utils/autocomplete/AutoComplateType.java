package ma.mystock.web.utils.autocomplete;

/**
 *
 * @author Abdessamad HALLAL
 */
public class AutoComplateType {

    public static final String vSmProductLikeDesignation = "vSmProductLikeDesignation";

    public static final String vSmProductLikeReference = "vSmProductLikeReference";

}
