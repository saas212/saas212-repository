/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.generators;

import java.util.Random;

/**
 *
 * @author anasshajami
 */
public class RandomGenerator {

    public static int randomInt(int rang1, int rang2) {
        Random rand = new Random();
        return rand.nextInt(rang2) + rang1;
    }
}
