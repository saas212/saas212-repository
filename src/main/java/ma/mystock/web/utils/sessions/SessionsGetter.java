package ma.mystock.web.utils.sessions;

import ma.mystock.core.dao.entities.CltModule;
import ma.mystock.core.dao.entities.PmModel;
import ma.mystock.core.dao.entities.views.VCltClient;
import ma.mystock.core.dao.entities.views.VCltClientDetails;
import ma.mystock.core.dao.entities.views.VCltClientLanguage;
import ma.mystock.core.dao.entities.views.VCltUserDetails;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.web.utils.globals.GlobalsAttributs;
import ma.mystock.web.utils.values.MapsGetter;

/**
 *
 * @author abdou
 *
 */
// GET VALUE FORM V_SESSION in SESSION OR DEFALUT VALUE in TABLE parrameter from V_Parameters 
public class SessionsGetter {

    // Globals statics 
    public static String V_CLT_USER_DEAILS = "V_CLT_USER_DEAILS";
    public static String CLT_USER_ID = "CLT_USER_ID";
    public static String CLT_USER_FULL_NAME = "CLT_USER_FULL_NAME";
    public static String CLT_USER_USERNAME = "CLT_USER_USERNAME";

    public static String CLT_MODULE = "CLT_MODULE";
    public static String CLT_MODULE_ID = "CLT_MODULE_ID";

    public static String PM_MODEL = "PM_MODEL";
    public static String PM_MODEL_ID = "PM_MODEL_ID";

    public static String V_CLT_CLIENT_LANGUAGE = "V_CLT_CLIENT_LANGUAGE";

    public static String INF_LANGUAGE = "INF_LANGUAGE";
    public static String INF_LANGUAGE_ID = "INF_LANGUAGE_ID";
    public static String INF_LANGUAGE_CODE = "INF_LANGUAGE_CODE";

    public static String V_CLT_CLIENT_DETAILS = "V_CLT_CLIENT_DETAILS";
    public static String CLT_CLIENT_ID = "CLT_CLIENT_ID";
    public static String V_CLT_CLIENT = "V_CLT_CLIENT";
    public static String CLT_MODULE_TYPE_ID = "CLT_MODULE_TYPE_ID";
    public static String CLT_CLIENT_CODE = "CLINET_CODE";

    public static String INF_PREFIX = "INF_PREFIX";
    public static String INF_PREFIX_ID = "INF_PREFIX_ID";
    public static String INF_PREFIX_CODE = "INF_PREFIX_CODE";

    public static String IS_CONNECTED = "IS_CONNECTED";

    public static String IS_WEB_MASTER = "IS_WEB_MASTER";

    public static String IS_SUPER_ADMIN = "IS_SUPER_ADMIN";

    public static String PARENT_MODULE_ID = "PARENT_MODULE_ID";

    public static String MODULE_ID_TO_MANAGER = "MODULE_ID_TO_MANAGER";

    public static String CLT_USER_IMAGE_PATH = "CLT_USER_IMAGE_PATH";

    // ################ :  V_CLT_USER_DEAILS & CLT_USER_ID
    public static VCltUserDetails getVCltUserDetails() {
        return (VCltUserDetails) SessionsUtils.get(V_CLT_USER_DEAILS);
    }

    public static void setVCltUserDetails(VCltUserDetails obj) {
        if (obj != null) {

            setCltUserFullName(obj.getFirstName() + ", " + obj.getLastName());
            setCltUserUsername(obj.getUsername());
            setCltUserId(obj.getId());
            setCltUserImagePath(obj.getImagePath());
            setIsConnected(true);

            if (GlobalsAttributs.CLT_USER_CATEGORY_WEB_MASTER_ID.equals(obj.getCategoryId())) {
                SessionsGetter.setIsWebMaster(true);
            } else if (GlobalsAttributs.CLT_USER_CATEGORY_SUPER_ADMIN_ID.equals(obj.getCategoryId())) {
                SessionsGetter.setIsSuperAdmin(true);
            } else if (GlobalsAttributs.CLT_USER_CATEGORY_ADMIN_ID.equals(obj.getCategoryId())) {

            } else if (GlobalsAttributs.CLT_USER_CATEGORY_STOCK_ID.equals(obj.getCategoryId())) {

            }

            SessionsUtils.set(V_CLT_USER_DEAILS, obj);
        }
    }

    public static String getCltUserFullName() {
        return SessionsUtils.getString(CLT_USER_FULL_NAME);
    }

    public static void setCltUserFullName(String value) {
        SessionsUtils.set(CLT_USER_FULL_NAME, value);
    }

    public static String getCltUserUsername() {
        return SessionsUtils.getString(CLT_USER_USERNAME);
    }

    public static void setCltUserUsername(String value) {
        SessionsUtils.set(CLT_USER_USERNAME, value);
    }

    public static Long getCltUserId() {
        return SessionsUtils.getLong(CLT_USER_ID);
    }

    public static void setCltUserId(Long id) {
        SessionsUtils.set(CLT_USER_ID, id);
    }

    // ################ CLT_MODEL ################
    public static Long getPmModelId() {
        return SessionsUtils.getLong(PM_MODEL_ID);
    }

    public static void setPmModelId(Long id) {
        SessionsUtils.set(PM_MODEL_ID, id);
    }

    public static PmModel getPmModel() {
        return (PmModel) SessionsUtils.get(PM_MODEL);
    }

    public static void setPmModel(PmModel pmModel) {
        setPmModelId(pmModel.getId());
        SessionsUtils.set(PM_MODEL, pmModel);
    }

    // ################ :  V_CLT_MODULE & V_CLT_MODULE_ID
    public static CltModule getCltModule() {
        return (CltModule) SessionsUtils.get(CLT_MODULE);
    }

    public static void setCltModule(CltModule obj) {
        if (obj != null) {
            setCltModuleId(obj.getId());
            setCltModuleTypeId(obj.getModuleTypeId());
            SessionsUtils.set(CLT_MODULE, obj);
        }
    }

    public static Long getCltParentModuleId() {
        return SessionsUtils.getLong(PARENT_MODULE_ID);
    }

    public static void setCltParentModuleId(Long id) {
        SessionsUtils.set(PARENT_MODULE_ID, id);
    }

    public static Long getCltModuleIdToManager() {
        Long id = SessionsUtils.getLong(MODULE_ID_TO_MANAGER);
        return id;
    }

    public static void setCltModuleIdToManager(Long id) {
        SessionsUtils.set(MODULE_ID_TO_MANAGER, id);
    }

    public static Long getCltModuleId() {
        return SessionsUtils.getLong(CLT_MODULE_ID);
    }

    public static void setCltModuleId(Long id) {
        SessionsUtils.set(CLT_MODULE_ID, id);
    }

    // ################ :  V_CLT_CLIENT_LANGUAGE VCltClientLanguage
    public static VCltClientLanguage getVCltClientLanguage() {
        return (VCltClientLanguage) SessionsUtils.get(V_CLT_CLIENT_LANGUAGE);
    }

    public static void setVCltClientLanguage(VCltClientLanguage obj) {

        if (obj != null) {

            setInfLanguageId(obj.getInfLanguageId());
            setInfLanguageCode(obj.getInfLanguageCode());
            setInfPrefixCode(obj.getInfPrefixCode());
            setInfPrefixId(obj.getInfPrefixId());

            SessionsUtils.set(V_CLT_CLIENT_LANGUAGE, obj);
        }

    }

    // ################ :  V_CLT_CLIENT_LANGUAGE VCltClientLanguage
    public static VCltClient getVCltClient() {
        return (VCltClient) SessionsUtils.get(V_CLT_CLIENT);
    }

    public static void setVCltClient(VCltClient obj) {

        if (obj != null) {

            SessionsUtils.set(V_CLT_CLIENT, obj);
        }

    }

    // ################ :  INF_LANGUAGE & INF_LANGUAGE_ID & INF_LANGUAGE_CODE
    public static String getInfLanguageCode() {

        if (!MyIs.isEmpty(SessionsUtils.getString(INF_LANGUAGE_CODE))) {
            return SessionsUtils.getString(INF_LANGUAGE_CODE);
        } else {
            return MapsGetter.getConfigBasicMap().get("4");
        }

    }

    public static void setInfLanguageCode(String code) {
        SessionsUtils.set(INF_LANGUAGE_CODE, code);
    }

    public static Long getInfLanguageId() {
        return SessionsUtils.getLong(INF_LANGUAGE_ID);
    }

    public static void setInfLanguageId(Long id) {
        SessionsUtils.set(INF_LANGUAGE_CODE, id);
    }

    // ################ :  V_CLT_CLIENT_DETAILS & CLT_CLIENT_ID
    public static VCltClientDetails getVCltClientDetails() {
        return (VCltClientDetails) SessionsUtils.get(V_CLT_CLIENT_DETAILS);
    }

    public static void setVCltClientDetails(VCltClientDetails obj) {
        if (obj != null) {
            setCltClientId(obj.getId());
            SessionsUtils.set(V_CLT_CLIENT_DETAILS, obj);
        }
    }

    public static String getCltClientCode() {
        return SessionsUtils.getString(CLT_CLIENT_CODE);
    }

    public static Long getCltClientId() {
        return SessionsUtils.getLong(CLT_CLIENT_ID);
    }

    public static void setCltClientId(Long id) {
        SessionsUtils.set(CLT_CLIENT_ID, id);
    }

    public static Long getCltModuleTypeId() {
        return SessionsUtils.getLong(CLT_MODULE_TYPE_ID);
    }

    public static void setCltModuleTypeId(Long id) {
        SessionsUtils.set(CLT_MODULE_TYPE_ID, id);
    }

    // ################ :  INF_PREFIX & INF_PREFIX_ID & INF_PREFIX_CODE
    public static String getInfPrefixCode() {
        return SessionsUtils.getString(INF_PREFIX_CODE);
    }

    public static void setInfPrefixCode(String code) {
        SessionsUtils.set(INF_PREFIX_CODE, code);
    }

    public static Long getInfPrefixId() {
        return SessionsUtils.getLong(INF_PREFIX_ID);
    }

    public static void setInfPrefixId(Long id) {
        SessionsUtils.set(INF_PREFIX_ID, id);
    }

    /*
     ################ General ################
     */
    public static boolean getIsConnected() {

        if (!MyIs.isEmpty(SessionsUtils.getBoolean(IS_CONNECTED))) {
            return SessionsUtils.getBoolean(IS_CONNECTED);
        } else {
            return false;
        }
    }

    public static void setIsConnected(boolean val) {
        SessionsUtils.set(IS_CONNECTED, val);
    }

    public static boolean getIsWebMaster() {

        if (!MyIs.isEmpty(SessionsUtils.getBoolean(IS_WEB_MASTER))) {
            return SessionsUtils.getBoolean(IS_WEB_MASTER);
        } else {
            return false;
        }
    }

    public static void setIsWebMaster(boolean val) {
        SessionsUtils.set(IS_WEB_MASTER, val);
    }

    public static boolean getIsSuperAdmin() {

        if (!MyIs.isEmpty(SessionsUtils.getBoolean(IS_SUPER_ADMIN))) {
            return SessionsUtils.getBoolean(IS_SUPER_ADMIN);
        } else {
            return false;
        }
    }

    public static void setIsSuperAdmin(boolean val) {
        SessionsUtils.set(IS_SUPER_ADMIN, val);
    }

    public static String getCltUserImagePath() {
        return SessionsUtils.getString(CLT_USER_IMAGE_PATH);

    }

    public static void setCltUserImagePath(String code) {
        SessionsUtils.set(CLT_USER_IMAGE_PATH, code);
    }

}
