/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.sessions;

import ma.mystock.web.utils.datatype.MyIs;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author abdou to be renomed to SessionsUtils send ti bapckage sessions
 */
public class SessionsUtils {

    /*
     La liste des attributs static
     */
    public static HttpSession getSession() {
        return (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
    }

    public static void set(String name, Object value) {
        try {
            getSession().setAttribute(name, value);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static String getString(String name) {
        String sessionParam = "";
        try {
            if (!MyIs.isEmpty(get(name))) {

                sessionParam = (String) getSession().getAttribute(name);

            }
        } catch (Exception exx) {

        }
        return sessionParam;
    }

    public static Object get(String name) {
        Object sessionParam = null;
        try {
            if (getSession().getAttribute(name) != null) {

                sessionParam = getSession().getAttribute(name);

            }
        } catch (Exception exx) {

        }
        return sessionParam;
    }

    public static Long getLong(String name) {
        Long value = null;
        try {
            if (!MyIs.isEmpty(get(name))) {

                value = (Long) get(name);

            }
        } catch (Exception exx) {

        }
        return value;
    }

    public static boolean getBoolean(String name) {

        boolean value = false;

        try {
            if (!MyIs.isEmpty(get(name))) {

                value = (Boolean) get(name);

            }
        } catch (Exception exx) {

        }
        return value;
    }

}
