package ma.mystock.web.utils.jsf;

import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

public class MessageContext {

    private static MessageContext instance;

    private MessageContext() {

    }

    public static MessageContext getInstance() {
        if (instance == null) {
            instance = new MessageContext();
        }
        return instance;
    }

    public void addMessage(Severity severity, String clientId, String metaKey, ResourceBundle bundle, Throwable t) {
        if (bundle == null) {
            bundle = FacesContext.getCurrentInstance().getApplication().getResourceBundle(FacesContext.getCurrentInstance(), "text");
        }
        String client = FacesUtils.findComponent(clientId).getClientId();
        String severityMsg = "";
        if (FacesMessage.SEVERITY_INFO.equals(severity)) {
            severityMsg = "Info : ";
        } else if (FacesMessage.SEVERITY_WARN.equals(severity)) {
            severityMsg = "Avertissement : ";
        } else if (FacesMessage.SEVERITY_ERROR.equals(severity)) {
            severityMsg = "Erreur : ";
        } else if (FacesMessage.SEVERITY_FATAL.equals(severity)) {
            severityMsg = "Fatale : ";
        }

        String displayMsg = "";
        if (metaKey != null) {
            displayMsg = bundle.getString(metaKey);
        }
        if (t != null) {
            if (metaKey != null) {
                displayMsg += " à cause de ";
            }
            displayMsg += t.getMessage();
        }
        FacesContext.getCurrentInstance().addMessage(client, new FacesMessage(severity, severityMsg, displayMsg));
    }

    public void addMessage(Severity severity, String clientId, String metaKey, ResourceBundle bundle) {
        addMessage(severity, clientId, metaKey, bundle, null);
    }

    public void addMessage(Severity severity, String clientId, String prefix, String suffix, ResourceBundle bundle) {
        String metamodel = prefix + clientId + suffix;
        addMessage(severity, clientId, metamodel, bundle);
    }

    public void addMessage(Severity severity, String clientId, String prefix, String suffix, ResourceBundle bundle, Throwable t) {
        String metamodel = prefix + clientId + suffix;
        addMessage(severity, clientId, metamodel, bundle, t);
    }
}
