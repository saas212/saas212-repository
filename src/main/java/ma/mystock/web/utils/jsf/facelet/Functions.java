package ma.mystock.web.utils.jsf.facelet;

import java.util.Date;
import ma.mystock.web.utils.datatype.MyDate;
import ma.mystock.web.utils.datatype.MyIs;
import ma.mystock.web.utils.MyItemCode;
import ma.mystock.web.utils.sessions.SessionsUtils;
import ma.mystock.module.pageManagement.messages.MessageDetail;
import ma.mystock.module.pageManagement.messages.MessageList;
import ma.mystock.web.utils.annotations.TODO;
import ma.mystock.web.utils.annotations.TODOs;
import ma.mystock.web.utils.datatype.MyDouble;
import ma.mystock.web.utils.functionUtils.params.ModuleParameterUtils;
import ma.mystock.web.utils.sessions.SessionsGetter;
import ma.mystock.web.utils.values.MapsGetter;

/**
 *
 * @author abdou
 */
@TODOs({
    @TODO(value = "Important ", level = TODO.Level.TRIVIAL)})
public class Functions {

    public static String hostAndContext;

    public static void onInit() {
        Functions.hostAndContext = MapsGetter.getConfigBasicMap().get("1");
    }

    /*
     * La Gestion des items codes (metaMode)
     */
    public String getTxtValue(String key) {
        return MapsGetter.getMetaModelMap().getTxtValue(key);
    }

    public static String getMessage(String itemCode, String msg) {
        if (MyIs.isEmpty(msg)) {
            return itemCode + ".message";
        } else {
            return msg;
        }
    }

    public static String getLabel(String itemCode, String label) {
        if (MyIs.isEmpty(label)) {
            return itemCode + ".label";
        } else {
            return label;
        }
    }

    public static String getHelp(String itemCode, String help) {
        if (MyIs.isEmpty(help)) {
            return itemCode + ".help";
        } else {
            return help;
        }
    }

    // Generate Class by ItemCode
    public static String getStyleClass(String itemCode, String styleClass) {

        String classe = MyItemCode.getName(itemCode) + "Class";
        if (MyIs.isEmpty(styleClass)) {
            return classe;
        } else {
            return styleClass + " " + classe;
        }

    }

    //Generate Id by ItemCode
    public static String getId(String itemCode, String id) {

        String newId = MyItemCode.getName(itemCode) + "Id";

        if (MyIs.isEmpty(id)) {
            return newId;
        } else {
            return id;
        }

    }

    // Vérifier est ce ce composant est valide ou non pour rendre le style
    public static String getValidStyle(String itemCode) {

        String resultat = "inputSimple";

        MessageList list = (MessageList) SessionsUtils.get("getErrorMessages");

        if (list != null) {

            for (MessageDetail item : list) {
                if (item.getItemCode().equalsIgnoreCase(itemCode)) {
                    resultat = "notvalid";
                    break;
                }
            }
        }

        /// defalut style border_input si on a pas une submit 
        return resultat;

    }

    /*
     * La Gestion des templates
     */
    public static String getContextPath() {
        return hostAndContext;
    }

    public static String getClientCode() {
        return SessionsGetter.getCltClientCode();
    }

    public static String getFramework() {
        return hostAndContext + "/templates/framework";
    }

    /**
     * Normalement on va getter le style selon le params de client
     *
     * @return
     */
    public static String getStyle() {
        return hostAndContext + "/templates/styles/default";
    }

    public static String getDefaultStyle() {
        return hostAndContext + "/templates/styles/default";
    }

    public static String getHomeStyle() {
        return hostAndContext + "/templates/styles/home";
    }

    public static String getImages() {
        return hostAndContext + "/templates/styles/default/image";
    }

    public static String getData() {
        return hostAndContext + "/data";
    }

    public static String getCss() {
        return hostAndContext + "/templates/styles/default/css";
    }

    public static String getJs() {
        return hostAndContext + "/templates/styles/default/js";
    }

    /*
     * La Gestion des sesssions
     */
    public static String getSession(String attr) {
        return SessionsUtils.getString(attr);
    }

    public static String getRandomValue() {

        String var = "1";

        if ("Y".equalsIgnoreCase(MapsGetter.getConfigBasicMap().get("2"))) {
            Date d = new Date();
            var = "ver-" + Math.random() + "-" + d.getTime();
            //log.info("version unstable (" + var+").");
        } else {
            //log.info("version stable (" + var+").");
        }

        return var;
    }

    /*
     * La Config user for Utils
     */
    public static String getCurrency() {
        return ModuleParameterUtils.getCurrencyDefaultValue();
    }

    public static String getSysDate() {
        return MyDate.formatDate(MyDate.getSysDate(), ModuleParameterUtils.getDateFormatDefaultValue());
    }

    public static String getSysDateTime() {
        return MyDate.formatDate(MyDate.getSysDate(), ModuleParameterUtils.getDateTimeFormatDefaultValue());
    }

    public static String getTxtYesNo(String val) {

        String result = "";

        if ("y".equalsIgnoreCase(val)) {
            result = MapsGetter.getMetaModelMap().get("globals.forms.yes.label");
        } else if ("n".equalsIgnoreCase(val)) {
            result = MapsGetter.getMetaModelMap().get("globals.forms.no.label");
        }
        return result;
    }

    /*
     * La Config user for client
     */
    public static String formatDate(Date d) {
        String resultat = "";
        if (d != null) {
            resultat = MyDate.formatDate(d, ModuleParameterUtils.getDateFormatDefaultValue());
        }
        return resultat;
    }

    public static String formatDateTime(Date d) {
        String resultat = "";
        if (d != null) {
            resultat = MyDate.formatDate(d, ModuleParameterUtils.getDateTimeFormatDefaultValue());
        }
        return resultat;
    }

    public static String formatDouble(Double d) {
        String resultat = "";
        if (d != null) {
            resultat = MyDouble.format(d);
        }
        return resultat;
    }

    public static String formatAmount(Double d) {

        if (d == null) {
            d = Double.valueOf("0");
        }
        return formatDouble(d) + " " + getCurrency();
    }

}
