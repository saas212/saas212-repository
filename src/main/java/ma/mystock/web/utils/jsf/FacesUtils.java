/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.jsf;

import java.io.IOException;
import java.util.ResourceBundle;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;

/**
 *
 * @author abdou
 */
public class FacesUtils {

    public static FacesContext getFacesContext() {
        FacesContext faces = FacesContext.getCurrentInstance();
        return faces;
    }

    public static ExternalContext getExternalContext() {
        return FacesContext.getCurrentInstance().getExternalContext();
    }

    public static String getRequestParameter(String key) {
        String val = "";
        try {
            val = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(key);
        } catch (Exception ex) {
        }

        return val;
    }

    public static String getRequestParam(String paramName) {

        String val = "";
        try {
            HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            val = request.getParameter(paramName);
        } catch (Exception ex) {
        }

        return val;

    }

    public static Object getObjectFromRequestParameter(String requestParameterName, Converter converter, UIComponent component) {
        String theId = getRequestParameter(requestParameterName);
        return converter.getAsObject(FacesContext.getCurrentInstance(), component, theId);
    }

    public static String getPath() {
        HttpServletRequest request = (HttpServletRequest) getFacesContext().getExternalContext().getRequest();
        return request.getContextPath();
    }

    public static void goTo(String page) {
        try {
            getExternalContext().redirect(page);
        } catch (Exception ex1) {
            ex1.printStackTrace();
        }
    }

    public static void newTab(String page) {
        try {
            RequestContext context = RequestContext.getCurrentInstance();
            context.execute("window.open('" + page + "', '_newtab')");
        } catch (Exception ex1) {
            ex1.printStackTrace();
        }
    }

    public static ServletContext getServletContext() {
        return (ServletContext) getExternalContext().getContext();
    }

    public static String getContextParam(String contextParam) {
        return getServletContext().getInitParameter(contextParam);
    }

    public static HttpServletRequest getHttpServletRequest() {
        return (HttpServletRequest) getExternalContext().getRequest();
    }

    public static HttpServletResponse getHttpServletResponse() {
        return (HttpServletResponse) getExternalContext().getResponse();
    }

    public static HttpSession getHttpSession() {
        return getHttpServletRequest().getSession();
    }

    public static HttpSession getHttpSession(boolean create) {
        return getHttpServletRequest().getSession(create);
    }

    public static Object getRequestAttribute(String name) {
        return getExternalContext().getRequestParameterMap().get(name);
    }

    public static Object getSessionAttribute(String name) {
        return getHttpSession().getAttribute(name);
    }

    public static Object getSessionAttribute(boolean create, String name) {
        return getHttpSession(true).getAttribute(name);
    }

    public static void removeSessionAttribute(String name) {
        getHttpSession().removeAttribute(name);
    }

    public static void setSessionAttribute(String name, Object value) {
        getHttpSession().setAttribute(name, value);
    }

    public static void redirect(String url) throws IOException {
        FacesContext.getCurrentInstance().getExternalContext().redirect(url);
    }

    public static void dispatch(String url) throws IOException {
        FacesContext.getCurrentInstance().getExternalContext().dispatch(url);
    }

    public static ResourceBundle getResourceBundle(String name) {
        return FacesContext.getCurrentInstance().getApplication().getResourceBundle(FacesContext.getCurrentInstance(), name);
    }

    public static UIComponent findComponent(String id) {
        UIComponent result = null;
        UIComponent root = FacesContext.getCurrentInstance().getViewRoot();
        if (root != null) {
            result = findComponent(root, id);
        }
        return result;
    }

    public static UIComponent findComponent(UIComponent root, String id) {
        UIComponent result = null;
        if (root.getId().equals(id)) {
            return root;
        }

        for (UIComponent child : root.getChildren()) {
            if (child.getId().equals(id)) {
                result = child;
                break;
            }
            result = findComponent(child, id);
            if (result != null) {
                break;
            }
        }
        return result;
    }

}
