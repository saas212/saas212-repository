
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.test;

import static java.util.concurrent.TimeUnit.*;
import ma.mystock.web.utils.password.MyPassword;
import ma.mystock.web.utils.datatype.MyString;

/**
 *
 * @author anasshajami
 */
public class MainTime {

    public static void main(String args[]) {

        System.out.println("admin : " + MyString.trim(MyPassword.toSHA1("admin")));
        System.out.println("webmaster : " + MyString.trim(MyPassword.toSHA1("webmaster")));
        System.out.println("stock : " + MyString.trim(MyPassword.toSHA1("stock")));
        System.out.println("superadmin : " + MyString.trim(MyPassword.toSHA1("superadmin")));

        long[] durations = new long[]{
            123,
            SECONDS.toMillis(5) + 123,
            DAYS.toMillis(1) + HOURS.toMillis(1),
            DAYS.toMillis(1) + SECONDS.toMillis(2),
            DAYS.toMillis(1) + HOURS.toMillis(1) + MINUTES.toMillis(2),
            DAYS.toMillis(4) + HOURS.toMillis(3) + MINUTES.toMillis(2) + SECONDS.toMillis(1),
            DAYS.toMillis(5) + HOURS.toMillis(4) + MINUTES.toMillis(1) + SECONDS.toMillis(23) + 123,
            DAYS.toMillis(42)
        };

        for (long duration : durations) {
            System.out.println(TimeUtils.formatMillis(duration, DAYS, SECONDS));
        }

        System.out.println("\nAgain in only hours and minutes\n");

        for (long duration : durations) {
            System.out.println(TimeUtils.formatMillis(duration, HOURS, MINUTES));
        }
    }
}
