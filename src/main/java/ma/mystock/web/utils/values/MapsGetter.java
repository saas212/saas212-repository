package ma.mystock.web.utils.values;

import ma.mystock.web.listners.LoadContextListener;
import ma.mystock.web.listners.helpers.AttributHiddenMap;
import ma.mystock.web.listners.helpers.ConfigBasicMap;
import ma.mystock.web.listners.helpers.LovMap;
import ma.mystock.web.listners.helpers.MetaModelMap;
import ma.mystock.web.listners.helpers.ModelMap;
import ma.mystock.web.listners.helpers.PageMap;
import ma.mystock.web.listners.helpers.PageParameterMap;
import ma.mystock.web.listners.helpers.PageValidationMap;
import ma.mystock.web.listners.helpers.ParameterClientMap;
import ma.mystock.web.listners.helpers.PrivilegesMap;

/**
 *
 * @author abdou
 */
public class MapsGetter {

    public static MetaModelMap getMetaModelMap() {
        return LoadContextListener.getMetaModelMap();
    }

    public static ConfigBasicMap getConfigBasicMap() {
        return LoadContextListener.getConfigBasicMap();
    }

    public static AttributHiddenMap getAttributHiddenMap() {
        return LoadContextListener.getAttributHiddenMap();
    }

    public static LovMap getLoadLovMap() {
        return LoadContextListener.getLovsMap();
    }

    public static ModelMap getModelMap() {
        return LoadContextListener.getModelMap();
    }

    public static PageParameterMap getPageParameterMap() {
        return LoadContextListener.getPageParameterMap();
    }

    public static PageValidationMap getPageValidationMap() {
        return LoadContextListener.getPageValidationMap();
    }

    public static PageMap getPageMap() {
        return LoadContextListener.getPageMap1();
    }

    public static ParameterClientMap getParameterClientMap() {
        return LoadContextListener.getParameterClientMap();
    }

    public static PrivilegesMap getPrivilegesMap() {
        return LoadContextListener.getPrivilegesMap();
    }

}
