/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.routing;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ma.mystock.web.filters.ChangeURIPathFilter;
import ma.mystock.web.utils.jsf.FacesUtils;
import ma.mystock.web.utils.jsf.facelet.Functions;
import ma.mystock.web.utils.sessions.SessionsGetter;
import ma.mystock.web.utils.values.MapsGetter;

/**
 *
 * @author abdou
 */
public class UrlsValues {

    public static String hostAndContext;

    public static void onInit() {
        hostAndContext = MapsGetter.getConfigBasicMap().get("1");
    }

    // URL PAGE GLOBALS
    public static String URL_LOGIN_PAGE = "login";
    public static String URL_MODULE_PAGE = "module";
    public static String URL_INDEX_PAGE = "index";
    public static String URL_EXCEPTION_PAGE = "exception";
    public static String URL_404_PAGE = "404";
    public static String URL_FACES = "";

    // METHODES 
    public static String getUrlLogin() {
        return URL_LOGIN_PAGE + ".xhtml";
    }

    public static String getUrlModule() {
        return URL_MODULE_PAGE + ".xhtml";
    }

    public static String getUrlIndex() {
        return "test1/" + URL_INDEX_PAGE + ".xhtml";
    }

    public static String getUrlFacesIndex() {
        return URL_FACES + "/" + URL_INDEX_PAGE + ".xhtml";
    }

    public static String getUrlException() {
        return URL_EXCEPTION_PAGE + ".xhtml";
    }

    public static String get404() {
        return URL_404_PAGE + ".xhtml";
    }

    public static String getFullUrlModule() {
        //System.out.println(" > " + MapsGetter.getConfigBasicMap().get("1") + "/" + SessionsGetter.getCltClientCode() + "/" + URL_MODULE_PAGE + ".xhtml");
        return MapsGetter.getConfigBasicMap().get("1")+ URL_MODULE_PAGE + ".xhtml";
    }

    // getUrlIndex
    // getFacesUrlIndex
    // getServletUrlIndex
    // getFullUrlIndex
}
