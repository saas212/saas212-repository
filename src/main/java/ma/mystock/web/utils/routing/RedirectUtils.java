/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.routing;

import javax.servlet.http.HttpServletResponse;
import ma.mystock.web.utils.jsf.FacesUtils;

/**
 *
 * @author abdou
 */
public class RedirectUtils {

    public static void goToLogin() {
        try {
            FacesUtils.getExternalContext().redirect(UrlsValues.getUrlLogin());
        } catch (Exception ex1) {
            ex1.printStackTrace();
        }
    }

    public static void goToModule() {
        try {
            FacesUtils.getExternalContext().redirect(UrlsValues.getFullUrlModule());
        } catch (Exception ex1) {
            ex1.printStackTrace();
        }
    }

    public static void goToIndex() {
        try {
            System.out.println(" goToIndex : " + UrlsValues.getUrlIndex());

            HttpServletResponse response = (HttpServletResponse) FacesUtils.getExternalContext().getResponse();

            if (!response.isCommitted()) {
                try {
                    FacesUtils.getExternalContext().redirect(UrlsValues.getUrlIndex());
                    return;
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
            }

        } catch (Exception ex1) {
            ex1.printStackTrace();
        }
    }

    public static void goToFacesIndex() {
        try {
            FacesUtils.getExternalContext().redirect(UrlsValues.getUrlFacesIndex());
        } catch (Exception ex1) {
            ex1.printStackTrace();
        }
    }

    public static void goToException() {
        try {
            FacesUtils.getExternalContext().redirect(UrlsValues.getUrlException());
        } catch (Exception ex1) {
            ex1.printStackTrace();
        }
    }

    public static void goTo404() {
        try {
            FacesUtils.getExternalContext().redirect(UrlsValues.get404());
        } catch (Exception ex1) {
            ex1.printStackTrace();
        }
    }

    public static void goTo(String page) {
        try {
            FacesUtils.getExternalContext().redirect(page + ".xhtml");
        } catch (Exception ex1) {
            ex1.printStackTrace();
        }
    }

}
