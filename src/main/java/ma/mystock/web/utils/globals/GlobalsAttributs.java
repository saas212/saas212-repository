package ma.mystock.web.utils.globals;

import ma.mystock.web.utils.sessions.SessionsGetter;
import ma.mystock.web.utils.values.MapsGetter;

/**
 *
 * @author abdou
 */
public class GlobalsAttributs {

    public static String ATTRIBUT_EXCLUD_MAP1 = "attributeExclud";
    public static String ATTRIBUT_REQUIRED_MAP1 = "attributeRequired";
    public static String ATTRIBUT_MAX_LENGTH_MAP1 = "attributeMxlength";

    public static String Y = "Y";
    public static String N = "N";

    public static String OPERATION_EDIT = "edit";
    public static String OPERATION_ADD = "add";

    public static Long CLT_MODULE_TYPE_WEB_MASTER_ID = 1L; // Support Technique
    public static Long CLT_MODULE_TYPE_SUPER_ADMIN_ID = 2L; // Represent du client
    public static Long CLT_MODULE_TYPE_ADMIN_ID = 3L; // Les administrateurs de stock
    public static Long CLT_MODULE_TYPE_STOCK_ID = 4L; // Les gestionnaires de stock

    public static Long CLT_USER_CATEGORY_WEB_MASTER_ID = 1L; // Support Technique
    public static Long CLT_USER_CATEGORY_SUPER_ADMIN_ID = 2L; // Represent du client
    public static Long CLT_USER_CATEGORY_ADMIN_ID = 3L; // Les administrateurs de stock
    public static Long CLT_USER_CATEGORY_STOCK_ID = 4L; // Les gestionnaires de stock

    public static Long BASIC_PARAM_CONTEXT = 1L;

    public static String INDEX_PDF = MapsGetter.getConfigBasicMap().get("1") + "/indexPDF.xhtml";//"http://localhost:8083/MyStock/indexPDF.xhtml";
    //public static String INDEX_PDF = MapsGetter.getConfigBasicMap().get("1") + "/" + SessionsGetter.getCltClientCode() + "/" + "/indexPDF.xhtml";//"http://localhost:8083/MyStock/indexPDF.xhtml";

    // Statut des receptions
    public static Long RECEPTION_STATUS_ID_IN_PROGRESS = 1L;
    public static Long RECEPTION_STATUS_ID_SUBMIT = 2L;
    public static Long RECEPTION_STATUS_ID_VALID = 3L;
    public static Long RECEPTION_STATUS_ID_NOT_VALID = 4L;

    // status des returnrecipte
    public static Long ORDER_SUPPLIER_STATUS_ID_IN_PROGRESS = 1L;
    public static Long ORDER_SUPPLIER_STATUS_ID_SUBMIT = 2L;
    public static Long ORDER_SUPPLIER_STATUS_ID_VALID = 3L;
    public static Long ORDER_SUPPLIER_STATUS_ID_NOT_VALID = 4L;

    public static Long RETURN_RECEIPT_STATUS_ID_IN_PROGRESS = 1L;
    public static Long RETURN_RECEIPT_STATUS_ID_SUBMIT = 2L;
    public static Long RETURN_RECEIPT_STATUS_ID_VALID = 3L;
    public static Long RETURN_RECEIPT_STATUS_ID_NOT_VALID = 4L;

    public static Long ORDER_STATUS_ID_IN_PROGRESS = 1L;
    public static Long ORDER_STATUS_ID_SUBMIT = 2L;
    public static Long ORDER_STATUS_ID_VALID = 3L;
    public static Long ORDER_STATUS_ID_NOT_VALID = 4L;
    public static String LANGUAGE;
    public static String LOCALE;

    public static final String ITEM_CODE_MESSAGE = "message";
    public static final String ITEM_CODE_HELP = "help";
    public static final String ITEM_CODE_LABEL = "label";
    public static final String ITEM_CODE_COMMENT = "comment";
    public static final String ITEM_CODE_TITLE = "title";

    public static final Double DEFAULT_PRICE_SALE = Double.valueOf("0");
    public static final Double DEFAULT_PRICE_BUY = Double.valueOf("0");
    public static final Long DEFAULT_QUANTITYE = 0L;

}
/*

 clt_module_parameter
 clt_parameter
 inf_basic_parameter
 pm_page_parameter
 pm_pdf_parameter

 GlobalsCltModuleParameter
 GlobalsParameter
 GlobalsBasicParameter
 GlobalsPageParameter
 GlobalsPdfParameter

 */
