/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.lovs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.model.SelectItem;
import ma.mystock.core.dao.entities.views.VCltClient;
import ma.mystock.core.dao.entities.views.VCltModuleParameterType;
import ma.mystock.core.dao.entities.views.VCltParameter;
import ma.mystock.core.dao.entities.views.VCltParameterType;
import ma.mystock.core.dao.entities.views.VCltUserCategory;
import ma.mystock.core.dao.entities.views.VInfBasicParameterType;
import ma.mystock.core.dao.entities.views.VInfCity;
import ma.mystock.core.dao.entities.views.VInfCountry;
import ma.mystock.core.dao.entities.views.VInfCurrency;
import ma.mystock.core.dao.entities.views.VPmPage;
import ma.mystock.core.dao.entities.views.VPmPageParameterType;
import ma.mystock.core.dao.entities.views.VSmCustomer;
import ma.mystock.core.dao.entities.views.VSmCustomerCategory;
import ma.mystock.core.dao.entities.views.VSmCustomerType;
import ma.mystock.core.dao.entities.views.VSmDeposit;
import ma.mystock.core.dao.entities.views.VSmExpenseType;
import ma.mystock.core.dao.entities.views.VSmProductColor;
import ma.mystock.core.dao.entities.views.VSmProductDepartment;
import ma.mystock.core.dao.entities.views.VSmProductFamily;
import ma.mystock.core.dao.entities.views.VSmProductGroup;
import ma.mystock.core.dao.entities.views.VSmProductSize;
import ma.mystock.core.dao.entities.views.VSmProductStatus;
import ma.mystock.core.dao.entities.views.VSmProductType;
import ma.mystock.core.dao.entities.views.VSmProductUnit;
import ma.mystock.core.dao.entities.views.VSmSupplier;
import ma.mystock.core.dao.entities.views.VSmSupplierCategory;
import ma.mystock.core.dao.entities.views.VSmSupplierType;
import ma.mystock.web.utils.springBeans.GloablsServices;
import ma.mystock.web.utils.globals.GlobalsAttributs;
import ma.mystock.web.utils.sessions.SessionsGetter;

/**
 *
 * @author abdou
 */
public class LovsUtils extends GloablsServices {

    //################## FiltedBy Model Id is Included ##################//
    public static Map<String, Object> paramQuery = new HashMap<>();

    public static String SM_CUSTOMER = "SM_CUSTOMER";
    public static String SM_SUPPLIER_TYPE = "SM_SUPPLIER_TYPE";
    public static String INF_COUNTRY = "INF_COUNTRY";
    public static String INF_CITY = "INF_CITY";
    public static String SM_CUSTOMER_TYPE = "SM_CUSTOMER_TYPE";
    public static String SM_SUPPLIER = "SM_SUPPLIER";
    public static String SM_DEPOSIT = "SM_DEPOSIT";

    /*##################################  SmSupplier  ##################################*/
    public static List<SelectItem> getSmSupplierItems() {

        paramQuery.clear();
        paramQuery.put("cltModuleId", SessionsGetter.getCltModuleId());
        paramQuery.put("active", GlobalsAttributs.Y);

        List<VSmSupplier> vSmSupplierList = vSmSupplierEJB.executeQuery(VSmSupplier.findByCltModuleIdAndActive, paramQuery);
        List<SelectItem> supplierItems = new ArrayList<>();

        for (VSmSupplier o : vSmSupplierList) {
            supplierItems.add(new SelectItem(o.getId(), o.getCompanyName()));
        }

        return supplierItems;
    }

    /*##################################    ##################################*/
    public static List<SelectItem> getInfCurrencyItems() {

        List<VInfCurrency> list = genericServices.findVInfCurrencyByActive(GlobalsAttributs.Y);

        List<SelectItem> items = new ArrayList<>();

        for (VInfCurrency o : list) {
            items.add(new SelectItem(o.getId(), o.getCode() + ", " + o.getName()));
        }

        return items;
    }

    public static List<SelectItem> getSmCustomerItems() {

        paramQuery.clear();
        paramQuery.put("cltModuleId", SessionsGetter.getCltModuleId());
        paramQuery.put("active", GlobalsAttributs.Y);

        List<VSmCustomer> customerList = vSmCustomerEJB.executeQuery(VSmCustomer.findByCltModuleIdAndActive, paramQuery);

        List<SelectItem> customerItems = new ArrayList<>();
        for (VSmCustomer item : customerList) {
            customerItems.add(new SelectItem(item.getId(), item.getCompanyName()));
        }
        return customerItems;
    }


    /*##################################  SM_CUSTOMER_TYPE  ##################################*/
    public static List<SelectItem> getSmCustomerTypeItem() {

        paramQuery.clear();
        paramQuery.put("cltModuleId", SessionsGetter.getCltModuleId());
        paramQuery.put("active", GlobalsAttributs.Y);

        List<VSmCustomerType> vSmCustomerTypeList = vSmCustomerTypeEJB.executeQuery(VSmCustomerType.findByCltModuleIdAndActive, paramQuery);
        List<SelectItem> customerTypeItems = new ArrayList<>();

        for (VSmCustomerType o : vSmCustomerTypeList) {
            customerTypeItems.add(new SelectItem(o.getId(), o.getName()));
        }

        return customerTypeItems;
    }

    /*##################################  Customer category  ##################################*/
    public static List<SelectItem> getSmCustomerCategoryItem() {

        paramQuery.clear();
        paramQuery.put("cltModuleId", SessionsGetter.getCltModuleId());
        paramQuery.put("active", GlobalsAttributs.Y);

        List<VSmCustomerCategory> vSmCustomerCategoryList = vSmCustomerCategoryEJB.executeQuery(VSmCustomerCategory.findByCltModuleIdAndActive, paramQuery);
        List<SelectItem> customerCategoryList = new ArrayList<>();

        for (VSmCustomerCategory o : vSmCustomerCategoryList) {
            customerCategoryList.add(new SelectItem(o.getId(), o.getName()));
        }

        return customerCategoryList;
    }

    /*##################################  INF_COUNTRY  ##################################*/
    public static List<SelectItem> getInfCountryItem() {

        List<VInfCountry> vInfCountryList = genericServices.findVInfCountryByActive("Y");

        //paramQuery.clear();
        //paramQuery.put("cltModuleId", SessionsGetter.getCltModuleId());
        //paramQuery.put("active", GlobalsAttributs.Y);
        //List<VInfCountry> vInfCountryList = vInfCountryEJB.executeQuery(VInfCountry.findByCltModuleIdAndActive, paramQuery);
        List<SelectItem> countryList = new ArrayList<>();

        for (VInfCountry o : vInfCountryList) {
            countryList.add(new SelectItem(o.getId(), o.getName()));
        }

        return countryList;
    }

    /*##################################  INF_CITY  ##################################*/
    public static List<SelectItem> getInfCityItem() {

        paramQuery.clear();
        paramQuery.put("cltModuleId", SessionsGetter.getCltModuleId());
        paramQuery.put("active", GlobalsAttributs.Y);

        List<VInfCity> vInfCityList = null;//vInfCityEJB.executeQuery(VInfCity.findByCltModuleIdAndActive, paramQuery);
        List<SelectItem> cityList = new ArrayList<>();

        //for (VInfCity o : vInfCityList) {
        //    cityList.add(new SelectItem(o.getId(), o.getName()));
        //}
        return cityList;
    }

    public static List<SelectItem> getInfCityByCountryIdItems(Long countryId) {

        List<VInfCity> vInfCityList = genericServices.findVInfCityByCountryIdAndActive(countryId, "Y");
        System.out.println(" => vInfCityList.size " + vInfCityList.size());
        List<SelectItem> cityList = new ArrayList<>();
        cityList.add(new SelectItem("", ""));
        for (VInfCity o : vInfCityList) {
            cityList.add(new SelectItem(o.getId(), o.getName()));
        }

        return cityList;
    }

    /*##################################  SM_EXPENSE_TYPE  ##################################*/
    public static List<SelectItem> getSmExpenseTypeItem() {

        paramQuery.clear();
        paramQuery.put("cltModuleId", SessionsGetter.getCltModuleId());

        List<VSmExpenseType> vSmExpenseTypeList = vSmExpenseTypeEJB.executeQuery(VSmExpenseType.findByCltModuleId, paramQuery);
        List<SelectItem> expenseList = new ArrayList<>();

        for (VSmExpenseType o : vSmExpenseTypeList) {
            expenseList.add(new SelectItem(o.getId(), o.getName()));
        }

        return expenseList;
    }

    /*##################################  VCltUserCategory  ##################################*/
    public static List<SelectItem> getCltUserCategoryItems() {

        List<VCltUserCategory> vCltUserCategoryList = genericServices.findVCltUserCategoryByCltModuleId(SessionsGetter.getCltModuleId());
        List<SelectItem> vCltUserCategoryItems = new ArrayList<>();

        for (VCltUserCategory o : vCltUserCategoryList) {
            vCltUserCategoryItems.add(new SelectItem(o.getId(), o.getName()));
        }

        return vCltUserCategoryItems;
    }

    /*##################################  VSmSupplierType  ##################################*/
    public static List<SelectItem> getSmSupplierTypeItem() {

        paramQuery.clear();
        paramQuery.put("cltModuleId", SessionsGetter.getCltModuleId());
        paramQuery.put("active", GlobalsAttributs.Y);

        List<VSmSupplierType> vSmSupplierTypeList = vSmSupplierTypeEJB.executeQuery(VSmSupplierType.findByCltModuleIdAndActive, paramQuery);

        List<SelectItem> supplierTypeList = new ArrayList<>();

        for (VSmSupplierType o : vSmSupplierTypeList) {
            supplierTypeList.add(new SelectItem(o.getId(), o.getName()));
        }

        return supplierTypeList;
    }

    /*##################################  VSmSupplierCategorie  ##################################*/
    public static List<SelectItem> getSmSupplierCategoryItem() {

        paramQuery.clear();
        paramQuery.put("cltModuleId", SessionsGetter.getCltModuleId());
        paramQuery.put("active", GlobalsAttributs.Y);

        List<VSmSupplierCategory> vSmSupplierCategoryList = vSmSupplierCategoryEJB.executeQuery(VSmSupplierCategory.findByCltModuleIdAndActive, paramQuery);

        List<SelectItem> SmSupplierCategoryList = new ArrayList<>();

        for (VSmSupplierCategory o : vSmSupplierCategoryList) {
            SmSupplierCategoryList.add(new SelectItem(o.getId(), o.getName()));
        }

        return SmSupplierCategoryList;
    }

    /*##################################  VInfBasicParameterType  ##################################*/
    public static List<SelectItem> getInfBasicParameterTypeItem() {

        paramQuery.clear();
        paramQuery.put("active", GlobalsAttributs.Y);

        List<VInfBasicParameterType> vInfBasicParameterTypeList = vInfBasicParameterTypeEJB.executeQuery(VInfBasicParameterType.findAllActive, paramQuery);

        List<SelectItem> supplierTypeList = new ArrayList<>();

        for (VInfBasicParameterType o : vInfBasicParameterTypeList) {
            supplierTypeList.add(new SelectItem(o.getId(), o.getName()));
        }

        return supplierTypeList;
    }

    /*##################################  VCltParameterType  ##################################*/
    public static List<SelectItem> getCltParameterTypeItem() {

        paramQuery.clear();
        paramQuery.put("active", GlobalsAttributs.Y);

        List<VCltParameterType> vCltParameterTypeList = vCltParameterTypeEJB.executeQuery(VCltParameterType.findAllActive, paramQuery);

        List<SelectItem> supplierTypeList = new ArrayList<>();

        for (VCltParameterType o : vCltParameterTypeList) {
            supplierTypeList.add(new SelectItem(o.getId(), o.getName()));
        }

        return supplierTypeList;
    }

    /*##################################  VCltParameter  ##################################*/
    public static List<SelectItem> getCltParameterItem() {

        paramQuery.clear();
        paramQuery.put("active", GlobalsAttributs.Y);

        List<VCltParameter> vCltParameterList = vCltParameterEJB.executeQuery(VCltParameter.findAllActive, paramQuery);

        List<SelectItem> supplierTypeList = new ArrayList<>();

        for (VCltParameter o : vCltParameterList) {
            supplierTypeList.add(new SelectItem(o.getId(), o.getName()));
        }

        return supplierTypeList;
    }

    /*##################################  VCltModuleParameterType  ##################################*/
    public static List<SelectItem> getCltModuleParameterTypeItem() {

        paramQuery.clear();
        paramQuery.put("active", GlobalsAttributs.Y);

        List<VCltModuleParameterType> vCltModuleParameterTypeList = vCltModuleParameterTypeEJB.executeQuery(VCltModuleParameterType.findAllActive, paramQuery);

        List<SelectItem> supplierTypeList = new ArrayList<>();

        for (VCltModuleParameterType o : vCltModuleParameterTypeList) {
            supplierTypeList.add(new SelectItem(o.getId(), o.getName()));
        }

        return supplierTypeList;
    }

    /*##################################  VPmPageParameterType  ##################################*/
    public static List<SelectItem> getPmPageParameterTypeItem() {

        paramQuery.clear();
        paramQuery.put("active", GlobalsAttributs.Y);

        List<VPmPageParameterType> vPmPageParameterTypeList = vPmPageParameterTypeEJB.executeQuery(VPmPageParameterType.findAllActive, paramQuery);

        List<SelectItem> supplierTypeList = new ArrayList<>();

        for (VPmPageParameterType o : vPmPageParameterTypeList) {
            supplierTypeList.add(new SelectItem(o.getId(), o.getName()));
        }

        return supplierTypeList;
    }

    /*##################################  page  ##################################*/
    public static List<SelectItem> getPmPageItem() {

        paramQuery.clear();
        paramQuery.put("active", GlobalsAttributs.Y);

        List<VPmPage> vPmPageList = vPmPageEJB.executeQuery(VPmPage.findAllActive, paramQuery);

        List<SelectItem> supplierTypeList = new ArrayList<>();

        for (VPmPage o : vPmPageList) {
            supplierTypeList.add(new SelectItem(o.getId(), o.getName()));
        }

        return supplierTypeList;
    }

    /*##################################  Client  ##################################*/
    public static List<SelectItem> getCltClientItem() {

        paramQuery.clear();
        paramQuery.put("active", GlobalsAttributs.Y);

        List<VCltClient> vCltClientList = vCltClientEJB.executeQuery(VCltClient.findAllActive, paramQuery);

        List<SelectItem> supplierTypeList = new ArrayList<>();

        for (VCltClient o : vCltClientList) {
            supplierTypeList.add(new SelectItem(o.getId(), o.getFirstName() + " " + o.getLastName()));
        }

        return supplierTypeList;
    }

    /*##################################  productTypeList  ##################################*/
    public static List<SelectItem> getSmProductTypeItem() {

        paramQuery.clear();
        paramQuery.put("cltModuleId", SessionsGetter.getCltModuleId());
        paramQuery.put("active", GlobalsAttributs.Y);

        List<VSmProductType> vSmProductTypeList = vSmProductTypeEJB.executeQuery(VSmProductType.findByCltModuleIdAndActive, paramQuery);

        List<SelectItem> productTypeList = new ArrayList<>();

        for (VSmProductType o : vSmProductTypeList) {
            productTypeList.add(new SelectItem(o.getId(), o.getName()));
        }

        return productTypeList;
    }

    /*##################################  product size  ##################################*/
    public static List<SelectItem> getSmProductSizeItem() {

        paramQuery.clear();
        paramQuery.put("cltModuleId", SessionsGetter.getCltModuleId());
        paramQuery.put("active", GlobalsAttributs.Y);

        List<VSmProductSize> vSmProductTypeList = vSmProductSizeEJB.executeQuery(VSmProductSize.findByCltModuleIdAndActive, paramQuery);

        List<SelectItem> smProductTypeList = new ArrayList<>();

        for (VSmProductSize o : vSmProductTypeList) {
            smProductTypeList.add(new SelectItem(o.getId(), o.getName()));
        }

        return smProductTypeList;
    }

    /*##################################  product status  ##################################*/
    public static List<SelectItem> getSmProductStatusItem() {

        paramQuery.clear();
        paramQuery.put("active", GlobalsAttributs.Y);

        List<VSmProductStatus> vSmProductStatusList = vSmProductStatusEJB.executeQuery(VSmProductStatus.findByActive, paramQuery);

        List<SelectItem> smProductStatusList = new ArrayList<>();

        for (VSmProductStatus o : vSmProductStatusList) {
            smProductStatusList.add(new SelectItem(o.getId(), o.getName()));
        }

        return smProductStatusList;
    }

    /*##################################  product Family  ##################################*/
    public static List<SelectItem> getSmProductFamilyItem() {

        paramQuery.clear();
        paramQuery.put("cltModuleId", SessionsGetter.getCltModuleId());
        paramQuery.put("active", GlobalsAttributs.Y);

        List<VSmProductFamily> vSmProductFamilyList = vSmProductFamilyEJB.executeQuery(VSmProductFamily.findByCltModuleIdAndActive, paramQuery);

        List<SelectItem> smProductFamilyList = new ArrayList<>();

        for (VSmProductFamily o : vSmProductFamilyList) {
            smProductFamilyList.add(new SelectItem(o.getId(), o.getName()));
        }

        return smProductFamilyList;
    }

    public static List<SelectItem> getSmProductFamilyItem(Long productGroupId) {

        paramQuery.clear();
        paramQuery.put("cltModuleId", SessionsGetter.getCltModuleId());
        paramQuery.put("active", GlobalsAttributs.Y);
        paramQuery.put("productGroupId", productGroupId);

        List<VSmProductFamily> vSmProductFamilyList = vSmProductFamilyEJB.executeQuery(VSmProductFamily.findByCltModuleIdAndProductGroupIdAndActive, paramQuery);

        List<SelectItem> smProductFamilyList = new ArrayList<>();

        for (VSmProductFamily o : vSmProductFamilyList) {
            smProductFamilyList.add(new SelectItem(o.getId(), o.getName()));
        }

        return smProductFamilyList;
    }

    /*##################################  product color  ##################################*/
    public static List<SelectItem> getSmProductColorItem() {

        paramQuery.clear();
        paramQuery.put("cltModuleId", SessionsGetter.getCltModuleId());
        paramQuery.put("active", GlobalsAttributs.Y);

        List<VSmProductColor> vSmProductColorList = vSmProductColorEJB.executeQuery(VSmProductColor.findByCltModuleIdAndActive, paramQuery);

        List<SelectItem> smProductColorList = new ArrayList<>();

        for (VSmProductColor o : vSmProductColorList) {
            smProductColorList.add(new SelectItem(o.getId(), o.getName()));
        }

        return smProductColorList;
    }

    /*##################################  product group  ##################################*/
    public static List<SelectItem> getSmProductGroupItem() {

        paramQuery.clear();
        paramQuery.put("cltModuleId", SessionsGetter.getCltModuleId());
        paramQuery.put("active", GlobalsAttributs.Y);

        List<VSmProductGroup> vSmProductGroupList = vSmProductGroupEJB.executeQuery(VSmProductGroup.findByCltModuleIdAndActive, paramQuery);

        List<SelectItem> smProductGroupList = new ArrayList<>();

        for (VSmProductGroup o : vSmProductGroupList) {
            smProductGroupList.add(new SelectItem(o.getId(), o.getName()));
        }

        return smProductGroupList;
    }

    /*##################################  product group  ##################################*/
    public static List<SelectItem> getSmDepositItem() {

        paramQuery.clear();
        paramQuery.put("cltModuleId", SessionsGetter.getCltModuleId());
        paramQuery.put("active", GlobalsAttributs.Y);

        List<VSmDeposit> vSmDepositList = vSmDepositEJB.executeQuery(VSmDeposit.findByCltModuleIdAndActive, paramQuery);

        List<SelectItem> smDepositList = new ArrayList<>();

        for (VSmDeposit o : vSmDepositList) {
            smDepositList.add(new SelectItem(o.getId(), o.getName()));
        }

        return smDepositList;
    }

    /*##################################  VSmProductDepartment  ##################################*/
    public static List<SelectItem> getSmProductDepartmentItems() {

        List<VSmProductDepartment> vSmProductDepartmentList = genericServices.findVSmProductDepartmentByActiveAndCltModuleId(GlobalsAttributs.Y, SessionsGetter.getCltModuleId());
        List<SelectItem> items = new ArrayList<>();

        for (VSmProductDepartment o : vSmProductDepartmentList) {
            items.add(new SelectItem(o.getId(), o.getName()));
        }

        return items;
    }

    /*##################################  VSmProductUnit  ##################################*/
    public static List<SelectItem> getSmProductUnitItems() {

        List<VSmProductUnit> vSmProductUnittList = genericServices.findVSmProductUnitByActiveAndCltModuleId(GlobalsAttributs.Y, SessionsGetter.getCltModuleId());
        List<SelectItem> items = new ArrayList<>();

        for (VSmProductUnit o : vSmProductUnittList) {
            items.add(new SelectItem(o.getId(), o.getName()));
        }

        return items;
    }

}
