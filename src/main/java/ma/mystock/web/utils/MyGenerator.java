/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

/**
 *
 * @author Abdessamad HALLAL
 */
public class MyGenerator {

    public static final int SIZE_SHORT = 3;
    public static final int SIZE_MEDIUM = 6;
    public static final int SIZE_LONG = 10;

    public static final int TYPE_NUMERIC = 1;
    public static final int TYPE_ALPHANUMERIC = 2;

    public static String generate(int size, int type) {
        char[] chars = "abcdefghijklmnopqrstuvwxyz123456789".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

    public static void generate1(int size, int type) {
        SecureRandom random = new SecureRandom();
        System.out.println(new BigInteger(130, random).toString(32));
        System.out.println(new BigInteger(130, random).toString(32));
    }

}
