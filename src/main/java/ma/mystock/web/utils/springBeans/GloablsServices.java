/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.springBeans;


import ma.mystock.core.dao.entities.SmSupplier;
import ma.mystock.core.dao.entities.PmPageParameter;
import ma.mystock.core.dao.entities.CltModule;
import ma.mystock.core.dao.entities.PmModel;
import ma.mystock.core.dao.entities.SmProductColor;
import ma.mystock.core.dao.entities.CltClient;
import ma.mystock.core.dao.entities.CltParameterClient;
import ma.mystock.core.dao.entities.CltUser;
import ma.mystock.core.dao.entities.SmOrderSupplierLine;
import ma.mystock.core.dao.entities.SmSupplierType;
import ma.mystock.core.dao.entities.SmProductSize;
import ma.mystock.core.dao.entities.CltModuleParameterClient;
import ma.mystock.core.dao.entities.CltParameter;
import ma.mystock.core.dao.entities.SmProduct;
import ma.mystock.core.dao.entities.SmOrderSupplier;
import ma.mystock.core.dao.entities.InfCountry;
import ma.mystock.core.dao.entities.PmPage;
//import ma.mystock.core.dao.entities.SmReceptionProducts;
import ma.mystock.core.dao.entities.SmReturnReceiptStatus;
import ma.mystock.core.dao.entities.CltUserClient;
import ma.mystock.core.dao.entities.InfBasicParameter;
import ma.mystock.core.dao.entities.SmOrder;
import ma.mystock.core.dao.entities.SmReturnReceipt;
import ma.mystock.core.dao.entities.SmExpense;
import ma.mystock.core.dao.entities.SmProductStatus;
import ma.mystock.core.dao.entities.InfCity;
import ma.mystock.core.dao.entities.SmReception;
import ma.mystock.core.dao.entities.SmReceptionStatus;
import ma.mystock.core.dao.entities.SmOrderLine;
import ma.mystock.core.dao.entities.SmCustomer;
import ma.mystock.core.dao.entities.SmProductFamily;
import ma.mystock.core.dao.entities.SmReturnReceiptLine;
import ma.mystock.core.dao.entities.CltModuleParameter;
import ma.mystock.core.dao.entities.SmExpenseType;
import ma.mystock.core.dao.entities.PmPageParameterModule;
import ma.mystock.core.dao.entities.SmReceptionLine;
import java.io.Serializable;
import ma.mystock.core.dao.entities.views.VSmDeposit;
//import ma.mystock.core.dao.entities.views.VSmSizeByReference;
import ma.mystock.core.dao.entities.views.VPmValidation;
import ma.mystock.core.dao.entities.views.VSmExpense;
//import ma.mystock.core.dao.entities.views.VSmReceptionProducts;
import ma.mystock.core.dao.entities.views.VSmOrderSummary;
import ma.mystock.core.dao.entities.views.VCltClient;
import ma.mystock.core.dao.entities.views.VSmProductFamily;
import ma.mystock.core.dao.entities.views.VSmOrder;
import ma.mystock.core.dao.entities.views.VCltModuleParameter;
import ma.mystock.core.dao.entities.views.VSmProduct;
//import ma.mystock.core.dao.entities.views.VPmAttributeReadonly;
import ma.mystock.core.dao.entities.views.VSmReceptionSummary;
import ma.mystock.core.dao.entities.views.VCltParameters;
import ma.mystock.core.dao.entities.views.VSmProductStatus;
//import ma.mystock.core.dao.entities.views.VPmAttributeHidden;
import ma.mystock.core.dao.entities.views.VSmOrderLine;
import ma.mystock.core.dao.entities.views.VSmCustomer;
import ma.mystock.core.dao.entities.views.VInfBasicParameterType;
import ma.mystock.core.dao.entities.views.VSmCustomerCategory;
import ma.mystock.core.dao.entities.views.VCltUserCategory;
//import ma.mystock.core.dao.entities.views.VSmRapDaily;
//import ma.mystock.core.dao.entities.views.VPmAttributeMaxLength;
import ma.mystock.core.dao.entities.views.VCltModuleType;
import ma.mystock.core.dao.entities.views.VInfCountry;
//import ma.mystock.core.dao.entities.views.VPmAttributeExclud;
import ma.mystock.core.dao.entities.views.VInfBasicParameter;
//import ma.mystock.core.dao.entities.views.VPmAttributeMaxWord;
//import ma.mystock.core.dao.entities.views.VSmInventaire;
import ma.mystock.core.dao.entities.views.VSmProductType;
import ma.mystock.core.dao.entities.views.VSmProductSize;
import ma.mystock.core.dao.entities.views.VSmCustomerType;
import ma.mystock.core.dao.entities.views.VInfMetaModel;
import ma.mystock.core.dao.entities.views.VSmSupplierType;
import ma.mystock.core.dao.entities.views.dto.VPmModelDto;
import ma.mystock.core.dao.entities.views.VPmComposition;
import ma.mystock.core.dao.entities.views.VInfCity;
import ma.mystock.core.dao.entities.views.dto.VPmMenusDto;
import ma.mystock.core.dao.entities.views.VSmReception;
import ma.mystock.core.dao.entities.views.VSmSupplierCategory;
import ma.mystock.core.dao.entities.views.VCltUserClient;
import ma.mystock.core.dao.entities.views.VCltParameterType;
import ma.mystock.core.dao.entities.views.VCltClientLanguage;
import ma.mystock.core.dao.entities.views.VPmPageModule;
import ma.mystock.core.dao.entities.views.VSmLovs;
import ma.mystock.core.dao.entities.views.VInfPrivileges;
import ma.mystock.core.dao.entities.views.dto.VPmGroupsDto;
import ma.mystock.core.dao.entities.views.VCltUserDetails;
import ma.mystock.core.dao.entities.views.VInfLovs;
import ma.mystock.core.dao.entities.views.VPmPageAttribute;
import ma.mystock.core.dao.entities.views.VCltModuleParameterType;
import ma.mystock.core.dao.entities.views.VCltParameterClient;
import ma.mystock.core.dao.entities.views.dto.VPmCategorysDto;
import ma.mystock.core.dao.entities.views.VCltModule;
import ma.mystock.core.dao.entities.views.VSmProductColor;
//import ma.mystock.core.dao.entities.views.VPmAttributeRequired;
import ma.mystock.core.dao.entities.views.VSmOrderSupplierLine;
import ma.mystock.core.dao.entities.views.VCltParameter;
import ma.mystock.core.dao.entities.views.dto.VPmPagesDto;
import ma.mystock.core.dao.entities.views.VCltModuleParameterClient;
//import ma.mystock.core.dao.entities.views.VSmColorByReference;
import ma.mystock.core.dao.entities.views.VSmOrderSupplier;
import ma.mystock.core.dao.entities.views.VPmPage;
import ma.mystock.core.dao.entities.views.VPmPageParameterType;
import ma.mystock.core.dao.entities.views.VCltUserModule;
import ma.mystock.core.dao.entities.views.VPmPageParameterModule;
import ma.mystock.core.dao.entities.views.VPmPageParameter;
import ma.mystock.core.dao.entities.views.VSmReturnReceipt;
import ma.mystock.core.dao.entities.views.VPmComposDetails;
import ma.mystock.core.dao.entities.views.VCltClientDetails;
import ma.mystock.core.dao.entities.views.VCltUser;
import ma.mystock.core.dao.entities.views.VSmProductGroup;
import ma.mystock.core.dao.entities.views.VSmExpenseType;
import ma.mystock.core.dao.entities.views.VSmSupplier;
import ma.mystock.core.dao.GenericDAO;
import ma.mystock.core.dao.entities.views.VSmOrderSupplierSummary;
import ma.mystock.core.dao.entities.views.VSmReceptionLine;
import ma.mystock.core.dao.entities.views.VSmReturnReceiptLine;
import ma.mystock.core.dao.entities.views.VSmReturnReceiptSummary;
import ma.mystock.core.services.GenericServices;

/**
 *
 * @author anasshajami
 */
public class GloablsServices implements Serializable {

    public static GenericDAO<VSmReception, Long> vSmReceptionEJB = AutowiredUtils.getEJB(VSmReception.class);

    public static GenericDAO<VSmReceptionLine, Long> vSmReceptionLineEJB = AutowiredUtils.getEJB(VSmReceptionLine.class);

    public static GenericDAO<VSmReceptionSummary, Long> vSmReceptionSummaryEJB = AutowiredUtils.getEJB(VSmReceptionSummary.class);

    public static GenericDAO<SmReception, Long> smReceptionEJB = AutowiredUtils.getEJB(SmReception.class);

    public static GenericDAO<SmReceptionLine, Long> smReceptionLineEJB = AutowiredUtils.getEJB(SmReceptionLine.class);

    public static GenericDAO<SmReceptionStatus, Long> smReceptionStatusEJB = AutowiredUtils.getEJB(SmReceptionStatus.class);

    public static GenericDAO<SmExpense, Long> smExpenseEJB = AutowiredUtils.getEJB(SmExpense.class);

    public static GenericDAO<CltUser, Long> cltUserEJB = AutowiredUtils.getEJB(CltUser.class);

    public static GenericDAO<SmExpenseType, Long> smExpenseTypeEJB = AutowiredUtils.getEJB(SmExpenseType.class);

    public static GenericDAO<VSmExpenseType, Long> vSmExpenseTypeEJB = AutowiredUtils.getEJB(VSmExpenseType.class);

    public static GenericDAO<SmSupplierType, Long> smSupplierTypeEJB = AutowiredUtils.getEJB(SmSupplierType.class);

    public static GenericDAO<InfCity, Long> infCityEJB = AutowiredUtils.getEJB(InfCity.class);

    public static GenericDAO<InfCountry, Long> infCountryEJB = AutowiredUtils.getEJB(InfCountry.class);

    public static GenericDAO<SmProduct, Long> smProductEJB = AutowiredUtils.getEJB(SmProduct.class);

    public static GenericDAO<SmProductColor, Long> smProductColorEJB = AutowiredUtils.getEJB(SmProductColor.class);

    public static GenericDAO<SmProductFamily, Long> smProductFamilyEJB = AutowiredUtils.getEJB(SmProductFamily.class);

    public static GenericDAO<SmProductSize, Long> smProductSizeEJB = AutowiredUtils.getEJB(SmProductSize.class);

    public static GenericDAO<SmProductStatus, Long> smProductStatusEJB = AutowiredUtils.getEJB(SmProductStatus.class);

    public static GenericDAO<VSmExpense, Long> vSmExpenseEJB = AutowiredUtils.getEJB(VSmExpense.class);

    public static GenericDAO<SmSupplier, Long> smSupplierEJB = AutowiredUtils.getEJB(SmSupplier.class);

    public static GenericDAO<VSmSupplier, Long> vSmSupplierEJB = AutowiredUtils.getEJB(VSmSupplier.class);

    public static GenericDAO<VSmSupplierCategory, Long> vSmSupplierCategoryEJB = AutowiredUtils.getEJB(VSmSupplierCategory.class);

    public static GenericDAO<VSmProduct, Long> vSmProductEJB = AutowiredUtils.getEJB(VSmProduct.class);

    public static GenericDAO<VPmComposition, Long> vPmCompositionEJB = AutowiredUtils.getEJB(VPmComposition.class);

    public static GenericDAO<VPmValidation, Long> vPmAttributeValidationEJB = AutowiredUtils.getEJB(VPmValidation.class);

    public static GenericDAO<VPmPagesDto, String> vPmPagesDtoEJB = AutowiredUtils.getEJB(VPmPagesDto.class);

    public static GenericDAO<VSmCustomer, Long> vSmCustomerEJB = AutowiredUtils.getEJB(VSmCustomer.class);

    public static GenericDAO<SmCustomer, Long> smCustomerEJB = AutowiredUtils.getEJB(SmCustomer.class);

    //public static GenericDAO<VPmAttributeExclud, String> vPmAttributExcludEJB = AutowiredUtils.getEJB(VPmAttributeExclud.class);
    //public static GenericDAO<VPmAttributeMaxLength, String> vPmAttributeMaxLengthEJB = AutowiredUtils.getEJB(VPmAttributeMaxLength.class);
    //public static GenericDAO<VPmAttributeRequired, String> vPmAttributeRequiredEJB = AutowiredUtils.getEJB(VPmAttributeRequired.class);
    public static GenericDAO<VInfBasicParameter, String> vInfBasicParameterEJB = AutowiredUtils.getEJB(VInfBasicParameter.class);

    public static GenericDAO<InfBasicParameter, Long> infBasicParameterEJB = AutowiredUtils.getEJB(InfBasicParameter.class);

    public static GenericDAO<VInfBasicParameterType, Long> vInfBasicParameterTypeEJB = AutowiredUtils.getEJB(VInfBasicParameterType.class);

    public static GenericDAO<VCltParameter, Long> vCltParameterEJB = AutowiredUtils.getEJB(VCltParameter.class);

    public static GenericDAO<CltParameter, Long> cltParameterEJB = AutowiredUtils.getEJB(CltParameter.class);

    public static GenericDAO<VCltParameterType, Long> vCltParameterTypeEJB = AutowiredUtils.getEJB(VCltParameterType.class);

    public static GenericDAO<VCltModuleParameter, Long> vCltModuleParameterEJB = AutowiredUtils.getEJB(VCltModuleParameter.class);

    public static GenericDAO<CltModuleParameter, Long> cltModuleParameterEJB = AutowiredUtils.getEJB(CltModuleParameter.class);

    public static GenericDAO<VCltModuleParameterType, Long> vCltModuleParameterTypeEJB = AutowiredUtils.getEJB(VCltModuleParameterType.class);

    public static GenericDAO<VPmPageParameter, Long> vPmPageParameterEJB = AutowiredUtils.getEJB(VPmPageParameter.class);

    public static GenericDAO<PmPageParameter, Long> pmPageParameterEJB = AutowiredUtils.getEJB(PmPageParameter.class);

    public static GenericDAO<VPmPageParameterType, Long> vPmPageParameterTypeEJB = AutowiredUtils.getEJB(VPmPageParameterType.class);

    public static GenericDAO<PmPage, Long> pmPageEJB = AutowiredUtils.getEJB(PmPage.class);

    public static GenericDAO<VPmPage, Long> vPmPageEJB = AutowiredUtils.getEJB(VPmPage.class);

    public static GenericDAO<VSmLovs, String> vSmLovsEJB = AutowiredUtils.getEJB(VSmLovs.class);

    public static GenericDAO<VInfMetaModel, String> vInfMetaModelEJB = AutowiredUtils.getEJB(VInfMetaModel.class);

    public static GenericDAO<VPmModelDto, String> vPmModelDtoEJB = AutowiredUtils.getEJB(VPmModelDto.class);

    public static GenericDAO<VPmGroupsDto, String> vPmGroupsDtoBeanEJB = AutowiredUtils.getEJB(VPmGroupsDto.class);

    public static GenericDAO<VPmCategorysDto, String> vPmCategorysDtoEJB = AutowiredUtils.getEJB(VPmCategorysDto.class);

    public static GenericDAO<VPmMenusDto, String> vPmMenusDtoEJB = AutowiredUtils.getEJB(VPmMenusDto.class);

    public static GenericDAO<VPmPageParameterModule, String> vPmPageParameterModuleEJB = AutowiredUtils.getEJB(VPmPageParameterModule.class);

    public static GenericDAO<VCltParameters, String> vCltParametersEJB = AutowiredUtils.getEJB(VCltParameters.class);

    public static GenericDAO<VInfPrivileges, String> vInfPrivilegesEJB = AutowiredUtils.getEJB(VInfPrivileges.class);

    //public static GenericDAO<VSmInventaire, String> vSmInventaireEJB = AutowiredUtils.getEJB(VSmInventaire.class);
    //public static GenericDAO<VSmColorByReference, String> vSmColorByReferenceEJB = AutowiredUtils.getEJB(VSmColorByReference.class);
    //public static GenericDAO<VSmSizeByReference, String> vSmSizeByReferenceEJB = AutowiredUtils.getEJB(VSmSizeByReference.class);
    public static GenericDAO<SmOrder, Long> smOrderEJB = AutowiredUtils.getEJB(SmOrder.class);

    public static GenericDAO<SmOrderLine, Long> smOrderLineEJB = AutowiredUtils.getEJB(SmOrderLine.class);

    public static GenericDAO<VSmOrderLine, Long> vSmOrderLineEJB = AutowiredUtils.getEJB(VSmOrderLine.class);

    public static GenericDAO<VSmOrderSummary, Long> vSmOrderSummaryEJB = AutowiredUtils.getEJB(VSmOrderSummary.class);

    public static GenericDAO<VSmOrder, Long> vSmOrderEJB = AutowiredUtils.getEJB(VSmOrder.class);

    public static GenericDAO<VSmProductColor, Long> vSmProductColorEJB = AutowiredUtils.getEJB(VSmProductColor.class);

    public static GenericDAO<VCltUser, Long> vCltUserEJB = AutowiredUtils.getEJB(VCltUser.class);

    public static GenericDAO<VSmCustomerType, Long> vSmCustomerTypeEJB = AutowiredUtils.getEJB(VSmCustomerType.class);

    public static GenericDAO<VInfCountry, Long> vInfCountryEJB = AutowiredUtils.getEJB(VInfCountry.class);

    public static GenericDAO<VInfCity, Long> vInfCityEJB = AutowiredUtils.getEJB(VInfCity.class);

    public static GenericDAO<VSmSupplierType, Long> vSmSupplierTypeEJB = AutowiredUtils.getEJB(VSmSupplierType.class);

    //public static GenericDAO<VSmRapDaily, Long> vSmRapDailyEJB = AutowiredUtils.getEJB(VSmRapDaily.class);
    public static GenericDAO<VInfLovs, Long> vInfLovsEJB = AutowiredUtils.getEJB(VInfLovs.class);

    public static GenericDAO<CltClient, Long> cltClientEJB = AutowiredUtils.getEJB(CltClient.class);

    public static GenericDAO<VCltClient, Long> vCltClientEJB = AutowiredUtils.getEJB(VCltClient.class);

    public static GenericDAO<VCltParameterClient, Long> vCltParameterClientEJB = AutowiredUtils.getEJB(VCltParameterClient.class);

    public static GenericDAO<CltParameterClient, Long> cltParameterClientEJB = AutowiredUtils.getEJB(CltParameterClient.class);

    public static GenericDAO<CltUserClient, Long> cltUserClientEJB = AutowiredUtils.getEJB(CltUserClient.class);

    public static GenericDAO<VCltUserClient, Long> vCltUserClientEJB = AutowiredUtils.getEJB(VCltUserClient.class);

    public static GenericDAO<VCltModuleParameterClient, Long> vCltModuleParameterClientEJB = AutowiredUtils.getEJB(VCltModuleParameterClient.class);

    public static GenericDAO<CltModuleParameterClient, Long> cltModuleParameterClientEJB = AutowiredUtils.getEJB(CltModuleParameterClient.class);

    public static GenericDAO<VCltModule, Long> vCltModuleEJB = AutowiredUtils.getEJB(VCltModule.class);

    public static GenericDAO<CltModule, Long> cltModuleEJB = AutowiredUtils.getEJB(CltModule.class);

    public static GenericDAO<PmModel, Long> pmModelEJB = AutowiredUtils.getEJB(PmModel.class);

    public static GenericDAO<VPmPageModule, Long> vPmPageModuleEJB = AutowiredUtils.getEJB(VPmPageModule.class);

    public static GenericDAO<PmPageParameterModule, Long> pmPageParameterModuleEJB = AutowiredUtils.getEJB(PmPageParameterModule.class);

    public static GenericDAO<VPmPageAttribute, Long> vPmPageAttributeEJB = AutowiredUtils.getEJB(VPmPageAttribute.class);

    public static GenericDAO<VCltUserModule, Long> vCltUserModuleEJB = AutowiredUtils.getEJB(VCltUserModule.class);

    //public static GenericDAO<VPmAttributeHidden, String> vPmAttributeHiddenEJB = AutowiredUtils.getEJB(VPmAttributeHidden.class);
    //public static GenericDAO<VPmAttributeMaxWord, String> vPmAttributeMaxWordEJB = AutowiredUtils.getEJB(VPmAttributeMaxWord.class);
    //public static GenericDAO<VPmAttributeReadonly, String> vPmAttributeReadonlyEJB = AutowiredUtils.getEJB(VPmAttributeReadonly.class);
    public static GenericDAO<VCltUserDetails, Long> vCltUserDetailsEJB = AutowiredUtils.getEJB(VCltUserDetails.class);

    public static GenericDAO<VCltClientLanguage, String> vCltClientLanguageEJB = AutowiredUtils.getEJB(VCltClientLanguage.class);

    public static GenericDAO<VCltClientDetails, Long> vCltClientDetailsEJB = AutowiredUtils.getEJB(VCltClientDetails.class);

    public static GenericDAO<VCltModuleType, Long> vCltModuleTypeEJB = AutowiredUtils.getEJB(VCltModuleType.class);

    public static GenericDAO<VCltUserCategory, Long> vCltUserCategoryEJB = AutowiredUtils.getEJB(VCltUserCategory.class);

    public static GenericDAO<VPmComposDetails, Long> vPmComposDetailsEJB = AutowiredUtils.getEJB(VPmComposDetails.class);

    public static GenericDAO<VSmProductType, Long> vSmProductTypeEJB = AutowiredUtils.getEJB(VSmProductType.class);

    public static GenericDAO<VSmProductSize, Long> vSmProductSizeEJB = AutowiredUtils.getEJB(VSmProductSize.class);

    public static GenericDAO<VSmProductStatus, Long> vSmProductStatusEJB = AutowiredUtils.getEJB(VSmProductStatus.class);

    public static GenericDAO<VSmProductFamily, Long> vSmProductFamilyEJB = AutowiredUtils.getEJB(VSmProductFamily.class);

    public static GenericDAO<VSmProductGroup, Long> vSmProductGroupEJB = AutowiredUtils.getEJB(VSmProductGroup.class);

    public static GenericDAO<VSmCustomerCategory, Long> vSmCustomerCategoryEJB = AutowiredUtils.getEJB(VSmCustomerCategory.class);

    //public static GenericDAO<SmReceptionProducts, Long> smReceptionProductsEJB = AutowiredUtils.getEJB(SmReceptionProducts.class);
    //public static GenericDAO<VSmReceptionProducts, Long> vSmReceptionProductsEJB = AutowiredUtils.getEJB(VSmReceptionProducts.class);
    public static GenericDAO<VSmDeposit, Long> vSmDepositEJB = AutowiredUtils.getEJB(VSmDeposit.class);

    public static GenericDAO<SmOrderSupplier, Long> smOrderSupplierEJB = AutowiredUtils.getEJB(SmOrderSupplier.class);

    public static GenericDAO<VSmOrderSupplier, Long> vSmOrderSupplierEJB = AutowiredUtils.getEJB(VSmOrderSupplier.class);

    public static GenericDAO<VSmOrderSupplierLine, Long> vSmOrderSupplierLineEJB = AutowiredUtils.getEJB(VSmOrderSupplierLine.class);

    public static GenericDAO<SmOrderSupplierLine, Long> smOrderSupplierLineEJB = AutowiredUtils.getEJB(SmOrderSupplierLine.class);

    public static GenericDAO<VSmOrderSupplierSummary, Long> vSmOrderSupplierSummaryEJB = AutowiredUtils.getEJB(VSmOrderSupplierSummary.class);

    public static GenericDAO<SmReturnReceipt, Long> smReturnReceiptEJB = AutowiredUtils.getEJB(SmReturnReceipt.class);

    public static GenericDAO<VSmReturnReceipt, Long> vSmReturnReceiptEJB = AutowiredUtils.getEJB(VSmReturnReceipt.class);

    public static GenericDAO<SmReturnReceiptLine, Long> smReturnReceiptLineEJB = AutowiredUtils.getEJB(SmReturnReceiptLine.class);

    public static GenericDAO<VSmReturnReceiptLine, Long> vSmReturnReceiptLineEJB = AutowiredUtils.getEJB(VSmReturnReceiptLine.class);

    public static GenericDAO<SmReturnReceiptStatus, Long> smReturnReceiptStatusEJB = AutowiredUtils.getEJB(SmReturnReceiptStatus.class);

    public static GenericDAO<VSmReturnReceiptSummary, Long> vSmReturnReceiptSummaryEJB = AutowiredUtils.getEJB(VSmReturnReceiptSummary.class);

    public static GenericServices genericServices = AutowiredUtils.getServices(GenericServices.class);

}
