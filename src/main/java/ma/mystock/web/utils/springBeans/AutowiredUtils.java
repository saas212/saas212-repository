package ma.mystock.web.utils.springBeans;

import ma.mystock.core.dao.GenericDAO;
import ma.mystock.core.services.GenericServices;

/**
 *
 * @author Abdessamad HALLAL
 */
public class AutowiredUtils {

    public static <T> GenericDAO getEJB(Class T) {
        try {
            GenericDAO<T, Long> service = (GenericDAO) SpringApplicationContext.getBean("GenericDAO");
            return service;
        } catch (Exception e) {
            return null;
        }

    }

    public static GenericServices getServices(Class T) {
        try {
            GenericServices service = (GenericServices) SpringApplicationContext.getBean("GenericServices");
            return service;
        } catch (Exception e) {
            return null;
        }

    }
}
