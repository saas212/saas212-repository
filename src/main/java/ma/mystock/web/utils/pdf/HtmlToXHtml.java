package ma.mystock.web.utils.pdf;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import org.apache.log4j.RollingFileAppender;
import org.w3c.tidy.Tidy;

public class HtmlToXHtml {

    private String xhtmlString;

    public String getXhtmlString() {
        return xhtmlString;
    }

    public StringReader convertHtmlToXHtml(String htmlContent) throws IOException {

        htmlContent = htmlContent.replaceAll("rel=\"stylesheet\"", "rel=\"stylesheet\" media=\"print\"");

        StringReader contentReader = new StringReader(htmlContent);
        StringWriter out = new StringWriter();

        try {

            Tidy tidy = new Tidy();
            //Tell Tidy to convert HTML to XML
            tidy.setXmlOut(true);
            //Convert files
            tidy.parse(contentReader, out);
            //	 System.out.print(out.toString());
            return new StringReader(out.toString());

        } catch (Exception e) {
            //logger.debug(e.getMessage());
            return null;
        } finally {
            //Clean up
            contentReader.close();
            out.close();
        }

    }

    public StringReader convertHtmlToXHtmlWithoutDTD(String htmlContent) throws IOException {

        htmlContent = htmlContent.replaceAll("rel=\"stylesheet\"", "rel=\"stylesheet\" media=\"print\"");

        StringReader contentReader = new StringReader(htmlContent);
        StringWriter out = new StringWriter();

        try {

            Tidy tidy = new Tidy();
            //Tell Tidy to convert HTML to XML
            tidy.setXmlOut(true);
            tidy.setDocType("omit");
            //tidy.setOutputEncoding("latin1");
            //Convert files
            /////////////////////////////////////////tidy.parse(contentReader, out);
            // System.out.print(out.toString());
            return new StringReader(out.toString());

        } catch (Exception e) {
            //logger.debug(e.getMessage());
            return null;
        } finally {
            //Clean up
            contentReader.close();
            out.close();
        }

    }

    public static StringReader convertHtmlToXHtmlWithoutDTD(String htmlContent, String context, String absoluteUrl) throws IOException {

        htmlContent = htmlContent.replaceAll("rel=\"stylesheet\"", "rel=\"stylesheet\" media=\"print\"");
        htmlContent = htmlContent.replaceAll(context, absoluteUrl);

        //System.out.println(htmlContent);
        StringReader contentReader = new StringReader(htmlContent);
        StringWriter out = new StringWriter();

        try {

            Tidy tidy = new Tidy();
            //Tell Tidy to convert HTML to XML
            tidy.setXmlOut(true);
            tidy.setDocType("omit");
            //tidy.setOutputEncoding("latin1");
            //Convert files
            //System.out.println("befor");
            contentReader.reset();
            tidy.parse(contentReader, out);
            //System.out.println("after");
            //System.out.print(out.toString());
            return new StringReader(out.toString());

        } catch (Exception e) {
            //logger.debug(e.getMessage());
            return null;
        } finally {
            //Clean up
            contentReader.close();
            out.close();
        }

    }

    public StringReader convertHtmlToXHtmlWithoutDTD2(String htmlContent, String context, String absoluteUrl) throws IOException {
        // RollingFileAppender appender = (RollingFileAppender) logger.getAppender(FILE_LOG);
        // logger.addAppender(appender);

        htmlContent = htmlContent.replaceAll("rel=\"stylesheet\"", "rel=\"stylesheet\" media=\"print\"");
        htmlContent = htmlContent.replaceAll("/" + context, absoluteUrl);

        StringReader contentReader = new StringReader(htmlContent);
        StringWriter out = new StringWriter();

        try {
            Tidy tidy = new Tidy();
            //Tell Tidy to convert HTML to XML
            tidy.setXmlOut(true);
            tidy.setDocType("omit");

            //Convert files
            tidy.parse(contentReader, out);
            xhtmlString = out.toString();
            xhtmlString = xhtmlString.replaceAll("https:-test", "https://uottawa-test");
            return new StringReader(xhtmlString);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            //Clean up
            contentReader.close();
            out.close();
        }

    }

    public String convertHtmlToXHtmlWithoutDTDS(String htmlContent) throws IOException {

        //traitement sur le string en input
        htmlContent = htmlContent.replaceAll("rel=\"stylesheet\"", "rel=\"stylesheet\" media=\"print\"");

        StringReader contentReader = new StringReader(htmlContent);
        StringWriter out = new StringWriter();

        try {

            Tidy tidy = new Tidy();
            //Tell Tidy to convert HTML to XML
            tidy.setXmlOut(true);
            tidy.setDocType("omit");
            //Convert files
            /////////////////////////////////////////tidy.parse(contentReader, out);
            // System.out.print(out.toString());
            return out.toString();

        } catch (Exception e) {
            //logger.debug(e.getMessage());
            return null;
        } finally {
            //Clean up
            contentReader.close();
            out.close();
        }

    }
}
