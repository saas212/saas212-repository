/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.pdf;

import java.util.Map;
import ma.mystock.web.utils.globals.GlobalsAttributs;

/**
 *
 * @author abdessamad hallal
 */
public class PdfUtils {

    public static String getUrl(Map<String, String> params) {

        StringBuilder url = new StringBuilder(GlobalsAttributs.INDEX_PDF);

        String paramSeparator = "?";
        for (Map.Entry<String, String> nameValue : params.entrySet()) {
            url.append(paramSeparator).append(nameValue.getKey()).append("=").append(nameValue.getValue());
            paramSeparator = "&";
        }

        return url.toString();
    }

    public static String getUrlDownloadPdf(Map<String, String> params) {

        if (params != null) {
            params.put("outputType", "pdf");
        }
        StringBuilder url = new StringBuilder(GlobalsAttributs.INDEX_PDF);

        String paramSeparator = "?";
        for (Map.Entry<String, String> nameValue : params.entrySet()) {
            url.append(paramSeparator).append(nameValue.getKey()).append("=").append(nameValue.getValue());
            paramSeparator = "&";
        }

        return url.toString();
    }

    public static String getUrlViewPdf(Map<String, String> params) {

        if (params != null) {
            params.put("outputType", "view");
        }
        StringBuilder url = new StringBuilder(GlobalsAttributs.INDEX_PDF);

        String paramSeparator = "?";
        for (Map.Entry<String, String> nameValue : params.entrySet()) {
            url.append(paramSeparator).append(nameValue.getKey()).append("=").append(nameValue.getValue());
            paramSeparator = "&";
        }

        return url.toString();
    }

    public static String getUrlViewHTML(Map<String, String> params) {

        if (params != null) {
            params.put("outputType", "html");
        }
        StringBuilder url = new StringBuilder(GlobalsAttributs.INDEX_PDF);

        String paramSeparator = "?";
        for (Map.Entry<String, String> nameValue : params.entrySet()) {
            url.append(paramSeparator).append(nameValue.getKey()).append("=").append(nameValue.getValue());
            paramSeparator = "&";
        }

        return url.toString();
    }

}
