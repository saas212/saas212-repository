/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.pdf;

/**
 *
 * @author Abdessamad HALLAL
 */
public class PdfGlobals {

    public static final Long PRINT_ALL_ORDER_SUPPLIERS_ID = 1L;
    public static final Long PRINT_ORDER_SUPPLIER_ID = 2L;

    public static final Long PRINT_ALL_RECEPTIONS_ID = 3L;
    public static final Long PRINT_RECEPTION_ID = 4L;

    public static final Long PRINT_ALL_ORDERS_ID = 5L;
    public static final Long PRINT_ORDER_ID = 6L;

    public static final Long PRINT_ALL_RETURN_RECEIPTS_ID = 7L;
    public static final Long PRINT_RETURN_RECEIPT_ID = 8L;

}
