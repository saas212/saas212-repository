/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.converter;

/**
 * Bytes to Kilobytes to Megabytes to Gigabytes KB MB GB TB Convert KB to MB
 *
 * @author Abdessamad HALLAL
 */
public class SizeConverter {

    public static String fromKilobytesToMegabytes(int kb) {
        return String.valueOf(kb / 1000);
    }

}
