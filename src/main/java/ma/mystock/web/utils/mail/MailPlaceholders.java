/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.mail;

/**
 *
 * @author Abdessamad HALLAL
 */
public class MailPlaceholders {

    // placeholder for user
    public static String USER_FULL_NAME = "__USER_FULL_NAME__";
    public static String USER_FIRST_NAME = "__USER_FIRST_NAME__";

    public static String CHANGE_PASSWORD_URL = "__CHANGE_PASSWORD_URL__";
    public static String EXPIRATION_TIME = "__EXPIRATION_TIME_";

    // placeolder globale
    public static String CLIENT_URL = "__CLIENT_URL__";
}
