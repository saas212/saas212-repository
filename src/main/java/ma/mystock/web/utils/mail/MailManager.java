/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.mail;

import ma.mystock.web.utils.springBeans.GloablsServices;

import java.util.Date;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

/**
 *
 * @author Abdessamad HALLAL Cette classe va remplcer la classe mail utils
 */
public class MailManager extends GloablsServices {

    public static void support(String subject, String message) {

        String title = "[MyStock][][ResearchPortal] : " + subject;
        Date d = new Date();
        String content = "<b> Nip </b> : ";
        content = content + "<b> Tag </b>: " + subject + " <br />";
        content = content + "<b> Message </b> : " + message + " <br />";
        content = content + "<b> Date </b> : " + d.toString() + " <br />";

        try {
            send("abdessamad.hallal@gmail.com", "abdessamad.hallal@gmail.com", title, content);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void send(String from, String to, String subject, String message, String... replyTo) throws Exception {

        String smtpServer = "";
        Properties props = System.getProperties();
        Session session;

        System.out.println("MailManager - Start sending from : " + from + ", to : " + to + ", title : " + subject);

        System.out.println("SMTP server : Gmail");
        //final String username = "eawards_support@evision.ca";
        //final String password = "1L0\\/E$uPP0R7";
        final String username = "softarys.notification@gmail.com";
        final String password = "softarys.notification1";

        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.ssl.trust", "*");
        session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        MimeMessage msg = new MimeMessage(session);
        msg.setHeader("Content-Type", "text/html; charset=UTF-8");
        msg.setFrom(new InternetAddress(from));
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));

        if (replyTo != null && replyTo.length > 0) {
            msg.setReplyTo(InternetAddress.parse(replyTo[0], false));
        }

        msg.setSubject(MimeUtility.encodeText(subject, "utf-8", "B"));
        msg.setContent(message, "text/html;charset=\"UTF-8\"");
        msg.setSentDate(new Date());

        Thread thread = new Thread() {

            @Override
            public void run() {
                System.out.println("MailManager - send : Start Run");
                try {
                    Transport.send(msg);
                } catch (Exception e) {
                    System.out.println("Exception : MailManager - send : Exception Run");
                    e.printStackTrace();
                }
                System.out.println("MailManager - send : End Run");
            }

        };
        thread.start();
        System.out.println("MailManager - End sending : OK");

    }
    /*
     
     public static void create(MailParameters mailParameters) {
     String language;
     try {

     for (VLovPerson person : mailParameters.getRecipientList()) {

     language = getCorrespondenceLanguageLabel(person.getFirstLanguageCode());
     System.out.println(" create : " + language + " -  " + person.getEmailAddress());
     // get message type enabled
     List<MessageTypeEnabled> messageTypeEnabledList = null;
     if (mailParameters.getCompositionId() != null) {
     messageTypeEnabledList = messagingManager.findMessageTypeEnabledByMessageTypeIdFbCompositionId(language, mailParameters.getMessageType(), mailParameters.getCompositionId());
     } else {
     messageTypeEnabledList = messagingManager.findMessageTypeEnabledByMessageTypeId(language, mailParameters.getMessageType());
     }

     if (messageTypeEnabledList == null || messageTypeEnabledList.isEmpty()) {
     // mail to Support Attention le message type mailParameters.getMessageType() 
     // avec la langue mailParameters.getCorrespondenceLanguage() n'extste pas dans la BD
     System.out.println("messageTypeEnabled not existe");
     return;
     }
     System.out.println("found ");
     MessageTypeEnabled messageTypeEnabled = messageTypeEnabledList.get(0);
     // message template
     MessageTemplate messageTemplate = messageTypeEnabled.getMessageTemplate();

     // replace paramètre niveau 1 : 
     String subject = replaceByPlaceholders(messageTemplate.getSubject(), mailParameters.getPlaceholders());
     String content = replaceByPlaceholders(messageTemplate.getContent(), mailParameters.getPlaceholders());

     //replace paramètre niveau 2 : person
     subject = replaceByVLovPerson(subject, person);
     content = replaceByVLovPerson(content, person);

     //replace paramètre niveau 3 : demande
     subject = replaceByDemId(subject, mailParameters.getDemId(), language);
     content = replaceByDemId(content, mailParameters.getDemId(), language);

     // message receipient
     MessageRecipient messageRecipient = new MessageRecipient();
     messageRecipient.setEmailAddress(person.getEmailAddress());
     messageRecipient.setNip(person.getNip());
     messageRecipient.setSalutationCode(person.getSalutationCode());
     messageRecipient.setFirstName(person.getFirstName());
     messageRecipient.setLastName(person.getLastName());
     messageRecipient = messagingManager.makePersistent(messageRecipient);

     // message schedule
     MessageSchedule messageSchedule = new MessageSchedule();
     messageSchedule.setMessageScheduleTypeId(MessagingGlobals.MESSAGESCHEDULETYPE_IMMEDIATE);
     messageSchedule.setMessageTemplateId(messageTemplate.getId());
     messageSchedule.setContent(content);
     messageSchedule.setSubject(subject);
     messageSchedule.setMessageTypeEnabledId(messageTypeEnabled.getId());
     messageSchedule.setMessageRecipient(messageRecipient);
     messageSchedule.setDemId(mailParameters.getDemId());
     messageSchedule = messagingManager.makePersistent(messageSchedule);

     // send and status
     try {

     send(messageTemplate.getSender(), person.getEmailAddress(), subject, content);

     MessageLog messageLog = new MessageLog();
     messageLog.setMessageScheduleId(messageSchedule.getId());
     messageLog.setMessageLogStatusId(MessagingGlobals.MESSAGELOGSTATUS_SENT);
     messageLog.setLogDate(new Timestamp(System.currentTimeMillis()));
     messageLog = messagingManager.makePersistent(messageLog);

     } catch (MailException e) {
     e.printStackTrace();

     MessageLog messageLog = new MessageLog();
     messageLog.setMessageScheduleId(messageSchedule.getId());
     messageLog.setMessageLogStatusId(MessagingGlobals.MESSAGELOGSTATUS_ERROR_WHILE_SENDING);
     messageLog.setLogDate(new Timestamp(System.currentTimeMillis()));
     messageLog.setLogComment(e.getMessage());
     messageLog = messagingManager.makePersistent(messageLog);

     } catch (Exception e) {
     e.printStackTrace();
     }

     }
     } catch (Exception ex) {
     ex.printStackTrace();
     }
     }

     */
}
