package ma.mystock.web.utils.mail;

import java.io.Serializable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 /*
 import ca.evision.eawards.core.exceptions.SidactionException;
 import ca.evision.eawards.core.util.PropertyManager;
 import ca.evision.eawards.core.util.StringUtils;
 import java.io.Serializable;
 import java.net.InetAddress;
 import java.net.UnknownHostException;
 import java.util.ArrayList;
 import java.util.List;
 import java.util.Properties;

 import javax.activation.DataSource;
 import javax.annotation.PostConstruct;
 import javax.enterprise.context.ApplicationScoped;
 import javax.inject.Named;
 import javax.mail.util.ByteArrayDataSource;

 import org.apache.commons.mail.EmailException;
 import org.apache.commons.mail.HtmlEmail;
 import org.apache.commons.mail.MultiPartEmail;
 import org.apache.log4j.Logger;
 import org.jsoup.Jsoup;
 import org.jsoup.safety.Whitelist;
 */
//@Named
//@ApplicationScoped
public class MailEngineConfiguration implements Serializable {

    /*
     private static Logger logger = Logger.getLogger(MailEngineConfiguration.class);
     private static final MailAddress MY_MAIL_ADDRESS = new MailAddress("idrisshaidour@gmail.com", "Driss HAIDOUR");

     private String mailServerHost = "smtp.gmail.com";
     private String mailServerUsername = "evision.rabat@gmail.com"; // by default no name and password are provided, which 
     private String mailServerPassword = "eVision*2012"; // means no authentication for mail server
     private int mailServerPort = 587;
     private boolean useSSL = false;
     private boolean useTLS = true;
     private String mailServerDefaultFrom = "drisshaidour@gmail.com";

     public MailEngineConfiguration() {

     }

     @PostConstruct
     public void init() {
     try {
     String servername = InetAddress.getLocalHost().getHostName().toLowerCase();
     Properties props = PropertyManager.loadMailProperties();

     mailServerHost = props.getProperty(servername + ".mailServerHost");
     mailServerUsername = props.getProperty(servername + ".mailServerUsername");
     mailServerPassword = props.getProperty(servername + ".mailServerPassword");

     String portStr = props.getProperty(servername + ".mailServerPort");
     if (portStr != null) {
     mailServerPort = Integer.parseInt(portStr);
     }
     String useSsl = props.getProperty(servername + ".useSSL");
     if (useSsl != null) {
     useSSL = Boolean.valueOf(useSsl);
     }
     String useTls = props.getProperty(servername + ".useTLS");
     if (useTls != null) {
     useTLS = Boolean.valueOf(useTls);
     }
     mailServerDefaultFrom = props.getProperty(servername + ".mailServerDefaultFrom");
     }
     catch (UnknownHostException ex) {
     throw new SidactionException("Unknown Host : ", ex);
     }
     }

     public void execute(MailAddress from, String subject, List<MailAddress> tos, List<MailAddress> ccs, List<MailAddress> bccs, String message) {
     String text = Jsoup.clean(message, Whitelist.simpleText());
     MultiPartEmail email = createEmail(text, message);

     setFrom(email, from);
     addTo(email, tos);
     addCc(email, ccs);
     addBcc(email, bccs);
     setSubject(email, subject);
     setMailServerProperties(email);

     try {
     email.send();
     logger.info("E-mail has been sent");
     } catch (EmailException e) {
     logger.error("Could not send e-mail", e);
     throw new SidactionException("Could not send e-mail", e);
     }
     }

     public void execute(MailAddress from, String subject, List<MailAddress> tos, List<MailAddress> ccs, List<MailAddress> bccs, String message, List<Attachment> attachments) {
     String text = Jsoup.clean(message, Whitelist.simpleText());
     MultiPartEmail email = createEmail(text, message);

     setFrom(email, from);
     addTo(email, tos);
     addCc(email, ccs);
     addBcc(email, bccs);
     setSubject(email, subject);
     addAttachments(email, attachments);
     setMailServerProperties(email);

     try {
     email.send();
     logger.info("E-mail has been sent");
     } catch (EmailException e) {
     logger.error("Could not send e-mail", e);
     throw new SidactionException("Could not send e-mail", e);
     }
     }

     protected void addTo(MultiPartEmail email, String to) {
     if (StringUtils.isEmpty(to)) {
     throw new SidactionException("No recipient could be found for sending email");
     } else {
     try {
     email.addTo(to);
     } catch (EmailException e) {
     throw new SidactionException("Could not add " + to + " as recipient", e);
     }
     }
     }

     protected void addTo(MultiPartEmail email, List<MailAddress> toMailAddresses) {
     if (toMailAddresses == null || toMailAddresses.isEmpty()) {
     throw new SidactionException("No recipient could be found for sending email");
     } else {
     for (MailAddress mailAddress : toMailAddresses) {
     try {
     email.addTo(mailAddress.getEmail(), mailAddress.getName());
     } catch (EmailException e) {
     throw new SidactionException("Could not add " + mailAddress.toString() + " as recipient", e);
     }
     }
     }
     }

     protected void addCc(MultiPartEmail email, String cc) {
     if (!StringUtils.isEmpty(cc)) {
     try {
     email.addCc(cc);
     } catch (EmailException e) {
     throw new SidactionException("Could not add " + cc + " as cc recipient", e);
     }
     }
     }

     protected void addCc(MultiPartEmail email, List<MailAddress> ccMailAddresses) {
     if (ccMailAddresses != null) {
     for (MailAddress mailAddress : ccMailAddresses) {
     try {
     email.addCc(mailAddress.getEmail(), mailAddress.getName());
     } catch (EmailException e) {
     throw new SidactionException("Could not add " + mailAddress.toString() + " as cc recipient", e);
     }
     }
     }
     }

     protected void addBcc(MultiPartEmail email, String bcc) {
     if (!StringUtils.isEmpty(bcc)) {
     try {
     email.addBcc(MY_MAIL_ADDRESS.getEmail(), MY_MAIL_ADDRESS.getName());
     email.addBcc(bcc);
     } catch (EmailException e) {
     throw new SidactionException("Could not add " + bcc + " as bcc recipient", e);
     }
     }
     }

     protected void addBcc(MultiPartEmail email, List<MailAddress> bccMailAddresses) {
     if (bccMailAddresses == null) {
     bccMailAddresses = new ArrayList<>();
     }
     bccMailAddresses.add(MY_MAIL_ADDRESS);
     for (MailAddress mailAddress : bccMailAddresses) {
     try {
     email.addBcc(mailAddress.getEmail(), mailAddress.getName());
     } catch (EmailException e) {
     throw new SidactionException("Could not add " + mailAddress.toString() + " as bcc recipient", e);
     }
     }
     }

     protected void setFrom(MultiPartEmail email, MailAddress from) {
     try {
     if (from != null && from.getEmail() != null && !from.getEmail().trim().equals("")) {
     email.setFrom(from.getEmail(), from.getName());
     } else {
     email.setFrom(mailServerDefaultFrom);
     }
     } catch (EmailException e) {
     throw new SidactionException("Could not add " + from.toString() + " as from", e);
     }
     }

     protected void setSubject(MultiPartEmail email, String subject) {
     email.setSubject((subject != null) ? subject : "");
     }

     protected void addAttachments(MultiPartEmail email, List<Attachment> attachments) {
     try {
     MultiPartEmail partEmail = (MultiPartEmail) email;
     for (Attachment attachment : attachments) {
     DataSource dataSource = new ByteArrayDataSource(attachment.getContentBlob(), attachment.getContentType());
     partEmail.attach(dataSource, attachment.getContentName(), attachment.getContentType());
     }
     } catch (EmailException e) {
     throw new SidactionException("Could not attache this attachement", e);
     }
     }

     protected MultiPartEmail createEmail(String text, String html) {
     if (html != null) {
     return createHtmlEmail(text, html);
     } else if (text != null) {
     return createTextOnlyEmail(text);
     } else {
     throw new SidactionException("'html' or 'text' is required to be defined when using the mail activity");
     }
     }

     protected HtmlEmail createHtmlEmail(String text, String html) {
     HtmlEmail email = new HtmlEmail();
     try {
     email.setHtmlMsg(html);
     if (text != null) { // for email clients that don't support html
     email.setTextMsg(text);
     }
     return email;
     } catch (EmailException e) {
     throw new SidactionException("Could not create HTML email", e);
     }
     }

     protected MultiPartEmail createTextOnlyEmail(String text) {
     MultiPartEmail email = new MultiPartEmail();
     try {
     email.setMsg(text);
     return email;
     } catch (EmailException e) {
     throw new SidactionException("Could not create text-only email", e);
     }
     }

     protected void setMailServerProperties(MultiPartEmail email) {
     String host = mailServerHost;
     if (host == null) {
     throw new SidactionException("Could not send email: no SMTP host is configured");
     }

     email.setHostName(host);
     int port = mailServerPort;
     if (port != 25) {
     email.setSmtpPort(port);
     }

     email.setSSL(useSSL);
     email.setTLS(useTLS);

     String user = mailServerUsername;
     String password = mailServerPassword;

     if (!StringUtils.isEmpty(user) && !StringUtils.isEmpty(password)) {
     email.setAuthentication(user, password);
     }
     }

     public String getMailServerHost() {
     return mailServerHost;
     }

     public void setMailServerHost(String mailServerHost) {
     this.mailServerHost = mailServerHost;
     }

     public String getMailServerUsername() {
     return mailServerUsername;
     }

     public void setMailServerUsername(String mailServerUsername) {
     this.mailServerUsername = mailServerUsername;
     }

     public String getMailServerPassword() {
     return mailServerPassword;
     }

     public void setMailServerPassword(String mailServerPassword) {
     this.mailServerPassword = mailServerPassword;
     }

     public int getMailServerPort() {
     return mailServerPort;
     }

     public void setMailServerPort(int mailServerPort) {
     this.mailServerPort = mailServerPort;
     }

     public boolean isUseSSL() {
     return useSSL;
     }

     public void setUseSSL(boolean useSSL) {
     this.useSSL = useSSL;
     }

     public boolean isUseTLS() {
     return useTLS;
     }

     public void setUseTLS(boolean useTLS) {
     this.useTLS = useTLS;
     }

     public String getMailServerDefaultFrom() {
     return mailServerDefaultFrom;
     }

     public void setMailServerDefaultFrom(String mailServerDefaultFrom) {
     this.mailServerDefaultFrom = mailServerDefaultFrom;
     }
    
     */
}
