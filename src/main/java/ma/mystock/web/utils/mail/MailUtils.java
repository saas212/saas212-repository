/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.mail;

import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.PasswordAuthentication;

/**
 *
 * @author Abdessamad HALLAL
 */
public class MailUtils {

    // smtp.menara.ma
    static String smtpServer = "smtp.menara.ma";

    public static void sendMail(String smpt, String from, String to, String subject, String message, String... replyTo) {

        try {

            Properties props = System.getProperties();

            props.setProperty("mail.smtp.auth", "false");
            props.put("mail.smtp.host", smtpServer);
            props.put("mail.smtp.port", "25");

            Session session = Session.getInstance(props);

            Message msg = new MimeMessage(session);

            msg.setFrom(new InternetAddress(from));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));

            msg.setSubject(subject);
            msg.setContent(message, "text/html");
            msg.setSentDate(new Date());

            Transport.send(msg);
            System.out.println("Message sent OK.");

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public static void gmail(int i, String sujet, String content, String to) throws Exception {

        Properties props = System.getProperties();
        Session session;

        //System.out.println("Using SMTP Gmail ... for Local environment ");
        //final String username = "softarys.rabat@gmail.com";
        //final String password = "eVisionInc";
        final String username = "abdessamad.hallal@gmail.com";
        final String password = "XXXX";

        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.ssl.trust", "*");

        //System.out.println("Mail Server Properties have been setup successfully..");
        session = Session.getInstance(props,
                new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        //System.out.println("Mail Session has been created successfully..");
        Message msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress("abdessamad.hallal@gmail.com"));
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));

        msg.setSubject(sujet);
        msg.setContent(content, "text/html");
        msg.setSentDate(new Date());

        //if( to.equalsIgnoreCase("abdessamadh@evision.ca")){
        //Transport.send(msg);
        // }
        System.out.println(i + " Message sent OK for : " + to);

    }

    public static void hotmail(int i, String sujet, String content, String to) throws Exception {

        Properties props = System.getProperties();

        final String username = "sybaway@hotmail.fr";
        final String password = "Evision2012";

        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.live.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.user", username);
        props.put("mail.smtp.pwd", password);
        Session session = Session.getDefaultInstance(props);
        session.setDebug(true);
        Transport trans = session.getTransport("smtp");
        trans.connect("smtp.live.com", 587, username, password);

        Message msg = new MimeMessage(session);

        msg.setFrom(new InternetAddress("sybaway@hotmail.fr"));
        msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));

        msg.setSubject(sujet);
        msg.setContent(content, "text/html");
        msg.setSentDate(new Date());

        //if (to.equalsIgnoreCase("sybaway@hotmail.fr")) {
        trans.send(msg);
        //}

        System.out.println(i + " Message with smtp.live sent OK for : " + to);

    }

    public static void menara(int i, String sujet, String content, String to) throws Exception {

        try {

            Properties props = System.getProperties();

            props.put("mail.smtp.auth", "false");
            props.put("mail.smtp.host", "smtp.menara.ma");
            props.put("mail.smtp.port", "25");

            //Session session = Session.getInstance(props);
            Session session = Session.getDefaultInstance(props, null);

            Message msg = new MimeMessage(session);

            msg.setFrom(new InternetAddress("abdessamad.hallal@gmail.com"));
            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to, false));

            msg.setSubject(sujet);
            msg.setContent(content, "text/html");
            msg.setSentDate(new Date());

            Transport.send(msg);
            System.out.println(i + " : Message sent OK.");

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

}
