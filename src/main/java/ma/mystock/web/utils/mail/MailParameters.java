package ma.mystock.web.utils.mail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Abdessamad HALLAL
 */
public class MailParameters {

    private Long messageId;
    private List<Long> recipientList;
    private boolean schedule;

    private Map<String, String> placeholders;

    /* ################# methods construct ################# */
    public MailParameters() {
        schedule = false;
        placeholders = new HashMap<>();
        recipientList = new ArrayList<>();
    }

    /* ################# methods utils ################# */
    public void addPlaceholder(String key, String value) {
        placeholders.put(key, value);
    }

    public List<Long> getRecipientList() {
        return recipientList;
    }

    public void setRecipientList(List<Long> recipientList) {
        this.recipientList = recipientList;
    }

    public boolean isSchedule() {
        return schedule;
    }

    public void setSchedule(boolean schedule) {
        this.schedule = schedule;
    }

    public Map<String, String> getPlaceholders() {
        return placeholders;
    }

    public void setPlaceholders(Map<String, String> placeholders) {
        this.placeholders = placeholders;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

}
