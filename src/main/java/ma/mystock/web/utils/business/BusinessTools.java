/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.business;

/**
 *
 * @author anasshajami
 */
public class BusinessTools {

    public static Double TVA_DEFAULT_VALUE = new Double("20.00");
    public static Double REMISE_DEFAULT_VALUE = new Double("0");
    public static Long QUANTITY_DEFAULT_VALUE = 1L;

    public static Double getTva() {
        return TVA_DEFAULT_VALUE;
    }

    public static Double getRemise() {
        return REMISE_DEFAULT_VALUE;
    }

    public static Long getQuantity() {
        return QUANTITY_DEFAULT_VALUE;
    }

}
