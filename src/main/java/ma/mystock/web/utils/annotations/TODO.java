package ma.mystock.web.utils.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

@Documented
@Retention(SOURCE)
public @interface TODO {

    String value();

    Level level() default Level.NORMAL;

    public static enum Level {

        BLOCKER, CRITICAL, MAJOR, NORMAL, MINORX, TRIVIAL
    };

}
