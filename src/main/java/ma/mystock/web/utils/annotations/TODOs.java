package ma.mystock.web.utils.annotations;

import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

@Documented
@Retention(SOURCE)
public @interface TODOs {

    TODO[] value();
}
