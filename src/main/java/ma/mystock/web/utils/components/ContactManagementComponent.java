package ma.mystock.web.utils.components;

import java.util.ArrayList;
import java.util.List;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.model.SelectItem;
import ma.mystock.core.dao.entities.CtaEntityEmail;
import ma.mystock.core.dao.entities.CtaEntityFax;
import ma.mystock.core.dao.entities.CtaEntityLocation;
import ma.mystock.core.dao.entities.CtaEntityPhone;
import ma.mystock.core.dao.entities.CtaEntityWeb;
import ma.mystock.core.dao.entities.views.VCtaEmailType;
import ma.mystock.core.dao.entities.views.VCtaEntityEmail;
import ma.mystock.core.dao.entities.views.VCtaEntityFax;
import ma.mystock.core.dao.entities.views.VCtaEntityLocation;
import ma.mystock.core.dao.entities.views.VCtaEntityPhone;
import ma.mystock.core.dao.entities.views.VCtaEntityWeb;
import ma.mystock.core.dao.entities.views.VCtaFaxType;
import ma.mystock.core.dao.entities.views.VCtaLocationType;
import ma.mystock.core.dao.entities.views.VCtaPhoneType;
import ma.mystock.core.dao.entities.views.VCtaWebType;
import ma.mystock.web.utils.functionUtils.StringUtils;
import ma.mystock.web.utils.lovs.LovsUtils;
import ma.mystock.web.utils.sessions.SessionsGetter;
import ma.mystock.web.utils.springBeans.GloablsServices;

/**
 *
 * @author anasshajami
 */
public class ContactManagementComponent extends GloablsServices {

    private Long entityId;

    // ####### WebSite   #########
    private List<VCtaEntityWeb> vCtaEntityWebList;
    private List<VCtaWebType> vCtaWebTypeList;
    private List<SelectItem> vCtaWebTypeItems;

    private CtaEntityWeb ctaEntityWeb;

    private String ctaWebType;
    private String ctaWebUrl;
    private String ctaWebPriority;

    private boolean showFormCtaEntityWeb;

    private Long ctaEntityWebId;

    // ####### Location   #########
    private List<VCtaEntityLocation> vCtaEntityLocationList;
    private List<VCtaLocationType> vCtaLocationTypeList;
    private List<SelectItem> vCtaLocationTypeItems;
    private List<SelectItem> vInfCountryItems;
    private List<SelectItem> vInfCityItems;

    private CtaEntityLocation ctaEntityLocation;

    private String ctaLocationLine1;
    private String ctaLocationLine2;
    private String ctaLocationLine3;
    private String ctaLocationCountry;
    private String ctaLocationCity;
    private String ctaLocationPostalCode;
    private String ctaLocationType;
    private String ctaLocationPriority;

    private boolean showFormCtaEntityLocation;

    private Long ctaEntityLocationId;

    // ####### Phones   #########
    private List<VCtaEntityPhone> vCtaEntityPhoneList;
    private List<VCtaPhoneType> vCtaPhoneTypeList;
    private List<SelectItem> vCtaPhoneTypeItems;

    private CtaEntityPhone ctaEntityPhone;

    private String ctaPhoneCountryCode;
    private String ctaPhoneNumber;
    private String ctaPhoneType;
    private String ctaPhonePriority;

    private boolean showFormCtaEntityPhone;

    private Long ctaEntityPhoneId;

    // ####### Faxes   #########
    private List<VCtaEntityFax> vCtaEntityFaxList;
    private List<VCtaFaxType> vCtaFaxTypeList;
    private List<SelectItem> vCtaFaxTypeItems;

    private CtaEntityFax ctaEntityFax;

    private String ctaFaxCountryCode;
    private String ctaFaxNumber;
    private String ctaFaxType;
    private String ctaFaxPriority;

    private boolean showFormCtaEntityFax;

    private Long ctaEntityFaxId;

    // ####### Emailes   #########
    private List<VCtaEntityEmail> vCtaEntityEmailList;
    private List<VCtaEmailType> vCtaEmailTypeList;
    private List<SelectItem> vCtaEmailTypeItems;

    private CtaEntityEmail ctaEntityEmail;

    private String ctaEmailAdresse;
    private String ctaEmailType;
    private String ctaEmailPriority;

    private boolean showFormCtaEntityEmail;

    private Long ctaEntityEmailId;

    public void init(Long entityId) {
        this.entityId = entityId;

        // ####### Site Web  #########
        initCtaEntityWeb();
        // ####### Location   #########
        initCtaEntityLocation();
        // ####### Phones   #########
        initCtaEntityPhone();
        // ####### Faxs   #########
        initCtaEntityFax();
        // ####### Emails   #########
        initCtaEntityEmail();
    }

    /*
     * ################# Location Managment  #################
     */
    public void initCtaEntityLocation() {
        vCtaEntityLocationList = genericServices.findVCtaEntityLocationByEntityId(entityId);
        System.out.println(" vCtaEntityLocationList " + vCtaEntityLocationList.size());
        showFormCtaEntityLocation = false;
    }

    public void initAddCtaEntityLocation() {
        vCtaLocationTypeList = genericServices.findVCtaLocationTypeByCltModuleId(SessionsGetter.getCltClientId());
        vCtaLocationTypeItems = new ArrayList<>();

        for (VCtaLocationType o : vCtaLocationTypeList) {
            vCtaLocationTypeItems.add(new SelectItem(o.getId(), o.getName()));
        }

        vInfCountryItems = LovsUtils.getInfCountryItem();
        vInfCityItems = new ArrayList<>();

    }

    public void initAttributesCtaEntityLocation() {
        System.out.println(" call initAttributCtaEntityLocation ");
        ctaLocationLine1 = "";
        ctaLocationLine2 = "";
        ctaLocationLine3 = "";
        ctaLocationCountry = "";
        ctaLocationCity = "";
        ctaLocationPostalCode = "";
        ctaLocationPriority = "";
        ctaLocationType = "";

    }

    public void addCtaEntityLocation(AjaxBehaviorEvent e) {
        System.out.println(" call  addCtaEntityLocation");
        initAddCtaEntityLocation();
        initAttributesCtaEntityLocation();
        showFormCtaEntityLocation = true;
    }

    public void loadCitiesCtaEntityLocation(AjaxBehaviorEvent e) {
        vInfCityItems = LovsUtils.getInfCityByCountryIdItems(StringUtils.stringToLang(ctaLocationCountry));
    }

    public void saveCtaEntityLocation(AjaxBehaviorEvent e) {
        System.out.println(" call  saveCtaEntityLocation");
        ctaEntityLocation = new CtaEntityLocation();
        ctaEntityLocation.setAddressLine1(ctaLocationLine1);
        ctaEntityLocation.setAddressLine2(ctaLocationLine2);
        ctaEntityLocation.setAddressLine3(ctaLocationLine3);
        ctaEntityLocation.setLocationTypeId(StringUtils.stringToLang(ctaLocationType));
        ctaEntityLocation.setPostalCode(ctaLocationPostalCode);
        ctaEntityLocation.setEntityId(entityId);
        ctaEntityLocation.setInfCountryId(StringUtils.stringToLang(ctaLocationCountry));
        ctaEntityLocation.setInfCityId(StringUtils.stringToLang(ctaLocationCity));
        ctaEntityLocation.setPriority(StringUtils.stringToLang(ctaLocationPriority));
        ctaEntityLocation = (CtaEntityLocation) genericServices.genericPersiste(ctaEntityLocation);
        initCtaEntityLocation();

        initAttributesCtaEntityLocation();

        showFormCtaEntityLocation = false;
    }

    public void cancelCtaEntityLocation(AjaxBehaviorEvent e) {
        System.out.println(" call cancelCtaEntityLocation ");
        showFormCtaEntityLocation = false;
    }

    public void deleteCtaEntityLocation(AjaxBehaviorEvent e) {
        ctaEntityLocationId = (Long) e.getComponent().getAttributes().get("ctaEntityLocationId");
        System.out.println(" call  deleteCtaEntityLocation : " + ctaEntityLocationId);

        ctaEntityLocation = genericServices.findCtaEntityLocationById(ctaEntityLocationId);

        System.out.println(" pass  : " + ctaEntityLocation.getId());
        genericServices.genericDelete(ctaEntityLocation);
        System.out.println("not pass ");
        initCtaEntityLocation();
    }

    /*
     * ################# Phone Managment  #################
     */
    public void initCtaEntityPhone() {
        vCtaEntityPhoneList = genericServices.findVCtaEntityPhoneByEntityId(entityId);
        showFormCtaEntityPhone = false;
    }

    public void initAddCtaEntityPhone() {
        vCtaPhoneTypeList = genericServices.findVCtaPhoneTypeByCltModuleId(SessionsGetter.getCltClientId());
        vCtaPhoneTypeItems = new ArrayList<>();

        for (VCtaPhoneType o : vCtaPhoneTypeList) {
            vCtaPhoneTypeItems.add(new SelectItem(o.getId(), o.getName()));
        }

    }

    public void initAttributesCtaEntityPhone() {
        ctaPhoneCountryCode = "";
        ctaPhoneNumber = "";
        ctaPhoneType = "";
        ctaPhonePriority = "";
    }

    public void addCtaEntityPhone(AjaxBehaviorEvent e) {
        System.out.println(" call  addCtaEntityPhone");
        initAddCtaEntityPhone();
        initAttributesCtaEntityPhone();
        showFormCtaEntityPhone = true;
    }

    public void saveCtaEntityPhone(AjaxBehaviorEvent e) {
        System.out.println(" call  saveCtaEntityPhone");
        ctaEntityPhone = new CtaEntityPhone();
        ctaEntityPhone.setEntityId(entityId);
        ctaEntityPhone.setCountryCode(ctaPhoneCountryCode);
        ctaEntityPhone.setNumber(ctaPhoneNumber);
        ctaEntityPhone.setPhoneTypeId(StringUtils.stringToLang(ctaPhoneType));
        ctaEntityPhone.setPriority(StringUtils.stringToLang(ctaPhonePriority));
        ctaEntityPhone = (CtaEntityPhone) genericServices.genericPersiste(ctaEntityPhone);
        initCtaEntityPhone();
        initAttributesCtaEntityPhone();
        showFormCtaEntityPhone = false;
    }

    public void cancelCtaEntityPhone(AjaxBehaviorEvent e) {
        System.out.println(" call cancelCtaEntityPhone ");
        showFormCtaEntityPhone = false;
    }

    public void deleteCtaEntityPhone(AjaxBehaviorEvent e) {
        ctaEntityPhoneId = (Long) e.getComponent().getAttributes().get("ctaEntityPhoneId");
        System.out.println(" call  deletCtaEntityPhone : " + ctaEntityPhoneId);

        ctaEntityPhone = genericServices.findCtaEntityPhoneById(ctaEntityPhoneId);

        System.out.println(" pass  : " + ctaEntityPhone.getId());
        genericServices.genericDelete(ctaEntityPhone);
        System.out.println("not pass ");
        initCtaEntityPhone();
    }

    /*
     * ################# Faxe Managment  #################
     */
    public void initCtaEntityFax() {
        vCtaEntityFaxList = genericServices.findVCtaEntityFaxByEntityId(entityId);
        showFormCtaEntityFax = false;
    }

    public void initAddCtaEntityFax() {
        vCtaFaxTypeList = genericServices.findVCtaFaxTypeByCltModuleId(SessionsGetter.getCltClientId());
        vCtaFaxTypeItems = new ArrayList<>();

        for (VCtaFaxType o : vCtaFaxTypeList) {
            vCtaFaxTypeItems.add(new SelectItem(o.getId(), o.getName()));
        }

    }

    public void initAttributesCtaEntityFax() {
        ctaFaxCountryCode = "";
        ctaFaxNumber = "";
        ctaFaxType = "";
        ctaFaxPriority = "";
    }

    public void addCtaEntityFax(AjaxBehaviorEvent e) {
        System.out.println(" call  addCtaEntityFax");
        initAddCtaEntityFax();
        initAttributesCtaEntityFax();
        showFormCtaEntityFax = true;
    }

    public void saveCtaEntityFax(AjaxBehaviorEvent e) {
        System.out.println(" call  saveCtaEntityFax");
        ctaEntityFax = new CtaEntityFax();
        ctaEntityFax.setEntityId(entityId);
        ctaEntityFax.setCountryCode(ctaFaxCountryCode);
        ctaEntityFax.setNumber(ctaFaxNumber);
        ctaEntityFax.setFaxTypeId(StringUtils.stringToLang(ctaFaxType));
        ctaEntityFax.setPriority(StringUtils.stringToLang(ctaFaxPriority));
        ctaEntityFax = (CtaEntityFax) genericServices.genericPersiste(ctaEntityFax);
        initCtaEntityFax();
        initAttributesCtaEntityFax();
        showFormCtaEntityFax = false;
    }

    public void cancelCtaEntityFax(AjaxBehaviorEvent e) {
        System.out.println(" call cancelCtaEntityFax ");
        showFormCtaEntityFax = false;
    }

    public void deleteCtaEntityFax(AjaxBehaviorEvent e) {
        ctaEntityFaxId = (Long) e.getComponent().getAttributes().get("ctaEntityFaxId");
        System.out.println(" call  deletCtaEntityFax : " + ctaEntityFaxId);

        ctaEntityFax = genericServices.findCtaEntityFaxById(ctaEntityFaxId);

        System.out.println(" pass  : " + ctaEntityFax.getId());
        genericServices.genericDelete(ctaEntityFax);
        System.out.println("not pass ");
        initCtaEntityFax();
    }

    /*
     * ################# Email Managment  #################
     */
    public void initCtaEntityEmail() {
        vCtaEntityEmailList = genericServices.findVCtaEntityEmailByEntityId(entityId);
        showFormCtaEntityEmail = false;
    }

    public void initAddCtaEntityEmail() {
        vCtaEmailTypeList = genericServices.findVCtaEmailTypeByCltModuleId(SessionsGetter.getCltClientId());
        vCtaEmailTypeItems = new ArrayList<>();

        for (VCtaEmailType o : vCtaEmailTypeList) {
            vCtaEmailTypeItems.add(new SelectItem(o.getId(), o.getName()));
        }
    }

    public void initAttributesCtaEntityEmail() {
        ctaEmailType = "";
        ctaEmailAdresse = "";
        ctaEmailPriority = "";
    }

    public void addCtaEntityEmail(AjaxBehaviorEvent e) {
        System.out.println(" call  addCtaEntityEmail");
        initAddCtaEntityEmail();
        initAttributesCtaEntityEmail();
        showFormCtaEntityEmail = true;
    }

    public void saveCtaEntityEmail(AjaxBehaviorEvent e) {
        System.out.println(" call  saveCtaEntityEmail : " + ctaEmailPriority);
        ctaEntityEmail = new CtaEntityEmail();
        ctaEntityEmail.setEntityId(entityId);
        ctaEntityEmail.setEmailAddress(ctaEmailAdresse);
        ctaEntityEmail.setEmailTypeId(StringUtils.stringToLang(ctaEmailType));
        ctaEntityEmail.setPriority(StringUtils.stringToLang(ctaEmailPriority));
        ctaEntityEmail = (CtaEntityEmail) genericServices.genericPersiste(ctaEntityEmail);
        initCtaEntityEmail();
        initAttributesCtaEntityEmail();
        showFormCtaEntityEmail = false;
    }

    public void cancelCtaEntityEmail(AjaxBehaviorEvent e) {
        System.out.println(" call cancelCtaEntityEmail ");
        showFormCtaEntityEmail = false;
    }

    public void deleteCtaEntityEmail(AjaxBehaviorEvent e) {
        ctaEntityEmailId = (Long) e.getComponent().getAttributes().get("ctaEntityEmailId");
        System.out.println(" call  deletCtaEntityEmail : " + ctaEntityEmailId);

        ctaEntityEmail = genericServices.findCtaEntityEmailById(ctaEntityEmailId);

        System.out.println(" pass  : " + ctaEntityEmail.getId());
        genericServices.genericDelete(ctaEntityEmail);
        System.out.println("not pass ");
        initCtaEntityEmail();
    }

    /*
     * ################# Site Web Managment  #################
     */
    public void initCtaEntityWeb() {
        vCtaEntityWebList = genericServices.findVCtaEntityWebByEntityId(entityId);
        showFormCtaEntityWeb = false;
    }

    public void initAddCtaEntityWeb() {
        vCtaWebTypeList = genericServices.findVCtaWebTypeByCltModuleId(SessionsGetter.getCltClientId());
        vCtaWebTypeItems = new ArrayList<>();

        for (VCtaWebType o : vCtaWebTypeList) {
            vCtaWebTypeItems.add(new SelectItem(o.getId(), o.getName()));
        }

    }

    public void initAttributesCtaEntityWeb() {
        ctaWebUrl = "";
        ctaWebType = "";
        ctaWebPriority = "";
    }

    public void addCtaEntityWeb(AjaxBehaviorEvent e) {
        System.out.println(" call  addCtaEntityWeb");
        initAddCtaEntityWeb();
        initAttributesCtaEntityWeb();
        showFormCtaEntityWeb = true;
    }

    public void saveCtaEntityWeb(AjaxBehaviorEvent e) {
        System.out.println(" call  saveCtaEntityWeb");
        ctaEntityWeb = new CtaEntityWeb();
        ctaEntityWeb.setEntityId(entityId);
        ctaEntityWeb.setUrl(ctaWebUrl);
        ctaEntityWeb.setWebTypeId(StringUtils.stringToLang(ctaWebType));
        ctaEntityWeb.setPriority(StringUtils.stringToLang(ctaWebPriority));
        ctaEntityWeb = (CtaEntityWeb) genericServices.genericPersiste(ctaEntityWeb);
        initCtaEntityWeb();
        initAttributesCtaEntityWeb();
        showFormCtaEntityWeb = false;
    }

    public void cancelCtaEntityWeb(AjaxBehaviorEvent e) {
        System.out.println(" call cancelCtaEntityWeb ");
        showFormCtaEntityWeb = false;
    }

    public void deleteCtaEntityWeb(AjaxBehaviorEvent e) {
        ctaEntityWebId = (Long) e.getComponent().getAttributes().get("ctaEntityWebId");
        System.out.println(" call  deleteCtaEntityWeb : " + ctaEntityWebId);

        ctaEntityWeb = genericServices.findCtaEntityWebById(ctaEntityWebId);

        System.out.println(" pass  : " + ctaEntityWeb.getId());
        genericServices.genericDelete(ctaEntityWeb);
        System.out.println("not pass ");
        initCtaEntityWeb();
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public List<VCtaEntityWeb> getvCtaEntityWebList() {
        return vCtaEntityWebList;
    }

    public void setvCtaEntityWebList(List<VCtaEntityWeb> vCtaEntityWebList) {
        this.vCtaEntityWebList = vCtaEntityWebList;
    }

    public boolean isShowFormCtaEntityWeb() {
        return showFormCtaEntityWeb;
    }

    public void setShowFormCtaEntityWeb(boolean showFormCtaEntityWeb) {
        this.showFormCtaEntityWeb = showFormCtaEntityWeb;
    }

    public List<VCtaWebType> getvCtaWebTypeList() {
        return vCtaWebTypeList;
    }

    public void setvCtaWebTypeList(List<VCtaWebType> vCtaWebTypeList) {
        this.vCtaWebTypeList = vCtaWebTypeList;
    }

    public List<SelectItem> getvCtaWebTypeItems() {
        return vCtaWebTypeItems;
    }

    public void setvCtaWebTypeItems(List<SelectItem> vCtaWebTypeItems) {
        this.vCtaWebTypeItems = vCtaWebTypeItems;
    }

    public String getCtaWebType() {
        return ctaWebType;
    }

    public void setCtaWebType(String ctaWebType) {
        this.ctaWebType = ctaWebType;
    }

    public String getCtaWebUrl() {
        return ctaWebUrl;
    }

    public void setCtaWebUrl(String ctaWebUrl) {
        this.ctaWebUrl = ctaWebUrl;
    }

    public String getCtaWebPriority() {
        return ctaWebPriority;
    }

    public void setCtaWebPriority(String ctaWebPriority) {
        this.ctaWebPriority = ctaWebPriority;
    }

    public CtaEntityWeb getCtaEntityWeb() {
        return ctaEntityWeb;
    }

    public void setCtaEntityWeb(CtaEntityWeb ctaEntityWeb) {
        this.ctaEntityWeb = ctaEntityWeb;
    }

    public List<VCtaEntityLocation> getvCtaEntityLocationList() {
        return vCtaEntityLocationList;
    }

    public void setvCtaEntityLocationList(List<VCtaEntityLocation> vCtaEntityLocationList) {
        this.vCtaEntityLocationList = vCtaEntityLocationList;
    }

    public List<VCtaLocationType> getvCtaLocationTypeList() {
        return vCtaLocationTypeList;
    }

    public void setvCtaLocationTypeList(List<VCtaLocationType> vCtaLocationTypeList) {
        this.vCtaLocationTypeList = vCtaLocationTypeList;
    }

    public List<SelectItem> getvCtaLocationTypeItems() {
        return vCtaLocationTypeItems;
    }

    public void setvCtaLocationTypeItems(List<SelectItem> vCtaLocationTypeItems) {
        this.vCtaLocationTypeItems = vCtaLocationTypeItems;
    }

    public CtaEntityLocation getCtaEntityLocation() {
        return ctaEntityLocation;
    }

    public void setCtaEntityLocation(CtaEntityLocation ctaEntityLocation) {
        this.ctaEntityLocation = ctaEntityLocation;
    }

    public String getCtaLocationLine1() {
        return ctaLocationLine1;
    }

    public void setCtaLocationLine1(String ctaLocationLine1) {
        this.ctaLocationLine1 = ctaLocationLine1;
    }

    public String getCtaLocationLine2() {
        return ctaLocationLine2;
    }

    public void setCtaLocationLine2(String ctaLocationLine2) {
        this.ctaLocationLine2 = ctaLocationLine2;
    }

    public String getCtaLocationLine3() {
        return ctaLocationLine3;
    }

    public void setCtaLocationLine3(String ctaLocationLine3) {
        this.ctaLocationLine3 = ctaLocationLine3;
    }

    public String getCtaLocationCountry() {
        return ctaLocationCountry;
    }

    public void setCtaLocationCountry(String ctaLocationCountry) {
        this.ctaLocationCountry = ctaLocationCountry;
    }

    public String getCtaLocationPostalCode() {
        return ctaLocationPostalCode;
    }

    public void setCtaLocationPostalCode(String ctaLocationPostalCode) {
        this.ctaLocationPostalCode = ctaLocationPostalCode;
    }

    public String getCtaLocationType() {
        return ctaLocationType;
    }

    public void setCtaLocationType(String ctaLocationType) {
        this.ctaLocationType = ctaLocationType;
    }

    public boolean isShowFormCtaEntityLocation() {
        return showFormCtaEntityLocation;
    }

    public void setShowFormCtaEntityLocation(boolean showFormCtaEntityLocation) {
        this.showFormCtaEntityLocation = showFormCtaEntityLocation;
    }

    public Long getCtaEntityWebId() {
        return ctaEntityWebId;
    }

    public void setCtaEntityWebId(Long ctaEntityWebId) {
        this.ctaEntityWebId = ctaEntityWebId;
    }

    public String getCtaLocationCity() {
        return ctaLocationCity;
    }

    public void setCtaLocationCity(String ctaLocationCity) {
        this.ctaLocationCity = ctaLocationCity;
    }

    public String getCtaLocationPriority() {
        return ctaLocationPriority;
    }

    public void setCtaLocationPriority(String ctaLocationPriority) {
        this.ctaLocationPriority = ctaLocationPriority;
    }

    public Long getCtaEntityLocationId() {
        return ctaEntityLocationId;
    }

    public void setCtaEntityLocationId(Long ctaEntityLocationId) {
        this.ctaEntityLocationId = ctaEntityLocationId;
    }

    public List<SelectItem> getvInfCountryItems() {
        return vInfCountryItems;
    }

    public void setvInfCountryItems(List<SelectItem> vInfCountryItems) {
        this.vInfCountryItems = vInfCountryItems;
    }

    public List<SelectItem> getvInfCityItems() {
        return vInfCityItems;
    }

    public void setvInfCityItems(List<SelectItem> vInfCityItems) {
        this.vInfCityItems = vInfCityItems;
    }

    public List<VCtaEntityPhone> getvCtaEntityPhoneList() {
        return vCtaEntityPhoneList;
    }

    public void setvCtaEntityPhoneList(List<VCtaEntityPhone> vCtaEntityPhoneList) {
        this.vCtaEntityPhoneList = vCtaEntityPhoneList;
    }

    public List<VCtaPhoneType> getvCtaPhoneTypeList() {
        return vCtaPhoneTypeList;
    }

    public void setvCtaPhoneTypeList(List<VCtaPhoneType> vCtaPhoneTypeList) {
        this.vCtaPhoneTypeList = vCtaPhoneTypeList;
    }

    public List<SelectItem> getvCtaPhoneTypeItems() {
        return vCtaPhoneTypeItems;
    }

    public void setvCtaPhoneTypeItems(List<SelectItem> vCtaPhoneTypeItems) {
        this.vCtaPhoneTypeItems = vCtaPhoneTypeItems;
    }

    public CtaEntityPhone getCtaEntityPhone() {
        return ctaEntityPhone;
    }

    public void setCtaEntityPhone(CtaEntityPhone ctaEntityPhone) {
        this.ctaEntityPhone = ctaEntityPhone;
    }

    public String getCtaPhoneCountryCode() {
        return ctaPhoneCountryCode;
    }

    public void setCtaPhoneCountryCode(String ctaPhoneCountryCode) {
        this.ctaPhoneCountryCode = ctaPhoneCountryCode;
    }

    public String getCtaPhoneNumber() {
        return ctaPhoneNumber;
    }

    public void setCtaPhoneNumber(String ctaPhoneNumber) {
        this.ctaPhoneNumber = ctaPhoneNumber;
    }

    public String getCtaPhoneType() {
        return ctaPhoneType;
    }

    public void setCtaPhoneType(String ctaPhoneType) {
        this.ctaPhoneType = ctaPhoneType;
    }

    public boolean isShowFormCtaEntityPhone() {
        return showFormCtaEntityPhone;
    }

    public void setShowFormCtaEntityPhone(boolean showFormCtaEntityPhone) {
        this.showFormCtaEntityPhone = showFormCtaEntityPhone;
    }

    public Long getCtaEntityPhoneId() {
        return ctaEntityPhoneId;
    }

    public void setCtaEntityPhoneId(Long ctaEntityPhoneId) {
        this.ctaEntityPhoneId = ctaEntityPhoneId;
    }

    public String getCtaPhonePriority() {
        return ctaPhonePriority;
    }

    public void setCtaPhonePriority(String ctaPhonePriority) {
        this.ctaPhonePriority = ctaPhonePriority;
    }

    public List<VCtaEntityFax> getvCtaEntityFaxList() {
        return vCtaEntityFaxList;
    }

    public void setvCtaEntityFaxList(List<VCtaEntityFax> vCtaEntityFaxList) {
        this.vCtaEntityFaxList = vCtaEntityFaxList;
    }

    public List<VCtaFaxType> getvCtaFaxTypeList() {
        return vCtaFaxTypeList;
    }

    public void setvCtaFaxTypeList(List<VCtaFaxType> vCtaFaxTypeList) {
        this.vCtaFaxTypeList = vCtaFaxTypeList;
    }

    public List<SelectItem> getvCtaFaxTypeItems() {
        return vCtaFaxTypeItems;
    }

    public void setvCtaFaxTypeItems(List<SelectItem> vCtaFaxTypeItems) {
        this.vCtaFaxTypeItems = vCtaFaxTypeItems;
    }

    public CtaEntityFax getCtaEntityFax() {
        return ctaEntityFax;
    }

    public void setCtaEntityFax(CtaEntityFax ctaEntityFax) {
        this.ctaEntityFax = ctaEntityFax;
    }

    public String getCtaFaxCountryCode() {
        return ctaFaxCountryCode;
    }

    public void setCtaFaxCountryCode(String ctaFaxCountryCode) {
        this.ctaFaxCountryCode = ctaFaxCountryCode;
    }

    public String getCtaFaxNumber() {
        return ctaFaxNumber;
    }

    public void setCtaFaxNumber(String ctaFaxNumber) {
        this.ctaFaxNumber = ctaFaxNumber;
    }

    public String getCtaFaxType() {
        return ctaFaxType;
    }

    public void setCtaFaxType(String ctaFaxType) {
        this.ctaFaxType = ctaFaxType;
    }

    public String getCtaFaxPriority() {
        return ctaFaxPriority;
    }

    public void setCtaFaxPriority(String ctaFaxPriority) {
        this.ctaFaxPriority = ctaFaxPriority;
    }

    public boolean isShowFormCtaEntityFax() {
        return showFormCtaEntityFax;
    }

    public void setShowFormCtaEntityFax(boolean showFormCtaEntityFax) {
        this.showFormCtaEntityFax = showFormCtaEntityFax;
    }

    public Long getCtaEntityFaxId() {
        return ctaEntityFaxId;
    }

    public void setCtaEntityFaxId(Long ctaEntityFaxId) {
        this.ctaEntityFaxId = ctaEntityFaxId;
    }

    public List<VCtaEmailType> getvCtaEmailTypeList() {
        return vCtaEmailTypeList;
    }

    public void setvCtaEmailTypeList(List<VCtaEmailType> vCtaEmailTypeList) {
        this.vCtaEmailTypeList = vCtaEmailTypeList;
    }

    public List<SelectItem> getvCtaEmailTypeItems() {
        return vCtaEmailTypeItems;
    }

    public void setvCtaEmailTypeItems(List<SelectItem> vCtaEmailTypeItems) {
        this.vCtaEmailTypeItems = vCtaEmailTypeItems;
    }

    public CtaEntityEmail getCtaEntityEmail() {
        return ctaEntityEmail;
    }

    public void setCtaEntityEmail(CtaEntityEmail ctaEntityEmail) {
        this.ctaEntityEmail = ctaEntityEmail;
    }

    public String getCtaEmailType() {
        return ctaEmailType;
    }

    public void setCtaEmailType(String ctaEmailType) {
        this.ctaEmailType = ctaEmailType;
    }

    public String getCtaEmailPriority() {
        return ctaEmailPriority;
    }

    public void setCtaEmailPriority(String ctaEmailPriority) {
        this.ctaEmailPriority = ctaEmailPriority;
    }

    public boolean isShowFormCtaEntityEmail() {
        return showFormCtaEntityEmail;
    }

    public void setShowFormCtaEntityEmail(boolean showFormCtaEntityEmail) {
        this.showFormCtaEntityEmail = showFormCtaEntityEmail;
    }

    public Long getCtaEntityEmailId() {
        return ctaEntityEmailId;
    }

    public void setCtaEntityEmailId(Long ctaEntityEmailId) {
        this.ctaEntityEmailId = ctaEntityEmailId;
    }

    public List<VCtaEntityEmail> getvCtaEntityEmailList() {
        return vCtaEntityEmailList;
    }

    public void setvCtaEntityEmailList(List<VCtaEntityEmail> vCtaEntityEmailList) {
        this.vCtaEntityEmailList = vCtaEntityEmailList;
    }

    public String getCtaEmailAdresse() {
        return ctaEmailAdresse;
    }

    public void setCtaEmailAdresse(String ctaEmailAdresse) {
        this.ctaEmailAdresse = ctaEmailAdresse;
    }

}
