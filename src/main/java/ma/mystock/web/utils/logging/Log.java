package ma.mystock.web.utils.logging;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Log is a logging class that you can utilize in your code to print out
 * messages to Console.
 *
 * @author Abdessamad HALLAL
 */
public final class Log {

    public static final Logger log = Logger.getLogger(Log.class);

    public static final int LOG_ID_MAIN = 0;
    public static final int LOG_ID_RADIO = 1;
    public static final int LOG_ID_EVENTS = 2;
    public static final int LOG_ID_SYSTEM = 3;
    public static final int LOG_ID_CRASH = 4;

    public static final int FATAL = 1;
    public static final int ERROR = 2;
    public static final int WARN = 3;
    public static final int INFO = 4;
    public static final int DEBUG = 5;
    public static final int TRACE = 6;

    /* ################# FATAL  ################# */
    public static void f(Object tag, Object msg) {
        println(FATAL, tag, msg);
    }

    public static void f(Object msg) {
        println(FATAL, msg);
    }

    /* ################# ERROR  ################# */
    public static void e(Object tag, Object msg) {
        println(ERROR, tag, msg);
    }

    public static void e(Object msg) {
        println(ERROR, msg);
    }

    /* ################# WARN  ################# */
    public static void w(Object tag, Object msg) {
        println(WARN, tag, msg);
    }

    public static void w(Object msg) {
        println(WARN, msg);
    }

    /* ################# INFO  ################# */
    public static void i(Object tag, Object msg) {
        println(INFO, tag, msg);
    }

    public static void i(Object msg) {
        Object st = (Object) msg;
        println(INFO, st);
    }

    /* ################# DEBUG  ################# */
    public static void d(Object tag, Object msg) {
        println(DEBUG, tag, msg);
    }

    public static void d(Object msg) {
        println(DEBUG, msg);
    }

    /* ################# TRACE  ################# */
    public static void t(Object tag, Object msg) {
        println(TRACE, tag, msg);
    }

    public static void t(Object msg) {
        println(TRACE, msg);
    }

    /* ################# MAIL  ################# */
    public static void mail(Object subject, Object message) {
        i(subject, message);

    }

    /* ################# UTILS  ################# */
    public static void println(int priority, Object tag, Object msg) {
        println(LOG_ID_MAIN, priority, tag, msg);
    }

    public static void println(int priority, Object msg) {
        println(LOG_ID_MAIN, priority, " null ", msg);
    }

    public static void println(int bufId, int priority, Object tag, Object msg) {

        String message = "{" + tag + "} : " + msg;

        if (LOG_ID_MAIN == bufId) {

            if (FATAL == priority) {
                log.log(Level.FATAL, message);
            } else if (ERROR == priority) {
                log.log(Level.ERROR, message);
            } else if (WARN == priority) {
                log.log(Level.WARN, message);
            } else if (INFO == priority) {
                log.log(Level.INFO, message);
            } else if (DEBUG == priority) {
                log.log(Level.DEBUG, message);
            } else if (TRACE == priority) {
                log.log(Level.TRACE, message);
            }
        }
    }

}
