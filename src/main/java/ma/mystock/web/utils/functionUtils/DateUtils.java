/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.functionUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author Abdessamad HALLAL
 */
public class DateUtils {

    public static Date getCurrentDate() {
        return new Date();
    }

    public static long getCurrentTime() {
        Date d = new Date();
        return d.getTime();
    }

    public static Date getDatePlusHours(long nbr) {

        Date now = new Date();
        Date hours = new Date(3600 * nbr * 1000);

        return new Date(now.getTime() + hours.getTime());

    }

    public static String formatDate(Date d, String format) {

        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(d);

    }

    public static String formatDate(Date d) {
        SimpleDateFormat sdf = new SimpleDateFormat("");
        return sdf.format(d);

    }

    public static long getCurrentMonth() {
        int month;
        GregorianCalendar date = new GregorianCalendar();
        month = date.get(Calendar.MONTH);
        month = month + 1;
        return month;
    }

    public static long getCurrentYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }

    public static long getCurrentDay() {
        return Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

    }

}
