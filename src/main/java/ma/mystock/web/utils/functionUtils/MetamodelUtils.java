package ma.mystock.web.utils.functionUtils;

import java.util.Locale;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import ma.mystock.web.utils.globals.GlobalsAttributs;

public class MetamodelUtils {

    public static String getValue(String key) {

        if (key == null) {
            return "::!!::";
        }
        String value;
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        Locale locale = (Locale) session.getAttribute(GlobalsAttributs.LOCALE);
        if (locale == null) {
            if (session.getAttribute(GlobalsAttributs.LANGUAGE) != null) {
                locale = new Locale((String) session.getAttribute(GlobalsAttributs.LANGUAGE));
            } else {
                locale = new Locale("en");
            }
        }
        Map<String, String> metaModelMap = null;

        if ("en".equals(locale.getLanguage())) {

            //metaModelMap = LoadMetaModelTag.getEnMetaModelMap();
            if (metaModelMap.size() == 0) {

                //metaModelMap = LoadMetaModelListner.getEnMetaModelMap();
            }

        } else {
            ///metaModelMap = LoadMetaModelTag.getFrMetaModelMap();
            if (metaModelMap.size() == 0) {

                //metaModelMap = LoadMetaModelListner.getFrMetaModelMap();
            }
        }

        if (metaModelMap == null) {
            return "??? " + key + " ???";
        }

        value = (String) metaModelMap.get(key);

        if (value == null) {
            value = ":::" + key + ":::";
        }

        return value;
    }

    public static String getValueWithLocal(String key, String local) {

        if (key == null) {
            return "::!!::";
        }
        String value;
        Map<String, String> metaModelMap = null;

        if ("en".equalsIgnoreCase(local)) {
            //metaModelMap = LoadMetaModelTag.getEnMetaModelMap(); //(Map <String, String>) FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().get("enMetaModelMap");
        } else {
            //metaModelMap = LoadMetaModelTag.getFrMetaModelMap(); //(Map <String, String>) FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().get("frMetaModelMap");
        }
        if (metaModelMap == null) {
            return "??? " + key + " ???";
        }
        value = (String) metaModelMap.get(key);

        if (value == null) {
            value = ":::" + key + ":::";
        }

        return value;
    }

}
