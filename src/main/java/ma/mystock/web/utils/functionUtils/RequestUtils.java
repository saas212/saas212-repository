/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.functionUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ma.mystock.web.utils.globals.GlobalsAttributs;

/**
 *
 * @author Abdessamad HALLAL
 */
public class RequestUtils {

    public static String getSiteFromParameterOrSession(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {

        String site = httpServletRequest.getParameter("site");

        return site;
    }

    public static String getLanguageFromParameterOrSessionOrDefault(HttpServletRequest httpServletRequest) {
        String lang = httpServletRequest.getParameter("lang");
        String sessionLang = (String) httpServletRequest.getSession().getAttribute(GlobalsAttributs.LANGUAGE);
        String cookieLang = getCookieValue(httpServletRequest.getCookies(), "APLANG", null);
        //System.out.println(" getLanguage eForms " + lang + ", " + sessionLang + ", cookieLang " + cookieLang);
        String language = (lang != null) ? lang : ((sessionLang != null) ? sessionLang : ((cookieLang != null) ? cookieLang : ""));
        return language;
    }

    public static String getCookieValue(Cookie[] cookies, String cookieName, String defaultValue) {
        //PrintOut.print("cookies " + (cookies!=null?cookies.length:"") , PrintOut.DEBUG);   
        if (cookies != null) {
            for (int i = 0; i < cookies.length; i++) {
                Cookie cookie = cookies[i];
                if (cookieName.equals(cookie.getName())) {
                    return (cookie.getValue());
                }
                //PrintOut.print("cookie " + cookie.getName() + " : " + cookie.getValue(), PrintOut.DEBUG);
            }
        }
        return (defaultValue);
    }

    public static String getFullURL(HttpServletRequest request) {
        StringBuffer requestURL = request.getRequestURL();
        String queryString = request.getQueryString();

        if (queryString == null) {
            return requestURL.toString();
        } else {
            return requestURL.append('?').append(queryString).toString();
        }
    }

    public static void shoInfo() {

        /*
         System.out.println(" ........... " + httpServletRequest.getCharacterEncoding());
         System.out.println(" ........... " + httpServletRequest.getServerName());
         System.out.println(" ........... " + httpServletRequest.getRemotePort());
         System.out.println(" ........... " + httpServletRequest.getServerPort());
         System.out.println(" ........... " + httpServletRequest.getServletContext().getServerInfo());
         System.out.println(" ........... " + httpServletRequest.getContextPath());
         System.out.println(" ........... " + httpServletRequest.getPathInfo());
         System.out.println(" ........... " + httpServletRequest.getRequestURL());
         System.out.println(" ........... " + httpServletRequest.getScheme());
         System.out.println(" ........... " + httpServletRequest.getServerPort());
         System.out.println(" ........... " + httpServletRequest.getRequestURI());
         System.out.println(" ........... " + httpServletRequest.getQueryString());
         System.out.println(" ........... ");
         System.out.println(" ........... ");
         System.out.println(" ........... ");
         System.out.println(" ........... ");
         */
        //ServletContext servletContext = httpServletRequest.getServletContext();
        //ServletRegistration servletRegistration = servletContext.getServletRegistration();
        //java.util.Collection<java.lang.String> mappings = servletRegistration.getMappings();
    }

    public static String getURL(HttpServletRequest req) {

        String scheme = req.getScheme();             // http
        String serverName = req.getServerName();     // hostname.com
        int serverPort = req.getServerPort();        // 80
        String contextPath = req.getContextPath();   // /mywebapp
        String servletPath = req.getServletPath();   // /servlet/MyServlet
        String pathInfo = req.getPathInfo();         // /a/b;c=123
        String queryString = req.getQueryString();          // d=789

        // Reconstruct original requesting URL
        StringBuilder url = new StringBuilder();
        url.append(scheme).append("://").append(serverName);

        if (serverPort != 80 && serverPort != 443) {
            url.append(":").append(serverPort);
        }

        url.append(contextPath).append(servletPath);

        if (pathInfo != null) {
            url.append(pathInfo);
        }
        if (queryString != null) {
            url.append("?").append(queryString);
        }
        return url.toString();
    }

    public static String getUrl(HttpServletRequest req) {
        String reqUrl = req.getRequestURL().toString();
        String queryString = req.getQueryString();   // d=789
        if (queryString != null) {
            reqUrl += "?" + queryString;
        }
        return reqUrl;
    }

    public static String getUrl1(HttpServletRequest httpServletRequest) {
        String uri = httpServletRequest.getScheme() + "://"
                + // "http" + "://
                httpServletRequest.getServerName()
                + // "myhost"
                ":"
                + // ":"
                httpServletRequest.getServerPort()
                + // "8080"
                httpServletRequest.getRequestURI()
                + // "/people"
                "?"
                + // "?"
                httpServletRequest.getQueryString();       // "lastname=Fox&age=30"
        return uri;
    }

    public static String getUrl2(HttpServletRequest httpServletRequest) {
        String uri1 = httpServletRequest.getScheme() + "://"
                + httpServletRequest.getServerName()
                + ("http".equals(httpServletRequest.getScheme()) && httpServletRequest.getServerPort() == 80 || "https".equals(httpServletRequest.getScheme()) && httpServletRequest.getServerPort() == 443 ? "" : ":" + httpServletRequest.getServerPort())
                + httpServletRequest.getRequestURI()
                + (httpServletRequest.getQueryString() != null ? "?" + httpServletRequest.getQueryString() : "");

        return uri1;
    }

}
