package ma.mystock.web.utils.functionUtils.params;

import ma.mystock.web.utils.annotations.TODO;
import ma.mystock.web.utils.values.MapsGetter;

/**
 *
 * @author anasshajami
 */
@TODO(value = "générer l'excéption Long.valueOf", level = TODO.Level.CRITICAL)
public class ClientParameterUtils {

    public static final Long PARAM_1_APPLICATION_NAME = 1L;
    public static final Long PARAM_2_APPLICATION_TITLE = 2L;
    public static final Long PARAM_3_DEFAULT_LANGUAGE = 3L;
    public static final Long PARAM_4_DEFAULT_PREFIX = 4L;
    public static final Long PARAM_5_ENABLE_CACHE_RESOURCES = 5L;
    public static final Long PARAM_6_PATTERN_DATE = 6L;
    public static final Long PARAM_7_PATTERN_DATETIME = 7L;
    public static final Long PARAM_8_PATTERN_TIME = 8L;
    public static final Long PARAM_9_CURRENCY = 9L;
    public static final Long PARAM_10_TENANT_NAME = 10L;
    public static final Long PARAM_11_IS_DEBUG = 11L;

    public static String getApplicationName() {
        return MapsGetter.getParameterClientMap().get(PARAM_1_APPLICATION_NAME);
    }

    public static String getApplicationTitle() {
        return MapsGetter.getParameterClientMap().get(PARAM_2_APPLICATION_TITLE);
    }

    public static Long getDefaultLanguage() {
        return Long.valueOf(MapsGetter.getParameterClientMap().get(PARAM_3_DEFAULT_LANGUAGE));
    }

    public static Long getDefaultPrefix() {
        return Long.valueOf(MapsGetter.getParameterClientMap().get(PARAM_4_DEFAULT_PREFIX));
    }

    public static boolean getEnableCacheResources() {
        return "true".equalsIgnoreCase(MapsGetter.getParameterClientMap().get(PARAM_5_ENABLE_CACHE_RESOURCES));
    }

    public static String getPatternDate() {
        return MapsGetter.getParameterClientMap().get(PARAM_6_PATTERN_DATE);

    }

    public static String getPatternDateTime() {
        return MapsGetter.getParameterClientMap().get(PARAM_7_PATTERN_DATETIME);

    }

    public static String getPatternTime() {
        return MapsGetter.getParameterClientMap().get(PARAM_8_PATTERN_TIME);

    }

    public static String getCurrency() {
        return MapsGetter.getParameterClientMap().get(PARAM_9_CURRENCY);
    }

    public static String getTenantName() {
        return MapsGetter.getParameterClientMap().get(PARAM_10_TENANT_NAME);
    }

    public static boolean getIsDebug() {
        return "true".equalsIgnoreCase(MapsGetter.getParameterClientMap().get(PARAM_11_IS_DEBUG));
    }

}
