package ma.mystock.web.utils.functionUtils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Normalizer;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import ma.mystock.web.utils.globals.GlobalsAttributs;

/**
 *
 * @author Abdessamad HALLAL
 */
public class StringUtils {

    public static final String DATE_PATTERN_OICR = "yyyy-MMM-dd";
    public static final String DATE_PATTERN_OICR_HH_MM = "yyyy-MMM-dd HH:mm";

    public static final String DATE_PATTERN_DEFAULT = "yyyy-MM-dd";
    public static final String DATE_PATTERN_DEFAULT_FR = "dd-MM-yyyy";
    public static final String DATE_PATTERN_LONG_DEFAULT = "yyyy-MM-dd HH:mm";
    public static final String DATE_PATTERN_LONG_DEFAULT_SECONDE = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_PATTERN_LONG_DEFAULT_SECONDE_MONTH = "yyyy-mm-dd HH:mm:ss";
    public static final String DATE_PATTERN_PUBLICATION = "yyyy";
    public static final String DATE_PATTERN_CA = "dd-MM-yy";

    public static final String DECIMAL_FORMATTER = "######.00";
    public static final String MONEY_FORMATTER = "###,##0.00 $";
    public static final String POURCENTAGE_FORMATTER = "##.00";

    public static Long stringToLang(String str) {
        try {
            return Long.valueOf(str);
        } catch (Exception e) {
            return null;
        }
    }

    public static String langToString(Long l) {
        try {
            return String.valueOf(l);
        } catch (Exception e) {
            return "";
        }
    }

    public static boolean isEmpty(String str) {
        if ((str == null) || (str.length() == 0)) {
            return true;
        } else {
            return false;
        }

    }

    public static String getMonthName(final int month, final String lang) {
        String s = null;
        if (lang.equals("EN")) {
            switch (month) {
                case 1:
                    s = "January";
                    break;
                case 2:
                    s = "February";
                    break;
                case 3:
                    s = "March";
                    break;
                case 4:
                    s = "April";
                    break;
                case 5:
                    s = "May";
                    break;
                case 6:
                    s = "June";
                    break;
                case 7:
                    s = "July";
                    break;
                case 8:
                    s = "August";
                    break;
                case 9:
                    s = "September";
                    break;
                case 10:
                    s = "October";
                    break;
                case 11:
                    s = "November";
                    break;
                case 12:
                    s = "December";
                    break;
            }
        } else {
            switch (month) {
                case 1:
                    s = "janvier";
                    break;
                case 2:
                    s = "février";
                    break;
                case 3:
                    s = "mars";
                    break;
                case 4:
                    s = "avril";
                    break;
                case 5:
                    s = "mai";
                    break;
                case 6:
                    s = "juin";
                    break;
                case 7:
                    s = "juillet";
                    break;
                case 8:
                    s = "août";
                    break;
                case 9:
                    s = "septembre";
                    break;
                case 10:
                    s = "octobre";
                    break;
                case 11:
                    s = "novembre";
                    break;
                case 12:
                    s = "décembre";
                    break;
            }
        }
        return s;
    }

    public static String getMonthNameEL(final int month, final String lang) {
        String s = null;
        if (lang.equals("EN")) {
            switch (month) {
                case 1:
                    s = "January";
                    break;
                case 2:
                    s = "February";
                    break;
                case 3:
                    s = "March";
                    break;
                case 4:
                    s = "April";
                    break;
                case 5:
                    s = "May";
                    break;
                case 6:
                    s = "June";
                    break;
                case 7:
                    s = "July";
                    break;
                case 8:
                    s = "August";
                    break;
                case 9:
                    s = "September";
                    break;
                case 10:
                    s = "October";
                    break;
                case 11:
                    s = "November";
                    break;
                case 12:
                    s = "December";
                    break;
            }
        } else {
            switch (month) {
                case 1:
                    s = "janvier";
                    break;
                case 2:
                    s = "février";
                    break;
                case 3:
                    s = "mars";
                    break;
                case 4:
                    s = "avril";
                    break;
                case 5:
                    s = "mai";
                    break;
                case 6:
                    s = "juin";
                    break;
                case 7:
                    s = "juillet";
                    break;
                case 8:
                    s = "août";
                    break;
                case 9:
                    s = "septembre";
                    break;
                case 10:
                    s = "octobre";
                    break;
                case 11:
                    s = "novembre";
                    break;
                case 12:
                    s = "décembre";
                    break;
            }
        }
        return s;
    }

    public static String sansEspaceTiret(String chaine) {

        chaine = chaine.replaceAll("_", "");
        chaine = chaine.replaceAll("-", "");
        chaine = chaine.replaceAll("'", "");
        chaine = chaine.replaceAll(" ", "");

        return chaine;

    }
    /**
     * Index du 1er caractere accentuÈ *
     */
    private static final int MIN = 192;
    /**
     * Index du dernier caractere accentuÈ *
     */
    private static final int MAX = 255;
    /**
     * Vecteur de correspondance entre accent / sans accent *
     */
    private static final Vector map = initMap();

    /**
     * Initialisation du tableau de correspondance entre les caractÈres
     * accentuÈs et leur homologues non accentuÈs
     */
    private static Vector initMap() {
        Vector Result = new Vector();
        java.lang.String car = null;

        car = new java.lang.String("A");
        Result.add(car);
        /* '\u00C0'   ¿   alt-0192  */

        Result.add(car);
        /* '\u00C1'   ¡   alt-0193  */

        Result.add(car);
        /* '\u00C2'   ¬   alt-0194  */

        Result.add(car);
        /* '\u00C3'   √   alt-0195  */

        Result.add(car);
        /* '\u00C4'   ƒ   alt-0196  */

        Result.add(car);
        /* '\u00C5'   ≈   alt-0197  */

        car = new java.lang.String("AE");
        Result.add(car);
        /* '\u00C6'   ∆   alt-0198  */

        car = new java.lang.String("C");
        Result.add(car);
        /* '\u00C7'   «   alt-0199  */

        car = new java.lang.String("E");
        Result.add(car);
        /* '\u00C8'   »   alt-0200  */

        Result.add(car);
        /* '\u00C9'   …   alt-0201  */

        Result.add(car);
        /* '\u00CA'       alt-0202  */

        Result.add(car);
        /* '\u00CB'   À   alt-0203  */

        car = new java.lang.String("I");
        Result.add(car);
        /* '\u00CC'   Ã   alt-0204  */

        Result.add(car);
        /* '\u00CD'   Õ   alt-0205  */

        Result.add(car);
        /* '\u00CE'   Œ   alt-0206  */

        Result.add(car);
        /* '\u00CF'   œ   alt-0207  */

        car = new java.lang.String("D");
        Result.add(car);
        /* '\u00D0'   –   alt-0208  */

        car = new java.lang.String("N");
        Result.add(car);
        /* '\u00D1'   —   alt-0209  */

        car = new java.lang.String("O");
        Result.add(car);
        /* '\u00D2'   “   alt-0210  */

        Result.add(car);
        /* '\u00D3'   ”   alt-0211  */

        Result.add(car);
        /* '\u00D4'   ‘   alt-0212  */

        Result.add(car);
        /* '\u00D5'   ’   alt-0213  */

        Result.add(car);
        /* '\u00D6'   ÷   alt-0214  */

        car = new java.lang.String("*");
        Result.add(car);
        /* '\u00D7'   ◊   alt-0215  */

        car = new java.lang.String("0");
        Result.add(car);
        /* '\u00D8'   ÿ   alt-0216  */

        car = new java.lang.String("U");
        Result.add(car);
        /* '\u00D9'   Ÿ   alt-0217  */

        Result.add(car);
        /* '\u00DA'   ⁄   alt-0218  */

        Result.add(car);
        /* '\u00DB'   €   alt-0219  */

        Result.add(car);
        /* '\u00DC'   ‹   alt-0220  */

        car = new java.lang.String("Y");
        Result.add(car);
        /* '\u00DD'   ›   alt-0221  */

        car = new java.lang.String("ﬁ");
        Result.add(car);
        /* '\u00DE'   ﬁ   alt-0222  */

        car = new java.lang.String("B");
        Result.add(car);
        /* '\u00DF'   ﬂ   alt-0223  */

        car = new java.lang.String("a");
        Result.add(car);
        /* '\u00E0'   ‡   alt-0224  */

        Result.add(car);
        /* '\u00E1'   ·   alt-0225  */

        Result.add(car);
        /* '\u00E2'   ‚   alt-0226  */

        Result.add(car);
        /* '\u00E3'   „   alt-0227  */

        Result.add(car);
        /* '\u00E4'   ‰   alt-0228  */

        Result.add(car);
        /* '\u00E5'   Â   alt-0229  */

        car = new java.lang.String("ae");
        Result.add(car);
        /* '\u00E6'   Ê   alt-0230  */

        car = new java.lang.String("c");
        Result.add(car);
        /* '\u00E7'   Á   alt-0231  */

        car = new java.lang.String("e");
        Result.add(car);
        /* '\u00E8'   Ë   alt-0232  */

        Result.add(car);
        /* '\u00E9'   È   alt-0233  */

        Result.add(car);
        /* '\u00EA'   Í   alt-0234  */

        Result.add(car);
        /* '\u00EB'   Î   alt-0235  */

        car = new java.lang.String("i");
        Result.add(car);
        /* '\u00EC'   Ï   alt-0236  */

        Result.add(car);
        /* '\u00ED'   Ì   alt-0237  */

        Result.add(car);
        /* '\u00EE'   Ó   alt-0238  */

        Result.add(car);
        /* '\u00EF'   Ô   alt-0239  */

        car = new java.lang.String("d");
        Result.add(car);
        /* '\u00F0'      alt-0240  */

        car = new java.lang.String("n");
        Result.add(car);
        /* '\u00F1'   Ò   alt-0241  */

        car = new java.lang.String("o");
        Result.add(car);
        /* '\u00F2'   Ú   alt-0242  */

        Result.add(car);
        /* '\u00F3'   Û   alt-0243  */

        Result.add(car);
        /* '\u00F4'   Ù   alt-0244  */

        Result.add(car);
        /* '\u00F5'   ı   alt-0245  */

        Result.add(car);
        /* '\u00F6'   ˆ   alt-0246  */

        car = new java.lang.String("/");
        Result.add(car);
        /* '\u00F7'   ˜   alt-0247  */

        car = new java.lang.String("0");
        Result.add(car);
        /* '\u00F8'   ¯   alt-0248  */

        car = new java.lang.String("u");
        Result.add(car);
        /* '\u00F9'   ˘   alt-0249  */

        Result.add(car);
        /* '\u00FA'   ˙   alt-0250  */

        Result.add(car);
        /* '\u00FB'   ˚   alt-0251  */

        Result.add(car);
        /* '\u00FC'   ¸   alt-0252  */

        car = new java.lang.String("y");
        Result.add(car);
        /* '\u00FD'   ˝   alt-0253  */

        car = new java.lang.String("˛");
        Result.add(car);
        /* '\u00FE'   ˛   alt-0254  */

        car = new java.lang.String("y");
        Result.add(car);
        /* '\u00FF'   ˇ   alt-0255  */

        Result.add(car);
        /* '\u00FF'       alt-0255  */

        return Result;
    }

    public static java.lang.String sansAccent(java.lang.String chaine) {
        java.lang.StringBuffer Result = new StringBuffer(chaine);

        for (int bcl = 0; bcl < Result.length(); bcl++) {
            int carVal = chaine.charAt(bcl);
            if (carVal >= MIN && carVal <= MAX) {  // Remplacement
                java.lang.String newVal = (java.lang.String) map.get(carVal - MIN);
                Result.replace(bcl, bcl + 1, newVal);
            }
        }
        return Result.toString();
    }

    public static String stringWithoutAccents(String chaine) {

        chaine = chaine.replaceAll("[èéêë]", "e");
        chaine = chaine.replaceAll("[ÙÚÛÜùúûü]", "u");
        chaine = chaine.replaceAll("[ÌÍÎÏìíîï]", "i");
        chaine = chaine.replaceAll("[àáâãäå]", "a");
        chaine = chaine.replaceAll("ÒÓÔÕÖØòóôõöø", "o");
        chaine = chaine.replaceAll("Çç", "c");
        chaine = chaine.replaceAll("[ÈÉÊË]", "E");
        chaine = chaine.replaceAll("[Ññ]", "n");
        chaine = chaine.replaceAll("[ÛÙ]", "U");
        chaine = chaine.replaceAll("[ÏÎ]", "I");
        chaine = chaine.replaceAll("[ÀÁÂÃÄÅ]", "A");
        chaine = chaine.replaceAll("Ô", "O");
        chaine = chaine.replaceAll("ÿ", "y");
        chaine = Normalizer.normalize(chaine, Normalizer.Form.NFD);
        chaine = chaine.replaceAll("[^\\p{ASCII}]", "");

        chaine = chaine.replaceAll("_", "");
        chaine = chaine.replaceAll("-", "");
        chaine = chaine.replaceAll("'", "");
        chaine = chaine.replaceAll(" ", "");

        return chaine;
    }

    public static String stringWithoutSpecialCaracters(String chaine) {

        chaine = chaine.replaceAll("[èéêë]", "e");
        chaine = chaine.replaceAll("[ÙÚÛÜùúûü]", "u");
        chaine = chaine.replaceAll("[ÌÍÎÏìíîï]", "i");
        chaine = chaine.replaceAll("[àáâãäå]", "a");
        chaine = chaine.replaceAll("ÒÓÔÕÖØòóôõöø", "o");
        chaine = chaine.replaceAll("Çç", "c");
        chaine = chaine.replaceAll("[ÈÉÊË]", "E");
        chaine = chaine.replaceAll("[Ññ]", "n");
        chaine = chaine.replaceAll("[ÛÙ]", "U");
        chaine = chaine.replaceAll("[ÏÎ]", "I");
        chaine = chaine.replaceAll("[ÀÁÂÃÄÅ]", "A");
        chaine = chaine.replaceAll("Ô", "O");
        chaine = chaine.replaceAll("ÿ", "y");

        return chaine;
    }

    public static boolean isValidEmailAddress(String email) {

        String ePattern = "[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public static String removeSuffixFromItemCode(String itemCode) {
        System.out.println("itemCode " + itemCode);
        if (!isEmpty(itemCode)) {
            System.out.println("itemCode to " + itemCode.substring(0, itemCode.lastIndexOf(".")));
            return itemCode.substring(0, itemCode.lastIndexOf("."));
        }
        return null;
    }

    public static String replaceDotWithUnderScore(String itemCode) {
        //System.out.println("itemCode " + itemCode);
        if (!isEmpty(itemCode)) {
            return itemCode.replace('.', '_');
        }
        return null;
    }

    public static Long stringToLong(String str) {
        if ((str == null) || (str.length() == 0)) {
            return null;
        }
        try {
            Long newLong = Long.parseLong(str);
            return newLong;
        } catch (NumberFormatException ex) {
            return null;
        }
    }

    public static Double stringToDouble(String str) {
        if ((str == null) || (str.length() == 0)) {
            return null;
        }
        try {
            Double newDouble = Double.parseDouble(str);
            return newDouble;
        } catch (NumberFormatException ex) {
            return null;
        }

    }

    public static Timestamp stringToTimestamp(String sDate) { // throws Exception 
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("YYYY...");
        try {
            if (sDate != null) {
                if (!sDate.equals("")) {
                    sdf.setLenient(false);
                    d = sdf.parse(sDate);

                    Timestamp myTimestamp = new Timestamp(d.getTime());
                    return myTimestamp;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    public static Timestamp stringToTimestampv2(String sDate) throws Exception {
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("....");
        try {
            if (sDate != null) {
                if (!sDate.equals("")) {
                    d = sdf.parse(sDate);
                    Timestamp myTimestamp = new Timestamp(d.getTime());
                    return myTimestamp;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            // e.printStackTrace();
            return null;
        }
    }

    public static Date stringToDate(String sDate) throws Exception {
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("....");
        if (sDate != null) {
            if (!sDate.equals("")) {
                d = sdf.parse(sDate);
                return d;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public static String timestampToString(Timestamp tDate) throws Exception {
        if (tDate != null) {
            Date d = new Date();
            d.setTime(tDate.getTime());
            DateFormat dateFormat = new SimpleDateFormat("....");
            return dateFormat.format(d);
        } else {
            return "";
        }
    }

    public static String timestampToStringWithTimeFormat(Timestamp tDate, String language) throws Exception {
        if (tDate != null) {
            Date d = new Date();
            d.setTime(tDate.getTime());
            DateFormat dateFormat = new SimpleDateFormat(
                    "en".equalsIgnoreCase(language) ? "...." : "....", new Locale(language));
            return dateFormat.format(d);
        } else {
            return "";
        }
    }

    public static Date timestampToDate(Timestamp tDate) throws Exception {
        return stringToDate(timestampToString(tDate));
    }

    public static String formatMoneyI18N(Long mnt, String langue) {
        String moneyF = "";
        if (mnt != null) {

            if ("en".equalsIgnoreCase(langue)) {
                DecimalFormat moneyFormat = new DecimalFormat("....");
                moneyF = moneyFormat.format(mnt);
            }
            if ("fr".equalsIgnoreCase(langue)) {
                DecimalFormatSymbols unusualSymbols = new DecimalFormatSymbols(
                        new Locale("fr"));
                DecimalFormat moneyFormat = new DecimalFormat(
                        "....", unusualSymbols);
                moneyF = moneyFormat.format(mnt);
            }

        }
        return moneyF;
    }

    public static String formatMoney(Long mnt) {
        if (mnt != null) {

            DecimalFormat moneyFormat = null;
            return moneyFormat.format(mnt);
        } else {
            return "";
        }
    }

    public static String formatMoneyFrensh(Long mnt) {
        if (mnt != null) {
            DecimalFormatSymbols unusualSymbols = new DecimalFormatSymbols(
                    new Locale("fr"));

            DecimalFormat moneyFormat = new DecimalFormat(
                    "....", unusualSymbols);
            return moneyFormat.format(mnt);
        } else {
            return "";
        }
    }

    public static String getMonthLibele(int code, String lang) {

        String month = "";
        if ("fr".equals(lang)) {
            switch (code) {
                case 1: {
                    month = "janvier";
                    break;
                }
                case 2: {
                    month = "février";
                    break;
                }
                case 3: {
                    month = "mars";
                    break;
                }
                case 4: {
                    month = "avril";
                    break;
                }
                case 5: {
                    month = "mai";
                    break;
                }
                case 6: {
                    month = "juin";
                    break;
                }
                case 7:
                    month = "juillet";
                    break;
                case 8: {
                    month = "août";
                    break;
                }
                case 9: {
                    month = "septembre";
                    break;
                }
                case 10: {
                    month = "octobre";
                    break;
                }
                case 11: {
                    month = "novembre";
                    break;
                }
                case 12: {
                    month = "décembre";
                    break;
                }
                default:
                    return "";
            }
        } else if ("en".equals(lang)) {
            switch (code) {
                case 1: {
                    month = "January";
                    break;
                }
                case 2: {
                    month = "February";
                    break;
                }
                case 3: {
                    month = "March";
                    break;
                }
                case 4: {
                    month = "April";
                    break;
                }
                case 5: {
                    month = "May";
                    break;
                }
                case 6: {
                    month = "June";
                    break;
                }
                case 7:
                    month = "July";
                    break;
                case 8: {
                    month = "August";
                    break;
                }
                case 9: {
                    month = "September";
                    break;
                }
                case 10: {
                    month = "October";
                    break;
                }
                case 11: {
                    month = "November";
                    break;
                }
                case 12: {
                    month = "December";
                    break;
                }
                default:
                    return "";
            }
        }
        return month;

    }

    public static String charSpecToString(String x) {
        String a, b, c, d, e, f, m, n, o, p, q, l, h, g;

        a = String.valueOf(x.replace("’", "'"));
        b = String.valueOf(a.replace("…", "..."));
        c = String.valueOf(b.replace("ˆ", "^"));
        d = String.valueOf(c.replace("‹", "<"));
        e = String.valueOf(d.replace("›", ">"));
        f = String.valueOf(e.replace("›", ">"));
        m = String.valueOf(f.replace("–", "-"));
        n = String.valueOf(m.replace("’", "'"));
        o = String.valueOf(n.replace("‘", "'"));
        p = String.valueOf(o.replace("‚", ","));
        q = String.valueOf(p.replace("“", "'" + "'"));
        l = String.valueOf(q.replace("”", "'" + "'"));
        h = String.valueOf(l.replace("Œ", "OE"));
        g = String.valueOf(h.replace("œ", "oe"));

        return g;

    }

    public static boolean isDistinct(String s1, String s2) {
        if (isEmpty(s1) && isEmpty(s2)) {
            return true;
        } else if (!isEmpty(s1) && !isEmpty(s2)) {
            return !s1.equalsIgnoreCase(s2);
        } else {
            return true;
        }

    }

    public static boolean isLong(String value) {
        try {
            Long.parseLong(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isDouble(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean validateEmail(String value) {
        boolean valid = true;
        Pattern pattern;

        pattern = Pattern
                .compile("^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$");
        boolean res = pattern.matcher(value).matches();
        if (!res) {

            return res;
        } else {
            return valid;
        }

    }

    public static boolean validatePostalCode(String value) {
        boolean valid = true;
        Pattern pattern;

        pattern = Pattern
                .compile("^[a-zA-Z]{1}[0-9]{1}[a-zA-Z]{1}(-| ){1}[0-9]{1}[a-zA-Z]{1}[0-9]{1}$");
        boolean res = pattern.matcher(value).matches();
        if (!res) {

            return res;
        } else {
            return valid;
        }

    }

    public static boolean validatePostalCodeWithoutSpace(String value) {
        boolean valid = true;
        Pattern pattern;

        pattern = Pattern
                .compile("^[a-zA-Z]{1}[0-9]{1}[a-zA-Z]{1}[0-9]{1}[a-zA-Z]{1}[0-9]{1}$");
        boolean res = pattern.matcher(value).matches();
        return res;
    }

    public static String timestampToStringByFormat(Timestamp tDate, String pattern) throws Exception {
        if (tDate != null) {
            Date d = new Date();
            d.setTime(tDate.getTime());
            DateFormat dateFormat = new SimpleDateFormat(
                    pattern);
            return dateFormat.format(d);
        } else {
            return "";
        }
    }

    public static Timestamp getCurrentDate() {
        Timestamp currentDate = null;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date cDate = new Date();
        try {
            currentDate = StringUtils.stringToTimestamp(dateFormat.format(cDate));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return currentDate;
    }

    public static Timestamp getCurrentDate(String format) {
        Timestamp currentDate = null;
        DateFormat dateFormat = new SimpleDateFormat(format);
        Date cDate = new Date();
        try {
            currentDate = StringUtils.stringToTimestamp(dateFormat.format(cDate));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return currentDate;
    }

    public static String getItemCodeMessage(String itemCode) {
        return itemCode + "." + GlobalsAttributs.ITEM_CODE_MESSAGE;
    }

    public static String getItemCodeLabel(String itemCode) {
        return itemCode + "." + GlobalsAttributs.ITEM_CODE_LABEL;
    }

    public static String getMetaCodeLabel(String itemCode) {
        return "??? " + itemCode + "." + GlobalsAttributs.ITEM_CODE_LABEL + " ???";
    }

    public static String getItemCodeHelp(String itemCode) {
        return itemCode + "." + GlobalsAttributs.ITEM_CODE_HELP;
    }

    public static String getItemCodeComment(String itemCode) {
        return itemCode + "." + GlobalsAttributs.ITEM_CODE_COMMENT;
    }

    public static String getMetaCodeComment(String itemCode) {
        return "??? " + itemCode + "." + GlobalsAttributs.ITEM_CODE_COMMENT + " ???";
    }

    public static String getItemCodeTitle(String itemCode) {
        return itemCode + "." + GlobalsAttributs.ITEM_CODE_TITLE;
    }

    public static String getMetaCodeTitle(String itemCode) {
        return "??? " + itemCode + "." + GlobalsAttributs.ITEM_CODE_TITLE + " ???";
    }

    public static String sum(Long a, Long b, String language) {
        NumberFormat formatter = NumberFormat.getInstance(new Locale(language));
        long longNumber = a + b;
        if (a == null) {
            a = 0L;
        }
        if (b == null) {
            b = 0L;
        }

        try {
            longNumber = Long.valueOf(Long.parseLong(longNumber + ""));
        } catch (Exception e) {
        }
        return formatter.format(longNumber);
    }

    public static String replacePeriodYear(String header, String period, String year) {
        if (header != null) {
            header = header.replace("__YEAR__", year);
            header = header.replace("__PERIOD__", period);

        }
        return header;
    }

    public static boolean contains(String str, String value) {
        boolean contains = false;
        if ((str != null) && (str.contains(value))) {
            contains = true;
        }
        return contains;
    }

    public static boolean isListEmpty(List list) {
        if ((list == null) || (list.size() == 0)) {
            return true;
        } else {
            return false;
        }

    }

    public static int countWords(String s) {

        int wordCount = 0;

        boolean word = false;
        int endOfLine = s.length() - 1;

        for (int i = 0; i < s.length(); i++) {
            // if the char is a letter, word = true.
            if (Character.isLetter(s.charAt(i)) && i != endOfLine) {
                word = true;
                // if char isn't a letter and there have been letters before,
                // counter goes up.
            } else if (!Character.isLetter(s.charAt(i)) && word) {
                wordCount++;
                word = false;
                // last word of String; if it doesn't end with a non letter, it
                // wouldn't count without this.
            } else if (Character.isLetter(s.charAt(i)) && i == endOfLine) {
                wordCount++;
            }
        }
        return wordCount;
    }

    public static String insertCharPeriodically(
            String text, String insert, int period) {
        StringBuilder builder = new StringBuilder(
                text.length() + insert.length() * (text.length() / period) + 1);

        int index = 0;
        String prefix = "";
        while (index < text.length()) {
            builder.append(prefix);
            prefix = insert;
            builder.append(text.substring(index,
                    Math.min(index + period, text.length())));
            index += period;
        }
        return builder.toString();
    }

    public static boolean validateFormatDate(String date) {
        boolean checkformat;
        if (date.matches("^(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)$")) {
            checkformat = true;
        } else {
            checkformat = false;
        }
        return checkformat;
    }

    public static boolean isValidFormat(String format, String value) {
        String regex = "^((19|20)\\d\\d)-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])$";
        Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            sdf.setLenient(false);
            date = sdf.parse(value);
            if (!value.equals(sdf.format(date))) {
                date = null;
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return date != null;
    }

    public static String timestampToStringWithFormat(Timestamp tDate, String format) throws Exception {
        if (tDate != null) {
            Date d = new Date();
            d.setTime(tDate.getTime());
            DateFormat dateFormat = new SimpleDateFormat(format);
            return dateFormat.format(d);
        } else {
            return "";
        }
    }

    public static Timestamp stringToTimestampWithFormat(String sDate, String format) throws Exception {
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        try {
            if (sDate != null) {
                if (!sDate.equals("")) {
                    d = sdf.parse(sDate);
                    Timestamp myTimestamp = new Timestamp(d.getTime());
                    return myTimestamp;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            // e.printStackTrace();
            return null;
        }
    }

    public static String replaceTabReturn(String s) {
        if (!isEmpty(s)) {
            s = s.replaceAll("(\r\n|\n)", " ").toLowerCase();
        }
        return s;
    }

    public static int wordCounter(String s) {

        if (s == null || "".equals(s)) {
            return 0;
        }
        int count = 0;
        //s = Normalizer.normalize(s, Normalizer.Form.NFD);
        //s = s.replaceAll("[^\\p{ASCII}]", "");
        //Pattern pattern = Pattern.compile("[a-zA-Z0-9'_/ÈËÍÎ?·??ÛÚÙ?ÌÏÓÔÁ?» À¿¡¬????÷ÃÕ??«\\-]+");
        //Pattern pattern = Pattern.compile("[a-zA-Z0-9'_/ÈËÍÎ?·??ÛÚÙ?ÌÏÓÔÁ?» À¿¡¬????÷ÃÕ??«\\-]+");			

        /*  Pattern pattern = Pattern.compile("[a-zA-Z0-9'_/?·??ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ_?» ¿¡¬????÷??«\\-]+");
         // crÈation díun moteur de recherche
		
         Matcher matcher = pattern.matcher(s);
		
         // Tant que la recherche est fructueuse
		
		
         while (matcher.find()){				
         System.out.println(s.substring(matcher.start(), matcher.end()));			     
         count++;
			
         }
         */
        String regExp = "\\s+";
        count = s.trim().split(regExp).length;
        System.out.println("Count: " + count);

        return count;
    }

    public static long wordCounterv2(String s) {

        if (s == null || "".equals(s)) {
            return 0;
        }
        long count = 0;
        //s = Normalizer.normalize(s, Normalizer.Form.NFD);
        //s = s.replaceAll("[^\\p{ASCII}]", "");
        Pattern pattern = Pattern.compile("[a-zA-Z0-9'_/ÈËÍÎ?·??ÛÚÙ?ÌÏÓÔÁ?» À¿¡¬????÷ÃÕ??«\\-]+");

        // crÈation díun moteur de recherche
        Matcher matcher = pattern.matcher(s);

        // Tant que la recherche est fructueuse
        while (matcher.find()) {

            count++;

        }

        System.out.println("Count of words is : " + count);

        return count;
    }

    public static boolean compareDate(String d1, String d2) throws Exception {
        //Si d1 > d2 Return false
        try {
            if (!isEmpty(d1) && !isEmpty(d2)) {
                long val1 = StringUtils.stringToTimestamp(d1).getTime();
                long val2 = StringUtils.stringToTimestamp(d2).getTime();
                System.out.println("compareDate.val1 : " + val1);
                System.out.println("compareDate.val2 : " + val2);
                if (val1 > val2) {
                    return false;
                }
            }
        } catch (Exception e) {
            return true;
        }
        return true;
    }

    public static String stringReplaceByHtmlCode(String chaine) {
        chaine = chaine.replaceAll("&", "&amp;");
        return chaine;
    }

    public static Date stringToDateWithFormat(String sDate, String format) throws Exception {
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        if (sDate != null) {
            if (!sDate.equals("")) {
                d = sdf.parse(sDate);
                return d;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public static boolean rangeValide(String value, Long minValue, Long maxValue) {
        try {
            Long year = Long.parseLong(value);
            System.out.println("year" + year + " min" + minValue + " max" + maxValue);
            if (year < minValue || year > maxValue) {
                System.out.println("here 1");
                return false;
            }
            return true;
        } catch (NumberFormatException e) {
            System.out.println("here 2");
            return false;
        }
    }

    private static HttpServletRequest getRequest() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();
        return request;
    }

    public static Date convertToDate(String stringDate, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date theDate = null;
        try {
            theDate = sdf.parse(stringDate);
        } catch (ParseException ex) {
        }
        return theDate;
    }

    public static String convertDateToString(Date date, String format) {
        if (date != null) {

            return new SimpleDateFormat(format).format(date);
        } else {
            return null;
        }
    }

    public static boolean isNumeric(String str) {
        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }

    public static String formaDecimal(double mnt) {
        String mony = "0.00";
        try {
            DecimalFormat decimalFormat = new DecimalFormat("##0.00");
            mony = decimalFormat.format(mnt);
            mony = mony.replaceAll(",", ".");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return mony;
    }

    public static Timestamp stringToTimestamp(String sDate, String language, Locale locale) throws Exception {
        Date d = new Date();
        SimpleDateFormat sdf = ("fr".equalsIgnoreCase(language) ? new SimpleDateFormat("yyyy-MMMM-dd", locale) : new SimpleDateFormat("yyyy-MMM-dd", locale));
        try {
            if (sDate != null) {
                if (!sDate.equals("")) {
                    sdf.setLenient(false);
                    d = sdf.parse(sDate);

                    Timestamp myTimestamp = new Timestamp(d.getTime());
                    return myTimestamp;
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            // e.printStackTrace();
            return null;
        }
    }

    private static String[][] htmlEscape
            = {{"&lt;", "<"}, {"&gt;", ">"},
            {"&amp;", "&"}, {"&quot;", "\""},
            {"&agrave;", "�"}, {"&Agrave;", "�"},
            {"&acirc;", "�"}, {"&auml;", "�"},
            {"&Auml;", "�"}, {"&Acirc;", "�"},
            {"&aring;", "�"}, {"&Aring;", "�"},
            {"&aelig;", "�"}, {"&AElig;", "�"},
            {"&ccedil;", "�"}, {"&Ccedil;", "�"},
            {"&eacute;", "�"}, {"&Eacute;", "�"},
            {"&egrave;", "�"}, {"&Egrave;", "�"},
            {"&ecirc;", "�"}, {"&Ecirc;", "�"},
            {"&euml;", "�"}, {"&Euml;", "�"},
            {"&iuml;", "�"}, {"&Iuml;", "�"},
            {"&ocirc;", "�"}, {"&Ocirc;", "�"},
            {"&ouml;", "�"}, {"&Ouml;", "�"},
            {"&oslash;", "�"}, {"&Oslash;", "�"},
            {"&szlig;", "�"}, {"&ugrave;", "�"},
            {"&Ugrave;", "�"}, {"&ucirc;", "�"},
            {"&Ucirc;", "�"}, {"&uuml;", "�"},
            {"&Uuml;", "�"},
            {"&copy;", "\u00a9"},
            {"&reg;", "\u00ae"},
            {"&euro;", "\u20a0"}
            };

    public static String replaceCaractaireSpecieaux(String labele) {
        int size = labele.length();
        System.out.println(htmlEscape.length);
        for (int j = 0; j < htmlEscape.length; j++) {
            for (int i = 0; i < size; i++) {
                //System.out.println(htmlEscape[j][1]+"|"+labele.charAt(i));
                if (htmlEscape[j][1].equals(labele.charAt(i) + "")) {
                    labele = labele.replace(htmlEscape[j][1], htmlEscape[j][0]);
                    break;
                }
            }
        }
        System.out.println(labele);
        return labele;
    }

    public static String toUpper(String val) {

        try {
            if ("".equalsIgnoreCase(val)) {
                return "";
            } else {
                return val.toUpperCase();
            }
        } catch (Exception e) {
            System.out.println("Exception : String toUpper(String val)");
            return "";
        }

    }

}
