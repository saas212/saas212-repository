/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.functionUtils.params;

import ma.mystock.web.utils.values.MapsGetter;

/**
 *
 * @author anasshajami
 */
public class BasicParameterUtils {

    // Parameter
    public static final Long PARAM_1_APPLICATION_URL = 1L;
    public static final Long PARAM_2_COMPANY_RIGHTS = 2L;
    public static final Long PARAM_3_COMPANY_NAME = 3L;
    public static final Long PARAM_4_REFRESH_MAPS_URL = 4L;
    public static final Long PARAM_5_OFFICIAL_WEBSITE = 5L;
    public static final Long PARAM_6_GET_ACCOUNT_URL = 6L;
    public static final Long PARAM_7_PRODUCT_VERSION = 7L;
    public static final Long PARAM_8_ENVIRONMENT = 8L;

    public static final String ENVIRONMENT_LOCAL = "LOCAL";
    public static final String ENVIRONMENT_DEV = "DEV";
    public static final String ENVIRONMENT_TEST = "TEST";
    public static final String ENVIRONMENT_PROD = "PROD";

    // gettter
    public static String getApplicationUrl() {
        return MapsGetter.getConfigBasicMap().get(PARAM_1_APPLICATION_URL.toString());
    }

    public static String getCompanyRights() {
        return MapsGetter.getConfigBasicMap().get(PARAM_2_COMPANY_RIGHTS.toString());
    }

    public static String getCompanyName() {
        return MapsGetter.getConfigBasicMap().get(PARAM_3_COMPANY_NAME.toString());
    }

    public static String getRefreshMapsUrl() {
        return MapsGetter.getConfigBasicMap().get(PARAM_4_REFRESH_MAPS_URL.toString());
    }

    public static String getOffecialWebSite() {
        return MapsGetter.getConfigBasicMap().get(PARAM_5_OFFICIAL_WEBSITE.toString());
    }

    public static String getAccountUrl() {
        return MapsGetter.getConfigBasicMap().get(PARAM_6_GET_ACCOUNT_URL.toString());
    }

    public static String getProductVersion() {
        return MapsGetter.getConfigBasicMap().get(PARAM_7_PRODUCT_VERSION.toString());
    }

    public static String getEnvironment() {
        return MapsGetter.getConfigBasicMap().get(PARAM_8_ENVIRONMENT.toString());
    }

    // Util
    public static boolean isLOCAL() {
        return ENVIRONMENT_LOCAL.equalsIgnoreCase(getEnvironment());
        //return true;
    }

    public static boolean isDEV() {
        return ENVIRONMENT_DEV.equalsIgnoreCase(getEnvironment());
    }

    public static boolean isTEST() {
        return ENVIRONMENT_TEST.equalsIgnoreCase(getEnvironment());
    }

    public static boolean isPROD() {
        return ENVIRONMENT_PROD.equalsIgnoreCase(getEnvironment());
    }

}
