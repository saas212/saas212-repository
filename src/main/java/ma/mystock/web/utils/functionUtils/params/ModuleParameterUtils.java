/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.functionUtils.params;

/**
 *
 * @author anasshajami
 */
// Ajouter PARAM_1_
public class ModuleParameterUtils {

    public final static Double PARAMS_TVA_DEFAULT_VALUE = new Double("20.00");
    public final static String PARAMS_CURRENCY_DEFAULT_VALUE = "DH";

    public final static String PARAMS_DATE_FORMAT_DEFAULT_VALUE = "yyyy-MM-dd";
    public final static String PARAMS_DATE_TIME_FORMAT_DEFAULT_VALUE = "yyyy-MM-dd HH:mm";

    public static Double getTvaDefaultValue() {
        return PARAMS_TVA_DEFAULT_VALUE;
    }

    public static String getCurrencyDefaultValue() {
        return PARAMS_CURRENCY_DEFAULT_VALUE;
    }

    public static String getDateFormatDefaultValue() {
        return PARAMS_DATE_FORMAT_DEFAULT_VALUE;
    }

    public static String getDateTimeFormatDefaultValue() {
        return PARAMS_DATE_TIME_FORMAT_DEFAULT_VALUE;
    }
}
