/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils.functionUtils;

import java.net.UnknownHostException;

/**
 *
 * @author Abdessamad HALLAL
 */
public class ServerUtils {

    public static String getServerName() {
        try {
            return java.net.InetAddress.getLocalHost().getHostName().toLowerCase();
        } catch (UnknownHostException ex) {
            System.out.println("ServerUtils.getServerName : " + ex.getMessage());
            return "";
        }
    }
}
