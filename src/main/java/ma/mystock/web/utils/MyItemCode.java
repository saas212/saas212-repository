/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.web.utils;

/**
 *
 * @author abdou
 */
public class MyItemCode extends MyParent {

    public static String getProperty(String itemCode) {
        String item = "";
        try {

            item = itemCode.replace('.', '_');
            item = item.split("_")[item.split("_").length - 1];

        } catch (Exception ex) {
            log.info("Exception : getProperty + " + itemCode);
        }
        return item;
    }

    public static String getPage(String itemCode) {

        String item = "";

        try {
            item = itemCode.replace('.', '_');
            item = item.split("_")[0];

        } catch (Exception ex) {
            log.info("Exception : getProperty + " + itemCode);
        }
        return item;
    }

    public static String getName(String itemCode) {
        String item = "";

        try {
            item = getPage(itemCode) + getProperty(itemCode);
        } catch (Exception ex) {
            log.info("Exception : getProperty + " + itemCode);
        }

        return item;
    }

}
