package ma.mystock.config;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pro
 */
@Configuration
@ImportResource("classpath:/WEB-INF/applicationContext.xml")
@ComponentScan(basePackages = { "ma.mystock" })
public class CoreConfig {
    
}
