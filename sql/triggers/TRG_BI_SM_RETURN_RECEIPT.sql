
-- DROP TRIGGER TRG_BI_SM_RETURN_RECEIPT;
DELIMITER |

CREATE TRIGGER TRG_BI_SM_RETURN_RECEIPT BEFORE INSERT ON SM_RETURN_RECEIPT
FOR EACH ROW BEGIN

    SET NEW.DATE_CREATION = NOW();
    
    IF EXISTS(SELECT * FROM SM_RETURN_RECEIPT WHERE CLT_MODULE_ID = NEW.CLT_MODULE_ID) THEN
		SET NEW.NO_SEQ = (SELECT MAX(NO_SEQ) + 1 FROM SM_RETURN_RECEIPT WHERE CLT_MODULE_ID = NEW.CLT_MODULE_ID);
    ELSE
		SET NEW.NO_SEQ = 1;
	END IF;
END;

|
DELIMITER ;



