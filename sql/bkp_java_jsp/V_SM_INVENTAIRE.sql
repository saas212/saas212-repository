
-- a supprimer

CREATE OR REPLACE VIEW V_SM_INVENTAIRE AS 

SELECT
    SM_P.ID AS ID,
    SM_P.REFERENCE AS REFERENCE,
    SM_PC.NAME AS PRODUCT_COLOR_NAME,
    SM_PS.NAME AS PRODUCT_SIZE_NAME,
    
    SM_P.DESIGNATION AS DESIGNATION,
    SM_P.QUANTITY AS QUANTITY,
    SM_P.PRICE_BUY AS PRICE_BUY,
    SM_P.PRICE_SALE AS PRICE_SALE,
    SM_P.ACTIVE AS ACTIVE,
    COUNT(SM_PD.RECEPTION_ID) RECEPTION_VALID_COUNT, 
    MAX(SM_PD.UNIT_PRICE_BUY)  UNIT_PRICE_BUY_MAX,
    AVG(SM_PD.UNIT_PRICE_BUY)  UNIT_PRICE_BUY_MOYENNE,
    MIN(SM_PD.UNIT_PRICE_BUY)  UNIT_PRICE_BUY_MIN,
    COUNT(SM_PD.QUANTITY) QUANTITY_COUNT

FROM SM_PRODUCT SM_P, SM_PRODUCT_DETAILS SM_PD, SM_PRODUCT_COLOR SM_PC, SM_PRODUCT_SIZE SM_PS

WHERE SM_P.ID = SM_PD.PRODUCT_ID  AND SM_P.PRODUCT_COLOR_ID = SM_PC.ID AND SM_P.PRODUCT_SIZE_ID = SM_PS.ID
AND   UPPER(SM_PD.ACTIVE) LIKE UPPER('Y')

GROUP BY SM_P.REFERENCE, SM_P.DESIGNATION, SM_P.QUANTITY,SM_P.PRICE_BUY, SM_P.PRICE_SALE, SM_P.ACTIVE;

SELECT * FROM V_SM_INVENTAIRE;
