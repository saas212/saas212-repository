/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_SM_SIZE_BY_REFERENCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmSizeByReference.findAll", query = "SELECT v FROM VSmSizeByReference v"),
    @NamedQuery(name = "VSmSizeByReference.findByReference", query = "SELECT v FROM VSmSizeByReference v WHERE v.reference = :reference"),
    @NamedQuery(name = "VSmSizeByReference.findByProductSizeId", query = "SELECT v FROM VSmSizeByReference v WHERE v.productSizeId = :productSizeId"),
    @NamedQuery(name = "VSmSizeByReference.findByProductSizeName", query = "SELECT v FROM VSmSizeByReference v WHERE v.productSizeName = :productSizeName"),
    @NamedQuery(name = "VSmSizeByReference.findByCltModuleId", query = "SELECT v FROM VSmSizeByReference v WHERE v.cltModuleId = :cltModuleId")})
public class VSmSizeByReference implements Serializable {
    private static final Long serialVersionUID = 1L;
    public static final String findByReferenceAndCltModuleId = "SELECT v FROM VSmSizeByReference v WHERE v.reference = :reference and v.cltModuleId = :cltModuleId";
    @Id
    @Column(name = "CODE")
    private String code;
    @Size(max = 255)
    @Column(name = "REFERENCE")
    private String reference;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRODUCT_SIZE_ID")
    private Long productSizeId;
    @Size(max = 255)
    @Column(name = "PRODUCT_SIZE_NAME")
    private String productSizeName;
    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    public VSmSizeByReference() {
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Long getProductSizeId() {
        return productSizeId;
    }

    public void setProductSizeId(Long productSizeId) {
        this.productSizeId = productSizeId;
    }

    public String getProductSizeName() {
        return productSizeName;
    }

    public void setProductSizeName(String productSizeName) {
        this.productSizeName = productSizeName;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    
}
