/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "V_SM_COLOR_BY_REFERENCE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmColorByReference.findAll", query = "SELECT v FROM VSmColorByReference v"),
    @NamedQuery(name = "VSmColorByReference.findByReference", query = "SELECT v FROM VSmColorByReference v WHERE v.reference = :reference"),
    @NamedQuery(name = "VSmColorByReference.findByProductColorId", query = "SELECT v FROM VSmColorByReference v WHERE v.productColorId = :productColorId"),
    @NamedQuery(name = "VSmColorByReference.findByProductColorName", query = "SELECT v FROM VSmColorByReference v WHERE v.productColorName = :productColorName"),
    @NamedQuery(name = "VSmColorByReference.findByCltModuleId", query = "SELECT v FROM VSmColorByReference v WHERE v.cltModuleId = :cltModuleId")})
public class VSmColorByReference implements Serializable {
    private static final Long serialVersionUID = 1L;
    
    public static final String findByReferenceAndCltModuleId = "SELECT v FROM VSmColorByReference v WHERE v.reference = :reference and v.cltModuleId = :cltModuleId ";
    @Id
    @Column(name = "CODE")
    private String code;
    
    @Size(max = 255)
    @Column(name = "REFERENCE")
    private String reference;
    @Basic(optional = false)
    @NotNull
    @Column(name = "PRODUCT_COLOR_ID")
    private Long productColorId;
    @Size(max = 255)
    @Column(name = "PRODUCT_COLOR_NAME")
    private String productColorName;
    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    public VSmColorByReference() {
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Long getProductColorId() {
        return productColorId;
    }

    public void setProductColorId(Long productColorId) {
        this.productColorId = productColorId;
    }

    public String getProductColorName() {
        return productColorName;
    }

    public void setProductColorName(String productColorName) {
        this.productColorName = productColorName;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    
    
}
