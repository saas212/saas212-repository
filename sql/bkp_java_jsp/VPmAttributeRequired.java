/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "v_pm_attribute_required")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VPmAttributeRequired.findAll", query = "SELECT v FROM VPmAttributeRequired v"),
    @NamedQuery(name = "VPmAttributeRequired.findByCode", query = "SELECT v FROM VPmAttributeRequired v WHERE v.code = :code"),
    @NamedQuery(name = "VPmAttributeRequired.findByInfItemCode", query = "SELECT v FROM VPmAttributeRequired v WHERE v.infItemCode = :infItemCode"),
    @NamedQuery(name = "VPmAttributeRequired.findByPageId", query = "SELECT v FROM VPmAttributeRequired v WHERE v.pageId = :pageId"),
    @NamedQuery(name = "VPmAttributeRequired.findByCltModuleId", query = "SELECT v FROM VPmAttributeRequired v WHERE v.cltModuleId = :cltModuleId")})
public class VPmAttributeRequired implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static String findAll = "SELECT v FROM VPmAttributeRequired v";

    @Id
    @Column(name = "CODE")
    private String code;

    @Column(name = "INF_ITEM_CODE")
    private String infItemCode;

    @Column(name = "PAGE_ID")
    private Long pageId;

    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    public VPmAttributeRequired() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getInfItemCode() {
        return infItemCode;
    }

    public void setInfItemCode(String infItemCode) {
        this.infItemCode = infItemCode;
    }

    public Long getPageId() {
        return pageId;
    }

    public void setPageId(Long pageId) {
        this.pageId = pageId;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

}
