/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "SM_RECEPTION_PRODUCTS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SmReceptionProducts.findAll", query = "SELECT s FROM SmReceptionProducts s"),
    @NamedQuery(name = "SmReceptionProducts.findById", query = "SELECT s FROM SmReceptionProducts s WHERE s.id = :id"),
    @NamedQuery(name = "SmReceptionProducts.findByProductId", query = "SELECT s FROM SmReceptionProducts s WHERE s.productId = :productId"),
    @NamedQuery(name = "SmReceptionProducts.findByDesignation", query = "SELECT s FROM SmReceptionProducts s WHERE s.designation = :designation"),
    @NamedQuery(name = "SmReceptionProducts.findByUnitPriceBuy", query = "SELECT s FROM SmReceptionProducts s WHERE s.unitPriceBuy = :unitPriceBuy"),
    @NamedQuery(name = "SmReceptionProducts.findByRemise", query = "SELECT s FROM SmReceptionProducts s WHERE s.remise = :remise"),
    @NamedQuery(name = "SmReceptionProducts.findByQuantity", query = "SELECT s FROM SmReceptionProducts s WHERE s.quantity = :quantity"),
    @NamedQuery(name = "SmReceptionProducts.findByTva", query = "SELECT s FROM SmReceptionProducts s WHERE s.tva = :tva"),
    @NamedQuery(name = "SmReceptionProducts.findByActive", query = "SELECT s FROM SmReceptionProducts s WHERE s.active = :active"),
    @NamedQuery(name = "SmReceptionProducts.findByReceptionId", query = "SELECT s FROM SmReceptionProducts s WHERE s.receptionId = :receptionId"),
    @NamedQuery(name = "SmReceptionProducts.findByDateCreation", query = "SELECT s FROM SmReceptionProducts s WHERE s.dateCreation = :dateCreation"),
    @NamedQuery(name = "SmReceptionProducts.findByUserCreation", query = "SELECT s FROM SmReceptionProducts s WHERE s.userCreation = :userCreation"),
    @NamedQuery(name = "SmReceptionProducts.findByDateUpdate", query = "SELECT s FROM SmReceptionProducts s WHERE s.dateUpdate = :dateUpdate"),
    @NamedQuery(name = "SmReceptionProducts.findByUserUpdate", query = "SELECT s FROM SmReceptionProducts s WHERE s.userUpdate = :userUpdate")})
public class SmReceptionProducts implements Serializable {
    private static final long serialVersionUID = 1L;
    
    public static final String findById = "SELECT s FROM SmReceptionProducts s WHERE s.id = :id";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Long id;
    @Column(name = "PRODUCT_ID")
    private Long productId;
    @Size(max = 255)
    @Column(name = "DESIGNATION")
    private String designation;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "UNIT_PRICE_BUY")
    private Double unitPriceBuy;
    @Column(name = "REMISE")
    private Double remise;
    @Column(name = "QUANTITY")
    private Long quantity;
    @Column(name = "TVA")
    private Double tva;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "RECEPTION_ID")
    private Long receptionId;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    public SmReceptionProducts() {
    }

    public SmReceptionProducts(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
    

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Double getUnitPriceBuy() {
        return unitPriceBuy;
    }

    public void setUnitPriceBuy(Double unitPriceBuy) {
        this.unitPriceBuy = unitPriceBuy;
    }

    public Double getRemise() {
        return remise;
    }

    public void setRemise(Double remise) {
        this.remise = remise;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getTva() {
        return tva;
    }

    public void setTva(Double tva) {
        this.tva = tva;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Long getReceptionId() {
        return receptionId;
    }

    public void setReceptionId(Long receptionId) {
        this.receptionId = receptionId;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SmReceptionProducts)) {
            return false;
        }
        SmReceptionProducts other = (SmReceptionProducts) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ma.mystock.core.views.SmReceptionProducts[ id=" + id + " ]";
    }
    
}
