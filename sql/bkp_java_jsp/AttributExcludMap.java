/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.mystock.web.listners.helpers;

import java.util.HashMap;
import ma.mystock.web.utils.sessions.SessionsGetter;

/**
 *
 * @author abdou
 */
public class AttributExcludMap  extends HashMap<String, String> {
    
    
    @Override
    public String get(Object key) {
        return  super.get(SessionsGetter.getCltModuleId()+ "." +key);
    }
    
    
}
