/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdessamad HALLAL
 */
@Entity
@Table(name = "v_pm_attribute_readonly")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VPmAttributeReadonly.findAll", query = "SELECT v FROM VPmAttributeReadonly v"),
    @NamedQuery(name = "VPmAttributeReadonly.findByCode", query = "SELECT v FROM VPmAttributeReadonly v WHERE v.code = :code"),
    @NamedQuery(name = "VPmAttributeReadonly.findByInfItemCode", query = "SELECT v FROM VPmAttributeReadonly v WHERE v.infItemCode = :infItemCode"),
    @NamedQuery(name = "VPmAttributeReadonly.findByPageId", query = "SELECT v FROM VPmAttributeReadonly v WHERE v.pageId = :pageId"),
    @NamedQuery(name = "VPmAttributeReadonly.findByCltModuleId", query = "SELECT v FROM VPmAttributeReadonly v WHERE v.cltModuleId = :cltModuleId")})
public class VPmAttributeReadonly implements Serializable {

    private static final Long serialVersionUID = 1L;
    
    public static final String findAll = "SELECT v FROM VPmAttributeReadonly v";
    @Id
    @Column(name = "code")
    private String code;

    @Column(name = "inf_item_code")
    private String infItemCode;

    @Column(name = "page_id")
    private Long pageId;

    @Basic(optional = false)
    @NotNull
    @Column(name = "clt_module_id")
    private int cltModuleId;

    public VPmAttributeReadonly() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getInfItemCode() {
        return infItemCode;
    }

    public void setInfItemCode(String infItemCode) {
        this.infItemCode = infItemCode;
    }

    public Long getPageId() {
        return pageId;
    }

    public void setPageId(Long pageId) {
        this.pageId = pageId;
    }

    public int getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(int cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

}
