/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "v_sm_inventaire")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmInventaire.findAll", query = "SELECT v FROM VSmInventaire v"),
    @NamedQuery(name = "VSmInventaire.findById", query = "SELECT v FROM VSmInventaire v WHERE v.id = :id"),
    @NamedQuery(name = "VSmInventaire.findByReference", query = "SELECT v FROM VSmInventaire v WHERE v.reference = :reference"),
    @NamedQuery(name = "VSmInventaire.findByDesignation", query = "SELECT v FROM VSmInventaire v WHERE v.designation = :designation"),
    @NamedQuery(name = "VSmInventaire.findByQuantity", query = "SELECT v FROM VSmInventaire v WHERE v.quantity = :quantity"),
    @NamedQuery(name = "VSmInventaire.findByPriceBuy", query = "SELECT v FROM VSmInventaire v WHERE v.priceBuy = :priceBuy"),
    @NamedQuery(name = "VSmInventaire.findByPriceSale", query = "SELECT v FROM VSmInventaire v WHERE v.priceSale = :priceSale"),
    @NamedQuery(name = "VSmInventaire.findByActive", query = "SELECT v FROM VSmInventaire v WHERE v.active = :active"),
    @NamedQuery(name = "VSmInventaire.findByReceptionValidCount", query = "SELECT v FROM VSmInventaire v WHERE v.receptionValidCount = :receptionValidCount"),
    @NamedQuery(name = "VSmInventaire.findByUnitPriceBuyMax", query = "SELECT v FROM VSmInventaire v WHERE v.unitPriceBuyMax = :unitPriceBuyMax"),
    @NamedQuery(name = "VSmInventaire.findByUnitPriceBuyMoyenne", query = "SELECT v FROM VSmInventaire v WHERE v.unitPriceBuyMoyenne = :unitPriceBuyMoyenne"),
    @NamedQuery(name = "VSmInventaire.findByUnitPriceBuyMin", query = "SELECT v FROM VSmInventaire v WHERE v.unitPriceBuyMin = :unitPriceBuyMin"),
    @NamedQuery(name = "VSmInventaire.findByQuantityCount", query = "SELECT v FROM VSmInventaire v WHERE v.quantityCount = :quantityCount")})
public class VSmInventaire implements Serializable {
    
    private static final Long serialVersionUID = 1L;
    public static final String findAll = "SELECT v FROM VSmInventaire v"; 
    
    @Basic(optional = false)
    @Id
    @NotNull
    @Column(name = "id")
    private Long id;
    @Size(max = 255)
    @Column(name = "reference")
    private String reference;
    @Size(max = 255)
    @Column(name = "designation")
    private String designation;
    @Column(name = "quantity")
    private Long quantity;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price_buy")
    private Double priceBuy;
    @Column(name = "price_sale")
    private Double priceSale;
    @Column(name = "active")
    private String active;
    @Basic(optional = false)
    @NotNull
    @Column(name = "reception_valid_count")
    private Long receptionValidCount;
    @Column(name = "unit_price_buy_max")
    private Double unitPriceBuyMax;
    @Column(name = "unit_price_buy_moyenne")
    private Double unitPriceBuyMoyenne;
    @Column(name = "unit_price_buy_min")
    private Double unitPriceBuyMin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantity_count")
    private Long quantityCount;
    
    @Column(name = "product_color_name")
    private String productColorName;
    
    @Column(name = "product_size_name")
    private String productSizeName;
    
    
    public VSmInventaire() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getPriceBuy() {
        return priceBuy;
    }

    public void setPriceBuy(Double priceBuy) {
        this.priceBuy = priceBuy;
    }

    public Double getPriceSale() {
        return priceSale;
    }

    public void setPriceSale(Double priceSale) {
        this.priceSale = priceSale;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Long getReceptionValidCount() {
        return receptionValidCount;
    }

    public void setReceptionValidCount(Long receptionValidCount) {
        this.receptionValidCount = receptionValidCount;
    }

    public Double getUnitPriceBuyMax() {
        return unitPriceBuyMax;
    }

    public void setUnitPriceBuyMax(Double unitPriceBuyMax) {
        this.unitPriceBuyMax = unitPriceBuyMax;
    }

    public Double getUnitPriceBuyMoyenne() {
        return unitPriceBuyMoyenne;
    }

    public void setUnitPriceBuyMoyenne(Double unitPriceBuyMoyenne) {
        this.unitPriceBuyMoyenne = unitPriceBuyMoyenne;
    }

    public Double getUnitPriceBuyMin() {
        return unitPriceBuyMin;
    }

    public void setUnitPriceBuyMin(Double unitPriceBuyMin) {
        this.unitPriceBuyMin = unitPriceBuyMin;
    }

    public Long getQuantityCount() {
        return quantityCount;
    }

    public void setQuantityCount(Long quantityCount) {
        this.quantityCount = quantityCount;
    }

    public String getProductColorName() {
        return productColorName;
    }

    public void setProductColorName(String productColorName) {
        this.productColorName = productColorName;
    }

    public String getProductSizeName() {
        return productSizeName;
    }

    public void setProductSizeName(String productSizeName) {
        this.productSizeName = productSizeName;
    }
    
}
