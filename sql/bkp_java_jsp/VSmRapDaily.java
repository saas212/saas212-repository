/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "v_sm_rap_daily")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmRapDaily.findAll", query = "SELECT v FROM VSmRapDaily v"),
    @NamedQuery(name = "VSmRapDaily.findByOrderLineId", query = "SELECT v FROM VSmRapDaily v WHERE v.orderLineId = :orderLineId"),
    @NamedQuery(name = "VSmRapDaily.findByClinetId", query = "SELECT v FROM VSmRapDaily v WHERE v.clinetId = :clinetId"),
    @NamedQuery(name = "VSmRapDaily.findByModuleId", query = "SELECT v FROM VSmRapDaily v WHERE v.moduleId = :moduleId"),
    @NamedQuery(name = "VSmRapDaily.findByClinetFirstName", query = "SELECT v FROM VSmRapDaily v WHERE v.clinetFirstName = :clinetFirstName"),
    @NamedQuery(name = "VSmRapDaily.findByReference", query = "SELECT v FROM VSmRapDaily v WHERE v.reference = :reference"),
    @NamedQuery(name = "VSmRapDaily.findByProductColorName", query = "SELECT v FROM VSmRapDaily v WHERE v.productColorName = :productColorName"),
    @NamedQuery(name = "VSmRapDaily.findByProductSizeName", query = "SELECT v FROM VSmRapDaily v WHERE v.productSizeName = :productSizeName"),
    @NamedQuery(name = "VSmRapDaily.findByUnitPriceBuy", query = "SELECT v FROM VSmRapDaily v WHERE v.unitPriceBuy = :unitPriceBuy"),
    @NamedQuery(name = "VSmRapDaily.findByQuantity", query = "SELECT v FROM VSmRapDaily v WHERE v.quantity = :quantity"),
    @NamedQuery(name = "VSmRapDaily.findByTotalPriceBuy", query = "SELECT v FROM VSmRapDaily v WHERE v.totalPriceBuy = :totalPriceBuy"),
    @NamedQuery(name = "VSmRapDaily.findByDateCreation", query = "SELECT v FROM VSmRapDaily v WHERE v.dateCreation = :dateCreation")})
public class VSmRapDaily implements Serializable {
    private static final Long serialVersionUID = 1L;
    
    public static final String findByModuleId = "SELECT v FROM VSmRapDaily v WHERE v.moduleId = :moduleId order by v.dateCreation desc";
    
    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "order_line_id")
    private Long orderLineId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "clinet_id")
    private Long clinetId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "module_id")
    private Long moduleId;
    @Size(max = 255)
    @Column(name = "clinet_first_name")
    private String clinetFirstName;
    @Size(max = 255)
    @Column(name = "reference")
    private String reference;
    @Size(max = 255)
    @Column(name = "product_color_name")
    private String productColorName;
    @Size(max = 255)
    @Column(name = "product_size_name")
    private String productSizeName;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "unit_price_buy")
    private Double unitPriceBuy;
    @Column(name = "quantity")
    private Long quantity;
    @Column(name = "total_price_buy")
    private Double totalPriceBuy;
    @Column(name = "date_creation")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    public VSmRapDaily() {
    }

    public Long getOrderLineId() {
        return orderLineId;
    }

    public void setOrderLineId(Long orderLineId) {
        this.orderLineId = orderLineId;
    }

    public Long getClinetId() {
        return clinetId;
    }

    public void setClinetId(Long clinetId) {
        this.clinetId = clinetId;
    }

    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

    public String getClinetFirstName() {
        return clinetFirstName;
    }

    public void setClinetFirstName(String clinetFirstName) {
        this.clinetFirstName = clinetFirstName;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getProductColorName() {
        return productColorName;
    }

    public void setProductColorName(String productColorName) {
        this.productColorName = productColorName;
    }

    public String getProductSizeName() {
        return productSizeName;
    }

    public void setProductSizeName(String productSizeName) {
        this.productSizeName = productSizeName;
    }

    public Double getUnitPriceBuy() {
        return unitPriceBuy;
    }

    public void setUnitPriceBuy(Double unitPriceBuy) {
        this.unitPriceBuy = unitPriceBuy;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getTotalPriceBuy() {
        return totalPriceBuy;
    }

    public void setTotalPriceBuy(Double totalPriceBuy) {
        this.totalPriceBuy = totalPriceBuy;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }
    
}
