package ma.mystock.core.dao.entities.views;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @Important cette les querys + fileds + tous à vérifier 
 */
@Entity
@Table(name = "v_clt_parameter_client")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VCltParameterClient.findAll", query = "SELECT v FROM VCltParameterClient v"),
    @NamedQuery(name = "VCltParameterClient.findByParamaterName", query = "SELECT v FROM VCltParameterClient v WHERE v.paramaterName = :paramaterName"),
    @NamedQuery(name = "VCltParameterClient.findByParameterTypeName", query = "SELECT v FROM VCltParameterClient v WHERE v.parameterTypeName = :parameterTypeName"),
    @NamedQuery(name = "VCltParameterClient.findByClinetId", query = "SELECT v FROM VCltParameterClient v WHERE v.clinetId = :clinetId"),
    @NamedQuery(name = "VCltParameterClient.findByParameterId", query = "SELECT v FROM VCltParameterClient v WHERE v.parameterId = :parameterId"),
    @NamedQuery(name = "VCltParameterClient.findByActive", query = "SELECT v FROM VCltParameterClient v WHERE v.active = :active"),
    @NamedQuery(name = "VCltParameterClient.findByClientAndActive", query = "SELECT v FROM VCltParameterClient v where v.clinetId = :clinetId and v.active = :active")})
public class VCltParameterClient implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String findAll = "SELECT v FROM VCltParameterClient v";
    public static final String findById = "SELECT v FROM VCltParameterClient v WHERE v.id = :id";
    public static final String findAllActive = "SELECT v FROM VCltParameterClient v WHERE v.active = :active";
    public static final String findByClientAndActive = "SELECT v FROM VCltParameterClient v where v.clinetId = :clinetId and v.active = :active";
    public static final String findByIdAndClinetId = "SELECT v FROM VCltParameterClient v where v.id = :id and v.clinetId = :clinetId";

    /*
     @Basic(optional = false)
     @NotNull
     @Id
     @Column(name = "ID")
     private Long id;
     */
    @Id
    @Column(name = "CODE")
    private String code;
    /*
     @Column(name = "first_name")
     private String firstName;

     @Column(name = "last_name")
     private String lastName;
     */
    @Column(name = "paramater_name")
    private String paramaterName;

    @Column(name = "parameter_type_name")
    private String parameterTypeName;

    //@Column(name = "DEFAULT_VALUE")
    //private String defaultValue;
    @Column(name = "value")
    private String value;

    @Column(name = "CLINET_ID")
    private Long clinetId;

    @Column(name = "PARAMETER_ID")
    private Long parameterId;

    @Column(name = "active")
    private String active;

    //@Column(name = "can_be_delete")
    //private String canBeDelete;
    public VCltParameterClient() {
    }
    /*
     public Long getId() {
     return id;
     }

     public void setId(Long id) {
     this.id = id;
     }

     public String getFirstName() {
     return firstName;
     }

     public void setFirstName(String firstName) {
     this.firstName = firstName;
     }

     public String getLastName() {
     return lastName;
     }

     public void setLastName(String lastName) {
     this.lastName = lastName;
     }
     */

    public String getParamaterName() {
        return paramaterName;
    }

    public void setParamaterName(String paramaterName) {
        this.paramaterName = paramaterName;
    }

    public String getParameterTypeName() {
        return parameterTypeName;
    }

    public void setParameterTypeName(String parameterTypeName) {
        this.parameterTypeName = parameterTypeName;
    }
    /*
     public String getDefaultValue() {
     return defaultValue;
     }

     public void setDefaultValue(String defaultValue) {
     this.defaultValue = defaultValue;
     }
     */

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getClinetId() {
        return clinetId;
    }

    public void setClinetId(Long clinetId) {
        this.clinetId = clinetId;
    }

    public Long getParameterId() {
        return parameterId;
    }

    public void setParameterId(Long parameterId) {
        this.parameterId = parameterId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
    /*
     public String getCanBeDelete() {
     return canBeDelete;
     }

     public void setCanBeDelete(String canBeDelete) {
     this.canBeDelete = canBeDelete;
     }
     */

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}
