/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "v_pm_page_parameter_module")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VPmPageParameterModule.findAll", query = "SELECT v FROM VPmPageParameterModule v"),
    @NamedQuery(name = "VPmPageParameterModule.findByCode", query = "SELECT v FROM VPmPageParameterModule v WHERE v.code = :code")})
public class VPmPageParameterModule implements Serializable {

    private static final Long serialVersionUID = 1L;
    public static String findAll = "SELECT v FROM VPmPageParameterModule v";
    public static String findByCode = "SELECT v FROM VPmPageParameterModule v WHERE v.code = :code";
    public static String findByPageIdAndcltModuleId = "SELECT v FROM VPmPageParameterModule v WHERE v.pageId = :pageId and v.cltModuleId = :cltModuleId";
    @Id

    @Column(name = "CODE")
    private String code;

    @Column(name = "VALUE")
    private String value;

    @Basic(optional = false)
    @NotNull
    @Column(name = "pm_page_parameter_module_id")
    private Long pmPageParameterModuleId;
    @Column(name = "PAGE_PARAMETER_ID")
    private Long pageParameterId;
    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;
    @Column(name = "ACTIVE")
    private String active;
    @Size(max = 255)
    @Column(name = "name_page_parameter")
    private String namePageParameter;
    @Lob
    @Size(max = 65535)
    @Column(name = "page_parameter_description")
    private String pageParameterDescription;
    @Lob
    @Size(max = 65535)
    @Column(name = "page_parameter_DEFAULT_VALUE")
    private String pageparameterDEFAULTVALUE;
    @Column(name = "PAGE_ID")
    private Long pageId;
    @Column(name = "PAGE_PARAMETER_TYPE_ID")
    private Long pageParameterTypeId;
    @Size(max = 255)
    @Column(name = "name_page_parameter_type")
    private String namePageParameterType;
    /*
     @Column(name = "CLIENT_ID")
     private Long clientId;
     @Basic(optional = false)
     @NotNull
     @Size(min = 1, max = 5)
     @Column(name = "can_be_delete")
     private String canBeDelete;
    
     */

    public VPmPageParameterModule() {
    }

    public static String getFindAll() {
        return findAll;
    }

    public static void setFindAll(String findAll) {
        VPmPageParameterModule.findAll = findAll;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getPmPageParameterModuleId() {
        return pmPageParameterModuleId;
    }

    public void setPmPageParameterModuleId(Long pmPageParameterModuleId) {
        this.pmPageParameterModuleId = pmPageParameterModuleId;
    }

    public Long getPageParameterId() {
        return pageParameterId;
    }

    public void setPageParameterId(Long pageParameterId) {
        this.pageParameterId = pageParameterId;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getNamePageParameter() {
        return namePageParameter;
    }

    public void setNamePageParameter(String namePageParameter) {
        this.namePageParameter = namePageParameter;
    }

    public String getPageParameterDescription() {
        return pageParameterDescription;
    }

    public void setPageParameterDescription(String pageParameterDescription) {
        this.pageParameterDescription = pageParameterDescription;
    }

    public String getPageparameterDEFAULTVALUE() {
        return pageparameterDEFAULTVALUE;
    }

    public void setPageparameterDEFAULTVALUE(String pageparameterDEFAULTVALUE) {
        this.pageparameterDEFAULTVALUE = pageparameterDEFAULTVALUE;
    }

    public Long getPageId() {
        return pageId;
    }

    public void setPageId(Long pageId) {
        this.pageId = pageId;
    }

    public Long getPageParameterTypeId() {
        return pageParameterTypeId;
    }

    public void setPageParameterTypeId(Long pageParameterTypeId) {
        this.pageParameterTypeId = pageParameterTypeId;
    }

    public String getNamePageParameterType() {
        return namePageParameterType;
    }

    public void setNamePageParameterType(String namePageParameterType) {
        this.namePageParameterType = namePageParameterType;
    }
    /*
    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getCanBeDelete() {
        return canBeDelete;
    }

    public void setCanBeDelete(String canBeDelete) {
        this.canBeDelete = canBeDelete;
    }
    */
}
