/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "v_pm_pdf")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VPmPdf.findAll", query = "SELECT v FROM VPmPdf v"),
    @NamedQuery(name = "VPmPdf.findById", query = "SELECT v FROM VPmPdf v WHERE v.id = :id"),
    @NamedQuery(name = "VPmPdf.findByName", query = "SELECT v FROM VPmPdf v WHERE v.name = :name"),
    @NamedQuery(name = "VPmPdf.findBySortKey", query = "SELECT v FROM VPmPdf v WHERE v.sortKey = :sortKey"),
    @NamedQuery(name = "VPmPdf.findByPdfTypeId", query = "SELECT v FROM VPmPdf v WHERE v.pdfTypeId = :pdfTypeId"),
    @NamedQuery(name = "VPmPdf.findByInDev", query = "SELECT v FROM VPmPdf v WHERE v.inDev = :inDev"),
    @NamedQuery(name = "VPmPdf.findByActive", query = "SELECT v FROM VPmPdf v WHERE v.active = :active"),
    @NamedQuery(name = "VPmPdf.findByUserCreation", query = "SELECT v FROM VPmPdf v WHERE v.userCreation = :userCreation"),
    @NamedQuery(name = "VPmPdf.findByDateCreation", query = "SELECT v FROM VPmPdf v WHERE v.dateCreation = :dateCreation"),
    @NamedQuery(name = "VPmPdf.findByUserUpdate", query = "SELECT v FROM VPmPdf v WHERE v.userUpdate = :userUpdate"),
    @NamedQuery(name = "VPmPdf.findByDateUpdate", query = "SELECT v FROM VPmPdf v WHERE v.dateUpdate = :dateUpdate"),
    @NamedQuery(name = "VPmPdf.findByPdfTypeName", query = "SELECT v FROM VPmPdf v WHERE v.pdfTypeName = :pdfTypeName")})
public class VPmPdf implements Serializable {

    public static final String findById = "SELECT v FROM VPmPdf v WHERE v.id = :id";
    private static final Long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;
    @Size(max = 255)
    @Column(name = "NAME")
    private String name;
    @Lob
    @Size(max = 65535)
    @Column(name = "DESCRIPTION")
    private String description;
    @Column(name = "SORT_KEY")
    private Long sortKey;
    @Column(name = "PDF_TYPE_ID")
    private Long pdfTypeId;
    @Column(name = "IN_DEV")
    private String inDev;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Size(max = 255)
    @Column(name = "pdf_type_name")
    private String pdfTypeName;

    public VPmPdf() {
    }

    public String getClasse() {

        // ma.mystock.web.beans.pages.p001.P001
        String newClasse;
        if (id < 10) {
            newClasse = "ma.mystock.web.beans.pdf.pdf00" + id + ".Pdf00" + id;
        } else if (id < 100) {
            newClasse = "ma.mystock.web.beans.pdf.pdf0" + id + ".Pdf0" + id;
        } else {
            newClasse = "ma.mystock.web.beans.pdf.pdf" + id + ".Pdf" + id;
        }
        System.out.println(" > newClasse : " + newClasse);
        return newClasse;

    }

    public String getJsp() {
        String newJsp;

        if ("Y".equalsIgnoreCase(inDev)) {
            newJsp = "encours/encours.xhtml";
        } else {
            if (id < 10) {
                newJsp = "pdf00" + id + "/pdf00" + id + ".xhtml";
            } else if (id < 100) {
                newJsp = "pdf0" + id + "/pdf0" + id + ".xhtml";
            } else {
                newJsp = "pdf" + id + "/pdf" + id + ".xhtml";
            }

        }

        System.out.println(" > newJsp : " + newJsp);
        return newJsp;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSortKey() {
        return sortKey;
    }

    public void setSortKey(Long sortKey) {
        this.sortKey = sortKey;
    }

    public Long getPdfTypeId() {
        return pdfTypeId;
    }

    public void setPdfTypeId(Long pdfTypeId) {
        this.pdfTypeId = pdfTypeId;
    }

    public String getInDev() {
        return inDev;
    }

    public void setInDev(String inDev) {
        this.inDev = inDev;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public String getPdfTypeName() {
        return pdfTypeName;
    }

    public void setPdfTypeName(String pdfTypeName) {
        this.pdfTypeName = pdfTypeName;
    }

}
