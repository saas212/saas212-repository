/*
-- Query: SELECT * FROM mystock.inf_item where code like '%p004%'
LIMIT 0, 500

-- Date: 2016-04-20 23:38
*/
INSERT INTO `inf_item` (`CODE`,`ACTIVE`,`DATE_CREATION`,`USER_CREATION`,`DATE_UPDATE`,`USER_UPDATE`) VALUES ('p004','Y',NULL,NULL,NULL,NULL);
INSERT INTO `inf_item` (`CODE`,`ACTIVE`,`DATE_CREATION`,`USER_CREATION`,`DATE_UPDATE`,`USER_UPDATE`) VALUES ('p004.s1','Y',NULL,NULL,NULL,NULL);
INSERT INTO `inf_item` (`CODE`,`ACTIVE`,`DATE_CREATION`,`USER_CREATION`,`DATE_UPDATE`,`USER_UPDATE`) VALUES ('p004.s1.active','Y',NULL,NULL,NULL,NULL);
INSERT INTO `inf_item` (`CODE`,`ACTIVE`,`DATE_CREATION`,`USER_CREATION`,`DATE_UPDATE`,`USER_UPDATE`) VALUES ('p004.s1.companyName','Y',NULL,NULL,NULL,NULL);
INSERT INTO `inf_item` (`CODE`,`ACTIVE`,`DATE_CREATION`,`USER_CREATION`,`DATE_UPDATE`,`USER_UPDATE`) VALUES ('p004.s1.email','Y',NULL,NULL,NULL,NULL);
INSERT INTO `inf_item` (`CODE`,`ACTIVE`,`DATE_CREATION`,`USER_CREATION`,`DATE_UPDATE`,`USER_UPDATE`) VALUES ('p004.s1.name','Y',NULL,NULL,NULL,NULL);
INSERT INTO `inf_item` (`CODE`,`ACTIVE`,`DATE_CREATION`,`USER_CREATION`,`DATE_UPDATE`,`USER_UPDATE`) VALUES ('p004.s1.phone','Y',NULL,NULL,NULL,NULL);
INSERT INTO `inf_item` (`CODE`,`ACTIVE`,`DATE_CREATION`,`USER_CREATION`,`DATE_UPDATE`,`USER_UPDATE`) VALUES ('p004.s1.type','Y',NULL,NULL,NULL,NULL);
INSERT INTO `inf_item` (`CODE`,`ACTIVE`,`DATE_CREATION`,`USER_CREATION`,`DATE_UPDATE`,`USER_UPDATE`) VALUES ('p004.s2','Y',NULL,NULL,NULL,NULL);
INSERT INTO `inf_item` (`CODE`,`ACTIVE`,`DATE_CREATION`,`USER_CREATION`,`DATE_UPDATE`,`USER_UPDATE`) VALUES ('p004.s2.active','Y',NULL,NULL,NULL,NULL);
INSERT INTO `inf_item` (`CODE`,`ACTIVE`,`DATE_CREATION`,`USER_CREATION`,`DATE_UPDATE`,`USER_UPDATE`) VALUES ('p004.s2.category','Y',NULL,NULL,NULL,NULL);
INSERT INTO `inf_item` (`CODE`,`ACTIVE`,`DATE_CREATION`,`USER_CREATION`,`DATE_UPDATE`,`USER_UPDATE`) VALUES ('p004.s2.companyName','Y',NULL,NULL,NULL,NULL);
INSERT INTO `inf_item` (`CODE`,`ACTIVE`,`DATE_CREATION`,`USER_CREATION`,`DATE_UPDATE`,`USER_UPDATE`) VALUES ('p004.s2.firstName','Y',NULL,NULL,NULL,NULL);
INSERT INTO `inf_item` (`CODE`,`ACTIVE`,`DATE_CREATION`,`USER_CREATION`,`DATE_UPDATE`,`USER_UPDATE`) VALUES ('p004.s2.fullLabel','Y',NULL,NULL,NULL,NULL);
INSERT INTO `inf_item` (`CODE`,`ACTIVE`,`DATE_CREATION`,`USER_CREATION`,`DATE_UPDATE`,`USER_UPDATE`) VALUES ('p004.s2.lastName','Y',NULL,NULL,NULL,NULL);
INSERT INTO `inf_item` (`CODE`,`ACTIVE`,`DATE_CREATION`,`USER_CREATION`,`DATE_UPDATE`,`USER_UPDATE`) VALUES ('p004.s2.nature','Y',NULL,NULL,NULL,NULL);
INSERT INTO `inf_item` (`CODE`,`ACTIVE`,`DATE_CREATION`,`USER_CREATION`,`DATE_UPDATE`,`USER_UPDATE`) VALUES ('p004.s2.note','Y',NULL,NULL,NULL,NULL);
INSERT INTO `inf_item` (`CODE`,`ACTIVE`,`DATE_CREATION`,`USER_CREATION`,`DATE_UPDATE`,`USER_UPDATE`) VALUES ('p004.s2.representative','Y',NULL,NULL,NULL,NULL);
INSERT INTO `inf_item` (`CODE`,`ACTIVE`,`DATE_CREATION`,`USER_CREATION`,`DATE_UPDATE`,`USER_UPDATE`) VALUES ('p004.s2.shortLabel','Y',NULL,NULL,NULL,NULL);
INSERT INTO `inf_item` (`CODE`,`ACTIVE`,`DATE_CREATION`,`USER_CREATION`,`DATE_UPDATE`,`USER_UPDATE`) VALUES ('p004.s2.type','Y',NULL,NULL,NULL,NULL);
