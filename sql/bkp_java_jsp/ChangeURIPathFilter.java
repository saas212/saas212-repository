package ma.mystock.web.filters;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.annotation.WebFilter;

/**
 *
 * @author Abdessamad HALLAL
 */
@WebFilter(urlPatterns = {"*.xhtml"})
public class ChangeURIPathFilter implements Filter {

    //private final Logger log = LoggerFactory.getLogger(ChangeURIPathFilter.class);
    //RequestWrapper modifiedRequest = null;
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // Nothing to initialize
        System.out.println(" init ChangeURIPathFilter ");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        //String contextPath = ((HttpServletRequest) request).getContextPath();

        //HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletRequest httpServleRequest = (HttpServletRequest) request;

        String requestURI = httpServleRequest.getRequestURI();
        
        //HttpServletResponse httpServletResponse = (HttpServletResponse) response;

        System.out.println(" doFilter ChangeURIPathFilter : " + requestURI);

        if (requestURI.indexOf("test") >= 0 /* && httpServletRequest.getSession().getAttribute("STRUCTURE_SET_IN_SESSION") == null*/) {
            // httpServletRequest.getSession().setAttribute("STRUCTURE_SET_IN_SESSION", true);
            request.getRequestDispatcher("/").forward(request, response);
            System.out.println("first");
        } else if (requestURI.equals("/") /*&& httpServletRequest.getSession().getAttribute("STRUCTURE_SET_IN_SESSION") == null*/) {
            request.getRequestDispatcher("/404.html").forward(request, response);
            System.out.println("third");
        } else {
            //httpServletRequest.getSession().setAttribute("STRUCTURE_SET_IN_SESSION", null);
            chain.doFilter(request, response);
            System.out.println("fourth");
        }

    }

    @Override
    public void destroy() {
        System.out.println(" destroy ChangeURIPathFilter ");
    }
}
