/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdessamad HALLAL
 */
@Entity
@Table(name = "v_pm_attribute_max_word")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VPmAttributeMaxWord.findAll", query = "SELECT v FROM VPmAttributeMaxWord v"),
    @NamedQuery(name = "VPmAttributeMaxWord.findByCode", query = "SELECT v FROM VPmAttributeMaxWord v WHERE v.code = :code"),
    @NamedQuery(name = "VPmAttributeMaxWord.findByInfItemCode", query = "SELECT v FROM VPmAttributeMaxWord v WHERE v.infItemCode = :infItemCode"),
    @NamedQuery(name = "VPmAttributeMaxWord.findByPageId", query = "SELECT v FROM VPmAttributeMaxWord v WHERE v.pageId = :pageId"),
    @NamedQuery(name = "VPmAttributeMaxWord.findByCltModuleId", query = "SELECT v FROM VPmAttributeMaxWord v WHERE v.cltModuleId = :cltModuleId"),
    @NamedQuery(name = "VPmAttributeMaxWord.findByValidationId", query = "SELECT v FROM VPmAttributeMaxWord v WHERE v.validationId = :validationId"),
    @NamedQuery(name = "VPmAttributeMaxWord.findByParams", query = "SELECT v FROM VPmAttributeMaxWord v WHERE v.params = :params"),
    @NamedQuery(name = "VPmAttributeMaxWord.findByParamNumber", query = "SELECT v FROM VPmAttributeMaxWord v WHERE v.paramNumber = :paramNumber"),
    @NamedQuery(name = "VPmAttributeMaxWord.findByErrorMessage", query = "SELECT v FROM VPmAttributeMaxWord v WHERE v.errorMessage = :errorMessage")})
public class VPmAttributeMaxWord implements Serializable {
    
    private static final Long serialVersionUID = 1L;
    public static final String findAll = "SELECT v FROM VPmAttributeMaxWord v";
    
    @Id
    @Column(name = "code")
    private String code;

    @Column(name = "inf_item_code")
    private String infItemCode;
    
    @Column(name = "page_id")
    private Long pageId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "clt_module_id")
    private int cltModuleId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "validation_id")
    private Long validationId;
    
    @Column(name = "params")
    private Long params;
    
    @Column(name = "param_number")
    private Long paramNumber;
    
    @Lob
    @Column(name = "help")
    private String help;
    
    @Lob
    @Column(name = "custom_error")
    private byte[] customError;
    
    @Column(name = "error_message")
    private String errorMessage;

    public VPmAttributeMaxWord() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getInfItemCode() {
        return infItemCode;
    }

    public void setInfItemCode(String infItemCode) {
        this.infItemCode = infItemCode;
    }

    public Long getPageId() {
        return pageId;
    }

    public void setPageId(Long pageId) {
        this.pageId = pageId;
    }

    public int getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(int cltModuleId) {
        this.cltModuleId = cltModuleId;
    }

    public Long getValidationId() {
        return validationId;
    }

    public void setValidationId(Long validationId) {
        this.validationId = validationId;
    }

    public Long getParams() {
        return params;
    }

    public void setParams(Long params) {
        this.params = params;
    }

    public Long getParamNumber() {
        return paramNumber;
    }

    public void setParamNumber(Long paramNumber) {
        this.paramNumber = paramNumber;
    }

    public String getHelp() {
        return help;
    }

    public void setHelp(String help) {
        this.help = help;
    }

    public byte[] getCustomError() {
        return customError;
    }

    public void setCustomError(byte[] customError) {
        this.customError = customError;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    
}
