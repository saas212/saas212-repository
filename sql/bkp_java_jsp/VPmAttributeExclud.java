/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
  
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author abdou
 */
@Entity
@Table(name = "v_pm_attribute_exclud")
@XmlRootElement
public class VPmAttributeExclud implements Serializable {
    private static final Long serialVersionUID = 1L;
    public static String findAll = "SELECT v FROM VPmAttributeExclud v";
    @Size(max = 276)
    @Id
    @Column(name = "CODE")
    private String code;
    @Column(name = "PAGE_ID")
    private Long pageId;
    @Size(max = 255)
    @Column(name = "INF_ITEM_CODE")
    private String infItemCode;
    @Column(name = "CLT_MODULE_ID")
    private Long cltModuleId;

    public VPmAttributeExclud() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getPageId() {
        return pageId;
    }

    public void setPageId(Long pageId) {
        this.pageId = pageId;
    }

    public String getInfItemCode() {
        return infItemCode;
    }

    public void setInfItemCode(String infItemCode) {
        this.infItemCode = infItemCode;
    }

    public Long getCltModuleId() {
        return cltModuleId;
    }

    public void setCltModuleId(Long cltModuleId) {
        this.cltModuleId = cltModuleId;
    }
    
}
