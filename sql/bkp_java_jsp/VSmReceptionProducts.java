/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ma.mystock.core.dao.entities.views;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author anasshajami
 */
@Entity
@Table(name = "V_SM_RECEPTION_PRODUCTS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VSmReceptionProducts.findAll", query = "SELECT v FROM VSmReceptionProducts v"),
    @NamedQuery(name = "VSmReceptionProducts.findById", query = "SELECT v FROM VSmReceptionProducts v WHERE v.id = :id"),
    @NamedQuery(name = "VSmReceptionProducts.findByProductId", query = "SELECT v FROM VSmReceptionProducts v WHERE v.productId = :productId"),
    @NamedQuery(name = "VSmReceptionProducts.findByDesignation", query = "SELECT v FROM VSmReceptionProducts v WHERE v.designation = :designation"),
    @NamedQuery(name = "VSmReceptionProducts.findByUnitPriceBuy", query = "SELECT v FROM VSmReceptionProducts v WHERE v.unitPriceBuy = :unitPriceBuy"),
    @NamedQuery(name = "VSmReceptionProducts.findByRemise", query = "SELECT v FROM VSmReceptionProducts v WHERE v.remise = :remise"),
    @NamedQuery(name = "VSmReceptionProducts.findByQuantity", query = "SELECT v FROM VSmReceptionProducts v WHERE v.quantity = :quantity"),
    @NamedQuery(name = "VSmReceptionProducts.findByTva", query = "SELECT v FROM VSmReceptionProducts v WHERE v.tva = :tva"),
    @NamedQuery(name = "VSmReceptionProducts.findByActive", query = "SELECT v FROM VSmReceptionProducts v WHERE v.active = :active"),
    @NamedQuery(name = "VSmReceptionProducts.findByReceptionId", query = "SELECT v FROM VSmReceptionProducts v WHERE v.receptionId = :receptionId"),
    @NamedQuery(name = "VSmReceptionProducts.findByDateCreation", query = "SELECT v FROM VSmReceptionProducts v WHERE v.dateCreation = :dateCreation"),
    @NamedQuery(name = "VSmReceptionProducts.findByUserCreation", query = "SELECT v FROM VSmReceptionProducts v WHERE v.userCreation = :userCreation"),
    @NamedQuery(name = "VSmReceptionProducts.findByDateUpdate", query = "SELECT v FROM VSmReceptionProducts v WHERE v.dateUpdate = :dateUpdate"),
    @NamedQuery(name = "VSmReceptionProducts.findByUserUpdate", query = "SELECT v FROM VSmReceptionProducts v WHERE v.userUpdate = :userUpdate"),
    @NamedQuery(name = "VSmReceptionProducts.findByTotalHt", query = "SELECT v FROM VSmReceptionProducts v WHERE v.totalHt = :totalHt"),
    @NamedQuery(name = "VSmReceptionProducts.findByTotalTtc", query = "SELECT v FROM VSmReceptionProducts v WHERE v.totalTtc = :totalTtc")})
public class VSmReceptionProducts implements Serializable {

    private static final Long serialVersionUID = 1L;

    public static final String findByReceptionIdAndActive = "SELECT v FROM VSmReceptionProducts v WHERE v.receptionId = :receptionId and v.active = :active";
    public static final String findById = "SELECT v FROM VSmReceptionProducts v WHERE v.id = :id";

    @Basic(optional = false)
    @NotNull
    @Id
    @Column(name = "ID")
    private Long id;
    @Column(name = "PRODUCT_ID")
    private Long productId;
    @Size(max = 255)
    @Column(name = "DESIGNATION")
    private String designation;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "UNIT_PRICE_BUY")
    private Double unitPriceBuy;
    @Column(name = "REMISE")
    private Double remise;
    @Column(name = "QUANTITY")
    private Long quantity;
    @Column(name = "TVA")
    private Double tva;
    @Column(name = "ACTIVE")
    private String active;
    @Column(name = "RECEPTION_ID")
    private Long receptionId;
    @Column(name = "DATE_CREATION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;
    @Column(name = "USER_CREATION")
    private Long userCreation;
    @Column(name = "DATE_UPDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateUpdate;
    @Column(name = "USER_UPDATE")
    private Long userUpdate;

    @Column(name = "TOTAL_HT")
    private Double totalHt;
    @Column(name = "TOTAL_TTC")
    private Double totalTtc;

    @Column(name = "REFERENCE")
    private String reference;

    public VSmReceptionProducts() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Double getUnitPriceBuy() {
        return unitPriceBuy;
    }

    public void setUnitPriceBuy(Double unitPriceBuy) {
        this.unitPriceBuy = unitPriceBuy;
    }

    public Double getRemise() {
        return remise;
    }

    public void setRemise(Double remise) {
        this.remise = remise;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Double getTva() {
        return tva;
    }

    public void setTva(Double tva) {
        this.tva = tva;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public Long getReceptionId() {
        return receptionId;
    }

    public void setReceptionId(Long receptionId) {
        this.receptionId = receptionId;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getUserCreation() {
        return userCreation;
    }

    public void setUserCreation(Long userCreation) {
        this.userCreation = userCreation;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Date dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

    public Long getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(Long userUpdate) {
        this.userUpdate = userUpdate;
    }

    public Double getTotalHt() {
        return totalHt;
    }

    public void setTotalHt(Double totalHt) {
        this.totalHt = totalHt;
    }

    public Double getTotalTtc() {
        return totalTtc;
    }

    public void setTotalTtc(Double totalTtc) {
        this.totalTtc = totalTtc;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

}
