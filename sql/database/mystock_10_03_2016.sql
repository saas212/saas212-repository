
-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Client :  localhost:3306
-- Généré le :  Mer 09 Mars 2016 à 00:31
-- Version du serveur :  5.5.38
-- Version de PHP :  5.3.29

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `mystock`
--

DELIMITER $$
--
-- Procédures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_SM_VALIDATE_ORDER`(OUT RESULT_STATUS INT, IN ORDER_ID BIGINT(20))
BEGIN
	
    -- MOIN DU STOCK

    UPDATE SM_PRODUCT  SM_P
	INNER JOIN SM_ORDER_LINE SM_OL
	ON SM_P.ID = SM_OL.PRODUCT_ID
	SET SM_P.QUANTITY = SM_P.QUANTITY - SM_OL.QUANTITY
	WHERE SM_OL.ORDER_ID =  ORDER_ID;

	-- CHANGE STATUTS
    UPDATE SM_ORDER SET ORDER_STATUS_ID = 3 WHERE ID = ORDER_ID;
    
    -- RETURN OK
    SET RESULT_STATUS = 1;
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_SM_VALIDATE_ORDER_SUPPLIER`(OUT RESULT_STATUS INT, IN ORDER_SUPPLIER_ID BIGINT(20))
BEGIN
	
    -- AUG
    /*
    UPDATE SM_PRODUCT  SM_P
	INNER JOIN SM_ORDER_SUPPLIER_LINE SM_OSL
	ON SM_P.ID = SM_OSL.PRODUCT_ID
	SET SM_P.QUANTITY = SM_P.QUANTITY + SM_OSL.QUANTITY
	WHERE SM_OSL.ORDER_SUPPLIER_ID =  ORDER_SUPPLIER_ID;
	*/
	-- CHANGE STATUTS
    UPDATE SM_ORDER_SUPPLIER SET ORDER_SUPPLIER_STATUS_ID = 3 WHERE ID = ORDER_SUPPLIER_ID;
    
    -- RETURN OK
    SET RESULT_STATUS = 1;
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_SM_VALIDATE_RECEPTION`(OUT RESULT_STATUS INT, IN SM_RECEPTION_ID BIGINT(20))
BEGIN

    -- AUG
    
    UPDATE SM_PRODUCT  SM_P
	INNER JOIN SM_RECEPTION_LINE SM_RL
	ON SM_P.ID = SM_RL.PRODUCT_ID
	SET SM_P.QUANTITY = SM_P.QUANTITY + SM_RL.QUANTITY
	WHERE SM_RL.RECEPTION_ID =  SM_RECEPTION_ID;
	
	-- CHANGE STATUTS
    UPDATE SM_RECEPTION SET RECEPTION_STATUS_ID = 3 WHERE ID = SM_RECEPTION_ID;
    
    -- RETURN OK
    SET RESULT_STATUS = 1;
    
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `clt_client`
--

CREATE TABLE `clt_client` (
`ID` bigint(20) NOT NULL,
  `FIRST_NAME` tinytext,
  `LAST_NAME` tinytext,
  `COMPANY_NAME` tinytext CHARACTER SET latin1,
  `ADRESS` tinytext CHARACTER SET latin1,
  `EMAIL` tinytext CHARACTER SET latin1,
  `CELL_PHONE` tinytext CHARACTER SET latin1,
  `FIXED_PHONE` tinytext CHARACTER SET latin1,
  `CURRENCY` tinytext CHARACTER SET latin1,
  `INF_COUNTRY_ID` bigint(20) DEFAULT NULL,
  `INF_CITY_ID` bigint(20) DEFAULT NULL,
  `CLIENT_STATUS_ID` bigint(20) DEFAULT NULL,
  `DEFAULT_LANGUAGE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_client`
--

INSERT INTO `clt_client` (`ID`, `FIRST_NAME`, `LAST_NAME`, `COMPANY_NAME`, `ADRESS`, `EMAIL`, `CELL_PHONE`, `FIXED_PHONE`, `CURRENCY`, `INF_COUNTRY_ID`, `INF_CITY_ID`, `CLIENT_STATUS_ID`, `DEFAULT_LANGUAGE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Jalal', 'TELEMSANI', 'SAJ ELEGENT', '33 passage sumica casablanca 20080 Centre ville', 'sajelegent@gmail.com', NULL, '+212 5 22 20 66 49', 'DH', 1, 2, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'Abdessamad', 'HALLAL', 'Sybaway', 'Adresse', 'abdessamad.hallal@gmail.com', NULL, NULL, 'DH', NULL, NULL, NULL, NULL, 'Y', '2015-08-06 22:13:07', NULL, NULL, NULL);

--
-- Déclencheurs `clt_client`
--
DELIMITER //
CREATE TRIGGER `TRIG_BI_CLIENT` BEFORE INSERT ON `clt_client`
 FOR EACH ROW BEGIN

    -- VERFIED ACTIVE
    
    IF NEW.ACTIVE IS NULL THEN
    
      SET NEW.ACTIVE = 'Y';
    
    END IF;
    
    IF NEW.ACTIVE IS NOT NULL THEN
      
      IF upper(NEW.ACTIVE) <> 'Y' and upper(NEW.ACTIVE) <> 'N' THEN
      
          SET  NEW.ACTIVE = 'Y';
      
      end if ;
      
    END IF;
    -- DATE CREATION 
    
    SET  NEW.DATE_CREATION = now();

END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `clt_client_language`
--

CREATE TABLE `clt_client_language` (
`ID` bigint(20) NOT NULL,
  `CLIENT_ID` bigint(20) DEFAULT NULL,
  `LANGUAGE_ID` bigint(20) DEFAULT NULL,
  `INF_PREFIX_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_client_language`
--

INSERT INTO `clt_client_language` (`ID`, `CLIENT_ID`, `LANGUAGE_ID`, `INF_PREFIX_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_client_status`
--

CREATE TABLE `clt_client_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_client_status`
--

INSERT INTO `clt_client_status` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Active', NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_module`
--

CREATE TABLE `clt_module` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `CLIENT_ID` bigint(20) DEFAULT NULL,
  `MODULE_STATUS_ID` bigint(20) DEFAULT NULL,
  `MODULE_TYPE_ID` bigint(20) DEFAULT NULL,
  `PARENT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `IMAGE_PATH` tinytext,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_module`
--

INSERT INTO `clt_module` (`ID`, `NAME`, `DESCRIPTION`, `CLIENT_ID`, `MODULE_STATUS_ID`, `MODULE_TYPE_ID`, `PARENT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `IMAGE_PATH`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Gestion de stocks', '', 1, 1, 4, NULL, 1, 'Y', 'packing.png', NULL, NULL, NULL, NULL),
(3, 'Admin de stock', '', 1, 1, 3, 1, 3, 'Y', 'wheel.png', NULL, NULL, NULL, NULL),
(5, 'Configuration', NULL, 1, 1, 2, NULL, 5, 'Y', 'software.png', NULL, NULL, NULL, NULL),
(10, 'webmaster', NULL, 1, 1, 1, NULL, NULL, 'N', 'software.png', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_module_parameter`
--

CREATE TABLE `clt_module_parameter` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext CHARACTER SET latin1,
  `DESCRIPTION` text CHARACTER SET latin1,
  `DEFAULT_VALUE` text,
  `MODULE_PARAMETER_TYPE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_module_parameter`
--

INSERT INTO `clt_module_parameter` (`ID`, `NAME`, `DESCRIPTION`, `DEFAULT_VALUE`, `MODULE_PARAMETER_TYPE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'La valeur pardéfaut de TVA', 'a2', '20', 1, 'Y', NULL, 1, NULL, NULL),
(2, '1', '2', '3', 1, 'Y', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_module_parameter_client`
--

CREATE TABLE `clt_module_parameter_client` (
`ID` bigint(20) NOT NULL,
  `MODULE_PARAMETER_ID` bigint(20) DEFAULT NULL,
  `VALUE` longtext,
  `MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_module_parameter_client`
--

INSERT INTO `clt_module_parameter_client` (`ID`, `MODULE_PARAMETER_ID`, `VALUE`, `MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, '20', 1, 'Y', NULL, 1, NULL, NULL),
(2, 1, 'aaa', 3, 'Y', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_module_parameter_type`
--

CREATE TABLE `clt_module_parameter_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_module_parameter_type`
--

INSERT INTO `clt_module_parameter_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'clt_module_parameter_type', 'type2', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_module_status`
--

CREATE TABLE `clt_module_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_module_status`
--

INSERT INTO `clt_module_status` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'ACTIF', NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Desactivé', NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_module_type`
--

CREATE TABLE `clt_module_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_module_type`
--

INSERT INTO `clt_module_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Webmaster', NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Super admin', NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(3, 'Admin', NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(4, 'Stock', NULL, 4, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_parameter`
--

CREATE TABLE `clt_parameter` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext CHARACTER SET latin1,
  `DEFAULT_VALUE` text,
  `PARAMETER_TYPE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `DESCRIPTION` tinytext CHARACTER SET latin1
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_parameter`
--

INSERT INTO `clt_parameter` (`ID`, `NAME`, `DEFAULT_VALUE`, `PARAMETER_TYPE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `DESCRIPTION`) VALUES
(1, 'Titre de l''application ', 'defaitl val 1', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(2, 'Package et jsp de page en cours', 'defaitl val 2', 1, 'Y', NULL, NULL, NULL, 1, ''),
(3, 'param 3', 'default va 3', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(4, 'nom1', 'nom3', 1, 'Y', NULL, 1, NULL, NULL, 'nom2'),
(5, '1', '3', 1, 'Y', NULL, 1, NULL, NULL, '2');

-- --------------------------------------------------------

--
-- Structure de la table `clt_parameter_client`
--

CREATE TABLE `clt_parameter_client` (
`ID` bigint(20) NOT NULL,
  `VALUE` longtext,
  `CLINET_ID` bigint(20) DEFAULT NULL,
  `PARAMETER_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_parameter_client`
--

INSERT INTO `clt_parameter_client` (`ID`, `VALUE`, `CLINET_ID`, `PARAMETER_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'My Stock Management', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'ma.mystock.web.beans.stock.encours,stock/encours', 1, 2, 'Y', NULL, NULL, NULL, NULL),
(3, '3', 1, 3, 'Y', NULL, 1, NULL, NULL),
(4, 'AA', 1, 4, 'Y', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_parameter_type`
--

CREATE TABLE `clt_parameter_type` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_parameter_type`
--

INSERT INTO `clt_parameter_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'type1', 'desc type 11', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_user`
--

CREATE TABLE `clt_user` (
`ID` bigint(20) NOT NULL,
  `FIRST_NAME` tinytext CHARACTER SET latin1,
  `LAST_NAME` tinytext CHARACTER SET latin1,
  `USERNAME` tinytext CHARACTER SET latin1,
  `PASSWORD` tinytext CHARACTER SET latin1,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `CLIENT_ID` bigint(20) DEFAULT NULL,
  `INF_COUNTRY_ID` bigint(20) DEFAULT NULL,
  `INF_CITY_ID` bigint(20) DEFAULT NULL,
  `USER_STATUS_ID` bigint(20) DEFAULT NULL,
  `DEFAULT_LANGUAGE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `CELL_PHONE` varchar(45) DEFAULT NULL,
  `FIXED_PHONE` varchar(45) DEFAULT NULL,
  `ADRESS` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `IMAGE_PATH` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_user`
--

INSERT INTO `clt_user` (`ID`, `FIRST_NAME`, `LAST_NAME`, `USERNAME`, `PASSWORD`, `CATEGORY_ID`, `CLIENT_ID`, `INF_COUNTRY_ID`, `INF_CITY_ID`, `USER_STATUS_ID`, `DEFAULT_LANGUAGE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `CELL_PHONE`, `FIXED_PHONE`, `ADRESS`, `SORT_KEY`, `IMAGE_PATH`) VALUES
(1, 'WebMaster', 'HALLAL', 'webmaster', '50f3f01caa053693ce619d596e14b0ff3901ab49', 1, 1, 1, 2, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'aaa', 'fff', 'rrrrG', NULL, 'icons/3.png'),
(2, 'Admin', 'HALLAL', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 2, 1, 1, 2, 1, 1, 'Y', NULL, NULL, NULL, NULL, '', '', '', NULL, 'icons/3.png'),
(3, 'Super Admin', 'HALLAL', 'superadmin', '889a3a791b3875cfae413574b53da4bb8a90d53e', 3, 1, 1, 2, 1, 1, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'icons/3.png'),
(4, 'Stock', 'HALLAL', 'stock', 'ed487e1e87c675af89db011b2903f20f99b11c7d', 4, 1, 1, 2, 1, 1, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'icons/3.png');

-- --------------------------------------------------------

--
-- Structure de la table `clt_user_category`
--

CREATE TABLE `clt_user_category` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_user_category`
--

INSERT INTO `clt_user_category` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'WebMaster', NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Super Admin', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Admin', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'Stock', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_user_client`
--

CREATE TABLE `clt_user_client` (
`ID` bigint(20) NOT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  `CLIENT_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_user_client`
--

INSERT INTO `clt_user_client` (`ID`, `USER_ID`, `CLIENT_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 1, 2, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_user_group`
--

CREATE TABLE `clt_user_group` (
`ID` bigint(20) NOT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  `INF_GROUP_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_user_group`
--

INSERT INTO `clt_user_group` (`ID`, `USER_ID`, `INF_GROUP_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(2, 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_user_module`
--

CREATE TABLE `clt_user_module` (
`ID` bigint(20) NOT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  `MODULE_ID` bigint(20) DEFAULT NULL,
  `FULL CONTROL` char(1) CHARACTER SET latin1 DEFAULT 'N',
  `MODULE_ID_TO_MANAGER` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_user_module`
--

INSERT INTO `clt_user_module` (`ID`, `USER_ID`, `MODULE_ID`, `FULL CONTROL`, `MODULE_ID_TO_MANAGER`, `ACTIVE`, `SORT_KEY`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(12, 2, 3, 'N', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(14, 2, 1, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(27, 1, 1, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(29, 1, 3, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(31, 1, 5, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(36, 3, 1, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(37, 3, 3, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(38, 3, 5, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(39, 4, 1, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_user_status`
--

CREATE TABLE `clt_user_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_user_status`
--

INSERT INTO `clt_user_status` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Actif 1', NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `cta_email_type`
--

CREATE TABLE `cta_email_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cta_fax_type`
--

CREATE TABLE `cta_fax_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cta_location_type`
--

CREATE TABLE `cta_location_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cta_party`
--

CREATE TABLE `cta_party` (
`ID` bigint(20) NOT NULL,
  `PARTY_TYPE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cta_party_email`
--

CREATE TABLE `cta_party_email` (
`ID` bigint(20) NOT NULL,
  `EMAIL_ADDRESS` tinytext,
  `IS_FOR_NOTIFICATION` char(1) DEFAULT NULL,
  `IS_FOR_USERNAME` char(1) DEFAULT NULL,
  `CONTACT_NAME` tinytext,
  `PARTY_ID` bigint(20) DEFAULT NULL,
  `EMAIL_TYPE` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cta_party_fax`
--

CREATE TABLE `cta_party_fax` (
`ID` bigint(20) NOT NULL,
  `COUNTRY_CODE` tinytext,
  `LOCAL_NUMBER` tinytext,
  `AREA_CODE` tinytext,
  `CONTACT_NAME` tinytext,
  `PARTY_ID` bigint(20) DEFAULT NULL,
  `FAX_TYPE` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cta_party_location`
--

CREATE TABLE `cta_party_location` (
`ID` bigint(20) NOT NULL,
  `ADDRESS_LINE_1` tinytext,
  `ADDRESS_LINE_2` tinytext,
  `ADDRESS_LINE_3` tinytext,
  `INF_COUNTRY_ID` bigint(20) DEFAULT NULL,
  `INF_CITY_ID` bigint(20) DEFAULT NULL,
  `POSTAL_CODE` tinytext,
  `CONTACT_NAME` tinytext,
  `PARTY_ID` bigint(20) DEFAULT NULL,
  `LOCATION_TYPE` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cta_party_phone`
--

CREATE TABLE `cta_party_phone` (
`ID` bigint(20) NOT NULL,
  `COUNTRY_CODE` tinytext,
  `EXTENSION` tinytext,
  `LOCAL_NUMBER` tinytext,
  `AREA_CODE` tinytext,
  `CONTACT_NAME` tinytext,
  `PARTY_ID` bigint(20) DEFAULT NULL,
  `PHONE_TYPE` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cta_party_type`
--

CREATE TABLE `cta_party_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `cta_party_type`
--

INSERT INTO `cta_party_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Tenant', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'User', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Supplier', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'Client / Customer', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `cta_party_web`
--

CREATE TABLE `cta_party_web` (
`ID` bigint(20) NOT NULL,
  `URL` tinytext,
  `NOTE` tinytext,
  `CONTACT_NAME` tinytext,
  `PARTY_ID` bigint(20) DEFAULT NULL,
  `WEB_TYPE` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cta_phone_type`
--

CREATE TABLE `cta_phone_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `cta_web_type`
--

CREATE TABLE `cta_web_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `inf_basic_parameter`
--

CREATE TABLE `inf_basic_parameter` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext CHARACTER SET latin1,
  `DESCRIPTION` text CHARACTER SET latin1,
  `VALUE` text,
  `BASIC_PARAMETER_TYPE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_basic_parameter`
--

INSERT INTO `inf_basic_parameter` (`ID`, `NAME`, `DESCRIPTION`, `VALUE`, `BASIC_PARAMETER_TYPE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'URL de l''application', 'url de l''application utilisé pour récupérer les images et styles et les fichiers javascript', 'http://localhost:8083/MyStock', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Variable de css, js et img', 'ce variable pour reladoer les fichiers pour chaque requûets HTTP', 'Y', 1, 'Y', NULL, NULL, NULL, NULL),
(3, 'default prefixe', 'le préfixe qui sera pardéfaut', 'default', 1, 'Y', NULL, NULL, NULL, NULL),
(4, 'langue par défaut', 'le code du langue', 'fr', 1, 'Y', NULL, NULL, NULL, NULL),
(5, 'copyright', 'le nom de la société', 'My Systems', 1, 'Y', NULL, NULL, NULL, NULL),
(6, 'Authentification', 'le titre de page d''authentification', 'My Stock Management', 1, 'Y', NULL, NULL, NULL, 1),
(7, 'Le nom de l''application', NULL, 'My Stock Management', 1, 'Y', NULL, NULL, NULL, NULL),
(8, 'Les droits', 'les droits qui s''affichent dans le footer ', 'Copyright © 2015 ', 1, 'Y', NULL, NULL, NULL, NULL),
(9, 'Réfresh tous les composants', 'Réfresh tous les composants', 'http://localhost:8083/MyStock/servlet/refreshContextListener?username=user&password=pass', 1, 'Y', NULL, NULL, NULL, NULL),
(10, 'param 101', 'desc pour param 101', '101', 2, 'Y', NULL, 1, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `inf_basic_parameter_type`
--

CREATE TABLE `inf_basic_parameter_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_basic_parameter_type`
--

INSERT INTO `inf_basic_parameter_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'basic config', 'basic config', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'type 1', 'esc type 1', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_city`
--

CREATE TABLE `inf_city` (
`ID` bigint(20) NOT NULL,
  `CODE` tinytext CHARACTER SET latin1,
  `NAME` tinytext,
  `DESCRIPTION` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `COUNTRY_ID` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_city`
--

INSERT INTO `inf_city` (`ID`, `CODE`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `COUNTRY_ID`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, NULL, 'Rabat', NULL, NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, NULL, 'Casablanca', NULL, NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(3, NULL, 'Rabat2', NULL, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(4, NULL, '111city1', NULL, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_country`
--

CREATE TABLE `inf_country` (
`ID` bigint(20) NOT NULL,
  `CODE` tinytext CHARACTER SET latin1,
  `NAME` tinytext,
  `DESCRIPTION` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_country`
--

INSERT INTO `inf_country` (`ID`, `CODE`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `CLT_MODULE_ID`) VALUES
(1, NULL, 'Maroc', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(2, NULL, 'Maroc2', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(3, NULL, 'pay12', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `inf_group`
--

CREATE TABLE `inf_group` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_group`
--

INSERT INTO `inf_group` (`ID`, `NAME`, `DESCRIPTION`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'group 1', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_item`
--

CREATE TABLE `inf_item` (
  `CODE` varchar(255) NOT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` varchar(45) DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_item`
--

INSERT INTO `inf_item` (`CODE`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
('basicParameter', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1.basicParameterType', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1.description', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1.id', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1.name', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1.value', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2.basicParameterType', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2.description', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2.id', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2.name', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2.value', 'Y', NULL, NULL, NULL, NULL),
('blValidation', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.customerName', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.description', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.id', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.sumQuantity', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.sumTotalPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s2', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s2.customer', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s2.description', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.color', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.size', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.color', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.price', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.quantity', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.reference', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.size', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.totalPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s5', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s5.sumQuantity', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s5.sumTotal', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s6', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s1', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s1.customer', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s1.description', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.color', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.quantity', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.size', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.color', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.price', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.size', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.totalPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s4', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s4.sumQuantity', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s4.sumTotal', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s5', 'Y', NULL, NULL, NULL, NULL),
('customer', 'Y', NULL, NULL, NULL, NULL),
('customer.s1', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.active', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.adress', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.category', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.city', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.company', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.country', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.email', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.firstName', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.fixedPhone', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.fullLabel', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.lastName', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.note', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.secondaryAddress', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.shortLabel', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.type', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.webSite', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.zipCode', 'Y', NULL, NULL, NULL, NULL),
('customer.s2', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.adress', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.city', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.country', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.email', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.firstName', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.fixedPhone', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.lastName', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.type', 'Y', NULL, NULL, NULL, NULL),
('globals', 'Y', NULL, NULL, NULL, NULL),
('globals.forms', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.add', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.cancel', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.clean', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.confirmClean', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.confirmDelete', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.delete', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.edit', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.no', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.save', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.validate', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.yes', 'Y', NULL, NULL, NULL, NULL),
('globals.list', 'Y', NULL, NULL, NULL, NULL),
('globals.list.activate', 'Y', NULL, NULL, NULL, NULL),
('globals.list.confirmDelete', 'Y', NULL, NULL, NULL, NULL),
('globals.list.delete', 'Y', NULL, NULL, NULL, NULL),
('globals.list.edit', 'Y', NULL, NULL, NULL, NULL),
('globals.list.option', 'Y', NULL, NULL, NULL, NULL),
('globals.list.validate', 'Y', NULL, NULL, NULL, NULL),
('globals.list.vide', 'Y', NULL, NULL, NULL, NULL),
('inventaire', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.active', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.designation', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.priceBuy', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.priceSale', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.productColorName', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.productSizeName', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.quantity', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.quantityCount', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.receptionValidCount', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.unitPriceBuyMax', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.unitPriceBuyMin', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.unitPriceBuyMoyenne', 'Y', NULL, NULL, NULL, NULL),
('login', 'Y', NULL, NULL, NULL, NULL),
('login.s1', 'Y', NULL, NULL, NULL, NULL),
('login.s1.forgetPassword', 'Y', NULL, NULL, NULL, NULL),
('login.s1.getAccount', 'Y', NULL, NULL, NULL, NULL),
('login.s1.login', 'Y', NULL, NULL, NULL, NULL),
('login.s1.password', 'Y', NULL, NULL, NULL, NULL),
('login.s1.sessionActive', 'Y', NULL, NULL, NULL, NULL),
('login.s1.username', 'Y', NULL, NULL, NULL, NULL),
('module', 'Y', NULL, NULL, NULL, NULL),
('module.s1', 'Y', NULL, NULL, NULL, NULL),
('module.s1.access', 'Y', NULL, NULL, NULL, NULL),
('module.s1.logout', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1.description', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1.id', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1.moduleParameterType', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1.name', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1.value', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2.description', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2.id', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2.moduleParameterType', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2.name', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2.value', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s1', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s1.company', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s1.email', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s1.name', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s2', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s2.descriptionModule', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s2.moduleTypeName', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s2.nameModule', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3.client', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3.defaultValue', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3.parameterModuleName', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3.parameterModuleTypeName', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3.value', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s4', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s4.client', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s4.parameterModule', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s4.parameterModuleDefaultValue', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s4.value', 'Y', NULL, NULL, NULL, NULL),
('p003', 'Y', NULL, NULL, NULL, NULL),
('p003.s1', 'Y', NULL, NULL, NULL, NULL),
('p003.s1.amount', 'Y', NULL, NULL, NULL, NULL),
('p003.s1.description', 'Y', NULL, NULL, NULL, NULL),
('p003.s1.expenseType', 'Y', NULL, NULL, NULL, NULL),
('p003.s1.expenseTypeName', 'Y', NULL, NULL, NULL, NULL),
('p003.s1.name', 'Y', NULL, NULL, NULL, NULL),
('p003.s2', 'Y', NULL, NULL, NULL, NULL),
('p003.s2.amount', 'Y', NULL, NULL, NULL, NULL),
('p003.s2.description', 'Y', NULL, NULL, NULL, NULL),
('p003.s2.name', 'Y', NULL, NULL, NULL, NULL),
('p003.s2.type', 'Y', NULL, NULL, NULL, NULL),
('p005.s1', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.color', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.designation', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.family', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.priceBuy', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.pricesale', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.quantity', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.size', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.status', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.title', 'Y', NULL, NULL, NULL, NULL),
('p005.s2', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.active', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.color', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.designation', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.family', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.group', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.priceBuy', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.pricesale', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.quantity', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.size', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.status', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.threshold', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.type', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.code', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.deadline', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.deposit', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.souche', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p032', 'Y', NULL, NULL, NULL, NULL),
('p032.s1', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.addReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.deleteReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.editReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p032.s2', 'Y', NULL, NULL, NULL, NULL),
('p032.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p032.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p032.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p032.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p032.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p032.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p032.s2.validate', 'Y', NULL, NULL, NULL, NULL),
('p032.s3', 'Y', NULL, NULL, NULL, NULL),
('p032.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p032.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p032.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p032.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p032.s3.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p032.s4', 'Y', NULL, NULL, NULL, NULL),
('p032.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p032.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p032.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p032.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p032.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p032.s5', 'Y', NULL, NULL, NULL, NULL),
('p032.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p032.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p032.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p032.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p032.s5.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p033', 'Y', NULL, NULL, NULL, NULL),
('p033.s1', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.addReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.deleteReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.editReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p033.s2', 'Y', NULL, NULL, NULL, NULL),
('p033.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p033.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p033.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p033.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p033.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p033.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p033.s3', 'Y', NULL, NULL, NULL, NULL),
('p033.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p033.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p033.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p033.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p033.s3.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p033.s4', 'Y', NULL, NULL, NULL, NULL),
('p033.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p033.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p033.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p033.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p033.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p033.s5', 'Y', NULL, NULL, NULL, NULL),
('p033.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p033.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p033.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p033.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p033.s5.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p034', 'Y', NULL, NULL, NULL, NULL),
('p034.s1', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.addReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.deleteReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.editReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p034.s2', 'Y', NULL, NULL, NULL, NULL),
('p034.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p034.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p034.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p034.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p034.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p034.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p034.s3', 'Y', NULL, NULL, NULL, NULL),
('p034.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p034.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p034.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p034.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p034.s3.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p034.s4', 'Y', NULL, NULL, NULL, NULL),
('p034.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p034.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p034.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p034.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p034.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p034.s5', 'Y', NULL, NULL, NULL, NULL),
('p034.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p034.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p034.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p034.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p034.s5.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p035', 'Y', NULL, NULL, NULL, NULL),
('p035.s1', 'Y', NULL, NULL, NULL, NULL),
('p035.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p035.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p035.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p035.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p035.s1.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p035.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p035.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p035.s2', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.commande', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.validate', 'Y', NULL, NULL, NULL, NULL),
('p035.s3', 'Y', NULL, NULL, NULL, NULL),
('p035.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p035.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p035.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p035.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p035.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p035.s4', 'Y', NULL, NULL, NULL, NULL),
('p035.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p035.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p035.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p035.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p035.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p035.s5', 'Y', NULL, NULL, NULL, NULL),
('p035.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p035.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p035.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p035.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p035.s5.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p036', 'Y', NULL, NULL, NULL, NULL),
('p036.s1', 'Y', NULL, NULL, NULL, NULL),
('p036.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p036.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p036.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p036.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p036.s1.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p036.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p036.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p036.s2', 'Y', NULL, NULL, NULL, NULL),
('p036.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p036.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p036.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p036.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p036.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p036.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p036.s2.validate', 'Y', NULL, NULL, NULL, NULL),
('p036.s3', 'Y', NULL, NULL, NULL, NULL),
('p036.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p036.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p036.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p036.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p036.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p036.s4', 'Y', NULL, NULL, NULL, NULL),
('p036.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p036.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p036.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p036.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p036.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p036.s5', 'Y', NULL, NULL, NULL, NULL),
('p036.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p036.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p036.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p036.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p036.s5.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p037', 'Y', NULL, NULL, NULL, NULL),
('p037.s1', 'Y', NULL, NULL, NULL, NULL),
('p037.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p037.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p037.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p037.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p037.s1.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p037.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p037.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p037.s2', 'Y', NULL, NULL, NULL, NULL),
('p037.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p037.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p037.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p037.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p037.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p037.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p037.s2.validate', 'Y', NULL, NULL, NULL, NULL),
('p037.s3', 'Y', NULL, NULL, NULL, NULL),
('p037.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p037.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p037.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p037.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p037.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p037.s4', 'Y', NULL, NULL, NULL, NULL),
('p037.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p037.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p037.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p037.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p037.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p037.s5', 'Y', NULL, NULL, NULL, NULL),
('p037.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p037.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p037.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p037.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p037.s5.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p038', 'Y', NULL, NULL, NULL, NULL),
('p038.s1', 'Y', NULL, NULL, NULL, NULL),
('p038.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p038.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p038.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p038.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p038.s1.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p038.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p038.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p038.s2', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.commande', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.validate', 'Y', NULL, NULL, NULL, NULL),
('p038.s3', 'Y', NULL, NULL, NULL, NULL),
('p038.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p038.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p038.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p038.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p038.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p038.s3.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p038.s4', 'Y', NULL, NULL, NULL, NULL),
('p038.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p038.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p038.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p038.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p038.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p038.s5', 'Y', NULL, NULL, NULL, NULL),
('p038.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p038.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p038.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p038.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p038.s5.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p038.s5.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p041', 'Y', NULL, NULL, NULL, NULL),
('p041.s1', 'Y', NULL, NULL, NULL, NULL),
('p041.s1.customerCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p041.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p041.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p041.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p041.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p041.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p041.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p041.s2', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.commande', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.customer', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.validate', 'Y', NULL, NULL, NULL, NULL),
('p041.s3', 'Y', NULL, NULL, NULL, NULL),
('p041.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p041.s3.negotiatePriceSale', 'Y', NULL, NULL, NULL, NULL),
('p041.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p041.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p041.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p041.s4', 'Y', NULL, NULL, NULL, NULL),
('p041.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p041.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p041.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p041.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p041.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p041.s5', 'Y', NULL, NULL, NULL, NULL),
('p041.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p041.s5.negotiatePriceSale', 'Y', NULL, NULL, NULL, NULL),
('p041.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p041.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p041.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('page1.s1.name', 'Y', NULL, NULL, NULL, NULL),
('pageParameter', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.description', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.id', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.name', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.page', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.pageParameterType', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.value', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.description', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.id', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.name', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.page', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.pageParameterType', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.value', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s1', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s1.company', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s1.email', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s1.name', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s2', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s2.descriptionModule', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s2.moduleTypeName', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s2.nameModule', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s3', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s3.client', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s3.pageDescription', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s3.pageName', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s3.pageTypeName', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s4', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s4.client', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s4.pageParameter', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s4.pageParameterDefaultValue', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s4.value', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s5', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s5.client', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s5.pageParameter', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s5.pageParameterDefaultValue', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s5.value', 'Y', NULL, NULL, NULL, NULL),
('parameter', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1.description', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1.id', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1.name', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1.parameterType', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1.value', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2.description', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2.id', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2.name', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2.parameterType', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2.value', 'Y', NULL, NULL, NULL, NULL),
('parameterClient', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s1', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s1.company', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s1.email', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s1.name', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2.client', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2.defaultValue', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2.parameterName', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2.parameterTypeName', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2.value', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s3', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s3.client', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s3.parameter', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s3.parameterDefaultValue', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s3.value', 'Y', NULL, NULL, NULL, NULL),
('pdf001', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s1', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s2', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s2.seq', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s2.totalHt', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s2.TotalQuantity', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s2.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('pdf002', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s1', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s1.status', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s1.supplier', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s1.the', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s2', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s2.designation', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s2.quantity', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s2.totalHt', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s2.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s3.amountTva', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s3.totalPu', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s3.valueTva', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s4', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s4.note', 'Y', NULL, NULL, NULL, NULL),
('pdf003', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s1', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s2', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s2.seq', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s2.totalHt', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s2.TotalQuantity', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s2.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('pdf004', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s1', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s1.status', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s1.supplier', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s1.the', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s2', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s2.designation', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s2.quantity', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s2.totalHt', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s2.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s3.amountTva', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s3.totalPu', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s3.valueTva', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s4', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s4.note', 'Y', NULL, NULL, NULL, NULL),
('pdf005', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s1', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s2', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s2.seq', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s2.totalHt', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s2.TotalQuantity', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s2.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('pdf006', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s1', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s1.status', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s1.supplier', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s1.the', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s2', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s2.designation', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s2.quantity', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s2.totalHt', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s2.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s3.amountTva', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s3.totalPu', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s3.valueTva', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s4', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s4.note', 'Y', NULL, NULL, NULL, NULL),
('product', 'Y', NULL, NULL, NULL, NULL),
('profile.s1', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.adress', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.email', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.firstName', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.fixedPhone', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.lastName', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.password', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.replayRassword', 'Y', NULL, NULL, NULL, NULL),
('reception', 'Y', NULL, NULL, NULL, NULL),
('reception.s1', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.active', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.amount', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.deadline', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.id', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.name', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.souche', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.supplierId', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.valid', 'Y', NULL, NULL, NULL, NULL),
('reception.s2', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.amount', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.deadline', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.deposit', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.description', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.id', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.name', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.sizes', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.souche', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.valid', 'Y', NULL, NULL, NULL, NULL),
('reception.s3', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.id', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.remise', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.tva', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('reception.s4', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.designation', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.id', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.quantity', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.remise', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.totalHt', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.tva', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('reception.s5', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.quantityTotal', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.remiseAvg', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.tvaAvg', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.unitPriceBuyTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.active', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.amount', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.deadline', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.id', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.name', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.souche', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.supplierId', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.amount', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.deadline', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.deposit', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.description', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.id', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.name', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.souche', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.id', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.remise', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.tva', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.quantityTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.remiseAvg', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.tvaAvg', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.unitPriceBuyTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.active', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.amount', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.deadline', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.id', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.name', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.souche', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.supplierId', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.amount', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.deadline', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.deposit', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.description', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.id', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.name', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.souche', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.id', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.remise', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.tva', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.quantityTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.remiseAvg', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.tvaAvg', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.unitPriceBuyTotal', 'Y', NULL, NULL, NULL, NULL),
('rofile.s1.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('stockState', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.productNotValid', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.productValid', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.quantityCount', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.receptionInprogressCount', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.receptionValidCount', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.unitPriceBuyMax', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.unitPriceBuyMin', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.unitPriceBuyMoyenne', 'Y', NULL, NULL, NULL, NULL),
('supplier', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.active', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.adress', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.category', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.city', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.companyName', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.country', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.email', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.firstName', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.fixedPhone', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.fullLabel', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.lastName', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.note', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.secondaryAddress', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.shortLabel', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.type', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.webSite', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.zipCode', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.adress', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.city', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.companyName', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.country', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.email', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.firstName', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.fixedPhone', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.lastName', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.postCode', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.type', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.dataTypeChar', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.dataTypeDateTime', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.dataTypeDouble', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.dataTypeInteger', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeCodePostale', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeDate', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeDateTime', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeEmail', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeFax', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypePhone', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeTime', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.maxlenght', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.maxWord', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.required', 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_language`
--

CREATE TABLE `inf_language` (
`ID` bigint(20) NOT NULL,
  `CODE` tinytext,
  `NAME` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_language`
--

INSERT INTO `inf_language` (`ID`, `CODE`, `NAME`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'fr', 'Français de France', 1, 'Y', '2014-09-30 00:00:00', 1, '2014-09-16 00:00:00', NULL),
(2, 'en', 'Anglais', NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_lovs`
--

CREATE TABLE `inf_lovs` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `TABLE_PREFIX` tinytext,
  `TABLE` tinytext,
  `VIEW` tinytext,
  `ITEM_CODE` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ENTITY` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_lovs`
--

INSERT INTO `inf_lovs` (`ID`, `NAME`, `DESCRIPTION`, `TABLE_PREFIX`, `TABLE`, `VIEW`, `ITEM_CODE`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `CLT_MODULE_ID`, `ENTITY`) VALUES
(1, 'Les types de fournisseurs', NULL, 'sm', 'sm_supplier_type', 'v_sm_supplier_type', 'itemCode', 1, 'Y', NULL, NULL, NULL, NULL, 1, 'SmSupplierType'),
(2, 'La couleurs', NULL, 'sm', 'sm_expense_type', 'v_sm_expense_type', 'itemCode', 2, 'Y', NULL, NULL, NULL, NULL, 1, 'SmProductColor'),
(3, 'Les Pays', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'InfCountry'),
(4, 'Les villes', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'InfCity');

-- --------------------------------------------------------

--
-- Structure de la table `inf_prefix`
--

CREATE TABLE `inf_prefix` (
`ID` bigint(20) NOT NULL,
  `CODE` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_prefix`
--

INSERT INTO `inf_prefix` (`ID`, `CODE`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'default', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_privilege`
--

CREATE TABLE `inf_privilege` (
`ID` bigint(20) NOT NULL,
  `ITEM_CODE` varchar(255) DEFAULT NULL,
  `ROLE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_privilege`
--

INSERT INTO `inf_privilege` (`ID`, `ITEM_CODE`, `ROLE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'login', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'login.s1', 1, 'Y', NULL, NULL, NULL, NULL),
(3, 'login.s1.username', 1, 'Y', NULL, NULL, NULL, NULL),
(4, 'login.s1.password', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_role`
--

CREATE TABLE `inf_role` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_role`
--

INSERT INTO `inf_role` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Role 1', 'Role 1', 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_role_group`
--

CREATE TABLE `inf_role_group` (
`ID` bigint(20) NOT NULL,
  `ROLE_ID` bigint(20) DEFAULT NULL,
  `GROUP_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_role_group`
--

INSERT INTO `inf_role_group` (`ID`, `ROLE_ID`, `GROUP_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_text`
--

CREATE TABLE `inf_text` (
`ID` bigint(20) NOT NULL,
  `PREFIX` bigint(20) DEFAULT NULL,
  `ITEM_CODE` varchar(255) DEFAULT NULL,
  `VALUE` longtext,
  `TEXT_TYPE_ID` bigint(20) DEFAULT NULL,
  `LANGUAGE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=893 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_text`
--

INSERT INTO `inf_text` (`ID`, `PREFIX`, `ITEM_CODE`, `VALUE`, `TEXT_TYPE_ID`, `LANGUAGE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(2, 1, 'login.s1.password', 'Mot de passe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(3, 1, 'login.s1.username', 'E-mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(4, 1, 'login.s1.username', 'username', 6, 1, 'Y', NULL, NULL, NULL, NULL),
(5, 1, 'login.s1.login', 'Se connecter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(10, 1, 'page1.s1.name', 'Résumé Général', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(11, 1, 'validation.v1.dataTypeInteger', '{0} doit être de type integer', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(12, 1, 'validation.v1.dataTypeDouble', '{0} doit être de type double', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(13, 1, 'validation.v1.maxWord', '{0} ne doit pas dépasser {1} mots', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(14, 1, 'validation.v1.formatTypeDate', '{0} doit être de type date', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(15, 1, 'validation.v1.dataTypeChar', '{0} doit être de type caractère', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(16, 1, 'validation.v1.dataTypeDateTime', '{0} doit être de type datetime', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(17, 1, 'validation.v1.formatTypeEmail', '{0} doit être de type email', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(18, 1, 'validation.v1.formatTypePhone', '{0} doit être de type phone', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(19, 1, 'validation.v1.maxlenght', '{0} ne doit pas dépasser {1} char', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(20, 1, 'validation.v1.required', '{0} est obligatoire', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(26, 1, 'p003.s1', 'La liste des dépenses', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(27, 1, 'p003.s1.name', 'Libelle', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(28, 1, 'p003.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(29, 1, 'p003.s1.amount', 'Montant', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(30, 1, 'p003.s1.expenseTypeName', 'Type de charge', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(34, 1, 'p003.s2', 'Ajouter / Modifier un dépense', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(36, 1, 'p003.s2.type', 'Type de charge', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(37, 1, 'p003.s2.name', 'Libelle', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(38, 1, 'p003.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(39, 1, 'p003.s2.amount', 'Montant', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(41, 1, 'supplier.s1', 'Ajouter / Modifier un fournisseur', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(43, 1, 'supplier.s2', 'Liste des fournisseurs', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(45, 1, 'supplier.s2.country', 'Pays', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(46, 1, 'supplier.s2.city', 'Ville', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(47, 1, 'supplier.s2.email', 'E-Mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(49, 1, 'supplier.s2.fixedPhone', 'Télé Fixe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(50, 1, 'supplier.s2.cellPhone', 'Télé Portable', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(51, 1, 'supplier.s2.companyName', 'Nom de Société ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(52, 1, 'p005.s1', 'List Produits :', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(53, 1, 'p005.s1.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(54, 1, 'p005.s1.quantity', 'quantity', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(55, 1, 'p005.s1.reference', 'reference', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(56, 1, 'p005.s1.pricesale', 'pricesale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(57, 1, 'p005.s1.status', 'status', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(58, 1, 'p005.s1.size', 'size', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(59, 1, 'p005.s1.family', 'family', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(60, 1, 'p005.s1.color', 'color', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(61, 1, 'p005.s1.edit', 'edit', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(62, 1, 'p005.s1.delete', 'delete', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(63, 1, 'p005.s1', 'List Produit : ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(64, 1, 'p005.s2.cancel', 'Annuler', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(65, 1, 'p005.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(66, 1, 'p005.s2.color', 'couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(67, 1, 'p005.s2.family', 'Famille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(68, 1, 'p005.s2.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(69, 1, 'p005.s2.status', 'Status', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(70, 1, 'p005.s2.pricesale', 'Prix de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(71, 1, 'p005.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(72, 1, 'p005.s2.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(73, 1, 'p005.s2.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(74, 1, 'p005.s2', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(75, 1, 'supplier.s1.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(76, 1, 'supplier.s1.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(77, 1, 'supplier.s1.country', 'Pays', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(78, 1, 'supplier.s1.city', 'Ville', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(79, 1, 'supplier.s1.companyName', 'Nom de Société ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(80, 1, 'supplier.s1.cellPhone', 'Télé Portable', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(81, 1, 'supplier.s1.fixedPhone', 'Télé Fixe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(82, 1, 'supplier.s1.adress', 'Adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(83, 1, 'supplier.s1.email', 'E-Mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(84, 1, 'supplier.s1.type', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(89, 1, 'supplier.s2.type', 'Type ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(90, 1, 'globals.list.delete', 'Supprimer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(91, 1, 'globals.list.edit', 'Modifier', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(92, 1, 'globals.list.option', 'Option', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(93, 1, 'globals.list.activate', 'Activer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(94, 1, 'globals.list.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(95, 1, 'globals.list.vide', 'la liste est vide.', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(96, 1, 'globals.list.confirmDelete', 'Êtes-vous sûr de vouloir supprimer ?', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(97, 1, 'globals.forms.add', 'Ajouter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(98, 1, 'globals.forms.edit', 'Modifier', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(99, 1, 'globals.forms.cancel', 'Annuler', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(100, 1, 'globals.forms.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(101, 1, 'globals.forms.delete', 'Supprimer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(102, 1, 'globals.forms.confirmDelete', 'Êtes-vous sûr de vouloir supprimer ?', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(103, 1, 'reception.s1', 'La liste des réceptions en cours', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(104, 1, 'reception.s1.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(105, 1, 'reception.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(106, 1, 'reception.s1.supplierId', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(107, 1, 'reception.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(108, 1, 'reception.s1.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(109, 1, 'reception.s1.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(110, 1, 'reception.s1.amount', 'Le montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(111, 1, 'reception.s1.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(112, 1, 'globals.forms.yes', 'Oui', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(113, 1, 'globals.forms.no', 'Non', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(146, 1, 'reception.s2', 'Ajouter une réception ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(147, 1, 'reception.s2.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(148, 1, 'reception.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(149, 1, 'reception.s2.supplier', 'Fournisseur ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(150, 1, 'reception.s2.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(151, 1, 'reception.s2.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(152, 1, 'reception.s2.amount', 'Montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(153, 1, 'reception.s2.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(155, 1, 'reception.s2.deposit', 'Depôt', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(156, 1, 'reception.s2.description', 'Description ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(158, 1, 'supplier.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(159, 1, 'product', 'La gestion des produits ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(160, 1, 'p005.s2.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(161, 1, 'p003.s1.expenseTypeName', 'Type de charge', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(162, 1, 'customer.s2', 'La liste des clients', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(163, 1, 'customer.s2.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(164, 1, 'customer.s1', 'Ajouter / Modifier un client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(165, 1, 'customer.s2.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(166, 1, 'customer.s2.country', 'Pays', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(167, 1, 'customer.s2.city', 'Ville', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(168, 1, 'customer.s2.email', 'E-Mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(169, 1, 'customer.s2.adress', 'Adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(170, 1, 'customer.s2.fixedPhone', 'Télé Fixe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(171, 1, 'customer.s2.cellPhone', 'Télé Portable', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(172, 1, 'customer.s1.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(173, 1, 'customer.s1.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(174, 1, 'customer.s1.country', 'Pays', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(175, 1, 'customer.s1.city', 'Ville', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(176, 1, 'customer.s1.cellPhone', 'Télé Portable', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(177, 1, 'customer.s1.fixedPhone', 'Télé Fixe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(178, 1, 'customer.s1.adress', 'Adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(179, 1, 'customer.s1.email', 'E-Mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(180, 1, 'customer.s1.type', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(186, 1, 'customer.s1.active', 'Active ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(187, 1, 'p005.s1.priceBuy', 'price by', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(188, 1, 'p005.s2.priceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(189, 1, 'supplier.s1.zipCode', 'Code postale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(190, 1, 'supplier.s2.postCode', 'Code postale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(202, 1, 'reception.s3', 'Ajouter des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(203, 1, 'reception.s3.id', 'Code d''article ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(204, 1, 'reception.s3.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(205, 1, 'reception.s3.quantity', 'Qté ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(206, 1, 'reception.s3.unitPriceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(207, 1, 'reception.s3.remise', 'Remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(208, 1, 'reception.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(209, 1, 'reception.s3.tva', 'Tva', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(210, 1, 'reception.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(211, 1, 'globals.forms.confirmClean', 'Etes-vous sur de vouloir vider le formulaire ?', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(212, 1, 'globals.forms.clean', 'vider le fourmulaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(213, 1, 'reception.s4', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(214, 1, 'reception.s4.id', 'Code d''article ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(215, 1, 'reception.s4.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(216, 1, 'reception.s4.quantity', 'Qté ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(217, 1, 'reception.s4.unitPriceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(218, 1, 'reception.s4.remise', 'Remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(219, 1, 'reception.s4.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(220, 1, 'reception.s4.tva', 'Tva', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(221, 1, 'reception.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(222, 1, 'page1.s1.name', 'name msg', 2, 1, 'Y', NULL, NULL, NULL, NULL),
(223, 1, 'page1.s1.name', 'name help', 4, 1, 'Y', NULL, NULL, NULL, NULL),
(224, 1, 'globals.forms.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(225, 1, 'globals.forms.validate', 'Validate', 3, 2, 'Y', NULL, NULL, NULL, NULL),
(226, 1, 'receptionValidation.s1', 'La liste des réceptions qui sont prêt pour la validation', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(227, 1, 'receptionValidation.s1.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(228, 1, 'receptionValidation.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(229, 1, 'receptionValidation.s1.supplierId', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(230, 1, 'receptionValidation.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(231, 1, 'receptionValidation.s1.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(232, 1, 'receptionValidation.s1.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(233, 1, 'receptionValidation.s1.amount', 'Le montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(234, 1, 'receptionValidation.s1.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(235, 1, 'receptionValidation.s2', 'Détails de réception ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(236, 1, 'receptionValidation.s2.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(237, 1, 'receptionValidation.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(238, 1, 'receptionValidation.s2.supplier', 'Fournisseur ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(239, 1, 'receptionValidation.s2.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(240, 1, 'receptionValidation.s2.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(241, 1, 'receptionValidation.s2.amount', 'Montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(242, 1, 'receptionValidation.s2.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(243, 1, 'receptionValidation.s2.deposit', 'Depôt', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(244, 1, 'receptionValidation.s2.description', 'Description ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(245, 1, 'receptionValidation.s3', 'Ajouter des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(246, 1, 'receptionValidation.s3.id', 'Code d''article ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(247, 1, 'receptionValidation.s3.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(248, 1, 'receptionValidation.s3.quantity', 'Qté ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(249, 1, 'receptionValidation.s3.unitPriceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(250, 1, 'receptionValidation.s3.remise', 'Remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(251, 1, 'receptionValidation.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(252, 1, 'receptionValidation.s3.tva', 'Tva', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(253, 1, 'receptionValidation.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(254, 1, 'receptionHistory.s1', 'Hisortique des réceptions', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(255, 1, 'receptionHistory.s1.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(256, 1, 'receptionHistory.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(257, 1, 'receptionHistory.s1.supplierId', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(258, 1, 'receptionHistory.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(259, 1, 'receptionHistory.s1.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(260, 1, 'receptionHistory.s1.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(261, 1, 'receptionHistory.s1.amount', 'Le montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(262, 1, 'receptionHistory.s1.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(263, 1, 'receptionHistory.s2', 'Détails de réception ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(264, 1, 'receptionHistory.s2.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(265, 1, 'receptionHistory.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(266, 1, 'receptionHistory.s2.supplier', 'Fournisseur ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(267, 1, 'receptionHistory.s2.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(268, 1, 'receptionHistory.s2.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(269, 1, 'receptionHistory.s2.amount', 'Montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(270, 1, 'receptionHistory.s2.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(271, 1, 'receptionHistory.s2.deposit', 'Depôt', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(272, 1, 'receptionHistory.s2.description', 'Description ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(273, 1, 'receptionHistory.s3', 'Ajouter des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(274, 1, 'receptionHistory.s3.id', 'Code d''article ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(275, 1, 'receptionHistory.s3.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(276, 1, 'receptionHistory.s3.quantity', 'Qté ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(277, 1, 'receptionHistory.s3.unitPriceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(278, 1, 'receptionHistory.s3.remise', 'Remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(279, 1, 'receptionHistory.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(280, 1, 'receptionHistory.s3.tva', 'Tva', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(281, 1, 'receptionHistory.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(282, 1, 'receptionValidation.s4', 'Résumé', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(283, 1, 'receptionValidation.s4.quantityTotal', 'La quantité totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(284, 1, 'receptionValidation.s4.unitPriceBuyTotal', 'Le prix unitaire totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(285, 1, 'receptionValidation.s4.remiseAvg', 'La moyenne de remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(286, 1, 'receptionValidation.s4.tvaAvg', 'La monyenne de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(287, 1, 'receptionValidation.s4.totalHtTotal', 'hore taxt total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(288, 1, 'receptionValidation.s4.totalTtcTotal', 'TTC total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(289, 1, 'receptionHistory.s4', 'Résumé Général', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(290, 1, 'receptionHistory.s4.quantityTotal', 'La quantité totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(291, 1, 'receptionHistory.s4.unitPriceBuyTotal', 'Le prix unitaire totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(292, 1, 'receptionHistory.s4.remiseAvg', 'La moyenne de remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(293, 1, 'receptionHistory.s4.tvaAvg', 'La monyenne de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(294, 1, 'receptionHistory.s4.totalHtTotal', 'hore taxt total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(295, 1, 'receptionHistory.s4.totalTtcTotal', 'TTC total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(303, 1, 'reception.s5', 'Résumé Général', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(304, 1, 'reception.s5.quantityTotal', 'La quantité totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(305, 1, 'reception.s5.unitPriceBuyTotal', 'Le prix unitaire totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(306, 1, 'reception.s5.remiseAvg', 'La moyenne de remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(307, 1, 'reception.s5.tvaAvg', 'La monyenne de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(308, 1, 'reception.s5.totalHtTotal', 'hore taxt total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(309, 1, 'reception.s5.totalTtcTotal', 'TTC total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(310, 1, 'reception.s4.totalHt', 'Total HT', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(311, 1, 'reception.s4.totalTtc', 'Total TTC', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(312, 1, 'inventaire.s1', 'Inventaire générale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(313, 1, 'inventaire.s1.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(314, 1, 'inventaire.s1.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(315, 1, 'inventaire.s1.quantity', 'Quantité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(316, 1, 'inventaire.s1.priceBuy', 'Prix de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(317, 1, 'inventaire.s1.priceSale', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(318, 1, 'inventaire.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(319, 1, 'inventaire.s1.receptionValidCount', 'Nombre de réception', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(320, 1, 'inventaire.s1.unitPriceBuyMax', 'Max de prix de vent', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(321, 1, 'inventaire.s1.unitPriceBuyMoyenne', 'Moyenne de prix de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(322, 1, 'inventaire.s1.unitPriceBuyMin', 'Min de prix de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(323, 1, 'inventaire.s1.quantityCount', 'Total des quantité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(324, 1, 'stockState', 'Statut de stock', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(325, 1, 'stockState.s1', 'Statut de stock', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(326, 1, 'stockState.s1.receptionValidCount', 'Le nombre des réceptions valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(327, 1, 'stockState.s1.unitPriceBuyMax', 'le max de prix unitaire de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(328, 1, 'stockState.s1.unitPriceBuyMoyenne', 'la moyenne de prix unitaire de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(329, 1, 'stockState.s1.unitPriceBuyMin', 'Le min de prix de vent unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(330, 1, 'stockState.s1.quantityCount', 'Total des quantités', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(331, 1, 'stockState.s1.productValid', 'Le nombre de produit valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(332, 1, 'stockState.s1.productNotValid', 'Le nombre de produit non valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(333, 1, 'stockState.s1.receptionInprogressCount', 'Le nombre des réceptions qui sont en cours', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(334, 1, 'login.s1.forgetPassword', 'Mot de passe oublié.', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(335, 1, 'login.s1.getAccount', 'Obtenir d''un compte', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(336, 1, 'login.s1.sessionActive', 'Garder ma session active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(337, 1, 'login.s1', 'Authentification', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(338, 1, 'module.s1.access', 'Accès au module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(339, 1, 'module.s1.logout', 'Déconnexion', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(340, 1, 'bonLivraison.s1', 'Nouveau bon de livraison', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(341, 1, 'bonLivraison.s1.customer', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(342, 1, 'bonLivraison.s1.description', 'Note', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(343, 1, 'bonLivraison.s2', 'Ajouter un produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(344, 1, 'bonLivraison.s2.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(345, 1, 'bonLivraison.s2.color', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(346, 1, 'bonLivraison.s2.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(347, 1, 'bonLivraison.s2.unitPriceBuy', 'Prix', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(348, 1, 'bonLivraison.s2.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(349, 1, 'bonLivraison.s2.totalTtc', 'Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(350, 1, 'bonLivraison.s3', 'La liste des articles', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(351, 1, 'bonLivraison.s3.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(352, 1, 'bonLivraison.s3.color', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(353, 1, 'bonLivraison.s3.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(354, 1, 'bonLivraison.s3.price', 'Prix Unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(355, 1, 'bonLivraison.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(356, 1, 'bonLivraison.s3.totalPriceBuy', 'Prix Totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(357, 1, 'bonLivraison.s4', 'Résumé Général', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(358, 1, 'bonLivraison.s5', 'Mode de paiement', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(359, 1, 'bonLivraison.s4.sumQuantity', 'Quantité Totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(360, 1, 'bonLivraison.s4.sumTotal', 'Montant Totale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(361, 1, 'blValidation.s1.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(362, 1, 'blValidation.s1.customerName', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(363, 1, 'blValidation.s1.description', 'Note', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(364, 1, 'blValidation.s1.sumTotalPriceBuy', 'Prix Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(365, 1, 'blValidation.s1', 'La liste des ventes attentes', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(366, 1, 'blValidation.s2', 'Modifer un vente', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(367, 1, 'blValidation.s2.customer', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(368, 1, 'blValidation.s2.description', 'Note', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(369, 1, 'blValidation.s3.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(370, 1, 'blValidation.s3.color', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(371, 1, 'blValidation.s3.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(372, 1, 'blValidation.s3.unitPriceBuy', 'Prix', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(373, 1, 'blValidation.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(374, 1, 'blValidation.s3.totalTtc', 'Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(375, 1, 'blValidation.s4', 'La liste des articles', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(376, 1, 'blValidation.s4.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(377, 1, 'blValidation.s4.color', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(378, 1, 'blValidation.s4.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(379, 1, 'blValidation.s4.price', 'Prix', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(380, 1, 'blValidation.s4.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(381, 1, 'blValidation.s4.totalPriceBuy', 'Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(382, 1, 'blValidation.s5', 'Résumé Général', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(383, 1, 'blValidation.s5.sumQuantity', 'Qté Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(384, 1, 'blValidation.s5.sumTotal', 'Montant Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(385, 1, 'blValidation.s5', 'Mode de paiement', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(386, 1, 'blValidation.s3', 'Ajouter un article', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(387, 1, 'blValidation.s1.sumQuantity', 'Qté Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(388, 1, 'blValidation.s6', 'Mode de paiement', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(389, 1, 'reception.s2.sizes', 'Taille de Réception', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(390, 1, 'inventaire.s1.productSizeName', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(391, 1, 'inventaire.s1.productColorName', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(393, 1, 'profile.s1.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(394, 1, 'profile.s1.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(395, 1, 'profile.s1.email', 'E-mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(396, 1, 'profile.s1.password', 'Mot de passe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(397, 1, 'profile.s1.replayRassword', 'Re mot de passe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(398, 1, 'profile.s1.cellPhone', 'Télé Mobile', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(399, 1, 'profile.s1.fixedPhone', 'Télé Fixe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(400, 1, 'profile.s1.adress', 'Adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(401, 1, 'profile.s1', 'Modifier Mon Profile', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(402, 1, 'basicParameter.s1', 'Basic Parameter', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(403, 1, 'basicParameter.s1.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(404, 1, 'basicParameter.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(405, 1, 'basicParameter.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(406, 1, 'basicParameter.s1.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(407, 1, 'basicParameter.s1.basicParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(408, 1, 'basicParameter.s2', 'Ajouter / Editer basic Parameter', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(409, 1, 'basicParameter.s2.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(410, 1, 'basicParameter.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(411, 1, 'basicParameter.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(412, 1, 'basicParameter.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(413, 1, 'basicParameter.s2.basicParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(414, 1, 'basicParameter.s1', 'Basic Parameter', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(415, 1, 'basicParameter.s1.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(416, 1, 'basicParameter.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(417, 1, 'basicParameter.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(418, 1, 'basicParameter.s1.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(419, 1, 'basicParameter.s1.basicParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(420, 1, 'basicParameter.s2', 'Ajouter / Editer basic Parameter', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(421, 1, 'basicParameter.s2.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(422, 1, 'basicParameter.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(423, 1, 'basicParameter.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(424, 1, 'basicParameter.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(425, 1, 'basicParameter.s2.basicParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(426, 1, 'moduleParameter.s1', 'Parameter du module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(427, 1, 'moduleParameter.s1.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(428, 1, 'moduleParameter.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(429, 1, 'moduleParameter.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(430, 1, 'moduleParameter.s1.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(431, 1, 'moduleParameter.s1.moduleParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(432, 1, 'moduleParameter.s2', 'Ajouter / Editer Parameter du module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(433, 1, 'moduleParameter.s2.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(434, 1, 'moduleParameter.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(435, 1, 'moduleParameter.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(436, 1, 'moduleParameter.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(437, 1, 'moduleParameter.s2.moduleParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(438, 1, 'parameterClient.s1', 'Liste du client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(439, 1, 'parameterClient.s1.name', 'Nom et prenom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(440, 1, 'parameterClient.s1.company', 'Societer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(441, 1, 'parameterClient.s1.email', 'Email', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(442, 1, 'parameterClient.s2', 'Parameter du client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(443, 1, 'parameterClient.s2.client', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(444, 1, 'parameterClient.s2.parameterName', 'Nom de parameter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(445, 1, 'parameterClient.s2.parameterTypeName', 'Type de parameter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(446, 1, 'parameterClient.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(447, 1, 'parameterClient.s2.defaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(448, 1, 'parameterClient.s3', 'Ajouter / Editer Parameter du client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(449, 1, 'parameterClient.s3.client', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(450, 1, 'parameterClient.s3.parameter', 'Nom de parameter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(451, 1, 'parameterClient.s3.parameterDefaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(452, 1, 'parameterClient.s3.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(453, 1, 'parameter.s1', 'Paramètres', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(454, 1, 'parameter.s1.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(455, 1, 'parameter.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(456, 1, 'parameter.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(457, 1, 'parameter.s1.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(458, 1, 'parameter.s1.parameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(459, 1, 'parameter.s2', 'Ajouter / Editer paramètres', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(460, 1, 'parameter.s2.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(461, 1, 'parameter.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(462, 1, 'parameter.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(463, 1, 'parameter.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(464, 1, 'parameter.s2.parameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(465, 1, 'pageParameterModule.s1', 'Liste du client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(466, 1, 'pageParameterModule.s1.name', 'Nom et prenom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(467, 1, 'pageParameterModule.s1.company', 'Societer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(468, 1, 'pageParameterModule.s1.email', 'Email', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(469, 1, 'pageParameterModule.s2', 'Liste du module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(470, 1, 'pageParameterModule.s2.nameModule', 'Nom du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(471, 1, 'pageParameterModule.s2.descriptionModule', 'Description du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(472, 1, 'pageParameterModule.s2.moduleTypeName', 'Type du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(473, 1, 'pageParameterModule.s3', 'Liste du page', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(474, 1, 'pageParameterModule.s3.client', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(475, 1, 'pageParameterModule.s3.pageName', 'Nom du page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(476, 1, 'pageParameterModule.s3.pageDescription', 'Description du page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(477, 1, 'pageParameterModule.s3.pageTypeName', 'Nom du type page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(478, 1, 'pageParameterModule.s4', 'Ajouter / Editer Parameter du page/module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(479, 1, 'pageParameterModule.s4.pageParameter', 'Nom de parameter du page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(480, 1, 'pageParameterModule.s4.pageParameterDefaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(481, 1, 'pageParameterModule.s4.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(482, 1, 'pageParameterModule.s5', 'Ajouter / Editer Parameter du page/module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(483, 1, 'pageParameterModule.s5.pageParameter', 'Nom de parameter du page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(484, 1, 'pageParameterModule.s5.pageParameterDefaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(485, 1, 'pageParameterModule.s5.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(486, 1, 'pageParameter.s1', 'Parameter du page', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(487, 1, 'pageParameter.s1.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(488, 1, 'pageParameter.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(489, 1, 'pageParameter.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(490, 1, 'pageParameter.s1.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(491, 1, 'pageParameter.s1.pageParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(492, 1, 'pageParameter.s1.page', 'Page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(493, 1, 'pageParameter.s2', 'Ajouter / Editer Parameter du page', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(494, 1, 'pageParameter.s2.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(495, 1, 'pageParameter.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(496, 1, 'pageParameter.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(497, 1, 'pageParameter.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(498, 1, 'pageParameter.s2.pageParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(499, 1, 'pageParameter.s2.page', 'Page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(500, 1, 'moduleParameterClient.s1', 'Liste du client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(501, 1, 'moduleParameterClient.s1.name', 'Nom et prenom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(502, 1, 'moduleParameterClient.s1.company', 'Societer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(503, 1, 'moduleParameterClient.s1.email', 'Email', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(504, 1, 'moduleParameterClient.s2', 'Liste du module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(505, 1, 'moduleParameterClient.s2.nameModule', 'Nom du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(506, 1, 'moduleParameterClient.s2.descriptionModule', 'Description du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(507, 1, 'moduleParameterClient.s2.moduleTypeName', 'Type du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(508, 1, 'moduleParameterClient.s3', 'Parameter du module/client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(509, 1, 'moduleParameterClient.s3.client', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(510, 1, 'moduleParameterClient.s3.parameterModuleName', 'Nom de parameter du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(511, 1, 'moduleParameterClient.s3.parameterModuleTypeName', 'Type de parameter du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(512, 1, 'moduleParameterClient.s3.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(513, 1, 'moduleParameterClient.s3.defaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(514, 1, 'moduleParameterClient.s4', 'Ajouter / Editer Parameter du module/client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(515, 1, 'moduleParameterClient.s4.client', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(516, 1, 'moduleParameterClient.s4.parameterModule', 'Nom de parameter du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(517, 1, 'moduleParameterClient.s4.parameterModuleDefaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(518, 1, 'moduleParameterClient.s4.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(519, 1, 'validation.v1.formatTypeFax', '{0} doit être de type fax', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(520, 1, 'validation.v1.formatTypeCodePostale', '{0} doit être de type code postale', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(521, 1, 'validation.v1.formatTypeTime', '{0} doit être de type time', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(522, 1, 'validation.v1.formatTypeDateTime', '{0} doit être de type datetime', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(523, 1, 'validation.v1.formatTypePattern', '{0} doit être un {1}', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(524, 1, 'p005.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(525, 1, 'p005.s2.group', 'Groupe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(526, 1, 'p005.s2.type', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(527, 1, 'customer.s1.secondaryAddress ', 'Adresse 2', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(528, 1, 'customer.s1.category ', 'Catégorie', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(529, 1, 'customer.s1.company', 'Société', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(530, 1, 'customer.s1.zipCode', 'Code postale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(531, 1, 'customer.s1.shortLabel', 'Libelle court', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(532, 1, 'customer.s1.fullLabel', 'Libelle complet', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(533, 1, 'customer.s1.webSite', 'Site web', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(534, 1, 'customer.s1.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(535, 1, 'supplier.s1.secondaryAddress', 'Adresse 2', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(536, 1, 'supplier.s1.shortLabel', 'Libelle court', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(537, 1, 'supplier.s1.fullLabel', 'Libelle complet', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(538, 1, 'supplier.s1.webSite', 'Site web', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(539, 1, 'supplier.s1.category', 'Catégorie', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(540, 1, 'supplier.s1.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(541, 1, 'p006.s2.code', 'Code réception', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(542, 1, 'p006.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(543, 1, 'p006.s2.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(544, 1, 'p006.s2.deadline', 'Date d''écheance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(545, 1, 'p006.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(546, 1, 'p006.s2.deposit', 'Dépôt', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(547, 1, 'p032.s1', 'La liste des commandes en cours', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(548, 1, 'p032.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(549, 1, 'p032.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(550, 1, 'p032.s1.supplierCompanyName', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(551, 1, 'p032.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(552, 1, 'p032.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(553, 1, 'p032.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(556, 1, 'p032.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(557, 1, 'p032.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(560, 1, 'p032.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(561, 1, 'p032.s2', 'Ajouter / Editer une commande', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(562, 1, 'p032.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(565, 1, 'p032.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(566, 1, 'p032.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(567, 1, 'p032.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(568, 1, 'p032.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(569, 1, 'p032.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(570, 1, 'p032.s3.unitPriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(571, 1, 'p032.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(572, 1, 'p032.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(573, 1, 'p032.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(574, 1, 'p032.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(575, 1, 'p032.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(576, 1, 'p032.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(577, 1, 'p032.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(578, 1, 'p032.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(579, 1, 'p032.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(580, 1, 'p032.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(581, 1, 'p032.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(582, 1, 'p032.s5.unitPriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(583, 1, 'p032.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(584, 1, 'p032.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(585, 1, 'pdf003.s1', 'Bon de réception', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(586, 1, 'pdf003.s1', 'La liste des réceptions en cours', 2, 1, 'Y', NULL, NULL, NULL, NULL),
(587, 1, 'pdf003.s2.seq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(588, 1, 'pdf003.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(589, 1, 'pdf003.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(590, 1, 'pdf003.s2.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(591, 1, 'pdf003.s2.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(592, 1, 'pdf003.s2.TotalQuantity', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(593, 1, 'pdf004.s1', 'Bon de réception', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(594, 1, 'pdf004.s1.reference', 'Référence ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(595, 1, 'pdf004.s1.status', 'Statut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(596, 1, 'pdf004.s1.supplier', ' Fournisseur ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(597, 1, 'pdf004.s1.the', 'Le : ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(598, 1, 'pdf004.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(599, 1, 'pdf004.s2.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(600, 1, 'pdf004.s2.unitPriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(601, 1, 'pdf004.s2.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(602, 1, 'pdf004.s2.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(603, 1, 'pdf004.s3.quantity', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(604, 1, 'pdf004.s3.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(605, 1, 'pdf004.s3.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(606, 1, 'pdf004.s3.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(607, 1, 'pdf004.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(608, 1, 'pdf004.s4.note', ' Note interne ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(609, 1, 'p005.s2.threshold', 'La seuil', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(610, 1, 'p033.s1', 'La liste des commandes soumise', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(611, 1, 'p033.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(612, 1, 'p033.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(613, 1, 'p033.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(614, 1, 'p033.s1.supplierCompanyName', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(615, 1, 'p033.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(616, 1, 'p033.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(617, 1, 'p033.s2', 'Ajouter / Editer une commande', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(618, 1, 'p033.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(619, 1, 'p033.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(620, 1, 'p033.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(621, 1, 'p033.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(622, 1, 'p033.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(623, 1, 'p033.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(624, 1, 'p033.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(625, 1, 'p033.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(626, 1, 'p033.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(627, 1, 'p033.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(628, 1, 'p033.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(629, 1, 'p033.s3.unitPriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(630, 1, 'p033.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(631, 1, 'p033.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(632, 1, 'p033.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(633, 1, 'p033.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(634, 1, 'p033.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(635, 1, 'p033.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(636, 1, 'p033.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(637, 1, 'p033.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(638, 1, 'p033.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(639, 1, 'p033.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(640, 1, 'p033.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(641, 1, 'p033.s5.unitPriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(642, 1, 'p034.s1', 'La liste des commandes validée', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(643, 1, 'p034.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(644, 1, 'p034.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(645, 1, 'p034.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(646, 1, 'p034.s1.supplierCompanyName', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(647, 1, 'p034.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(648, 1, 'p034.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(649, 1, 'p034.s2', 'Ajouter / Editer une commande', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(650, 1, 'p034.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(651, 1, 'p034.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(652, 1, 'p034.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(653, 1, 'p034.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(654, 1, 'p034.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(655, 1, 'p034.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(656, 1, 'p034.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(657, 1, 'p034.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(658, 1, 'p034.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(659, 1, 'p034.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(660, 1, 'p034.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(661, 1, 'p034.s3.unitPriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(662, 1, 'p034.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(663, 1, 'p034.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(664, 1, 'p034.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(665, 1, 'p034.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(666, 1, 'p034.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(667, 1, 'p034.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(668, 1, 'p034.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(669, 1, 'p034.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(670, 1, 'p034.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(671, 1, 'p034.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(672, 1, 'p034.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(673, 1, 'p034.s5.unitPriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(674, 1, 'p032.s2.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(675, 1, 'p035.s1', 'La liste des réceptions en cours', 1, 1, 'Y', NULL, NULL, NULL, NULL);
INSERT INTO `inf_text` (`ID`, `PREFIX`, `ITEM_CODE`, `VALUE`, `TEXT_TYPE_ID`, `LANGUAGE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(676, 1, 'p035.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(677, 1, 'p035.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(678, 1, 'p035.s1.supplierCompanyName', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(679, 1, 'p035.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(680, 1, 'p035.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(681, 1, 'p035.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(682, 1, 'p035.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(683, 1, 'p035.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(684, 1, 'p035.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(685, 1, 'p035.s2', 'Ajouter / Editer une réception', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(686, 1, 'p035.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(687, 1, 'p035.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(688, 1, 'p035.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(689, 1, 'p035.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(690, 1, 'p035.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(691, 1, 'p035.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(692, 1, 'p035.s3.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(693, 1, 'p035.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(694, 1, 'p035.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(695, 1, 'p035.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(696, 1, 'p035.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(697, 1, 'p035.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(698, 1, 'p035.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(699, 1, 'p035.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(700, 1, 'p035.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(701, 1, 'p035.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(702, 1, 'p035.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(703, 1, 'p035.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(704, 1, 'p035.s5.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(705, 1, 'p035.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(706, 1, 'p035.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(707, 1, 'p035.s2.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(708, 1, 'pdf001.s1', 'Bon de commande fournisseur', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(709, 1, 'pdf001.s1', 'La liste des commande en cours', 2, 1, 'Y', NULL, NULL, NULL, NULL),
(710, 1, 'pdf001.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(711, 1, 'pdf001.s2.seq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(712, 1, 'pdf001.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(713, 1, 'pdf001.s2.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(714, 1, 'pdf001.s2.TotalQuantity', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(715, 1, 'pdf001.s2.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(716, 1, 'pdf002.s1', 'Bon de commande fournisseur', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(717, 1, 'pdf002.s1.reference', 'Référence ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(718, 1, 'pdf002.s1.status', 'Statut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(719, 1, 'pdf002.s1.supplier', ' Fournisseur ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(720, 1, 'pdf002.s1.the', 'Le : ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(721, 1, 'pdf002.s2.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(722, 1, 'pdf002.s2.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(723, 1, 'pdf002.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(724, 1, 'pdf002.s2.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(725, 1, 'pdf002.s2.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(726, 1, 'pdf002.s3.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(727, 1, 'pdf002.s3.quantity', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(728, 1, 'pdf002.s3.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(729, 1, 'pdf002.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(730, 1, 'pdf002.s3.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(731, 1, 'pdf002.s4.note', ' Note interne ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(732, 1, 'p036.s1', 'La liste des réceptions soumise', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(733, 1, 'p036.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(734, 1, 'p036.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(735, 1, 'p036.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(736, 1, 'p036.s1.supplierCompanyName', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(737, 1, 'p036.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(738, 1, 'p036.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(739, 1, 'p036.s2', 'Ajouter / Editer une réception', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(740, 1, 'p036.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(741, 1, 'p036.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(742, 1, 'p036.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(743, 1, 'p036.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(744, 1, 'p036.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(745, 1, 'p036.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(746, 1, 'p036.s2.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(747, 1, 'p036.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(748, 1, 'p036.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(749, 1, 'p036.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(750, 1, 'p036.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(751, 1, 'p036.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(752, 1, 'p036.s3.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(753, 1, 'p036.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(754, 1, 'p036.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(755, 1, 'p036.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(756, 1, 'p036.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(757, 1, 'p036.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(758, 1, 'p036.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(759, 1, 'p036.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(760, 1, 'p036.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(761, 1, 'p036.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(762, 1, 'p036.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(763, 1, 'p036.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(764, 1, 'p036.s5.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(765, 1, 'p037.s1', 'La liste des réceptions validée', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(766, 1, 'p037.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(767, 1, 'p037.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(768, 1, 'p037.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(769, 1, 'p037.s1.supplierCompanyName', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(770, 1, 'p037.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(771, 1, 'p037.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(772, 1, 'p037.s2', 'Ajouter / Editer une réception', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(773, 1, 'p037.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(774, 1, 'p037.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(775, 1, 'p037.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(776, 1, 'p037.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(777, 1, 'p037.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(778, 1, 'p037.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(779, 1, 'p037.s2.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(780, 1, 'p037.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(781, 1, 'p037.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(782, 1, 'p037.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(783, 1, 'p037.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(784, 1, 'p037.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(785, 1, 'p037.s3.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(786, 1, 'p037.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(787, 1, 'p037.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(788, 1, 'p037.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(789, 1, 'p037.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(790, 1, 'p037.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(791, 1, 'p037.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(792, 1, 'p037.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(793, 1, 'p037.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(794, 1, 'p037.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(795, 1, 'p037.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(796, 1, 'p037.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(797, 1, 'p037.s5.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(798, 1, 'p035.s2.commande', 'Commande', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(799, 1, 'p038.s1', 'La liste des ventes en cours', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(800, 1, 'p038.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(801, 1, 'p038.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(802, 1, 'p038.s1.supplierCompanyName', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(803, 1, 'p038.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(804, 1, 'p038.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(805, 1, 'p038.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(806, 1, 'p038.s2.supplier', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(807, 1, 'p038.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(808, 1, 'p038.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(809, 1, 'p038.s2', 'Ajouter / Editer un vente', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(810, 1, 'p038.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(811, 1, 'p038.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(812, 1, 'p038.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(813, 1, 'p038.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(814, 1, 'p038.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(815, 1, 'p038.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(816, 1, 'p038.s3.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(817, 1, 'p038.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(818, 1, 'p038.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(819, 1, 'p038.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(820, 1, 'p038.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(821, 1, 'p038.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(822, 1, 'p038.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(823, 1, 'p038.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(824, 1, 'p038.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(825, 1, 'p038.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(826, 1, 'p038.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(827, 1, 'p038.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(828, 1, 'p038.s5.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(829, 1, 'p038.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(830, 1, 'p038.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(831, 1, 'p038.s2.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(832, 1, 'p038.s2.commande', 'Commande', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(833, 1, 'p038.s3.unitPriceSale', 'Prix vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(834, 1, 'p038.s5.unitPriceSale', 'Prix vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(835, 1, 'pdf005.s1', 'Bon de vente', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(836, 1, 'pdf005.s1', 'La liste des ventes en cours', 2, 1, 'Y', NULL, NULL, NULL, NULL),
(837, 1, 'pdf005.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(838, 1, 'pdf005.s2.seq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(839, 1, 'pdf005.s2.supplier', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(840, 1, 'pdf005.s2.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(841, 1, 'pdf005.s2.TotalQuantity', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(842, 1, 'pdf005.s2.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(843, 1, 'pdf006.s1', 'Bon de vente', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(844, 1, 'pdf006.s1.reference', 'Référence ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(845, 1, 'pdf006.s1.status', 'Statut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(846, 1, 'pdf006.s1.supplier', ' Client ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(847, 1, 'pdf006.s1.the', 'Le : ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(848, 1, 'pdf006.s2.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(849, 1, 'pdf006.s2.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(850, 1, 'pdf006.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(851, 1, 'pdf006.s2.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(852, 1, 'pdf006.s2.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(853, 1, 'pdf006.s3.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(854, 1, 'pdf006.s3.quantity', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(855, 1, 'pdf006.s3.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(856, 1, 'pdf006.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(857, 1, 'pdf006.s3.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(858, 1, 'pdf006.s4.note', ' Note interne ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(859, 1, 'p041.s1', 'La liste des bons de retour en cours', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(860, 1, 'p041.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(861, 1, 'p041.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(862, 1, 'p041.s1.customerCompanyName', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(863, 1, 'p041.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(864, 1, 'p041.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(865, 1, 'p041.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(866, 1, 'p041.s2.customer', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(867, 1, 'p041.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(868, 1, 'p041.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(869, 1, 'p041.s2', 'Ajouter / Editer un bon de retour', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(870, 1, 'p041.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(871, 1, 'p041.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(872, 1, 'p041.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(873, 1, 'p041.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(874, 1, 'p041.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(875, 1, 'p041.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(876, 1, 'p041.s3.negotiatePriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(877, 1, 'p041.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(878, 1, 'p041.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(879, 1, 'p041.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(880, 1, 'p041.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(881, 1, 'p041.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(882, 1, 'p041.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(883, 1, 'p041.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(884, 1, 'p041.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(885, 1, 'p041.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(886, 1, 'p041.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(887, 1, 'p041.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(888, 1, 'p041.s5.negotiatePriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(889, 1, 'p041.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(890, 1, 'p041.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(891, 1, 'p041.s2.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(892, 1, 'p041.s2.commande', 'Commande', 3, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_text_type`
--

CREATE TABLE `inf_text_type` (
`ID` bigint(20) NOT NULL,
  `CODE` tinytext CHARACTER SET latin1,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_text_type`
--

INSERT INTO `inf_text_type` (`ID`, `CODE`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'title', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'message', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'label', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'help', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(5, 'comment', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(6, 'placeholder', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(7, 'error', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_attribute_exclud`
--

CREATE TABLE `pm_attribute_exclud` (
`ID` bigint(20) NOT NULL,
  `PAGE_ID` bigint(20) DEFAULT NULL,
  `INF_ITEM_CODE` tinytext,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_attribute_exclud`
--

INSERT INTO `pm_attribute_exclud` (`ID`, `PAGE_ID`, `INF_ITEM_CODE`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 3, 'ok', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_attribute_validation`
--

CREATE TABLE `pm_attribute_validation` (
`ID` bigint(20) NOT NULL,
  `PAGE_ID` bigint(20) DEFAULT NULL,
  `INF_ITEM_CODE` tinytext,
  `VALIDATION_ID` bigint(20) DEFAULT NULL,
  `PARAMS` mediumtext CHARACTER SET latin1,
  `CUSTOM_ERROR` text CHARACTER SET latin1,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=286 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_attribute_validation`
--

INSERT INTO `pm_attribute_validation` (`ID`, `PAGE_ID`, `INF_ITEM_CODE`, `VALIDATION_ID`, `PARAMS`, `CUSTOM_ERROR`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, 'page1.s1.name', 10, '', 'error', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 1, 'page1.s1.name', 7, '', NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(3, 1, 'page1.s1.name', 9, '3', NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(5, 3, 'expense.s2.description', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(7, 3, 'expense.s2.amount', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(8, 3, 'expense.s2.amount', 2, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(10, 3, 'expense.s2.type', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(11, 4, 'supplier.s1.type', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(14, 4, 'supplier.s1.country', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(15, 4, 'supplier.s1.city', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(16, 4, 'supplier.s1.companyName', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(18, 4, 'supplier.s1.fixedPhone', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(19, 4, 'supplier.s1.adress', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(20, 4, 'supplier.s1.email', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(21, 4, 'supplier.s1.active', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(22, 6, 'reception.s2.name', 10, '', 'error', 1, 'Y', NULL, NULL, NULL, NULL),
(24, 6, 'reception.s2.supplier', 10, '', '', 1, 'Y', NULL, NULL, NULL, NULL),
(31, 5, 'product.s2.designation', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(32, 5, 'product.s2.quantity', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(33, 5, 'product.s2.reference', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(34, 5, 'product.s2.pricesale', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(35, 5, 'product.s2.active', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(36, 5, 'product.s2.status', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(37, 5, 'product.s2.size', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(38, 5, 'product.s2.family', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(39, 5, 'product.s2.color', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(40, 5, 'product.s2.save', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(41, 5, 'product.s2.quantity', 1, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(42, 5, 'product.s2.pricesale', 2, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(43, 3, 'expense.s2.name', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(44, 5, 'product.s2.priceBuy', 2, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(45, 5, 'product.s2.priceBuy', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(46, 4, 'supplier.s1.postCode', 1, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(50, 7, 'customer.s1.adress', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(51, 7, 'customer.s1.country', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(52, 7, 'customer.s1.city', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(53, 7, 'customer.s1.type', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(54, 7, 'customer.s1.active', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(55, 13, 'bonLivraison.s1.customer', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(56, 13, 'bonLivraison.s1.description', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(57, 1, 'page1.s1.name', 10, '', 'error', 3, 'Y', NULL, NULL, NULL, NULL),
(58, 1, 'page1.s1.name', 7, '', NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(59, 1, 'page1.s1.name', 9, '3', NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(60, 3, 'expense.s2.description', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(61, 3, 'expense.s2.amount', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(62, 3, 'expense.s2.amount', 2, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(63, 3, 'expense.s2.type', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(64, 4, 'supplier.s1.type', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(65, 4, 'supplier.s1.country', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(66, 4, 'supplier.s1.city', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(67, 4, 'supplier.s1.companyName', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(68, 4, 'supplier.s1.fixedPhone', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(69, 4, 'supplier.s1.adress', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(70, 4, 'supplier.s1.email', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(71, 4, 'supplier.s1.active', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(72, 6, 'reception.s2.name', 10, '', 'error', 3, 'Y', NULL, NULL, NULL, NULL),
(73, 6, 'reception.s2.supplier', 10, '', '', 3, 'Y', NULL, NULL, NULL, NULL),
(74, 5, 'product.s2.designation', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(75, 5, 'product.s2.quantity', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(76, 5, 'product.s2.reference', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(77, 5, 'product.s2.pricesale', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(78, 5, 'product.s2.active', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(79, 5, 'product.s2.status', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(80, 5, 'product.s2.size', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(81, 5, 'product.s2.family', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(82, 5, 'product.s2.color', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(83, 5, 'product.s2.save', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(84, 5, 'product.s2.quantity', 1, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(85, 5, 'product.s2.pricesale', 2, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(86, 3, 'expense.s2.name', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(87, 5, 'product.s2.priceBuy', 2, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(88, 5, 'product.s2.priceBuy', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(89, 4, 'supplier.s1.postCode', 1, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(90, 7, 'customer.s1.adress', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(91, 7, 'customer.s1.country', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(92, 7, 'customer.s1.city', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(93, 7, 'customer.s1.type', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(94, 7, 'customer.s1.active', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(95, 13, 'bonLivraison.s1.customer', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(96, 13, 'bonLivraison.s1.description', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(120, 1, 'page1.s1.name', 10, '', 'error', 4, 'Y', NULL, NULL, NULL, NULL),
(121, 1, 'page1.s1.name', 7, '', NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(122, 1, 'page1.s1.name', 9, '3', NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(123, 3, 'expense.s2.description', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(124, 3, 'expense.s2.amount', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(125, 3, 'expense.s2.amount', 2, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(126, 3, 'expense.s2.type', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(127, 4, 'supplier.s1.type', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(128, 4, 'supplier.s1.country', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(129, 4, 'supplier.s1.city', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(130, 4, 'supplier.s1.companyName', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(131, 4, 'supplier.s1.fixedPhone', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(132, 4, 'supplier.s1.adress', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(133, 4, 'supplier.s1.email', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(134, 4, 'supplier.s1.active', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(135, 6, 'reception.s2.name', 10, '', 'error', 4, 'Y', NULL, NULL, NULL, NULL),
(136, 6, 'reception.s2.supplier', 10, '', '', 4, 'Y', NULL, NULL, NULL, NULL),
(137, 5, 'product.s2.designation', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(138, 5, 'product.s2.quantity', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(139, 5, 'product.s2.reference', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(140, 5, 'product.s2.pricesale', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(141, 5, 'product.s2.active', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(142, 5, 'product.s2.status', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(143, 5, 'product.s2.size', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(144, 5, 'product.s2.family', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(145, 5, 'product.s2.color', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(146, 5, 'product.s2.save', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(147, 5, 'product.s2.quantity', 1, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(148, 5, 'product.s2.pricesale', 2, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(149, 3, 'expense.s2.name', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(150, 5, 'product.s2.priceBuy', 2, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(151, 5, 'product.s2.priceBuy', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(152, 4, 'supplier.s1.postCode', 1, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(153, 7, 'customer.s1.adress', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(154, 7, 'customer.s1.country', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(155, 7, 'customer.s1.city', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(156, 7, 'customer.s1.type', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(157, 7, 'customer.s1.active', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(158, 13, 'bonLivraison.s1.customer', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(159, 13, 'bonLivraison.s1.description', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(183, 1, 'page1.s1.name', 10, '', 'error', 5, 'Y', NULL, NULL, NULL, NULL),
(184, 1, 'page1.s1.name', 7, '', NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(185, 1, 'page1.s1.name', 9, '3', NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(186, 3, 'expense.s2.description', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(187, 3, 'expense.s2.amount', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(188, 3, 'expense.s2.amount', 2, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(189, 3, 'expense.s2.type', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(190, 4, 'supplier.s1.type', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(191, 4, 'supplier.s1.country', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(192, 4, 'supplier.s1.city', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(193, 4, 'supplier.s1.companyName', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(194, 4, 'supplier.s1.fixedPhone', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(195, 4, 'supplier.s1.adress', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(196, 4, 'supplier.s1.email', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(197, 4, 'supplier.s1.active', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(198, 6, 'reception.s2.name', 10, '', 'error', 5, 'Y', NULL, NULL, NULL, NULL),
(199, 6, 'reception.s2.supplier', 10, '', '', 5, 'Y', NULL, NULL, NULL, NULL),
(200, 5, 'product.s2.designation', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(201, 5, 'product.s2.quantity', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(202, 5, 'product.s2.reference', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(203, 5, 'product.s2.pricesale', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(204, 5, 'product.s2.active', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(205, 5, 'product.s2.status', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(206, 5, 'product.s2.size', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(207, 5, 'product.s2.family', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(208, 5, 'product.s2.color', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(209, 5, 'product.s2.save', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(210, 5, 'product.s2.quantity', 1, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(211, 5, 'product.s2.pricesale', 2, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(212, 3, 'expense.s2.name', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(213, 5, 'product.s2.priceBuy', 2, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(214, 5, 'product.s2.priceBuy', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(215, 4, 'supplier.s1.postCode', 1, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(216, 7, 'customer.s1.adress', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(217, 7, 'customer.s1.country', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(218, 7, 'customer.s1.city', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(219, 7, 'customer.s1.type', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(220, 7, 'customer.s1.active', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(221, 13, 'bonLivraison.s1.customer', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(222, 13, 'bonLivraison.s1.description', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(246, 1, 'page1.s1.name', 10, '', 'error', 6, 'Y', NULL, NULL, NULL, NULL),
(247, 1, 'page1.s1.name', 7, '', NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(248, 1, 'page1.s1.name', 9, '3', NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(249, 3, 'expense.s2.description', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(250, 3, 'expense.s2.amount', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(251, 3, 'expense.s2.amount', 2, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(252, 3, 'expense.s2.type', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(253, 4, 'supplier.s1.type', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(254, 4, 'supplier.s1.country', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(255, 4, 'supplier.s1.city', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(256, 4, 'supplier.s1.companyName', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(257, 4, 'supplier.s1.fixedPhone', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(258, 4, 'supplier.s1.adress', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(259, 4, 'supplier.s1.email', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(260, 4, 'supplier.s1.active', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(261, 6, 'reception.s2.name', 10, '', 'error', 6, 'Y', NULL, NULL, NULL, NULL),
(262, 6, 'reception.s2.supplier', 10, '', '', 6, 'Y', NULL, NULL, NULL, NULL),
(263, 5, 'product.s2.designation', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(264, 5, 'product.s2.quantity', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(265, 5, 'product.s2.reference', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(266, 5, 'product.s2.pricesale', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(267, 5, 'product.s2.active', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(268, 5, 'product.s2.status', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(269, 5, 'product.s2.size', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(270, 5, 'product.s2.family', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(271, 5, 'product.s2.color', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(272, 5, 'product.s2.save', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(273, 5, 'product.s2.quantity', 1, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(274, 5, 'product.s2.pricesale', 2, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(275, 3, 'expense.s2.name', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(276, 5, 'product.s2.priceBuy', 2, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(277, 5, 'product.s2.priceBuy', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(278, 4, 'supplier.s1.postCode', 1, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(279, 7, 'customer.s1.adress', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(280, 7, 'customer.s1.country', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(281, 7, 'customer.s1.city', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(282, 7, 'customer.s1.type', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(283, 7, 'customer.s1.active', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(284, 13, 'bonLivraison.s1.customer', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(285, 13, 'bonLivraison.s1.description', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_category`
--

CREATE TABLE `pm_category` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT NULL,
  `CATEGORY_TYPE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `IMAGE_PATH` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_category`
--

INSERT INTO `pm_category` (`ID`, `NAME`, `DESCRIPTION`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `ACTIVE`, `CATEGORY_TYPE_ID`, `SORT_KEY`, `IMAGE_PATH`) VALUES
(1, 'Fichiers', 'Fichiers', NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(2, 'Entrées:Fournisseur', 'Entrées', NULL, NULL, NULL, NULL, 'Y', 1, 2, 'icons/1.png'),
(3, 'Sorties : Client', 'Sorties', NULL, NULL, NULL, NULL, 'Y', 1, 3, 'icons/1.png'),
(4, 'Divers', 'Divers', '2014-11-13 00:00:00', NULL, NULL, NULL, 'Y', 1, 4, 'icons/1.png'),
(5, 'Statistiques', 'Rapport', NULL, NULL, NULL, NULL, 'Y', 1, 6, 'icons/1.png'),
(6, 'Recourcie', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 7, 'icons/1.png'),
(7, 'Generale', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(8, 'Ressource', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 2, 'icons/1.png'),
(9, 'Module', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 3, 'icons/1.png'),
(10, 'Parametrage', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 4, 'icons/1.png'),
(11, 'Generale', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(12, 'Ressource', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(13, 'Modules', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(14, 'Paramètrage', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(15, 'Paramètrage', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(16, 'Comd Fournisseur', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(17, 'Retour : Avoir', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png');

-- --------------------------------------------------------

--
-- Structure de la table `pm_category_type`
--

CREATE TABLE `pm_category_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext CHARACTER SET latin1,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_category_type`
--

INSERT INTO `pm_category_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Header', 'DFFF', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Sub Header', ' FGGE', 2, 'Y', NULL, NULL, NULL, NULL),
(3, 'Principal', ' FERT', 3, 'Y', NULL, NULL, NULL, NULL),
(4, 'Footer', 'FFZF', 5, 'Y', NULL, NULL, NULL, NULL),
(5, 'Footer', '  GGDZ', 4, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_component`
--

CREATE TABLE `pm_component` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` tinytext,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` tinytext,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_component`
--

INSERT INTO `pm_component` (`ID`, `NAME`, `DESCRIPTION`, `ACTIVE`, `SORT_KEY`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'inpuText', NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'textarea', NULL, 'Y', 2, NULL, NULL, NULL, NULL),
(3, 'comobox', NULL, 'Y', 3, NULL, NULL, NULL, NULL),
(4, 'table', NULL, 'Y', 4, NULL, NULL, NULL, NULL),
(5, 'column', NULL, 'Y', 5, NULL, NULL, NULL, NULL),
(6, 'other', NULL, 'Y', 6, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_composition`
--

CREATE TABLE `pm_composition` (
`ID` bigint(20) NOT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `GROUP_ID` bigint(20) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `MENU_ID` bigint(20) DEFAULT NULL,
  `PAGE_ID` bigint(20) DEFAULT NULL,
  `INDEX_SHOW` char(1) DEFAULT NULL,
  `GROUP_SORT` bigint(20) DEFAULT NULL,
  `MENU_SORT` bigint(20) DEFAULT NULL,
  `CATEGORY_SORT` bigint(20) DEFAULT NULL,
  `PAGE_SORT` bigint(20) DEFAULT NULL,
  `INDEX_SORT` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=217 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_composition`
--

INSERT INTO `pm_composition` (`ID`, `CLT_MODULE_ID`, `GROUP_ID`, `CATEGORY_ID`, `MENU_ID`, `PAGE_ID`, `INDEX_SHOW`, `GROUP_SORT`, `MENU_SORT`, `CATEGORY_SORT`, `PAGE_SORT`, `INDEX_SORT`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, 3, 1, 1, 5, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 1, 3, 1, 2, 12, '', 1, 2, 2, 2, 0, 'Y', NULL, NULL, NULL, NULL),
(4, 1, 3, 1, 4, 4, '', 1, 10, 10, 6, NULL, 'Y', NULL, NULL, NULL, NULL),
(5, 1, 3, 2, 5, 35, NULL, 1, 11, 11, 7, NULL, 'Y', NULL, NULL, NULL, NULL),
(6, 1, 3, 2, 6, 36, NULL, 1, 12, 12, 8, NULL, 'Y', NULL, NULL, NULL, NULL),
(7, 1, 3, 2, 7, 37, NULL, 1, 13, 13, 9, NULL, 'Y', NULL, NULL, NULL, NULL),
(8, 1, 3, 1, 8, 7, NULL, 1, 14, 14, 10, NULL, 'Y', NULL, NULL, NULL, NULL),
(9, 1, 3, 3, 9, 38, NULL, 1, 15, 15, 11, NULL, 'Y', NULL, NULL, NULL, NULL),
(10, 1, 3, 3, 10, 39, NULL, 1, 16, 16, 12, NULL, 'Y', NULL, NULL, NULL, NULL),
(11, 1, 3, 3, 11, 40, NULL, 1, 17, 17, 13, NULL, 'Y', NULL, NULL, NULL, NULL),
(15, 1, 3, 5, 15, 14, NULL, 1, 21, 21, 15, NULL, 'Y', NULL, NULL, NULL, NULL),
(16, 1, 2, 6, 16, 4, NULL, 1, 22, 22, 16, NULL, 'Y', NULL, NULL, NULL, NULL),
(17, 1, 2, 6, 17, 25, NULL, 1, 23, 23, 17, NULL, 'Y', NULL, NULL, NULL, NULL),
(18, 1, 2, 6, 18, 7, NULL, 1, 24, 24, 18, NULL, 'Y', NULL, NULL, NULL, NULL),
(19, 1, 2, 6, 19, 13, NULL, 1, 25, 25, 19, NULL, 'Y', NULL, NULL, NULL, NULL),
(20, 1, 2, 6, 20, 16, NULL, 1, 29, 29, 24, NULL, 'Y', NULL, NULL, NULL, NULL),
(72, 5, 3, 11, 26, 28, NULL, 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(188, 3, 3, 7, 21, 1, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(190, 3, 3, 8, 22, 1, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(193, 3, 3, 9, 23, 31, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(196, 3, 3, 9, 24, 27, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(197, 5, 3, 12, 27, 30, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(198, 3, 3, 10, 25, 26, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(199, 5, 3, 13, 28, 29, NULL, 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(200, 5, 3, 14, 29, 25, NULL, 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(201, 10, 3, 15, 30, 21, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(202, 10, 3, 15, 31, 22, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(203, 10, 3, 15, 32, 23, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(204, 10, 3, 15, 33, 24, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(206, 3, 3, 10, 35, 19, NULL, 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(207, 1, 1, 1, 36, 20, NULL, 1, 30, 30, 25, NULL, 'Y', NULL, NULL, NULL, NULL),
(208, 3, 1, 1, 36, 20, NULL, 1, 23, 23, 23, NULL, 'Y', NULL, NULL, NULL, NULL),
(209, 5, 1, 1, 36, 20, NULL, 1, 24, 24, 24, NULL, 'Y', NULL, NULL, NULL, NULL),
(210, 10, 1, 1, 36, 20, NULL, 1, 25, 25, 25, NULL, 'Y', NULL, NULL, NULL, NULL),
(211, 1, 3, 16, 38, 32, NULL, 1, 3, 3, 3, NULL, 'Y', NULL, NULL, NULL, NULL),
(212, 1, 3, 16, 39, 33, NULL, 1, 4, 4, 4, NULL, 'Y', NULL, NULL, NULL, NULL),
(213, 1, 3, 16, 40, 34, NULL, 1, 5, 5, 5, NULL, 'Y', NULL, NULL, NULL, NULL),
(214, 1, 3, 17, 41, 41, NULL, 1, 26, 26, 20, NULL, 'Y', NULL, NULL, NULL, NULL),
(215, 1, 3, 17, 42, 42, NULL, 1, 27, 27, 22, NULL, 'Y', NULL, NULL, NULL, NULL),
(216, 1, 3, 17, 43, 43, NULL, 1, 28, 28, 23, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_data_type`
--

CREATE TABLE `pm_data_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `PARAM_NUMBER` bigint(20) DEFAULT '1',
  `PARAMS` tinytext,
  `ERROR_MESSAGE` tinytext,
  `HELP` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_data_type`
--

INSERT INTO `pm_data_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `PARAM_NUMBER`, `PARAMS`, `ERROR_MESSAGE`, `HELP`) VALUES
(1, 'Texte', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 1, 'String', 'validation.v1.dataTypeString', NULL),
(2, 'Caractère', NULL, 2, 'Y', NULL, NULL, NULL, NULL, 1, 'Char', 'validation.v1.dataTypeChar', NULL),
(3, 'Nombre entière', NULL, 3, 'Y', NULL, NULL, NULL, NULL, 1, 'Integer', 'validation.v1.dataTypeInteger', NULL),
(4, 'Nombre réel', NULL, 4, 'Y', NULL, NULL, NULL, NULL, 1, 'Double', 'validation.v1.dataTypeDouble', NULL),
(5, 'datetime', NULL, 5, 'Y', NULL, NULL, NULL, NULL, 1, 'DateTime', 'validation.v1.dataTypeDateTime', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_format_type`
--

CREATE TABLE `pm_format_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `PARAM_NUMBER` bigint(20) DEFAULT '1',
  `PARAMS` tinytext,
  `ERROR_MESSAGE` tinytext,
  `HELP` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_format_type`
--

INSERT INTO `pm_format_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `PARAM_NUMBER`, `PARAMS`, `ERROR_MESSAGE`, `HELP`) VALUES
(1, 'None', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'none', 'validation.v1.formatTypeNone', NULL),
(2, 'Email', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'mail', 'validation.v1.formatTypeEmail', NULL),
(3, 'Phone', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'phone', 'validation.v1.formatTypePhone', NULL),
(4, 'Fax', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'fax', 'validation.v1.formatTypeFax', NULL),
(5, 'Code postale', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'codePostale', 'validation.v1.formatTypeCodePostale', NULL),
(6, 'Date', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'date', 'validation.v1.formatTypeDate', NULL),
(7, 'Time', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'time', 'validation.v1.formatTypeTime', NULL),
(8, 'DateTime', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'datetime', 'validation.v1.formatTypeDateTime', NULL),
(9, 'Pattern', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, NULL, 'validation.v1.formatTypePattern', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_group`
--

CREATE TABLE `pm_group` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `GROUP_TYPE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_group`
--

INSERT INTO `pm_group` (`ID`, `NAME`, `DESCRIPTION`, `GROUP_TYPE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Header', 'header', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'sub Header', 'sub Header', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(3, 'Menu', 'Menu', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(4, 'Footer', 'Footer', 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_group_type`
--

CREATE TABLE `pm_group_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_group_type`
--

INSERT INTO `pm_group_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Simple', 'Simple', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Avancée', 'Avancée', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_menu`
--

CREATE TABLE `pm_menu` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `MENU_TYPE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `IMAGE_PATH` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_menu`
--

INSERT INTO `pm_menu` (`ID`, `NAME`, `DESCRIPTION`, `MENU_TYPE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `IMAGE_PATH`) VALUES
(1, 'Produit', 'Page de TEST', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/product.png'),
(2, 'Statut de stock', 'Gestion des produits', 1, 2, 'Y', NULL, NULL, NULL, NULL, 'icons/status.png'),
(3, 'Inventaire', 'Gestion des dépenses', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(4, 'Gestion des Fournisseurs', 'Gestion des fournisseurs   ', 1, NULL, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(5, 'Reception', 'Gestion des réception', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(6, 'Validation des réceptions', 'Réception', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(7, 'Historique des réceptions', 'Dépense', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(8, 'Client', 'Fournisseurs', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(9, 'Nouveau Bon laivraison', 'Produits', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(10, 'Validation des BL', 'Réception', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(11, 'Historiques des BL', 'Client', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(12, 'Changer un produit', 'Gestion des clients', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(13, 'Dépenses', 'Validation des réceptions', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/coins.png'),
(14, 'Avance de produit', 'Historique des réceptions', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(15, 'Rapports et statistiques', 'Promotion des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/chart.png'),
(16, 'Fournisseur', 'Inventaire', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/onebit_63.png'),
(17, 'Reception', 'Statut de stock', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/onebit_19.png'),
(18, 'Clients', 'Nouveau Vente', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/onebit_18.png'),
(19, 'Nou Vents', 'Vente en attente', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/onebit_85.png'),
(20, 'Statistiques', 'R. Depense', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/onebit_66.png'),
(21, 'Information générale', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(22, 'Rôle d''utilisateur', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(23, 'Proprietes des elements', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(24, 'Configuration des pages', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(25, 'Configuration générale', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(26, 'Information', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(27, 'Utilisateur', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(28, 'Modules', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(29, 'Parametrage', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(30, 'Les paramèetres Basique', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(31, 'Les paramèetres Client', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(32, 'Les paramèetres module', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(33, 'Les paramèetres de page', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(34, 'Promotions de produits', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(35, 'Liste des valeurs', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(36, 'Mon Profile', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/4.png'),
(37, 'to del', 'Configuration des modules', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(38, 'Commandes', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(39, 'Validation commande', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(40, 'Historique commande', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(41, 'Bon de retour', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(42, 'Validation Bon de retour', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(43, 'Historique bon de retour', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png');

-- --------------------------------------------------------

--
-- Structure de la table `pm_menu_type`
--

CREATE TABLE `pm_menu_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_menu_type`
--

INSERT INTO `pm_menu_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'menu1 typ', 'Menu 1 type ', 1, 'Y', NULL, NULL, NULL, NULL),
(4, 'menu type 2', ' mznu type 2', 2, 'Y', NULL, NULL, NULL, NULL),
(6, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_module_attribute`
--

CREATE TABLE `pm_module_attribute` (
`ID` bigint(20) NOT NULL,
  `PM_PAGE_ATTRIBUTE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `IS_REQUIRED` char(1) DEFAULT 'N',
  `IS_READONLY` char(1) DEFAULT 'N',
  `DATA_TYPE_ID` bigint(20) DEFAULT NULL,
  `FORMAT_TYPE_ID` bigint(20) DEFAULT NULL,
  `MAX_LENGHT` bigint(20) DEFAULT NULL,
  `MAX_WORD` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `DATE_UPDATE` varchar(45) DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pm_module_pdf_attribute`
--

CREATE TABLE `pm_module_pdf_attribute` (
`ID` bigint(20) NOT NULL,
  `PM_PDF_ATTRIBUTE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATA_TYPE_ID` bigint(20) DEFAULT NULL,
  `FORMAT_TYPE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `DATE_UPDATE` varchar(45) DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pm_page`
--

CREATE TABLE `pm_page` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `PAGE_TYPE_ID` bigint(20) DEFAULT NULL,
  `IN_DEV` char(1) DEFAULT 'N',
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_page`
--

INSERT INTO `pm_page` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `PAGE_TYPE_ID`, `IN_DEV`, `ACTIVE`, `USER_CREATION`, `DATE_CREATION`, `USER_UPDATE`, `DATE_UPDATE`) VALUES
(1, 'page Test', 'page test1', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(2, 'Page add admin ', 'Page add admin', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(3, 'Page Expense', 'Page pour la gestion des dépense', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(4, 'Page Supplier', 'Page pour la gestion des fournisseurs', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(5, 'Page produit', 'Page pour la gestion des produits', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(6, 'Page de réception', 'Page pour la gestion des réceptions et BL', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(7, 'Page de custmer', 'customer', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(8, 'Page de validation des réceptions', 'Page de validation des réceptions', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(9, 'page de historique de reception', 'page de historique de reception', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(10, 'Page de promotion des prodtuis', 'promo produit', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(11, 'Inventaire', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(12, 'statut de stock : capital', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(13, 'Bon de livraison Client', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(14, 'Valdation de Bon de livraison Client', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(15, 'Historique de Bon de livraison Client', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(16, 'les rapports cotidiennet', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(17, 'Changé un produit', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(18, 'avance de produit', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(19, 'la liste des valeurs', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(20, 'Mon profil', NULL, 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(21, 'Les paramètres basiques', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(22, 'Paramètres', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(23, 'Parameter du module', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(24, 'Parameter du page', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(25, 'Parameter du client', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(26, 'Parameter du module/client', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(27, 'Parameter du page/module', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(28, 'Gestion des clients', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(29, 'Gestion des modules', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(30, 'Gestion des utilisateurs', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(31, 'Proprietes des elements', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(32, 'Commande fournisseur', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(33, 'Validation commande fournisseur', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(34, 'Historique commande fournisseur', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(35, 'Receptions en cours', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(36, 'Validation des receptions', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(37, 'Historique des validations', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(38, 'Ventes en cours', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(39, 'Validation des ventes', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(40, 'Historique des ventes', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(41, 'Avoir en cours', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(42, 'Validation Bon de retour', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(43, 'Historique Bon de retour', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_page_attribute`
--

CREATE TABLE `pm_page_attribute` (
`ID` bigint(20) NOT NULL,
  `PAGE_ID` bigint(20) DEFAULT NULL,
  `INF_ITEM_CODE` varchar(255) DEFAULT NULL,
  `PM_COMPONENT_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `IS_REQUIRED` char(1) DEFAULT 'N',
  `IS_READONLY` char(1) DEFAULT 'N',
  `IS_HIDDEN` char(1) DEFAULT 'N',
  `DATA_TYPE_ID` bigint(20) DEFAULT NULL,
  `FORMAT_TYPE_ID` bigint(20) DEFAULT NULL,
  `MAX_LENGHT` bigint(20) DEFAULT NULL,
  `MAX_WORD` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `DATE_UPDATE` varchar(45) DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_page_attribute`
--

INSERT INTO `pm_page_attribute` (`ID`, `PAGE_ID`, `INF_ITEM_CODE`, `PM_COMPONENT_ID`, `SORT_KEY`, `ACTIVE`, `IS_REQUIRED`, `IS_READONLY`, `IS_HIDDEN`, `DATA_TYPE_ID`, `FORMAT_TYPE_ID`, `MAX_LENGHT`, `MAX_WORD`, `DATE_CREATION`, `DATE_UPDATE`, `USER_CREATION`, `USER_UPDATE`) VALUES
(1, 3, 'p003.s2.name', 1, 1, 'Y', 'Y', 'N', 'N', NULL, 2, 10, 0, NULL, NULL, NULL, NULL),
(2, 3, 'p003.s1.name', 5, 2, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 3, 'p003.s1.description', 5, 3, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 3, 'p003.s1.type', 5, 1, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 3, 'p003.s1.amount', 5, 4, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 3, 'p003.s1', 4, 1, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 3, 'p003.s1.edit', 5, 6, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 3, 'p003.s1.delete', 5, 7, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 3, 'p003.s2.type', 3, 1, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL),
(11, 3, 'p003.s2.description', 2, 3, 'Y', 'Y', 'Y', 'N', NULL, NULL, 100, 30, NULL, NULL, NULL, NULL),
(12, 3, 'p003.s2.amount', 1, 4, 'Y', 'Y', 'N', 'N', 3, 2, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 6, 'reception.s1', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 6, 'reception.s2.name', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 6, 'reception.s2.description', 1, 3, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 6, 'reception.s2.supplier', 1, 4, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 6, 'reception.s2.deadline', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 6, 'reception.s2.souche', 1, 6, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 6, 'reception.s2.amount', 1, 7, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 6, 'reception.s2.deposit', 1, 8, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 6, 'reception.s1.id', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 6, 'reception.s1.name', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 6, 'reception.s1.souche', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 6, 'reception.s1.supplierId', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 6, 'reception.s1.amount', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 6, 'reception.s1.deadline', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 6, 'reception.s1.option', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 6, 'reception.s1.edit', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 6, 'reception.s1.delete', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 4, 'supplier.s1', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 4, 'supplier.s1.country', 1, 3, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 4, 'supplier.s1.city', 1, 4, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 4, 'supplier.s1.companyName', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 4, 'supplier.s1.cellPhone', 1, 6, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 4, 'supplier.s1.fixedPhone', 1, 7, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, 4, 'supplier.s1.adress', 1, 8, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 4, 'supplier.s1.email', 1, 9, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 4, 'supplier.s1.type', 1, 10, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, 4, 'supplier.s1.edit', 1, 11, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 4, 'supplier.s1.delete', 1, 12, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 4, 'supplier.s1.add', 1, 13, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 4, 'supplier.s2.firstName', 1, 14, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 4, 'supplier.s2.lastName', 1, 15, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 4, 'supplier.s2.type', 1, 16, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 4, 'supplier.s2.country', 1, 17, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 4, 'supplier.s2.city', 1, 18, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 4, 'supplier.s2.companyName', 1, 19, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, 4, 'supplier.s2.cellPhone', 1, 20, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 4, 'supplier.s2.fixedPhone', 1, 21, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 4, 'supplier.s2.adress', 1, 22, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 4, 'supplier.s2.email', 1, 23, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 4, 'product.s2.ative', 1, 24, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 4, 'supplier.s2.save', 1, 25, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 4, 'supplier.s1.option', 1, 25, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 5, 'product.s1', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 5, 'product.s1.designation', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 5, 'product.s1.quantity', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 5, 'product.s1.reference', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 5, 'product.s1.pricesale', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 5, 'product.s1.size', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 5, 'product.s1.status', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 5, 'product.s1.family', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 5, 'product.s1.color', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 5, 'product.s1.option', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 5, 'product.s1.edit', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 5, 'product.s1.add', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 5, 'product.s1.delete', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(73, 5, 'product.s2.designation', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, 5, 'product.s2.quantity', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(75, 5, 'product.s2.reference', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(76, 5, 'p005.s2.priceSale', 1, 0, 'Y', 'N', 'N', 'N', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(77, 5, 'product.s2.active', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(78, 5, 'product.s2.status', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(79, 5, 'product.s2.size', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(80, 5, 'product.s2.family', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(81, 5, 'product.s2.color', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(82, 5, 'product.s2.save', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(83, 5, 'product.s1.priceBuy', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(84, 5, 'p005.s2.priceBuy', 1, 0, 'Y', 'N', 'N', 'N', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(85, 4, 'supplier.s1.postCode', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(86, 4, 'supplier.s2.postCode', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(87, 13, 'bonLivraison.s1.description', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(88, 13, 'bonLivraison.s1.customer', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(89, 6, 'reception.s2.sizes', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(90, 21, 'basicParameter.s1', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(91, 21, 'basicParameter.s2.name', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(92, 21, 'basicParameter.s2.description', 1, 3, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(93, 21, 'basicParameter.s2.value', 1, 4, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(94, 21, 'basicParameter.s2.basicParameterType', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(95, 21, 'basicParameter.s1', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(96, 21, 'basicParameter.s2.name', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(97, 21, 'basicParameter.s2.description', 1, 3, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(98, 21, 'basicParameter.s2.value', 1, 4, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(99, 21, 'basicParameter.s2.basicParameterType', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(100, 23, 'moduleParameter.s1', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(101, 23, 'moduleParameter.s2.name', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(102, 23, 'moduleParameter.s2.description', 1, 3, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(103, 23, 'moduleParameter.s2.value', 1, 4, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(104, 23, 'moduleParameter.s2.moduleParameterType', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(105, 25, 'parameterClient.s1', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(106, 25, 'parameterClient.s2', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(107, 25, 'parameterClient.s3.client', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(108, 25, 'parameterClient.s3.parameter', 1, 3, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(109, 25, 'parameterClient.s3.value', 1, 4, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(110, 22, 'parameter.s1', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(111, 22, 'parameter.s2.name', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(112, 22, 'parameter.s2.description', 1, 3, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(113, 22, 'parameter.s2.value', 1, 4, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(114, 22, 'parameter.s2.parameterType', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(115, 27, 'pageParameterModule.s1', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(116, 27, 'pageParameterModule.s2', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(117, 27, 'pageParameterModule.s3', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(118, 27, 'pageParameterModule.s4', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(119, 27, 'pageParameterModule.s5.module', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(120, 27, 'pageParameterModule.s5.pageParameter', 1, 3, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(121, 27, 'pageParameterModule.s5.value', 1, 4, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(122, 24, 'pageParameter.s1', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(123, 24, 'pageParameter.s2.name', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(124, 24, 'pageParameter.s2.description', 1, 3, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(125, 24, 'pageParameter.s2.value', 1, 4, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(126, 24, 'pageParameter.s2.pageParameterType', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(127, 24, 'pageParameter.s2.page', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(128, 26, 'moduleParameterClient.s1', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(129, 26, 'moduleParameterClient.s2', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(130, 26, 'moduleParameterClient.s3', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(131, 26, 'moduleParameterClient.s4.module', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(132, 26, 'moduleParameterClient.s4.parameterModule', 1, 3, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(133, 26, 'moduleParameterClient.s4.value', 1, 4, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(134, 6, 'p006.s2.code', 1, 1, 'Y', 'N', 'Y', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(135, 6, 'p006.s2.deadline', 1, 1, 'Y', 'N', 'N', 'N', 5, 8, NULL, NULL, NULL, NULL, NULL, NULL),
(136, 5, 'p005.s2.reference', NULL, NULL, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(137, 5, 'p005.s2.designation', NULL, NULL, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(138, 5, 'p005.s2.priceBuy', NULL, NULL, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(139, 5, 'p005.s2.family', NULL, NULL, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_page_parameter`
--

CREATE TABLE `pm_page_parameter` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext CHARACTER SET latin1,
  `DESCRIPTION` text CHARACTER SET latin1,
  `DEFAULT_VALUE` text,
  `PAGE_ID` bigint(20) DEFAULT NULL,
  `PAGE_PARAMETER_TYPE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_page_parameter`
--

INSERT INTO `pm_page_parameter` (`ID`, `NAME`, `DESCRIPTION`, `DEFAULT_VALUE`, `PAGE_ID`, `PAGE_PARAMETER_TYPE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'titre de l''application', 'title of application', 'My Stock Management', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'visiblité de form add edit ', NULL, 'no', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(4, '1', '2', '3', 20, 1, 'Y', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_page_parameter_module`
--

CREATE TABLE `pm_page_parameter_module` (
`ID` bigint(20) NOT NULL,
  `PAGE_PARAMETER_ID` bigint(20) DEFAULT NULL,
  `VALUE` longtext,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pm_page_parameter_type`
--

CREATE TABLE `pm_page_parameter_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_page_parameter_type`
--

INSERT INTO `pm_page_parameter_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'pm_page_parameter_type', 'pm_page_parameter_type', NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_page_type`
--

CREATE TABLE `pm_page_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_page_type`
--

INSERT INTO `pm_page_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'TYpe 1', NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'type autre ', 'desc', 2, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_pdf`
--

CREATE TABLE `pm_pdf` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `PDF_TYPE_ID` bigint(20) DEFAULT NULL,
  `IN_DEV` char(1) DEFAULT 'N',
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_pdf`
--

INSERT INTO `pm_pdf` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `PDF_TYPE_ID`, `IN_DEV`, `ACTIVE`, `USER_CREATION`, `DATE_CREATION`, `USER_UPDATE`, `DATE_UPDATE`) VALUES
(1, 'Afficher la liste des commandes ', NULL, 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(2, 'Afficher le détails d''une commande', NULL, 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(3, 'Afficher la liste des réceptions', '1', NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(4, 'Afficher le détails d''une récéption', '1', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(5, 'Afficher la liste des ventes ', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(6, 'Afficher le détails d''une vente', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(7, 'Afficher la liste des avoirs', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(8, 'Afficher le détails d''une avoir', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_pdf_attribute`
--

CREATE TABLE `pm_pdf_attribute` (
`ID` bigint(20) NOT NULL,
  `PDF_ID` bigint(20) DEFAULT NULL,
  `INF_ITEM_CODE` varchar(255) DEFAULT NULL,
  `PM_COMPONENT_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `IS_HIDDEN` char(1) DEFAULT 'N',
  `DATA_TYPE_ID` bigint(20) DEFAULT NULL,
  `FORMAT_TYPE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `DATE_UPDATE` varchar(45) DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pm_pdf_attribute_exclud`
--

CREATE TABLE `pm_pdf_attribute_exclud` (
`ID` bigint(20) NOT NULL,
  `PDF_ID` bigint(20) DEFAULT NULL,
  `INF_ITEM_CODE` tinytext,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pm_pdf_parameter`
--

CREATE TABLE `pm_pdf_parameter` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext CHARACTER SET latin1,
  `DESCRIPTION` text CHARACTER SET latin1,
  `DEFAULT_VALUE` text,
  `PDF_ID` bigint(20) DEFAULT NULL,
  `PDF_PARAMETER_TYPE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pm_pdf_parameter_module`
--

CREATE TABLE `pm_pdf_parameter_module` (
`ID` bigint(20) NOT NULL,
  `PDF_PARAMETER_ID` bigint(20) DEFAULT NULL,
  `VALUE` longtext,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pm_pdf_parameter_type`
--

CREATE TABLE `pm_pdf_parameter_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pm_pdf_type`
--

CREATE TABLE `pm_pdf_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_pdf_type`
--

INSERT INTO `pm_pdf_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'type simple', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_validation_type`
--

CREATE TABLE `pm_validation_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `PARAM_NUMBER` bigint(20) DEFAULT NULL,
  `ERROR_MESSAGE` tinytext,
  `HELP` text CHARACTER SET latin1,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_validation_type`
--

INSERT INTO `pm_validation_type` (`ID`, `NAME`, `DESCRIPTION`, `PARAM_NUMBER`, `ERROR_MESSAGE`, `HELP`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'required', 'required', 0, 'validation.v1.required', 'validation.v1.required', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'maxlenght', 'maxlenght', 1, 'validation.v1.maxlenght', 'validation.v1.maxlenght', 2, 'Y', NULL, NULL, NULL, NULL),
(3, 'maxWord', 'maxWord', 1, 'validation.v1.maxWord', 'validation.v1.maxWord', 3, 'Y', NULL, NULL, NULL, NULL),
(4, 'dataType', 'dataType : ne sera pas changé pas un locataire', 1, 'validation.v1.dataType', 'validation.v1.dataType', 4, 'Y', NULL, NULL, NULL, NULL),
(5, 'formatType', 'formatType', 1, 'validation.v1.formatType', 'validation.v1.formatType', 5, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_advanced`
--

CREATE TABLE `sm_advanced` (
`ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `CUSTOMER_ID` bigint(20) DEFAULT NULL,
  `ADVANCED_STATUS_ID` bigint(20) DEFAULT NULL,
  `AMOUNT` double DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_advanced_status`
--

CREATE TABLE `sm_advanced_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_bank_type`
--

CREATE TABLE `sm_bank_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_check`
--

CREATE TABLE `sm_check` (
`ID` bigint(20) NOT NULL,
  `BANK_ID` bigint(20) DEFAULT NULL,
  `NAME_PERSON` tinytext CHARACTER SET latin1,
  `NUMBER` tinytext CHARACTER SET latin1,
  `ACCOUNT` tinytext CHARACTER SET latin1,
  `PHONE` tinytext CHARACTER SET latin1,
  `PAYABLE_IN` tinytext CHARACTER SET latin1,
  `AMOUNT` double DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_customer`
--

CREATE TABLE `sm_customer` (
`ID` bigint(20) NOT NULL,
  `FIRST_NAME` tinytext CHARACTER SET latin1,
  `LAST_NAME` tinytext CHARACTER SET latin1,
  `INF_COUNTRY_ID` bigint(20) DEFAULT NULL,
  `INF_CITY_ID` bigint(20) DEFAULT NULL,
  `CELL_PHONE` tinytext CHARACTER SET latin1,
  `FIXED_PHONE` tinytext CHARACTER SET latin1,
  `ADRESS` tinytext CHARACTER SET latin1,
  `SECONDARY_ADDRESS` tinytext,
  `MAIL` tinytext CHARACTER SET latin1,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `CUSTOMER_TYPE_ID` bigint(20) DEFAULT NULL,
  `CUSTOMER_CATEGORY_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `COMPANY_NAME` tinytext,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `ZIP_CODE` tinytext,
  `SHORT_LABEL` tinytext,
  `FULL_LABEL` text,
  `WEB_SITE` tinytext,
  `NOTE` text,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `IDENTIFICATION` tinytext,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_customer`
--

INSERT INTO `sm_customer` (`ID`, `FIRST_NAME`, `LAST_NAME`, `INF_COUNTRY_ID`, `INF_CITY_ID`, `CELL_PHONE`, `FIXED_PHONE`, `ADRESS`, `SECONDARY_ADDRESS`, `MAIL`, `CLT_MODULE_ID`, `CUSTOMER_TYPE_ID`, `CUSTOMER_CATEGORY_ID`, `SORT_KEY`, `COMPANY_NAME`, `ACTIVE`, `ZIP_CODE`, `SHORT_LABEL`, `FULL_LABEL`, `WEB_SITE`, `NOTE`, `DATE_CREATION`, `USER_CREATION`, `IDENTIFICATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(4, 'AHmed', 'ML', 1, 1, '+212 6 66 93 98', '+212 6 66 93 98', 'SYBA 87', NULL, 'abdessamad.hallal@evision.ca', 1, 1, NULL, 1, 'Société 1', 'Y', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1),
(5, 'Abdessamad2', 'HALLAL2', 1, 1, '+212 5 76 87 87', '+212 5 76 87 87', 'SYBA 5 N 879', NULL, 'abdessamad@hallal.com', 1, 1, NULL, NULL, 'Société 2', 'Y', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1),
(6, 'Abdessamad3', 'HALLAL3', 1, 1, '+212 5 76 87 87', '+212 5 76 87 87', 'SYBA 5 N 879', NULL, 'abdessamad@hallal.com', 1, 1, NULL, NULL, 'Société 3', 'Y', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1),
(10, 'Abdessamad', 'HALLAL', 1, 1, '+212 5 76 87 87', '+212 5 76 87 87', 'aaaa', NULL, 'abdessamad12@hallal.com', 1, 1, NULL, NULL, 'Société  4', 'Y', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL),
(11, ' Prénom ', 'Nom ', 1, 1, ' Télé Portable ', ' Télé Fixe ', ' Adresse 2 ', 'Adresse ', ' E-Mail ', 1, 1, 2, NULL, 'Société 5', 'Y', ' Code postale ', ' Libelle court ', ' Libelle complet ', ' Site web ', ' Note interne ', NULL, 2, NULL, NULL, 2),
(16, '1', '2', 1, 1, '4', '5', '7', '6', '3', 1, 1, 1, NULL, 'Société 6', 'N', '9', '10', '11', '12', '13', NULL, 1, NULL, NULL, 1),
(17, 'a', 'b', 2, 4, 's', 'd', 's', 's', 'c', 1, 1, 2, NULL, 's', 'Y', 's', 's', 'f', 'f', 'f', NULL, 1, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `sm_customer_category`
--

CREATE TABLE `sm_customer_category` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_customer_category`
--

INSERT INTO `sm_customer_category` (`ID`, `NAME`, `DESCRIPTION`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Category 1', NULL, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'Category 2', NULL, 1, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_customer_type`
--

CREATE TABLE `sm_customer_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_customer_type`
--

INSERT INTO `sm_customer_type` (`ID`, `NAME`, `DESCRIPTION`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Particulier', 'Particulier', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Société', 'Société', 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_deposit`
--

CREATE TABLE `sm_deposit` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_deposit`
--

INSERT INTO `sm_deposit` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'depôt 1', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'depôt', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_expense`
--

CREATE TABLE `sm_expense` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `AMOUNT` double DEFAULT NULL,
  `EXPENSE_TYPE_ID` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_expense`
--

INSERT INTO `sm_expense` (`ID`, `NAME`, `DESCRIPTION`, `AMOUNT`, `EXPENSE_TYPE_ID`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(23, 'Libelle1', 'Description1', 11, 1, 1, NULL, 'Y', NULL, 1, NULL, NULL),
(29, 'le petit déjounée ', 'avec ... ', 50, 1, 1, NULL, 'Y', NULL, 1, NULL, 1),
(31, 'aaa', 'bb', 3344, 1, 1, NULL, 'Y', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_expense_type`
--

CREATE TABLE `sm_expense_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_expense_type`
--

INSERT INTO `sm_expense_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Générale', 'Générale', 1, 3, 'Y', NULL, NULL, NULL, NULL),
(62, 'Femme de menage', 'Femme de menage', 2, 3, 'Y', NULL, NULL, NULL, NULL),
(63, 'Transport', 'Transport', 3, 3, 'Y', NULL, NULL, NULL, NULL),
(64, 'Eléctrisité', 'Eléctrisité', 4, 3, 'Y', NULL, NULL, NULL, NULL),
(65, 'Restaurant', 'Restaurant', 5, 3, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_order`
--

CREATE TABLE `sm_order` (
`ID` bigint(20) NOT NULL,
  `NO_SEQ` bigint(20) DEFAULT NULL,
  `REFERENCE` tinytext,
  `CUSTOMER_ID` bigint(20) DEFAULT NULL,
  `PAYMENT_METHOD_ID` bigint(20) DEFAULT NULL,
  `CHECK_ID` bigint(20) DEFAULT NULL,
  `DELIVERY` tinytext,
  `NOTE` text,
  `ORDER_STATUS_ID` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_order`
--

INSERT INTO `sm_order` (`ID`, `NO_SEQ`, `REFERENCE`, `CUSTOMER_ID`, `PAYMENT_METHOD_ID`, `CHECK_ID`, `DELIVERY`, `NOTE`, `ORDER_STATUS_ID`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(44, 1, 'V1', 4, NULL, NULL, NULL, 'eeee', 3, 1, NULL, '2015-12-24 22:28:45', NULL, NULL, NULL),
(45, 2, 'V2', 5, NULL, NULL, NULL, 'ddd', 2, 1, NULL, '2015-12-24 22:30:27', NULL, NULL, NULL),
(46, 3, 'V3', 6, NULL, NULL, NULL, 'dsqdsq', 2, 1, NULL, '2015-12-24 23:09:58', NULL, NULL, NULL),
(47, 4, 'V4', 10, NULL, NULL, NULL, 'note1', 3, 1, NULL, '2015-12-24 23:37:30', NULL, NULL, NULL),
(48, 5, 'V5', 10, NULL, NULL, NULL, 'AEE', 3, 1, NULL, '2015-12-24 23:41:49', NULL, NULL, NULL),
(49, 6, 'V6', NULL, NULL, NULL, NULL, '', 1, 1, NULL, '2015-12-24 23:49:17', NULL, NULL, NULL),
(50, 7, 'V7', 5, NULL, NULL, NULL, 'gggg', 1, 1, NULL, '2015-12-24 23:52:11', NULL, NULL, NULL),
(51, 8, 'V8', NULL, NULL, NULL, NULL, '', 1, 1, NULL, '2015-12-24 23:52:57', NULL, NULL, NULL),
(52, 9, 'V9', 6, NULL, NULL, NULL, 'abc111', 1, 1, NULL, '2015-12-24 23:54:06', NULL, NULL, NULL),
(53, 10, 'V10', 5, NULL, NULL, NULL, 'centre', 3, 1, NULL, '2015-12-24 23:59:09', NULL, NULL, NULL),
(54, 11, 'V11', 6, NULL, NULL, NULL, 'centre d''appelle', 3, 1, NULL, '2015-12-27 18:55:51', NULL, NULL, NULL);

--
-- Déclencheurs `sm_order`
--
DELIMITER //
CREATE TRIGGER `TRG_BI_SM_ORDER` BEFORE INSERT ON `sm_order`
 FOR EACH ROW BEGIN

    SET NEW.DATE_CREATION = NOW();
    
    IF EXISTS(SELECT * FROM SM_ORDER WHERE CLT_MODULE_ID = NEW.CLT_MODULE_ID) THEN
		SET NEW.NO_SEQ = (SELECT MAX(NO_SEQ) + 1 FROM SM_ORDER WHERE CLT_MODULE_ID = NEW.CLT_MODULE_ID);
    else
		SET NEW.NO_SEQ = 1;
	END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `sm_order_line`
--

CREATE TABLE `sm_order_line` (
`ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `DESIGNATION` text,
  `ORDER_ID` bigint(20) DEFAULT NULL,
  `PROMOTION_ID` bigint(20) DEFAULT NULL,
  `NEGOTIATE_PRICE_SALE` double DEFAULT NULL,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_order_line`
--

INSERT INTO `sm_order_line` (`ID`, `PRODUCT_ID`, `DESIGNATION`, `ORDER_ID`, `PROMOTION_ID`, `NEGOTIATE_PRICE_SALE`, `QUANTITY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(4, NULL, 'Pc portable1', 44, NULL, 100, 1, NULL, NULL, NULL, NULL, NULL),
(5, 23, 'Note Portable', 44, NULL, 500, 1, NULL, NULL, NULL, NULL, NULL),
(6, 23, 'Sumsung duos', 45, NULL, 300, 1, NULL, NULL, NULL, NULL, NULL),
(7, 2, 'Pc portable1', 46, NULL, 100, 1, NULL, NULL, NULL, NULL, NULL),
(8, NULL, 'Nokia', 47, NULL, 200, 1, NULL, NULL, NULL, NULL, NULL),
(10, NULL, 'Sumsung duos', 48, NULL, 300, 1, NULL, NULL, NULL, NULL, NULL),
(11, NULL, 'Nokia', 49, NULL, 200, 1, NULL, NULL, NULL, NULL, NULL),
(13, 2, 'Pc portable1', 50, NULL, 100, 1, NULL, NULL, NULL, NULL, NULL),
(14, 18, 'Mac OS', 51, NULL, 300, 1, NULL, NULL, NULL, NULL, NULL),
(15, 18, 'Mac OS', 52, NULL, 300, 1, NULL, NULL, NULL, NULL, NULL),
(16, 2, 'Pc portable1', 53, NULL, 100, 1, NULL, NULL, NULL, NULL, NULL),
(17, 18, 'Mac OS', 53, NULL, 300, 1, NULL, NULL, NULL, NULL, NULL),
(18, 3, 'TEST 11111', 53, NULL, 4567, 222, NULL, NULL, NULL, NULL, NULL),
(19, 4, 'Sumsung duos', 53, NULL, 300, 22, NULL, NULL, NULL, NULL, NULL),
(20, 4, 'Note Portable', 53, NULL, 111.33, 222, NULL, NULL, NULL, NULL, NULL),
(21, 2, 'Pc portable1', 53, NULL, 100, 4567, NULL, NULL, NULL, NULL, NULL),
(22, 2, 'Pc portable1', 52, NULL, 100, 1, NULL, NULL, NULL, NULL, NULL),
(23, 18, 'Mac OS', 47, NULL, 300, 1, NULL, NULL, NULL, NULL, NULL),
(24, 23, 'Note Portable', 48, NULL, 500, 1, NULL, NULL, NULL, NULL, NULL),
(25, 4, 'Sumsung duos', 46, NULL, 300, 1, NULL, NULL, NULL, NULL, NULL),
(26, 2, 'Pc portable1', 46, NULL, 100, 1, NULL, NULL, NULL, NULL, NULL),
(27, 2, 'Pc portable1', 54, NULL, 100, 9, NULL, NULL, NULL, NULL, NULL),
(28, 3, 'Nokia', 54, NULL, 200, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_order_status`
--

CREATE TABLE `sm_order_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_order_status`
--

INSERT INTO `sm_order_status` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'En cours de saisie', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'Transmis', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Validé', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'Non Validé', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_order_supplier`
--

CREATE TABLE `sm_order_supplier` (
`ID` bigint(20) NOT NULL,
  `NO_SEQ` bigint(20) DEFAULT NULL,
  `REFERENCE` tinytext,
  `NOTE` text,
  `ORDER_SUPPLIER_STATUS_ID` bigint(20) DEFAULT NULL,
  `SUPPLIER_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_order_supplier`
--

INSERT INTO `sm_order_supplier` (`ID`, `NO_SEQ`, `REFERENCE`, `NOTE`, `ORDER_SUPPLIER_STATUS_ID`, `SUPPLIER_ID`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(6, 3, '111', '222', 1, 16, NULL, 1, '2015-11-22 15:46:52', NULL, NULL, NULL),
(7, 4, 'kaka', 'baba', 1, 1, NULL, 1, '2015-11-22 15:49:07', NULL, NULL, NULL),
(9, 6, 'R0012', 'aaaaa', 1, 1, NULL, 1, '2015-11-28 14:47:48', NULL, NULL, NULL),
(12, 7, 'aa', '', 1, 15, NULL, 1, '2015-11-29 00:06:57', NULL, NULL, NULL),
(19, 8, 'C8', 'dddd', 1, 1, NULL, 1, '2015-11-29 00:26:10', NULL, NULL, NULL),
(20, 9, 'C9', '', 1, NULL, NULL, 1, '2015-11-29 00:27:32', NULL, NULL, NULL),
(23, 10, 'C10', 'ddsqdsq', 3, 1, NULL, 1, '2015-11-29 13:45:02', NULL, NULL, NULL),
(24, 11, 'TEST', 'onte', 3, 1, NULL, 1, '2015-11-29 14:50:22', NULL, NULL, NULL),
(26, 13, 'C13', '', 3, 15, NULL, 1, '2015-12-27 18:56:24', NULL, NULL, NULL),
(27, 14, 'C14', 'note 1', 1, 1, NULL, 1, '2016-03-08 19:10:14', NULL, NULL, NULL);

--
-- Déclencheurs `sm_order_supplier`
--
DELIMITER //
CREATE TRIGGER `TRG_BI_SM_ORDER_SUPPLIER` BEFORE INSERT ON `sm_order_supplier`
 FOR EACH ROW BEGIN

    SET NEW.DATE_CREATION = NOW();
    
    IF EXISTS(SELECT * FROM SM_ORDER_SUPPLIER WHERE CLT_MODULE_ID = NEW.CLT_MODULE_ID) THEN
		SET NEW.NO_SEQ = (SELECT MAX(NO_SEQ) + 1 FROM SM_ORDER_SUPPLIER WHERE CLT_MODULE_ID = NEW.CLT_MODULE_ID);
    else
		SET NEW.NO_SEQ = 1;
	END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `sm_order_supplier_line`
--

CREATE TABLE `sm_order_supplier_line` (
`ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `DESIGNATION` text,
  `UNIT_PRICE_SALE` double DEFAULT NULL,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `TVA` double DEFAULT NULL,
  `REMISE` double DEFAULT NULL,
  `PROMOTION_ID` bigint(20) DEFAULT NULL,
  `ORDER_SUPPLIER_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_order_supplier_line`
--

INSERT INTO `sm_order_supplier_line` (`ID`, `PRODUCT_ID`, `DESIGNATION`, `UNIT_PRICE_SALE`, `QUANTITY`, `TVA`, `REMISE`, `PROMOTION_ID`, `ORDER_SUPPLIER_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(13, 18, 'Mac OS', 300, 32, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL),
(14, 4, 'Sumsung duos1199', 300, 32, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL),
(15, NULL, 'Note Portable111', 900, 32, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL),
(19, 3, 'Nokia', 200, 32, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL),
(20, 3, 'Nokia1', 200, 112, NULL, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL),
(31, 3, 'Nokia', 200, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 19, 'HP Portable', 400, 1, NULL, NULL, NULL, 19, NULL, NULL, NULL, NULL, NULL),
(38, 3, 'Note Portable', 500, 60, NULL, NULL, NULL, 23, NULL, NULL, NULL, NULL, NULL),
(39, 2, 'Pc portable1', 100, 9, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL),
(41, 2, 'Pc portable1', 100, 7, NULL, NULL, NULL, 26, NULL, NULL, NULL, NULL, NULL),
(42, 3, 'Nokia', 200, 180, NULL, NULL, NULL, 26, NULL, NULL, NULL, NULL, NULL),
(43, 2, 'Pc portable1', 100, 3, NULL, NULL, NULL, 27, NULL, NULL, NULL, NULL, NULL),
(44, 2, 'Sumsung duos', 300, 4, NULL, NULL, NULL, 27, NULL, NULL, NULL, NULL, NULL),
(45, 23, 'du Text', 500, 1, NULL, NULL, NULL, 27, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_order_supplier_status`
--

CREATE TABLE `sm_order_supplier_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_order_supplier_status`
--

INSERT INTO `sm_order_supplier_status` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'En cours', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'Submit', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Valider', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'Non Validé', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_payment_method`
--

CREATE TABLE `sm_payment_method` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_payment_method`
--

INSERT INTO `sm_payment_method` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Chèque', NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'Prélèvement', NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Espèces', NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'Carte bancaire', NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(5, 'Traite', NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(6, 'Autre', NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_product`
--

CREATE TABLE `sm_product` (
`ID` bigint(20) NOT NULL,
  `NO_SEQ` bigint(20) DEFAULT NULL,
  `REFERENCE` tinytext CHARACTER SET latin1,
  `DESIGNATION` tinytext CHARACTER SET latin1,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `PRICE_SALE` double DEFAULT NULL,
  `PRICE_BUY` double DEFAULT NULL,
  `PRODUCT_GROUP_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_FAMILY_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_SIZE_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_COLOR_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_STATUS_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_TYPE_ID` bigint(20) DEFAULT NULL,
  `THRESHOLD` bigint(20) DEFAULT NULL,
  `NOTE` text,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_product`
--

INSERT INTO `sm_product` (`ID`, `NO_SEQ`, `REFERENCE`, `DESIGNATION`, `QUANTITY`, `PRICE_SALE`, `PRICE_BUY`, `PRODUCT_GROUP_ID`, `PRODUCT_FAMILY_ID`, `PRODUCT_SIZE_ID`, `PRODUCT_COLOR_ID`, `PRODUCT_STATUS_ID`, `PRODUCT_TYPE_ID`, `THRESHOLD`, `NOTE`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(2, 0, 'P001', 'Pc portable1', 214, 100, 111, 3, 4, 47, 1, 1, 1, NULL, 'Note interne', 'N', 1, NULL, 2, NULL, 2),
(3, 0, 'P002', 'Nokia', 200, 200, 222, 2, 3, 50, 2, 1, 2, NULL, 'Note interne', 'Y', 1, NULL, 2, NULL, NULL),
(4, 0, 'P003', 'Sumsung duos', -2, 300, 333, 1, 2, 47, 8, 1, 2, NULL, 'Intene', 'Y', 1, NULL, 2, NULL, 1),
(18, 2, 'P004', 'Mac OS', 0, 300, 444, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 1, '2015-11-13 23:17:10', 1, NULL, NULL),
(19, 3, 'P005', 'HP Portable', 12, 400, 555, 2, 3, NULL, NULL, NULL, 1, NULL, '', 'Y', 1, '2015-11-14 13:00:09', 1, NULL, 1),
(23, 7, 'P006', 'Note Portable', -1, 500, 666, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 1, '2015-11-14 13:43:19', 1, NULL, NULL),
(25, 8, '6WKWFZ', '', NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, 1, NULL, '', 'N', 1, '2016-02-09 22:28:26', 1, NULL, NULL),
(26, 9, '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'Y', 1, '2016-02-09 22:32:45', 1, NULL, NULL);

--
-- Déclencheurs `sm_product`
--
DELIMITER //
CREATE TRIGGER `TRG_BI_SM_PRODUCT` BEFORE INSERT ON `sm_product`
 FOR EACH ROW BEGIN

    SET NEW.DATE_CREATION = NOW();
    
    IF EXISTS(SELECT * FROM sm_product WHERE CLT_MODULE_ID = NEW.CLT_MODULE_ID) THEN
		SET NEW.NO_SEQ = (SELECT MAX(NO_SEQ) + 1 FROM sm_product WHERE CLT_MODULE_ID = NEW.CLT_MODULE_ID);
    else
		SET NEW.NO_SEQ = 1;
	END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `sm_product_color`
--

CREATE TABLE `sm_product_color` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `HEX` tinytext CHARACTER SET latin1,
  `RGB` tinytext CHARACTER SET latin1,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_product_color`
--

INSERT INTO `sm_product_color` (`ID`, `NAME`, `HEX`, `RGB`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `CLT_MODULE_ID`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Blanc1', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(2, 'Bleu', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(3, 'Brun', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(4, 'Gris', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(5, 'Noir', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(6, 'Or', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(7, 'Orange', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(8, 'Rose', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(9, 'Rouge', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(10, 'Vert', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(11, 'Violet', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(12, 'New Color11', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(13, 'bbb', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_product_family`
--

CREATE TABLE `sm_product_family` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `PRODUCT_GROUP_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_product_family`
--

INSERT INTO `sm_product_family` (`ID`, `NAME`, `DESCRIPTION`, `PRODUCT_GROUP_ID`, `SORT_KEY`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'PC', 'family 2', 1, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'Electro', NULL, 1, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(3, 'TV', NULL, 2, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(4, 'Tools', NULL, 3, NULL, 'Y', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_product_group`
--

CREATE TABLE `sm_product_group` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_product_group`
--

INSERT INTO `sm_product_group` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Groupe 01', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Groupe 02', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(3, 'Groupe 03', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_product_size`
--

CREATE TABLE `sm_product_size` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_product_size`
--

INSERT INTO `sm_product_size` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(47, '37', NULL, NULL, 1, 'Y', NULL, 1, NULL, NULL),
(48, '38', NULL, NULL, 1, 'Y', NULL, 1, NULL, NULL),
(49, '39', NULL, NULL, 1, 'Y', NULL, 1, NULL, NULL),
(50, '40', NULL, NULL, 1, 'Y', NULL, 1, NULL, NULL),
(51, '32', NULL, NULL, 1, 'Y', NULL, 1, NULL, NULL),
(52, '36', NULL, NULL, 1, 'Y', NULL, 1, NULL, NULL),
(53, '41', NULL, NULL, 1, 'Y', NULL, 1, NULL, NULL),
(54, '67', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(55, '78', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(56, '89', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(57, '98', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(58, '17', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(59, '23', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(60, '35', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(61, '42', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_product_status`
--

CREATE TABLE `sm_product_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_product_status`
--

INSERT INTO `sm_product_status` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'status 01', 'desc', 1, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'status 02', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_product_type`
--

CREATE TABLE `sm_product_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` varchar(45) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_product_type`
--

INSERT INTO `sm_product_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Type 01', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Type 02', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(3, 'Type 03', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_promotion`
--

CREATE TABLE `sm_promotion` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `PROMOTION_TYPE_ID` bigint(20) DEFAULT NULL,
  `PRODUCT` bigint(20) DEFAULT NULL,
  `VALUE` double DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_promotion_type`
--

CREATE TABLE `sm_promotion_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_reception`
--

CREATE TABLE `sm_reception` (
`ID` bigint(20) NOT NULL,
  `NO_SEQ` bigint(20) DEFAULT NULL,
  `REFERENCE` tinytext,
  `SOUCHE` tinytext,
  `SUPPLIER_ID` bigint(20) DEFAULT NULL,
  `DEPOSIT_ID` bigint(20) DEFAULT NULL,
  `RECEPTION_STATUS_ID` bigint(20) DEFAULT NULL,
  `DEADLINE` datetime DEFAULT NULL,
  `NOTE` text,
  `TVA` double DEFAULT NULL,
  `DELIVERY` tinytext,
  `ORDER_SUPPLIER_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `EXPIRATION_DATE` datetime DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_reception`
--

INSERT INTO `sm_reception` (`ID`, `NO_SEQ`, `REFERENCE`, `SOUCHE`, `SUPPLIER_ID`, `DEPOSIT_ID`, `RECEPTION_STATUS_ID`, `DEADLINE`, `NOTE`, `TVA`, `DELIVERY`, `ORDER_SUPPLIER_ID`, `ACTIVE`, `EXPIRATION_DATE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(43, 1, 'R1', NULL, 1, NULL, 3, NULL, 'centre', NULL, NULL, NULL, NULL, NULL, 1, '2015-12-13 17:59:36', NULL, NULL, NULL),
(44, 2, 'R2', NULL, NULL, NULL, 1, NULL, '', NULL, NULL, NULL, NULL, NULL, 1, '2015-12-13 18:32:47', NULL, NULL, NULL),
(45, 3, 'R3', NULL, NULL, NULL, 2, NULL, '', NULL, NULL, NULL, NULL, NULL, 1, '2015-12-13 22:28:11', NULL, NULL, NULL),
(46, 4, 'R4', NULL, 16, NULL, 3, NULL, 'centre', NULL, NULL, NULL, NULL, NULL, 1, '2015-12-27 18:54:30', NULL, NULL, NULL),
(47, 5, 'R5', NULL, 15, NULL, 3, NULL, 'Ok merci', NULL, NULL, NULL, NULL, NULL, 1, '2015-12-27 18:57:14', NULL, NULL, NULL),
(48, 6, 'B97C1K', NULL, 1, NULL, 3, NULL, ' Note interne  11', NULL, NULL, NULL, NULL, NULL, 1, '2016-01-21 23:52:16', NULL, NULL, NULL),
(49, 7, '54TKNU', NULL, 1, NULL, 2, NULL, 'note', NULL, NULL, NULL, NULL, NULL, 1, '2016-02-09 22:17:24', NULL, NULL, NULL),
(50, 8, 'UDFPV6', NULL, 1, NULL, 1, NULL, 'fdsf', NULL, NULL, NULL, NULL, NULL, 1, '2016-03-06 19:38:26', NULL, NULL, NULL),
(51, 9, 'R9', NULL, 1, NULL, 3, NULL, 'note', NULL, NULL, NULL, NULL, NULL, 1, '2016-03-08 19:43:51', NULL, NULL, NULL);

--
-- Déclencheurs `sm_reception`
--
DELIMITER //
CREATE TRIGGER `TRG_BI_SM_RECEPTION` BEFORE INSERT ON `sm_reception`
 FOR EACH ROW BEGIN

    SET NEW.DATE_CREATION = NOW();
    
    IF EXISTS(SELECT * FROM sm_reception WHERE CLT_MODULE_ID = NEW.CLT_MODULE_ID) THEN
		SET NEW.NO_SEQ = (SELECT MAX(NO_SEQ) + 1 FROM sm_reception WHERE CLT_MODULE_ID = NEW.CLT_MODULE_ID);
    else
		SET NEW.NO_SEQ = 1;
	END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `sm_reception_line`
--

CREATE TABLE `sm_reception_line` (
`ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `DESIGNATION` tinytext,
  `UNIT_PRICE_BUY` double DEFAULT NULL,
  `REMISE` double DEFAULT NULL,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `TVA` double DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `RECEPTION_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_reception_line`
--

INSERT INTO `sm_reception_line` (`ID`, `PRODUCT_ID`, `DESIGNATION`, `UNIT_PRICE_BUY`, `REMISE`, `QUANTITY`, `TVA`, `ACTIVE`, `RECEPTION_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 2, 'Pc portable1', 100, NULL, 1, NULL, NULL, 43, NULL, NULL, NULL, NULL),
(2, 3, 'Nokia', 200, NULL, 1, NULL, NULL, 43, NULL, NULL, NULL, NULL),
(3, 4, 'Sumsung duos', 300, NULL, 1, NULL, NULL, 43, NULL, NULL, NULL, NULL),
(4, 18, 'Mac OS', 300, NULL, 1, NULL, NULL, 43, NULL, NULL, NULL, NULL),
(5, 19, 'HP Portable', 400, NULL, 1, NULL, NULL, 43, NULL, NULL, NULL, NULL),
(6, 23, 'Note Portable', 500, NULL, 1, NULL, NULL, 43, NULL, NULL, NULL, NULL),
(11, 2, 'Pc portable1', 100, NULL, 9, NULL, NULL, 45, NULL, NULL, NULL, NULL),
(12, 2, 'Pc portable1', 100, NULL, 9, NULL, NULL, 46, NULL, NULL, NULL, NULL),
(13, 3, 'Nokia', 200, NULL, 1, NULL, NULL, 46, NULL, NULL, NULL, NULL),
(14, 2, 'Pc portable1', 100, NULL, 7, NULL, NULL, 47, NULL, NULL, NULL, NULL),
(15, 3, 'Nokia', 200, NULL, 180, NULL, NULL, 47, NULL, NULL, NULL, NULL),
(16, 2, 'Pc portable1', 100, NULL, 7, NULL, NULL, 48, NULL, NULL, NULL, NULL),
(17, 3, 'Nokia', 200, NULL, 180, NULL, NULL, 48, NULL, NULL, NULL, NULL),
(18, 2, 'Pc portable1', 100, NULL, 9, NULL, NULL, 49, NULL, NULL, NULL, NULL),
(19, NULL, 'Pc portable1', 100, NULL, 1, NULL, NULL, 49, NULL, NULL, NULL, NULL),
(20, NULL, 'Nokia', 200, NULL, 1, NULL, NULL, 49, NULL, NULL, NULL, NULL),
(21, NULL, 'Nokia', 200, NULL, 1, NULL, NULL, 49, NULL, NULL, NULL, NULL),
(22, NULL, 'Nokia', 200, NULL, 1, NULL, NULL, 49, NULL, NULL, NULL, NULL),
(23, NULL, 'Pc portable1', 100, NULL, 1, NULL, NULL, 50, NULL, NULL, NULL, NULL),
(24, NULL, 'Pc portable1', 100, NULL, 1, NULL, NULL, 49, NULL, NULL, NULL, NULL),
(25, 2, 'Pc portable1', 100, NULL, 9, NULL, NULL, 44, NULL, NULL, NULL, NULL),
(26, 2, 'Pc portable1', 100, NULL, 1, NULL, NULL, 44, NULL, NULL, NULL, NULL),
(27, 19, 'HP Portable', 400, NULL, 1, NULL, NULL, 44, NULL, NULL, NULL, NULL),
(28, 2, 'Pc portable1', 100, NULL, 7, NULL, NULL, 51, NULL, NULL, NULL, NULL),
(30, 19, 'HP Portable', 400, NULL, 1, NULL, NULL, 51, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_reception_products`
--

CREATE TABLE `sm_reception_products` (
`ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `DESIGNATION` tinytext,
  `UNIT_PRICE_BUY` double DEFAULT NULL,
  `REMISE` double DEFAULT NULL,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `TVA` double DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `RECEPTION_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_reception_status`
--

CREATE TABLE `sm_reception_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_reception_status`
--

INSERT INTO `sm_reception_status` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'En cours de saisie', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'Transmis', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Validé', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'Non Validé', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_return_receipt`
--

CREATE TABLE `sm_return_receipt` (
`ID` bigint(20) NOT NULL,
  `NO_SEQ` bigint(20) DEFAULT NULL,
  `REFERENCE` tinytext,
  `CUSTOMER_ID` bigint(20) DEFAULT NULL,
  `ORDER_ID` bigint(20) DEFAULT NULL,
  `RETURN_RECEIPT_STATUS_ID` bigint(20) DEFAULT NULL,
  `NOTE` text,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_return_receipt`
--

INSERT INTO `sm_return_receipt` (`ID`, `NO_SEQ`, `REFERENCE`, `CUSTOMER_ID`, `ORDER_ID`, `RETURN_RECEIPT_STATUS_ID`, `NOTE`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(5, 1, 'ref1', 1, NULL, 1, 'Note12', 'Y', 1, '2015-11-21 02:46:35', NULL, NULL, NULL),
(6, 1, NULL, 1, NULL, 2, NULL, 'Y', 1, '2015-11-21 02:46:35', NULL, NULL, NULL),
(7, 2, 'R2', 1, NULL, 1, 'aaaaaa', NULL, 1, '2015-12-27 21:39:07', NULL, NULL, NULL),
(8, 3, 'R3', 6, NULL, 1, 'note123', NULL, 1, '2015-12-27 22:52:59', NULL, NULL, NULL),
(9, 4, 'XZ6Q31', 6, NULL, 1, 'OKSDFG67', NULL, 1, '2015-12-27 23:01:12', NULL, NULL, NULL),
(10, 5, 'R5', 6, NULL, 2, 'aaaa', NULL, 1, '2016-01-16 21:53:30', NULL, NULL, NULL);

--
-- Déclencheurs `sm_return_receipt`
--
DELIMITER //
CREATE TRIGGER `TRG_BI_SM_RETURN_RECEIPT` BEFORE INSERT ON `sm_return_receipt`
 FOR EACH ROW BEGIN

    SET NEW.DATE_CREATION = NOW();
    
    IF EXISTS(SELECT * FROM sm_return_receipt WHERE CLT_MODULE_ID = NEW.CLT_MODULE_ID) THEN
		SET NEW.NO_SEQ = (SELECT MAX(NO_SEQ) + 1 FROM sm_return_receipt WHERE CLT_MODULE_ID = NEW.CLT_MODULE_ID);
    else
		SET NEW.NO_SEQ = 1;
	END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `sm_return_receipt_line`
--

CREATE TABLE `sm_return_receipt_line` (
`ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `DESIGNATION` text,
  `RETURN_RECEIPT_ID` bigint(20) DEFAULT NULL,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `PROMOTION_ID` bigint(20) DEFAULT NULL,
  `NEGOTIATE_PRICE_SALE` double DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_return_receipt_line`
--

INSERT INTO `sm_return_receipt_line` (`ID`, `PRODUCT_ID`, `DESIGNATION`, `RETURN_RECEIPT_ID`, `QUANTITY`, `PROMOTION_ID`, `NEGOTIATE_PRICE_SALE`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(3, 2, 'Pc portable1', 7, 1, NULL, 100, NULL, NULL, NULL, NULL, NULL),
(4, 18, 'Mac OS', 7, 1, NULL, 300, NULL, NULL, NULL, NULL, NULL),
(5, 3, 'TEST 11111', 7, 222, NULL, 4567, NULL, NULL, NULL, NULL, NULL),
(6, 4, 'Sumsung duos', 7, 22, NULL, 300, NULL, NULL, NULL, NULL, NULL),
(7, 4, 'Note Portable', 7, 222, NULL, 111.33, NULL, NULL, NULL, NULL, NULL),
(8, 2, 'Pc portable1', 7, 4567, NULL, 100, NULL, NULL, NULL, NULL, NULL),
(9, 2, 'Pc portable1', 5, 11, NULL, 100, NULL, NULL, NULL, NULL, NULL),
(11, 23, 'Note Portable', 8, 1, NULL, 500, NULL, NULL, NULL, NULL, NULL),
(12, 2, 'Pc portable1', 8, 1, NULL, 100, NULL, NULL, NULL, NULL, NULL),
(13, 2, 'Pc portable1', 9, 1, NULL, 100, NULL, NULL, NULL, NULL, NULL),
(14, 23, 'Note Portable', 10, 1, NULL, 500, NULL, NULL, NULL, NULL, NULL),
(15, 2, 'Pc portable1', 10, 1, NULL, 100, NULL, NULL, NULL, NULL, NULL),
(16, 19, 'HP Portable', 10, 1, NULL, 400, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_return_receipt_status`
--

CREATE TABLE `sm_return_receipt_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_return_receipt_status`
--

INSERT INTO `sm_return_receipt_status` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'en cours', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'submit', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Validé', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'Non Validé', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_supplier`
--

CREATE TABLE `sm_supplier` (
`ID` bigint(20) NOT NULL,
  `FIRST_NAME` tinytext CHARACTER SET latin1,
  `LAST_NAME` tinytext CHARACTER SET latin1,
  `SUPPLIER_TYPE_ID` bigint(20) DEFAULT NULL,
  `INF_COUNTRY_ID` bigint(20) DEFAULT NULL,
  `INF_CITY_ID` bigint(20) DEFAULT NULL,
  `COMPANY_NAME` tinytext,
  `CELL_PHONE` tinytext CHARACTER SET latin1,
  `FIXED_PHONE` tinytext CHARACTER SET latin1,
  `ADRESS` tinytext CHARACTER SET latin1,
  `MAIL` tinytext CHARACTER SET latin1,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `NOTE` text,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `SECONDARY_ADDRESS` text,
  `ZIP_CODE` tinytext,
  `SHORT_LABEL` tinytext,
  `FULL_LABEL` text,
  `WEB_SITE` tinytext,
  `SUPPLIER_CATEGORY_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_supplier`
--

INSERT INTO `sm_supplier` (`ID`, `FIRST_NAME`, `LAST_NAME`, `SUPPLIER_TYPE_ID`, `INF_COUNTRY_ID`, `INF_CITY_ID`, `COMPANY_NAME`, `CELL_PHONE`, `FIXED_PHONE`, `ADRESS`, `MAIL`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `NOTE`, `USER_UPDATE`, `SECONDARY_ADDRESS`, `ZIP_CODE`, `SHORT_LABEL`, `FULL_LABEL`, `WEB_SITE`, `SUPPLIER_CATEGORY_ID`) VALUES
(1, 'Abdessamad', 'HALLAL', 1, 1, 1, 'Sybaway Inc', '+212 6 66 39 18 32', '+212 5 37 60 98 98', 'Kasser Elbher Bloc D 70', 'abdessamad.hallal@gmail.com', 1, NULL, 'Y', NULL, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(15, '', '', 1, 1, 1, 'JIA DA LI SHOES', '', '-', '-', '-', 1, NULL, 'Y', NULL, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'Ahmed', 'Ali', 1, 1, 1, 'INF sos', '+212 6 24 40 18 81', '+212 6 24 40 18 81', 'adress', 'aaa@gmail.com', 1, NULL, 'Y', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, '1', '2', 1, 1, 1, '3', '6', '5', '7', '4', 1, NULL, 'N', NULL, 1, NULL, '13', 1, '8', '9', '10', '11', '12', 1);

-- --------------------------------------------------------

--
-- Structure de la table `sm_supplier_category`
--

CREATE TABLE `sm_supplier_category` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` varchar(45) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_supplier_category`
--

INSERT INTO `sm_supplier_category` (`ID`, `NAME`, `DESCRIPTION`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'categorie One', NULL, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'categorie Tow', NULL, 1, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_supplier_type`
--

CREATE TABLE `sm_supplier_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_supplier_type`
--

INSERT INTO `sm_supplier_type` (`ID`, `NAME`, `DESCRIPTION`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Type par défaut12 One', '12', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'nom1', 'description1', 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'aa', 'bb', 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'tt', 'err', 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(5, 'tr', 'tr', 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(6, 'type1111', 'type222', 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(7, 'nouveau type ', 'centre', 1, NULL, 'Y', NULL, NULL, NULL, NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `clt_client`
--
ALTER TABLE `clt_client`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_CLT_CLIENT_INF_COUNTRY1_idx` (`INF_COUNTRY_ID`), ADD KEY `fk_CLT_CLIENT_INF_CITY1_idx` (`INF_CITY_ID`), ADD KEY `fk_CLT_CLIENT_CLT_CLIENT_STATUS1_idx` (`CLIENT_STATUS_ID`);

--
-- Index pour la table `clt_client_language`
--
ALTER TABLE `clt_client_language`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_CLT_CLIENT_LANGUAGE_CLT_CLIENT1_idx` (`CLIENT_ID`), ADD KEY `fk_CLT_CLIENT_LANGUAGE_INF_LANGUAGE1_idx` (`LANGUAGE_ID`), ADD KEY `fk_CLT_CLIENT_LANGUAGE_INF_PREFIX_ID` (`INF_PREFIX_ID`);

--
-- Index pour la table `clt_client_status`
--
ALTER TABLE `clt_client_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `clt_module`
--
ALTER TABLE `clt_module`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_CLT_FOLDER_CLT_CLIENT1_idx` (`CLIENT_ID`), ADD KEY `fk_CLT_MODULE_CLT_MODULE_STATUS1_idx` (`MODULE_STATUS_ID`), ADD KEY `fk_CLT_MODULE_TYPE1_idx` (`MODULE_TYPE_ID`);

--
-- Index pour la table `clt_module_parameter`
--
ALTER TABLE `clt_module_parameter`
 ADD PRIMARY KEY (`ID`), ADD KEY `idx_clt_module_parameter` (`MODULE_PARAMETER_TYPE_ID`);

--
-- Index pour la table `clt_module_parameter_client`
--
ALTER TABLE `clt_module_parameter_client`
 ADD PRIMARY KEY (`ID`), ADD KEY `idx_clt_module_parameter_client` (`MODULE_PARAMETER_ID`), ADD KEY `idx_clt_module_parameter_client_0` (`MODULE_ID`);

--
-- Index pour la table `clt_module_parameter_type`
--
ALTER TABLE `clt_module_parameter_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `clt_module_status`
--
ALTER TABLE `clt_module_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `clt_module_type`
--
ALTER TABLE `clt_module_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `clt_parameter`
--
ALTER TABLE `clt_parameter`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_CLT_PARAMETER_CLT_TYPE_PARAMETER1_idx` (`PARAMETER_TYPE_ID`);

--
-- Index pour la table `clt_parameter_client`
--
ALTER TABLE `clt_parameter_client`
 ADD PRIMARY KEY (`ID`), ADD KEY `idx_clt_parameter_client` (`PARAMETER_ID`), ADD KEY `idx_clt_parameter_client_0` (`CLINET_ID`);

--
-- Index pour la table `clt_parameter_type`
--
ALTER TABLE `clt_parameter_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `clt_user`
--
ALTER TABLE `clt_user`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_CLT_USER_CLT_CLIENT1_idx` (`CLIENT_ID`), ADD KEY `fk_CLT_USER_CLT_CATEGORY_USER1_idx` (`CATEGORY_ID`), ADD KEY `fk_CLT_USER_INF_COUNTRY1_idx` (`INF_COUNTRY_ID`), ADD KEY `fk_CLT_USER_INF_CITY1_idx` (`INF_CITY_ID`), ADD KEY `idx_clt_user` (`USER_STATUS_ID`);

--
-- Index pour la table `clt_user_category`
--
ALTER TABLE `clt_user_category`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `clt_user_client`
--
ALTER TABLE `clt_user_client`
 ADD PRIMARY KEY (`ID`), ADD KEY `FK_USER_CLIENT_ID_idx` (`CLIENT_ID`), ADD KEY `FK2_USER_CLIENT_idx` (`USER_ID`);

--
-- Index pour la table `clt_user_group`
--
ALTER TABLE `clt_user_group`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_CLT_USER_GROUP_CLT_USER1_idx` (`USER_ID`), ADD KEY `fk_CLT_USER_GROUP_INF_GROUP1_idx` (`INF_GROUP_ID`);

--
-- Index pour la table `clt_user_module`
--
ALTER TABLE `clt_user_module`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_CLT_USER_FOLDER_CLT_USER1_idx` (`USER_ID`), ADD KEY `fk_CLT_USER_FOLDER_CLT_FOLDER1_idx` (`MODULE_ID`);

--
-- Index pour la table `clt_user_status`
--
ALTER TABLE `clt_user_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `cta_email_type`
--
ALTER TABLE `cta_email_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `cta_fax_type`
--
ALTER TABLE `cta_fax_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `cta_location_type`
--
ALTER TABLE `cta_location_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `cta_party`
--
ALTER TABLE `cta_party`
 ADD PRIMARY KEY (`ID`), ADD KEY `FK1_PARTY_TYPE_ID_idx` (`PARTY_TYPE_ID`);

--
-- Index pour la table `cta_party_email`
--
ALTER TABLE `cta_party_email`
 ADD PRIMARY KEY (`ID`), ADD KEY `FK1_PARTY_ID_idx` (`PARTY_ID`), ADD KEY `FK2_EMAIL_TYPE_idx` (`EMAIL_TYPE`);

--
-- Index pour la table `cta_party_fax`
--
ALTER TABLE `cta_party_fax`
 ADD PRIMARY KEY (`ID`), ADD KEY `FK1_PARTY_ID_idx` (`PARTY_ID`), ADD KEY `FK2_FAX_TYPE_idx` (`FAX_TYPE`);

--
-- Index pour la table `cta_party_location`
--
ALTER TABLE `cta_party_location`
 ADD PRIMARY KEY (`ID`), ADD KEY `FK44_PARTY_ID_idx` (`PARTY_ID`), ADD KEY `FK1_LOCATION_TYPE_idx` (`LOCATION_TYPE`);

--
-- Index pour la table `cta_party_phone`
--
ALTER TABLE `cta_party_phone`
 ADD PRIMARY KEY (`ID`), ADD KEY `FK9_PARTY_ID_idx` (`PARTY_ID`), ADD KEY `FK1_PHONE_TYPE_idx` (`PHONE_TYPE`);

--
-- Index pour la table `cta_party_type`
--
ALTER TABLE `cta_party_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `cta_party_web`
--
ALTER TABLE `cta_party_web`
 ADD PRIMARY KEY (`ID`), ADD KEY `FK10_PARTY_ID_idx` (`PARTY_ID`), ADD KEY `FK1_WEB_TYPE_idx` (`WEB_TYPE`);

--
-- Index pour la table `cta_phone_type`
--
ALTER TABLE `cta_phone_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `cta_web_type`
--
ALTER TABLE `cta_web_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_basic_parameter`
--
ALTER TABLE `inf_basic_parameter`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_basic_parameter_type`
--
ALTER TABLE `inf_basic_parameter_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_city`
--
ALTER TABLE `inf_city`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_INF_CITY_INF_COUNTRY1_idx` (`COUNTRY_ID`);

--
-- Index pour la table `inf_country`
--
ALTER TABLE `inf_country`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_group`
--
ALTER TABLE `inf_group`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_INF_GROUP_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `inf_item`
--
ALTER TABLE `inf_item`
 ADD PRIMARY KEY (`CODE`);

--
-- Index pour la table `inf_language`
--
ALTER TABLE `inf_language`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_lovs`
--
ALTER TABLE `inf_lovs`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_prefix`
--
ALTER TABLE `inf_prefix`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_privilege`
--
ALTER TABLE `inf_privilege`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_INF_PRIVILEGE_INF_ITEM1_idx` (`ITEM_CODE`), ADD KEY `fk_INF_PRIVILEGE_INF_ROLE1_idx` (`ROLE_ID`);

--
-- Index pour la table `inf_role`
--
ALTER TABLE `inf_role`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_role_group`
--
ALTER TABLE `inf_role_group`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_INF_ROLE_GROUP_INF_GROUP_idx` (`GROUP_ID`), ADD KEY `fk_INF_ROLE_GROUP_INF_ROLE1_idx` (`ROLE_ID`);

--
-- Index pour la table `inf_text`
--
ALTER TABLE `inf_text`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_INF_TEXT_INF_PREFIX1_idx` (`PREFIX`), ADD KEY `fk_inf_text` (`ITEM_CODE`);

--
-- Index pour la table `inf_text_type`
--
ALTER TABLE `inf_text_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_attribute_exclud`
--
ALTER TABLE `pm_attribute_exclud`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_COMPOS_EXECLUD_PM_PAGE_ITEM1_idx` (`PAGE_ID`);

--
-- Index pour la table `pm_attribute_validation`
--
ALTER TABLE `pm_attribute_validation`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_VALID_SIMPLE_PAGE_PM_VALID_SIMPLE1_idx` (`VALIDATION_ID`), ADD KEY `fk_PM_VALID_SIMPLE_PAGE_PM_PAGE1_idx` (`PAGE_ID`);

--
-- Index pour la table `pm_category`
--
ALTER TABLE `pm_category`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_MENU_PM_MENU_TYPE1_idx` (`CATEGORY_TYPE_ID`);

--
-- Index pour la table `pm_category_type`
--
ALTER TABLE `pm_category_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_component`
--
ALTER TABLE `pm_component`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_composition`
--
ALTER TABLE `pm_composition`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_COMPOSITION_PM_MENU1_idx` (`MENU_ID`), ADD KEY `fk_PM_COMPOSITION_PM_PAGE1_idx` (`PAGE_ID`), ADD KEY `fk_PM_COMPOSITION_CLT_FOLDER1_idx` (`CLT_MODULE_ID`), ADD KEY `FK2` (`GROUP_ID`), ADD KEY `FK3` (`CATEGORY_ID`);

--
-- Index pour la table `pm_data_type`
--
ALTER TABLE `pm_data_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_format_type`
--
ALTER TABLE `pm_format_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_group`
--
ALTER TABLE `pm_group`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_GROUP_PM_GROUP_TYPE1_idx` (`GROUP_TYPE_ID`);

--
-- Index pour la table `pm_group_type`
--
ALTER TABLE `pm_group_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_menu`
--
ALTER TABLE `pm_menu`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_SECTION_PM_SECTION_TYPE1_idx` (`MENU_TYPE_ID`);

--
-- Index pour la table `pm_menu_type`
--
ALTER TABLE `pm_menu_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_module_attribute`
--
ALTER TABLE `pm_module_attribute`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_PAGE_ATTRIBUTE1_idx` (`PM_PAGE_ATTRIBUTE_ID`), ADD KEY `fk_DATA_TYPE2_idx` (`DATA_TYPE_ID`), ADD KEY `FORMAT_TYPE_ID2_idx` (`FORMAT_TYPE_ID`);

--
-- Index pour la table `pm_module_pdf_attribute`
--
ALTER TABLE `pm_module_pdf_attribute`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_page`
--
ALTER TABLE `pm_page`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_PAGE_PM_TYPE_PAGE1_idx` (`PAGE_TYPE_ID`);

--
-- Index pour la table `pm_page_attribute`
--
ALTER TABLE `pm_page_attribute`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_PAGE_ITEM_PM_PAGE1_idx` (`PAGE_ID`), ADD KEY `fk_PM_PAGE_ITEM_INF_ITEM1_idx` (`INF_ITEM_CODE`), ADD KEY `fk_PM_PAGE_ITEM_PM_COMPONENTE1_idx` (`PM_COMPONENT_ID`), ADD KEY `fk_data_type1_idx` (`DATA_TYPE_ID`), ADD KEY `fk_foramt_type1_idx` (`FORMAT_TYPE_ID`);

--
-- Index pour la table `pm_page_parameter`
--
ALTER TABLE `pm_page_parameter`
 ADD PRIMARY KEY (`ID`), ADD KEY `FK1_PAGE_PAPRAMETER_idx` (`PAGE_PARAMETER_TYPE_ID`), ADD KEY `idx_pm_page_parameter` (`PAGE_ID`);

--
-- Index pour la table `pm_page_parameter_module`
--
ALTER TABLE `pm_page_parameter_module`
 ADD PRIMARY KEY (`ID`), ADD KEY `idx_pm_page_parameter_module` (`PAGE_PARAMETER_ID`);

--
-- Index pour la table `pm_page_parameter_type`
--
ALTER TABLE `pm_page_parameter_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_page_type`
--
ALTER TABLE `pm_page_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_pdf`
--
ALTER TABLE `pm_pdf`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_pdf_attribute`
--
ALTER TABLE `pm_pdf_attribute`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_pdf_attribute_exclud`
--
ALTER TABLE `pm_pdf_attribute_exclud`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_pdf_parameter`
--
ALTER TABLE `pm_pdf_parameter`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_pdf_parameter_module`
--
ALTER TABLE `pm_pdf_parameter_module`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_pdf_parameter_type`
--
ALTER TABLE `pm_pdf_parameter_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_pdf_type`
--
ALTER TABLE `pm_pdf_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_validation_type`
--
ALTER TABLE `pm_validation_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_advanced`
--
ALTER TABLE `sm_advanced`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_ADVANCED_SM_ADVANCED_STATUS1_idx` (`ADVANCED_STATUS_ID`), ADD KEY `fk_SM_ADVANCED_SM_CUSTOMER1_idx` (`CUSTOMER_ID`), ADD KEY `fk_SM_ADVANCED_SM_PRODUCT1_idx` (`PRODUCT_ID`);

--
-- Index pour la table `sm_advanced_status`
--
ALTER TABLE `sm_advanced_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_bank_type`
--
ALTER TABLE `sm_bank_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_check`
--
ALTER TABLE `sm_check`
 ADD PRIMARY KEY (`ID`), ADD UNIQUE KEY `pk_sm_check_0` (`BANK_ID`);

--
-- Index pour la table `sm_customer`
--
ALTER TABLE `sm_customer`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_CUSTOMER_CLT_MODULE1_idx` (`CLT_MODULE_ID`), ADD KEY `idx_sm_customer` (`CUSTOMER_TYPE_ID`);

--
-- Index pour la table `sm_customer_category`
--
ALTER TABLE `sm_customer_category`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_customer_type`
--
ALTER TABLE `sm_customer_type`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_CUSTOMER_TYPE_CLT_FOLDER1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `sm_deposit`
--
ALTER TABLE `sm_deposit`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_expense`
--
ALTER TABLE `sm_expense`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_EXPENSE_SM_EXPENSE_TYPE1_idx` (`EXPENSE_TYPE_ID`), ADD KEY `fk_SM_EXPENSE_CLT_FOLDER1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `sm_expense_type`
--
ALTER TABLE `sm_expense_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_order`
--
ALTER TABLE `sm_order`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_ORDER_SM_ORDER_STATUS1_idx` (`ORDER_STATUS_ID`), ADD KEY `fk_SM_ORDER_SM_PAYMENT_METHOD1_idx` (`PAYMENT_METHOD_ID`), ADD KEY `fk_SM_ORDER_SM_CUSTOMER1_idx` (`CUSTOMER_ID`), ADD KEY `fk_SM_ORDER_SM_CHECK1_idx` (`CHECK_ID`);

--
-- Index pour la table `sm_order_line`
--
ALTER TABLE `sm_order_line`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_ORDER_LINE_SM_PRODUCT1_idx` (`PRODUCT_ID`), ADD KEY `fk_SM_ORDER_LINE_SM_ORDER1_idx` (`ORDER_ID`);

--
-- Index pour la table `sm_order_status`
--
ALTER TABLE `sm_order_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_order_supplier`
--
ALTER TABLE `sm_order_supplier`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk1_sm_order_supplier_idx` (`ORDER_SUPPLIER_STATUS_ID`);

--
-- Index pour la table `sm_order_supplier_line`
--
ALTER TABLE `sm_order_supplier_line`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk1_sm_order_supplier_line_idx` (`ORDER_SUPPLIER_ID`), ADD KEY `fk2_sm_order_supplier_line_idx` (`PRODUCT_ID`);

--
-- Index pour la table `sm_order_supplier_status`
--
ALTER TABLE `sm_order_supplier_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_payment_method`
--
ALTER TABLE `sm_payment_method`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_PAYMENT_METHOD_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `sm_product`
--
ALTER TABLE `sm_product`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_PRODUCT_SM_PRODUCT_STATUS1_idx` (`PRODUCT_STATUS_ID`), ADD KEY `fk_SM_PRODUCT_SM_PRODUCT_FAMILY1_idx` (`PRODUCT_FAMILY_ID`), ADD KEY `fk_SM_PRODUCT_SM_PRODUCT_SIZE1_idx` (`PRODUCT_SIZE_ID`), ADD KEY `fk_SM_PRODUCT_SM_PRODUCT_COLOR1_idx` (`PRODUCT_COLOR_ID`);

--
-- Index pour la table `sm_product_color`
--
ALTER TABLE `sm_product_color`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_PRODUCT_COLOR_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `sm_product_family`
--
ALTER TABLE `sm_product_family`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_PRODUCT_FAMILY_SM_PRODUCT_GROUP1_idx` (`PRODUCT_GROUP_ID`), ADD KEY `fk_SM_PRODUCT_FAMILY_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `sm_product_group`
--
ALTER TABLE `sm_product_group`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_PRODUCT_GROUP_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `sm_product_size`
--
ALTER TABLE `sm_product_size`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_product_status`
--
ALTER TABLE `sm_product_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_product_type`
--
ALTER TABLE `sm_product_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_promotion`
--
ALTER TABLE `sm_promotion`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_PROMOTION_SM_PROMOTION_TYPE1_idx` (`PROMOTION_TYPE_ID`), ADD KEY `fk_SM_PROMOTION_SM_PRODUCT1_idx` (`PRODUCT`);

--
-- Index pour la table `sm_promotion_type`
--
ALTER TABLE `sm_promotion_type`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_PROMOTION_TYPE_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `sm_reception`
--
ALTER TABLE `sm_reception`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SPM_RECEPTION_STATUS_idx` (`RECEPTION_STATUS_ID`), ADD KEY `fk1_sm_reception_idx` (`SUPPLIER_ID`), ADD KEY `fk3_sm_reception_idx` (`ORDER_SUPPLIER_ID`);

--
-- Index pour la table `sm_reception_line`
--
ALTER TABLE `sm_reception_line`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk2_sm_reception_line_idx` (`RECEPTION_ID`), ADD KEY `fk1_sm_reception_line_idx` (`PRODUCT_ID`);

--
-- Index pour la table `sm_reception_products`
--
ALTER TABLE `sm_reception_products`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk1_reception_id_idx` (`RECEPTION_ID`);

--
-- Index pour la table `sm_reception_status`
--
ALTER TABLE `sm_reception_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_return_receipt`
--
ALTER TABLE `sm_return_receipt`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk1_sm_return_receipt_idx` (`RETURN_RECEIPT_STATUS_ID`), ADD KEY `fk2_sm_return_receipt_idx` (`ORDER_ID`);

--
-- Index pour la table `sm_return_receipt_line`
--
ALTER TABLE `sm_return_receipt_line`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk1_ sm_return_receipt_line_idx` (`RETURN_RECEIPT_ID`), ADD KEY `fk2_ sm_return_receipt_line_idx` (`PRODUCT_ID`);

--
-- Index pour la table `sm_return_receipt_status`
--
ALTER TABLE `sm_return_receipt_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_supplier`
--
ALTER TABLE `sm_supplier`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_SUPPLIER_SM_SUPPLIER_TYPE1_idx` (`SUPPLIER_TYPE_ID`);

--
-- Index pour la table `sm_supplier_category`
--
ALTER TABLE `sm_supplier_category`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_supplier_type`
--
ALTER TABLE `sm_supplier_type`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_SUPPLIER_TYPE_CLT_FOLDER1_idx` (`CLT_MODULE_ID`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `clt_client`
--
ALTER TABLE `clt_client`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `clt_client_language`
--
ALTER TABLE `clt_client_language`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `clt_client_status`
--
ALTER TABLE `clt_client_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `clt_module`
--
ALTER TABLE `clt_module`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `clt_module_parameter`
--
ALTER TABLE `clt_module_parameter`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `clt_module_parameter_client`
--
ALTER TABLE `clt_module_parameter_client`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `clt_module_parameter_type`
--
ALTER TABLE `clt_module_parameter_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `clt_module_status`
--
ALTER TABLE `clt_module_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `clt_module_type`
--
ALTER TABLE `clt_module_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `clt_parameter`
--
ALTER TABLE `clt_parameter`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `clt_parameter_client`
--
ALTER TABLE `clt_parameter_client`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `clt_user`
--
ALTER TABLE `clt_user`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `clt_user_category`
--
ALTER TABLE `clt_user_category`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `clt_user_client`
--
ALTER TABLE `clt_user_client`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `clt_user_group`
--
ALTER TABLE `clt_user_group`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `clt_user_module`
--
ALTER TABLE `clt_user_module`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT pour la table `clt_user_status`
--
ALTER TABLE `clt_user_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `cta_email_type`
--
ALTER TABLE `cta_email_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `cta_fax_type`
--
ALTER TABLE `cta_fax_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `cta_location_type`
--
ALTER TABLE `cta_location_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `cta_party`
--
ALTER TABLE `cta_party`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `cta_party_email`
--
ALTER TABLE `cta_party_email`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `cta_party_fax`
--
ALTER TABLE `cta_party_fax`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `cta_party_location`
--
ALTER TABLE `cta_party_location`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `cta_party_phone`
--
ALTER TABLE `cta_party_phone`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `cta_party_type`
--
ALTER TABLE `cta_party_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `cta_party_web`
--
ALTER TABLE `cta_party_web`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `cta_phone_type`
--
ALTER TABLE `cta_phone_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `cta_web_type`
--
ALTER TABLE `cta_web_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `inf_basic_parameter`
--
ALTER TABLE `inf_basic_parameter`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `inf_basic_parameter_type`
--
ALTER TABLE `inf_basic_parameter_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `inf_city`
--
ALTER TABLE `inf_city`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `inf_country`
--
ALTER TABLE `inf_country`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `inf_group`
--
ALTER TABLE `inf_group`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `inf_language`
--
ALTER TABLE `inf_language`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `inf_lovs`
--
ALTER TABLE `inf_lovs`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `inf_prefix`
--
ALTER TABLE `inf_prefix`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `inf_privilege`
--
ALTER TABLE `inf_privilege`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `inf_role`
--
ALTER TABLE `inf_role`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `inf_role_group`
--
ALTER TABLE `inf_role_group`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `inf_text`
--
ALTER TABLE `inf_text`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=893;
--
-- AUTO_INCREMENT pour la table `inf_text_type`
--
ALTER TABLE `inf_text_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `pm_attribute_exclud`
--
ALTER TABLE `pm_attribute_exclud`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `pm_attribute_validation`
--
ALTER TABLE `pm_attribute_validation`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=286;
--
-- AUTO_INCREMENT pour la table `pm_category`
--
ALTER TABLE `pm_category`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `pm_category_type`
--
ALTER TABLE `pm_category_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `pm_component`
--
ALTER TABLE `pm_component`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `pm_composition`
--
ALTER TABLE `pm_composition`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=217;
--
-- AUTO_INCREMENT pour la table `pm_data_type`
--
ALTER TABLE `pm_data_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `pm_format_type`
--
ALTER TABLE `pm_format_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `pm_group`
--
ALTER TABLE `pm_group`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `pm_group_type`
--
ALTER TABLE `pm_group_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `pm_menu`
--
ALTER TABLE `pm_menu`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT pour la table `pm_menu_type`
--
ALTER TABLE `pm_menu_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `pm_module_attribute`
--
ALTER TABLE `pm_module_attribute`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pm_module_pdf_attribute`
--
ALTER TABLE `pm_module_pdf_attribute`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pm_page`
--
ALTER TABLE `pm_page`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT pour la table `pm_page_attribute`
--
ALTER TABLE `pm_page_attribute`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=140;
--
-- AUTO_INCREMENT pour la table `pm_page_parameter`
--
ALTER TABLE `pm_page_parameter`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `pm_page_parameter_module`
--
ALTER TABLE `pm_page_parameter_module`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pm_page_parameter_type`
--
ALTER TABLE `pm_page_parameter_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `pm_page_type`
--
ALTER TABLE `pm_page_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `pm_pdf`
--
ALTER TABLE `pm_pdf`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `pm_pdf_attribute`
--
ALTER TABLE `pm_pdf_attribute`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pm_pdf_attribute_exclud`
--
ALTER TABLE `pm_pdf_attribute_exclud`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pm_pdf_parameter`
--
ALTER TABLE `pm_pdf_parameter`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pm_pdf_parameter_module`
--
ALTER TABLE `pm_pdf_parameter_module`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pm_pdf_parameter_type`
--
ALTER TABLE `pm_pdf_parameter_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pm_pdf_type`
--
ALTER TABLE `pm_pdf_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `pm_validation_type`
--
ALTER TABLE `pm_validation_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `sm_advanced`
--
ALTER TABLE `sm_advanced`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_advanced_status`
--
ALTER TABLE `sm_advanced_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_bank_type`
--
ALTER TABLE `sm_bank_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_check`
--
ALTER TABLE `sm_check`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_customer`
--
ALTER TABLE `sm_customer`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `sm_customer_category`
--
ALTER TABLE `sm_customer_category`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `sm_customer_type`
--
ALTER TABLE `sm_customer_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `sm_deposit`
--
ALTER TABLE `sm_deposit`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `sm_expense`
--
ALTER TABLE `sm_expense`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT pour la table `sm_expense_type`
--
ALTER TABLE `sm_expense_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT pour la table `sm_order`
--
ALTER TABLE `sm_order`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT pour la table `sm_order_line`
--
ALTER TABLE `sm_order_line`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT pour la table `sm_order_status`
--
ALTER TABLE `sm_order_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `sm_order_supplier`
--
ALTER TABLE `sm_order_supplier`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT pour la table `sm_order_supplier_line`
--
ALTER TABLE `sm_order_supplier_line`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT pour la table `sm_order_supplier_status`
--
ALTER TABLE `sm_order_supplier_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `sm_payment_method`
--
ALTER TABLE `sm_payment_method`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `sm_product`
--
ALTER TABLE `sm_product`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT pour la table `sm_product_color`
--
ALTER TABLE `sm_product_color`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `sm_product_family`
--
ALTER TABLE `sm_product_family`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `sm_product_group`
--
ALTER TABLE `sm_product_group`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `sm_product_size`
--
ALTER TABLE `sm_product_size`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT pour la table `sm_product_status`
--
ALTER TABLE `sm_product_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `sm_product_type`
--
ALTER TABLE `sm_product_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `sm_promotion`
--
ALTER TABLE `sm_promotion`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_promotion_type`
--
ALTER TABLE `sm_promotion_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_reception`
--
ALTER TABLE `sm_reception`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT pour la table `sm_reception_line`
--
ALTER TABLE `sm_reception_line`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT pour la table `sm_reception_products`
--
ALTER TABLE `sm_reception_products`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_reception_status`
--
ALTER TABLE `sm_reception_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `sm_return_receipt`
--
ALTER TABLE `sm_return_receipt`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `sm_return_receipt_line`
--
ALTER TABLE `sm_return_receipt_line`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT pour la table `sm_return_receipt_status`
--
ALTER TABLE `sm_return_receipt_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `sm_supplier`
--
ALTER TABLE `sm_supplier`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT pour la table `sm_supplier_category`
--
ALTER TABLE `sm_supplier_category`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `sm_supplier_type`
--
ALTER TABLE `sm_supplier_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `clt_client`
--
ALTER TABLE `clt_client`
ADD CONSTRAINT `fk_CLT_CLIENT_CLT_CLIENT_STATUS1` FOREIGN KEY (`CLIENT_STATUS_ID`) REFERENCES `clt_client_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_CLIENT_INF_CITY1` FOREIGN KEY (`INF_CITY_ID`) REFERENCES `inf_city` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_CLIENT_INF_COUNTRY1` FOREIGN KEY (`INF_COUNTRY_ID`) REFERENCES `inf_country` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `clt_client_language`
--
ALTER TABLE `clt_client_language`
ADD CONSTRAINT `fk_CLT_CLIENT_LANGUAGE_CLT_CLIENT1` FOREIGN KEY (`CLIENT_ID`) REFERENCES `clt_client` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_CLIENT_LANGUAGE_INF_PREFIX_ID` FOREIGN KEY (`INF_PREFIX_ID`) REFERENCES `inf_prefix` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_CLIENT_LANGUAGE_LANGUAGE_ID` FOREIGN KEY (`LANGUAGE_ID`) REFERENCES `inf_language` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `clt_module`
--
ALTER TABLE `clt_module`
ADD CONSTRAINT `fk_CLT_MODULE_CLT_CLIENT1` FOREIGN KEY (`CLIENT_ID`) REFERENCES `clt_client` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_MODULE_CLT_MODULE_STATUS1` FOREIGN KEY (`MODULE_STATUS_ID`) REFERENCES `clt_module_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_MODULE_TYPE1` FOREIGN KEY (`MODULE_TYPE_ID`) REFERENCES `clt_module_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `clt_module_parameter`
--
ALTER TABLE `clt_module_parameter`
ADD CONSTRAINT `fk_clt_module_parameter` FOREIGN KEY (`MODULE_PARAMETER_TYPE_ID`) REFERENCES `clt_module_parameter_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `clt_module_parameter_client`
--
ALTER TABLE `clt_module_parameter_client`
ADD CONSTRAINT `fk_clt_module_parameter_client` FOREIGN KEY (`MODULE_PARAMETER_ID`) REFERENCES `clt_module_parameter` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_clt_module_parameter_client_0` FOREIGN KEY (`MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `clt_parameter`
--
ALTER TABLE `clt_parameter`
ADD CONSTRAINT `fk_clt_parameter` FOREIGN KEY (`PARAMETER_TYPE_ID`) REFERENCES `clt_parameter_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `clt_parameter_client`
--
ALTER TABLE `clt_parameter_client`
ADD CONSTRAINT `fk_clt_parameter_client` FOREIGN KEY (`PARAMETER_ID`) REFERENCES `clt_parameter` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_clt_parameter_client_0` FOREIGN KEY (`CLINET_ID`) REFERENCES `clt_client` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `clt_user`
--
ALTER TABLE `clt_user`
ADD CONSTRAINT `fk_clt_user` FOREIGN KEY (`USER_STATUS_ID`) REFERENCES `clt_user_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_USER_CLT_CATEGORY_USER1` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `clt_user_category` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_USER_CLT_CLIENT1` FOREIGN KEY (`CLIENT_ID`) REFERENCES `clt_client` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_USER_INF_CITY1` FOREIGN KEY (`INF_CITY_ID`) REFERENCES `inf_city` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_USER_INF_COUNTRY1` FOREIGN KEY (`INF_COUNTRY_ID`) REFERENCES `inf_country` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `clt_user_client`
--
ALTER TABLE `clt_user_client`
ADD CONSTRAINT `FK1_USER_CLIENT` FOREIGN KEY (`CLIENT_ID`) REFERENCES `clt_client` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK2_USER_CLIENT` FOREIGN KEY (`USER_ID`) REFERENCES `clt_user` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `clt_user_group`
--
ALTER TABLE `clt_user_group`
ADD CONSTRAINT `fk_CLT_USER_GROUP_CLT_USER1` FOREIGN KEY (`USER_ID`) REFERENCES `clt_user` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_USER_GROUP_INF_GROUP1` FOREIGN KEY (`INF_GROUP_ID`) REFERENCES `inf_group` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `clt_user_module`
--
ALTER TABLE `clt_user_module`
ADD CONSTRAINT `fk_CLT_USER_FOLDER_CLT_FOLDER1` FOREIGN KEY (`MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_USER_FOLDER_CLT_USER1` FOREIGN KEY (`USER_ID`) REFERENCES `clt_user` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `cta_party`
--
ALTER TABLE `cta_party`
ADD CONSTRAINT `FK1_PARTY_TYPE_ID` FOREIGN KEY (`PARTY_TYPE_ID`) REFERENCES `cta_party_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `cta_party_email`
--
ALTER TABLE `cta_party_email`
ADD CONSTRAINT `FK1_PARTY_ID` FOREIGN KEY (`PARTY_ID`) REFERENCES `cta_party` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK2_EMAIL_TYPE` FOREIGN KEY (`EMAIL_TYPE`) REFERENCES `cta_email_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `cta_party_fax`
--
ALTER TABLE `cta_party_fax`
ADD CONSTRAINT `FK3_FAX_TYPE` FOREIGN KEY (`FAX_TYPE`) REFERENCES `cta_fax_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK3_PARTY_ID` FOREIGN KEY (`PARTY_ID`) REFERENCES `cta_party` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `cta_party_location`
--
ALTER TABLE `cta_party_location`
ADD CONSTRAINT `FK1_LOCATION_TYPE` FOREIGN KEY (`LOCATION_TYPE`) REFERENCES `cta_location_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK44_PARTY_ID` FOREIGN KEY (`PARTY_ID`) REFERENCES `cta_party` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `cta_party_phone`
--
ALTER TABLE `cta_party_phone`
ADD CONSTRAINT `FK1_PHONE_TYPE` FOREIGN KEY (`PHONE_TYPE`) REFERENCES `cta_phone_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK9_PARTY_ID` FOREIGN KEY (`PARTY_ID`) REFERENCES `cta_party` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `cta_party_web`
--
ALTER TABLE `cta_party_web`
ADD CONSTRAINT `FK10_PARTY_ID` FOREIGN KEY (`PARTY_ID`) REFERENCES `cta_party` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK1_WEB_TYPE` FOREIGN KEY (`WEB_TYPE`) REFERENCES `cta_web_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `inf_city`
--
ALTER TABLE `inf_city`
ADD CONSTRAINT `fk_INF_CITY_INF_COUNTRY1` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `inf_country` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `inf_group`
--
ALTER TABLE `inf_group`
ADD CONSTRAINT `fk_INF_GROUP_CLT_MODULE1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `inf_privilege`
--
ALTER TABLE `inf_privilege`
ADD CONSTRAINT `fk_inf_privilege` FOREIGN KEY (`ITEM_CODE`) REFERENCES `inf_item` (`CODE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_INF_PRIVILEGE_INF_ROLE1` FOREIGN KEY (`ROLE_ID`) REFERENCES `inf_role` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `inf_role_group`
--
ALTER TABLE `inf_role_group`
ADD CONSTRAINT `fk_INF_ROLE_GROUP_INF_GROUP` FOREIGN KEY (`GROUP_ID`) REFERENCES `inf_group` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_INF_ROLE_GROUP_INF_ROLE1` FOREIGN KEY (`ROLE_ID`) REFERENCES `inf_role` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_attribute_exclud`
--
ALTER TABLE `pm_attribute_exclud`
ADD CONSTRAINT `fk_pm_attribute_exclud` FOREIGN KEY (`PAGE_ID`) REFERENCES `pm_page` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_attribute_validation`
--
ALTER TABLE `pm_attribute_validation`
ADD CONSTRAINT `fk_PM_VALID_SIMPLE_PAGE_PM_PAGE1` FOREIGN KEY (`PAGE_ID`) REFERENCES `pm_page` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_category`
--
ALTER TABLE `pm_category`
ADD CONSTRAINT `fk_pm_category` FOREIGN KEY (`CATEGORY_TYPE_ID`) REFERENCES `pm_category_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_composition`
--
ALTER TABLE `pm_composition`
ADD CONSTRAINT `FK1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE SET NULL ON UPDATE SET NULL,
ADD CONSTRAINT `FK2` FOREIGN KEY (`GROUP_ID`) REFERENCES `pm_group` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK3` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `pm_category` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK4` FOREIGN KEY (`MENU_ID`) REFERENCES `pm_menu` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK5` FOREIGN KEY (`PAGE_ID`) REFERENCES `pm_page` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_group`
--
ALTER TABLE `pm_group`
ADD CONSTRAINT `fk_pm_group` FOREIGN KEY (`GROUP_TYPE_ID`) REFERENCES `pm_group_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_menu`
--
ALTER TABLE `pm_menu`
ADD CONSTRAINT `fk_PM_SECTION_PM_SECTION_TYPE1` FOREIGN KEY (`MENU_TYPE_ID`) REFERENCES `pm_menu_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_module_attribute`
--
ALTER TABLE `pm_module_attribute`
ADD CONSTRAINT `fk_DATA_TYPE2` FOREIGN KEY (`DATA_TYPE_ID`) REFERENCES `pm_data_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_PM_PAGE_ATTRIBUTE1` FOREIGN KEY (`PM_PAGE_ATTRIBUTE_ID`) REFERENCES `pm_page_attribute` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FORMAT_TYPE_ID2` FOREIGN KEY (`FORMAT_TYPE_ID`) REFERENCES `pm_format_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_page`
--
ALTER TABLE `pm_page`
ADD CONSTRAINT `fk_PM_PAGE_PM_TYPE_PAGE1` FOREIGN KEY (`PAGE_TYPE_ID`) REFERENCES `pm_page_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_page_attribute`
--
ALTER TABLE `pm_page_attribute`
ADD CONSTRAINT `fk_data_type1` FOREIGN KEY (`DATA_TYPE_ID`) REFERENCES `pm_data_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_foramt_type1` FOREIGN KEY (`FORMAT_TYPE_ID`) REFERENCES `pm_format_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_PM_PAGE_ITEM_PM_COMPONENTE1` FOREIGN KEY (`PM_COMPONENT_ID`) REFERENCES `pm_component` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_PM_PAGE_ITEM_PM_PAGE1` FOREIGN KEY (`PAGE_ID`) REFERENCES `pm_page` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_page_parameter`
--
ALTER TABLE `pm_page_parameter`
ADD CONSTRAINT `FK1_PAGE_PAPRAMETER` FOREIGN KEY (`PAGE_PARAMETER_TYPE_ID`) REFERENCES `pm_page_parameter_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_pm_page_parameter` FOREIGN KEY (`PAGE_ID`) REFERENCES `pm_page` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_page_parameter_module`
--
ALTER TABLE `pm_page_parameter_module`
ADD CONSTRAINT `fk_pm_page_parameter_module` FOREIGN KEY (`PAGE_PARAMETER_ID`) REFERENCES `pm_page_parameter` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_advanced`
--
ALTER TABLE `sm_advanced`
ADD CONSTRAINT `fk_SM_ADVANCED_SM_ADVANCED_STATUS1` FOREIGN KEY (`ADVANCED_STATUS_ID`) REFERENCES `sm_advanced_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_ADVANCED_SM_CUSTOMER1` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `sm_customer` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_ADVANCED_SM_PRODUCT1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sm_product` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_customer`
--
ALTER TABLE `sm_customer`
ADD CONSTRAINT `fk_SM_CUSTOMER_CLT_MODULE1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_sm_customer_sm_customer_type1` FOREIGN KEY (`CUSTOMER_TYPE_ID`) REFERENCES `sm_customer_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_customer_type`
--
ALTER TABLE `sm_customer_type`
ADD CONSTRAINT `fk_SM_CUSTOMER_TYPE_CLT_FOLDER1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_expense`
--
ALTER TABLE `sm_expense`
ADD CONSTRAINT `fk_SM_EXPENSE_CLT_FOLDER1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_EXPENSE_SM_EXPENSE_TYPE1` FOREIGN KEY (`EXPENSE_TYPE_ID`) REFERENCES `sm_expense_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_order`
--
ALTER TABLE `sm_order`
ADD CONSTRAINT `fk_SM_ORDER_SM_CHECK1` FOREIGN KEY (`CHECK_ID`) REFERENCES `sm_check` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_ORDER_SM_CUSTOMER1` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `sm_customer` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_ORDER_SM_ORDER_STATUS1` FOREIGN KEY (`ORDER_STATUS_ID`) REFERENCES `sm_order_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_ORDER_SM_PAYMENT_METHOD1` FOREIGN KEY (`PAYMENT_METHOD_ID`) REFERENCES `sm_payment_method` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_order_line`
--
ALTER TABLE `sm_order_line`
ADD CONSTRAINT `fk_SM_ORDER_LINE_SM_ORDER1` FOREIGN KEY (`ORDER_ID`) REFERENCES `sm_order` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_ORDER_LINE_SM_PRODUCT1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sm_product` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_order_supplier`
--
ALTER TABLE `sm_order_supplier`
ADD CONSTRAINT `fk1_sm_order_supplier` FOREIGN KEY (`ORDER_SUPPLIER_STATUS_ID`) REFERENCES `sm_order_supplier_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_order_supplier_line`
--
ALTER TABLE `sm_order_supplier_line`
ADD CONSTRAINT `fk1_sm_order_supplier_line` FOREIGN KEY (`ORDER_SUPPLIER_ID`) REFERENCES `sm_order_supplier` (`ID`) ON DELETE CASCADE ON UPDATE SET NULL,
ADD CONSTRAINT `fk2_sm_order_supplier_line` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sm_product` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_payment_method`
--
ALTER TABLE `sm_payment_method`
ADD CONSTRAINT `fk_SM_PAYMENT_METHOD_CLT_MODULE1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_product`
--
ALTER TABLE `sm_product`
ADD CONSTRAINT `fk_SM_PRODUCT_SM_PRODUCT_COLOR1` FOREIGN KEY (`PRODUCT_COLOR_ID`) REFERENCES `sm_product_color` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_PRODUCT_SM_PRODUCT_FAMILY1` FOREIGN KEY (`PRODUCT_FAMILY_ID`) REFERENCES `sm_product_family` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_sm_product_sm_product_size1` FOREIGN KEY (`PRODUCT_SIZE_ID`) REFERENCES `sm_product_size` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_PRODUCT_SM_PRODUCT_STATUS1` FOREIGN KEY (`PRODUCT_STATUS_ID`) REFERENCES `sm_product_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_product_color`
--
ALTER TABLE `sm_product_color`
ADD CONSTRAINT `fk_SM_PRODUCT_COLOR_CLT_MODULE1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_product_family`
--
ALTER TABLE `sm_product_family`
ADD CONSTRAINT `fk_SM_PRODUCT_FAMILY_CLT_MODULE1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_PRODUCT_FAMILY_SM_PRODUCT_GROUP1` FOREIGN KEY (`PRODUCT_GROUP_ID`) REFERENCES `sm_product_group` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_product_group`
--
ALTER TABLE `sm_product_group`
ADD CONSTRAINT `fk_SM_PRODUCT_GROUP_CLT_MODULE1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_promotion`
--
ALTER TABLE `sm_promotion`
ADD CONSTRAINT `fk_SM_PROMOTION_SM_PRODUCT1` FOREIGN KEY (`PRODUCT`) REFERENCES `sm_product` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_PROMOTION_SM_PROMOTION_TYPE1` FOREIGN KEY (`PROMOTION_TYPE_ID`) REFERENCES `sm_promotion_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_promotion_type`
--
ALTER TABLE `sm_promotion_type`
ADD CONSTRAINT `fk_SM_PROMOTION_TYPE_CLT_MODULE1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_reception`
--
ALTER TABLE `sm_reception`
ADD CONSTRAINT `fk1_sm_reception` FOREIGN KEY (`SUPPLIER_ID`) REFERENCES `sm_supplier` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk2_sm_reception` FOREIGN KEY (`RECEPTION_STATUS_ID`) REFERENCES `sm_reception_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk3_sm_reception` FOREIGN KEY (`ORDER_SUPPLIER_ID`) REFERENCES `sm_order_supplier` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_reception_line`
--
ALTER TABLE `sm_reception_line`
ADD CONSTRAINT `fk1_sm_reception_line` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sm_product` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk2_sm_reception_line` FOREIGN KEY (`RECEPTION_ID`) REFERENCES `sm_reception` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_reception_products`
--
ALTER TABLE `sm_reception_products`
ADD CONSTRAINT `fk1_sm_reception_id` FOREIGN KEY (`RECEPTION_ID`) REFERENCES `sm_reception` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_return_receipt`
--
ALTER TABLE `sm_return_receipt`
ADD CONSTRAINT `fk1_sm_return_receipt` FOREIGN KEY (`RETURN_RECEIPT_STATUS_ID`) REFERENCES `sm_return_receipt_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk2_sm_return_receipt` FOREIGN KEY (`ORDER_ID`) REFERENCES `sm_order` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_return_receipt_line`
--
ALTER TABLE `sm_return_receipt_line`
ADD CONSTRAINT `fk1_ sm_return_receipt_line` FOREIGN KEY (`RETURN_RECEIPT_ID`) REFERENCES `sm_return_receipt` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk2_ sm_return_receipt_line` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sm_product` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_supplier`
--
ALTER TABLE `sm_supplier`
ADD CONSTRAINT `fk_SM_SUPPLIER_SM_SUPPLIER_TYPE1` FOREIGN KEY (`SUPPLIER_TYPE_ID`) REFERENCES `sm_supplier_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_supplier_type`
--
ALTER TABLE `sm_supplier_type`
ADD CONSTRAINT `fk_SM_SUPPLIER_TYPE_CLT_FOLDER1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;
SET FOREIGN_KEY_CHECKS=1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
