-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Client :  localhost:3306
-- Généré le :  Sam 10 Décembre 2016 à 14:42
-- Version du serveur :  5.5.42
-- Version de PHP :  7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mystock`
--
CREATE DATABASE IF NOT EXISTS `mystock` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mystock`;

-- --------------------------------------------------------

--
-- Structure de la table `CLT_CLIENT`
--

CREATE TABLE IF NOT EXISTS `CLT_CLIENT` (
  `ID` bigint(20) NOT NULL,
  `CODE` varchar(255) NOT NULL,
  `FIRST_NAME` tinytext,
  `LAST_NAME` tinytext,
  `COMPANY_NAME` tinytext CHARACTER SET latin1,
  `CLIENT_STATUS_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `ENTITY_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_CLIENT`
--

INSERT INTO `CLT_CLIENT` (`ID`, `CODE`, `FIRST_NAME`, `LAST_NAME`, `COMPANY_NAME`, `CLIENT_STATUS_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `ENTITY_ID`) VALUES
(1, 'demo', 'demo first', 'demo last ', 'demo', 1, 'Y', '2016-04-24 13:47:55', NULL, NULL, NULL, 1),
(2, 'demo1', 'demo1 first', 'demo1 last', 'demo1', 1, 'N', '2016-04-24 13:47:55', NULL, NULL, NULL, 2),
(3, 'demo2', 'demo2 first', 'demo2 last', 'demo2', 1, 'N', '2016-04-24 13:47:55', NULL, NULL, NULL, 3),
(4, 'demo3', 'demo3 first', 'demo3 last', 'demo3', 1, 'N', '2016-04-24 13:47:55', NULL, NULL, NULL, 4),
(5, 'demo4', 'demo4 first', 'demo4 last', 'demo4', 1, 'N', '2016-04-24 13:47:55', NULL, NULL, NULL, 5),
(6, 'demo5', 'demo5 first', 'demo5 last', 'demo5', 1, 'N', '2016-04-24 14:15:53', NULL, NULL, NULL, 16),
(7, 'demo6', 'demo6 first', 'demo6 last', 'demo6', 1, 'N', '2016-05-03 23:03:03', NULL, NULL, NULL, 51),
(8, 'demo7', 'demo7 first', 'demo7 last', 'demo7', 1, 'N', '2016-05-03 23:03:03', NULL, NULL, NULL, 52),
(9, 'demo8', 'demo8 first', 'demo8 last', 'demo8', 1, 'N', '2016-05-03 23:03:03', NULL, NULL, NULL, 53),
(10, 'demo9', 'demo9 first', 'demo9 last', 'demo9', 1, 'N', '2016-05-03 23:03:03', NULL, NULL, NULL, 54),
(11, 'sagetech', 'sagetech first', 'sagetech last', 'SageTech', 1, 'Y', '2016-05-03 23:03:03', NULL, NULL, NULL, 55);

-- --------------------------------------------------------

--
-- Structure de la table `CLT_CLIENT_LANGUAGE`
--

CREATE TABLE IF NOT EXISTS `CLT_CLIENT_LANGUAGE` (
  `ID` bigint(20) NOT NULL,
  `CLIENT_ID` bigint(20) DEFAULT NULL,
  `INF_LANGUAGE_ID` bigint(20) DEFAULT NULL,
  `INF_PREFIX_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_CLIENT_LANGUAGE`
--

INSERT INTO `CLT_CLIENT_LANGUAGE` (`ID`, `CLIENT_ID`, `INF_LANGUAGE_ID`, `INF_PREFIX_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 2, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(3, 3, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(4, 4, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(5, 5, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(6, 6, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(7, 7, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(8, 8, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(9, 9, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(10, 10, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(11, 11, 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CLT_CLIENT_STATUS`
--

CREATE TABLE IF NOT EXISTS `CLT_CLIENT_STATUS` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_CLIENT_STATUS`
--

INSERT INTO `CLT_CLIENT_STATUS` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Active', NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CLT_MODEL_PARAMETER`
--

CREATE TABLE IF NOT EXISTS `CLT_MODEL_PARAMETER` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `DEFAULT_VALUE` text,
  `MODEL_PARAMETER_TYPE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_MODEL_PARAMETER`
--

INSERT INTO `CLT_MODEL_PARAMETER` (`ID`, `NAME`, `DESCRIPTION`, `DEFAULT_VALUE`, `MODEL_PARAMETER_TYPE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'La valeur pardéfaut de TVA', 'a2', '20', 1, 'Y', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CLT_MODEL_PARAMETER_MODEL`
--

CREATE TABLE IF NOT EXISTS `CLT_MODEL_PARAMETER_MODEL` (
  `ID` bigint(20) NOT NULL,
  `MODEL_PARAMETER_ID` bigint(20) DEFAULT NULL,
  `VALUE` longtext,
  `PM_MODEL_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `CLT_MODEL_PARAMETER_MODULE`
--

CREATE TABLE IF NOT EXISTS `CLT_MODEL_PARAMETER_MODULE` (
  `ID` bigint(20) NOT NULL,
  `MODEL_PARAMETER_ID` bigint(20) DEFAULT NULL,
  `VALUE` longtext,
  `MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_MODEL_PARAMETER_MODULE`
--

INSERT INTO `CLT_MODEL_PARAMETER_MODULE` (`ID`, `MODEL_PARAMETER_ID`, `VALUE`, `MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, '20', 1, 'Y', NULL, 1, NULL, NULL),
(2, 1, 'aaa', 3, 'Y', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CLT_MODEL_PARAMETER_TYPE`
--

CREATE TABLE IF NOT EXISTS `CLT_MODEL_PARAMETER_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_MODEL_PARAMETER_TYPE`
--

INSERT INTO `CLT_MODEL_PARAMETER_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'clt_module_parameter_type', 'type2', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CLT_MODULE`
--

CREATE TABLE IF NOT EXISTS `CLT_MODULE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `CLIENT_ID` bigint(20) DEFAULT NULL,
  `MODULE_STATUS_ID` bigint(20) DEFAULT NULL,
  `MODULE_TYPE_ID` bigint(20) DEFAULT NULL,
  `PARENT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `IMAGE_PATH` tinytext,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `PM_MODEL_ID` bigint(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_MODULE`
--

INSERT INTO `CLT_MODULE` (`ID`, `NAME`, `DESCRIPTION`, `CLIENT_ID`, `MODULE_STATUS_ID`, `MODULE_TYPE_ID`, `PARENT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `IMAGE_PATH`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `PM_MODEL_ID`) VALUES
(1, 'Gestion de stocks', '', 1, 1, 4, NULL, 1, 'Y', 'packing.png', NULL, NULL, NULL, NULL, 1),
(2, 'a', NULL, 1, 1, NULL, NULL, NULL, 'Y', 'packing.png', NULL, NULL, NULL, NULL, 0),
(3, 'Admin de stock', '', 1, 1, 3, 0, 3, 'Y', 'wheel.png', NULL, NULL, NULL, NULL, 2),
(4, NULL, NULL, 1, 1, NULL, NULL, NULL, 'Y', 'packing.png', NULL, NULL, NULL, NULL, 0),
(5, 'Configuration', NULL, 1, 1, 2, NULL, 5, 'Y', 'software.png', NULL, NULL, NULL, NULL, 3),
(6, NULL, NULL, 1, 1, NULL, NULL, NULL, 'Y', 'packing.png', NULL, NULL, NULL, NULL, 0),
(7, NULL, NULL, 1, 1, NULL, NULL, NULL, 'Y', 'packing.png', NULL, NULL, NULL, NULL, 0),
(8, NULL, NULL, 1, 1, NULL, NULL, NULL, 'Y', 'packing.png', NULL, NULL, NULL, NULL, 0),
(9, NULL, NULL, 1, 1, NULL, NULL, NULL, 'Y', 'packing.png', NULL, NULL, NULL, NULL, 0),
(10, 'webmaster', NULL, 1, 1, 1, NULL, NULL, 'Y', 'software.png', NULL, NULL, NULL, NULL, 4),
(11, 'Gestion de stocks', NULL, 11, 1, 4, NULL, NULL, 'Y', 'packing.png', NULL, NULL, NULL, NULL, 1),
(12, 'Admin de stock', NULL, 11, 1, 3, NULL, NULL, 'Y', 'wheel.png', NULL, NULL, NULL, NULL, 2),
(13, 'Configuration', NULL, 11, 1, 2, NULL, NULL, 'Y', 'software.png', NULL, NULL, NULL, NULL, 3);

-- --------------------------------------------------------

--
-- Structure de la table `CLT_MODULE_STATUS`
--

CREATE TABLE IF NOT EXISTS `CLT_MODULE_STATUS` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_MODULE_STATUS`
--

INSERT INTO `CLT_MODULE_STATUS` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'ACTIF', NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Desactivé', NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CLT_MODULE_TYPE`
--

CREATE TABLE IF NOT EXISTS `CLT_MODULE_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_MODULE_TYPE`
--

INSERT INTO `CLT_MODULE_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Webmaster', NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Super admin', NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(3, 'Admin', NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(4, 'Stock', NULL, 4, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CLT_PARAMETER`
--

CREATE TABLE IF NOT EXISTS `CLT_PARAMETER` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext CHARACTER SET latin1,
  `DEFAULT_VALUE` text,
  `PARAMETER_TYPE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `DESCRIPTION` tinytext CHARACTER SET latin1
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_PARAMETER`
--

INSERT INTO `CLT_PARAMETER` (`ID`, `NAME`, `DEFAULT_VALUE`, `PARAMETER_TYPE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `DESCRIPTION`) VALUES
(1, 'Nom de l''application ', 'My Stock Management', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(2, 'Titre de l''application', 'My Stock Management', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(3, 'La langue par défaut', '1', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(4, 'Le préfixe par défaut', '1', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(5, 'Activer les caches des ressources css, js et images', 'false', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(6, 'Le patterne de la date', 'yyyy-MM-dd', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(7, 'Le patterne de date et l''heure', 'yyyy-MM-dd HH:mm', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(8, 'Le patterne de l''heure', 'HH:mm', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(9, 'La devise (point sur inf_currency)', 'DH', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(10, 'Le nom du locataire', 'Saas212', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(11, 'Active le mode debug', 'false', 1, 'Y', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CLT_PARAMETER_CLIENT`
--

CREATE TABLE IF NOT EXISTS `CLT_PARAMETER_CLIENT` (
  `ID` bigint(20) NOT NULL,
  `VALUE` longtext,
  `CLINET_ID` bigint(20) DEFAULT NULL,
  `PARAMETER_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_PARAMETER_CLIENT`
--

INSERT INTO `CLT_PARAMETER_CLIENT` (`ID`, `VALUE`, `CLINET_ID`, `PARAMETER_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Gestion de stock pour SageTech', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, '1', 1, 3, 'Y', NULL, NULL, NULL, NULL),
(3, '1', 1, 4, 'Y', NULL, NULL, NULL, NULL),
(4, 'Y', 1, 5, 'Y', NULL, NULL, NULL, NULL),
(5, 'Gestion de stock || SageTech', 1, 2, 'Y', NULL, NULL, NULL, NULL),
(6, 'yyyy-MM-dd', 1, 6, 'Y', NULL, NULL, NULL, NULL),
(7, 'yyyy-MM-dd HH:mm', 1, 7, 'Y', NULL, NULL, NULL, NULL),
(8, 'HH:mm', 1, 8, 'Y', NULL, NULL, NULL, NULL),
(9, 'MAD', 1, 9, 'Y', NULL, NULL, NULL, NULL),
(10, 'SageTech.ma', 1, 10, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CLT_PARAMETER_TYPE`
--

CREATE TABLE IF NOT EXISTS `CLT_PARAMETER_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_PARAMETER_TYPE`
--

INSERT INTO `CLT_PARAMETER_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'type1', 'desc type 11', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CLT_STRUCTURE`
--

CREATE TABLE IF NOT EXISTS `CLT_STRUCTURE` (
  `ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `CLT_STRUCTURE_ROLE`
--

CREATE TABLE IF NOT EXISTS `CLT_STRUCTURE_ROLE` (
  `ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `CLT_STRUCTURE_TYPE`
--

CREATE TABLE IF NOT EXISTS `CLT_STRUCTURE_TYPE` (
  `ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `CLT_USER`
--

CREATE TABLE IF NOT EXISTS `CLT_USER` (
  `ID` bigint(20) NOT NULL,
  `FIRST_NAME` tinytext,
  `LAST_NAME` text,
  `USERNAME` text,
  `PASSWORD` text,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `CLIENT_ID` bigint(20) DEFAULT NULL,
  `USER_STATUS_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `IMAGE_PATH` tinytext,
  `ENTITY_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_USER`
--

INSERT INTO `CLT_USER` (`ID`, `FIRST_NAME`, `LAST_NAME`, `USERNAME`, `PASSWORD`, `CATEGORY_ID`, `CLIENT_ID`, `USER_STATUS_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `IMAGE_PATH`, `ENTITY_ID`) VALUES
(1, 'Abdessamad', 'HALLAL', 'user1.demo@saas212.com', '8308651804facb7b9af8ffc53a33a22d6a1c8ac2', 1, 1, 1, 'Y', NULL, NULL, '2016-09-25 12:53:07', 1, 'icons/2_48.png', 6),
(2, 'Super Admin', 'HALLAL', 'user2.demo@saas212.com', '8308651804facb7b9af8ffc53a33a22d6a1c8ac2', NULL, 1, 1, 'Y', NULL, NULL, NULL, 1, 'icons/2_48.png', 7),
(3, 'Admin', 'HALLAL', 'user3.demo@saas212.com', '8308651804facb7b9af8ffc53a33a22d6a1c8ac2', 3, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2_48.png', 8),
(4, 'Stock', 'HALLAL', 'user4.demo@saas212.com', '8308651804facb7b9af8ffc53a33a22d6a1c8ac2', 4, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2_48.png', 9),
(5, 'First Name', 'Last Name', 'user5.demo@saas212.com', '8308651804facb7b9af8ffc53a33a22d6a1c8ac2', NULL, 1, NULL, 'Y', '2016-04-24 14:00:39', NULL, NULL, NULL, 'icons/2_48.png', 10),
(6, 'First Name', 'Last Name', 'user6.demo@saas212.com', '8308651804facb7b9af8ffc53a33a22d6a1c8ac2', NULL, 1, NULL, 'Y', '2016-04-24 14:00:39', NULL, NULL, NULL, 'icons/2_48.png', 11),
(7, 'First Name', 'Last Name', 'user1.demo1@saas212.com', '8308651804facb7b9af8ffc53a33a22d6a1c8ac2', NULL, 1, NULL, 'Y', '2016-04-24 14:00:39', NULL, NULL, NULL, 'icons/2_48.png', 12),
(8, 'First Name', 'Last Name', 'user1.demo2@saas212.com', '8308651804facb7b9af8ffc53a33a22d6a1c8ac2', NULL, 1, NULL, 'Y', '2016-04-24 14:00:39', NULL, NULL, NULL, 'icons/2_48.png', 13),
(9, 'First Name', 'Last Name', 'user1.demo3@saas212.com', '8308651804facb7b9af8ffc53a33a22d6a1c8ac2', NULL, 1, NULL, 'Y', '2016-04-24 14:00:39', NULL, NULL, NULL, 'icons/2_48.png', 14),
(10, 'First Name', 'Last Name', 'user1.demo4@saas212.com', '8308651804facb7b9af8ffc53a33a22d6a1c8ac2', NULL, 1, NULL, 'Y', '2016-04-24 14:00:39', NULL, NULL, NULL, 'icons/2_48.png', 15),
(11, 'First Name', 'Last Name', 'user1.sagetech@saas212.com', '8308651804facb7b9af8ffc53a33a22d6a1c8ac2', 1, 11, NULL, 'Y', '2016-05-03 23:05:26', NULL, '2016-05-28 16:27:17', 11, 'icons/2_48.png', 56);

-- --------------------------------------------------------

--
-- Structure de la table `CLT_USER_CATEGORY`
--

CREATE TABLE IF NOT EXISTS `CLT_USER_CATEGORY` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_USER_CATEGORY`
--

INSERT INTO `CLT_USER_CATEGORY` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'WebMaster', NULL, 1, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'Super Admin', NULL, NULL, 'Y', 2, NULL, NULL, NULL, NULL),
(3, 'Admin', NULL, NULL, 'Y', 3, NULL, NULL, NULL, NULL),
(4, 'Stock', NULL, NULL, 'Y', 4, NULL, NULL, NULL, NULL),
(5, 'Autre', NULL, NULL, 'Y', 5, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CLT_USER_CLIENT`
--

CREATE TABLE IF NOT EXISTS `CLT_USER_CLIENT` (
  `ID` bigint(20) NOT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  `CLIENT_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_USER_CLIENT`
--

INSERT INTO `CLT_USER_CLIENT` (`ID`, `USER_ID`, `CLIENT_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 1, 2, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CLT_USER_GROUP`
--

CREATE TABLE IF NOT EXISTS `CLT_USER_GROUP` (
  `ID` bigint(20) NOT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  `INF_GROUP_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_USER_GROUP`
--

INSERT INTO `CLT_USER_GROUP` (`ID`, `USER_ID`, `INF_GROUP_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(2, 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CLT_USER_MODULE`
--

CREATE TABLE IF NOT EXISTS `CLT_USER_MODULE` (
  `ID` bigint(20) NOT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  `MODULE_ID` bigint(20) DEFAULT NULL,
  `FULL CONTROL` char(1) CHARACTER SET latin1 DEFAULT 'N',
  `MODULE_ID_TO_MANAGER` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_USER_MODULE`
--

INSERT INTO `CLT_USER_MODULE` (`ID`, `USER_ID`, `MODULE_ID`, `FULL CONTROL`, `MODULE_ID_TO_MANAGER`, `ACTIVE`, `SORT_KEY`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(41, 1, 1, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(42, 1, 3, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(43, 1, 5, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(50, 11, 11, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(51, 11, 12, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(52, 11, 13, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CLT_USER_STATUS`
--

CREATE TABLE IF NOT EXISTS `CLT_USER_STATUS` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_USER_STATUS`
--

INSERT INTO `CLT_USER_STATUS` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Actif 1', NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_EMAIL_TYPE`
--

CREATE TABLE IF NOT EXISTS `CTA_EMAIL_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_EMAIL_TYPE`
--

INSERT INTO `CTA_EMAIL_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'E-mail professionnel', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'E-mail personnel', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_ENTITY`
--

CREATE TABLE IF NOT EXISTS `CTA_ENTITY` (
  `ID` bigint(20) NOT NULL,
  `PARTY_TYPE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_ENTITY`
--

INSERT INTO `CTA_ENTITY` (`ID`, `PARTY_TYPE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, NULL, NULL, NULL, NULL),
(2, 1, NULL, NULL, NULL, NULL),
(3, 1, NULL, NULL, NULL, NULL),
(4, 1, NULL, NULL, NULL, NULL),
(5, 1, NULL, NULL, NULL, NULL),
(6, NULL, NULL, NULL, NULL, NULL),
(7, NULL, NULL, NULL, NULL, NULL),
(8, NULL, NULL, NULL, NULL, NULL),
(9, NULL, NULL, NULL, NULL, NULL),
(10, NULL, NULL, NULL, NULL, NULL),
(11, NULL, NULL, NULL, NULL, NULL),
(12, NULL, NULL, NULL, NULL, NULL),
(13, NULL, NULL, NULL, NULL, NULL),
(14, NULL, NULL, NULL, NULL, NULL),
(15, NULL, NULL, NULL, NULL, NULL),
(16, 1, NULL, NULL, NULL, NULL),
(30, 2, NULL, NULL, NULL, NULL),
(31, NULL, NULL, NULL, NULL, 958),
(32, 2, NULL, NULL, NULL, NULL),
(33, 2, NULL, NULL, NULL, NULL),
(34, 2, NULL, NULL, NULL, NULL),
(35, 2, NULL, NULL, NULL, NULL),
(51, 1, NULL, NULL, NULL, NULL),
(52, 1, NULL, NULL, NULL, NULL),
(53, 1, NULL, NULL, NULL, NULL),
(54, 1, NULL, NULL, NULL, NULL),
(55, 1, NULL, NULL, NULL, NULL),
(56, 2, NULL, NULL, NULL, NULL),
(57, 4, NULL, NULL, NULL, NULL),
(58, 3, NULL, NULL, NULL, NULL),
(59, 4, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_ENTITY_EMAIL`
--

CREATE TABLE IF NOT EXISTS `CTA_ENTITY_EMAIL` (
  `ID` bigint(20) NOT NULL,
  `EMAIL_ADDRESS` tinytext,
  `IS_FOR_NOTIFICATION` char(1) DEFAULT NULL,
  `ENTITY_ID` bigint(20) DEFAULT NULL,
  `PRIORITY` bigint(20) DEFAULT NULL,
  `IS_PRINCIPAL` char(1) DEFAULT NULL,
  `EMAIL_TYPE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_ENTITY_EMAIL`
--

INSERT INTO `CTA_ENTITY_EMAIL` (`ID`, `EMAIL_ADDRESS`, `IS_FOR_NOTIFICATION`, `ENTITY_ID`, `PRIORITY`, `IS_PRINCIPAL`, `EMAIL_TYPE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(4, 'email@exemple.com', NULL, 3, 1, NULL, 2, NULL, NULL, NULL, NULL),
(5, 'email@exemple.com', NULL, 2, 12, NULL, 1, NULL, NULL, NULL, NULL),
(6, 'email@exemple.com', NULL, 1, 1, NULL, 2, NULL, NULL, NULL, NULL),
(7, 'email@exemple.com', NULL, 6, 1, NULL, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_ENTITY_FAX`
--

CREATE TABLE IF NOT EXISTS `CTA_ENTITY_FAX` (
  `ID` bigint(20) NOT NULL,
  `COUNTRY_CODE` tinytext,
  `NUMBER` tinytext,
  `PRIORITY` bigint(20) DEFAULT NULL,
  `ENTITY_ID` bigint(20) DEFAULT NULL,
  `FAX_TYPE_ID` bigint(20) DEFAULT NULL,
  `IS_PRINCIPAL` char(1) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_ENTITY_FAX`
--

INSERT INTO `CTA_ENTITY_FAX` (`ID`, `COUNTRY_CODE`, `NUMBER`, `PRIORITY`, `ENTITY_ID`, `FAX_TYPE_ID`, `IS_PRINCIPAL`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(2, '+212', '6 61 76 89', 1, 1, 2, NULL, NULL, NULL, NULL, NULL),
(3, '+212', '6 61 76 89', 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(4, '+212', '6 61 76 89', 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(5, '+212', '6 61 76 89', 1, 57, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_ENTITY_LOCATION`
--

CREATE TABLE IF NOT EXISTS `CTA_ENTITY_LOCATION` (
  `ID` bigint(20) NOT NULL,
  `ADDRESS_LINE_1` tinytext,
  `ADDRESS_LINE_2` tinytext,
  `ADDRESS_LINE_3` tinytext,
  `INF_COUNTRY_ID` bigint(20) DEFAULT NULL,
  `INF_CITY_ID` bigint(20) DEFAULT NULL,
  `POSTAL_CODE` tinytext,
  `PRIORITY` bigint(20) DEFAULT NULL,
  `ENTITY_ID` bigint(20) DEFAULT NULL,
  `LOCATION_TYPE_ID` bigint(20) DEFAULT NULL,
  `IS_PRINCIPAL` char(1) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_ENTITY_LOCATION`
--

INSERT INTO `CTA_ENTITY_LOCATION` (`ID`, `ADDRESS_LINE_1`, `ADDRESS_LINE_2`, `ADDRESS_LINE_3`, `INF_COUNTRY_ID`, `INF_CITY_ID`, `POSTAL_CODE`, `PRIORITY`, `ENTITY_ID`, `LOCATION_TYPE_ID`, `IS_PRINCIPAL`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(3, 'Casa technopart ', 'bloc 1', 'num 3', 1, 1, '40000', 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(4, 'Casa technopart ', 'bloc 1', 'num 3', 1, 2, '40000', 1, 3, 2, NULL, NULL, NULL, NULL, NULL),
(7, 'Casa technopart ', 'bloc 1', 'num 3', 1, 2, '40000', 1, 2, 1, NULL, NULL, NULL, NULL, NULL),
(8, 'Casa technopart ', 'bloc 1', 'num 3', 1, 2, '40000', 1, 6, 2, NULL, NULL, NULL, NULL, NULL),
(9, 'Casa technopart ', 'bloc 1', 'num 3', 1, 3, '40000', 1, 7, 1, NULL, NULL, NULL, NULL, NULL),
(10, 'Casa technopart ', 'bloc 1', 'num 3', 1, 1, '40000', 1, 34, 2, NULL, NULL, NULL, NULL, NULL),
(11, 'Casa technopart ', 'bloc 1', 'num 3', 1, 4, '40000', 1, 1, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_ENTITY_PHONE`
--

CREATE TABLE IF NOT EXISTS `CTA_ENTITY_PHONE` (
  `ID` bigint(20) NOT NULL,
  `COUNTRY_CODE` tinytext,
  `NUMBER` tinytext,
  `ENTITY_ID` bigint(20) DEFAULT NULL,
  `PHONE_TYPE_ID` bigint(20) DEFAULT NULL,
  `PRIORITY` bigint(20) DEFAULT NULL,
  `IS_PRINCIPAL` char(1) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_ENTITY_PHONE`
--

INSERT INTO `CTA_ENTITY_PHONE` (`ID`, `COUNTRY_CODE`, `NUMBER`, `ENTITY_ID`, `PHONE_TYPE_ID`, `PRIORITY`, `IS_PRINCIPAL`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(5, '+212', '6 61 76 89', 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(6, '+212', '6 61 76 89', 3, 2, 1, NULL, NULL, NULL, NULL, NULL),
(7, '+212', '6 61 76 89', 1, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(8, '+212', '6 61 76 89', 1, 3, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_ENTITY_TYPE`
--

CREATE TABLE IF NOT EXISTS `CTA_ENTITY_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_ENTITY_TYPE`
--

INSERT INTO `CTA_ENTITY_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Client / Tenant', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'Utilisateur', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Fournisseur', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'CLient / Customer', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_ENTITY_WEB`
--

CREATE TABLE IF NOT EXISTS `CTA_ENTITY_WEB` (
  `ID` bigint(20) NOT NULL,
  `URL` tinytext,
  `PRIORITY` bigint(20) DEFAULT NULL,
  `ENTITY_ID` bigint(20) DEFAULT NULL,
  `WEB_TYPE_ID` bigint(20) DEFAULT NULL,
  `IS_PRINCIPAL` char(1) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_ENTITY_WEB`
--

INSERT INTO `CTA_ENTITY_WEB` (`ID`, `URL`, `PRIORITY`, `ENTITY_ID`, `WEB_TYPE_ID`, `IS_PRINCIPAL`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(14, 'https://saas212.com', 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(15, 'https://saas212.com', 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(16, 'https://saas212.com', 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(17, 'https://saas212.com', 1, 1, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_FAX_TYPE`
--

CREATE TABLE IF NOT EXISTS `CTA_FAX_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_FAX_TYPE`
--

INSERT INTO `CTA_FAX_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Télécopieur professionnel', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'Télécopieur personnel', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(3, 'Télécopieur temporaire', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_LOCATION_TYPE`
--

CREATE TABLE IF NOT EXISTS `CTA_LOCATION_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_LOCATION_TYPE`
--

INSERT INTO `CTA_LOCATION_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Adresse du siège social', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'Adresse de correspondance', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(3, 'Adresse professionnelle', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(4, 'Adresse personnelle', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_PHONE_TYPE`
--

CREATE TABLE IF NOT EXISTS `CTA_PHONE_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='		';

--
-- Contenu de la table `CTA_PHONE_TYPE`
--

INSERT INTO `CTA_PHONE_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Téléphone professionnel', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'Téléphone personnel', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(3, 'Téléphone mobile', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(4, 'Téléphone standard', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_WEB_TYPE`
--

CREATE TABLE IF NOT EXISTS `CTA_WEB_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_WEB_TYPE`
--

INSERT INTO `CTA_WEB_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Site web professionnel', NULL, 1, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'Site web personnel', NULL, 2, 'Y', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `INF_BASIC_PARAMETER`
--

CREATE TABLE IF NOT EXISTS `INF_BASIC_PARAMETER` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext CHARACTER SET latin1,
  `DESCRIPTION` text CHARACTER SET latin1,
  `VALUE` text,
  `BASIC_PARAMETER_TYPE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `INF_BASIC_PARAMETER`
--

INSERT INTO `INF_BASIC_PARAMETER` (`ID`, `NAME`, `DESCRIPTION`, `VALUE`, `BASIC_PARAMETER_TYPE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Le liens publique de l''application', 'Le liens publique de l''application', 'http://localhost:8083/portal/', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Les droits de la société', 'Les droits de la société', 'Copyright © 2016', 1, 'Y', NULL, NULL, NULL, NULL),
(3, 'Le nom de la société', 'Le nom de la société', 'Saas212', 1, 'Y', NULL, NULL, NULL, NULL),
(4, 'Rafraichir tous les composants', 'Cette variable pour reloader les ressources pour chaque requêtes HTTP', 'http://localhost:8083/portal/servlet/refresh?username=user&password=pass', 1, 'Y', NULL, NULL, NULL, NULL),
(5, 'Le site officiel de projet', 'Le site officielle de projet', 'http://www.saas212.com', 1, 'Y', NULL, NULL, NULL, NULL),
(6, 'Le lien pour obtenir d''un compte', 'Le lien pour obtenir d''un compte', 'http://www.saas212.com/ObtenirCompte', 1, 'Y', NULL, NULL, NULL, NULL),
(7, 'Version de produit', 'Version de produit', '1.0 Beta', 1, 'Y', NULL, NULL, NULL, NULL),
(8, 'Environnement', 'Environnement', 'LOCAL', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `INF_BASIC_PARAMETER_TYPE`
--

CREATE TABLE IF NOT EXISTS `INF_BASIC_PARAMETER_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `INF_BASIC_PARAMETER_TYPE`
--

INSERT INTO `INF_BASIC_PARAMETER_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'basic config', 'basic config', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'type 1', 'esc type 1', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `INF_CITY`
--

CREATE TABLE IF NOT EXISTS `INF_CITY` (
  `ID` bigint(20) NOT NULL,
  `CODE` tinytext CHARACTER SET latin1,
  `NAME` tinytext,
  `DESCRIPTION` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `COUNTRY_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=133 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `INF_CITY`
--

INSERT INTO `INF_CITY` (`ID`, `CODE`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `COUNTRY_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, NULL, 'city 1', NULL, NULL, 2, 'N', NULL, NULL, NULL, NULL),
(2, NULL, 'city 2', NULL, NULL, 2, 'N', NULL, NULL, NULL, NULL),
(3, NULL, 'city 3', NULL, NULL, 2, 'N', NULL, NULL, NULL, NULL),
(4, NULL, 'city 4', NULL, NULL, 2, 'N', NULL, NULL, NULL, NULL),
(5, NULL, 'Agadir', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(6, NULL, 'Ain Harrouda', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(7, NULL, 'Ait Baha', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(8, NULL, 'Ait Melloul', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(9, NULL, 'Al Haouz', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(10, NULL, 'Al Hocïema', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(11, NULL, 'Aousserd', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(12, NULL, 'Arfoud', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(13, NULL, 'Assa zag', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(14, NULL, 'Assilah', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(15, NULL, 'Azemmour', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(16, NULL, 'Azilal', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(17, NULL, 'Azrou', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(18, NULL, 'Ben Ahmed', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(19, NULL, 'Ben Guerir', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(20, NULL, 'Béni Mellal', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(21, NULL, 'Benslimane', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(22, NULL, 'Berkane', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(23, NULL, 'Berrechid', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(24, NULL, 'Bin El Ouidane', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(25, NULL, 'Bir Jdid', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(26, NULL, 'Boujdour', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(27, NULL, 'Boujniba', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(28, NULL, 'Boulanouar', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(29, NULL, 'Boulmane', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(30, NULL, 'Bouskoura', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(31, NULL, 'Bouznika', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(32, NULL, 'Casablanca', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(33, NULL, 'Chefchaouen', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(34, NULL, 'Chichaoua', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(35, NULL, 'Dakhla', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(36, NULL, 'Dar Chaffai', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(37, NULL, 'Deroua', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(38, NULL, 'El Borouj', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(39, NULL, 'El Gara', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(40, NULL, 'El Hajeb', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(41, NULL, 'El Harhoura', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(42, NULL, 'El Jadida', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(43, NULL, 'El Mansouria', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(44, NULL, 'Errachidia', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(45, NULL, 'Es-Semara', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(46, NULL, 'Essaouira', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(47, NULL, 'Fès', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(48, NULL, 'Fnideq', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(49, NULL, 'Fquih Ben Saleh', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(50, NULL, 'Goulmima', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(51, NULL, 'Guelmim', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(52, NULL, 'Guercif', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(53, NULL, 'Had Soualem', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(54, NULL, 'Ifrane', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(55, NULL, 'Imouzzer', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(56, NULL, 'Inzegan', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(57, NULL, 'Jamaat Shaim', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(58, NULL, 'Jrada', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(59, NULL, 'Kelaat Es-Sraghna', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(60, NULL, 'Kénitra', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(61, NULL, 'Khemisset', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(62, NULL, 'Khénifra', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(63, NULL, 'Khouribga', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(64, NULL, 'Ksar el-Kebir', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(65, NULL, 'Ksar es-Seghir', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(66, NULL, 'Laâyoune', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(67, NULL, 'Lagouira', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(68, NULL, 'Lakhiaita', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(69, NULL, 'Larache', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(70, NULL, 'Marrakech', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(71, NULL, 'Martil', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(72, NULL, 'Mdiq', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(73, NULL, 'Mediouna', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(74, NULL, 'Mehdia', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(75, NULL, 'Meknès', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(76, NULL, 'Merzouga', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(77, NULL, 'Midelt', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(78, NULL, 'Mirleft', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(79, NULL, 'Mohammedia', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(80, NULL, 'Moulay Bousselham', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(81, NULL, 'Moulay Yaccoub', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(82, NULL, 'Nador', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(83, NULL, 'Nouaceur', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(84, NULL, 'Oualidia', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(85, NULL, 'Ouarzazate', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(86, NULL, 'Ouazzane', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(87, NULL, 'Oued Zem', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(88, NULL, 'Oujda', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(89, NULL, 'Ouled Frej', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(90, NULL, 'Outat El Haj', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(91, NULL, 'Rabat', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(92, NULL, 'Rissani', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(93, NULL, 'Safi', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(94, NULL, 'Saidia', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(95, NULL, 'Sala Al-Jadida', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(96, NULL, 'Salé', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(97, NULL, 'Sebt Gzoula', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(98, NULL, 'Sefrou', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(99, NULL, 'Selouane', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(100, NULL, 'Settat', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(101, NULL, 'Sidi Bennour', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(102, NULL, 'Sidi Bouzid', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(103, NULL, 'Sidi el Aîdi', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(104, NULL, 'Sidi Hajjaj', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(105, NULL, 'Sidi Ifni', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(106, NULL, 'Sidi Kacem', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(107, NULL, 'Sidi Rahhal', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(108, NULL, 'Sidi Slimane', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(109, NULL, 'Sidi Yahya El Gharb', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(110, NULL, 'Skhirat', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(111, NULL, 'Soualem', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(112, NULL, 'Taghazout', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(113, NULL, 'Tamaris', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(114, NULL, 'Tamensourt', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(115, NULL, 'Tamouda Bay', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(116, NULL, 'Tan-Tan', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(117, NULL, 'Tanger', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(118, NULL, 'Taounate', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(119, NULL, 'Taourirt', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(120, NULL, 'Tarfaya', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(121, NULL, 'Taroudant', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(122, NULL, 'Tata', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(123, NULL, 'Taza', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(124, NULL, 'Temara', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(125, NULL, 'Temsia', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(126, NULL, 'Tétouan', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(127, NULL, 'Tifelt', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(128, NULL, 'Tikiouine', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(129, NULL, 'Tiznit', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(130, NULL, 'Youssoufia', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(131, NULL, 'Zagora', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(132, NULL, 'Zemamra', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `INF_COUNTRY`
--

CREATE TABLE IF NOT EXISTS `INF_COUNTRY` (
  `ID` bigint(20) NOT NULL,
  `CODE` tinytext CHARACTER SET latin1,
  `NAME` tinytext,
  `DESCRIPTION` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `INF_COUNTRY`
--

INSERT INTO `INF_COUNTRY` (`ID`, `CODE`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, NULL, 'Maroc', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, NULL, 'country 1', NULL, NULL, 'N', NULL, NULL, NULL, NULL),
(3, NULL, 'country 2', NULL, NULL, 'N', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `INF_CURRENCY`
--

CREATE TABLE IF NOT EXISTS `INF_CURRENCY` (
  `ID` bigint(20) NOT NULL,
  `CODE` tinytext,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `SYMBOL` tinytext,
  `FORMAT` tinytext,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `INF_CURRENCY`
--

INSERT INTO `INF_CURRENCY` (`ID`, `CODE`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `SYMBOL`, `FORMAT`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'MAD', 'Dirham marocain', NULL, NULL, 'Y', 'DH', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `INF_GROUP`
--

CREATE TABLE IF NOT EXISTS `INF_GROUP` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `INF_GROUP`
--

INSERT INTO `INF_GROUP` (`ID`, `NAME`, `DESCRIPTION`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'group 1', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `INF_ITEM`
--

CREATE TABLE IF NOT EXISTS `INF_ITEM` (
  `CODE` varchar(255) NOT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` varchar(45) DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `INF_ITEM`
--

INSERT INTO `INF_ITEM` (`CODE`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
('basicParameter', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1.basicParameterType', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1.description', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1.id', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1.name', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1.value', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2.basicParameterType', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2.description', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2.id', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2.name', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2.value', 'Y', NULL, NULL, NULL, NULL),
('blValidation', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.customerName', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.description', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.id', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.sumQuantity', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.sumTotalPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s2', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s2.customer', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s2.description', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.color', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.size', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.color', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.price', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.quantity', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.reference', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.size', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.totalPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s5', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s5.sumQuantity', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s5.sumTotal', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s6', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s1', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s1.customer', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s1.description', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.color', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.quantity', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.size', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.color', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.price', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.size', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.totalPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s4', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s4.sumQuantity', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s4.sumTotal', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s5', 'Y', NULL, NULL, NULL, NULL),
('contactManagement', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1.addressLine', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1.ctaLocationLine1', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1.ctaLocationLine2', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1.ctaLocationLine3', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1.ctaLocationPostalCode', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1.ctaLocationPriority', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1.infCityName', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1.infCountryName', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1.locationTypeName', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1.postalCode', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1.priority', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s2', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s2.countryCode', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s2.ctaPhoneCountryCode', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s2.ctaPhoneNumber', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s2.ctaPhonePriority', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s2.number', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s2.phoneTypeName', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s2.priority', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s3', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s3.countryCode', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s3.ctaFaxCountryCode', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s3.ctaFaxNumber', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s3.ctaFaxPriority', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s3.faxTypeName', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s3.number', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s3.priority', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s4', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s4.ctaWebPriority', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s4.ctaWebUrl', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s4.externalUrl', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s4.priority', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s4.webTypeName', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s5', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s5.ctaEmailAdresse', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s5.ctaEmailPriority', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s5.emailAddress', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s5.emailTypeName', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s5.priority', 'Y', NULL, NULL, NULL, NULL),
('customer', 'Y', NULL, NULL, NULL, NULL),
('customer.s1', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.active', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.adress', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.category', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.city', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.company', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.country', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.email', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.firstName', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.fixedPhone', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.fullLabel', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.lastName', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.note', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.secondaryAddress', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.shortLabel', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.type', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.webSite', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.zipCode', 'Y', NULL, NULL, NULL, NULL),
('customer.s2', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.adress', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.city', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.country', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.email', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.firstName', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.fixedPhone', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.lastName', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.type', 'Y', NULL, NULL, NULL, NULL),
('globals', 'Y', NULL, NULL, NULL, NULL),
('globals.forms', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.add', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.cancel', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.clean', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.confirmClean', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.confirmDelete', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.delete', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.edit', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.no', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.save', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.search', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.validate', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.yes', 'Y', NULL, NULL, NULL, NULL),
('globals.list', 'Y', NULL, NULL, NULL, NULL),
('globals.list.activate', 'Y', NULL, NULL, NULL, NULL),
('globals.list.confirmDelete', 'Y', NULL, NULL, NULL, NULL),
('globals.list.delete', 'Y', NULL, NULL, NULL, NULL),
('globals.list.edit', 'Y', NULL, NULL, NULL, NULL),
('globals.list.option', 'Y', NULL, NULL, NULL, NULL),
('globals.list.validate', 'Y', NULL, NULL, NULL, NULL),
('globals.list.vide', 'Y', NULL, NULL, NULL, NULL),
('inventaire', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.active', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.designation', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.priceBuy', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.priceSale', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.productColorName', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.productSizeName', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.quantity', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.quantityCount', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.receptionValidCount', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.unitPriceBuyMax', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.unitPriceBuyMin', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.unitPriceBuyMoyenne', 'Y', NULL, NULL, NULL, NULL),
('login', 'Y', NULL, NULL, NULL, NULL),
('login.s1', 'Y', NULL, NULL, NULL, NULL),
('login.s1.forgetPassword', 'Y', NULL, NULL, NULL, NULL),
('login.s1.getAccount', 'Y', NULL, NULL, NULL, NULL),
('login.s1.login', 'Y', NULL, NULL, NULL, NULL),
('login.s1.password', 'Y', NULL, NULL, NULL, NULL),
('login.s1.sessionActive', 'Y', NULL, NULL, NULL, NULL),
('login.s1.username', 'Y', NULL, NULL, NULL, NULL),
('module', 'Y', NULL, NULL, NULL, NULL),
('module.s1', 'Y', NULL, NULL, NULL, NULL),
('module.s1.access', 'Y', NULL, NULL, NULL, NULL),
('module.s1.logout', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1.description', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1.id', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1.moduleParameterType', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1.name', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1.value', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2.description', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2.id', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2.moduleParameterType', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2.name', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2.value', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s1', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s1.company', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s1.email', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s1.name', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s2', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s2.descriptionModule', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s2.moduleTypeName', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s2.nameModule', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3.client', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3.defaultValue', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3.parameterModuleName', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3.parameterModuleTypeName', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3.value', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s4', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s4.client', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s4.parameterModule', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s4.parameterModuleDefaultValue', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s4.value', 'Y', NULL, NULL, NULL, NULL),
('p003', 'Y', NULL, NULL, NULL, NULL),
('p003.s1', 'Y', NULL, NULL, NULL, NULL),
('p003.s1.amount', 'Y', NULL, NULL, NULL, NULL),
('p003.s1.description', 'Y', NULL, NULL, NULL, NULL),
('p003.s1.expenseType', 'Y', NULL, NULL, NULL, NULL),
('p003.s1.expenseTypeName', 'Y', NULL, NULL, NULL, NULL),
('p003.s1.name', 'Y', NULL, NULL, NULL, NULL),
('p003.s2', 'Y', NULL, NULL, NULL, NULL),
('p003.s2.amount', 'Y', NULL, NULL, NULL, NULL),
('p003.s2.description', 'Y', NULL, NULL, NULL, NULL),
('p003.s2.name', 'Y', NULL, NULL, NULL, NULL),
('p003.s2.type', 'Y', NULL, NULL, NULL, NULL),
('p004', 'Y', NULL, NULL, NULL, NULL),
('p004.s1', 'Y', NULL, NULL, NULL, NULL),
('p004.s1.active', 'Y', NULL, NULL, NULL, NULL),
('p004.s1.companyName', 'Y', NULL, NULL, NULL, NULL),
('p004.s1.email', 'Y', NULL, NULL, NULL, NULL),
('p004.s1.name', 'Y', NULL, NULL, NULL, NULL),
('p004.s1.phone', 'Y', NULL, NULL, NULL, NULL),
('p004.s1.type', 'Y', NULL, NULL, NULL, NULL),
('p004.s2', 'Y', NULL, NULL, NULL, NULL),
('p004.s2.active', 'Y', NULL, NULL, NULL, NULL),
('p004.s2.category', 'Y', NULL, NULL, NULL, NULL),
('p004.s2.companyName', 'Y', NULL, NULL, NULL, NULL),
('p004.s2.firstName', 'Y', NULL, NULL, NULL, NULL),
('p004.s2.fullLabel', 'Y', NULL, NULL, NULL, NULL),
('p004.s2.lastName', 'Y', NULL, NULL, NULL, NULL),
('p004.s2.nature', 'Y', NULL, NULL, NULL, NULL),
('p004.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p004.s2.representative', 'Y', NULL, NULL, NULL, NULL),
('p004.s2.shortLabel', 'Y', NULL, NULL, NULL, NULL),
('p004.s2.type', 'Y', NULL, NULL, NULL, NULL),
('p005.s1', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.searchColor', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.searchDepartment', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.searchDesignation', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.searchFamily', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.searchQte', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.searchReference', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.searchSize', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.searchType', 'Y', NULL, NULL, NULL, NULL),
('p005.s2', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.designation', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.priceBuy', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.priceSale', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.productFamilyName', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.productGroupName', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.productTypeName', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.quantity', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.threshold', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.active', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.additionalInformation', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.basicInformation', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.color', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.family', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.group', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.note', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.priceBuy', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.priceSale', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.reorganization', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.size', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.threshold', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.type', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.unit', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.code', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.deadline', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.deposit', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.souche', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p007', 'Y', NULL, NULL, NULL, NULL),
('p007.s1', 'Y', NULL, NULL, NULL, NULL),
('p007.s1.active', 'Y', NULL, NULL, NULL, NULL),
('p007.s1.companyName', 'Y', NULL, NULL, NULL, NULL),
('p007.s1.email', 'Y', NULL, NULL, NULL, NULL),
('p007.s1.name', 'Y', NULL, NULL, NULL, NULL),
('p007.s1.phone', 'Y', NULL, NULL, NULL, NULL),
('p007.s1.type', 'Y', NULL, NULL, NULL, NULL),
('p007.s2', 'Y', NULL, NULL, NULL, NULL),
('p007.s2.active', 'Y', NULL, NULL, NULL, NULL),
('p007.s2.category', 'Y', NULL, NULL, NULL, NULL),
('p007.s2.companyName', 'Y', NULL, NULL, NULL, NULL),
('p007.s2.firstName', 'Y', NULL, NULL, NULL, NULL),
('p007.s2.fullLabel', 'Y', NULL, NULL, NULL, NULL),
('p007.s2.lastName', 'Y', NULL, NULL, NULL, NULL),
('p007.s2.nature', 'Y', NULL, NULL, NULL, NULL),
('p007.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p007.s2.representative', 'Y', NULL, NULL, NULL, NULL),
('p007.s2.shortLabel', 'Y', NULL, NULL, NULL, NULL),
('p007.s2.type', 'Y', NULL, NULL, NULL, NULL),
('p008', 'Y', NULL, NULL, NULL, NULL),
('p008.s1', 'Y', NULL, NULL, NULL, NULL),
('p008.s1.designation', 'Y', NULL, NULL, NULL, NULL),
('p008.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p008.s2.designation', 'Y', NULL, NULL, NULL, NULL),
('p008.s2.quantity', 'Y', NULL, NULL, NULL, NULL),
('p008.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p008.s2.threshold', 'Y', NULL, NULL, NULL, NULL),
('p008.s2.totalCommanded', 'Y', NULL, NULL, NULL, NULL),
('p008.s2.totalRecieved', 'Y', NULL, NULL, NULL, NULL),
('p009', 'Y', NULL, NULL, NULL, NULL),
('p009.s1', 'Y', NULL, NULL, NULL, NULL),
('p009.s1.designation', 'Y', NULL, NULL, NULL, NULL),
('p009.s1.orderSupplierReference', 'Y', NULL, NULL, NULL, NULL),
('p009.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p009.s1.supplier', 'Y', NULL, NULL, NULL, NULL),
('p009.s2', 'Y', NULL, NULL, NULL, NULL),
('p009.s2.designation', 'Y', NULL, NULL, NULL, NULL),
('p009.s2.orderSupplierReference', 'Y', NULL, NULL, NULL, NULL),
('p009.s2.qteCommanded', 'Y', NULL, NULL, NULL, NULL),
('p009.s2.qteRecieved', 'Y', NULL, NULL, NULL, NULL),
('p009.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p009.s2.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p012.s1', 'Y', NULL, NULL, NULL, NULL),
('p012.s1.productTotalQuantity', 'Y', NULL, NULL, NULL, NULL),
('p012.s2', 'Y', NULL, NULL, NULL, NULL),
('p012.s2.orderSupplierInProgressCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s2.orderSupplierRefusedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s2.orderSupplierRejectedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s2.orderSupplierTransmittedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s2.orderSupplierValidatedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s3', 'Y', NULL, NULL, NULL, NULL),
('p012.s3.receptionInProgressCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s3.receptionRefusedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s3.receptionRejectedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s3.receptionTransmittedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s3.receptionValidatedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s4', 'Y', NULL, NULL, NULL, NULL),
('p012.s4.orderdRefusedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s4.orderdRejectedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s4.orderInProgressCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s4.orderTransmittedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s4.orderValidatedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s5', 'Y', NULL, NULL, NULL, NULL),
('p012.s5.returnReceiptInProgressCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s5.returnReceiptRefusedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s5.returnReceiptRejectedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s5.returnReceiptTransmittedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s5.returnReceiptValidatedCount', 'Y', NULL, NULL, NULL, NULL),
('p029.s1', 'Y', NULL, NULL, NULL, NULL),
('p029.s1.active', 'Y', NULL, NULL, NULL, NULL),
('p029.s1.description', 'Y', NULL, NULL, NULL, NULL),
('p029.s1.name', 'Y', NULL, NULL, NULL, NULL),
('p030', 'Y', NULL, NULL, NULL, NULL),
('p030.s1', 'Y', NULL, NULL, NULL, NULL),
('p030.s1.active', 'Y', NULL, NULL, NULL, NULL),
('p030.s1.category', 'Y', NULL, NULL, NULL, NULL),
('p030.s1.entityEmail', 'Y', NULL, NULL, NULL, NULL),
('p030.s1.entityPhone', 'Y', NULL, NULL, NULL, NULL),
('p030.s1.firstName', 'Y', NULL, NULL, NULL, NULL),
('p030.s1.fullName', 'Y', NULL, NULL, NULL, NULL),
('p030.s1.lastName', 'Y', NULL, NULL, NULL, NULL),
('p030.s1.username', 'Y', NULL, NULL, NULL, NULL),
('p030.s2', 'Y', NULL, NULL, NULL, NULL),
('p030.s2.active', 'Y', NULL, NULL, NULL, NULL),
('p030.s2.category', 'Y', NULL, NULL, NULL, NULL),
('p030.s2.firstName', 'Y', NULL, NULL, NULL, NULL),
('p030.s2.lastName', 'Y', NULL, NULL, NULL, NULL),
('p030.s2.password', 'Y', NULL, NULL, NULL, NULL),
('p030.s2.username', 'Y', NULL, NULL, NULL, NULL),
('p032', 'Y', NULL, NULL, NULL, NULL),
('p032.s1', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.addReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.deleteReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.editReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p032.s2', 'Y', NULL, NULL, NULL, NULL),
('p032.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p032.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p032.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p032.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p032.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p032.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p032.s2.validate', 'Y', NULL, NULL, NULL, NULL),
('p032.s3', 'Y', NULL, NULL, NULL, NULL),
('p032.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p032.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p032.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p032.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p032.s3.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p032.s4', 'Y', NULL, NULL, NULL, NULL),
('p032.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p032.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p032.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p032.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p032.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p032.s5', 'Y', NULL, NULL, NULL, NULL),
('p032.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p032.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p032.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p032.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p032.s5.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p033', 'Y', NULL, NULL, NULL, NULL),
('p033.s1', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.addReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.deleteReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.editReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p033.s2', 'Y', NULL, NULL, NULL, NULL),
('p033.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p033.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p033.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p033.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p033.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p033.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p033.s3', 'Y', NULL, NULL, NULL, NULL),
('p033.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p033.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p033.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p033.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p033.s3.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p033.s4', 'Y', NULL, NULL, NULL, NULL),
('p033.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p033.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p033.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p033.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p033.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p033.s5', 'Y', NULL, NULL, NULL, NULL),
('p033.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p033.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p033.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p033.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p033.s5.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p034', 'Y', NULL, NULL, NULL, NULL),
('p034.s1', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.addReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.deleteReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.editReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p034.s2', 'Y', NULL, NULL, NULL, NULL),
('p034.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p034.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p034.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p034.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p034.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p034.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p034.s3', 'Y', NULL, NULL, NULL, NULL),
('p034.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p034.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p034.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p034.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p034.s3.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p034.s4', 'Y', NULL, NULL, NULL, NULL),
('p034.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p034.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p034.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p034.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p034.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p034.s5', 'Y', NULL, NULL, NULL, NULL),
('p034.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p034.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p034.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p034.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p034.s5.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p035', 'Y', NULL, NULL, NULL, NULL),
('p035.s1', 'Y', NULL, NULL, NULL, NULL),
('p035.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p035.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p035.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p035.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p035.s1.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p035.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p035.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p035.s2', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.commande', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.validate', 'Y', NULL, NULL, NULL, NULL),
('p035.s3', 'Y', NULL, NULL, NULL, NULL),
('p035.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p035.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p035.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p035.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p035.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p035.s4', 'Y', NULL, NULL, NULL, NULL),
('p035.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p035.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p035.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p035.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p035.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p035.s5', 'Y', NULL, NULL, NULL, NULL),
('p035.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p035.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p035.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p035.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p035.s5.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p036', 'Y', NULL, NULL, NULL, NULL),
('p036.s1', 'Y', NULL, NULL, NULL, NULL),
('p036.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p036.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p036.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p036.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p036.s1.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p036.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p036.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p036.s2', 'Y', NULL, NULL, NULL, NULL),
('p036.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p036.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p036.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p036.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p036.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p036.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p036.s2.validate', 'Y', NULL, NULL, NULL, NULL),
('p036.s3', 'Y', NULL, NULL, NULL, NULL),
('p036.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p036.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p036.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p036.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p036.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p036.s4', 'Y', NULL, NULL, NULL, NULL),
('p036.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p036.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p036.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p036.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p036.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p036.s5', 'Y', NULL, NULL, NULL, NULL),
('p036.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p036.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p036.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p036.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p036.s5.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p037', 'Y', NULL, NULL, NULL, NULL),
('p037.s1', 'Y', NULL, NULL, NULL, NULL),
('p037.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p037.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p037.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p037.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p037.s1.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p037.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p037.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p037.s2', 'Y', NULL, NULL, NULL, NULL),
('p037.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p037.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p037.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p037.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p037.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p037.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p037.s2.validate', 'Y', NULL, NULL, NULL, NULL),
('p037.s3', 'Y', NULL, NULL, NULL, NULL),
('p037.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p037.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p037.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p037.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p037.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p037.s4', 'Y', NULL, NULL, NULL, NULL),
('p037.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p037.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p037.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p037.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p037.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p037.s5', 'Y', NULL, NULL, NULL, NULL),
('p037.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p037.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p037.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p037.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p037.s5.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p038', 'Y', NULL, NULL, NULL, NULL),
('p038.s1', 'Y', NULL, NULL, NULL, NULL),
('p038.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p038.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p038.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p038.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p038.s1.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p038.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p038.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p038.s2', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.commande', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.validate', 'Y', NULL, NULL, NULL, NULL),
('p038.s3', 'Y', NULL, NULL, NULL, NULL),
('p038.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p038.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p038.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p038.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p038.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p038.s3.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p038.s4', 'Y', NULL, NULL, NULL, NULL),
('p038.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p038.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p038.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p038.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p038.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p038.s5', 'Y', NULL, NULL, NULL, NULL),
('p038.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p038.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p038.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p038.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p038.s5.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p038.s5.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p041', 'Y', NULL, NULL, NULL, NULL),
('p041.s1', 'Y', NULL, NULL, NULL, NULL),
('p041.s1.customerCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p041.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p041.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p041.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p041.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p041.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p041.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p041.s2', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.commande', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.customer', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.validate', 'Y', NULL, NULL, NULL, NULL),
('p041.s3', 'Y', NULL, NULL, NULL, NULL),
('p041.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p041.s3.negotiatePriceSale', 'Y', NULL, NULL, NULL, NULL),
('p041.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p041.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p041.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p041.s4', 'Y', NULL, NULL, NULL, NULL),
('p041.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p041.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p041.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p041.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p041.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p041.s5', 'Y', NULL, NULL, NULL, NULL),
('p041.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p041.s5.negotiatePriceSale', 'Y', NULL, NULL, NULL, NULL),
('p041.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p041.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p041.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p051', 'Y', NULL, NULL, NULL, NULL),
('p051.s1', 'Y', NULL, NULL, NULL, NULL),
('p051.s1.code', 'Y', NULL, NULL, NULL, NULL),
('p051.s1.companyName', 'Y', NULL, NULL, NULL, NULL),
('p051.s1.firstName', 'Y', NULL, NULL, NULL, NULL),
('p051.s1.lastName', 'Y', NULL, NULL, NULL, NULL),
('page1.s1.name', 'Y', NULL, NULL, NULL, NULL),
('pageParameter', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.description', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.id', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.name', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.page', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.pageParameterType', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.value', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.description', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.id', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.name', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.page', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.pageParameterType', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.value', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s1', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s1.company', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s1.email', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s1.name', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s2', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s2.descriptionModule', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s2.moduleTypeName', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s2.nameModule', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s3', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s3.client', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s3.pageDescription', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s3.pageName', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s3.pageTypeName', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s4', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s4.client', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s4.pageParameter', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s4.pageParameterDefaultValue', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s4.value', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s5', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s5.client', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s5.pageParameter', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s5.pageParameterDefaultValue', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s5.value', 'Y', NULL, NULL, NULL, NULL),
('parameter', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1.description', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1.id', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1.name', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1.parameterType', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1.value', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2.description', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2.id', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2.name', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2.parameterType', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2.value', 'Y', NULL, NULL, NULL, NULL),
('parameterClient', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s1', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s1.company', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s1.email', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s1.name', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2.client', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2.defaultValue', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2.parameterName', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2.parameterTypeName', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2.value', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s3', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s3.client', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s3.parameter', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s3.parameterDefaultValue', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s3.value', 'Y', NULL, NULL, NULL, NULL),
('pdf001', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s1', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s2', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s2.seq', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s2.totalHt', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s2.TotalQuantity', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s2.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('pdf002', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s1', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s1.status', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s1.supplier', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s1.the', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s2', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s2.designation', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s2.quantity', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s2.totalHt', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s2.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s3.amountTva', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s3.totalPu', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s3.valueTva', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s4', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s4.note', 'Y', NULL, NULL, NULL, NULL),
('pdf003', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s1', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s2', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s2.seq', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s2.totalHt', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s2.TotalQuantity', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s2.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('pdf004', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s1', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s1.status', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s1.supplier', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s1.the', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s2', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s2.designation', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s2.quantity', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s2.totalHt', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s2.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s3.amountTva', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s3.totalPu', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s3.valueTva', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s4', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s4.note', 'Y', NULL, NULL, NULL, NULL),
('pdf005', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s1', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s2', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s2.seq', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s2.totalHt', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s2.TotalQuantity', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s2.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('pdf006', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s1', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s1.status', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s1.supplier', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s1.the', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s2', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s2.designation', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s2.quantity', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s2.totalHt', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s2.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s3.amountTva', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s3.totalPu', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s3.valueTva', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s4', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s4.note', 'Y', NULL, NULL, NULL, NULL),
('product', 'Y', NULL, NULL, NULL, NULL),
('profile.s1', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.adress', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.email', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.firstName', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.fixedPhone', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.lastName', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.password', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.replayRassword', 'Y', NULL, NULL, NULL, NULL),
('reception', 'Y', NULL, NULL, NULL, NULL),
('reception.s1', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.active', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.amount', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.deadline', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.id', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.name', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.souche', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.supplierId', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.valid', 'Y', NULL, NULL, NULL, NULL),
('reception.s2', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.amount', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.deadline', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.deposit', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.description', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.id', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.name', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.sizes', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.souche', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.valid', 'Y', NULL, NULL, NULL, NULL),
('reception.s3', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.id', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.remise', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.tva', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('reception.s4', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.designation', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.id', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.quantity', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.remise', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.totalHt', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.tva', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('reception.s5', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.quantityTotal', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.remiseAvg', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.tvaAvg', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.unitPriceBuyTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.active', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.amount', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.deadline', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.id', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.name', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.souche', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.supplierId', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.amount', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.deadline', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.deposit', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.description', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.id', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.name', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.souche', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.id', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.remise', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.tva', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.quantityTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.remiseAvg', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.tvaAvg', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.unitPriceBuyTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.active', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.amount', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.deadline', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.id', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.name', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.souche', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.supplierId', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.amount', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.deadline', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.deposit', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.description', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.id', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.name', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.souche', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.id', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.remise', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.tva', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.quantityTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.remiseAvg', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.totalHtTotal', 'Y', NULL, NULL, NULL, NULL);
INSERT INTO `INF_ITEM` (`CODE`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
('receptionValidation.s4.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.tvaAvg', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.unitPriceBuyTotal', 'Y', NULL, NULL, NULL, NULL),
('rofile.s1.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('supplier', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.dataTypeChar', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.dataTypeDateTime', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.dataTypeDouble', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.dataTypeInteger', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeCodePostale', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeDate', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeDateTime', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeEmail', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeFax', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypePhone', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeTime', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.maxlenght', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.maxWord', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.required', 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `INF_LANGUAGE`
--

CREATE TABLE IF NOT EXISTS `INF_LANGUAGE` (
  `ID` bigint(20) NOT NULL,
  `CODE` tinytext,
  `NAME` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `INF_LANGUAGE`
--

INSERT INTO `INF_LANGUAGE` (`ID`, `CODE`, `NAME`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'fr', 'Français de France', 1, 'Y', '2014-09-30 00:00:00', 1, '2014-09-16 00:00:00', NULL),
(2, 'en', 'Anglais', NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `INF_LOVS`
--

CREATE TABLE IF NOT EXISTS `INF_LOVS` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `TABLE_PREFIX` tinytext,
  `TABLE` tinytext,
  `VIEW` tinytext,
  `ITEM_CODE` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ENTITY` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `INF_LOVS`
--

INSERT INTO `INF_LOVS` (`ID`, `NAME`, `DESCRIPTION`, `TABLE_PREFIX`, `TABLE`, `VIEW`, `ITEM_CODE`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `CLT_MODULE_ID`, `ENTITY`) VALUES
(1, 'Les types de fournisseurs', NULL, 'sm', 'sm_supplier_type', 'v_sm_supplier_type', 'itemCode', 1, 'Y', NULL, NULL, NULL, NULL, 1, 'SmSupplierType'),
(2, 'La couleurs', NULL, 'sm', 'sm_expense_type', 'v_sm_expense_type', 'itemCode', 2, 'Y', NULL, NULL, NULL, NULL, 1, 'SmProductColor'),
(3, 'Les Pays', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'InfCountry'),
(4, 'Les villes', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'InfCity');

-- --------------------------------------------------------

--
-- Structure de la table `INF_MESSAGE`
--

CREATE TABLE IF NOT EXISTS `INF_MESSAGE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `MAIL` tinytext,
  `SUBJECT` text,
  `CONTENT` text,
  `IS_VIEW` char(1) CHARACTER SET latin1 DEFAULT 'N',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `INF_MONTH`
--

CREATE TABLE IF NOT EXISTS `INF_MONTH` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `NUMBER_OF_DAYS` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `INF_MONTH`
--

INSERT INTO `INF_MONTH` (`ID`, `NAME`, `NUMBER_OF_DAYS`, `SORT_KEY`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'January', 31, NULL, NULL, NULL, NULL, NULL),
(2, 'February', 28, NULL, NULL, NULL, NULL, NULL),
(3, 'March', 31, NULL, NULL, NULL, NULL, NULL),
(4, 'April', 30, NULL, NULL, NULL, NULL, NULL),
(5, 'May', 31, NULL, NULL, NULL, NULL, NULL),
(6, 'June', 30, NULL, NULL, NULL, NULL, NULL),
(7, 'July', 31, NULL, NULL, NULL, NULL, NULL),
(8, 'August', 31, NULL, NULL, NULL, NULL, NULL),
(9, 'September', 30, NULL, NULL, NULL, NULL, NULL),
(10, 'October', 31, NULL, NULL, NULL, NULL, NULL),
(11, 'November', 30, NULL, NULL, NULL, NULL, NULL),
(12, 'December', 31, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `INF_PACKAGE`
--

CREATE TABLE IF NOT EXISTS `INF_PACKAGE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `INF_PACKAGE`
--

INSERT INTO `INF_PACKAGE` (`ID`, `NAME`, `DESCRIPTION`) VALUES
(1, 'Pack Basique', 'Pack Basique');

-- --------------------------------------------------------

--
-- Structure de la table `INF_PREFIX`
--

CREATE TABLE IF NOT EXISTS `INF_PREFIX` (
  `ID` bigint(20) NOT NULL,
  `CODE` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `INF_PREFIX`
--

INSERT INTO `INF_PREFIX` (`ID`, `CODE`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'default', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `INF_PRIVILEGE`
--

CREATE TABLE IF NOT EXISTS `INF_PRIVILEGE` (
  `ID` bigint(20) NOT NULL,
  `ITEM_CODE` varchar(255) DEFAULT NULL,
  `ROLE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `INF_PRIVILEGE`
--

INSERT INTO `INF_PRIVILEGE` (`ID`, `ITEM_CODE`, `ROLE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'login', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'login.s1', 1, 'Y', NULL, NULL, NULL, NULL),
(3, 'login.s1.username', 1, 'Y', NULL, NULL, NULL, NULL),
(4, 'login.s1.password', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `INF_ROLE`
--

CREATE TABLE IF NOT EXISTS `INF_ROLE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `INF_ROLE`
--

INSERT INTO `INF_ROLE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Role 1', 'Role 1', 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `INF_ROLE_GROUP`
--

CREATE TABLE IF NOT EXISTS `INF_ROLE_GROUP` (
  `ID` bigint(20) NOT NULL,
  `ROLE_ID` bigint(20) DEFAULT NULL,
  `GROUP_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `INF_ROLE_GROUP`
--

INSERT INTO `INF_ROLE_GROUP` (`ID`, `ROLE_ID`, `GROUP_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `INF_TEXT`
--

CREATE TABLE IF NOT EXISTS `INF_TEXT` (
  `ID` bigint(20) NOT NULL,
  `PREFIX` bigint(20) DEFAULT NULL,
  `ITEM_CODE` varchar(255) DEFAULT NULL,
  `VALUE` longtext,
  `TEXT_TYPE_ID` bigint(20) DEFAULT NULL,
  `LANGUAGE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1080 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `INF_TEXT`
--

INSERT INTO `INF_TEXT` (`ID`, `PREFIX`, `ITEM_CODE`, `VALUE`, `TEXT_TYPE_ID`, `LANGUAGE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(2, 1, 'module.s1.access', 'Accès au module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(3, 1, 'login.s1.username', 'E-mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(4, 1, 'login.s1.username', 'username', 6, 1, 'Y', NULL, NULL, NULL, NULL),
(5, 1, 'login.s1.login', 'Se connecter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(10, 1, 'page1.s1.name', 'Résumé Général', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(11, 1, 'validation.v1.dataTypeInteger', '{0} doit être de type integer', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(12, 1, 'validation.v1.dataTypeDouble', '{0} doit être de type double', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(13, 1, 'validation.v1.maxWord', '{0} ne doit pas dépasser {1} mots', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(14, 1, 'validation.v1.formatTypeDate', '{0} doit être de type date', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(15, 1, 'validation.v1.dataTypeChar', '{0} doit être de type caractère', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(16, 1, 'validation.v1.dataTypeDateTime', '{0} doit être de type datetime', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(17, 1, 'validation.v1.formatTypeEmail', '{0} doit être de type email', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(18, 1, 'validation.v1.formatTypePhone', '{0} doit être de type phone', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(19, 1, 'validation.v1.maxlenght', '{0} ne doit pas dépasser {1} char', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(20, 1, 'validation.v1.required', '{0} est obligatoire', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(26, 1, 'p003.s1', 'La liste des dépenses', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(27, 1, 'p003.s1.name', 'Libelle', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(28, 1, 'p003.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(29, 1, 'p003.s1.amount', 'Montant', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(30, 1, 'p003.s1.expenseTypeName', 'Type de charge', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(34, 1, 'p003.s2', 'Ajouter / Modifier un dépense', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(36, 1, 'p003.s2.type', 'Type de charge', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(37, 1, 'p003.s2.name', 'Libelle', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(38, 1, 'p003.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(39, 1, 'p003.s2.amount', 'Montant', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(53, 1, 'p005.s1.searchReference', 'Réference', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(54, 1, 'p005.s1.searchDesignation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(55, 1, 'p005.s1.searchFamily', 'Famille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(56, 1, 'p005.s1.searchQte', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(57, 1, 'p005.s1.searchType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(58, 1, 'p005.s1.searchDepartment', 'Département', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(59, 1, 'p005.s1.searchSize', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(60, 1, 'p005.s1.searchColor', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(63, 1, 'p005.s1', 'Gestion de produits : Recherche Multi-critères', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(67, 1, 'p005.s2.productFamilyName', 'Famille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(70, 1, 'p005.s2.priceSale', 'Prix de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(71, 1, 'p005.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(72, 1, 'p005.s2.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(73, 1, 'p005.s2.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(74, 1, 'p005.s2', 'Résultat de recherche', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(90, 1, 'globals.list.delete', 'Supprimer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(91, 1, 'globals.list.edit', 'Modifier', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(92, 1, 'globals.list.option', 'Option', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(93, 1, 'globals.list.activate', 'Activer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(94, 1, 'globals.list.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(95, 1, 'globals.list.vide', 'la liste est vide.', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(96, 1, 'globals.list.confirmDelete', 'Êtes-vous sûr de vouloir supprimer ?', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(97, 1, 'globals.forms.add', 'Ajouter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(98, 1, 'globals.forms.edit', 'Modifier', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(99, 1, 'globals.forms.cancel', 'Annuler', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(100, 1, 'globals.forms.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(101, 1, 'globals.forms.delete', 'Supprimer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(102, 1, 'globals.forms.confirmDelete', 'Êtes-vous sûr de vouloir supprimer ?', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(103, 1, 'reception.s1', 'La liste des réceptions en cours', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(104, 1, 'reception.s1.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(105, 1, 'reception.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(106, 1, 'reception.s1.supplierId', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(107, 1, 'reception.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(108, 1, 'reception.s1.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(109, 1, 'reception.s1.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(110, 1, 'reception.s1.amount', 'Le montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(111, 1, 'reception.s1.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(112, 1, 'globals.forms.yes', 'Oui', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(113, 1, 'globals.forms.no', 'Non', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(146, 1, 'reception.s2', 'Ajouter une réception ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(147, 1, 'reception.s2.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(148, 1, 'reception.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(149, 1, 'reception.s2.supplier', 'Fournisseur ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(150, 1, 'reception.s2.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(151, 1, 'reception.s2.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(152, 1, 'reception.s2.amount', 'Montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(153, 1, 'reception.s2.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(155, 1, 'reception.s2.deposit', 'Depôt', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(156, 1, 'reception.s2.description', 'Description ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(159, 1, 'product', 'La gestion des produits ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(161, 1, 'p003.s1.expenseTypeName', 'Type de charge', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(162, 1, 'customer.s2', 'La liste des clients', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(163, 1, 'customer.s2.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(164, 1, 'customer.s1', 'Ajouter / Modifier un client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(165, 1, 'customer.s2.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(166, 1, 'customer.s2.country', 'Pays', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(167, 1, 'customer.s2.city', 'Ville', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(168, 1, 'customer.s2.email', 'E-Mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(169, 1, 'customer.s2.adress', 'Adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(170, 1, 'customer.s2.fixedPhone', 'Télé Fixe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(171, 1, 'customer.s2.cellPhone', 'Télé Portable', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(172, 1, 'customer.s1.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(173, 1, 'customer.s1.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(174, 1, 'customer.s1.country', 'Pays', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(175, 1, 'customer.s1.city', 'Ville', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(176, 1, 'customer.s1.cellPhone', 'Télé Portable', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(177, 1, 'customer.s1.fixedPhone', 'Télé Fixe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(178, 1, 'customer.s1.adress', 'Adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(179, 1, 'customer.s1.email', 'E-Mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(180, 1, 'customer.s1.type', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(186, 1, 'customer.s1.active', 'Active ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(188, 1, 'p005.s2.priceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(202, 1, 'reception.s3', 'Ajouter des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(203, 1, 'reception.s3.id', 'Code d''article ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(204, 1, 'reception.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(205, 1, 'reception.s3.quantity', 'Qté ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(206, 1, 'reception.s3.unitPriceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(207, 1, 'reception.s3.remise', 'Remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(208, 1, 'reception.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(209, 1, 'reception.s3.tva', 'Tva', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(210, 1, 'reception.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(211, 1, 'globals.forms.confirmClean', 'Etes-vous sur de vouloir vider le formulaire ?', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(212, 1, 'globals.forms.clean', 'vider le fourmulaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(213, 1, 'reception.s4', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(214, 1, 'reception.s4.id', 'Code d''article ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(215, 1, 'reception.s4.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(216, 1, 'reception.s4.quantity', 'Qté ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(217, 1, 'reception.s4.unitPriceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(218, 1, 'reception.s4.remise', 'Remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(219, 1, 'reception.s4.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(220, 1, 'reception.s4.tva', 'Tva', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(221, 1, 'reception.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(222, 1, 'page1.s1.name', 'name msg', 2, 1, 'Y', NULL, NULL, NULL, NULL),
(223, 1, 'page1.s1.name', 'name help', 4, 1, 'Y', NULL, NULL, NULL, NULL),
(224, 1, 'globals.forms.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(225, 1, 'globals.forms.validate', 'Validate', 3, 2, 'Y', NULL, NULL, NULL, NULL),
(226, 1, 'receptionValidation.s1', 'La liste des réceptions qui sont prêt pour la validation', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(227, 1, 'receptionValidation.s1.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(228, 1, 'receptionValidation.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(229, 1, 'receptionValidation.s1.supplierId', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(230, 1, 'receptionValidation.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(231, 1, 'receptionValidation.s1.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(232, 1, 'receptionValidation.s1.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(233, 1, 'receptionValidation.s1.amount', 'Le montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(234, 1, 'receptionValidation.s1.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(235, 1, 'receptionValidation.s2', 'Détails de réception ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(236, 1, 'receptionValidation.s2.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(237, 1, 'receptionValidation.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(238, 1, 'receptionValidation.s2.supplier', 'Fournisseur ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(239, 1, 'receptionValidation.s2.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(240, 1, 'receptionValidation.s2.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(241, 1, 'receptionValidation.s2.amount', 'Montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(242, 1, 'receptionValidation.s2.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(243, 1, 'receptionValidation.s2.deposit', 'Depôt', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(244, 1, 'receptionValidation.s2.description', 'Description ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(245, 1, 'receptionValidation.s3', 'Ajouter des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(246, 1, 'receptionValidation.s3.id', 'Code d''article ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(247, 1, 'receptionValidation.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(248, 1, 'receptionValidation.s3.quantity', 'Qté ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(249, 1, 'receptionValidation.s3.unitPriceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(250, 1, 'receptionValidation.s3.remise', 'Remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(251, 1, 'receptionValidation.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(252, 1, 'receptionValidation.s3.tva', 'Tva', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(253, 1, 'receptionValidation.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(254, 1, 'receptionHistory.s1', 'Hisortique des réceptions', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(255, 1, 'receptionHistory.s1.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(256, 1, 'receptionHistory.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(257, 1, 'receptionHistory.s1.supplierId', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(258, 1, 'receptionHistory.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(259, 1, 'receptionHistory.s1.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(260, 1, 'receptionHistory.s1.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(261, 1, 'receptionHistory.s1.amount', 'Le montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(262, 1, 'receptionHistory.s1.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(263, 1, 'receptionHistory.s2', 'Détails de réception ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(264, 1, 'receptionHistory.s2.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(265, 1, 'receptionHistory.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(266, 1, 'receptionHistory.s2.supplier', 'Fournisseur ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(267, 1, 'receptionHistory.s2.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(268, 1, 'receptionHistory.s2.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(269, 1, 'receptionHistory.s2.amount', 'Montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(270, 1, 'receptionHistory.s2.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(271, 1, 'receptionHistory.s2.deposit', 'Depôt', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(272, 1, 'receptionHistory.s2.description', 'Description ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(273, 1, 'receptionHistory.s3', 'Ajouter des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(274, 1, 'receptionHistory.s3.id', 'Code d''article ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(275, 1, 'receptionHistory.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(276, 1, 'receptionHistory.s3.quantity', 'Qté ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(277, 1, 'receptionHistory.s3.unitPriceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(278, 1, 'receptionHistory.s3.remise', 'Remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(279, 1, 'receptionHistory.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(280, 1, 'receptionHistory.s3.tva', 'Tva', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(281, 1, 'receptionHistory.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(282, 1, 'receptionValidation.s4', 'Résumé', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(283, 1, 'receptionValidation.s4.quantityTotal', 'La quantité totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(284, 1, 'receptionValidation.s4.unitPriceBuyTotal', 'Le prix unitaire totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(285, 1, 'receptionValidation.s4.remiseAvg', 'La moyenne de remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(286, 1, 'receptionValidation.s4.tvaAvg', 'La monyenne de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(287, 1, 'receptionValidation.s4.totalHtTotal', 'hore taxt total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(288, 1, 'receptionValidation.s4.totalTtcTotal', 'TTC total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(289, 1, 'receptionHistory.s4', 'Résumé Général', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(290, 1, 'receptionHistory.s4.quantityTotal', 'La quantité totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(291, 1, 'receptionHistory.s4.unitPriceBuyTotal', 'Le prix unitaire totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(292, 1, 'receptionHistory.s4.remiseAvg', 'La moyenne de remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(293, 1, 'receptionHistory.s4.tvaAvg', 'La monyenne de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(294, 1, 'receptionHistory.s4.totalHtTotal', 'hore taxt total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(295, 1, 'receptionHistory.s4.totalTtcTotal', 'TTC total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(303, 1, 'reception.s5', 'Résumé Général', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(304, 1, 'reception.s5.quantityTotal', 'La quantité totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(305, 1, 'reception.s5.unitPriceBuyTotal', 'Le prix unitaire totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(306, 1, 'reception.s5.remiseAvg', 'La moyenne de remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(307, 1, 'reception.s5.tvaAvg', 'La monyenne de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(308, 1, 'reception.s5.totalHtTotal', 'hore taxt total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(309, 1, 'reception.s5.totalTtcTotal', 'TTC total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(310, 1, 'reception.s4.totalHt', 'Total HT', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(311, 1, 'reception.s4.totalTtc', 'Total TTC', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(312, 1, 'inventaire.s1', 'Inventaire générale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(313, 1, 'inventaire.s1.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(314, 1, 'inventaire.s1.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(315, 1, 'inventaire.s1.quantity', 'Quantité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(316, 1, 'inventaire.s1.priceBuy', 'Prix de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(317, 1, 'inventaire.s1.priceSale', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(318, 1, 'inventaire.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(319, 1, 'inventaire.s1.receptionValidCount', 'Nombre de réception', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(320, 1, 'inventaire.s1.unitPriceBuyMax', 'Max de prix de vent', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(321, 1, 'inventaire.s1.unitPriceBuyMoyenne', 'Moyenne de prix de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(322, 1, 'inventaire.s1.unitPriceBuyMin', 'Min de prix de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(323, 1, 'inventaire.s1.quantityCount', 'Total des quantité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(334, 1, 'login.s1.forgetPassword', 'Mot de passe oublié.', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(335, 1, 'login.s1.getAccount', 'Obtenir d''un compte', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(336, 1, 'login.s1.sessionActive', 'Garder ma session active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(337, 1, 'login.s1', 'Authentification', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(338, 1, 'module.s1.access', 'Accès au module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(339, 1, 'module.s1.logout', 'Déconnexion', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(340, 1, 'bonLivraison.s1', 'Nouveau bon de livraison', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(341, 1, 'bonLivraison.s1.customer', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(342, 1, 'bonLivraison.s1.description', 'Note', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(343, 1, 'bonLivraison.s2', 'Ajouter un produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(344, 1, 'bonLivraison.s2.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(345, 1, 'bonLivraison.s2.color', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(346, 1, 'bonLivraison.s2.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(347, 1, 'bonLivraison.s2.unitPriceBuy', 'Prix', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(348, 1, 'bonLivraison.s2.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(349, 1, 'bonLivraison.s2.totalTtc', 'Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(350, 1, 'bonLivraison.s3', 'La liste des articles', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(351, 1, 'bonLivraison.s3.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(352, 1, 'bonLivraison.s3.color', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(353, 1, 'bonLivraison.s3.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(354, 1, 'bonLivraison.s3.price', 'Prix Unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(355, 1, 'bonLivraison.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(356, 1, 'bonLivraison.s3.totalPriceBuy', 'Prix Totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(357, 1, 'bonLivraison.s4', 'Résumé Général', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(358, 1, 'bonLivraison.s5', 'Mode de paiement', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(359, 1, 'bonLivraison.s4.sumQuantity', 'Quantité Totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(360, 1, 'bonLivraison.s4.sumTotal', 'Montant Totale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(361, 1, 'blValidation.s1.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(362, 1, 'blValidation.s1.customerName', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(363, 1, 'blValidation.s1.description', 'Note', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(364, 1, 'blValidation.s1.sumTotalPriceBuy', 'Prix Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(365, 1, 'blValidation.s1', 'La liste des ventes attentes', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(366, 1, 'blValidation.s2', 'Modifer un vente', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(367, 1, 'blValidation.s2.customer', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(368, 1, 'blValidation.s2.description', 'Note', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(369, 1, 'blValidation.s3.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(370, 1, 'blValidation.s3.color', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(371, 1, 'blValidation.s3.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(372, 1, 'blValidation.s3.unitPriceBuy', 'Prix', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(373, 1, 'blValidation.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(374, 1, 'blValidation.s3.totalTtc', 'Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(375, 1, 'blValidation.s4', 'La liste des articles', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(376, 1, 'blValidation.s4.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(377, 1, 'blValidation.s4.color', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(378, 1, 'blValidation.s4.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(379, 1, 'blValidation.s4.price', 'Prix', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(380, 1, 'blValidation.s4.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(381, 1, 'blValidation.s4.totalPriceBuy', 'Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(382, 1, 'blValidation.s5', 'Résumé Général', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(383, 1, 'blValidation.s5.sumQuantity', 'Qté Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(384, 1, 'blValidation.s5.sumTotal', 'Montant Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(385, 1, 'blValidation.s5', 'Mode de paiement', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(386, 1, 'blValidation.s3', 'Ajouter un article', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(387, 1, 'blValidation.s1.sumQuantity', 'Qté Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(388, 1, 'blValidation.s6', 'Mode de paiement', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(389, 1, 'reception.s2.sizes', 'Taille de Réception', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(390, 1, 'inventaire.s1.productSizeName', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(391, 1, 'inventaire.s1.productColorName', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(393, 1, 'profile.s1.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(394, 1, 'profile.s1.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(395, 1, 'profile.s1.email', 'E-mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(396, 1, 'profile.s1.password', 'Mot de passe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(397, 1, 'profile.s1.replayRassword', 'Re mot de passe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(398, 1, 'profile.s1.cellPhone', 'Télé Mobile', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(399, 1, 'profile.s1.fixedPhone', 'Télé Fixe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(400, 1, 'profile.s1.adress', 'Adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(401, 1, 'profile.s1', 'Modifier Mon Profile', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(402, 1, 'basicParameter.s1', 'Basic Parameter', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(403, 1, 'basicParameter.s1.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(404, 1, 'basicParameter.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(405, 1, 'basicParameter.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(406, 1, 'basicParameter.s1.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(407, 1, 'basicParameter.s1.basicParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(408, 1, 'basicParameter.s2', 'Ajouter / Editer basic Parameter', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(409, 1, 'basicParameter.s2.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(410, 1, 'basicParameter.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(411, 1, 'basicParameter.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(412, 1, 'basicParameter.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(413, 1, 'basicParameter.s2.basicParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(414, 1, 'basicParameter.s1', 'Basic Parameter', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(415, 1, 'basicParameter.s1.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(416, 1, 'basicParameter.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(417, 1, 'basicParameter.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(418, 1, 'basicParameter.s1.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(419, 1, 'basicParameter.s1.basicParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(420, 1, 'basicParameter.s2', 'Ajouter / Editer basic Parameter', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(421, 1, 'basicParameter.s2.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(422, 1, 'basicParameter.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(423, 1, 'basicParameter.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(424, 1, 'basicParameter.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(425, 1, 'basicParameter.s2.basicParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(426, 1, 'moduleParameter.s1', 'Parameter du module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(427, 1, 'moduleParameter.s1.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(428, 1, 'moduleParameter.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(429, 1, 'moduleParameter.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(430, 1, 'moduleParameter.s1.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(431, 1, 'moduleParameter.s1.moduleParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(432, 1, 'moduleParameter.s2', 'Ajouter / Editer Parameter du module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(433, 1, 'moduleParameter.s2.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(434, 1, 'moduleParameter.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(435, 1, 'moduleParameter.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(436, 1, 'moduleParameter.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(437, 1, 'moduleParameter.s2.moduleParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(438, 1, 'parameterClient.s1', 'Liste du client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(439, 1, 'parameterClient.s1.name', 'Nom et prenom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(440, 1, 'parameterClient.s1.company', 'Societer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(441, 1, 'parameterClient.s1.email', 'Email', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(442, 1, 'parameterClient.s2', 'Parameter du client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(443, 1, 'parameterClient.s2.client', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(444, 1, 'parameterClient.s2.parameterName', 'Nom de parameter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(445, 1, 'parameterClient.s2.parameterTypeName', 'Type de parameter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(446, 1, 'parameterClient.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(447, 1, 'parameterClient.s2.defaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(448, 1, 'parameterClient.s3', 'Ajouter / Editer Parameter du client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(449, 1, 'parameterClient.s3.client', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(450, 1, 'parameterClient.s3.parameter', 'Nom de parameter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(451, 1, 'parameterClient.s3.parameterDefaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(452, 1, 'parameterClient.s3.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(453, 1, 'parameter.s1', 'Paramètres', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(454, 1, 'parameter.s1.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(455, 1, 'parameter.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(456, 1, 'parameter.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(457, 1, 'parameter.s1.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(458, 1, 'parameter.s1.parameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(459, 1, 'parameter.s2', 'Ajouter / Editer paramètres', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(460, 1, 'parameter.s2.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(461, 1, 'parameter.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(462, 1, 'parameter.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(463, 1, 'parameter.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(464, 1, 'parameter.s2.parameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(465, 1, 'pageParameterModule.s1', 'Liste du client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(466, 1, 'pageParameterModule.s1.name', 'Nom et prenom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(467, 1, 'pageParameterModule.s1.company', 'Societer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(468, 1, 'pageParameterModule.s1.email', 'Email', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(469, 1, 'pageParameterModule.s2', 'Liste du module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(470, 1, 'pageParameterModule.s2.nameModule', 'Nom du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(471, 1, 'pageParameterModule.s2.descriptionModule', 'Description du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(472, 1, 'pageParameterModule.s2.moduleTypeName', 'Type du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(473, 1, 'pageParameterModule.s3', 'Liste du page', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(474, 1, 'pageParameterModule.s3.client', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(475, 1, 'pageParameterModule.s3.pageName', 'Nom du page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(476, 1, 'pageParameterModule.s3.pageDescription', 'Description du page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(477, 1, 'pageParameterModule.s3.pageTypeName', 'Nom du type page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(478, 1, 'pageParameterModule.s4', 'Ajouter / Editer Parameter du page/module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(479, 1, 'pageParameterModule.s4.pageParameter', 'Nom de parameter du page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(480, 1, 'pageParameterModule.s4.pageParameterDefaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(481, 1, 'pageParameterModule.s4.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(482, 1, 'pageParameterModule.s5', 'Ajouter / Editer Parameter du page/module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(483, 1, 'pageParameterModule.s5.pageParameter', 'Nom de parameter du page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(484, 1, 'pageParameterModule.s5.pageParameterDefaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(485, 1, 'pageParameterModule.s5.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(486, 1, 'pageParameter.s1', 'Parameter du page', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(487, 1, 'pageParameter.s1.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(488, 1, 'pageParameter.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(489, 1, 'pageParameter.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(490, 1, 'pageParameter.s1.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(491, 1, 'pageParameter.s1.pageParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(492, 1, 'pageParameter.s1.page', 'Page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(493, 1, 'pageParameter.s2', 'Ajouter / Editer Parameter du page', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(494, 1, 'pageParameter.s2.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(495, 1, 'pageParameter.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(496, 1, 'pageParameter.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(497, 1, 'pageParameter.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(498, 1, 'pageParameter.s2.pageParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(499, 1, 'pageParameter.s2.page', 'Page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(500, 1, 'moduleParameterClient.s1', 'Liste du client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(501, 1, 'moduleParameterClient.s1.name', 'Nom et prenom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(502, 1, 'moduleParameterClient.s1.company', 'Societer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(503, 1, 'moduleParameterClient.s1.email', 'Email', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(504, 1, 'moduleParameterClient.s2', 'Liste du module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(505, 1, 'moduleParameterClient.s2.nameModule', 'Nom du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(506, 1, 'moduleParameterClient.s2.descriptionModule', 'Description du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(507, 1, 'moduleParameterClient.s2.moduleTypeName', 'Type du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(508, 1, 'moduleParameterClient.s3', 'Parameter du module/client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(509, 1, 'moduleParameterClient.s3.client', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(510, 1, 'moduleParameterClient.s3.parameterModuleName', 'Nom de parameter du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(511, 1, 'moduleParameterClient.s3.parameterModuleTypeName', 'Type de parameter du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(512, 1, 'moduleParameterClient.s3.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(513, 1, 'moduleParameterClient.s3.defaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(514, 1, 'moduleParameterClient.s4', 'Ajouter / Editer Parameter du module/client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(515, 1, 'moduleParameterClient.s4.client', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(516, 1, 'moduleParameterClient.s4.parameterModule', 'Nom de parameter du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(517, 1, 'moduleParameterClient.s4.parameterModuleDefaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(518, 1, 'moduleParameterClient.s4.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(519, 1, 'validation.v1.formatTypeFax', '{0} doit être de type fax', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(520, 1, 'validation.v1.formatTypeCodePostale', '{0} doit être de type code postale', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(521, 1, 'validation.v1.formatTypeTime', '{0} doit être de type time', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(522, 1, 'validation.v1.formatTypeDateTime', '{0} doit être de type datetime', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(523, 1, 'validation.v1.formatTypePattern', '{0} doit être un {1}', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(525, 1, 'p005.s2.productGroupName', 'Groupe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(526, 1, 'p005.s2.productTypeName', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(527, 1, 'customer.s1.secondaryAddress ', 'Adresse 2', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(528, 1, 'customer.s1.category ', 'Catégorie', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(529, 1, 'customer.s1.company', 'Société', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(530, 1, 'customer.s1.zipCode', 'Code postale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(531, 1, 'customer.s1.shortLabel', 'Libelle court', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(532, 1, 'customer.s1.fullLabel', 'Libelle complet', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(533, 1, 'customer.s1.webSite', 'Site web', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(534, 1, 'customer.s1.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(541, 1, 'p006.s2.code', 'Code réception', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(542, 1, 'p006.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(543, 1, 'p006.s2.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(544, 1, 'p006.s2.deadline', 'Date d''écheance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(545, 1, 'p006.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(546, 1, 'p006.s2.deposit', 'Dépôt', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(547, 1, 'p032.s1', 'La liste des commandes en cours', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(548, 1, 'p032.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(549, 1, 'p032.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(550, 1, 'p032.s1.supplierCompanyName', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(551, 1, 'p032.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(552, 1, 'p032.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(553, 1, 'p032.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(556, 1, 'p032.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(557, 1, 'p032.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(560, 1, 'p032.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(561, 1, 'p032.s2', 'Ajouter / Editer une commande', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(562, 1, 'p032.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(565, 1, 'p032.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(566, 1, 'p032.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(567, 1, 'p032.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(568, 1, 'p032.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(569, 1, 'p032.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(570, 1, 'p032.s3.unitPriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(571, 1, 'p032.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(572, 1, 'p032.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(573, 1, 'p032.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(574, 1, 'p032.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(575, 1, 'p032.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(576, 1, 'p032.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(577, 1, 'p032.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(578, 1, 'p032.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(579, 1, 'p032.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(580, 1, 'p032.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(581, 1, 'p032.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(582, 1, 'p032.s5.unitPriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(583, 1, 'p032.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(584, 1, 'p032.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(585, 1, 'pdf003.s1', 'Bon de réception', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(586, 1, 'pdf003.s1', 'La liste des réceptions en cours', 2, 1, 'Y', NULL, NULL, NULL, NULL),
(587, 1, 'pdf003.s2.seq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(588, 1, 'pdf003.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(589, 1, 'pdf003.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(590, 1, 'pdf003.s2.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(591, 1, 'pdf003.s2.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(592, 1, 'pdf003.s2.TotalQuantity', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(593, 1, 'pdf004.s1', 'Bon de réception', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(594, 1, 'pdf004.s1.reference', 'Référence ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(595, 1, 'pdf004.s1.status', 'Statut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(596, 1, 'pdf004.s1.supplier', ' Fournisseur ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(597, 1, 'pdf004.s1.the', 'Le : ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(598, 1, 'pdf004.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(599, 1, 'pdf004.s2.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(600, 1, 'pdf004.s2.unitPriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(601, 1, 'pdf004.s2.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(602, 1, 'pdf004.s2.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(603, 1, 'pdf004.s3.quantity', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(604, 1, 'pdf004.s3.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(605, 1, 'pdf004.s3.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(606, 1, 'pdf004.s3.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(607, 1, 'pdf004.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(608, 1, 'pdf004.s4.note', ' Note interne ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(609, 1, 'p005.s2.threshold', 'Seuil d''alerte', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(610, 1, 'p033.s1', 'La liste des commandes soumise', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(611, 1, 'p033.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(612, 1, 'p033.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(613, 1, 'p033.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(614, 1, 'p033.s1.supplierCompanyName', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(615, 1, 'p033.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(616, 1, 'p033.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(617, 1, 'p033.s2', 'Ajouter / Editer une commande', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(618, 1, 'p033.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(619, 1, 'p033.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(620, 1, 'p033.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(621, 1, 'p033.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(622, 1, 'p033.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(623, 1, 'p033.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(624, 1, 'p033.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(625, 1, 'p033.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(626, 1, 'p033.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(627, 1, 'p033.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(628, 1, 'p033.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(629, 1, 'p033.s3.unitPriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(630, 1, 'p033.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(631, 1, 'p033.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(632, 1, 'p033.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(633, 1, 'p033.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(634, 1, 'p033.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(635, 1, 'p033.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(636, 1, 'p033.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(637, 1, 'p033.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(638, 1, 'p033.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(639, 1, 'p033.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(640, 1, 'p033.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(641, 1, 'p033.s5.unitPriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(642, 1, 'p034.s1', 'La liste des commandes validée', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(643, 1, 'p034.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(644, 1, 'p034.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(645, 1, 'p034.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(646, 1, 'p034.s1.supplierCompanyName', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(647, 1, 'p034.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(648, 1, 'p034.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(649, 1, 'p034.s2', 'Ajouter / Editer une commande', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(650, 1, 'p034.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(651, 1, 'p034.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(652, 1, 'p034.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(653, 1, 'p034.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(654, 1, 'p034.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(655, 1, 'p034.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(656, 1, 'p034.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(657, 1, 'p034.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(658, 1, 'p034.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(659, 1, 'p034.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(660, 1, 'p034.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(661, 1, 'p034.s3.unitPriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(662, 1, 'p034.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(663, 1, 'p034.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(664, 1, 'p034.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(665, 1, 'p034.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(666, 1, 'p034.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(667, 1, 'p034.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(668, 1, 'p034.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(669, 1, 'p034.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(670, 1, 'p034.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(671, 1, 'p034.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(672, 1, 'p034.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(673, 1, 'p034.s5.unitPriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(674, 1, 'p032.s2.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(675, 1, 'p035.s1', 'La liste des réceptions en cours', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(676, 1, 'p035.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(677, 1, 'p035.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(678, 1, 'p035.s1.supplierCompanyName', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(679, 1, 'p035.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(680, 1, 'p035.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(681, 1, 'p035.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(682, 1, 'p035.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(683, 1, 'p035.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(684, 1, 'p035.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(685, 1, 'p035.s2', 'Ajouter / Editer une réception', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(686, 1, 'p035.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(687, 1, 'p035.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(688, 1, 'p035.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(689, 1, 'p035.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(690, 1, 'p035.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(691, 1, 'p035.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(692, 1, 'p035.s3.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(693, 1, 'p035.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(694, 1, 'p035.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(695, 1, 'p035.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(696, 1, 'p035.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(697, 1, 'p035.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(698, 1, 'p035.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(699, 1, 'p035.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(700, 1, 'p035.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(701, 1, 'p035.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(702, 1, 'p035.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(703, 1, 'p035.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(704, 1, 'p035.s5.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(705, 1, 'p035.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(706, 1, 'p035.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(707, 1, 'p035.s2.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(708, 1, 'pdf001.s1', 'Bon de commande fournisseur', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(709, 1, 'pdf001.s1', 'La liste des commande en cours', 2, 1, 'Y', NULL, NULL, NULL, NULL),
(710, 1, 'pdf001.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(711, 1, 'pdf001.s2.seq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(712, 1, 'pdf001.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(713, 1, 'pdf001.s2.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(714, 1, 'pdf001.s2.TotalQuantity', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(715, 1, 'pdf001.s2.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(716, 1, 'pdf002.s1', 'Bon de commande fournisseur', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(717, 1, 'pdf002.s1.reference', 'Référence ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(718, 1, 'pdf002.s1.status', 'Statut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(719, 1, 'pdf002.s1.supplier', ' Fournisseur ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(720, 1, 'pdf002.s1.the', 'Le : ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(721, 1, 'pdf002.s2.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(722, 1, 'pdf002.s2.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(723, 1, 'pdf002.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(724, 1, 'pdf002.s2.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(725, 1, 'pdf002.s2.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(726, 1, 'pdf002.s3.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(727, 1, 'pdf002.s3.quantity', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL);
INSERT INTO `INF_TEXT` (`ID`, `PREFIX`, `ITEM_CODE`, `VALUE`, `TEXT_TYPE_ID`, `LANGUAGE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(728, 1, 'pdf002.s3.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(729, 1, 'pdf002.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(730, 1, 'pdf002.s3.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(731, 1, 'pdf002.s4.note', ' Note interne ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(732, 1, 'p036.s1', 'La liste des réceptions soumise', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(733, 1, 'p036.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(734, 1, 'p036.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(735, 1, 'p036.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(736, 1, 'p036.s1.supplierCompanyName', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(737, 1, 'p036.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(738, 1, 'p036.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(739, 1, 'p036.s2', 'Ajouter / Editer une réception', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(740, 1, 'p036.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(741, 1, 'p036.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(742, 1, 'p036.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(743, 1, 'p036.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(744, 1, 'p036.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(745, 1, 'p036.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(746, 1, 'p036.s2.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(747, 1, 'p036.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(748, 1, 'p036.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(749, 1, 'p036.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(750, 1, 'p036.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(751, 1, 'p036.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(752, 1, 'p036.s3.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(753, 1, 'p036.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(754, 1, 'p036.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(755, 1, 'p036.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(756, 1, 'p036.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(757, 1, 'p036.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(758, 1, 'p036.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(759, 1, 'p036.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(760, 1, 'p036.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(761, 1, 'p036.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(762, 1, 'p036.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(763, 1, 'p036.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(764, 1, 'p036.s5.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(765, 1, 'p037.s1', 'La liste des réceptions validée', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(766, 1, 'p037.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(767, 1, 'p037.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(768, 1, 'p037.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(769, 1, 'p037.s1.supplierCompanyName', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(770, 1, 'p037.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(771, 1, 'p037.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(772, 1, 'p037.s2', 'Ajouter / Editer une réception', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(773, 1, 'p037.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(774, 1, 'p037.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(775, 1, 'p037.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(776, 1, 'p037.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(777, 1, 'p037.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(778, 1, 'p037.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(779, 1, 'p037.s2.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(780, 1, 'p037.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(781, 1, 'p037.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(782, 1, 'p037.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(783, 1, 'p037.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(784, 1, 'p037.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(785, 1, 'p037.s3.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(786, 1, 'p037.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(787, 1, 'p037.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(788, 1, 'p037.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(789, 1, 'p037.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(790, 1, 'p037.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(791, 1, 'p037.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(792, 1, 'p037.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(793, 1, 'p037.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(794, 1, 'p037.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(795, 1, 'p037.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(796, 1, 'p037.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(797, 1, 'p037.s5.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(798, 1, 'p035.s2.commande', 'Commande', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(799, 1, 'p038.s1', 'La liste des ventes en cours', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(800, 1, 'p038.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(801, 1, 'p038.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(802, 1, 'p038.s1.supplierCompanyName', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(803, 1, 'p038.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(804, 1, 'p038.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(805, 1, 'p038.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(806, 1, 'p038.s2.supplier', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(807, 1, 'p038.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(808, 1, 'p038.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(809, 1, 'p038.s2', 'Ajouter / Editer un vente', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(810, 1, 'p038.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(811, 1, 'p038.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(812, 1, 'p038.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(813, 1, 'p038.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(814, 1, 'p038.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(815, 1, 'p038.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(816, 1, 'p038.s3.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(817, 1, 'p038.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(818, 1, 'p038.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(819, 1, 'p038.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(820, 1, 'p038.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(821, 1, 'p038.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(822, 1, 'p038.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(823, 1, 'p038.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(824, 1, 'p038.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(825, 1, 'p038.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(826, 1, 'p038.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(827, 1, 'p038.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(828, 1, 'p038.s5.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(829, 1, 'p038.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(830, 1, 'p038.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(831, 1, 'p038.s2.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(832, 1, 'p038.s2.commande', 'Commande', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(833, 1, 'p038.s3.unitPriceSale', 'Prix vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(834, 1, 'p038.s5.unitPriceSale', 'Prix vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(835, 1, 'pdf005.s1', 'Bon de vente', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(836, 1, 'pdf005.s1', 'La liste des ventes en cours', 2, 1, 'Y', NULL, NULL, NULL, NULL),
(837, 1, 'pdf005.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(838, 1, 'pdf005.s2.seq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(839, 1, 'pdf005.s2.supplier', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(840, 1, 'pdf005.s2.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(841, 1, 'pdf005.s2.TotalQuantity', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(842, 1, 'pdf005.s2.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(843, 1, 'pdf006.s1', 'Bon de vente', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(844, 1, 'pdf006.s1.reference', 'Référence ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(845, 1, 'pdf006.s1.status', 'Statut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(846, 1, 'pdf006.s1.supplier', ' Client ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(847, 1, 'pdf006.s1.the', 'Le : ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(848, 1, 'pdf006.s2.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(849, 1, 'pdf006.s2.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(850, 1, 'pdf006.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(851, 1, 'pdf006.s2.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(852, 1, 'pdf006.s2.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(853, 1, 'pdf006.s3.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(854, 1, 'pdf006.s3.quantity', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(855, 1, 'pdf006.s3.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(856, 1, 'pdf006.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(857, 1, 'pdf006.s3.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(858, 1, 'pdf006.s4.note', ' Note interne ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(859, 1, 'p041.s1', 'La liste des bons de retour en cours', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(860, 1, 'p041.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(861, 1, 'p041.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(862, 1, 'p041.s1.customerCompanyName', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(863, 1, 'p041.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(864, 1, 'p041.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(865, 1, 'p041.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(866, 1, 'p041.s2.customer', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(867, 1, 'p041.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(868, 1, 'p041.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(869, 1, 'p041.s2', 'Ajouter / Editer un bon de retour', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(870, 1, 'p041.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(871, 1, 'p041.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(872, 1, 'p041.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(873, 1, 'p041.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(874, 1, 'p041.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(875, 1, 'p041.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(876, 1, 'p041.s3.negotiatePriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(877, 1, 'p041.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(878, 1, 'p041.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(879, 1, 'p041.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(880, 1, 'p041.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(881, 1, 'p041.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(882, 1, 'p041.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(883, 1, 'p041.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(884, 1, 'p041.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(885, 1, 'p041.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(886, 1, 'p041.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(887, 1, 'p041.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(888, 1, 'p041.s5.negotiatePriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(889, 1, 'p041.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(890, 1, 'p041.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(891, 1, 'p041.s2.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(892, 1, 'p041.s2.commande', 'Commande', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(893, 1, 'login.s1.password', 'Mot de passe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(909, 1, 'contactManagement.s1', 'Adresses', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(910, 1, 'contactManagement.s2', 'Téléphones', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(911, 1, 'contactManagement.s3', 'Faxes', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(912, 1, 'contactManagement.s4', 'Sites web', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(913, 1, 'contactManagement.s5', 'Emails', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(914, 1, 'contactManagement.s1.locationTypeName', 'Type d''adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(915, 1, 'contactManagement.s1.addressLine', 'Adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(916, 1, 'contactManagement.s1.postalCode', 'Code postal', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(917, 1, 'contactManagement.s1.infCountryName', 'Pays', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(918, 1, 'contactManagement.s1.infCityName', 'Ville', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(919, 1, 'contactManagement.s1.priority', 'Priorité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(920, 1, 'contactManagement.s1.ctaLocationPostalCode', 'Code postal', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(921, 1, 'contactManagement.s1.ctaLocationPriority', 'Priorité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(922, 1, 'contactManagement.s1.ctaLocationLine1', 'Ligne 1 ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(923, 1, 'contactManagement.s1.ctaLocationLine2', 'Ligne 2 ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(924, 1, 'contactManagement.s1.ctaLocationLine3', 'Ligne 3 ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(925, 1, 'contactManagement.s2.phoneTypeName', 'Type d''adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(926, 1, 'contactManagement.s2.countryCode', 'Indicatif', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(927, 1, 'contactManagement.s2.number', 'Numéro', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(928, 1, 'contactManagement.s2.priority', 'Priorité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(929, 1, 'contactManagement.s2.ctaPhoneCountryCode', 'Indicatif', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(930, 1, 'contactManagement.s2.ctaPhoneNumber', 'Numéro', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(931, 1, 'contactManagement.s2.ctaPhonePriority', 'Priorité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(932, 1, 'contactManagement.s3.faxTypeName', 'Type de faxe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(933, 1, 'contactManagement.s3.countryCode', 'Indicatif', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(934, 1, 'contactManagement.s3.number', 'Numéro', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(935, 1, 'contactManagement.s3.priority', 'Priorité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(936, 1, 'contactManagement.s3.ctaFaxCountryCode', 'Indicatif', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(937, 1, 'contactManagement.s3.ctaFaxNumber', 'Numéro', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(938, 1, 'contactManagement.s3.ctaFaxPriority', 'Priorité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(939, 1, 'contactManagement.s4.webTypeName', 'Type de sites web', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(940, 1, 'contactManagement.s4.externalUrl', 'Le lien', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(941, 1, 'contactManagement.s4.priority', 'Priorité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(942, 1, 'contactManagement.s4.ctaWebUrl', 'Le lien', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(943, 1, 'contactManagement.s4.ctaWebPriority', 'Priorité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(944, 1, 'contactManagement.s5.emailTypeName', 'Type d''email', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(945, 1, 'contactManagement.s5.emailAddress', 'E-mail adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(946, 1, 'contactManagement.s5.priority', 'Priorité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(947, 1, 'contactManagement.s5.ctaEmailAdresse', 'E-mail adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(948, 1, 'contactManagement.s5.ctaEmailPriority', 'Priorité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(949, 1, 'p009.s1', 'La liste des produits qui sont en arrivage', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(950, 1, 'p009.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(951, 1, 'p009.s1.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(952, 1, 'p009.s1.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(953, 1, 'p009.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(954, 1, 'p009.s2.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(955, 1, 'p009.s2.qteCommanded', 'Qté commandée', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(956, 1, 'p009.s2.qteRecieved', 'Qté reçue', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(957, 1, 'p009.s2.supplierCompanyName', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(958, 1, 'globals.forms.search', 'Rechercher', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(959, 1, 'p009.s2.orderSupplierReference', 'Réf de commande', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(960, 1, 'p009.s1.orderSupplierReference', 'Réf de commande', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(961, 1, 'p009.s2.orderSupplierReference', 'Réf de commande', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(962, 1, 'p009.s1.orderSupplierReference', 'Réf de commande', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(963, 1, 'p008.s1', 'Alerte de stocks', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(964, 1, 'p008.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(965, 1, 'p008.s1.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(966, 1, 'p008.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(967, 1, 'p008.s2.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(968, 1, 'p008.s2.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(969, 1, 'p008.s2.threshold', 'Seuil d''alerte', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(970, 1, 'p008.s2.totalCommanded', 'Qté commandée', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(971, 1, 'p008.s2.totalRecieved', 'Qté reçue', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(972, 1, 'p005.s2.priceSale', 'Prix de vente ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(973, 1, 'p005.s2.threshold', 'Seuil d''alerte', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(974, 1, 'p005.s3.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(975, 1, 'p005.s3.additionalInformation', 'Information supplémentaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(976, 1, 'p005.s3.basicInformation', 'Informations de base', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(977, 1, 'p005.s3.color', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(978, 1, 'p005.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(979, 1, 'p005.s3.family', 'Famille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(980, 1, 'p005.s3.group', 'Groupe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(981, 1, 'p005.s3.note', 'Note', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(982, 1, 'p005.s3.priceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(983, 1, 'p005.s3.priceSale', 'Prix de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(984, 1, 'p005.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(985, 1, 'p005.s3.reorganization', 'Réorganisation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(986, 1, 'p005.s3.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(987, 1, 'p005.s3.threshold', 'Seuil d''alerte', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(988, 1, 'p005.s3.type', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(989, 1, 'p005.s3.unit', 'Unité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(990, 1, 'p012.s1', 'Produits : Information générale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(991, 1, 'p012.s1.productTotalQuantity', 'La quantité totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(992, 1, 'p012.s2', 'Commande : Information générale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(993, 1, 'p012.s2.orderSupplierInProgressCount', 'Opérations en cours', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(994, 1, 'p012.s2.orderSupplierTransmittedCount', 'Opérations soumise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(995, 1, 'p012.s2.orderSupplierValidatedCount', 'Opérations validées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(996, 1, 'p012.s3', 'Réceptions : Information générale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(997, 1, 'p012.s3.receptionInProgressCount', 'Opérations en cours', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(998, 1, 'p012.s3.receptionTransmittedCount', 'Opérations soumise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(999, 1, 'p012.s3.receptionValidatedCount', 'Opérations validées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1000, 1, 'p012.s4.orderInProgressCount', 'Opérations en cours', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1001, 1, 'p012.s4.orderTransmittedCount', 'Opérations soumise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1002, 1, 'p012.s4.orderValidatedCount', 'Opérations validées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1003, 1, 'p012.s5', 'Bon de retour : Information générale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(1004, 1, 'p012.s5.returnReceiptInProgressCount', 'Opérations en cours', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1005, 1, 'p012.s5.returnReceiptTransmittedCount', 'Opérations soumise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1006, 1, 'p012.s5.returnReceiptValidatedCount', 'Opérations validées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1008, 1, 'p012.s4', 'Ventes : Information générale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(1009, 1, 'p004.s1', 'Liste des fournisseurs', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(1010, 1, 'p004.s1.companyName', 'Entreprise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1011, 1, 'p004.s1.name', 'Nom ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1012, 1, 'p004.s1.phone', 'Télé\n', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1013, 1, 'p004.s1.email', 'E-mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1014, 1, 'p004.s1.type', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1015, 1, 'p004.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1017, 1, 'p004.s2', 'Ajouter / Modifier Fournisseur ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(1018, 1, 'p004.s2.nature', 'Nature', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1019, 1, 'p004.s2.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1020, 1, 'p004.s2.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1021, 1, 'p004.s2.companyName', 'Nom de l''entreprise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1022, 1, 'p004.s2.representative', 'Représentant', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1023, 1, 'p004.s2.shortLabel', 'Libellé court', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1024, 1, 'p004.s2.fullLabel', 'Libellé complète', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1025, 1, 'p004.s2.category', 'Catégorie', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1026, 1, 'p004.s2.type', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1027, 1, 'p004.s2.note', 'Note', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1028, 1, 'p004.s2.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1029, 1, 'p007.s1', 'Liste des clients', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(1030, 1, 'p007.s1.companyName', 'Entreprise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1031, 1, 'p007.s1.name', 'Nom complet', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1032, 1, 'p007.s1.phone', 'Télé\n', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1033, 1, 'p007.s1.email', 'E-mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1034, 1, 'p007.s1.type', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1035, 1, 'p007.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1036, 1, 'p007.s2', 'Ajouter / Modifier Client ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(1037, 1, 'p007.s2.nature', 'Nature', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1038, 1, 'p007.s2.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1039, 1, 'p007.s2.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1040, 1, 'p007.s2.companyName', 'Nom de l''entreprise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1041, 1, 'p007.s2.representative', 'Représentant', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1042, 1, 'p007.s2.shortLabel', 'Libellé court', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1043, 1, 'p007.s2.fullLabel', 'Libellé complète', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1044, 1, 'p007.s2.category', 'Catégorie', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1045, 1, 'p007.s2.type', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1046, 1, 'p007.s2.note', 'Note', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1047, 1, 'p007.s2.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1048, 1, 'p012.s2.orderSupplierRefusedCount', 'Opérations réfusées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1049, 1, 'p012.s2.orderSupplierRejectedCount', 'Opérations rejetées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1050, 1, 'p012.s3.receptionRefusedCount', 'Opérations réfusées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1051, 1, 'p012.s3.receptionRejectedCount', 'Opérations rejetées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1052, 1, 'p012.s4.orderdRefusedCount', 'Opérations réfusées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1053, 1, 'p012.s4.orderdRejectedCount', 'Opérations rejetées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1054, 1, 'p012.s5.returnReceiptRefusedCount', 'Opérations réfusées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1055, 1, 'p012.s5.returnReceiptRejectedCount', 'Opérations rejetées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1056, 1, 'p030.s1', 'La liste des utilisateurs', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(1057, 1, 'p030.s1.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1058, 1, 'p030.s1.username', 'Nom d''utilisateur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1059, 1, 'p030.s1.entityPhone', 'Téléphone', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1060, 1, 'p030.s1.entityEmail', 'E-mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1061, 1, 'p030.s1.category', 'Catégorie', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1062, 1, 'p030.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1063, 1, 'p030.s2', 'Ajouter / Modifier un utilisateur', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(1064, 1, 'p030.s2.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1065, 1, 'p030.s2.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1066, 1, 'p030.s2.username', 'Nom d''utilisateur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1067, 1, 'p030.s2.password', 'Mot de passe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1068, 1, 'p030.s2.category', 'Catégorie', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1069, 1, 'p030.s2.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1070, 1, 'p030.s1.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1071, 1, 'p051.s1', 'Information général', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(1072, 1, 'p051.s1.code', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1073, 1, 'p051.s1.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1074, 1, 'p051.s1.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1075, 1, 'p051.s1.companyName', 'Société', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1076, 1, 'p029.s1', 'La liste des modules', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(1077, 1, 'p029.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1078, 1, 'p029.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1079, 1, 'p029.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `INF_TEXT_TYPE`
--

CREATE TABLE IF NOT EXISTS `INF_TEXT_TYPE` (
  `ID` bigint(20) NOT NULL,
  `CODE` tinytext CHARACTER SET latin1,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `INF_TEXT_TYPE`
--

INSERT INTO `INF_TEXT_TYPE` (`ID`, `CODE`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'title', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'message', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'label', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'help', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(5, 'comment', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(6, 'placeholder', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(7, 'error', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `MAIL_CATEGORY`
--

CREATE TABLE IF NOT EXISTS `MAIL_CATEGORY` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` varchar(45) DEFAULT NULL,
  `SENDER` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `MAIL_CATEGORY`
--

INSERT INTO `MAIL_CATEGORY` (`ID`, `NAME`, `DESCRIPTION`, `SENDER`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Authentification', NULL, 'aaaa@bbb.cc', NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `MAIL_LOG`
--

CREATE TABLE IF NOT EXISTS `MAIL_LOG` (
  `ID` bigint(20) NOT NULL,
  `MESSAGE_ID` bigint(20) DEFAULT NULL,
  `SUBJECT` tinytext,
  `CONTENT` varchar(45) DEFAULT NULL,
  `SENDER` tinytext,
  `RECIPIENT` tinytext,
  `LOG_STATUS_ID` bigint(20) DEFAULT NULL,
  `DATE_SENT` datetime DEFAULT NULL,
  `USER_SENT` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `MAIL_LOG_STATUS`
--

CREATE TABLE IF NOT EXISTS `MAIL_LOG_STATUS` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` tinytext,
  `ACTIVE` char(1) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `MAIL_MESSAGE`
--

CREATE TABLE IF NOT EXISTS `MAIL_MESSAGE` (
  `ID` bigint(20) NOT NULL,
  `ACTION` tinytext,
  `DESCRIPTION` varchar(45) DEFAULT NULL,
  `SUBJECT` varchar(45) DEFAULT NULL,
  `CONTENT` varchar(45) DEFAULT NULL,
  `SENDER` varchar(45) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT NULL,
  `IS_SEND_TO_SUPER_ADMIN` char(1) DEFAULT 'Y',
  `IS_SEND_TO_CURRENT_USER` char(1) DEFAULT 'Y',
  `TYPE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `MAIL_MESSAGE`
--

INSERT INTO `MAIL_MESSAGE` (`ID`, `ACTION`, `DESCRIPTION`, `SUBJECT`, `CONTENT`, `SENDER`, `SORT_KEY`, `ACTIVE`, `IS_SEND_TO_SUPER_ADMIN`, `IS_SEND_TO_CURRENT_USER`, `TYPE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Login', NULL, 'Login Subject', 'Login Mail Content', 'abdessamad.hallal@gmail.com', 1, 'Y', 'Y', 'Y', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `MAIL_MESSAGE_CLIENT`
--

CREATE TABLE IF NOT EXISTS `MAIL_MESSAGE_CLIENT` (
  `ID` bigint(20) NOT NULL,
  `SUBJECT` varchar(45) DEFAULT NULL,
  `CONTENT` varchar(45) DEFAULT NULL,
  `SENDER` varchar(45) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT NULL,
  `IS_SEND_TO_SUPER_ADMIN` char(1) DEFAULT 'Y',
  `IS_SEND_TO_CURRENT_USER` char(1) DEFAULT 'Y',
  `MESSAGE_ID` bigint(20) DEFAULT NULL,
  `INF_CLIENT_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `MAIL_MESSAGE_CLIENT`
--

INSERT INTO `MAIL_MESSAGE_CLIENT` (`ID`, `SUBJECT`, `CONTENT`, `SENDER`, `ACTIVE`, `IS_SEND_TO_SUPER_ADMIN`, `IS_SEND_TO_CURRENT_USER`, `MESSAGE_ID`, `INF_CLIENT_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Subject AAA', 'content CCC', 'sybaway@gmail.cil', 'Y', 'Y', 'Y', 1, 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `MAIL_MESSAGE_CLINET_RECIPIENT`
--

CREATE TABLE IF NOT EXISTS `MAIL_MESSAGE_CLINET_RECIPIENT` (
  `ID` bigint(20) NOT NULL,
  `MESSAGE_ID` bigint(20) DEFAULT NULL,
  `CLT_CLIENT_ID` bigint(20) DEFAULT NULL,
  `CLT_USER_ID` varchar(45) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `MAIL_MESSAGE_MODEL`
--

CREATE TABLE IF NOT EXISTS `MAIL_MESSAGE_MODEL` (
  `ID` bigint(20) NOT NULL,
  `SUBJECT` varchar(45) DEFAULT NULL,
  `CONTENT` varchar(45) DEFAULT NULL,
  `SENDER` varchar(45) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT NULL,
  `IS_SEND_TO_SUPER_ADMIN` char(1) DEFAULT 'Y',
  `IS_SEND_TO_CURRENT_USER` char(1) DEFAULT 'Y',
  `MESSAGE_ID` bigint(20) DEFAULT NULL,
  `PM_MODEL_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `MAIL_MESSAGE_MODULE`
--

CREATE TABLE IF NOT EXISTS `MAIL_MESSAGE_MODULE` (
  `ID` bigint(20) NOT NULL,
  `SUBJECT` varchar(45) DEFAULT NULL,
  `CONTENT` varchar(45) DEFAULT NULL,
  `SENDER` varchar(45) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT NULL,
  `IS_SEND_TO_SUPER_ADMIN` char(1) DEFAULT 'Y',
  `IS_SEND_TO_CURRENT_USER` char(1) DEFAULT 'Y',
  `MESSAGE_ID` bigint(20) DEFAULT NULL,
  `INF_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `MAIL_MESSAGE_MODULE_RECIPIENT`
--

CREATE TABLE IF NOT EXISTS `MAIL_MESSAGE_MODULE_RECIPIENT` (
  `ID` bigint(20) NOT NULL,
  `MESSAGE_ID` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `CLT_USER_ID` varchar(45) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `MAIL_MESSAGE_PLACEHOLDER`
--

CREATE TABLE IF NOT EXISTS `MAIL_MESSAGE_PLACEHOLDER` (
  `ID` bigint(20) NOT NULL,
  `MESSAGE_ID` bigint(20) DEFAULT NULL,
  `MESSAGE_PLACEHOLDER_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `MAIL_MESSAGE_SCHEDULE`
--

CREATE TABLE IF NOT EXISTS `MAIL_MESSAGE_SCHEDULE` (
  `ID` bigint(20) NOT NULL,
  `MESSAGE_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `MAIL_PLACEHOLDER`
--

CREATE TABLE IF NOT EXISTS `MAIL_PLACEHOLDER` (
  `ID` bigint(20) NOT NULL,
  `NAME` varchar(45) DEFAULT NULL,
  `DESCRIPTION` varchar(45) DEFAULT NULL,
  `KEY` varchar(45) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `MAIL_TYPE`
--

CREATE TABLE IF NOT EXISTS `MAIL_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` varchar(45) DEFAULT NULL,
  `SENDER` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `MAIL_TYPE`
--

INSERT INTO `MAIL_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SENDER`, `SORT_KEY`, `ACTIVE`, `CATEGORY_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Login', NULL, 'eeee@gggg.gg', NULL, 'Y', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `MIG_INF_CITY`
--

CREATE TABLE IF NOT EXISTS `MIG_INF_CITY` (
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `MIG_INF_CITY`
--

INSERT INTO `MIG_INF_CITY` (`name`) VALUES
('Agadir'),
('Ain Harrouda'),
('Ait Baha'),
('Ait Melloul'),
('Al Haouz'),
('Al Hocïema'),
('Aousserd'),
('Arfoud'),
('Assa zag'),
('Assilah'),
('Azemmour'),
('Azilal'),
('Azrou'),
('Ben Ahmed'),
('Ben Guerir'),
('Béni Mellal'),
('Benslimane'),
('Berkane'),
('Berrechid'),
('Bin El Ouidane'),
('Bir Jdid'),
('Boujdour'),
('Boujniba'),
('Boulanouar'),
('Boulmane'),
('Bouskoura'),
('Bouznika'),
('Casablanca'),
('Chefchaouen'),
('Chichaoua'),
('Dakhla'),
('Dar Chaffai'),
('Deroua'),
('El Borouj'),
('El Gara'),
('El Hajeb'),
('El Harhoura'),
('El Jadida'),
('El Mansouria'),
('Errachidia'),
('Es-Semara'),
('Essaouira'),
('Fès'),
('Fnideq'),
('Fquih Ben Saleh'),
('Goulmima'),
('Guelmim'),
('Guercif'),
('Had Soualem'),
('Ifrane'),
('Imouzzer'),
('Inzegan'),
('Jamaat Shaim'),
('Jrada'),
('Kelaat Es-Sraghna'),
('Kénitra'),
('Khemisset'),
('Khénifra'),
('Khouribga'),
('Ksar el-Kebir'),
('Ksar es-Seghir'),
('Laâyoune'),
('Lagouira'),
('Lakhiaita'),
('Larache'),
('Marrakech'),
('Martil'),
('Mdiq'),
('Mediouna'),
('Mehdia'),
('Meknès'),
('Merzouga'),
('Midelt'),
('Mirleft'),
('Mohammedia'),
('Moulay Bousselham'),
('Moulay Yaccoub'),
('Nador'),
('Nouaceur'),
('Oualidia'),
('Ouarzazate'),
('Ouazzane'),
('Oued Zem'),
('Oujda'),
('Ouled Frej'),
('Outat El Haj'),
('Rabat'),
('Rissani'),
('Safi'),
('Saidia'),
('Sala Al-Jadida'),
('Salé'),
('Sebt Gzoula'),
('Sefrou'),
('Selouane'),
('Settat'),
('Sidi Bennour'),
('Sidi Bouzid'),
('Sidi el Aîdi'),
('Sidi Hajjaj'),
('Sidi Ifni'),
('Sidi Kacem'),
('Sidi Rahhal'),
('Sidi Slimane'),
('Sidi Yahya El Gharb'),
('Skhirat'),
('Soualem'),
('Taghazout'),
('Tamaris'),
('Tamensourt'),
('Tamouda Bay'),
('Tan-Tan'),
('Tanger'),
('Taounate'),
('Taourirt'),
('Tarfaya'),
('Taroudant'),
('Tata'),
('Taza'),
('Temara'),
('Temsia'),
('Tétouan'),
('Tifelt'),
('Tikiouine'),
('Tiznit'),
('Youssoufia'),
('Zagora'),
('Zemamra');

-- --------------------------------------------------------

--
-- Structure de la table `MIG_SM_PRODUCT`
--

CREATE TABLE IF NOT EXISTS `MIG_SM_PRODUCT` (
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `MIG_SM_PRODUCT`
--

INSERT INTO `MIG_SM_PRODUCT` (`name`) VALUES
('Toner HP Lazer Jet 5A'),
('Toner HP Lazer Jet 12A'),
('Toner HP Lazer Jet 15A'),
('Toner HP Lazer Jet 35A'),
('Toner HP Lazer Jet 36A'),
('Toner HP Lazer Jet 38A'),
('Toner HP Lazer Jet 42A'),
('Toner HP Lazer Jet 53A'),
('Toner HP Lazer Jet 85A'),
('Toner HP Lazer Jet 90A'),
('Toner HP Lazer Jet C530A'),
('Toner HP Lazer Jet C531A'),
('Toner HP Lazer Jet C532A'),
('Toner HP Lazer Jet C533A'),
('Toner HP Lazer Jet C540A'),
('Toner HP Lazer Jet C541A'),
('Toner HP Lazer Jet C542A'),
('Toner HP Lazer Jet C543A'),
('Toner HP Lazer Jet C645K'),
('Toner HP Lazer Jet C645C'),
('Toner HP Lazer Jet C645M'),
('Toner HP Lazer Jet C645Y'),
('CDR'),
('DVDR'),
('Clé USB 8G'),
('Clé USB 16G'),
('Clé USB 32G'),
('Clé USB 64G'),
('Toner LEXMARK C734K'),
('Toner LEXMARK C734C'),
('Toner LEXMARK C734M'),
('Toner LEXMARK C734Y'),
('Toner LEXMARK 540K'),
('Toner LEXMARK 540C'),
('Toner LEXMARK 540M'),
('Toner LEXMARK 540Y'),
('Photo conducteur X734'),
('Rame Papier photocopieur 21X29,7 80grs Unt'),
('Parapheur 18 volets'),
('pochette perforée A4 - Paquet de 100 pochettes'),
('Rame chemise bulle blanche paquet de 250 chemises 100 grs'),
('Rame chemise cartonnée couleur bleue Extra de 100 Unt 180 grs '),
('Rame chemise bulle couleur jaune double - paquet de 250 chemises 100 grs '),
('Couverture transparente A4 0,15mm X 210mm X 29,7mm'),
('Rame chemise à sangle D.T de 10 Unt'),
('Chemise à coin  4100 paquets de 100 pc'),
('Encre Toner noir pour fax Brother 2845 TN2220'),
('Encre Toner noir pour fax Brother 2845 TN2000'),
('Stylo feutre G3 noir paquet de 12 pc '),
('Stylo feutre G3 bleu paquet de 12 pc '),
('Agrafeuse 24/6 et 26/6'),
('Blanco Stylo paquet de 24 pc'),
('Tube stick colle 21 GM paquet de 12 pc'),
('Boite attache de 24/8mm 100 Unt'),
('Boite agrafes 24/6 mm '),
('Trombones 28mm plastifIés 100 pc'),
('Registre de 5 mains'),
('Registre de 3 mains'),
('Chemise a Rabat élastique 400 grs'),
('Boite d''archive GM n°135'),
('Parapheur de 25 volets'),
('Parapheur 12 volets'),
('Grayons noirs paquet de 12 pc'),
('Ciseaux GM 17cm'),
('Chemise suspendus - Réf 380 tiroirs couleur orange paquet de 100 pc'),
('Serres feuilles Couleur blanche n°6'),
('Serres feuilles Couleur blanche n°10'),
('Serres feuilles Couleur blanche n°12'),
('Serres feuilles Couleur blanche n°14'),
('Serres feuilles Couleur blanche n°28'),
('Serres feuilles Couleur blanche n°30'),
('Stylos Bleus à bille'),
('Stylos Noirs à bille'),
('Stylos Rouge à bille'),
('Marqueurs Noirs'),
('Marqueurs Bleus'),
('Fluorescents jaunes'),
('Fluorescents Verts'),
('Fluorescents Orange'),
('Encreur Bleu'),
('Toner pour photocopieur Toshiba sudio 206'),
('Toner pour photocopieur Toshiba sudio 163'),
('Toner pour photocopieur Toshiba sudio 207'),
('Toner pour photocopieur Sharp AR 5516'),
('Toner couleur noir pour photocopieur Sharp M-X 2301 N'),
('Toner couleur magenta pour photocopieur Sharp M-X 2301 N'),
('Toner couleur jaune pour photocopieur Sharp M-X 2301 N'),
('Toner couleur bleue pour photocopieur Sharp M-X 2301 N'),
('Toner pour photocopieur Sharp M-X 503 N'),
('Toner pour photocopieur Xerox Workcenter 5230'),
('Toner pour photocopieur Xerox Workcenter 5222'),
('Toner pour photocopieur develop Ineo+215'),
('Toner couleur noire pour photocopieur develop Ineo+224e'),
('Toner couleur magenta pour photocopieur develop Ineo+224e'),
('Toner couleur jaune pour photocopieur develop Ineo+224e'),
('Toner couleur bleue pour photocopieur develop Ineo+224e'),
('Toner couleur noire pour photocopieur develop Ineo+3110'),
('Toner couleur magenta pour photocopieur develop Ineo+3110'),
('Toner couleur jaune pour photocopieur develop Ineo+3110'),
('Toner couleur bleue develop Ineo + 3110'),
('Toner pour photocopieur develop Ineo 423'),
('Toner HP CC531 cyan'),
('Toner HP CC532 jaune'),
('Toner HP CC533 magenta'),
('Toner HP CC530 noir'),
('Toner HP 36A'),
('Toner Lexmark E260A'),
('CD imation avec pouchettes'),
('Photoconducteure lexmark C540'),
('DVD imatchettesion avec pou'),
('Cartouche HP 10 noir '),
('Cartouche HP 11 magenta'),
('Cartouche HP 11 cayan'),
('Cartouche HP 11 jaune'),
('Toner develope TN 414'),
('Toner Xerox 5222'),
('Drum Xerox 5222'),
('Unite d''image E250'),
('Drum brother DR 2200'),
('Toner brother TN 2210'),
('Toner HP 05A'),
('Toner Lexmark C540A1CG bleu'),
('Toner Lexmark C540A1YG jaune'),
('Toner Lexmark C540A1MG magenta'),
('Toner Lexmark C540A1MG noir'),
('Toner HP 90A'),
('Photoconducteure lexmarK E260'),
('Clé USB 4 GB TDK '),
('Bloc note A4160p point de mire '),
('Paquet etiquettes 4 formtec lsm 2626'),
('Classeur chrono noir'),
('Serre feuilles express 16mm'),
('Serre feuilles express 12mm'),
('Serre feuilles express 14mm'),
('Pouchettes blanc avec bande securite 26X36'),
('Boite agrafe 24/8'),
('Boite agrafe 23/10'),
('Boite agrafe 23/17'),
('Carton double format 30X30X50'),
('Agrafeuse geante'),
('Registre pot 4 mains'),
('Marqueur permanent mon ami bleu et noir'),
('Boite agrafe 24/6'),
('Scotche d''emballage 100m marron'),
('Agrafeuse deli 24/6'),
('Pouchettes en plastique 80 micro prérforé'),
('Corn box en simili'),
('Chemises à Rabat en carte de lyon '),
('Spirale en plastique DM28MM'),
('Parapheur 18 volet '),
('Porte tarif spirale deli'),
('Boite de trombonne plastifiés'),
('chemises leitz'),
('Punaises chromé'),
('Post it 4 couleur 75X75'),
('Ouvre lettres maped'),
('Stylo pilot V5'),
('Chemises à songle'),
('Toner sharp AR21FT'),
('Toner HP 85A'),
('Clé USB  virbatime 4GB'),
('Toner HP 35A'),
('Clé USB 16 GB TDK'),
('Clé USB 8GB TDK'),
('CD arita'),
('Souris touchmat'),
('Clé USB 32GO Kingston'),
('CD imprimable imation'),
('Toner HP CB 540A noir'),
('Toner HP CB 541A cayan'),
('Toner HP CB 542A jaune'),
('Toner HP CB 543A magenta'),
('Toner HP 12A'),
('Cassettes mini dv'),
('Pille Sony'),
('Toner Lexmark C 734 noir'),
('Toner Lexmark C 734 cayan'),
('Toner Lexmark C 734 jaune'),
('Toner Lexmark C 734 magenta'),
('Javel ACE'),
('Sanicroix propre '),
('Déodorant'),
('Papier hygienique '),
('Savon taouss'),
('Chamoisine'),
('Peau de chameau '),
('Esprit de sel'),
('Abattant'),
('Boite baguette de soudure'),
('Boite de poudre'),
('Chauffe eau de 80 litre'),
('Chlore en 5 kg'),
('Colle divnyl'),
('Coude 20x3/4 Tm'),
('Coude retube Retube de 20'),
('Coude wc'),
('Ensemble WC 1er choix'),
('flexible branchement'),
('Glace de lavabo avec patte'),
('Groupe sécurité 3/4'),
('Kit de jonction'),
('Kit de wc complet clever'),
('lavabo avec colonne'),
('mecanimse poussoir'),
('Mecanisme poussoir SAS'),
('Melangeur  lavabo'),
('Melangeur cuisine'),
('Mitigeur Bidet'),
('mitigeur lavabo'),
('Rallonge wc '),
('Reservoire wc complet'),
('Robinet de service'),
('Tete de melangeur'),
('Tuyau d''arrosage'),
('Wc complet à l''angalise complet'),
('Accessoires wc en inox'),
('Chauffe eau de 80 litre'),
('Chauffe eau instantanné'),
('Collier galvanisé 3/4'),
('Coude 1/2 chromé'),
('Cuivre 12mm'),
('Esprit de sel'),
('Fixation wc'),
('Flexible de mitigeur'),
('Flexible evacuation wc Italy'),
('Machine pour ppr '),
('Plongeoir complet '),
('Polytilene 25'),
('Pompe 2ch 220v'),
('Porte savon liquide inox'),
('PVC d''evacuation'),
('PVC d''evacuation 110'),
('Raccord 1/2/3/4'),
('Raccord 20*20 tm'),
('Raccord de 20 male de retube'),
('Raccord male de 16 tm'),
('Raccord male de 20 tm'),
('Raccord union 1P'),
('Raccord union 2P'),
('Reduction 1"* 3/4'),
('Reduction 3/4*1/2'),
('Reservoire wc complet'),
('Retube en alluminuim 16'),
('Retube en alluminuim 20'),
('Robiner jardin 3/4'),
('Robinet 1/2 1/2'),
('Robinet 1/2 3/8'),
('Robinet à bec'),
('Robinet d''arret 1/2 tm'),
('Robinet d''arret 3/4 tm'),
('Robinet de lavabo'),
('Robinet de service'),
('Robinet en cuivre pour fontaine'),
('Robinet flotteur'),
('Robinet jardin 3/4 '),
('Robinet oslo ST'),
('Silicone d''etanchieté'),
('Siphon 32'),
('Siphon 40'),
('Siphon gorge '),
('Té 20*20 Tm'),
('Té chromé 1/2'),
('Te egale de 16'),
('Teflon gm'),
('Tube galvanisé 3/4'),
('Vane 3/4'),
('Vanne 1"1/2'),
('Vanne à bille 1"'),
('Vanne à volant 2"'),
('vanne jardin'),
('Vide cave'),
('Wc  à la turque'),
('Wc complet à l''angalise complet'),
('Essence White spirit  "en litre"'),
('Enduit poudre "  en kg"'),
('Duliant  en litre'),
('Peinture coloflexe "en kg"'),
('Peinture à eau   en kg'),
('Peinture antirouille  en kg'),
('Peinture laque blanche en kg'),
('vernis  en litre'),
('Teinte  divers');

-- --------------------------------------------------------

--
-- Structure de la table `PM_ATTRIBUTE_VALIDATION`
--

CREATE TABLE IF NOT EXISTS `PM_ATTRIBUTE_VALIDATION` (
  `ID` bigint(20) NOT NULL,
  `PAGE_ATTRIBUTE_ID` bigint(20) DEFAULT NULL,
  `VALIDATION_TYPE_ID` bigint(20) DEFAULT NULL,
  `PARAMS` mediumtext CHARACTER SET latin1,
  `CUSTOM_ERROR` text CHARACTER SET latin1,
  `MODEL_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `PM_CATEGORY`
--

CREATE TABLE IF NOT EXISTS `PM_CATEGORY` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT NULL,
  `CATEGORY_TYPE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `IMAGE_PATH` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `PM_CATEGORY`
--

INSERT INTO `PM_CATEGORY` (`ID`, `NAME`, `DESCRIPTION`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `ACTIVE`, `CATEGORY_TYPE_ID`, `SORT_KEY`, `IMAGE_PATH`) VALUES
(1, 'Fichiers', 'Fichiers', NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(2, 'Entrées', 'Entrées', NULL, NULL, NULL, NULL, 'Y', 1, 2, 'icons/1.png'),
(3, 'Sorties', 'Sorties', NULL, NULL, NULL, NULL, 'Y', 1, 3, 'icons/1.png'),
(4, 'Divers', 'Divers', '2014-11-13 00:00:00', NULL, NULL, NULL, 'Y', 1, 4, 'icons/1.png'),
(5, 'Statistiques', 'Rapport', NULL, NULL, NULL, NULL, 'Y', 1, 6, 'icons/1.png'),
(6, 'Recourcie', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 7, 'icons/1.png'),
(7, 'Generale', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(8, 'Ressource', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 2, 'icons/1.png'),
(9, 'Module', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 3, 'icons/1.png'),
(10, 'Parametrage', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 4, 'icons/1.png'),
(11, 'Generale', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(12, 'Ressource', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(13, 'Modules', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(14, 'Paramètrage', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(15, 'Paramètrage', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(16, 'Commandes', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(17, 'Retour - Avoir', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/19_48.png'),
(18, 'Locataire', 'Locataire', NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(19, 'Administration', 'Administration', NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png');

-- --------------------------------------------------------

--
-- Structure de la table `PM_CATEGORY_TYPE`
--

CREATE TABLE IF NOT EXISTS `PM_CATEGORY_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext CHARACTER SET latin1,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `PM_CATEGORY_TYPE`
--

INSERT INTO `PM_CATEGORY_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Header', 'DFFF', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Sub Header', ' FGGE', 2, 'Y', NULL, NULL, NULL, NULL),
(3, 'Principal', ' FERT', 3, 'Y', NULL, NULL, NULL, NULL),
(4, 'Footer', 'FFZF', 5, 'Y', NULL, NULL, NULL, NULL),
(5, 'Footer', '  GGDZ', 4, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `PM_COMPONENT`
--

CREATE TABLE IF NOT EXISTS `PM_COMPONENT` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` tinytext,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` tinytext,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `PM_COMPONENT`
--

INSERT INTO `PM_COMPONENT` (`ID`, `NAME`, `DESCRIPTION`, `ACTIVE`, `SORT_KEY`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'inpuText', NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'textarea', NULL, 'Y', 2, NULL, NULL, NULL, NULL),
(3, 'comobox', NULL, 'Y', 3, NULL, NULL, NULL, NULL),
(4, 'table', NULL, 'Y', 4, NULL, NULL, NULL, NULL),
(5, 'column', NULL, 'Y', 5, NULL, NULL, NULL, NULL),
(6, 'other', NULL, 'Y', 6, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `PM_COMPOSITION`
--

CREATE TABLE IF NOT EXISTS `PM_COMPOSITION` (
  `ID` bigint(20) NOT NULL,
  `GROUP_ID` bigint(20) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `MENU_ID` bigint(20) DEFAULT NULL,
  `PAGE_ID` bigint(20) DEFAULT NULL,
  `INDEX_SHOW` char(1) DEFAULT NULL,
  `GROUP_SORT` bigint(20) DEFAULT NULL,
  `MENU_SORT` bigint(20) DEFAULT NULL,
  `CATEGORY_SORT` bigint(20) DEFAULT NULL,
  `PAGE_SORT` bigint(20) DEFAULT NULL,
  `INDEX_SORT` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `MODEL_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=221 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `PM_COMPOSITION`
--

INSERT INTO `PM_COMPOSITION` (`ID`, `GROUP_ID`, `CATEGORY_ID`, `MENU_ID`, `PAGE_ID`, `INDEX_SHOW`, `GROUP_SORT`, `MENU_SORT`, `CATEGORY_SORT`, `PAGE_SORT`, `INDEX_SORT`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `MODEL_ID`) VALUES
(1, 3, 1, 1, 5, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(2, 3, 1, 2, 12, '', 1, 3, 1, 2, 0, 'Y', NULL, NULL, NULL, NULL, 1),
(4, 3, 1, 4, 4, '', 1, 4, 1, 6, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(5, 3, 2, 5, 35, NULL, 1, 2, 3, 7, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(6, 3, 2, 6, 36, NULL, 1, 3, 3, 8, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(7, 3, 2, 7, 37, NULL, 1, 4, 3, 9, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(8, 3, 1, 8, 7, NULL, 1, 5, 1, 10, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(9, 3, 3, 9, 38, NULL, 1, 15, 4, 11, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(10, 3, 3, 10, 39, NULL, 1, 16, 4, 12, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(11, 3, 3, 11, 40, NULL, 1, 17, 4, 13, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(15, 3, 5, 15, 14, NULL, 1, 21, 6, 15, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(16, 2, 6, 16, 5, NULL, 1, 22, 22, 16, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(17, 2, 6, 17, 8, NULL, 1, 23, 23, 17, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(18, 2, 6, 18, 9, NULL, 1, 24, 24, 18, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(19, 2, 6, 19, 35, NULL, 1, 25, 25, 19, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(20, 2, 6, 20, 38, NULL, 1, 29, 29, 24, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(188, 3, 7, 21, 1, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 2),
(197, 3, 19, 27, 30, '', 1, 2, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 3),
(199, 3, 19, 28, 29, NULL, 1, 3, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 3),
(200, 3, 19, 29, 25, NULL, 1, 4, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 3),
(201, 3, 15, 30, 21, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 4),
(202, 3, 15, 31, 22, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 4),
(203, 3, 15, 32, 23, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 4),
(204, 3, 15, 33, 24, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 4),
(207, 1, 1, 36, 20, NULL, 1, 30, 1, 25, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(208, 1, 1, 36, 20, NULL, 1, 23, 23, 23, NULL, 'Y', NULL, NULL, NULL, NULL, 2),
(209, 3, 19, 26, 51, NULL, 1, 1, 24, 24, NULL, 'Y', NULL, NULL, NULL, NULL, 3),
(210, 1, 1, 36, 20, NULL, 1, 25, 25, 25, NULL, 'Y', NULL, NULL, NULL, NULL, 4),
(211, 3, 16, 38, 32, NULL, 1, 3, 2, 3, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(212, 3, 16, 39, 33, NULL, 1, 4, 2, 4, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(213, 3, 16, 40, 34, NULL, 1, 5, 2, 5, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(214, 3, 17, 41, 41, NULL, 1, 26, 5, 20, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(215, 3, 17, 42, 42, NULL, 1, 27, 5, 22, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(216, 3, 17, 43, 43, NULL, 1, 28, 5, 23, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(217, 3, 1, 44, 8, NULL, 1, 2, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(218, 3, 2, 45, 9, NULL, 1, 1, 3, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(220, 1, 1, 36, 20, NULL, 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 3);

-- --------------------------------------------------------

--
-- Structure de la table `PM_DATA_TYPE`
--

CREATE TABLE IF NOT EXISTS `PM_DATA_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `PARAM_NUMBER` bigint(20) DEFAULT '1',
  `PARAMS` tinytext,
  `ERROR_MESSAGE` tinytext,
  `HELP` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `PM_DATA_TYPE`
--

INSERT INTO `PM_DATA_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `PARAM_NUMBER`, `PARAMS`, `ERROR_MESSAGE`, `HELP`) VALUES
(1, 'Texte', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 1, 'String', 'validation.v1.dataTypeString', NULL),
(2, 'Caractère', NULL, 2, 'Y', NULL, NULL, NULL, NULL, 1, 'Char', 'validation.v1.dataTypeChar', NULL),
(3, 'Nombre entière', NULL, 3, 'Y', NULL, NULL, NULL, NULL, 1, 'Integer', 'validation.v1.dataTypeInteger', NULL),
(4, 'Nombre réel', NULL, 4, 'Y', NULL, NULL, NULL, NULL, 1, 'Double', 'validation.v1.dataTypeDouble', NULL),
(5, 'datetime', NULL, 5, 'Y', NULL, NULL, NULL, NULL, 1, 'DateTime', 'validation.v1.dataTypeDateTime', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `PM_FORMAT_TYPE`
--

CREATE TABLE IF NOT EXISTS `PM_FORMAT_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `PARAM_NUMBER` bigint(20) DEFAULT '1',
  `PARAMS` tinytext,
  `ERROR_MESSAGE` tinytext,
  `HELP` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `PM_FORMAT_TYPE`
--

INSERT INTO `PM_FORMAT_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `PARAM_NUMBER`, `PARAMS`, `ERROR_MESSAGE`, `HELP`) VALUES
(1, 'None', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'none', 'validation.v1.formatTypeNone', NULL),
(2, 'Email', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'mail', 'validation.v1.formatTypeEmail', NULL),
(3, 'Phone', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'phone', 'validation.v1.formatTypePhone', NULL),
(4, 'Fax', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'fax', 'validation.v1.formatTypeFax', NULL),
(5, 'Code postale', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'codePostale', 'validation.v1.formatTypeCodePostale', NULL),
(6, 'Date', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'date', 'validation.v1.formatTypeDate', NULL),
(7, 'Time', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'time', 'validation.v1.formatTypeTime', NULL),
(8, 'DateTime', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'datetime', 'validation.v1.formatTypeDateTime', NULL),
(9, 'Pattern', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, NULL, 'validation.v1.formatTypePattern', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `PM_GROUP`
--

CREATE TABLE IF NOT EXISTS `PM_GROUP` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `GROUP_TYPE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `PM_GROUP`
--

INSERT INTO `PM_GROUP` (`ID`, `NAME`, `DESCRIPTION`, `GROUP_TYPE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Header', 'header', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'sub Header', 'sub Header', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(3, 'Menu', 'Menu', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(4, 'Footer', 'Footer', 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `PM_GROUP_TYPE`
--

CREATE TABLE IF NOT EXISTS `PM_GROUP_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `PM_GROUP_TYPE`
--

INSERT INTO `PM_GROUP_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Simple', 'Simple', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Avancée', 'Avancée', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `PM_MENU`
--

CREATE TABLE IF NOT EXISTS `PM_MENU` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `MENU_TYPE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `IMAGE_PATH` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `PM_MENU`
--

INSERT INTO `PM_MENU` (`ID`, `NAME`, `DESCRIPTION`, `MENU_TYPE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `IMAGE_PATH`) VALUES
(1, 'Produit', 'Page de TEST', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/1_64.png'),
(2, 'Statut de stock', 'Gestion des produits', 1, 2, 'Y', NULL, NULL, NULL, NULL, 'icons/16_64.png'),
(3, 'Inventaire', 'Gestion des dépenses', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(4, 'Gestion des Fournisseurs', 'Gestion des fournisseurs   ', 1, NULL, 'Y', NULL, NULL, NULL, NULL, 'icons/17_64.png'),
(5, 'Reception', 'Gestion des réception', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/15_48.png'),
(6, 'Validation des réceptions', 'Validation des réceptions', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/12_48.png'),
(7, 'Historique des réceptions', 'Historique des réceptions', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/11_48.png'),
(8, 'Gestion des Clients', 'Fournisseurs', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/18_64.png'),
(9, 'Nouveau Bon laivraison', 'Nouveau Bon laivraison', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/15_48.png'),
(10, 'Validation des BL', 'Validation des BL', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/12_48.png'),
(11, 'Historiques des BL', 'Historiques des BL', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/11_48.png'),
(12, 'Changer un produit', 'Gestion des clients', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/1_64.png'),
(13, 'Dépenses', 'Dépenses', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(14, 'Avance de produit', 'Avance de produit', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/11_64.png'),
(15, 'Rapports et statistiques', 'Promotion des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/chart.png'),
(16, 'Produit', 'Produit', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/1_64.png'),
(17, 'Seuil d''alerte', 'Seuil d''alerte', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/4_64.png'),
(18, 'En arrivage', 'En arrivage', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/3_64.png'),
(19, 'Réception', 'Réception', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/15_48.png'),
(20, 'Ventes', 'Ventes', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/6_64.png'),
(21, 'Information générale', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(22, 'Rôle d''utilisateur', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(23, 'Proprietes des elements', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(24, 'Configuration des pages', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(25, 'Configuration générale', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(26, 'Information', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(27, 'Utilisateur', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(28, 'Modules', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(29, 'Parametrage', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(30, 'Les paramèetres Basique', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(31, 'Les paramèetres Client', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(32, 'Les paramèetres module', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(33, 'Les paramèetres de page', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(34, 'Promotions de produits', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(35, 'Liste des valeurs', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(36, 'Mon Profile', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/4.png'),
(37, 'to del', 'Configuration des modules', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(38, 'Commandes', 'Commandes', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/15_48.png'),
(39, 'Validation commande', 'Validation commande', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/12_48.png'),
(40, 'Historique commande', 'Historique commande', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/11_48.png'),
(41, 'Bon de retour', 'Bon de retour', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/15_48.png'),
(42, 'Validation Bon de retour', 'Validation Bon de retour', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/12_48.png'),
(43, 'Historique bon de retour', 'Historique bon de retour', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/11_48.png'),
(44, 'Alert produit', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/4_64.png'),
(45, 'En arrivage', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/3_64.png'),
(46, 'Information de locataire', 'Information de locataire', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png');

-- --------------------------------------------------------

--
-- Structure de la table `PM_MENU_TYPE`
--

CREATE TABLE IF NOT EXISTS `PM_MENU_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `PM_MENU_TYPE`
--

INSERT INTO `PM_MENU_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'menu1 typ', 'Menu 1 type ', 1, 'Y', NULL, NULL, NULL, NULL),
(4, 'menu type 2', ' mznu type 2', 2, 'Y', NULL, NULL, NULL, NULL),
(6, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `PM_MODEL`
--

CREATE TABLE IF NOT EXISTS `PM_MODEL` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `INF_PACKAGE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(20) DEFAULT 'Y'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `PM_MODEL`
--

INSERT INTO `PM_MODEL` (`ID`, `NAME`, `DESCRIPTION`, `INF_PACKAGE_ID`, `ACTIVE`) VALUES
(1, 'Modél de Stock', 'Modél de Stock', 1, 'Y'),
(2, 'Model d''admin de stock', 'Model d''admin de stock', 1, 'Y'),
(3, 'Modél de super admin', 'Modél de super admin', 1, 'Y'),
(4, 'Modél de webmaster', 'Modél de webmaster', 1, 'Y');

-- --------------------------------------------------------

--
-- Structure de la table `PM_MODEL_STATUS`
--

CREATE TABLE IF NOT EXISTS `PM_MODEL_STATUS` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `PM_PAGE`
--

CREATE TABLE IF NOT EXISTS `PM_PAGE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `PAGE_TYPE_ID` bigint(20) DEFAULT NULL,
  `IN_DEV` char(1) DEFAULT 'N',
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1009 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `PM_PAGE`
--

INSERT INTO `PM_PAGE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `PAGE_TYPE_ID`, `IN_DEV`, `ACTIVE`, `USER_CREATION`, `DATE_CREATION`, `USER_UPDATE`, `DATE_UPDATE`) VALUES
(1, 'page Test', 'page test1', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(2, 'Page add admin ', 'Page add admin', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(3, 'Page Expense', 'Page pour la gestion des dépense', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(4, 'Page Supplier', 'Page pour la gestion des fournisseurs', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(5, 'Page produit', 'Page pour la gestion des produits', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(6, 'Page de réception', 'Page pour la gestion des réceptions et BL', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(7, 'Page de custmer', 'customer', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(8, 'Alert produit', 'Alert produit', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(9, 'En arrivage', 'En arrivage', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(10, 'Page de promotion des prodtuis', 'promo produit', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(11, 'Inventaire', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(12, 'statut de stock : capital', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(13, 'Bon de livraison Client', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(14, 'Valdation de Bon de livraison Client', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(15, 'Historique de Bon de livraison Client', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(16, 'les rapports cotidiennet', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(17, 'Changé un produit', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(18, 'avance de produit', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(19, 'la liste des valeurs', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(20, 'Mon profil', NULL, 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(21, 'Les paramètres basiques', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(22, 'Paramètres', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(23, 'Parameter du module', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(24, 'Parameter du page', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(25, 'Parameter du client', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(26, 'Parameter du module/client', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(27, 'Parameter du page/module', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(28, 'Gestion des clients', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(29, 'Gestion des modules', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(30, 'Gestion des utilisateurs', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(31, 'Proprietes des elements', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(32, 'Commande fournisseur', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(33, 'Validation commande fournisseur', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(34, 'Historique commande fournisseur', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(35, 'Receptions en cours', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(36, 'Validation des receptions', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(37, 'Historique des validations', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(38, 'Ventes en cours', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(39, 'Validation des ventes', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(40, 'Historique des ventes', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(41, 'Avoir en cours', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(42, 'Validation Bon de retour', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(43, 'Historique Bon de retour', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(51, 'Information de client (locataire)', 'Information de client (locataire)', NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(1001, 'Afficher la liste des commandes ', NULL, NULL, 2, 'N', 'Y', NULL, NULL, NULL, NULL),
(1002, 'Afficher le détails d''une commande', NULL, NULL, 2, 'N', 'Y', NULL, NULL, NULL, NULL),
(1003, 'Afficher la liste des réceptions', NULL, NULL, 2, 'N', 'Y', NULL, NULL, NULL, NULL),
(1004, 'Afficher le détails d''une récéption', NULL, NULL, 2, 'N', 'Y', NULL, NULL, NULL, NULL),
(1005, 'Afficher la liste des ventes ', NULL, NULL, 2, 'N', 'Y', NULL, NULL, NULL, NULL),
(1006, 'Afficher le détails d''une vente', NULL, NULL, 2, 'N', 'Y', NULL, NULL, NULL, NULL),
(1007, 'Afficher la liste des avoirs', NULL, NULL, 2, 'N', 'Y', NULL, NULL, NULL, NULL),
(1008, 'Afficher le détails d''une avoir', NULL, NULL, 2, 'N', 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `PM_PAGE_ATTRIBUTE`
--

CREATE TABLE IF NOT EXISTS `PM_PAGE_ATTRIBUTE` (
  `ID` bigint(20) NOT NULL,
  `PAGE_ID` bigint(20) DEFAULT NULL,
  `INF_ITEM_CODE` varchar(255) DEFAULT NULL,
  `PM_COMPONENT_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `IS_REQUIRED` char(1) DEFAULT 'N',
  `IS_READONLY` char(1) DEFAULT 'N',
  `IS_HIDDEN` char(1) DEFAULT 'N',
  `DATA_TYPE_ID` bigint(20) DEFAULT NULL,
  `FORMAT_TYPE_ID` bigint(20) DEFAULT NULL,
  `MAX_LENGHT` bigint(20) DEFAULT NULL,
  `MAX_WORD` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `DATE_UPDATE` varchar(45) DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `PM_PAGE_ATTRIBUTE`
--

INSERT INTO `PM_PAGE_ATTRIBUTE` (`ID`, `PAGE_ID`, `INF_ITEM_CODE`, `PM_COMPONENT_ID`, `SORT_KEY`, `ACTIVE`, `IS_REQUIRED`, `IS_READONLY`, `IS_HIDDEN`, `DATA_TYPE_ID`, `FORMAT_TYPE_ID`, `MAX_LENGHT`, `MAX_WORD`, `DATE_CREATION`, `DATE_UPDATE`, `USER_CREATION`, `USER_UPDATE`) VALUES
(141, 4, 'p004.s2.note', 1, 1, 'Y', 'Y', 'N', 'N', 1, 1, 20, 4, NULL, NULL, NULL, NULL),
(142, 4, 'p004.s2.category', 1, 2, 'Y', '', '', 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `PM_PAGE_ATTRIBUTE_MODEL`
--

CREATE TABLE IF NOT EXISTS `PM_PAGE_ATTRIBUTE_MODEL` (
  `ID` bigint(20) NOT NULL,
  `PAGE_ATTRIBUTE_ID` bigint(20) DEFAULT NULL,
  `VALUE` longtext,
  `MODEL_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `IS_REQUIRED` char(1) DEFAULT 'N',
  `IS_READONLY` char(1) DEFAULT 'N',
  `IS_HIDDEN` char(1) DEFAULT 'N',
  `DATA_TYPE_ID` bigint(20) DEFAULT NULL,
  `FORMAT_TYPE_ID` bigint(20) DEFAULT NULL,
  `MAX_LENGHT` bigint(20) DEFAULT NULL,
  `MAX_WORD` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `PM_PAGE_ATTRIBUTE_MODULE`
--

CREATE TABLE IF NOT EXISTS `PM_PAGE_ATTRIBUTE_MODULE` (
  `ID` bigint(20) NOT NULL,
  `PAGE_ATTRIBUTE_ID` bigint(20) DEFAULT NULL,
  `VALUE` longtext,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `IS_REQUIRED` char(1) DEFAULT 'N',
  `IS_READONLY` char(1) DEFAULT 'N',
  `IS_HIDDEN` char(1) DEFAULT 'N',
  `DATA_TYPE_ID` bigint(20) DEFAULT NULL,
  `FORMAT_TYPE_ID` bigint(20) DEFAULT NULL,
  `MAX_LENGHT` bigint(20) DEFAULT NULL,
  `MAX_WORD` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `PM_PAGE_PARAMETER`
--

CREATE TABLE IF NOT EXISTS `PM_PAGE_PARAMETER` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext CHARACTER SET latin1,
  `DESCRIPTION` text CHARACTER SET latin1,
  `DEFAULT_VALUE` text,
  `PAGE_ID` bigint(20) DEFAULT NULL,
  `PAGE_PARAMETER_TYPE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `PM_PAGE_PARAMETER`
--

INSERT INTO `PM_PAGE_PARAMETER` (`ID`, `NAME`, `DESCRIPTION`, `DEFAULT_VALUE`, `PAGE_ID`, `PAGE_PARAMETER_TYPE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'titre de l''application', 'title of application', 'My Stock Management', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'visiblité de form add edit ', NULL, 'no', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(4, '1', '2', '3', 20, 1, 'Y', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `PM_PAGE_PARAMETER_MODEL`
--

CREATE TABLE IF NOT EXISTS `PM_PAGE_PARAMETER_MODEL` (
  `ID` bigint(20) NOT NULL,
  `PAGE_PARAMETER_ID` bigint(20) DEFAULT NULL,
  `VALUE` longtext,
  `MODEL_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `PM_PAGE_PARAMETER_MODULE`
--

CREATE TABLE IF NOT EXISTS `PM_PAGE_PARAMETER_MODULE` (
  `ID` bigint(20) NOT NULL,
  `PAGE_PARAMETER_ID` bigint(20) DEFAULT NULL,
  `VALUE` longtext,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `PM_PAGE_PARAMETER_TYPE`
--

CREATE TABLE IF NOT EXISTS `PM_PAGE_PARAMETER_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `PM_PAGE_PARAMETER_TYPE`
--

INSERT INTO `PM_PAGE_PARAMETER_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'pm_page_parameter_type', 'pm_page_parameter_type', NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `PM_PAGE_TYPE`
--

CREATE TABLE IF NOT EXISTS `PM_PAGE_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `PM_PAGE_TYPE`
--

INSERT INTO `PM_PAGE_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Pages', '0   --> 999', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Pdf', '1001 --> 1999', 2, 'Y', NULL, NULL, NULL, NULL),
(3, 'Widget', '2001 --> 2999', 3, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `PM_VALIDATION_TYPE`
--

CREATE TABLE IF NOT EXISTS `PM_VALIDATION_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `PARAM_NUMBER` bigint(20) DEFAULT NULL,
  `ERROR_MESSAGE` tinytext,
  `HELP` text CHARACTER SET latin1,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `PM_VALIDATION_TYPE`
--

INSERT INTO `PM_VALIDATION_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `PARAM_NUMBER`, `ERROR_MESSAGE`, `HELP`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'required', 'required', 0, 'validation.v1.required', 'validation.v1.required', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'maxlenght', 'maxlenght', 1, 'validation.v1.maxlenght', 'validation.v1.maxlenght', 2, 'Y', NULL, NULL, NULL, NULL),
(3, 'maxWord', 'maxWord', 1, 'validation.v1.maxWord', 'validation.v1.maxWord', 3, 'Y', NULL, NULL, NULL, NULL),
(4, 'dataType', 'dataType : ne sera pas changé pas un locataire', 1, 'validation.v1.dataType', 'validation.v1.dataType', 4, 'Y', NULL, NULL, NULL, NULL),
(5, 'formatType', 'formatType', 1, 'validation.v1.formatType', 'validation.v1.formatType', 5, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_ADVANCED`
--

CREATE TABLE IF NOT EXISTS `SM_ADVANCED` (
  `ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `CUSTOMER_ID` bigint(20) DEFAULT NULL,
  `ADVANCED_STATUS_ID` bigint(20) DEFAULT NULL,
  `AMOUNT` double DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `SM_ADVANCED_STATUS`
--

CREATE TABLE IF NOT EXISTS `SM_ADVANCED_STATUS` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_ADVANCED_STATUS`
--

INSERT INTO `SM_ADVANCED_STATUS` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'En cours de saisie', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'Transmis', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Validé', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'Refuser', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(5, 'Rejeter', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_BANK_TYPE`
--

CREATE TABLE IF NOT EXISTS `SM_BANK_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_BANK_TYPE`
--

INSERT INTO `SM_BANK_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'BANK Default Value', 'BANK Description Default Value', NULL, 'Y', 11, '2016-05-03 23:55:20', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_CHECK`
--

CREATE TABLE IF NOT EXISTS `SM_CHECK` (
  `ID` bigint(20) NOT NULL,
  `BANK_ID` bigint(20) DEFAULT NULL,
  `NAME_PERSON` tinytext CHARACTER SET latin1,
  `NUMBER` tinytext CHARACTER SET latin1,
  `ACCOUNT` tinytext CHARACTER SET latin1,
  `PHONE` tinytext CHARACTER SET latin1,
  `PAYABLE_IN` tinytext CHARACTER SET latin1,
  `AMOUNT` double DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `SM_CUSTOMER`
--

CREATE TABLE IF NOT EXISTS `SM_CUSTOMER` (
  `ID` bigint(20) NOT NULL,
  `FIRST_NAME` tinytext CHARACTER SET latin1,
  `LAST_NAME` tinytext CHARACTER SET latin1,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `CUSTOMER_TYPE_ID` bigint(20) DEFAULT NULL,
  `CUSTOMER_CATEGORY_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `SHORT_LABEL` tinytext,
  `FULL_LABEL` text,
  `NOTE` text,
  `COMPANY_NAME` tinytext,
  `REPRESENTATIVE` tinytext,
  `IDENTIFICATION` tinytext,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `CUSTOMER_NATURE_ID` bigint(20) DEFAULT '1',
  `ENTITY_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_CUSTOMER`
--

INSERT INTO `SM_CUSTOMER` (`ID`, `FIRST_NAME`, `LAST_NAME`, `CLT_MODULE_ID`, `CUSTOMER_TYPE_ID`, `CUSTOMER_CATEGORY_ID`, `SORT_KEY`, `ACTIVE`, `SHORT_LABEL`, `FULL_LABEL`, `NOTE`, `COMPANY_NAME`, `REPRESENTATIVE`, `IDENTIFICATION`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `CUSTOMER_NATURE_ID`, `ENTITY_ID`) VALUES
(4, 'AHmed', 'ML', 1, 1, 1, 1, 'Y', ' Libellé court ', ' Libellé complète ', 'aaa', ' Nom de l''entreprise ', ' Représentant ', NULL, NULL, 1, '2016-08-02 21:59:54', 1, 1, 1),
(5, 'Abdessamad2', 'HALLAL2', 1, 1, NULL, NULL, 'Y', '', '', '', '', '', NULL, NULL, 1, '2016-08-02 22:00:01', 1, 2, 1),
(6, 'Abdessamad3', 'HALLAL3', 1, 1, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 1, 1),
(10, 'Abdessamad', 'HALLAL', 1, 1, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1),
(11, ' Prénom ', 'Nom ', 1, 1, 2, NULL, 'Y', ' Libelle court ', ' Libelle complet ', ' Note interne ', NULL, NULL, NULL, NULL, 2, NULL, 2, 1, 1),
(17, 'dsdqs', '', 1, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1),
(18, 'qsdqsdsqdqsdsq111', '', 1, NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, '2016-04-16 00:15:55', 1, NULL, NULL, 1, 1),
(19, 'sds', 'qdqsd', 1, 1, 2, NULL, 'Y', 'qsdsq', 'dsqdqs', 'qsdqs', 'dqs', 'dsqd', NULL, '2016-04-20 23:38:05', 1, NULL, NULL, 2, 1),
(20, 'dsdsq', 'dqsdqs', 1, 1, 2, NULL, 'Y', '', 'dqsdqs', 'dsqdqs', '', '', NULL, '2016-04-20 23:42:15', 1, NULL, NULL, 2, 1),
(21, 'prénom', 'nom', 1, 1, 1, NULL, 'Y', '', 'liv', '', '', '', NULL, '2016-04-20 23:43:54', 1, NULL, NULL, 2, 1),
(22, '', '', 1, NULL, NULL, NULL, '', '', '', '', '', '', NULL, '2016-04-21 00:22:29', 1, NULL, NULL, 1, 1),
(24, '', '', 11, 3, 3, NULL, 'Y', '', '', '', 'test', 'aaa', NULL, '2016-05-18 22:49:56', 11, NULL, NULL, 1, 58);

-- --------------------------------------------------------

--
-- Structure de la table `SM_CUSTOMER_CATEGORY`
--

CREATE TABLE IF NOT EXISTS `SM_CUSTOMER_CATEGORY` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_CUSTOMER_CATEGORY`
--

INSERT INTO `SM_CUSTOMER_CATEGORY` (`ID`, `NAME`, `DESCRIPTION`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Category 1', NULL, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'Category 2', NULL, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Client catégorie Default Value', 'Client catégorie  Description Default Value', 11, NULL, 'Y', '2016-05-03 23:55:20', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_CUSTOMER_TYPE`
--

CREATE TABLE IF NOT EXISTS `SM_CUSTOMER_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_CUSTOMER_TYPE`
--

INSERT INTO `SM_CUSTOMER_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Type 1', 'Type 1', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Type 2', 'Type 2', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(3, 'Client Type Default Value', 'Client Type Description Default Value', 11, NULL, 'Y', '2016-05-03 23:55:20', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_DEPOSIT`
--

CREATE TABLE IF NOT EXISTS `SM_DEPOSIT` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_DEPOSIT`
--

INSERT INTO `SM_DEPOSIT` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'depôt 1', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'depôt', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(3, 'Dépôt Default Value', 'Dépôt Description Default Value', NULL, 'Y', 11, '2016-05-04 00:01:33', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_EXPENSE`
--

CREATE TABLE IF NOT EXISTS `SM_EXPENSE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `AMOUNT` double DEFAULT NULL,
  `EXPENSE_TYPE_ID` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_EXPENSE`
--

INSERT INTO `SM_EXPENSE` (`ID`, `NAME`, `DESCRIPTION`, `AMOUNT`, `EXPENSE_TYPE_ID`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(23, 'Libelle1', 'Description1', 11, 1, 1, NULL, 'Y', NULL, 1, NULL, NULL),
(29, 'le petit déjounée ', 'avec ... ', 50, 1, 1, NULL, 'Y', NULL, 1, NULL, 1),
(31, 'aaa', 'bb', 3344, 1, 1, NULL, 'Y', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_EXPENSE_TYPE`
--

CREATE TABLE IF NOT EXISTS `SM_EXPENSE_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_EXPENSE_TYPE`
--

INSERT INTO `SM_EXPENSE_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Générale', 'Générale', 1, 3, 'Y', NULL, NULL, NULL, NULL),
(62, 'Femme de menage', 'Femme de menage', 2, 3, 'Y', NULL, NULL, NULL, NULL),
(63, 'Transport', 'Transport', 3, 3, 'Y', NULL, NULL, NULL, NULL),
(64, 'Eléctrisité', 'Eléctrisité', 4, 3, 'Y', NULL, NULL, NULL, NULL),
(65, 'Restaurant', 'Restaurant', 5, 3, 'Y', NULL, NULL, NULL, NULL),
(66, 'Expense Type Default Value', 'Expense Type  Description Default Value', NULL, 11, 'Y', '2016-05-03 23:55:20', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_JUSTIFICATION_STATUS`
--

CREATE TABLE IF NOT EXISTS `SM_JUSTIFICATION_STATUS` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `JUSTIFICATION_STATUS_CATEGORY_ID` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `SM_JUSTIFICATION_STATUS_CATEGORY`
--

CREATE TABLE IF NOT EXISTS `SM_JUSTIFICATION_STATUS_CATEGORY` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_JUSTIFICATION_STATUS_CATEGORY`
--

INSERT INTO `SM_JUSTIFICATION_STATUS_CATEGORY` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'sm_order_supplier', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'sm_reception', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'sm_order', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'sm_return_receipt', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_ORDER`
--

CREATE TABLE IF NOT EXISTS `SM_ORDER` (
  `ID` bigint(20) NOT NULL,
  `NO_SEQ` bigint(20) DEFAULT NULL,
  `REFERENCE` tinytext,
  `CUSTOMER_ID` bigint(20) DEFAULT NULL,
  `PAYMENT_METHOD_ID` bigint(20) DEFAULT NULL,
  `CHECK_ID` bigint(20) DEFAULT NULL,
  `DELIVERY` tinytext,
  `NOTE` text,
  `ORDER_STATUS_ID` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_ORDER`
--

INSERT INTO `SM_ORDER` (`ID`, `NO_SEQ`, `REFERENCE`, `CUSTOMER_ID`, `PAYMENT_METHOD_ID`, `CHECK_ID`, `DELIVERY`, `NOTE`, `ORDER_STATUS_ID`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(44, 1, 'V1', 4, NULL, NULL, NULL, 'eeee', 3, 11, NULL, '2015-12-24 22:28:45', NULL, '2016-06-02 11:45:42', NULL),
(45, 2, 'V2', NULL, NULL, NULL, NULL, 'ddd', 3, 11, NULL, '2015-12-24 22:30:27', NULL, '2016-06-01 11:49:19', 11),
(46, 3, 'V3', 6, NULL, NULL, NULL, 'dsqdsq', 5, 11, NULL, '2015-12-24 23:09:58', NULL, '2016-06-04 11:45:42', NULL),
(47, 4, 'V4', 10, NULL, NULL, NULL, 'note1', 3, 11, NULL, '2015-12-24 23:37:30', NULL, '2016-06-05 11:45:42', NULL),
(48, 5, 'V5', 10, NULL, NULL, NULL, 'AEE', 4, 11, NULL, '2015-12-24 23:41:49', NULL, '2016-06-06 11:45:42', NULL),
(49, 6, 'V6', NULL, NULL, NULL, NULL, '', 3, 11, NULL, '2015-12-24 23:49:17', NULL, '2016-06-01 11:49:17', 11),
(50, 7, 'V7', NULL, NULL, NULL, NULL, 'gggg', 3, 11, NULL, '2015-12-24 23:52:11', NULL, '2016-06-01 11:49:21', 11),
(51, 8, 'V8', NULL, NULL, NULL, NULL, '', 4, 11, NULL, '2015-12-24 23:52:57', NULL, '2016-06-15 11:45:42', NULL),
(52, 9, 'V9', NULL, NULL, NULL, NULL, 'abc111', 3, 11, NULL, '2015-12-24 23:54:06', NULL, '2016-06-01 11:49:15', 11),
(53, 10, 'V10', 5, NULL, NULL, NULL, 'centre', 5, 11, NULL, '2015-12-24 23:59:09', NULL, '2016-06-01 11:45:42', NULL),
(54, 11, 'V11', 6, NULL, NULL, NULL, 'centre d''appelle', 3, 11, NULL, '2015-12-27 18:55:51', NULL, '2016-06-25 11:45:42', NULL),
(55, 1, 'K5VV4D', 24, NULL, NULL, NULL, '', 3, 11, NULL, '2016-05-30 22:18:15', 11, '0000-00-00 00:00:00', 11),
(56, 2, 'V2', 24, NULL, NULL, NULL, '', 3, 11, NULL, '2016-06-01 11:45:23', 11, '2016-06-01 11:45:42', 11),
(57, 1, 'V1', 11, NULL, NULL, NULL, 'aaaa', 1, 1, NULL, '2016-07-13 00:44:53', 1, '2016-07-13 00:45:18', 1),
(58, 2, 'V2', NULL, NULL, NULL, NULL, '', 1, 1, NULL, '2016-12-09 21:32:08', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_ORDER_LINE`
--

CREATE TABLE IF NOT EXISTS `SM_ORDER_LINE` (
  `ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `DESIGNATION` text,
  `ORDER_ID` bigint(20) DEFAULT NULL,
  `PROMOTION_ID` bigint(20) DEFAULT NULL,
  `NEGOTIATE_PRICE_SALE` double DEFAULT NULL,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_ORDER_LINE`
--

INSERT INTO `SM_ORDER_LINE` (`ID`, `PRODUCT_ID`, `DESIGNATION`, `ORDER_ID`, `PROMOTION_ID`, `NEGOTIATE_PRICE_SALE`, `QUANTITY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(4, NULL, 'Pc portable1', 44, NULL, 100, 1, NULL, NULL, NULL, NULL, NULL),
(5, 23, 'Note Portable', 44, NULL, 500, 1, NULL, NULL, NULL, NULL, NULL),
(6, 23, 'Sumsung duos', 45, NULL, 300, 1, NULL, NULL, NULL, NULL, NULL),
(7, 2, 'Pc portable1', 46, NULL, 100, 1, NULL, NULL, NULL, NULL, NULL),
(8, NULL, 'Nokia', 47, NULL, 200, 1, NULL, NULL, NULL, NULL, NULL),
(10, NULL, 'Sumsung duos', 48, NULL, 300, 1, NULL, NULL, NULL, NULL, NULL),
(11, NULL, 'Nokia', 49, NULL, 200, 1, NULL, NULL, NULL, NULL, NULL),
(13, 2, 'Pc portable1', 50, NULL, 100, 1, NULL, NULL, NULL, NULL, NULL),
(14, 18, 'Mac OS', 51, NULL, 300, 1, NULL, NULL, NULL, NULL, NULL),
(15, 18, 'Mac OS', 52, NULL, 300, 1, NULL, NULL, NULL, NULL, NULL),
(16, 2, 'Pc portable1', 53, NULL, 100, 1, NULL, NULL, NULL, NULL, NULL),
(17, 18, 'Mac OS', 53, NULL, 300, 1, NULL, NULL, NULL, NULL, NULL),
(18, 3, 'TEST 11111', 53, NULL, 4567, 222, NULL, NULL, NULL, NULL, NULL),
(19, 4, 'Sumsung duos', 53, NULL, 300, 22, NULL, NULL, NULL, NULL, NULL),
(20, 4, 'Note Portable', 53, NULL, 111.33, 222, NULL, NULL, NULL, NULL, NULL),
(21, 2, 'Pc portable1', 53, NULL, 100, 4567, NULL, NULL, NULL, NULL, NULL),
(22, 2, 'Pc portable1', 52, NULL, 100, 1, NULL, NULL, NULL, NULL, NULL),
(23, 18, 'Mac OS', 47, NULL, 300, 1, NULL, NULL, NULL, NULL, NULL),
(24, 23, 'Note Portable', 48, NULL, 500, 1, NULL, NULL, NULL, NULL, NULL),
(25, 4, 'Sumsung duos', 46, NULL, 300, 1, NULL, NULL, NULL, NULL, NULL),
(26, 2, 'Pc portable1', 46, NULL, 100, 1, NULL, NULL, NULL, NULL, NULL),
(27, 2, 'Pc portable1', 54, NULL, 100, 9, NULL, NULL, NULL, NULL, NULL),
(28, 3, 'Nokia', 54, NULL, 200, 1, NULL, NULL, NULL, NULL, NULL),
(29, 3, 'Nokia', 51, NULL, 200, 1, NULL, NULL, NULL, NULL, NULL),
(30, 41, 'note', 55, NULL, 1500, 1, NULL, '2016-05-30 22:18:23', 11, NULL, NULL),
(31, 41, 'note', 56, NULL, 1500, 20, NULL, '2016-06-01 11:45:32', 11, NULL, NULL),
(32, 18, 'Mac OS', 57, NULL, 300, 2, NULL, '2016-07-13 00:45:18', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_ORDER_STATUS`
--

CREATE TABLE IF NOT EXISTS `SM_ORDER_STATUS` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_ORDER_STATUS`
--

INSERT INTO `SM_ORDER_STATUS` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'En cours de saisie', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'Transmis', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Validé', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'Refuser', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(5, 'Rejeter', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_ORDER_STATUS_LOG`
--

CREATE TABLE IF NOT EXISTS `SM_ORDER_STATUS_LOG` (
  `ID` bigint(20) NOT NULL,
  `ORDER_ID` bigint(20) DEFAULT NULL,
  `ORDER_STATUS_ID` bigint(20) DEFAULT NULL,
  `JUSTIFICATION_STATUS_ID` bigint(20) DEFAULT NULL,
  `REASON_TITLE` tinytext,
  `REASON_CONTENT` text,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `SM_ORDER_SUPPLIER`
--

CREATE TABLE IF NOT EXISTS `SM_ORDER_SUPPLIER` (
  `ID` bigint(20) NOT NULL,
  `NO_SEQ` bigint(20) DEFAULT NULL,
  `REFERENCE` tinytext,
  `NOTE` text,
  `ORDER_SUPPLIER_STATUS_ID` bigint(20) DEFAULT NULL,
  `SUPPLIER_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_ORDER_SUPPLIER`
--

INSERT INTO `SM_ORDER_SUPPLIER` (`ID`, `NO_SEQ`, `REFERENCE`, `NOTE`, `ORDER_SUPPLIER_STATUS_ID`, `SUPPLIER_ID`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(6, 3, '111', '222', 3, 16, NULL, 1, '2015-11-22 15:46:52', NULL, NULL, NULL),
(7, 4, 'kaka', 'baba', 3, 1, NULL, 1, '2015-11-22 15:49:07', NULL, NULL, NULL),
(9, 6, 'R0012', 'aaaaa', 3, 1, NULL, 1, '2015-11-28 14:47:48', NULL, NULL, NULL),
(12, 7, 'aa', '', 1, 15, NULL, 1, '2015-11-29 00:06:57', NULL, NULL, NULL),
(19, 8, 'C8', 'dddd', 1, 1, NULL, 1, '2015-11-29 00:26:10', NULL, NULL, NULL),
(20, 9, 'C9', '', 1, NULL, NULL, 1, '2015-11-29 00:27:32', NULL, NULL, NULL),
(23, 10, 'C10', 'ddsqdsq', 3, 1, NULL, 1, '2015-11-29 13:45:02', NULL, NULL, NULL),
(24, 11, 'TEST', 'onte', 3, 1, NULL, 1, '2015-11-29 14:50:22', NULL, NULL, NULL),
(26, 13, 'C13', '', 3, 15, NULL, 1, '2015-12-27 18:56:24', NULL, NULL, NULL),
(27, 14, 'C14', 'note 1', 1, 1, NULL, 1, '2016-03-08 19:10:14', NULL, NULL, NULL),
(28, 15, 'C15', '', 1, NULL, NULL, 1, '2016-03-09 21:46:28', NULL, NULL, NULL),
(29, 16, 'VJ26BT', '', 1, 15, NULL, 1, '2016-04-15 23:23:30', NULL, NULL, NULL),
(30, 17, 'C17', '', 1, 15, NULL, 1, '2016-04-15 23:50:31', NULL, NULL, NULL),
(31, 18, 'C18', '', 1, NULL, NULL, 1, '2016-04-30 12:46:16', NULL, NULL, NULL),
(32, 19, 'C19', '', 1, NULL, NULL, 1, '2016-04-30 12:49:41', NULL, NULL, NULL),
(33, 20, 'sdqf', NULL, 3, 15, 'Y', 1, '2016-04-30 13:01:04', NULL, NULL, NULL),
(34, 21, 'C21', '', 3, 15, NULL, 1, '2016-05-15 23:59:08', NULL, NULL, NULL),
(35, 1, '6QJGXL', 'note', 3, 25, NULL, 11, '2016-05-18 22:36:09', NULL, NULL, NULL),
(36, 2, 'HA4ZG8', '', 3, 25, NULL, 11, '2016-05-26 23:55:37', 11, '2016-05-26 23:57:00', 11),
(37, 22, 'KHZA76', 'aaa', 1, 15, NULL, 1, '2016-07-16 17:00:18', 1, NULL, NULL),
(38, 23, 'C23', 'Note', 3, 16, NULL, 1, '2016-07-29 19:40:01', 1, '2016-07-29 19:43:15', 1),
(39, 24, 'L3P78N', 'note interne', 1, 15, NULL, 1, '2016-09-25 13:29:40', 1, '2016-09-25 13:31:04', 1),
(40, 25, 'aa', '', 1, NULL, NULL, 1, '2016-12-09 21:09:48', 1, '2016-12-09 21:09:56', 1);

-- --------------------------------------------------------

--
-- Structure de la table `SM_ORDER_SUPPLIER_LINE`
--

CREATE TABLE IF NOT EXISTS `SM_ORDER_SUPPLIER_LINE` (
  `ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `DESIGNATION` text,
  `UNIT_PRICE_SALE` double DEFAULT NULL,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `TVA` double DEFAULT NULL,
  `REMISE` double DEFAULT NULL,
  `PROMOTION_ID` bigint(20) DEFAULT NULL,
  `ORDER_SUPPLIER_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_ORDER_SUPPLIER_LINE`
--

INSERT INTO `SM_ORDER_SUPPLIER_LINE` (`ID`, `PRODUCT_ID`, `DESIGNATION`, `UNIT_PRICE_SALE`, `QUANTITY`, `TVA`, `REMISE`, `PROMOTION_ID`, `ORDER_SUPPLIER_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(13, 18, 'Mac OS', 300, 32, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL),
(14, 3, 'Sumsung duos1199', 300, 32, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL),
(15, 3, 'Note Portable111', 900, 32, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL),
(19, 3, 'Nokia', 200, 32, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL),
(20, 3, 'Nokia1', 200, 112, NULL, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL),
(31, 3, 'Nokia', 200, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 19, 'HP Portable', 400, 1, NULL, NULL, NULL, 19, NULL, NULL, NULL, NULL, NULL),
(38, 3, 'Note Portable', 500, 60, NULL, NULL, NULL, 23, NULL, NULL, NULL, NULL, NULL),
(39, 2, 'Pc portable1', 100, 9, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL),
(41, 2, 'Pc portable1', 100, 7, NULL, NULL, NULL, 26, NULL, NULL, NULL, NULL, NULL),
(42, 3, 'Nokia', 200, 180, NULL, NULL, NULL, 26, NULL, NULL, NULL, NULL, NULL),
(43, 2, 'Pc portable1', 100, 3, NULL, NULL, NULL, 27, NULL, NULL, NULL, NULL, NULL),
(44, 2, 'Sumsung duos', 300, 4, NULL, NULL, NULL, 27, NULL, NULL, NULL, NULL, NULL),
(45, 23, 'du Text', 500, 1, NULL, NULL, NULL, 27, NULL, NULL, NULL, NULL, NULL),
(46, NULL, 'Sumsung duos', 300, 1, NULL, NULL, NULL, 28, NULL, NULL, NULL, NULL, NULL),
(47, 19, 'HP Portable', 400, 1, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL),
(48, 19, 'HP Portable', 400, 1, NULL, NULL, NULL, 29, NULL, NULL, NULL, NULL, NULL),
(49, NULL, 'produitismail', 200, 5, NULL, NULL, NULL, 31, NULL, NULL, NULL, NULL, NULL),
(50, 35, 'produitismail', 200, 1, NULL, NULL, NULL, 32, NULL, NULL, NULL, NULL, NULL),
(51, 35, 'produitismail', 300, 5, NULL, NULL, NULL, 33, 'Y', NULL, NULL, NULL, NULL),
(52, 4, 'Sumsung duos', 300, 1, NULL, NULL, NULL, 34, NULL, NULL, NULL, NULL, NULL),
(53, 4, 'Sumsung duos', 300, 1, NULL, NULL, NULL, 34, NULL, NULL, NULL, NULL, NULL),
(55, 4, 'Sumsung duos', 300, 1, NULL, NULL, NULL, 34, NULL, NULL, NULL, NULL, NULL),
(56, 41, 'note', 1500, 10, NULL, NULL, NULL, 35, NULL, NULL, NULL, NULL, NULL),
(57, 40, 'npte11', 0, 1, NULL, NULL, NULL, 35, NULL, NULL, NULL, NULL, NULL),
(58, 40, 'desc1', 1500, 99, NULL, NULL, NULL, 35, NULL, NULL, NULL, NULL, NULL),
(59, 40, 'desc', 0, 1, NULL, NULL, NULL, 36, NULL, NULL, NULL, NULL, NULL),
(60, 2, 'Pc portable1', 9.01037, 12, NULL, NULL, NULL, 37, NULL, NULL, NULL, NULL, NULL),
(61, 42, 'Sumsung Galaxy S7', 6000, 100, NULL, NULL, NULL, 38, NULL, NULL, NULL, NULL, NULL),
(62, NULL, 'Toner HP Lazer Jet C533A', 250, 1, NULL, NULL, NULL, 39, NULL, NULL, NULL, NULL, NULL),
(63, NULL, 'Toner HP Lazer Jet C533A', 250, 1, NULL, NULL, NULL, 39, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_ORDER_SUPPLIER_STATUS`
--

CREATE TABLE IF NOT EXISTS `SM_ORDER_SUPPLIER_STATUS` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_ORDER_SUPPLIER_STATUS`
--

INSERT INTO `SM_ORDER_SUPPLIER_STATUS` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'En cours', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'Submit', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Valider', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'Refuser', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(5, 'Rejeter', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_ORDER_SUPPLIER_STATUS_LOG`
--

CREATE TABLE IF NOT EXISTS `SM_ORDER_SUPPLIER_STATUS_LOG` (
  `ID` bigint(20) NOT NULL,
  `ORDER_SUPPLIER_ID` bigint(20) DEFAULT NULL,
  `ORDER_SUPPLIER_STATUS_ID` bigint(20) DEFAULT NULL,
  `JUSTIFICATION_STATUS_ID` bigint(20) DEFAULT NULL,
  `REASON_TITLE` tinytext,
  `REASON_CONTENT` text,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `SM_PAYMENT_METHOD`
--

CREATE TABLE IF NOT EXISTS `SM_PAYMENT_METHOD` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_PAYMENT_METHOD`
--

INSERT INTO `SM_PAYMENT_METHOD` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Chèque', NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'Prélèvement', NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Espèces', NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'Carte bancaire', NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(5, 'Traite', NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(6, 'Autre', NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_PRODUCT`
--

CREATE TABLE IF NOT EXISTS `SM_PRODUCT` (
  `ID` bigint(20) NOT NULL,
  `NO_SEQ` bigint(20) DEFAULT NULL,
  `REFERENCE` tinytext CHARACTER SET latin1,
  `DESIGNATION` tinytext CHARACTER SET latin1,
  `QUANTITY` bigint(20) DEFAULT '0',
  `PRICE_SALE` double DEFAULT '0',
  `PRICE_BUY` double DEFAULT '0',
  `PRODUCT_GROUP_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_FAMILY_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_SIZE_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_COLOR_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_STATUS_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_TYPE_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_DEPARTMENT_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_UNIT_ID` bigint(20) DEFAULT NULL,
  `THRESHOLD` bigint(20) DEFAULT '0',
  `NOTE` text,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=328 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_PRODUCT`
--

INSERT INTO `SM_PRODUCT` (`ID`, `NO_SEQ`, `REFERENCE`, `DESIGNATION`, `QUANTITY`, `PRICE_SALE`, `PRICE_BUY`, `PRODUCT_GROUP_ID`, `PRODUCT_FAMILY_ID`, `PRODUCT_SIZE_ID`, `PRODUCT_COLOR_ID`, `PRODUCT_STATUS_ID`, `PRODUCT_TYPE_ID`, `PRODUCT_DEPARTMENT_ID`, `PRODUCT_UNIT_ID`, `THRESHOLD`, `NOTE`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(2, 0, 'P0', 'Pc portable1', 221, 100, 111, 3, 4, 47, 1, 1, 1, NULL, NULL, 9000, 'Note interne', 'N', 11, NULL, 2, NULL, 2),
(3, 0, 'P0', 'Nokia', 200, 200, 222, 2, 3, 50, 2, 1, 2, NULL, NULL, 9000, 'Note interne', 'Y', 11, NULL, 2, NULL, NULL),
(4, 0, 'P0', 'Sumsung duos', -2, 300, 333, 1, 2, 47, 8, 1, 2, NULL, NULL, 9000, 'Intene', 'Y', 11, NULL, 2, NULL, 1),
(18, 2, 'P2', 'Mac OS', -1, 300, 444, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9000, '', '', 11, '2015-11-13 23:17:10', 1, NULL, NULL),
(19, 3, 'P3', 'HP Portable', 12, 400, 555, 2, 3, NULL, NULL, NULL, 1, NULL, NULL, 90000, '', 'Y', 11, '2015-11-14 13:00:09', 1, NULL, 1),
(23, 7, 'P7', 'Note Portable', -2, 500, 666, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9000, '', '', 11, '2015-11-14 13:43:19', 1, NULL, NULL),
(24, 1, 'P1', NULL, 200, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9000, NULL, 'Y', 2, '2016-04-07 22:54:55', NULL, NULL, NULL),
(25, 1, 'P1', NULL, 200, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 96000, NULL, 'Y', 2, '2016-04-07 22:54:55', NULL, NULL, NULL),
(26, 1, 'P1', NULL, 200, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9000, NULL, 'Y', 2, '2016-04-07 22:54:55', NULL, NULL, NULL),
(27, 1, 'P1', NULL, 200, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9000, NULL, 'Y', 3, '2016-04-07 22:54:55', NULL, NULL, NULL),
(28, 8, 'P8', 'aaaa', 0, 111, 222, 2, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, '', 'Y', 11, '2016-04-15 22:29:27', 1, NULL, NULL),
(30, 10, 'P10', 'autooo', 0, 12.15, 12.11, 1, 1, 47, 11, NULL, 1, NULL, NULL, 900, 'note1', 'Y', 11, '2016-04-17 13:21:46', 1, NULL, NULL),
(31, 11, 'P11', 'aa', 0, 11, 22, 1, 1, 47, 1, NULL, 2, NULL, NULL, 11, 'ccc', 'Y', 11, '2016-04-17 13:47:48', 1, NULL, NULL),
(33, 13, 'P13', '', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'Y', 11, '2016-04-22 01:24:00', 1, NULL, NULL),
(34, 14, 'P14', ' Désignation ', 0, 11, 22, 3, 4, 48, 9, NULL, 3, NULL, NULL, 23, 'note', 'Y', 11, '2016-04-22 01:25:09', 1, NULL, 1),
(35, 15, 'P15', 'produitismail', 0, 200, 200, 1, 1, 49, 4, NULL, 2, NULL, NULL, 10, 'sqdf', 'Y', 11, '2016-04-30 12:42:58', 1, NULL, NULL),
(36, 16, 'P16', 'desc1', 0, 1900, 2999, 1, 1, 47, 13, NULL, 1, NULL, NULL, 15, 'note', 'Y', 11, '2016-05-15 23:57:16', 1, NULL, 1),
(37, 17, 'P17', '????????', 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 20, '', 'Y', 11, '2016-05-16 22:04:50', NULL, '2016-07-29 19:25:45', 1),
(40, 3, 'P3', '', 1, 0, 0, 4, NULL, 62, 14, NULL, 4, NULL, NULL, 19, '', 'Y', 11, '2016-05-16 23:35:34', 11, '2016-05-18 22:36:52', 11),
(41, 4, 'P4', 'note', -11, 1500, 1800, 4, NULL, 62, 14, NULL, 4, NULL, NULL, 15, '', 'Y', 11, '2016-05-18 22:35:47', 11, NULL, NULL),
(42, 18, 'P18', 'Sumsung Galaxy S7', 47, 6000, 550, 1, 2, 52, 7, NULL, 3, NULL, NULL, 5, 'Note ceci est une note', 'Y', 11, '2016-07-29 19:38:58', 1, NULL, NULL),
(44, 19, 'P19', 'Toner HP Lazer Jet 5A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(45, 20, 'P20', 'Toner HP Lazer Jet 12A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(46, 21, 'P21', 'Toner HP Lazer Jet 15A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(47, 22, 'P22', 'Toner HP Lazer Jet 35A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(48, 23, 'P23', 'Toner HP Lazer Jet 36A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(49, 24, 'P24', 'Toner HP Lazer Jet 38A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(50, 25, 'P25', 'Toner HP Lazer Jet 42A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(51, 26, 'P26', 'Toner HP Lazer Jet 53A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(52, 27, 'P27', 'Toner HP Lazer Jet 85A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(53, 28, 'P28', 'Toner HP Lazer Jet 90A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(54, 29, 'P29', 'Toner HP Lazer Jet C530A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(55, 30, 'P30', 'Toner HP Lazer Jet C531A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(56, 31, 'P31', 'Toner HP Lazer Jet C532A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(57, 32, 'P32', 'Toner HP Lazer Jet C533A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(58, 33, 'P33', 'Toner HP Lazer Jet C540A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(59, 34, 'P34', 'Toner HP Lazer Jet C541A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(60, 35, 'P35', 'Toner HP Lazer Jet C542A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(61, 36, 'P36', 'Toner HP Lazer Jet C543A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(62, 37, 'P37', 'Toner HP Lazer Jet C645K', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(63, 38, 'P38', 'Toner HP Lazer Jet C645C', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(64, 39, 'P39', 'Toner HP Lazer Jet C645M', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(65, 40, 'P40', 'Toner HP Lazer Jet C645Y', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(66, 41, 'P41', 'CDR', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(67, 42, 'P42', 'DVDR', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(68, 43, 'P43', 'Clé USB 8G', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(69, 44, 'P44', 'Clé USB 16G', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(70, 45, 'P45', 'Clé USB 32G', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(71, 46, 'P46', 'Clé USB 64G', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(72, 47, 'P47', 'Toner LEXMARK C734K', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(73, 48, 'P48', 'Toner LEXMARK C734C', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(74, 49, 'P49', 'Toner LEXMARK C734M', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(75, 50, 'P50', 'Toner LEXMARK C734Y', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(76, 51, 'P51', 'Toner LEXMARK 540K', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(77, 52, 'P52', 'Toner LEXMARK 540C', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(78, 53, 'P53', 'Toner LEXMARK 540M', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(79, 54, 'P54', 'Toner LEXMARK 540Y', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(80, 55, 'P55', 'Photo conducteur X734', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(81, 56, 'P56', 'Rame Papier photocopieur 21X29,7 80grs Unt', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(82, 57, 'P57', 'Parapheur 18 volets', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(83, 58, 'P58', 'pochette perforée A4 - Paquet de 100 pochettes', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(84, 59, 'P59', 'Rame chemise bulle blanche paquet de 250 chemises 100 grs', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(85, 60, 'P60', 'Rame chemise cartonnée couleur bleue Extra de 100 Unt 180 grs ', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(86, 61, 'P61', 'Rame chemise bulle couleur jaune double - paquet de 250 chemises 100 grs ', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(87, 62, 'P62', 'Couverture transparente A4 0,15mm X 210mm X 29,7mm', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(88, 63, 'P63', 'Rame chemise à sangle D.T de 10 Unt', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(89, 64, 'P64', 'Chemise à coin  4100 paquets de 100 pc', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(90, 65, 'P65', 'Encre Toner noir pour fax Brother 2845 TN2220', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(91, 66, 'P66', 'Encre Toner noir pour fax Brother 2845 TN2000', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(92, 67, 'P67', 'Stylo feutre G3 noir paquet de 12 pc ', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(93, 68, 'P68', 'Stylo feutre G3 bleu paquet de 12 pc ', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(94, 69, 'P69', 'Agrafeuse 24/6 et 26/6', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(95, 70, 'P70', 'Blanco Stylo paquet de 24 pc', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(96, 71, 'P71', 'Tube stick colle 21 GM paquet de 12 pc', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(97, 72, 'P72', 'Boite attache de 24/8mm 100 Unt', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(98, 73, 'P73', 'Boite agrafes 24/6 mm ', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(99, 74, 'P74', 'Trombones 28mm plastifIés 100 pc', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(100, 75, 'P75', 'Registre de 5 mains', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(101, 76, 'P76', 'Registre de 3 mains', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(102, 77, 'P77', 'Chemise a Rabat élastique 400 grs', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(103, 78, 'P78', 'Boite d''archive GM n°135', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(104, 79, 'P79', 'Parapheur de 25 volets', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(105, 80, 'P80', 'Parapheur 12 volets', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(106, 81, 'P81', 'Grayons noirs paquet de 12 pc', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(107, 82, 'P82', 'Ciseaux GM 17cm', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(108, 83, 'P83', 'Chemise suspendus - Réf 380 tiroirs couleur orange paquet de 100 pc', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(109, 84, 'P84', 'Serres feuilles Couleur blanche n°6', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(110, 85, 'P85', 'Serres feuilles Couleur blanche n°10', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(111, 86, 'P86', 'Serres feuilles Couleur blanche n°12', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(112, 87, 'P87', 'Serres feuilles Couleur blanche n°14', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(113, 88, 'P88', 'Serres feuilles Couleur blanche n°28', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(114, 89, 'P89', 'Serres feuilles Couleur blanche n°30', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(115, 90, 'P90', 'Stylos Bleus à bille', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(116, 91, 'P91', 'Stylos Noirs à bille', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(117, 92, 'P92', 'Stylos Rouge à bille', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(118, 93, 'P93', 'Marqueurs Noirs', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(119, 94, 'P94', 'Marqueurs Bleus', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(120, 95, 'P95', 'Fluorescents jaunes', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(121, 96, 'P96', 'Fluorescents Verts', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(122, 97, 'P97', 'Fluorescents Orange', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(123, 98, 'P98', 'Encreur Bleu', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(124, 99, 'P99', 'Toner pour photocopieur Toshiba sudio 206', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(125, 100, 'P100', 'Toner pour photocopieur Toshiba sudio 163', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(126, 101, 'P101', 'Toner pour photocopieur Toshiba sudio 207', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(127, 102, 'P102', 'Toner pour photocopieur Sharp AR 5516', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(128, 103, 'P103', 'Toner couleur noir pour photocopieur Sharp M-X 2301 N', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(129, 104, 'P104', 'Toner couleur magenta pour photocopieur Sharp M-X 2301 N', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(130, 105, 'P105', 'Toner couleur jaune pour photocopieur Sharp M-X 2301 N', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(131, 106, 'P106', 'Toner couleur bleue pour photocopieur Sharp M-X 2301 N', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(132, 107, 'P107', 'Toner pour photocopieur Sharp M-X 503 N', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(133, 108, 'P108', 'Toner pour photocopieur Xerox Workcenter 5230', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(134, 109, 'P109', 'Toner pour photocopieur Xerox Workcenter 5222', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(135, 110, 'P110', 'Toner pour photocopieur develop Ineo+215', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(136, 111, 'P111', 'Toner couleur noire pour photocopieur develop Ineo+224e', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(137, 112, 'P112', 'Toner couleur magenta pour photocopieur develop Ineo+224e', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(138, 113, 'P113', 'Toner couleur jaune pour photocopieur develop Ineo+224e', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(139, 114, 'P114', 'Toner couleur bleue pour photocopieur develop Ineo+224e', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(140, 115, 'P115', 'Toner couleur noire pour photocopieur develop Ineo+3110', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(141, 116, 'P116', 'Toner couleur magenta pour photocopieur develop Ineo+3110', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(142, 117, 'P117', 'Toner couleur jaune pour photocopieur develop Ineo+3110', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(143, 118, 'P118', 'Toner couleur bleue develop Ineo + 3110', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(144, 119, 'P119', 'Toner pour photocopieur develop Ineo 423', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(145, 120, 'P120', 'Toner HP CC531 cyan', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(146, 121, 'P121', 'Toner HP CC532 jaune', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(147, 122, 'P122', 'Toner HP CC533 magenta', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(148, 123, 'P123', 'Toner HP CC530 noir', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(149, 124, 'P124', 'Toner HP 36A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(150, 125, 'P125', 'Toner Lexmark E260A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(151, 126, 'P126', 'CD imation avec pouchettes', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(152, 127, 'P127', 'Photoconducteure lexmark C540', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(153, 128, 'P128', 'DVD imatchettesion avec pou', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(154, 129, 'P129', 'Cartouche HP 10 noir ', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(155, 130, 'P130', 'Cartouche HP 11 magenta', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(156, 131, 'P131', 'Cartouche HP 11 cayan', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(157, 132, 'P132', 'Cartouche HP 11 jaune', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(158, 133, 'P133', 'Toner develope TN 414', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(159, 134, 'P134', 'Toner Xerox 5222', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(160, 135, 'P135', 'Drum Xerox 5222', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(161, 136, 'P136', 'Unite d''image E250', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(162, 137, 'P137', 'Drum brother DR 2200', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(163, 138, 'P138', 'Toner brother TN 2210', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(164, 139, 'P139', 'Toner HP 05A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(165, 140, 'P140', 'Toner Lexmark C540A1CG bleu', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(166, 141, 'P141', 'Toner Lexmark C540A1YG jaune', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(167, 142, 'P142', 'Toner Lexmark C540A1MG magenta', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(168, 143, 'P143', 'Toner Lexmark C540A1MG noir', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(169, 144, 'P144', 'Toner HP 90A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(170, 145, 'P145', 'Photoconducteure lexmarK E260', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(171, 146, 'P146', 'Clé USB 4 GB TDK ', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(172, 147, 'P147', 'Bloc note A4160p point de mire ', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(173, 148, 'P148', 'Paquet etiquettes 4 formtec lsm 2626', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(174, 149, 'P149', 'Classeur chrono noir', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(175, 150, 'P150', 'Serre feuilles express 16mm', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(176, 151, 'P151', 'Serre feuilles express 12mm', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(177, 152, 'P152', 'Serre feuilles express 14mm', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(178, 153, 'P153', 'Pouchettes blanc avec bande securite 26X36', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(179, 154, 'P154', 'Boite agrafe 24/8', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(180, 155, 'P155', 'Boite agrafe 23/10', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(181, 156, 'P156', 'Boite agrafe 23/17', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(182, 157, 'P157', 'Carton double format 30X30X50', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(183, 158, 'P158', 'Agrafeuse geante', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(184, 159, 'P159', 'Registre pot 4 mains', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(185, 160, 'P160', 'Marqueur permanent mon ami bleu et noir', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(186, 161, 'P161', 'Boite agrafe 24/6', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(187, 162, 'P162', 'Scotche d''emballage 100m marron', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(188, 163, 'P163', 'Agrafeuse deli 24/6', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(189, 164, 'P164', 'Pouchettes en plastique 80 micro prérforé', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(190, 165, 'P165', 'Corn box en simili', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(191, 166, 'P166', 'Chemises à Rabat en carte de lyon ', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(192, 167, 'P167', 'Spirale en plastique DM28MM', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(193, 168, 'P168', 'Parapheur 18 volet ', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(194, 169, 'P169', 'Porte tarif spirale deli', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(195, 170, 'P170', 'Boite de trombonne plastifiés', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(196, 171, 'P171', 'chemises leitz', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(197, 172, 'P172', 'Punaises chromé', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(198, 173, 'P173', 'Post it 4 couleur 75X75', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(199, 174, 'P174', 'Ouvre lettres maped', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(200, 175, 'P175', 'Stylo pilot V5', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(201, 176, 'P176', 'Chemises à songle', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(202, 177, 'P177', 'Toner sharp AR21FT', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(203, 178, 'P178', 'Toner HP 85A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(204, 179, 'P179', 'Clé USB  virbatime 4GB', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(205, 180, 'P180', 'Toner HP 35A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(206, 181, 'P181', 'Clé USB 16 GB TDK', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(207, 182, 'P182', 'Clé USB 8GB TDK', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(208, 183, 'P183', 'CD arita', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(209, 184, 'P184', 'Souris touchmat', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(210, 185, 'P185', 'Clé USB 32GO Kingston', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(211, 186, 'P186', 'CD imprimable imation', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(212, 187, 'P187', 'Toner HP CB 540A noir', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(213, 188, 'P188', 'Toner HP CB 541A cayan', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(214, 189, 'P189', 'Toner HP CB 542A jaune', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(215, 190, 'P190', 'Toner HP CB 543A magenta', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(216, 191, 'P191', 'Toner HP 12A', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(217, 192, 'P192', 'Cassettes mini dv', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(218, 193, 'P193', 'Pille Sony', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(219, 194, 'P194', 'Toner Lexmark C 734 noir', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(220, 195, 'P195', 'Toner Lexmark C 734 cayan', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(221, 196, 'P196', 'Toner Lexmark C 734 jaune', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(222, 197, 'P197', 'Toner Lexmark C 734 magenta', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(223, 198, 'P198', 'Javel ACE', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(224, 199, 'P199', 'Sanicroix propre ', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(225, 200, 'P200', 'Déodorant', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(226, 201, 'P201', 'Papier hygienique ', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(227, 202, 'P202', 'Savon taouss', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(228, 203, 'P203', 'Chamoisine', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(229, 204, 'P204', 'Peau de chameau ', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(230, 205, 'P205', 'Esprit de sel', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(231, 206, 'P206', 'Abattant', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(232, 207, 'P207', 'Boite baguette de soudure', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(233, 208, 'P208', 'Boite de poudre', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(234, 209, 'P209', 'Chauffe eau de 80 litre', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(235, 210, 'P210', 'Chlore en 5 kg', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(236, 211, 'P211', 'Colle divnyl', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(237, 212, 'P212', 'Coude 20x3/4 Tm', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(238, 213, 'P213', 'Coude retube Retube de 20', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(239, 214, 'P214', 'Coude wc', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(240, 215, 'P215', 'Ensemble WC 1er choix', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(241, 216, 'P216', 'flexible branchement', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(242, 217, 'P217', 'Glace de lavabo avec patte', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(243, 218, 'P218', 'Groupe sécurité 3/4', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(244, 219, 'P219', 'Kit de jonction', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(245, 220, 'P220', 'Kit de wc complet clever', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(246, 221, 'P221', 'lavabo avec colonne', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(247, 222, 'P222', 'mecanimse poussoir', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(248, 223, 'P223', 'Mecanisme poussoir SAS', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(249, 224, 'P224', 'Melangeur  lavabo', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(250, 225, 'P225', 'Melangeur cuisine', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(251, 226, 'P226', 'Mitigeur Bidet', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(252, 227, 'P227', 'mitigeur lavabo', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(253, 228, 'P228', 'Rallonge wc ', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(254, 229, 'P229', 'Reservoire wc complet', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(255, 230, 'P230', 'Robinet de service', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(256, 231, 'P231', 'Tete de melangeur', 100, 250, 300, 1, 1, 1, 1, 1, 5, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, '2016-08-09 00:28:31', 1),
(257, 232, 'P232', 'Tuyau d''arrosage', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(258, 233, 'P233', 'Wc complet à l''angalise complet', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(259, 234, 'P234', 'Accessoires wc en inox', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(260, 235, 'P235', 'Chauffe eau de 80 litre', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(261, 236, 'P236', 'Chauffe eau instantanné', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(262, 237, 'P237', 'Collier galvanisé 3/4', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(263, 238, 'P238', 'Coude 1/2 chromé', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(264, 239, 'P239', 'Cuivre 12mm', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(265, 240, 'P240', 'Esprit de sel', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(266, 241, 'P241', 'Fixation wc', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(267, 242, 'P242', 'Flexible de mitigeur', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(268, 243, 'P243', 'Flexible evacuation wc Italy', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(269, 244, 'P244', 'Machine pour ppr ', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(270, 245, 'P245', 'Plongeoir complet ', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(271, 246, 'P246', 'Polytilene 25', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(272, 247, 'P247', 'Pompe 2ch 220v', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(273, 248, 'P248', 'Porte savon liquide inox', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(274, 249, 'P249', 'PVC d''evacuation', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(275, 250, 'P250', 'PVC d''evacuation 110', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(276, 251, 'P251', 'Raccord 1/2/3/4', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(277, 252, 'P252', 'Raccord 20*20 tm', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(278, 253, 'P253', 'Raccord de 20 male de retube', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(279, 254, 'P254', 'Raccord male de 16 tm', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(280, 255, 'P255', 'Raccord male de 20 tm', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(281, 256, 'P256', 'Raccord union 1P', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(282, 257, 'P257', 'Raccord union 2P', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(283, 258, 'P258', 'Reduction 1"* 3/4', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(284, 259, 'P259', 'Reduction 3/4*1/2', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(285, 260, 'P260', 'Reservoire wc complet', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(286, 261, 'P261', 'Retube en alluminuim 16', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(287, 262, 'P262', 'Retube en alluminuim 20', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(288, 263, 'P263', 'Robiner jardin 3/4', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(289, 264, 'P264', 'Robinet 1/2 1/2', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(290, 265, 'P265', 'Robinet 1/2 3/8', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(291, 266, 'P266', 'Robinet à bec', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(292, 267, 'P267', 'Robinet d''arret 1/2 tm', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(293, 268, 'P268', 'Robinet d''arret 3/4 tm', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(294, 269, 'P269', 'Robinet de lavabo', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(295, 270, 'P270', 'Robinet de service', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(296, 271, 'P271', 'Robinet en cuivre pour fontaine', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(297, 272, 'P272', 'Robinet flotteur', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(298, 273, 'P273', 'Robinet jardin 3/4 ', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(299, 274, 'P274', 'Robinet oslo ST', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(300, 275, 'P275', 'Silicone d''etanchieté', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(301, 276, 'P276', 'Siphon 32', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(302, 277, 'P277', 'Siphon 40', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(303, 278, 'P278', 'Siphon gorge ', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(304, 279, 'P279', 'Té 20*20 Tm', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(305, 280, 'P280', 'Té chromé 1/2', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(306, 281, 'P281', 'Te egale de 16', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(307, 282, 'P282', 'Teflon gm', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(308, 283, 'P283', 'Tube galvanisé 3/4', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(309, 284, 'P284', 'Vane 3/4', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(310, 285, 'P285', 'Vanne 1"1/2', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(311, 286, 'P286', 'Vanne à bille 1"', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(312, 287, 'P287', 'Vanne à volant 2"', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(313, 288, 'P288', 'vanne jardin', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(314, 289, 'P289', 'Vide cave', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(315, 290, 'P290', 'Wc  à la turque', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(316, 291, 'P291', 'Wc complet à l''angalise complet', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(317, 292, 'P292', 'Essence White spirit  "en litre"', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(318, 293, 'P293', 'Enduit poudre "  en kg"', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(319, 294, 'P294', 'Duliant  en litre', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(320, 295, 'P295', 'Peinture coloflexe "en kg"', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(321, 296, 'P296', 'Peinture à eau   en kg', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(322, 297, 'P297', 'Peinture antirouille  en kg', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(323, 298, 'P298', 'Peinture laque blanche en kg', 100, 250, 300, 1, 1, 1, 1, 1, 5, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, '2016-08-09 00:51:50', 1),
(324, 299, 'P299', 'vernis  en litre', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(325, 300, 'P300', 'Teinte  divers', 100, 250, 300, 1, 1, 1, 1, 1, 1, 1, 1, 25, 'Note ', 'Y', 1, '2016-08-09 00:01:22', NULL, NULL, NULL),
(326, 301, 'P301', 'Produit N1', 0, 0, 0, 1, 2, NULL, NULL, NULL, 5, NULL, NULL, NULL, '', 'N', 1, '2016-08-24 13:13:34', 1, NULL, NULL),
(327, 302, 'P302', 'Poduit1 je suis là ', 0, 200, 300, 2, 4, 51, 5, NULL, 5, NULL, NULL, 500, 'note p032', 'Y', 1, '2016-08-30 18:37:35', 1, '2016-09-28 10:48:59', 1);

-- --------------------------------------------------------

--
-- Structure de la table `SM_PRODUCT_COLOR`
--

CREATE TABLE IF NOT EXISTS `SM_PRODUCT_COLOR` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `HEX` tinytext CHARACTER SET latin1,
  `RGB` tinytext CHARACTER SET latin1,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_PRODUCT_COLOR`
--

INSERT INTO `SM_PRODUCT_COLOR` (`ID`, `NAME`, `HEX`, `RGB`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `CLT_MODULE_ID`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Bleu', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(2, 'Blanc', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(3, 'Gris', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(4, 'Jaune', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(5, 'Marron', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(6, 'Noir', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(7, 'Or', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(8, 'Rouge', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(9, 'Vert', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(10, 'Violet', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(11, 'Violet', NULL, NULL, NULL, 'N', NULL, 1, NULL, NULL, NULL),
(12, 'New Color11', NULL, NULL, NULL, 'N', NULL, 1, NULL, NULL, NULL),
(13, 'bbb', NULL, NULL, NULL, 'N', NULL, 1, NULL, NULL, NULL),
(14, 'Couleur de produit  Value', NULL, NULL, NULL, 'N', '2016-05-04 00:02:20', 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_PRODUCT_DEPARTMENT`
--

CREATE TABLE IF NOT EXISTS `SM_PRODUCT_DEPARTMENT` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` varchar(45) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_PRODUCT_DEPARTMENT`
--

INSERT INTO `SM_PRODUCT_DEPARTMENT` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Département 1', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Département 2', 'Département de produit  Description Default V', NULL, 1, 'Y', '2016-05-04 00:01:26', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_PRODUCT_FAMILY`
--

CREATE TABLE IF NOT EXISTS `SM_PRODUCT_FAMILY` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `PRODUCT_GROUP_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_PRODUCT_FAMILY`
--

INSERT INTO `SM_PRODUCT_FAMILY` (`ID`, `NAME`, `DESCRIPTION`, `PRODUCT_GROUP_ID`, `SORT_KEY`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Fournitures Informatiques', 'Fournitures Informatiques', 1, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'Matériel Informatique', 'Matériel Informatique', 1, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(3, 'Mobilier de bureau', 'Mobilier de bureau', 1, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(4, 'Matériel de bureau', 'Matériel de bureau', 2, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(5, 'Fournitures de bureau', 'Fournitures de bureau', 1, NULL, 'Y', 1, '2016-05-04 00:01:26', NULL, NULL, NULL),
(6, 'Documentations', 'Documentations', 1, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(7, 'Impression', 'Impression', 1, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(8, 'DET et Netoyage', 'DET et Netoyage', 2, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(9, 'Outillage et Quillerie', 'Outillage et Quillerie', 2, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(10, 'F. de Plomberie', 'F. de Plomberie', 2, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(11, 'F. éléctrique', 'F. éléctrique', 1, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(12, 'Matériaux de construction', 'Matériaux de construction', 1, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(13, 'Récéption', 'Récéption', 2, NULL, 'Y', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_PRODUCT_GROUP`
--

CREATE TABLE IF NOT EXISTS `SM_PRODUCT_GROUP` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_PRODUCT_GROUP`
--

INSERT INTO `SM_PRODUCT_GROUP` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Groupe 01', 'Groupe 01', NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Groupe 02', 'Groupe 02', NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(3, 'Groupe 03', 'Groupe 03', NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(4, 'Groupe 04', 'Groupe 04', NULL, 11, 'Y', '2016-05-04 00:03:52', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_PRODUCT_SIZE`
--

CREATE TABLE IF NOT EXISTS `SM_PRODUCT_SIZE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_PRODUCT_SIZE`
--

INSERT INTO `SM_PRODUCT_SIZE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Moyenne', 'Moyenne', NULL, 1, 'N', NULL, NULL, NULL, NULL),
(47, '37', NULL, NULL, 1, 'N', NULL, 1, NULL, NULL),
(48, '38', NULL, NULL, 1, 'N', NULL, 1, NULL, NULL),
(49, '39', NULL, NULL, 1, 'N', NULL, 1, NULL, NULL),
(50, '40', NULL, NULL, 1, 'N', NULL, 1, NULL, NULL),
(51, 'Moyenne', NULL, 3, 1, 'Y', NULL, 1, NULL, NULL),
(52, 'Très Grand', NULL, 5, 1, 'Y', NULL, 1, NULL, NULL),
(53, '41', NULL, NULL, 1, 'N', NULL, 1, NULL, NULL),
(54, '67', NULL, NULL, 1, 'N', NULL, 1, NULL, NULL),
(55, '78', NULL, NULL, 1, 'N', NULL, 1, NULL, NULL),
(56, '89', NULL, NULL, 1, 'N', NULL, 1, NULL, NULL),
(57, '98', NULL, NULL, 1, 'N', NULL, 1, NULL, NULL),
(58, 'Très petit', NULL, 1, 1, 'Y', NULL, 1, NULL, NULL),
(59, 'Petit', NULL, 2, 1, 'Y', NULL, 1, NULL, NULL),
(60, 'Grand', NULL, 4, 1, 'Y', NULL, 1, NULL, NULL),
(61, '42', NULL, NULL, 1, 'N', NULL, 1, NULL, NULL),
(62, 'Taille de produit Default Value', 'taille de produit  Description Default Value', NULL, 11, 'N', '2016-05-04 00:01:26', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_PRODUCT_STATUS`
--

CREATE TABLE IF NOT EXISTS `SM_PRODUCT_STATUS` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_PRODUCT_STATUS`
--

INSERT INTO `SM_PRODUCT_STATUS` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Statuts product 1', 'desc', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Statuts product 2', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_PRODUCT_TYPE`
--

CREATE TABLE IF NOT EXISTS `SM_PRODUCT_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` varchar(45) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_PRODUCT_TYPE`
--

INSERT INTO `SM_PRODUCT_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Type 01', 'Type 01', NULL, 11, 'Y', NULL, NULL, NULL, NULL),
(2, 'Type 02', 'Type 02', NULL, 11, 'Y', NULL, NULL, NULL, NULL),
(3, 'Type 03', 'Type 03', NULL, 11, 'Y', NULL, NULL, NULL, NULL),
(4, 'Type 04', 'Type 04', NULL, 11, 'Y', '2016-05-03 23:55:20', NULL, NULL, NULL),
(5, 'Consommables', 'Consommables', NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(6, 'Matériel', 'Matériel', NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_PRODUCT_UNIT`
--

CREATE TABLE IF NOT EXISTS `SM_PRODUCT_UNIT` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` varchar(45) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_PRODUCT_UNIT`
--

INSERT INTO `SM_PRODUCT_UNIT` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'lettre', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Unit Default Value', 'Unit Description Default Value', NULL, 11, 'Y', '2016-05-04 00:04:34', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_PROMOTION`
--

CREATE TABLE IF NOT EXISTS `SM_PROMOTION` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `PROMOTION_TYPE_ID` bigint(20) DEFAULT NULL,
  `PRODUCT` bigint(20) DEFAULT NULL,
  `VALUE` double DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `SM_PROMOTION_TYPE`
--

CREATE TABLE IF NOT EXISTS `SM_PROMOTION_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_PROMOTION_TYPE`
--

INSERT INTO `SM_PROMOTION_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Promotion Type Default Value', 'Promotion Type Description Default Value', NULL, 11, 'Y', '2016-05-03 23:55:20', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_RECEPTION`
--

CREATE TABLE IF NOT EXISTS `SM_RECEPTION` (
  `ID` bigint(20) NOT NULL,
  `NO_SEQ` bigint(20) DEFAULT NULL,
  `REFERENCE` tinytext,
  `SOUCHE` tinytext,
  `SUPPLIER_ID` bigint(20) DEFAULT NULL,
  `DEPOSIT_ID` bigint(20) DEFAULT NULL,
  `RECEPTION_STATUS_ID` bigint(20) DEFAULT NULL,
  `DEADLINE` datetime DEFAULT NULL,
  `NOTE` text,
  `TVA` double DEFAULT NULL,
  `DELIVERY` tinytext,
  `ORDER_SUPPLIER_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `EXPIRATION_DATE` datetime DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_RECEPTION`
--

INSERT INTO `SM_RECEPTION` (`ID`, `NO_SEQ`, `REFERENCE`, `SOUCHE`, `SUPPLIER_ID`, `DEPOSIT_ID`, `RECEPTION_STATUS_ID`, `DEADLINE`, `NOTE`, `TVA`, `DELIVERY`, `ORDER_SUPPLIER_ID`, `ACTIVE`, `EXPIRATION_DATE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(43, 1, 'R1', NULL, 1, NULL, 3, NULL, 'centre', NULL, NULL, 9, NULL, NULL, 1, '2015-12-13 17:59:36', NULL, NULL, NULL),
(44, 2, 'R2', NULL, NULL, NULL, 3, NULL, '', NULL, NULL, 9, NULL, NULL, 1, '2015-12-13 18:32:47', NULL, NULL, NULL),
(45, 3, 'R3', NULL, 16, NULL, 3, NULL, '', NULL, NULL, 9, NULL, NULL, 1, '2015-12-13 22:28:11', NULL, NULL, NULL),
(46, 4, 'R4', NULL, 16, NULL, 3, NULL, 'centre', NULL, NULL, 9, NULL, NULL, 1, '2015-12-27 18:54:30', NULL, NULL, NULL),
(47, 5, 'R5', NULL, 15, NULL, 3, NULL, 'Ok merci', NULL, NULL, 9, NULL, NULL, 1, '2015-12-27 18:57:14', NULL, NULL, NULL),
(48, 6, 'B97C1K', NULL, 1, NULL, 3, NULL, ' Note interne  11', NULL, NULL, 7, NULL, NULL, 1, '2016-01-21 23:52:16', NULL, NULL, NULL),
(49, 7, '54TKNU', NULL, 1, NULL, 3, NULL, 'note', NULL, NULL, NULL, NULL, NULL, 1, '2016-02-09 22:17:24', NULL, NULL, NULL),
(50, 8, 'UDFPV6', NULL, 1, NULL, 2, NULL, 'fdsf', NULL, NULL, NULL, NULL, NULL, 1, '2016-03-06 19:38:26', NULL, NULL, NULL),
(51, 9, 'R9', NULL, 1, NULL, 3, NULL, 'note', NULL, NULL, NULL, NULL, NULL, 1, '2016-03-08 19:43:51', NULL, NULL, NULL),
(53, 10, 'R10', NULL, 15, NULL, 1, NULL, 'Aa', NULL, NULL, NULL, NULL, NULL, 1, '2016-03-13 00:49:36', NULL, '2016-08-30 18:39:33', 1),
(54, 1, 'QSRFG', NULL, 1, NULL, 3, NULL, NULL, NULL, NULL, 24, 'Y', NULL, NULL, '2016-04-07 23:01:36', NULL, NULL, NULL),
(55, 1, 'qsdfqdf', NULL, 1, NULL, 3, NULL, NULL, NULL, NULL, 26, 'Y', NULL, NULL, '2016-04-07 23:02:21', NULL, NULL, NULL),
(56, 1, 'VK3MM9', NULL, 25, NULL, 3, NULL, 'note1', NULL, NULL, NULL, NULL, NULL, 11, '2016-05-18 22:39:42', NULL, '2016-05-27 00:02:41', 11),
(57, 2, 'OA7XK7', NULL, 25, NULL, 1, NULL, '', NULL, NULL, NULL, NULL, NULL, 11, '2016-05-21 15:17:30', 11, '2016-05-21 15:17:44', 11),
(58, 3, 'WCBVLO', NULL, 25, NULL, 2, NULL, '', NULL, NULL, NULL, NULL, NULL, 11, '2016-05-27 00:01:53', 11, '2016-05-27 00:02:28', 11),
(59, 11, 'R11', NULL, 16, NULL, 3, NULL, '', NULL, NULL, NULL, NULL, NULL, 1, '2016-07-29 19:43:44', 1, '2016-07-29 19:45:08', 1),
(60, 12, 'R12', NULL, NULL, NULL, 1, NULL, '', NULL, NULL, NULL, NULL, NULL, 1, '2016-12-09 21:19:42', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_RECEPTION_LINE`
--

CREATE TABLE IF NOT EXISTS `SM_RECEPTION_LINE` (
  `ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `DESIGNATION` tinytext,
  `UNIT_PRICE_BUY` double DEFAULT NULL,
  `REMISE` double DEFAULT NULL,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `TVA` double DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `RECEPTION_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_RECEPTION_LINE`
--

INSERT INTO `SM_RECEPTION_LINE` (`ID`, `PRODUCT_ID`, `DESIGNATION`, `UNIT_PRICE_BUY`, `REMISE`, `QUANTITY`, `TVA`, `ACTIVE`, `RECEPTION_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 2, 'Pc portable1', 100, NULL, 1, NULL, NULL, 43, NULL, NULL, NULL, NULL),
(2, 3, 'Nokia', 200, NULL, 1, NULL, NULL, 43, NULL, NULL, NULL, NULL),
(3, 4, 'Sumsung duos', 300, NULL, 1, NULL, NULL, 43, NULL, NULL, NULL, NULL),
(4, 18, 'Mac OS', 300, NULL, 1, NULL, NULL, 43, NULL, NULL, NULL, NULL),
(5, 19, 'HP Portable', 400, NULL, 1, NULL, NULL, 43, NULL, NULL, NULL, NULL),
(6, 23, 'Note Portable', 500, NULL, 1, NULL, NULL, 43, NULL, NULL, NULL, NULL),
(11, 2, 'Pc portable1', 100, NULL, 9, NULL, NULL, 45, NULL, NULL, NULL, NULL),
(12, 2, 'Pc portable1', 100, NULL, 9, NULL, NULL, 46, NULL, NULL, NULL, NULL),
(13, 3, 'Nokia', 200, NULL, 1, NULL, NULL, 46, NULL, NULL, NULL, NULL),
(14, 2, 'Pc portable1', 100, NULL, 7, NULL, NULL, 47, NULL, NULL, NULL, NULL),
(15, 3, 'Nokia', 200, NULL, 180, NULL, NULL, 47, NULL, NULL, NULL, NULL),
(16, 2, 'Pc portable1', 100, NULL, 7, NULL, NULL, 48, NULL, NULL, NULL, NULL),
(17, 3, 'Nokia', 200, NULL, 7, NULL, NULL, 48, NULL, NULL, NULL, NULL),
(18, 2, 'Pc portable1', 100, NULL, 9, NULL, NULL, 49, NULL, NULL, NULL, NULL),
(19, NULL, 'Pc portable1', 100, NULL, 1, NULL, NULL, 49, NULL, NULL, NULL, NULL),
(20, NULL, 'Nokia', 200, NULL, 1, NULL, NULL, 49, NULL, NULL, NULL, NULL),
(21, NULL, 'Nokia', 200, NULL, 1, NULL, NULL, 49, NULL, NULL, NULL, NULL),
(22, NULL, 'Nokia', 200, NULL, 1, NULL, NULL, 49, NULL, NULL, NULL, NULL),
(23, NULL, 'Pc portable1', 100, NULL, 1, NULL, NULL, 50, NULL, NULL, NULL, NULL),
(24, NULL, 'Pc portable1', 100, NULL, 1, NULL, NULL, 49, NULL, NULL, NULL, NULL),
(25, 2, 'Pc portable1', 100, NULL, 9, NULL, NULL, 44, NULL, NULL, NULL, NULL),
(26, 2, 'Pc portable1', 100, NULL, 1, NULL, NULL, 44, NULL, NULL, NULL, NULL),
(27, 19, 'HP Portable', 400, NULL, 1, NULL, NULL, 44, NULL, NULL, NULL, NULL),
(28, 2, 'Pc portable1', 100, NULL, 7, NULL, NULL, 51, NULL, NULL, NULL, NULL),
(30, 19, 'HP Portable', 400, NULL, 1, NULL, NULL, 51, NULL, NULL, NULL, NULL),
(33, 3, 'Nokia', 200, NULL, 12, NULL, NULL, 53, NULL, NULL, '2016-08-30 18:39:29', 1),
(34, 2, NULL, NULL, NULL, 2, NULL, 'Y', 54, NULL, NULL, NULL, NULL),
(35, 2, NULL, NULL, NULL, 3, NULL, 'Y', 55, NULL, NULL, NULL, NULL),
(36, 4, 'Sumsung duos', 300, NULL, 1, NULL, NULL, 50, NULL, NULL, NULL, NULL),
(37, 41, 'note', 1500, NULL, 10, NULL, NULL, 56, NULL, NULL, NULL, NULL),
(38, 40, 'npte11', 0, NULL, 1, NULL, NULL, 56, NULL, NULL, NULL, NULL),
(39, 40, 'desc1', 1500, NULL, 99, NULL, NULL, 56, NULL, NULL, NULL, NULL),
(40, 41, 'note', 1500, NULL, 50, NULL, NULL, 56, NULL, NULL, NULL, NULL),
(41, 41, 'note', 1500, NULL, 10, NULL, NULL, 57, NULL, NULL, NULL, NULL),
(42, 40, 'npte11', 0, NULL, 1, NULL, NULL, 57, NULL, NULL, NULL, NULL),
(43, 40, 'desc1', 1500, NULL, 99, NULL, NULL, 57, NULL, NULL, NULL, NULL),
(44, 41, 'note', 1500, NULL, 10, NULL, NULL, 58, NULL, NULL, NULL, NULL),
(45, 40, 'npte11', 0, NULL, 1, NULL, NULL, 58, NULL, NULL, NULL, NULL),
(46, 40, 'desc1', 1500, NULL, 4, NULL, NULL, 58, NULL, NULL, '2016-05-27 00:02:12', 11),
(47, 42, 'Sumsung Galaxy S7', 6000, NULL, 47, NULL, NULL, 59, NULL, NULL, '2016-07-29 19:44:20', 1);

-- --------------------------------------------------------

--
-- Structure de la table `SM_RECEPTION_PRODUCTS`
--

CREATE TABLE IF NOT EXISTS `SM_RECEPTION_PRODUCTS` (
  `ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `DESIGNATION` tinytext,
  `UNIT_PRICE_BUY` double DEFAULT NULL,
  `REMISE` double DEFAULT NULL,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `TVA` double DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `RECEPTION_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `SM_RECEPTION_STATUS`
--

CREATE TABLE IF NOT EXISTS `SM_RECEPTION_STATUS` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_RECEPTION_STATUS`
--

INSERT INTO `SM_RECEPTION_STATUS` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'En cours de saisie', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'Transmis', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Validé', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'Refuser', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(5, 'Rejeter', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_RECEPTION_STATUS_LOG`
--

CREATE TABLE IF NOT EXISTS `SM_RECEPTION_STATUS_LOG` (
  `ID` bigint(20) NOT NULL,
  `RECEPTION_ID` bigint(20) DEFAULT NULL,
  `RECEPTION_STATUS_ID` bigint(20) DEFAULT NULL,
  `JUSTIFICATION_STATUS_ID` bigint(20) DEFAULT NULL,
  `REASON_TITLE` tinytext,
  `REASON_CONTENT` text,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `SM_RETURN_RECEIPT`
--

CREATE TABLE IF NOT EXISTS `SM_RETURN_RECEIPT` (
  `ID` bigint(20) NOT NULL,
  `NO_SEQ` bigint(20) DEFAULT NULL,
  `REFERENCE` tinytext,
  `CUSTOMER_ID` bigint(20) DEFAULT NULL,
  `ORDER_ID` bigint(20) DEFAULT NULL,
  `RETURN_RECEIPT_STATUS_ID` bigint(20) DEFAULT NULL,
  `NOTE` text,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_RETURN_RECEIPT`
--

INSERT INTO `SM_RETURN_RECEIPT` (`ID`, `NO_SEQ`, `REFERENCE`, `CUSTOMER_ID`, `ORDER_ID`, `RETURN_RECEIPT_STATUS_ID`, `NOTE`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(5, 1, 'ref1', 6, NULL, 1, 'Note12', 'Y', 1, '2015-11-21 02:46:35', NULL, NULL, NULL),
(6, 1, NULL, 6, NULL, 2, NULL, 'Y', 1, '2015-11-21 02:46:35', NULL, NULL, NULL),
(7, 2, 'R2', 6, NULL, 1, 'aaaaaa', NULL, 1, '2015-12-27 21:39:07', NULL, NULL, NULL),
(8, 3, 'R3', 6, NULL, 1, 'note123', NULL, 1, '2015-12-27 22:52:59', NULL, NULL, NULL),
(9, 4, 'XZ6Q31', 6, NULL, 1, 'OKSDFG67', NULL, 1, '2015-12-27 23:01:12', NULL, NULL, NULL),
(10, 5, 'R5', 6, NULL, 2, 'aaaa', NULL, 1, '2016-01-16 21:53:30', NULL, NULL, NULL),
(11, 6, 'R6', NULL, NULL, 2, '', NULL, 1, '2016-04-15 23:02:35', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_RETURN_RECEIPT_LINE`
--

CREATE TABLE IF NOT EXISTS `SM_RETURN_RECEIPT_LINE` (
  `ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `DESIGNATION` text,
  `RETURN_RECEIPT_ID` bigint(20) DEFAULT NULL,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `PROMOTION_ID` bigint(20) DEFAULT NULL,
  `NEGOTIATE_PRICE_SALE` double DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_RETURN_RECEIPT_LINE`
--

INSERT INTO `SM_RETURN_RECEIPT_LINE` (`ID`, `PRODUCT_ID`, `DESIGNATION`, `RETURN_RECEIPT_ID`, `QUANTITY`, `PROMOTION_ID`, `NEGOTIATE_PRICE_SALE`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(3, 2, 'Pc portable1', 7, 1, NULL, 100, NULL, NULL, NULL, NULL, NULL),
(4, 18, 'Mac OS', 7, 1, NULL, 300, NULL, NULL, NULL, NULL, NULL),
(5, 3, 'TEST 11111', 7, 222, NULL, 4567, NULL, NULL, NULL, NULL, NULL),
(6, 4, 'Sumsung duos', 7, 22, NULL, 300, NULL, NULL, NULL, NULL, NULL),
(7, 4, 'Note Portable', 7, 222, NULL, 111.33, NULL, NULL, NULL, NULL, NULL),
(8, 2, 'Pc portable1', 7, 4567, NULL, 100, NULL, NULL, NULL, NULL, NULL),
(9, 2, 'Pc portable1', 5, 11, NULL, 100, NULL, NULL, NULL, NULL, NULL),
(11, 23, 'Note Portable', 8, 1, NULL, 500, NULL, NULL, NULL, NULL, NULL),
(12, 2, 'Pc portable1', 8, 1, NULL, 100, NULL, NULL, NULL, NULL, NULL),
(13, 2, 'Pc portable1', 9, 1, NULL, 100, NULL, NULL, NULL, NULL, NULL),
(14, 23, 'Note Portable', 10, 1, NULL, 500, NULL, NULL, NULL, NULL, NULL),
(15, 2, 'Pc portable1', 10, 1, NULL, 100, NULL, NULL, NULL, NULL, NULL),
(16, 19, 'HP Portable', 10, 1, NULL, 400, NULL, NULL, NULL, NULL, NULL),
(17, 23, 'Note Portable', 11, 1, NULL, 500, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_RETURN_RECEIPT_STATUS`
--

CREATE TABLE IF NOT EXISTS `SM_RETURN_RECEIPT_STATUS` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_RETURN_RECEIPT_STATUS`
--

INSERT INTO `SM_RETURN_RECEIPT_STATUS` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'en cours', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'submit', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Validé', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'Refuser', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(5, 'Rejeter', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_RETURN_RECEIPT_STATUS_LOG`
--

CREATE TABLE IF NOT EXISTS `SM_RETURN_RECEIPT_STATUS_LOG` (
  `ID` bigint(20) NOT NULL,
  `RETURN_RECEIPT_ID` bigint(20) DEFAULT NULL,
  `RETURN_RECEIPT_STATUS_ID` bigint(20) DEFAULT NULL,
  `JUSTIFICATION_STATUS_ID` bigint(20) DEFAULT NULL,
  `REASON_TITLE` tinytext,
  `REASON_CONTENT` text,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `SM_SUPPLIER`
--

CREATE TABLE IF NOT EXISTS `SM_SUPPLIER` (
  `ID` bigint(20) NOT NULL,
  `FIRST_NAME` tinytext CHARACTER SET latin1,
  `LAST_NAME` tinytext CHARACTER SET latin1,
  `COMPANY_NAME` tinytext,
  `REPRESENTATIVE` tinytext,
  `SHORT_LABEL` tinytext,
  `FULL_LABEL` text,
  `NOTE` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `SUPPLIER_TYPE_ID` bigint(20) DEFAULT NULL,
  `SUPPLIER_CATEGORY_ID` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ENTITY_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `SUPPLIER_NATURE_ID` bigint(20) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_SUPPLIER`
--

INSERT INTO `SM_SUPPLIER` (`ID`, `FIRST_NAME`, `LAST_NAME`, `COMPANY_NAME`, `REPRESENTATIVE`, `SHORT_LABEL`, `FULL_LABEL`, `NOTE`, `SORT_KEY`, `ACTIVE`, `SUPPLIER_TYPE_ID`, `SUPPLIER_CATEGORY_ID`, `CLT_MODULE_ID`, `ENTITY_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `SUPPLIER_NATURE_ID`) VALUES
(1, 'sybaway@gmail.com', 'sybaway@gmail.com', ' Nom de l''entreprise ', ' Représentant ', ' Libellé court ', ' Libellé complète ', 'note 1', NULL, 'Y', 2, 1, 1, 1, NULL, 1, '2016-09-25 13:12:58', 1, 1),
(15, ' Prénom ', 'Nom ', 'JIA DA LI SHOES', '', '', ' Libellé complète ', 'kaka', NULL, 'Y', 1, NULL, 1, 1, NULL, 1, '2016-08-02 21:54:47', 1, 2),
(16, 'Ahmed', 'Ali', 'INF sos inc', 'Abdessamad', '', '', '', NULL, 'Y', 1, 2, 1, 1, NULL, 1, '2016-12-09 21:11:21', 1, 2),
(22, 'dqsdqsdsq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, 1, '2016-04-12 13:00:46', NULL, NULL, NULL, 1),
(25, '', '', 'Test1', 'Abdesmsaad HALLAL', 'centre', 'aaaa', 'note1', NULL, 'Y', 3, 3, 11, 57, '2016-05-18 22:34:49', 11, NULL, NULL, 1),
(26, '', '', '', 'Abdessamad', '', '', '', NULL, '', NULL, NULL, 1, 59, '2016-12-09 21:11:58', 1, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `SM_SUPPLIER_CATEGORY`
--

CREATE TABLE IF NOT EXISTS `SM_SUPPLIER_CATEGORY` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` varchar(45) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_SUPPLIER_CATEGORY`
--

INSERT INTO `SM_SUPPLIER_CATEGORY` (`ID`, `NAME`, `DESCRIPTION`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'categorie One', NULL, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'categorie Tow', NULL, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Fournisseur catégorie Default Value', 'Fournisseur catégorie Description Default Val', 11, NULL, 'Y', '2016-05-03 23:55:20', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_SUPPLIER_TYPE`
--

CREATE TABLE IF NOT EXISTS `SM_SUPPLIER_TYPE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_SUPPLIER_TYPE`
--

INSERT INTO `SM_SUPPLIER_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Type1', 'Type1', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Type 2', 'Type 2', 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Type1', 'Type1', 11, NULL, 'Y', '2016-05-03 23:55:20', NULL, NULL, NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `CLT_CLIENT`
--
ALTER TABLE `CLT_CLIENT`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `CODE_UNIQUE` (`CODE`),
  ADD KEY `fk_CLT_CLIENT_CLT_CLIENT_STATUS1_idx` (`CLIENT_STATUS_ID`),
  ADD KEY `FK4_CLT_CLIENT_idx` (`ENTITY_ID`);

--
-- Index pour la table `CLT_CLIENT_LANGUAGE`
--
ALTER TABLE `CLT_CLIENT_LANGUAGE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_CLT_CLIENT_LANGUAGE_CLT_CLIENT1_idx` (`CLIENT_ID`),
  ADD KEY `fk_CLT_CLIENT_LANGUAGE_INF_LANGUAGE1_idx` (`INF_LANGUAGE_ID`),
  ADD KEY `FK2_CLT_CLIENT_LANGUAGE` (`INF_PREFIX_ID`);

--
-- Index pour la table `CLT_CLIENT_STATUS`
--
ALTER TABLE `CLT_CLIENT_STATUS`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `CLT_MODEL_PARAMETER`
--
ALTER TABLE `CLT_MODEL_PARAMETER`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_CLT_MODEL_PARAMETER_idx` (`MODEL_PARAMETER_TYPE_ID`);

--
-- Index pour la table `CLT_MODEL_PARAMETER_MODEL`
--
ALTER TABLE `CLT_MODEL_PARAMETER_MODEL`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_CLT_MODEL_PARAMETER_MODEL_idx` (`MODEL_PARAMETER_ID`),
  ADD KEY `FK2_CLT_MODEL_PARAMETER_MODEL_idx` (`PM_MODEL_ID`);

--
-- Index pour la table `CLT_MODEL_PARAMETER_MODULE`
--
ALTER TABLE `CLT_MODEL_PARAMETER_MODULE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `idx_clt_module_parameter_client` (`MODEL_PARAMETER_ID`),
  ADD KEY `idx_clt_module_parameter_client_0` (`MODULE_ID`);

--
-- Index pour la table `CLT_MODEL_PARAMETER_TYPE`
--
ALTER TABLE `CLT_MODEL_PARAMETER_TYPE`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `CLT_MODULE`
--
ALTER TABLE `CLT_MODULE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_CLT_FOLDER_CLT_CLIENT1_idx` (`CLIENT_ID`),
  ADD KEY `fk_CLT_MODULE_CLT_MODULE_STATUS1_idx` (`MODULE_STATUS_ID`),
  ADD KEY `fk_CLT_MODULE_TYPE1_idx` (`MODULE_TYPE_ID`);

--
-- Index pour la table `CLT_MODULE_STATUS`
--
ALTER TABLE `CLT_MODULE_STATUS`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `CLT_MODULE_TYPE`
--
ALTER TABLE `CLT_MODULE_TYPE`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `CLT_PARAMETER`
--
ALTER TABLE `CLT_PARAMETER`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_CLT_PARAMETER_CLT_TYPE_PARAMETER1_idx` (`PARAMETER_TYPE_ID`);

--
-- Index pour la table `CLT_PARAMETER_CLIENT`
--
ALTER TABLE `CLT_PARAMETER_CLIENT`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `idx_clt_parameter_client` (`PARAMETER_ID`),
  ADD KEY `idx_clt_parameter_client_0` (`CLINET_ID`);

--
-- Index pour la table `CLT_PARAMETER_TYPE`
--
ALTER TABLE `CLT_PARAMETER_TYPE`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `CLT_STRUCTURE`
--
ALTER TABLE `CLT_STRUCTURE`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `CLT_STRUCTURE_ROLE`
--
ALTER TABLE `CLT_STRUCTURE_ROLE`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `CLT_STRUCTURE_TYPE`
--
ALTER TABLE `CLT_STRUCTURE_TYPE`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `CLT_USER`
--
ALTER TABLE `CLT_USER`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_CLT_USER_CLT_CLIENT1_idx` (`CLIENT_ID`),
  ADD KEY `fk_CLT_USER_CLT_CATEGORY_USER1_idx` (`CATEGORY_ID`),
  ADD KEY `idx_clt_user` (`USER_STATUS_ID`),
  ADD KEY `FK6_CLT_USER_idx` (`ENTITY_ID`);

--
-- Index pour la table `CLT_USER_CATEGORY`
--
ALTER TABLE `CLT_USER_CATEGORY`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_CLT_USER_CATEGORY_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `CLT_USER_CLIENT`
--
ALTER TABLE `CLT_USER_CLIENT`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_USER_CLIENT_ID_idx` (`CLIENT_ID`),
  ADD KEY `FK2_USER_CLIENT_idx` (`USER_ID`);

--
-- Index pour la table `CLT_USER_GROUP`
--
ALTER TABLE `CLT_USER_GROUP`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_CLT_USER_GROUP_CLT_USER1_idx` (`USER_ID`),
  ADD KEY `fk_CLT_USER_GROUP_INF_GROUP1_idx` (`INF_GROUP_ID`);

--
-- Index pour la table `CLT_USER_MODULE`
--
ALTER TABLE `CLT_USER_MODULE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_CLT_USER_FOLDER_CLT_USER1_idx` (`USER_ID`),
  ADD KEY `fk_CLT_USER_FOLDER_CLT_FOLDER1_idx` (`MODULE_ID`);

--
-- Index pour la table `CLT_USER_STATUS`
--
ALTER TABLE `CLT_USER_STATUS`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `CTA_EMAIL_TYPE`
--
ALTER TABLE `CTA_EMAIL_TYPE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_CTA_EMAIL_TYPE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `CTA_ENTITY`
--
ALTER TABLE `CTA_ENTITY`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_PARTY_TYPE_ID_idx` (`PARTY_TYPE_ID`);

--
-- Index pour la table `CTA_ENTITY_EMAIL`
--
ALTER TABLE `CTA_ENTITY_EMAIL`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_PARTY_ID_idx` (`ENTITY_ID`),
  ADD KEY `FK2_EMAIL_TYPE_idx` (`EMAIL_TYPE_ID`);

--
-- Index pour la table `CTA_ENTITY_FAX`
--
ALTER TABLE `CTA_ENTITY_FAX`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_PARTY_ID_idx` (`ENTITY_ID`),
  ADD KEY `FK2_FAX_TYPE_idx` (`FAX_TYPE_ID`);

--
-- Index pour la table `CTA_ENTITY_LOCATION`
--
ALTER TABLE `CTA_ENTITY_LOCATION`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK44_PARTY_ID_idx` (`ENTITY_ID`),
  ADD KEY `FK1_LOCATION_TYPE_idx` (`LOCATION_TYPE_ID`),
  ADD KEY `FK2_CTA_ENTITY_LOCATION_idx` (`INF_COUNTRY_ID`);

--
-- Index pour la table `CTA_ENTITY_PHONE`
--
ALTER TABLE `CTA_ENTITY_PHONE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK9_PARTY_ID_idx` (`ENTITY_ID`),
  ADD KEY `FK1_PHONE_TYPE_idx` (`PHONE_TYPE_ID`);

--
-- Index pour la table `CTA_ENTITY_TYPE`
--
ALTER TABLE `CTA_ENTITY_TYPE`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `CTA_ENTITY_WEB`
--
ALTER TABLE `CTA_ENTITY_WEB`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK10_PARTY_ID_idx` (`ENTITY_ID`),
  ADD KEY `FK1_WEB_TYPE_idx` (`WEB_TYPE_ID`);

--
-- Index pour la table `CTA_FAX_TYPE`
--
ALTER TABLE `CTA_FAX_TYPE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_CTA_FAX_TYPE_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `CTA_LOCATION_TYPE`
--
ALTER TABLE `CTA_LOCATION_TYPE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_CTA_LOCATION_TYPE_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `CTA_PHONE_TYPE`
--
ALTER TABLE `CTA_PHONE_TYPE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_CTA_PHONE_TYPE_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `CTA_WEB_TYPE`
--
ALTER TABLE `CTA_WEB_TYPE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_CTA_WEB_TYPE_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `INF_BASIC_PARAMETER`
--
ALTER TABLE `INF_BASIC_PARAMETER`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_INF_BASIC_PARAMETER_idx` (`BASIC_PARAMETER_TYPE_ID`);

--
-- Index pour la table `INF_BASIC_PARAMETER_TYPE`
--
ALTER TABLE `INF_BASIC_PARAMETER_TYPE`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `INF_CITY`
--
ALTER TABLE `INF_CITY`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_INF_CITY_INF_COUNTRY1_idx` (`COUNTRY_ID`);

--
-- Index pour la table `INF_COUNTRY`
--
ALTER TABLE `INF_COUNTRY`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `INF_CURRENCY`
--
ALTER TABLE `INF_CURRENCY`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `INF_GROUP`
--
ALTER TABLE `INF_GROUP`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_INF_GROUP_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `INF_ITEM`
--
ALTER TABLE `INF_ITEM`
  ADD PRIMARY KEY (`CODE`);

--
-- Index pour la table `INF_LANGUAGE`
--
ALTER TABLE `INF_LANGUAGE`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `INF_LOVS`
--
ALTER TABLE `INF_LOVS`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_INF_LOVS_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `INF_MESSAGE`
--
ALTER TABLE `INF_MESSAGE`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `INF_MONTH`
--
ALTER TABLE `INF_MONTH`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `INF_PACKAGE`
--
ALTER TABLE `INF_PACKAGE`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `INF_PREFIX`
--
ALTER TABLE `INF_PREFIX`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `INF_PRIVILEGE`
--
ALTER TABLE `INF_PRIVILEGE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_INF_PRIVILEGE_INF_ITEM1_idx` (`ITEM_CODE`),
  ADD KEY `fk_INF_PRIVILEGE_INF_ROLE1_idx` (`ROLE_ID`);

--
-- Index pour la table `INF_ROLE`
--
ALTER TABLE `INF_ROLE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_INF_ROLE_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `INF_ROLE_GROUP`
--
ALTER TABLE `INF_ROLE_GROUP`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_INF_ROLE_GROUP_INF_GROUP_idx` (`GROUP_ID`),
  ADD KEY `fk_INF_ROLE_GROUP_INF_ROLE1_idx` (`ROLE_ID`);

--
-- Index pour la table `INF_TEXT`
--
ALTER TABLE `INF_TEXT`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_INF_TEXT_INF_PREFIX1_idx` (`PREFIX`),
  ADD KEY `fk_inf_text` (`ITEM_CODE`),
  ADD KEY `FK2_INF_TEXT_idx` (`TEXT_TYPE_ID`),
  ADD KEY `FK3_INF_TEXT_idx` (`LANGUAGE_ID`);

--
-- Index pour la table `INF_TEXT_TYPE`
--
ALTER TABLE `INF_TEXT_TYPE`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `MAIL_CATEGORY`
--
ALTER TABLE `MAIL_CATEGORY`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `MAIL_LOG`
--
ALTER TABLE `MAIL_LOG`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_MAIL_LOG_idx` (`MESSAGE_ID`),
  ADD KEY `FK2_MAIL_LOG_idx` (`LOG_STATUS_ID`);

--
-- Index pour la table `MAIL_LOG_STATUS`
--
ALTER TABLE `MAIL_LOG_STATUS`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `MAIL_MESSAGE`
--
ALTER TABLE `MAIL_MESSAGE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_MAIL_MESSAGE_idx` (`TYPE_ID`);

--
-- Index pour la table `MAIL_MESSAGE_CLIENT`
--
ALTER TABLE `MAIL_MESSAGE_CLIENT`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_MAIL_MESSAGE_CLINET_idx` (`MESSAGE_ID`);

--
-- Index pour la table `MAIL_MESSAGE_CLINET_RECIPIENT`
--
ALTER TABLE `MAIL_MESSAGE_CLINET_RECIPIENT`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_MAIL_MESSAGE_CLINET_RECIPIENT_idx` (`MESSAGE_ID`);

--
-- Index pour la table `MAIL_MESSAGE_MODEL`
--
ALTER TABLE `MAIL_MESSAGE_MODEL`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_MAIL_MESSAGE_MODEL_idx` (`PM_MODEL_ID`),
  ADD KEY `FK2_MAIL_MESSAGE_MODEL_idx` (`MESSAGE_ID`);

--
-- Index pour la table `MAIL_MESSAGE_MODULE`
--
ALTER TABLE `MAIL_MESSAGE_MODULE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_MAIL_MESSAGE_MODULE_idx` (`MESSAGE_ID`),
  ADD KEY `FK2_MAIL_MESSAGE_MODULE_idx` (`INF_MODULE_ID`);

--
-- Index pour la table `MAIL_MESSAGE_MODULE_RECIPIENT`
--
ALTER TABLE `MAIL_MESSAGE_MODULE_RECIPIENT`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_MAIL_MESSAGE_MODEL_idx_idx` (`MESSAGE_ID`),
  ADD KEY `FK2_MAIL_MESSAGE_MODEL_idx_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `MAIL_MESSAGE_PLACEHOLDER`
--
ALTER TABLE `MAIL_MESSAGE_PLACEHOLDER`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_MAIL_MESSAGE_PLACEHOLDER_idx` (`MESSAGE_ID`),
  ADD KEY `FK2_MAIL_MESSAGE_PLACEHOLDER_idx` (`MESSAGE_PLACEHOLDER_ID`);

--
-- Index pour la table `MAIL_MESSAGE_SCHEDULE`
--
ALTER TABLE `MAIL_MESSAGE_SCHEDULE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_MAIL_MESSAGE_SCHEDULE_idx` (`MESSAGE_ID`);

--
-- Index pour la table `MAIL_PLACEHOLDER`
--
ALTER TABLE `MAIL_PLACEHOLDER`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `MAIL_TYPE`
--
ALTER TABLE `MAIL_TYPE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_MAIL_TYPE_idx` (`CATEGORY_ID`);

--
-- Index pour la table `MIG_INF_CITY`
--
ALTER TABLE `MIG_INF_CITY`
  ADD UNIQUE KEY `name` (`name`);

--
-- Index pour la table `PM_ATTRIBUTE_VALIDATION`
--
ALTER TABLE `PM_ATTRIBUTE_VALIDATION`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_PM_VALID_SIMPLE_PAGE_PM_VALID_SIMPLE1_idx` (`VALIDATION_TYPE_ID`),
  ADD KEY `fk1_pm_attribute_validation` (`MODEL_ID`),
  ADD KEY `FK2_PM_ATTRIBUTE_VALIDATION_idx` (`PAGE_ATTRIBUTE_ID`);

--
-- Index pour la table `PM_CATEGORY`
--
ALTER TABLE `PM_CATEGORY`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_PM_MENU_PM_MENU_TYPE1_idx` (`CATEGORY_TYPE_ID`);

--
-- Index pour la table `PM_CATEGORY_TYPE`
--
ALTER TABLE `PM_CATEGORY_TYPE`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `PM_COMPONENT`
--
ALTER TABLE `PM_COMPONENT`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `PM_COMPOSITION`
--
ALTER TABLE `PM_COMPOSITION`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_PM_COMPOSITION_PM_MENU1_idx` (`MENU_ID`),
  ADD KEY `fk_PM_COMPOSITION_PM_PAGE1_idx` (`PAGE_ID`),
  ADD KEY `fk1_pm_composition` (`MODEL_ID`),
  ADD KEY `FK2_PM_COMPOSITION` (`GROUP_ID`),
  ADD KEY `FK3_PM_COMPOSITION` (`CATEGORY_ID`);

--
-- Index pour la table `PM_DATA_TYPE`
--
ALTER TABLE `PM_DATA_TYPE`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `PM_FORMAT_TYPE`
--
ALTER TABLE `PM_FORMAT_TYPE`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `PM_GROUP`
--
ALTER TABLE `PM_GROUP`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_PM_GROUP_PM_GROUP_TYPE1_idx` (`GROUP_TYPE_ID`);

--
-- Index pour la table `PM_GROUP_TYPE`
--
ALTER TABLE `PM_GROUP_TYPE`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `PM_MENU`
--
ALTER TABLE `PM_MENU`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_PM_SECTION_PM_SECTION_TYPE1_idx` (`MENU_TYPE_ID`);

--
-- Index pour la table `PM_MENU_TYPE`
--
ALTER TABLE `PM_MENU_TYPE`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `PM_MODEL`
--
ALTER TABLE `PM_MODEL`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk1_pm_model_idx` (`INF_PACKAGE_ID`);

--
-- Index pour la table `PM_MODEL_STATUS`
--
ALTER TABLE `PM_MODEL_STATUS`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `PM_PAGE`
--
ALTER TABLE `PM_PAGE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_PM_PAGE_PM_TYPE_PAGE1_idx` (`PAGE_TYPE_ID`);

--
-- Index pour la table `PM_PAGE_ATTRIBUTE`
--
ALTER TABLE `PM_PAGE_ATTRIBUTE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_PM_PAGE_ITEM_PM_PAGE1_idx` (`PAGE_ID`),
  ADD KEY `fk_PM_PAGE_ITEM_INF_ITEM1_idx` (`INF_ITEM_CODE`),
  ADD KEY `fk_PM_PAGE_ITEM_PM_COMPONENTE1_idx` (`PM_COMPONENT_ID`),
  ADD KEY `fk_data_type1_idx` (`DATA_TYPE_ID`),
  ADD KEY `fk_foramt_type1_idx` (`FORMAT_TYPE_ID`);

--
-- Index pour la table `PM_PAGE_ATTRIBUTE_MODEL`
--
ALTER TABLE `PM_PAGE_ATTRIBUTE_MODEL`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `idx_pm_page_attribute_model` (`PAGE_ATTRIBUTE_ID`),
  ADD KEY `FK2_PM_PAGE_ATTRIBUTE_MODEL_idx` (`MODEL_ID`),
  ADD KEY `FK3_PM_PAGE_ATTRIBUTE_MODEL_idx` (`DATA_TYPE_ID`),
  ADD KEY `FK4_PM_PAGE_ATTRIBUTE_MODEL_idx` (`FORMAT_TYPE_ID`);

--
-- Index pour la table `PM_PAGE_ATTRIBUTE_MODULE`
--
ALTER TABLE `PM_PAGE_ATTRIBUTE_MODULE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `idx_pm_page_attribute_module` (`PAGE_ATTRIBUTE_ID`);

--
-- Index pour la table `PM_PAGE_PARAMETER`
--
ALTER TABLE `PM_PAGE_PARAMETER`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_PAGE_PAPRAMETER_idx` (`PAGE_PARAMETER_TYPE_ID`),
  ADD KEY `idx_pm_page_parameter` (`PAGE_ID`);

--
-- Index pour la table `PM_PAGE_PARAMETER_MODEL`
--
ALTER TABLE `PM_PAGE_PARAMETER_MODEL`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `PM_PAGE_PARAMETER_MODULE`
--
ALTER TABLE `PM_PAGE_PARAMETER_MODULE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `idx_pm_page_parameter_module` (`PAGE_PARAMETER_ID`);

--
-- Index pour la table `PM_PAGE_PARAMETER_TYPE`
--
ALTER TABLE `PM_PAGE_PARAMETER_TYPE`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `PM_PAGE_TYPE`
--
ALTER TABLE `PM_PAGE_TYPE`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `PM_VALIDATION_TYPE`
--
ALTER TABLE `PM_VALIDATION_TYPE`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `SM_ADVANCED`
--
ALTER TABLE `SM_ADVANCED`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_SM_ADVANCED_SM_ADVANCED_STATUS1_idx` (`ADVANCED_STATUS_ID`),
  ADD KEY `fk_SM_ADVANCED_SM_CUSTOMER1_idx` (`CUSTOMER_ID`),
  ADD KEY `fk_SM_ADVANCED_SM_PRODUCT1_idx` (`PRODUCT_ID`);

--
-- Index pour la table `SM_ADVANCED_STATUS`
--
ALTER TABLE `SM_ADVANCED_STATUS`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `SM_BANK_TYPE`
--
ALTER TABLE `SM_BANK_TYPE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_SM_BANK_TYPE_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `SM_CHECK`
--
ALTER TABLE `SM_CHECK`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `pk_sm_check_0` (`BANK_ID`);

--
-- Index pour la table `SM_CUSTOMER`
--
ALTER TABLE `SM_CUSTOMER`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_SM_CUSTOMER_CLT_MODULE1_idx` (`CLT_MODULE_ID`),
  ADD KEY `idx_sm_customer` (`CUSTOMER_TYPE_ID`),
  ADD KEY `FK3_SM_CUSTOMER_idx` (`ENTITY_ID`),
  ADD KEY `FK4_SM_CUSTOMER` (`CUSTOMER_CATEGORY_ID`);

--
-- Index pour la table `SM_CUSTOMER_CATEGORY`
--
ALTER TABLE `SM_CUSTOMER_CATEGORY`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_SM_CUSTOMER_CATEGORY_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `SM_CUSTOMER_TYPE`
--
ALTER TABLE `SM_CUSTOMER_TYPE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_SM_CUSTOMER_TYPE_CLT_FOLDER1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `SM_DEPOSIT`
--
ALTER TABLE `SM_DEPOSIT`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_SM_DEPOSIT_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `SM_EXPENSE`
--
ALTER TABLE `SM_EXPENSE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_SM_EXPENSE_SM_EXPENSE_TYPE1_idx` (`EXPENSE_TYPE_ID`),
  ADD KEY `fk_SM_EXPENSE_CLT_FOLDER1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `SM_EXPENSE_TYPE`
--
ALTER TABLE `SM_EXPENSE_TYPE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_SM_EXPENSE_TYPE_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `SM_JUSTIFICATION_STATUS`
--
ALTER TABLE `SM_JUSTIFICATION_STATUS`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1SM_JUSTIFICATION_STATUS_idx` (`CLT_MODULE_ID`),
  ADD KEY `FK2_SM_JUSTIFICATION_STATUS_idx` (`JUSTIFICATION_STATUS_CATEGORY_ID`);

--
-- Index pour la table `SM_JUSTIFICATION_STATUS_CATEGORY`
--
ALTER TABLE `SM_JUSTIFICATION_STATUS_CATEGORY`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `SM_ORDER`
--
ALTER TABLE `SM_ORDER`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_SM_ORDER_SM_ORDER_STATUS1_idx` (`ORDER_STATUS_ID`),
  ADD KEY `fk_SM_ORDER_SM_PAYMENT_METHOD1_idx` (`PAYMENT_METHOD_ID`),
  ADD KEY `fk_SM_ORDER_SM_CUSTOMER1_idx` (`CUSTOMER_ID`),
  ADD KEY `fk_SM_ORDER_SM_CHECK1_idx` (`CHECK_ID`),
  ADD KEY `FK5_SM_ORDER` (`CLT_MODULE_ID`);

--
-- Index pour la table `SM_ORDER_LINE`
--
ALTER TABLE `SM_ORDER_LINE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_SM_ORDER_LINE_SM_PRODUCT1_idx` (`PRODUCT_ID`),
  ADD KEY `fk_SM_ORDER_LINE_SM_ORDER1_idx` (`ORDER_ID`),
  ADD KEY `FK3_SM_ORDER_LINE_idx` (`PROMOTION_ID`);

--
-- Index pour la table `SM_ORDER_STATUS`
--
ALTER TABLE `SM_ORDER_STATUS`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `SM_ORDER_STATUS_LOG`
--
ALTER TABLE `SM_ORDER_STATUS_LOG`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_SM_ORDER_STATUS_LOG` (`ORDER_ID`),
  ADD KEY `FK2_SM_ORDER_STATUS_LOG` (`ORDER_STATUS_ID`),
  ADD KEY `FK3_SM_ORDER_STATUS_LOG` (`JUSTIFICATION_STATUS_ID`);

--
-- Index pour la table `SM_ORDER_SUPPLIER`
--
ALTER TABLE `SM_ORDER_SUPPLIER`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk1_sm_order_supplier_idx` (`ORDER_SUPPLIER_STATUS_ID`),
  ADD KEY `FK2_sm_order_supplier` (`SUPPLIER_ID`),
  ADD KEY `fk3_sm_order_supplier` (`CLT_MODULE_ID`);

--
-- Index pour la table `SM_ORDER_SUPPLIER_LINE`
--
ALTER TABLE `SM_ORDER_SUPPLIER_LINE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk1_sm_order_supplier_line_idx` (`ORDER_SUPPLIER_ID`),
  ADD KEY `fk2_sm_order_supplier_line_idx` (`PRODUCT_ID`),
  ADD KEY `FK3_SM_ORDER_SUPPLIER_LINE_idx` (`PROMOTION_ID`);

--
-- Index pour la table `SM_ORDER_SUPPLIER_STATUS`
--
ALTER TABLE `SM_ORDER_SUPPLIER_STATUS`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `SM_ORDER_SUPPLIER_STATUS_LOG`
--
ALTER TABLE `SM_ORDER_SUPPLIER_STATUS_LOG`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_SM_ORDER_SUPPLIER_STATUS_LOG` (`ORDER_SUPPLIER_ID`),
  ADD KEY `FK2_SM_ORDER_SUPPLIER_STATUS_LOG` (`ORDER_SUPPLIER_STATUS_ID`),
  ADD KEY `FK3_SM_ORDER_SUPPLIER_STATUS_LOG` (`JUSTIFICATION_STATUS_ID`);

--
-- Index pour la table `SM_PAYMENT_METHOD`
--
ALTER TABLE `SM_PAYMENT_METHOD`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_SM_PAYMENT_METHOD_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `SM_PRODUCT`
--
ALTER TABLE `SM_PRODUCT`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_SM_PRODUCT_SM_PRODUCT_STATUS1_idx` (`PRODUCT_STATUS_ID`),
  ADD KEY `fk_SM_PRODUCT_SM_PRODUCT_FAMILY1_idx` (`PRODUCT_FAMILY_ID`),
  ADD KEY `fk_SM_PRODUCT_SM_PRODUCT_SIZE1_idx` (`PRODUCT_SIZE_ID`),
  ADD KEY `fk_SM_PRODUCT_SM_PRODUCT_COLOR1_idx` (`PRODUCT_COLOR_ID`),
  ADD KEY `FK5_SM_PRODUCT` (`PRODUCT_GROUP_ID`),
  ADD KEY `FK6_SM_PRODUCT` (`PRODUCT_TYPE_ID`),
  ADD KEY `FK7_SM_PRODUCT` (`CLT_MODULE_ID`),
  ADD KEY `FK8_SM_PRODUCT` (`PRODUCT_UNIT_ID`),
  ADD KEY `FK9_SM_PRODUCT` (`PRODUCT_DEPARTMENT_ID`);

--
-- Index pour la table `SM_PRODUCT_COLOR`
--
ALTER TABLE `SM_PRODUCT_COLOR`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_SM_PRODUCT_COLOR_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `SM_PRODUCT_DEPARTMENT`
--
ALTER TABLE `SM_PRODUCT_DEPARTMENT`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_SM_PRODUCT_DEPARTMENT_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `SM_PRODUCT_FAMILY`
--
ALTER TABLE `SM_PRODUCT_FAMILY`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_SM_PRODUCT_FAMILY_SM_PRODUCT_GROUP1_idx` (`PRODUCT_GROUP_ID`),
  ADD KEY `fk_SM_PRODUCT_FAMILY_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `SM_PRODUCT_GROUP`
--
ALTER TABLE `SM_PRODUCT_GROUP`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_SM_PRODUCT_GROUP_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `SM_PRODUCT_SIZE`
--
ALTER TABLE `SM_PRODUCT_SIZE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_SM_PRODUCT_SIZE_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `SM_PRODUCT_STATUS`
--
ALTER TABLE `SM_PRODUCT_STATUS`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `SM_PRODUCT_TYPE`
--
ALTER TABLE `SM_PRODUCT_TYPE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_SM_PRODUCT_TYPE_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `SM_PRODUCT_UNIT`
--
ALTER TABLE `SM_PRODUCT_UNIT`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_SM_PRODUCT_UNIT_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `SM_PROMOTION`
--
ALTER TABLE `SM_PROMOTION`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_SM_PROMOTION_SM_PROMOTION_TYPE1_idx` (`PROMOTION_TYPE_ID`),
  ADD KEY `fk_SM_PROMOTION_SM_PRODUCT1_idx` (`PRODUCT`);

--
-- Index pour la table `SM_PROMOTION_TYPE`
--
ALTER TABLE `SM_PROMOTION_TYPE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_SM_PROMOTION_TYPE_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `SM_RECEPTION`
--
ALTER TABLE `SM_RECEPTION`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_SPM_RECEPTION_STATUS_idx` (`RECEPTION_STATUS_ID`),
  ADD KEY `fk1_sm_reception_idx` (`SUPPLIER_ID`),
  ADD KEY `fk3_sm_reception_idx` (`ORDER_SUPPLIER_ID`),
  ADD KEY `FK4_SM_RECEPTION` (`DEPOSIT_ID`),
  ADD KEY `FK5_SM_RECEPTION` (`CLT_MODULE_ID`);

--
-- Index pour la table `SM_RECEPTION_LINE`
--
ALTER TABLE `SM_RECEPTION_LINE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk2_sm_reception_line_idx` (`RECEPTION_ID`),
  ADD KEY `fk1_sm_reception_line_idx` (`PRODUCT_ID`);

--
-- Index pour la table `SM_RECEPTION_PRODUCTS`
--
ALTER TABLE `SM_RECEPTION_PRODUCTS`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk1_reception_id_idx` (`RECEPTION_ID`);

--
-- Index pour la table `SM_RECEPTION_STATUS`
--
ALTER TABLE `SM_RECEPTION_STATUS`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `SM_RECEPTION_STATUS_LOG`
--
ALTER TABLE `SM_RECEPTION_STATUS_LOG`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_SM_RECEPTION_STATUS_LOG` (`RECEPTION_ID`),
  ADD KEY `FK2_SM_RECEPTION_STATUS_LOG` (`RECEPTION_STATUS_ID`),
  ADD KEY `FK3_SM_RECEPTION_STATUS_LOG` (`JUSTIFICATION_STATUS_ID`);

--
-- Index pour la table `SM_RETURN_RECEIPT`
--
ALTER TABLE `SM_RETURN_RECEIPT`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk1_sm_return_receipt_idx` (`RETURN_RECEIPT_STATUS_ID`),
  ADD KEY `fk2_sm_return_receipt_idx` (`ORDER_ID`),
  ADD KEY `FK3_SM_RETURN_RECEIPT` (`CUSTOMER_ID`),
  ADD KEY `FK4_SM_RETURN_RECEIPT` (`CLT_MODULE_ID`);

--
-- Index pour la table `SM_RETURN_RECEIPT_LINE`
--
ALTER TABLE `SM_RETURN_RECEIPT_LINE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk1_ sm_return_receipt_line_idx` (`RETURN_RECEIPT_ID`),
  ADD KEY `fk2_ sm_return_receipt_line_idx` (`PRODUCT_ID`),
  ADD KEY `FK3_SM_RETURN_RECEIPT_LINE_idx` (`PROMOTION_ID`);

--
-- Index pour la table `SM_RETURN_RECEIPT_STATUS`
--
ALTER TABLE `SM_RETURN_RECEIPT_STATUS`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `SM_RETURN_RECEIPT_STATUS_LOG`
--
ALTER TABLE `SM_RETURN_RECEIPT_STATUS_LOG`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_SM_RETURN_RECEIPT_STATUS_LOG` (`RETURN_RECEIPT_ID`),
  ADD KEY `FK2_SM_RETURN_RECEIPT_STATUS_LOG` (`RETURN_RECEIPT_STATUS_ID`),
  ADD KEY `FK3_SM_RETURN_RECEIPT_STATUS_LOG` (`JUSTIFICATION_STATUS_ID`);

--
-- Index pour la table `SM_SUPPLIER`
--
ALTER TABLE `SM_SUPPLIER`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_SM_SUPPLIER_SM_SUPPLIER_TYPE1_idx` (`SUPPLIER_TYPE_ID`),
  ADD KEY `FK2_SUPPLIER_idx` (`CLT_MODULE_ID`),
  ADD KEY `FK3_SUPPLIER_idx` (`ENTITY_ID`),
  ADD KEY `FK4_SM_SUPPLIER` (`SUPPLIER_CATEGORY_ID`);

--
-- Index pour la table `SM_SUPPLIER_CATEGORY`
--
ALTER TABLE `SM_SUPPLIER_CATEGORY`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK1_SM_SUPPLIER_CATEGORY_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `SM_SUPPLIER_TYPE`
--
ALTER TABLE `SM_SUPPLIER_TYPE`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `fk_SM_SUPPLIER_TYPE_CLT_FOLDER1_idx` (`CLT_MODULE_ID`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `CLT_CLIENT`
--
ALTER TABLE `CLT_CLIENT`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `CLT_CLIENT_LANGUAGE`
--
ALTER TABLE `CLT_CLIENT_LANGUAGE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `CLT_CLIENT_STATUS`
--
ALTER TABLE `CLT_CLIENT_STATUS`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `CLT_MODEL_PARAMETER`
--
ALTER TABLE `CLT_MODEL_PARAMETER`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `CLT_MODEL_PARAMETER_MODEL`
--
ALTER TABLE `CLT_MODEL_PARAMETER_MODEL`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `CLT_MODEL_PARAMETER_MODULE`
--
ALTER TABLE `CLT_MODEL_PARAMETER_MODULE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `CLT_MODEL_PARAMETER_TYPE`
--
ALTER TABLE `CLT_MODEL_PARAMETER_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `CLT_MODULE`
--
ALTER TABLE `CLT_MODULE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `CLT_MODULE_STATUS`
--
ALTER TABLE `CLT_MODULE_STATUS`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `CLT_MODULE_TYPE`
--
ALTER TABLE `CLT_MODULE_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `CLT_PARAMETER`
--
ALTER TABLE `CLT_PARAMETER`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `CLT_PARAMETER_CLIENT`
--
ALTER TABLE `CLT_PARAMETER_CLIENT`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `CLT_STRUCTURE`
--
ALTER TABLE `CLT_STRUCTURE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `CLT_STRUCTURE_ROLE`
--
ALTER TABLE `CLT_STRUCTURE_ROLE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `CLT_STRUCTURE_TYPE`
--
ALTER TABLE `CLT_STRUCTURE_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `CLT_USER`
--
ALTER TABLE `CLT_USER`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `CLT_USER_CATEGORY`
--
ALTER TABLE `CLT_USER_CATEGORY`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `CLT_USER_CLIENT`
--
ALTER TABLE `CLT_USER_CLIENT`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `CLT_USER_GROUP`
--
ALTER TABLE `CLT_USER_GROUP`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `CLT_USER_MODULE`
--
ALTER TABLE `CLT_USER_MODULE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT pour la table `CLT_USER_STATUS`
--
ALTER TABLE `CLT_USER_STATUS`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `CTA_EMAIL_TYPE`
--
ALTER TABLE `CTA_EMAIL_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `CTA_ENTITY`
--
ALTER TABLE `CTA_ENTITY`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT pour la table `CTA_ENTITY_EMAIL`
--
ALTER TABLE `CTA_ENTITY_EMAIL`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `CTA_ENTITY_FAX`
--
ALTER TABLE `CTA_ENTITY_FAX`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `CTA_ENTITY_LOCATION`
--
ALTER TABLE `CTA_ENTITY_LOCATION`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `CTA_ENTITY_PHONE`
--
ALTER TABLE `CTA_ENTITY_PHONE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `CTA_ENTITY_TYPE`
--
ALTER TABLE `CTA_ENTITY_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `CTA_ENTITY_WEB`
--
ALTER TABLE `CTA_ENTITY_WEB`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `CTA_FAX_TYPE`
--
ALTER TABLE `CTA_FAX_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `CTA_LOCATION_TYPE`
--
ALTER TABLE `CTA_LOCATION_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `CTA_PHONE_TYPE`
--
ALTER TABLE `CTA_PHONE_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `CTA_WEB_TYPE`
--
ALTER TABLE `CTA_WEB_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `INF_BASIC_PARAMETER`
--
ALTER TABLE `INF_BASIC_PARAMETER`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `INF_BASIC_PARAMETER_TYPE`
--
ALTER TABLE `INF_BASIC_PARAMETER_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `INF_CITY`
--
ALTER TABLE `INF_CITY`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=133;
--
-- AUTO_INCREMENT pour la table `INF_COUNTRY`
--
ALTER TABLE `INF_COUNTRY`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `INF_CURRENCY`
--
ALTER TABLE `INF_CURRENCY`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `INF_GROUP`
--
ALTER TABLE `INF_GROUP`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `INF_LANGUAGE`
--
ALTER TABLE `INF_LANGUAGE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `INF_LOVS`
--
ALTER TABLE `INF_LOVS`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `INF_MESSAGE`
--
ALTER TABLE `INF_MESSAGE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `INF_MONTH`
--
ALTER TABLE `INF_MONTH`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `INF_PREFIX`
--
ALTER TABLE `INF_PREFIX`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `INF_PRIVILEGE`
--
ALTER TABLE `INF_PRIVILEGE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `INF_ROLE`
--
ALTER TABLE `INF_ROLE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `INF_ROLE_GROUP`
--
ALTER TABLE `INF_ROLE_GROUP`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `INF_TEXT`
--
ALTER TABLE `INF_TEXT`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1080;
--
-- AUTO_INCREMENT pour la table `INF_TEXT_TYPE`
--
ALTER TABLE `INF_TEXT_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `MAIL_CATEGORY`
--
ALTER TABLE `MAIL_CATEGORY`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `MAIL_LOG`
--
ALTER TABLE `MAIL_LOG`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `MAIL_LOG_STATUS`
--
ALTER TABLE `MAIL_LOG_STATUS`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `MAIL_MESSAGE`
--
ALTER TABLE `MAIL_MESSAGE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `MAIL_MESSAGE_CLIENT`
--
ALTER TABLE `MAIL_MESSAGE_CLIENT`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `MAIL_MESSAGE_CLINET_RECIPIENT`
--
ALTER TABLE `MAIL_MESSAGE_CLINET_RECIPIENT`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `MAIL_MESSAGE_MODEL`
--
ALTER TABLE `MAIL_MESSAGE_MODEL`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `MAIL_MESSAGE_MODULE`
--
ALTER TABLE `MAIL_MESSAGE_MODULE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `MAIL_MESSAGE_MODULE_RECIPIENT`
--
ALTER TABLE `MAIL_MESSAGE_MODULE_RECIPIENT`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `MAIL_MESSAGE_PLACEHOLDER`
--
ALTER TABLE `MAIL_MESSAGE_PLACEHOLDER`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `MAIL_MESSAGE_SCHEDULE`
--
ALTER TABLE `MAIL_MESSAGE_SCHEDULE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `MAIL_PLACEHOLDER`
--
ALTER TABLE `MAIL_PLACEHOLDER`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `MAIL_TYPE`
--
ALTER TABLE `MAIL_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `PM_ATTRIBUTE_VALIDATION`
--
ALTER TABLE `PM_ATTRIBUTE_VALIDATION`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `PM_CATEGORY`
--
ALTER TABLE `PM_CATEGORY`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT pour la table `PM_CATEGORY_TYPE`
--
ALTER TABLE `PM_CATEGORY_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `PM_COMPONENT`
--
ALTER TABLE `PM_COMPONENT`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `PM_COMPOSITION`
--
ALTER TABLE `PM_COMPOSITION`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=221;
--
-- AUTO_INCREMENT pour la table `PM_DATA_TYPE`
--
ALTER TABLE `PM_DATA_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `PM_FORMAT_TYPE`
--
ALTER TABLE `PM_FORMAT_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `PM_GROUP`
--
ALTER TABLE `PM_GROUP`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `PM_GROUP_TYPE`
--
ALTER TABLE `PM_GROUP_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `PM_MENU`
--
ALTER TABLE `PM_MENU`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT pour la table `PM_MENU_TYPE`
--
ALTER TABLE `PM_MENU_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `PM_MODEL`
--
ALTER TABLE `PM_MODEL`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `PM_MODEL_STATUS`
--
ALTER TABLE `PM_MODEL_STATUS`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `PM_PAGE`
--
ALTER TABLE `PM_PAGE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1009;
--
-- AUTO_INCREMENT pour la table `PM_PAGE_ATTRIBUTE`
--
ALTER TABLE `PM_PAGE_ATTRIBUTE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=143;
--
-- AUTO_INCREMENT pour la table `PM_PAGE_ATTRIBUTE_MODEL`
--
ALTER TABLE `PM_PAGE_ATTRIBUTE_MODEL`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `PM_PAGE_ATTRIBUTE_MODULE`
--
ALTER TABLE `PM_PAGE_ATTRIBUTE_MODULE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `PM_PAGE_PARAMETER`
--
ALTER TABLE `PM_PAGE_PARAMETER`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `PM_PAGE_PARAMETER_MODEL`
--
ALTER TABLE `PM_PAGE_PARAMETER_MODEL`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `PM_PAGE_PARAMETER_MODULE`
--
ALTER TABLE `PM_PAGE_PARAMETER_MODULE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `PM_PAGE_PARAMETER_TYPE`
--
ALTER TABLE `PM_PAGE_PARAMETER_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `PM_PAGE_TYPE`
--
ALTER TABLE `PM_PAGE_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `PM_VALIDATION_TYPE`
--
ALTER TABLE `PM_VALIDATION_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `SM_ADVANCED`
--
ALTER TABLE `SM_ADVANCED`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `SM_ADVANCED_STATUS`
--
ALTER TABLE `SM_ADVANCED_STATUS`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `SM_BANK_TYPE`
--
ALTER TABLE `SM_BANK_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `SM_CHECK`
--
ALTER TABLE `SM_CHECK`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `SM_CUSTOMER`
--
ALTER TABLE `SM_CUSTOMER`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT pour la table `SM_CUSTOMER_CATEGORY`
--
ALTER TABLE `SM_CUSTOMER_CATEGORY`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `SM_CUSTOMER_TYPE`
--
ALTER TABLE `SM_CUSTOMER_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `SM_DEPOSIT`
--
ALTER TABLE `SM_DEPOSIT`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `SM_EXPENSE`
--
ALTER TABLE `SM_EXPENSE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT pour la table `SM_EXPENSE_TYPE`
--
ALTER TABLE `SM_EXPENSE_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT pour la table `SM_JUSTIFICATION_STATUS`
--
ALTER TABLE `SM_JUSTIFICATION_STATUS`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `SM_JUSTIFICATION_STATUS_CATEGORY`
--
ALTER TABLE `SM_JUSTIFICATION_STATUS_CATEGORY`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `SM_ORDER`
--
ALTER TABLE `SM_ORDER`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT pour la table `SM_ORDER_LINE`
--
ALTER TABLE `SM_ORDER_LINE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT pour la table `SM_ORDER_STATUS`
--
ALTER TABLE `SM_ORDER_STATUS`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `SM_ORDER_STATUS_LOG`
--
ALTER TABLE `SM_ORDER_STATUS_LOG`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `SM_ORDER_SUPPLIER`
--
ALTER TABLE `SM_ORDER_SUPPLIER`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT pour la table `SM_ORDER_SUPPLIER_LINE`
--
ALTER TABLE `SM_ORDER_SUPPLIER_LINE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT pour la table `SM_ORDER_SUPPLIER_STATUS`
--
ALTER TABLE `SM_ORDER_SUPPLIER_STATUS`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `SM_ORDER_SUPPLIER_STATUS_LOG`
--
ALTER TABLE `SM_ORDER_SUPPLIER_STATUS_LOG`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `SM_PAYMENT_METHOD`
--
ALTER TABLE `SM_PAYMENT_METHOD`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `SM_PRODUCT`
--
ALTER TABLE `SM_PRODUCT`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=328;
--
-- AUTO_INCREMENT pour la table `SM_PRODUCT_COLOR`
--
ALTER TABLE `SM_PRODUCT_COLOR`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `SM_PRODUCT_DEPARTMENT`
--
ALTER TABLE `SM_PRODUCT_DEPARTMENT`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `SM_PRODUCT_FAMILY`
--
ALTER TABLE `SM_PRODUCT_FAMILY`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `SM_PRODUCT_GROUP`
--
ALTER TABLE `SM_PRODUCT_GROUP`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `SM_PRODUCT_SIZE`
--
ALTER TABLE `SM_PRODUCT_SIZE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT pour la table `SM_PRODUCT_STATUS`
--
ALTER TABLE `SM_PRODUCT_STATUS`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `SM_PRODUCT_TYPE`
--
ALTER TABLE `SM_PRODUCT_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `SM_PRODUCT_UNIT`
--
ALTER TABLE `SM_PRODUCT_UNIT`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `SM_PROMOTION`
--
ALTER TABLE `SM_PROMOTION`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `SM_PROMOTION_TYPE`
--
ALTER TABLE `SM_PROMOTION_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `SM_RECEPTION`
--
ALTER TABLE `SM_RECEPTION`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT pour la table `SM_RECEPTION_LINE`
--
ALTER TABLE `SM_RECEPTION_LINE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT pour la table `SM_RECEPTION_PRODUCTS`
--
ALTER TABLE `SM_RECEPTION_PRODUCTS`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `SM_RECEPTION_STATUS`
--
ALTER TABLE `SM_RECEPTION_STATUS`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `SM_RECEPTION_STATUS_LOG`
--
ALTER TABLE `SM_RECEPTION_STATUS_LOG`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `SM_RETURN_RECEIPT`
--
ALTER TABLE `SM_RETURN_RECEIPT`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `SM_RETURN_RECEIPT_LINE`
--
ALTER TABLE `SM_RETURN_RECEIPT_LINE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `SM_RETURN_RECEIPT_STATUS`
--
ALTER TABLE `SM_RETURN_RECEIPT_STATUS`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `SM_RETURN_RECEIPT_STATUS_LOG`
--
ALTER TABLE `SM_RETURN_RECEIPT_STATUS_LOG`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `SM_SUPPLIER`
--
ALTER TABLE `SM_SUPPLIER`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT pour la table `SM_SUPPLIER_CATEGORY`
--
ALTER TABLE `SM_SUPPLIER_CATEGORY`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `SM_SUPPLIER_TYPE`
--
ALTER TABLE `SM_SUPPLIER_TYPE`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `CLT_CLIENT`
--
ALTER TABLE `CLT_CLIENT`
  ADD CONSTRAINT `FK1_CLT_CLIENT` FOREIGN KEY (`CLIENT_STATUS_ID`) REFERENCES `clt_client_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK4_CLT_CLIENT` FOREIGN KEY (`ENTITY_ID`) REFERENCES `CTA_ENTITY` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `CLT_CLIENT_LANGUAGE`
--
ALTER TABLE `CLT_CLIENT_LANGUAGE`
  ADD CONSTRAINT `FK2_CLT_CLIENT_LANGUAGE` FOREIGN KEY (`INF_PREFIX_ID`) REFERENCES `inf_prefix` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3_CLT_CLIENT_LANGUAGE` FOREIGN KEY (`INF_LANGUAGE_ID`) REFERENCES `inf_language` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_CLT_CLIENT_LANGUAGE_CLT_CLIENT1` FOREIGN KEY (`CLIENT_ID`) REFERENCES `clt_client` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CLT_MODEL_PARAMETER`
--
ALTER TABLE `CLT_MODEL_PARAMETER`
  ADD CONSTRAINT `FK1_CLT_MODEL_PARAMETER` FOREIGN KEY (`MODEL_PARAMETER_TYPE_ID`) REFERENCES `CLT_MODEL_PARAMETER_TYPE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CLT_MODEL_PARAMETER_MODEL`
--
ALTER TABLE `CLT_MODEL_PARAMETER_MODEL`
  ADD CONSTRAINT `FK1_CLT_MODEL_PARAMETER_MODEL` FOREIGN KEY (`MODEL_PARAMETER_ID`) REFERENCES `CLT_MODEL_PARAMETER` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_CLT_MODEL_PARAMETER_MODEL` FOREIGN KEY (`PM_MODEL_ID`) REFERENCES `PM_MODEL` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CLT_MODEL_PARAMETER_MODULE`
--
ALTER TABLE `CLT_MODEL_PARAMETER_MODULE`
  ADD CONSTRAINT `FK1_CLT_MODEL_PARAMETER_MODULE` FOREIGN KEY (`MODEL_PARAMETER_ID`) REFERENCES `clt_model_parameter` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_CLT_MODEL_PARAMETER_MODULE` FOREIGN KEY (`MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CLT_MODULE`
--
ALTER TABLE `CLT_MODULE`
  ADD CONSTRAINT `FK2_CLT_MODULE` FOREIGN KEY (`MODULE_STATUS_ID`) REFERENCES `clt_module_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3_CLT_MODULE` FOREIGN KEY (`MODULE_TYPE_ID`) REFERENCES `clt_module_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_CLT_MODULE_CLT_CLIENT1` FOREIGN KEY (`CLIENT_ID`) REFERENCES `clt_client` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CLT_PARAMETER`
--
ALTER TABLE `CLT_PARAMETER`
  ADD CONSTRAINT `FK1_CLT_PARAMETER` FOREIGN KEY (`PARAMETER_TYPE_ID`) REFERENCES `clt_parameter_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CLT_PARAMETER_CLIENT`
--
ALTER TABLE `CLT_PARAMETER_CLIENT`
  ADD CONSTRAINT `FK1_CLT_PARAMETER_CLIENT` FOREIGN KEY (`PARAMETER_ID`) REFERENCES `clt_parameter` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_clt_parameter_client_0` FOREIGN KEY (`CLINET_ID`) REFERENCES `clt_client` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CLT_USER`
--
ALTER TABLE `CLT_USER`
  ADD CONSTRAINT `FK1_CLT_USER` FOREIGN KEY (`USER_STATUS_ID`) REFERENCES `clt_user_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_CLT_USER` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `clt_user_category` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3_CLT_USER` FOREIGN KEY (`CLIENT_ID`) REFERENCES `clt_client` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK6_CLT_USER` FOREIGN KEY (`ENTITY_ID`) REFERENCES `CTA_ENTITY` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `CLT_USER_CATEGORY`
--
ALTER TABLE `CLT_USER_CATEGORY`
  ADD CONSTRAINT `FK1_CLT_USER_CATEGORY` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CLT_USER_CLIENT`
--
ALTER TABLE `CLT_USER_CLIENT`
  ADD CONSTRAINT `FK1_USER_CLIENT` FOREIGN KEY (`CLIENT_ID`) REFERENCES `clt_client` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_USER_CLIENT` FOREIGN KEY (`USER_ID`) REFERENCES `clt_user` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CLT_USER_GROUP`
--
ALTER TABLE `CLT_USER_GROUP`
  ADD CONSTRAINT `FK1_CLT_USER_GROUP` FOREIGN KEY (`INF_GROUP_ID`) REFERENCES `inf_group` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_CLT_USER_GROUP_CLT_USER1` FOREIGN KEY (`USER_ID`) REFERENCES `clt_user` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CLT_USER_MODULE`
--
ALTER TABLE `CLT_USER_MODULE`
  ADD CONSTRAINT `FK1_CLT_USER_MODULE` FOREIGN KEY (`MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_CLT_USER_FOLDER_CLT_USER1` FOREIGN KEY (`USER_ID`) REFERENCES `clt_user` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `CTA_EMAIL_TYPE`
--
ALTER TABLE `CTA_EMAIL_TYPE`
  ADD CONSTRAINT `FK1_CTA_EMAIL_TYPE` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CTA_ENTITY`
--
ALTER TABLE `CTA_ENTITY`
  ADD CONSTRAINT `FK1_PARTY_TYPE_ID` FOREIGN KEY (`PARTY_TYPE_ID`) REFERENCES `cta_entity_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CTA_ENTITY_EMAIL`
--
ALTER TABLE `CTA_ENTITY_EMAIL`
  ADD CONSTRAINT `FK1_CTA_ENTITY_EMAIL` FOREIGN KEY (`ENTITY_ID`) REFERENCES `cta_entity` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK2_CTA_ENTITY_EMAIL` FOREIGN KEY (`EMAIL_TYPE_ID`) REFERENCES `cta_email_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CTA_ENTITY_FAX`
--
ALTER TABLE `CTA_ENTITY_FAX`
  ADD CONSTRAINT `FK1_CTA_ENTITY_FAX` FOREIGN KEY (`FAX_TYPE_ID`) REFERENCES `cta_fax_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_CTA_ENTITY_FAX` FOREIGN KEY (`ENTITY_ID`) REFERENCES `cta_entity` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `CTA_ENTITY_LOCATION`
--
ALTER TABLE `CTA_ENTITY_LOCATION`
  ADD CONSTRAINT `FK_1_CTA_ENTITY_LOCATION` FOREIGN KEY (`ENTITY_ID`) REFERENCES `cta_entity` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_2_ CTA_ENTITY_LOCATION` FOREIGN KEY (`LOCATION_TYPE_ID`) REFERENCES `CTA_LOCATION_TYPE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_3_CTA_ENTITY_LOCATION` FOREIGN KEY (`INF_COUNTRY_ID`) REFERENCES `INF_COUNTRY` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CTA_ENTITY_PHONE`
--
ALTER TABLE `CTA_ENTITY_PHONE`
  ADD CONSTRAINT `FK1_CTA_ENTITY_PHONE` FOREIGN KEY (`PHONE_TYPE_ID`) REFERENCES `cta_phone_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_CTA_ENTITY_PHONE` FOREIGN KEY (`ENTITY_ID`) REFERENCES `cta_entity` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `CTA_ENTITY_WEB`
--
ALTER TABLE `CTA_ENTITY_WEB`
  ADD CONSTRAINT `FK1_CTA_ENTITY_WEB` FOREIGN KEY (`WEB_TYPE_ID`) REFERENCES `CTA_WEB_TYPE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_CTA_ENTITY_WEB` FOREIGN KEY (`ENTITY_ID`) REFERENCES `CTA_ENTITY` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `CTA_FAX_TYPE`
--
ALTER TABLE `CTA_FAX_TYPE`
  ADD CONSTRAINT `FK1_CTA_FAX_TYPE` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CTA_LOCATION_TYPE`
--
ALTER TABLE `CTA_LOCATION_TYPE`
  ADD CONSTRAINT `FK1_CTA_LOCATION_TYPE` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CTA_PHONE_TYPE`
--
ALTER TABLE `CTA_PHONE_TYPE`
  ADD CONSTRAINT `FK1_CTA_PHONE_TYPE` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CTA_WEB_TYPE`
--
ALTER TABLE `CTA_WEB_TYPE`
  ADD CONSTRAINT `FK1_CTA_WEB_TYPE` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `INF_BASIC_PARAMETER`
--
ALTER TABLE `INF_BASIC_PARAMETER`
  ADD CONSTRAINT `FK1_INF_BASIC_PARAMETER` FOREIGN KEY (`BASIC_PARAMETER_TYPE_ID`) REFERENCES `INF_BASIC_PARAMETER_TYPE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `INF_CITY`
--
ALTER TABLE `INF_CITY`
  ADD CONSTRAINT `FK1_INF_CITY` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `inf_country` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `INF_GROUP`
--
ALTER TABLE `INF_GROUP`
  ADD CONSTRAINT `FK1_INF_GROUP` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `INF_LOVS`
--
ALTER TABLE `INF_LOVS`
  ADD CONSTRAINT `FK1_INF_LOVS` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `INF_PRIVILEGE`
--
ALTER TABLE `INF_PRIVILEGE`
  ADD CONSTRAINT `FK1_INF_PRIVILEGE` FOREIGN KEY (`ITEM_CODE`) REFERENCES `inf_item` (`CODE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_INF_PRIVILEGE` FOREIGN KEY (`ROLE_ID`) REFERENCES `inf_role` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `INF_ROLE`
--
ALTER TABLE `INF_ROLE`
  ADD CONSTRAINT `FK_INF_ROLE` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `INF_ROLE_GROUP`
--
ALTER TABLE `INF_ROLE_GROUP`
  ADD CONSTRAINT `FK1_INF_ROLE_GROUP` FOREIGN KEY (`GROUP_ID`) REFERENCES `inf_group` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_INF_ROLE_GROUP` FOREIGN KEY (`ROLE_ID`) REFERENCES `inf_role` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `INF_TEXT`
--
ALTER TABLE `INF_TEXT`
  ADD CONSTRAINT `FK2_INF_TEXT` FOREIGN KEY (`TEXT_TYPE_ID`) REFERENCES `INF_TEXT_TYPE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3_INF_TEXT` FOREIGN KEY (`LANGUAGE_ID`) REFERENCES `INF_LANGUAGE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `MAIL_LOG`
--
ALTER TABLE `MAIL_LOG`
  ADD CONSTRAINT `FK1_MAIL_LOG` FOREIGN KEY (`MESSAGE_ID`) REFERENCES `MAIL_MESSAGE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_MAIL_LOG` FOREIGN KEY (`LOG_STATUS_ID`) REFERENCES `MAIL_LOG_STATUS` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `MAIL_MESSAGE`
--
ALTER TABLE `MAIL_MESSAGE`
  ADD CONSTRAINT `FK1_MAIL_MESSAGE` FOREIGN KEY (`TYPE_ID`) REFERENCES `MAIL_TYPE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `MAIL_MESSAGE_CLIENT`
--
ALTER TABLE `MAIL_MESSAGE_CLIENT`
  ADD CONSTRAINT `FK1_MAIL_MESSAGE_CLINET` FOREIGN KEY (`MESSAGE_ID`) REFERENCES `MAIL_MESSAGE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `MAIL_MESSAGE_CLINET_RECIPIENT`
--
ALTER TABLE `MAIL_MESSAGE_CLINET_RECIPIENT`
  ADD CONSTRAINT `FK1_MAIL_MESSAGE_CLINET_RECIPIENT` FOREIGN KEY (`MESSAGE_ID`) REFERENCES `MAIL_MESSAGE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `MAIL_MESSAGE_MODEL`
--
ALTER TABLE `MAIL_MESSAGE_MODEL`
  ADD CONSTRAINT `FK1_MAIL_MESSAGE_MODEL` FOREIGN KEY (`PM_MODEL_ID`) REFERENCES `PM_MODEL` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_MAIL_MESSAGE_MODEL` FOREIGN KEY (`MESSAGE_ID`) REFERENCES `MAIL_MESSAGE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `MAIL_MESSAGE_MODULE`
--
ALTER TABLE `MAIL_MESSAGE_MODULE`
  ADD CONSTRAINT `FK1_MAIL_MESSAGE_MODULE` FOREIGN KEY (`MESSAGE_ID`) REFERENCES `MAIL_MESSAGE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_MAIL_MESSAGE_MODULE` FOREIGN KEY (`INF_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `MAIL_MESSAGE_MODULE_RECIPIENT`
--
ALTER TABLE `MAIL_MESSAGE_MODULE_RECIPIENT`
  ADD CONSTRAINT `FK1_MAIL_MESSAGE_MODEL_idx` FOREIGN KEY (`MESSAGE_ID`) REFERENCES `MAIL_MESSAGE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_MAIL_MESSAGE_MODEL_idx` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `MAIL_MESSAGE_PLACEHOLDER`
--
ALTER TABLE `MAIL_MESSAGE_PLACEHOLDER`
  ADD CONSTRAINT `FK1_MAIL_MESSAGE_PLACEHOLDER` FOREIGN KEY (`MESSAGE_ID`) REFERENCES `MAIL_MESSAGE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_MAIL_MESSAGE_PLACEHOLDER` FOREIGN KEY (`MESSAGE_PLACEHOLDER_ID`) REFERENCES `MAIL_PLACEHOLDER` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `MAIL_MESSAGE_SCHEDULE`
--
ALTER TABLE `MAIL_MESSAGE_SCHEDULE`
  ADD CONSTRAINT `FK1_MAIL_MESSAGE_SCHEDULE` FOREIGN KEY (`MESSAGE_ID`) REFERENCES `MAIL_MESSAGE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `MAIL_TYPE`
--
ALTER TABLE `MAIL_TYPE`
  ADD CONSTRAINT `FK1_MAIL_TYPE` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `MAIL_CATEGORY` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `PM_ATTRIBUTE_VALIDATION`
--
ALTER TABLE `PM_ATTRIBUTE_VALIDATION`
  ADD CONSTRAINT `fk1_pm_attribute_validation` FOREIGN KEY (`MODEL_ID`) REFERENCES `pm_model` (`ID`),
  ADD CONSTRAINT `FK2_PM_ATTRIBUTE_VALIDATION` FOREIGN KEY (`PAGE_ATTRIBUTE_ID`) REFERENCES `PM_PAGE_ATTRIBUTE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3_PM_ATTRIBUTE_VALIDATION` FOREIGN KEY (`VALIDATION_TYPE_ID`) REFERENCES `PM_VALIDATION_TYPE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `PM_CATEGORY`
--
ALTER TABLE `PM_CATEGORY`
  ADD CONSTRAINT `FK1_PM_CATEGORY` FOREIGN KEY (`CATEGORY_TYPE_ID`) REFERENCES `pm_category_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `PM_COMPOSITION`
--
ALTER TABLE `PM_COMPOSITION`
  ADD CONSTRAINT `fk1_pm_composition` FOREIGN KEY (`MODEL_ID`) REFERENCES `pm_model` (`ID`),
  ADD CONSTRAINT `FK2_PM_COMPOSITION` FOREIGN KEY (`GROUP_ID`) REFERENCES `pm_group` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3_PM_COMPOSITION` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `pm_category` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK4_PM_COMPOSITION` FOREIGN KEY (`MENU_ID`) REFERENCES `pm_menu` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK5_PM_COMPOSITION` FOREIGN KEY (`PAGE_ID`) REFERENCES `pm_page` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `PM_GROUP`
--
ALTER TABLE `PM_GROUP`
  ADD CONSTRAINT `FK1_PM_GROUP` FOREIGN KEY (`GROUP_TYPE_ID`) REFERENCES `pm_group_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `PM_MENU`
--
ALTER TABLE `PM_MENU`
  ADD CONSTRAINT `FK1_PM_MENU` FOREIGN KEY (`MENU_TYPE_ID`) REFERENCES `pm_menu_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `PM_MODEL`
--
ALTER TABLE `PM_MODEL`
  ADD CONSTRAINT `fk1_pm_model` FOREIGN KEY (`INF_PACKAGE_ID`) REFERENCES `inf_package` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `PM_PAGE`
--
ALTER TABLE `PM_PAGE`
  ADD CONSTRAINT `FK1_PM_PAGE` FOREIGN KEY (`PAGE_TYPE_ID`) REFERENCES `pm_page_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `PM_PAGE_ATTRIBUTE`
--
ALTER TABLE `PM_PAGE_ATTRIBUTE`
  ADD CONSTRAINT `FK1_PM_PAGE_ATTRIBUTE` FOREIGN KEY (`DATA_TYPE_ID`) REFERENCES `pm_data_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_PM_PAGE_ATTRIBUTE` FOREIGN KEY (`FORMAT_TYPE_ID`) REFERENCES `pm_format_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3_PM_PAGE_ATTRIBUTE` FOREIGN KEY (`PM_COMPONENT_ID`) REFERENCES `pm_component` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK4_PM_PAGE_ATTRIBUTE` FOREIGN KEY (`PAGE_ID`) REFERENCES `pm_page` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK5_PM_PAGE_ATTRIBUTE` FOREIGN KEY (`INF_ITEM_CODE`) REFERENCES `INF_ITEM` (`CODE`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `PM_PAGE_ATTRIBUTE_MODEL`
--
ALTER TABLE `PM_PAGE_ATTRIBUTE_MODEL`
  ADD CONSTRAINT `FK1_PM_PAGE_ATTRIBUTE_MODEL` FOREIGN KEY (`PAGE_ATTRIBUTE_ID`) REFERENCES `pm_page_attribute` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_PM_PAGE_ATTRIBUTE_MODEL` FOREIGN KEY (`MODEL_ID`) REFERENCES `PM_MODEL` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3_PM_PAGE_ATTRIBUTE_MODEL` FOREIGN KEY (`DATA_TYPE_ID`) REFERENCES `PM_DATA_TYPE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK4_PM_PAGE_ATTRIBUTE_MODEL` FOREIGN KEY (`FORMAT_TYPE_ID`) REFERENCES `PM_FORMAT_TYPE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `PM_PAGE_ATTRIBUTE_MODULE`
--
ALTER TABLE `PM_PAGE_ATTRIBUTE_MODULE`
  ADD CONSTRAINT `fk_pm_page_attribute_module` FOREIGN KEY (`PAGE_ATTRIBUTE_ID`) REFERENCES `pm_page_attribute` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `PM_PAGE_PARAMETER`
--
ALTER TABLE `PM_PAGE_PARAMETER`
  ADD CONSTRAINT `FK1_PAGE_PAPRAMETER` FOREIGN KEY (`PAGE_PARAMETER_TYPE_ID`) REFERENCES `pm_page_parameter_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pm_page_parameter` FOREIGN KEY (`PAGE_ID`) REFERENCES `pm_page` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `PM_PAGE_PARAMETER_MODULE`
--
ALTER TABLE `PM_PAGE_PARAMETER_MODULE`
  ADD CONSTRAINT `fk_pm_page_parameter_module` FOREIGN KEY (`PAGE_PARAMETER_ID`) REFERENCES `pm_page_parameter` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_ADVANCED`
--
ALTER TABLE `SM_ADVANCED`
  ADD CONSTRAINT `FK1_SM_ADVANCED` FOREIGN KEY (`ADVANCED_STATUS_ID`) REFERENCES `sm_advanced_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_SM_ADVANCED` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `sm_customer` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3_SM_ADVANCED` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sm_product` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_BANK_TYPE`
--
ALTER TABLE `SM_BANK_TYPE`
  ADD CONSTRAINT `FK1_SM_BANK_TYPE` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_CHECK`
--
ALTER TABLE `SM_CHECK`
  ADD CONSTRAINT `FK1_SM_CHECK` FOREIGN KEY (`BANK_ID`) REFERENCES `SM_BANK_TYPE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_CUSTOMER`
--
ALTER TABLE `SM_CUSTOMER`
  ADD CONSTRAINT `FK1_SM_CUSTOMER` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_SM_CUSTOMER` FOREIGN KEY (`CUSTOMER_TYPE_ID`) REFERENCES `sm_customer_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3_SM_CUSTOMER` FOREIGN KEY (`ENTITY_ID`) REFERENCES `CTA_ENTITY` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK4_SM_CUSTOMER` FOREIGN KEY (`CUSTOMER_CATEGORY_ID`) REFERENCES `SM_CUSTOMER_CATEGORY` (`ID`);

--
-- Contraintes pour la table `SM_CUSTOMER_CATEGORY`
--
ALTER TABLE `SM_CUSTOMER_CATEGORY`
  ADD CONSTRAINT `FK1_SM_CUSTOMER_CATEGORY` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_CUSTOMER_TYPE`
--
ALTER TABLE `SM_CUSTOMER_TYPE`
  ADD CONSTRAINT `FK1_SM_CUSTOMER_TYPE` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_DEPOSIT`
--
ALTER TABLE `SM_DEPOSIT`
  ADD CONSTRAINT `FK1_SM_DEPOSIT` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_EXPENSE`
--
ALTER TABLE `SM_EXPENSE`
  ADD CONSTRAINT `FK1_SM_EXPENSE` FOREIGN KEY (`EXPENSE_TYPE_ID`) REFERENCES `sm_expense_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_SM_EXPENSE` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_EXPENSE_TYPE`
--
ALTER TABLE `SM_EXPENSE_TYPE`
  ADD CONSTRAINT `FK1_SM_EXPENSE_TYPE` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_JUSTIFICATION_STATUS`
--
ALTER TABLE `SM_JUSTIFICATION_STATUS`
  ADD CONSTRAINT `FK1_SM_JUSTIFICATION_STATUS` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_SM_JUSTIFICATION_STATUS` FOREIGN KEY (`JUSTIFICATION_STATUS_CATEGORY_ID`) REFERENCES `SM_JUSTIFICATION_STATUS_CATEGORY` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_ORDER`
--
ALTER TABLE `SM_ORDER`
  ADD CONSTRAINT `FK1_SM_ORDER` FOREIGN KEY (`CHECK_ID`) REFERENCES `sm_check` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_SM_ORDER` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `sm_customer` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3_SM_ORDER` FOREIGN KEY (`ORDER_STATUS_ID`) REFERENCES `sm_order_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK4_SM_ORDER` FOREIGN KEY (`PAYMENT_METHOD_ID`) REFERENCES `sm_payment_method` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK5_SM_ORDER` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`);

--
-- Contraintes pour la table `SM_ORDER_LINE`
--
ALTER TABLE `SM_ORDER_LINE`
  ADD CONSTRAINT `FK3_SM_ORDER_LINE` FOREIGN KEY (`PROMOTION_ID`) REFERENCES `SM_PROMOTION` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_SM_ORDER_LINE_SM_ORDER1` FOREIGN KEY (`ORDER_ID`) REFERENCES `sm_order` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_SM_ORDER_LINE_SM_PRODUCT1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sm_product` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_ORDER_STATUS_LOG`
--
ALTER TABLE `SM_ORDER_STATUS_LOG`
  ADD CONSTRAINT `FK1_SM_ORDER_STATUS_LOG` FOREIGN KEY (`ORDER_ID`) REFERENCES `SM_ORDER` (`ID`),
  ADD CONSTRAINT `FK2_SM_ORDER_STATUS_LOG` FOREIGN KEY (`ORDER_STATUS_ID`) REFERENCES `SM_ORDER_STATUS` (`ID`),
  ADD CONSTRAINT `FK3_SM_ORDER_STATUS_LOG` FOREIGN KEY (`JUSTIFICATION_STATUS_ID`) REFERENCES `SM_JUSTIFICATION_STATUS` (`ID`);

--
-- Contraintes pour la table `SM_ORDER_SUPPLIER`
--
ALTER TABLE `SM_ORDER_SUPPLIER`
  ADD CONSTRAINT `fk1_sm_order_supplier` FOREIGN KEY (`ORDER_SUPPLIER_STATUS_ID`) REFERENCES `sm_order_supplier_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_sm_order_supplier` FOREIGN KEY (`SUPPLIER_ID`) REFERENCES `SM_SUPPLIER` (`ID`),
  ADD CONSTRAINT `fk3_sm_order_supplier` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`);

--
-- Contraintes pour la table `SM_ORDER_SUPPLIER_LINE`
--
ALTER TABLE `SM_ORDER_SUPPLIER_LINE`
  ADD CONSTRAINT `fk1_sm_order_supplier_line` FOREIGN KEY (`ORDER_SUPPLIER_ID`) REFERENCES `sm_order_supplier` (`ID`) ON DELETE CASCADE ON UPDATE SET NULL,
  ADD CONSTRAINT `fk2_sm_order_supplier_line` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sm_product` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3_SM_ORDER_SUPPLIER_LINE` FOREIGN KEY (`PROMOTION_ID`) REFERENCES `SM_PROMOTION` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_ORDER_SUPPLIER_STATUS_LOG`
--
ALTER TABLE `SM_ORDER_SUPPLIER_STATUS_LOG`
  ADD CONSTRAINT `FK1_SM_ORDER_SUPPLIER_STATUS_LOG` FOREIGN KEY (`ORDER_SUPPLIER_ID`) REFERENCES `SM_ORDER_SUPPLIER` (`ID`),
  ADD CONSTRAINT `FK2_SM_ORDER_SUPPLIER_STATUS_LOG` FOREIGN KEY (`ORDER_SUPPLIER_STATUS_ID`) REFERENCES `SM_ORDER_SUPPLIER_STATUS` (`ID`),
  ADD CONSTRAINT `FK3_SM_ORDER_SUPPLIER_STATUS_LOG` FOREIGN KEY (`JUSTIFICATION_STATUS_ID`) REFERENCES `SM_JUSTIFICATION_STATUS` (`ID`);

--
-- Contraintes pour la table `SM_PAYMENT_METHOD`
--
ALTER TABLE `SM_PAYMENT_METHOD`
  ADD CONSTRAINT `FK1_SM_PAYMENT_METHOD` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_PRODUCT`
--
ALTER TABLE `SM_PRODUCT`
  ADD CONSTRAINT `FK1_SM_PRODUCT` FOREIGN KEY (`PRODUCT_COLOR_ID`) REFERENCES `sm_product_color` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_SM_PRODUCT` FOREIGN KEY (`PRODUCT_FAMILY_ID`) REFERENCES `sm_product_family` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3_SM_PRODUCT` FOREIGN KEY (`PRODUCT_SIZE_ID`) REFERENCES `sm_product_size` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK4_SM_PRODUCT` FOREIGN KEY (`PRODUCT_STATUS_ID`) REFERENCES `sm_product_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK5_SM_PRODUCT` FOREIGN KEY (`PRODUCT_GROUP_ID`) REFERENCES `SM_PRODUCT_GROUP` (`ID`),
  ADD CONSTRAINT `FK6_SM_PRODUCT` FOREIGN KEY (`PRODUCT_TYPE_ID`) REFERENCES `SM_PRODUCT_TYPE` (`ID`),
  ADD CONSTRAINT `FK7_SM_PRODUCT` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`),
  ADD CONSTRAINT `FK8_SM_PRODUCT` FOREIGN KEY (`PRODUCT_UNIT_ID`) REFERENCES `SM_PRODUCT_UNIT` (`ID`),
  ADD CONSTRAINT `FK9_SM_PRODUCT` FOREIGN KEY (`PRODUCT_DEPARTMENT_ID`) REFERENCES `SM_PRODUCT_DEPARTMENT` (`ID`);

--
-- Contraintes pour la table `SM_PRODUCT_COLOR`
--
ALTER TABLE `SM_PRODUCT_COLOR`
  ADD CONSTRAINT `FK1_SM_PRODUCT_COLOR` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_PRODUCT_DEPARTMENT`
--
ALTER TABLE `SM_PRODUCT_DEPARTMENT`
  ADD CONSTRAINT `FK1_SM_PRODUCT_DEPARTMENT` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_PRODUCT_FAMILY`
--
ALTER TABLE `SM_PRODUCT_FAMILY`
  ADD CONSTRAINT `FK1_SM_PRODUCT_FAMILY` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_SM_PRODUCT_FAMILY` FOREIGN KEY (`PRODUCT_GROUP_ID`) REFERENCES `sm_product_group` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_PRODUCT_GROUP`
--
ALTER TABLE `SM_PRODUCT_GROUP`
  ADD CONSTRAINT `FK1_SM_PRODUCT_GROUP` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_PRODUCT_SIZE`
--
ALTER TABLE `SM_PRODUCT_SIZE`
  ADD CONSTRAINT `FK1_SM_PRODUCT_SIZE` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_PRODUCT_TYPE`
--
ALTER TABLE `SM_PRODUCT_TYPE`
  ADD CONSTRAINT `FK1_SM_PRODUCT_TYPE` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_PRODUCT_UNIT`
--
ALTER TABLE `SM_PRODUCT_UNIT`
  ADD CONSTRAINT `FK1_SM_PRODUCT_UNIT` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_PROMOTION`
--
ALTER TABLE `SM_PROMOTION`
  ADD CONSTRAINT `fk_SM_PROMOTION_SM_PRODUCT1` FOREIGN KEY (`PRODUCT`) REFERENCES `sm_product` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_SM_PROMOTION_SM_PROMOTION_TYPE1` FOREIGN KEY (`PROMOTION_TYPE_ID`) REFERENCES `sm_promotion_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_PROMOTION_TYPE`
--
ALTER TABLE `SM_PROMOTION_TYPE`
  ADD CONSTRAINT `FK1_SM_PROMOTION_TYPE` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_RECEPTION`
--
ALTER TABLE `SM_RECEPTION`
  ADD CONSTRAINT `fk1_sm_reception` FOREIGN KEY (`SUPPLIER_ID`) REFERENCES `sm_supplier` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk2_sm_reception` FOREIGN KEY (`RECEPTION_STATUS_ID`) REFERENCES `sm_reception_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk3_sm_reception` FOREIGN KEY (`ORDER_SUPPLIER_ID`) REFERENCES `sm_order_supplier` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK4_SM_RECEPTION` FOREIGN KEY (`DEPOSIT_ID`) REFERENCES `SM_DEPOSIT` (`ID`),
  ADD CONSTRAINT `FK5_SM_RECEPTION` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`);

--
-- Contraintes pour la table `SM_RECEPTION_LINE`
--
ALTER TABLE `SM_RECEPTION_LINE`
  ADD CONSTRAINT `fk1_sm_reception_line` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sm_product` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk2_sm_reception_line` FOREIGN KEY (`RECEPTION_ID`) REFERENCES `sm_reception` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_RECEPTION_PRODUCTS`
--
ALTER TABLE `SM_RECEPTION_PRODUCTS`
  ADD CONSTRAINT `fk1_sm_reception_id` FOREIGN KEY (`RECEPTION_ID`) REFERENCES `sm_reception` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_SM_RECEPTION_ID` FOREIGN KEY (`RECEPTION_ID`) REFERENCES `SM_RECEPTION` (`ID`);

--
-- Contraintes pour la table `SM_RECEPTION_STATUS_LOG`
--
ALTER TABLE `SM_RECEPTION_STATUS_LOG`
  ADD CONSTRAINT `FK1_SM_RECEPTION_STATUS_LOG` FOREIGN KEY (`RECEPTION_ID`) REFERENCES `SM_RECEPTION` (`ID`),
  ADD CONSTRAINT `FK2_SM_RECEPTION_STATUS_LOG` FOREIGN KEY (`RECEPTION_STATUS_ID`) REFERENCES `SM_RECEPTION_STATUS` (`ID`),
  ADD CONSTRAINT `FK3_SM_RECEPTION_STATUS_LOG` FOREIGN KEY (`JUSTIFICATION_STATUS_ID`) REFERENCES `SM_JUSTIFICATION_STATUS` (`ID`);

--
-- Contraintes pour la table `SM_RETURN_RECEIPT`
--
ALTER TABLE `SM_RETURN_RECEIPT`
  ADD CONSTRAINT `fk1_sm_return_receipt` FOREIGN KEY (`RETURN_RECEIPT_STATUS_ID`) REFERENCES `sm_return_receipt_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk2_sm_return_receipt` FOREIGN KEY (`ORDER_ID`) REFERENCES `sm_order` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3_SM_RETURN_RECEIPT` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `SM_CUSTOMER` (`ID`),
  ADD CONSTRAINT `FK4_SM_RETURN_RECEIPT` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`);

--
-- Contraintes pour la table `SM_RETURN_RECEIPT_LINE`
--
ALTER TABLE `SM_RETURN_RECEIPT_LINE`
  ADD CONSTRAINT `fk1_ sm_return_receipt_line` FOREIGN KEY (`RETURN_RECEIPT_ID`) REFERENCES `sm_return_receipt` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk2_ sm_return_receipt_line` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sm_product` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3_SM_RETURN_RECEIPT_LINE` FOREIGN KEY (`PROMOTION_ID`) REFERENCES `SM_PROMOTION` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_RETURN_RECEIPT_STATUS_LOG`
--
ALTER TABLE `SM_RETURN_RECEIPT_STATUS_LOG`
  ADD CONSTRAINT `FK1_SM_RETURN_RECEIPT_STATUS_LOG` FOREIGN KEY (`RETURN_RECEIPT_ID`) REFERENCES `SM_RETURN_RECEIPT` (`ID`),
  ADD CONSTRAINT `FK2_SM_RETURN_RECEIPT_STATUS_LOG` FOREIGN KEY (`RETURN_RECEIPT_STATUS_ID`) REFERENCES `SM_RETURN_RECEIPT_STATUS` (`ID`),
  ADD CONSTRAINT `FK3_SM_RETURN_RECEIPT_STATUS_LOG` FOREIGN KEY (`JUSTIFICATION_STATUS_ID`) REFERENCES `SM_JUSTIFICATION_STATUS` (`ID`);

--
-- Contraintes pour la table `SM_SUPPLIER`
--
ALTER TABLE `SM_SUPPLIER`
  ADD CONSTRAINT `FK1_SM_SUPPLIER` FOREIGN KEY (`SUPPLIER_TYPE_ID`) REFERENCES `sm_supplier_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK2_SM_SUPPLIER` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK3_SM_SUPPLIER` FOREIGN KEY (`ENTITY_ID`) REFERENCES `CTA_ENTITY` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK4_SM_SUPPLIER` FOREIGN KEY (`SUPPLIER_CATEGORY_ID`) REFERENCES `SM_SUPPLIER_CATEGORY` (`ID`);

--
-- Contraintes pour la table `SM_SUPPLIER_CATEGORY`
--
ALTER TABLE `SM_SUPPLIER_CATEGORY`
  ADD CONSTRAINT `FK1_SM_SUPPLIER_CATEGORY` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `CLT_MODULE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_SUPPLIER_TYPE`
--
ALTER TABLE `SM_SUPPLIER_TYPE`
  ADD CONSTRAINT `FK1_SM_SUPPLIER_TYPE` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
