
-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Client :  localhost:3306
-- Généré le :  Lun 25 Avril 2016 à 21:34
-- Version du serveur :  5.5.38
-- Version de PHP :  5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `mystock`
--

DELIMITER $$
--
-- Procédures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_SM_VALIDATE_ORDER`(OUT RESULT_STATUS INT, IN ORDER_ID BIGINT(20))
BEGIN
	
    -- MOIN DU STOCK

    UPDATE SM_PRODUCT  SM_P
	INNER JOIN SM_ORDER_LINE SM_OL
	ON SM_P.ID = SM_OL.PRODUCT_ID
	SET SM_P.QUANTITY = SM_P.QUANTITY - SM_OL.QUANTITY
	WHERE SM_OL.ORDER_ID =  ORDER_ID;

	-- CHANGE STATUTS
    UPDATE SM_ORDER SET ORDER_STATUS_ID = 3 WHERE ID = ORDER_ID;
    
    -- RETURN OK
    SET RESULT_STATUS = 1;
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_SM_VALIDATE_ORDER_SUPPLIER`(OUT RESULT_STATUS INT, IN ORDER_SUPPLIER_ID BIGINT(20))
BEGIN
	
    -- AUG
    /*
    UPDATE SM_PRODUCT  SM_P
	INNER JOIN SM_ORDER_SUPPLIER_LINE SM_OSL
	ON SM_P.ID = SM_OSL.PRODUCT_ID
	SET SM_P.QUANTITY = SM_P.QUANTITY + SM_OSL.QUANTITY
	WHERE SM_OSL.ORDER_SUPPLIER_ID =  ORDER_SUPPLIER_ID;
	*/
	-- CHANGE STATUTS
    UPDATE SM_ORDER_SUPPLIER SET ORDER_SUPPLIER_STATUS_ID = 3 WHERE ID = ORDER_SUPPLIER_ID;
    
    -- RETURN OK
    SET RESULT_STATUS = 1;
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_SM_VALIDATE_RECEPTION`(OUT RESULT_STATUS INT, IN SM_RECEPTION_ID BIGINT(20))
BEGIN

    -- AUG
    
    UPDATE SM_PRODUCT  SM_P
	INNER JOIN SM_RECEPTION_LINE SM_RL
	ON SM_P.ID = SM_RL.PRODUCT_ID
	SET SM_P.QUANTITY = SM_P.QUANTITY + SM_RL.QUANTITY
	WHERE SM_RL.RECEPTION_ID =  SM_RECEPTION_ID;
	
	-- CHANGE STATUTS
    UPDATE SM_RECEPTION SET RECEPTION_STATUS_ID = 3 WHERE ID = SM_RECEPTION_ID;
    
    -- RETURN OK
    SET RESULT_STATUS = 1;
    
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `CLT_CLIENT`
--

CREATE TABLE `CLT_CLIENT` (
`ID` bigint(20) NOT NULL,
  `CODE` varchar(255) NOT NULL,
  `FIRST_NAME` tinytext,
  `LAST_NAME` tinytext,
  `COMPANY_NAME` tinytext CHARACTER SET latin1,
  `CLIENT_STATUS_ID` bigint(20) DEFAULT NULL,
  `DEFAULT_LANGUAGE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `ENTITY_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_CLIENT`
--

INSERT INTO `CLT_CLIENT` (`ID`, `CODE`, `FIRST_NAME`, `LAST_NAME`, `COMPANY_NAME`, `CLIENT_STATUS_ID`, `DEFAULT_LANGUAGE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `ENTITY_ID`) VALUES
(1, 'demo', 'demo first', 'demo last', 'demo', 1, 1, 'Y', '2016-04-24 13:47:55', NULL, NULL, NULL, 1),
(2, 'demo1', 'demo1 first', 'demo1 last', 'demo1', 1, 1, 'N', '2016-04-24 13:47:55', NULL, NULL, NULL, 2),
(3, 'demo2', 'demo2 first', 'demo2 last', 'demo2', 1, 1, 'N', '2016-04-24 13:47:55', NULL, NULL, NULL, 3),
(4, 'demo3', 'demo3 first', 'demo3 last', 'demo3', 1, 1, 'N', '2016-04-24 13:47:55', NULL, NULL, NULL, 4),
(5, 'demo4', 'demo4 first', 'demo4 last', 'demo4', 1, 1, 'N', '2016-04-24 13:47:55', NULL, NULL, NULL, 5),
(6, 'sagetech', 'sagetech first', 'sagetech last', 'SageTech', 1, 1, 'Y', '2016-04-24 14:15:53', NULL, NULL, NULL, 16);

--
-- Déclencheurs `CLT_CLIENT`
--
DELIMITER //
CREATE TRIGGER `trg_bi_clt_client` BEFORE INSERT ON `clt_client`
 FOR EACH ROW begin

    -- verfied active
    
    if new.active is null then
    
      set new.active = 'y';
    
    end if;
    
    if new.active is not null then
      
      if upper(new.active) <> 'y' and upper(new.active) <> 'n' then
      
          set  new.active = 'y';
      
      end if ;
      
    end if;
    -- date creation 
    
    set  new.date_creation = now();


    -- generate entity id pour ctl_client


    insert into cta_entity (party_type_id) values (1);
    set new.entity_id = last_insert_id();  
    
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `CLT_CLIENT_LANGUAGE`
--

CREATE TABLE `CLT_CLIENT_LANGUAGE` (
`ID` bigint(20) NOT NULL,
  `CLIENT_ID` bigint(20) DEFAULT NULL,
  `LANGUAGE_ID` bigint(20) DEFAULT NULL,
  `INF_PREFIX_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_CLIENT_LANGUAGE`
--

INSERT INTO `CLT_CLIENT_LANGUAGE` (`ID`, `CLIENT_ID`, `LANGUAGE_ID`, `INF_PREFIX_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 2, 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CLT_CLIENT_STATUS`
--

CREATE TABLE `CLT_CLIENT_STATUS` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_CLIENT_STATUS`
--

INSERT INTO `CLT_CLIENT_STATUS` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Active', NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CLT_MODEL_PARAMETER`
--

CREATE TABLE `CLT_MODEL_PARAMETER` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext CHARACTER SET latin1,
  `DESCRIPTION` text CHARACTER SET latin1,
  `DEFAULT_VALUE` text,
  `MODEL_PARAMETER_TYPE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_MODEL_PARAMETER`
--

INSERT INTO `CLT_MODEL_PARAMETER` (`ID`, `NAME`, `DESCRIPTION`, `DEFAULT_VALUE`, `MODEL_PARAMETER_TYPE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'La valeur pardéfaut de TVA', 'a2', '20', 1, 'Y', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CLT_MODEL_PARAMETER_MODEL`
--

CREATE TABLE `CLT_MODEL_PARAMETER_MODEL` (
`ID` bigint(20) NOT NULL,
  `MODEL_PARAMETER_ID` bigint(20) DEFAULT NULL,
  `VALUE` longtext,
  `PM_MODEL_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `CLT_MODEL_PARAMETER_MODULE`
--

CREATE TABLE `CLT_MODEL_PARAMETER_MODULE` (
`ID` bigint(20) NOT NULL,
  `MODEL_PARAMETER_ID` bigint(20) DEFAULT NULL,
  `VALUE` longtext,
  `MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_MODEL_PARAMETER_MODULE`
--

INSERT INTO `CLT_MODEL_PARAMETER_MODULE` (`ID`, `MODEL_PARAMETER_ID`, `VALUE`, `MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, '20', 1, 'Y', NULL, 1, NULL, NULL),
(2, 1, 'aaa', 3, 'Y', NULL, 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CLT_MODEL_PARAMETER_TYPE`
--

CREATE TABLE `CLT_MODEL_PARAMETER_TYPE` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_MODEL_PARAMETER_TYPE`
--

INSERT INTO `CLT_MODEL_PARAMETER_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'clt_module_parameter_type', 'type2', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CLT_MODULE`
--

CREATE TABLE `CLT_MODULE` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `CLIENT_ID` bigint(20) DEFAULT NULL,
  `MODULE_STATUS_ID` bigint(20) DEFAULT NULL,
  `MODULE_TYPE_ID` bigint(20) DEFAULT NULL,
  `PARENT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `IMAGE_PATH` tinytext,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `PM_MODEL_ID` bigint(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CLT_MODULE`
--

INSERT INTO `CLT_MODULE` (`ID`, `NAME`, `DESCRIPTION`, `CLIENT_ID`, `MODULE_STATUS_ID`, `MODULE_TYPE_ID`, `PARENT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `IMAGE_PATH`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `PM_MODEL_ID`) VALUES
(1, 'Gestion de stocks', '', 1, 1, 4, NULL, 1, 'Y', 'packing.png', NULL, NULL, NULL, NULL, 1),
(3, 'Admin de stock', '', 1, 1, 3, 0, 3, 'Y', 'wheel.png', NULL, NULL, NULL, NULL, 2),
(5, 'Configuration', NULL, 1, 1, 2, NULL, 5, 'Y', 'software.png', NULL, NULL, NULL, NULL, 3),
(10, 'webmaster', NULL, 1, 1, 1, NULL, NULL, 'Y', 'software.png', NULL, NULL, NULL, NULL, 4),
(11, 'Gestion de stock pour SageTech', NULL, 6, 1, 4, NULL, NULL, 'Y', 'packing.png', NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `clt_module_status`
--

CREATE TABLE `clt_module_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_module_status`
--

INSERT INTO `clt_module_status` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'ACTIF', NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Desactivé', NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_module_type`
--

CREATE TABLE `clt_module_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_module_type`
--

INSERT INTO `clt_module_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Webmaster', NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Super admin', NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(3, 'Admin', NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(4, 'Stock', NULL, 4, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_parameter`
--

CREATE TABLE `clt_parameter` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext CHARACTER SET latin1,
  `DEFAULT_VALUE` text,
  `PARAMETER_TYPE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `DESCRIPTION` tinytext CHARACTER SET latin1
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_parameter`
--

INSERT INTO `clt_parameter` (`ID`, `NAME`, `DEFAULT_VALUE`, `PARAMETER_TYPE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `DESCRIPTION`) VALUES
(1, 'Nom de l''application ', 'My Stock Management', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(2, 'Titre de l''application', 'My Stock Management', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(3, 'La langue par défaut', '1', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(4, 'Le préfixe par défaut', '1', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(5, 'Activer les caches des ressources css, js et images', 'Y', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(6, 'Le patterne de la date', 'yyyy-MM-dd', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(7, 'Le patterne de date et l''heure', 'yyyy-MM-dd HH:mm', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(8, 'Le patterne de l''heure', 'HH:mm', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(9, 'La devise (point sur inf_currency)', 'DH', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(10, 'Le nom du locataire', 'Saas212', 1, 'Y', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_parameter_client`
--

CREATE TABLE `clt_parameter_client` (
`ID` bigint(20) NOT NULL,
  `VALUE` longtext,
  `CLINET_ID` bigint(20) DEFAULT NULL,
  `PARAMETER_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_parameter_client`
--

INSERT INTO `clt_parameter_client` (`ID`, `VALUE`, `CLINET_ID`, `PARAMETER_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Gestion de stock pour SageTech', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, '1', 1, 3, 'Y', NULL, NULL, NULL, NULL),
(3, '1', 1, 4, 'Y', NULL, NULL, NULL, NULL),
(4, 'Y', 1, 5, 'Y', NULL, NULL, NULL, NULL),
(5, 'Gestion de stock || SageTech', 1, 2, 'Y', NULL, NULL, NULL, NULL),
(6, 'yyyy-MM-dd', 1, 6, 'Y', NULL, NULL, NULL, NULL),
(7, 'yyyy-MM-dd HH:mm', 1, 7, 'Y', NULL, NULL, NULL, NULL),
(8, 'HH:mm', 1, 8, 'Y', NULL, NULL, NULL, NULL),
(9, 'MAD', 1, 9, 'Y', NULL, NULL, NULL, NULL),
(10, 'SageTech.ma', 1, 10, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_parameter_type`
--

CREATE TABLE `clt_parameter_type` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_parameter_type`
--

INSERT INTO `clt_parameter_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'type1', 'desc type 11', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_structure`
--

CREATE TABLE `clt_structure` (
`ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `clt_structure_role`
--

CREATE TABLE `clt_structure_role` (
`ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `clt_structure_type`
--

CREATE TABLE `clt_structure_type` (
`ID` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `clt_user`
--

CREATE TABLE `clt_user` (
`ID` bigint(20) NOT NULL,
  `FIRST_NAME` tinytext CHARACTER SET latin1,
  `LAST_NAME` tinytext CHARACTER SET latin1,
  `USERNAME` tinytext CHARACTER SET latin1,
  `PASSWORD` tinytext CHARACTER SET latin1,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `CLIENT_ID` bigint(20) DEFAULT NULL,
  `USER_STATUS_ID` bigint(20) DEFAULT NULL,
  `DEFAULT_LANGUAGE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `IMAGE_PATH` tinytext,
  `ENTITY_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_user`
--

INSERT INTO `clt_user` (`ID`, `FIRST_NAME`, `LAST_NAME`, `USERNAME`, `PASSWORD`, `CATEGORY_ID`, `CLIENT_ID`, `USER_STATUS_ID`, `DEFAULT_LANGUAGE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `IMAGE_PATH`, `ENTITY_ID`) VALUES
(1, 'WebMaster', 'HALLAL', 'user1.demo@mystock.ma', '50f3f01caa053693ce619d596e14b0ff3901ab49', 1, 1, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/3.png', 6),
(2, 'Super Admin', 'HALLAL', 'user2.demo@mystock.ma', '889a3a791b3875cfae413574b53da4bb8a90d53e', 2, 1, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/3.png', 7),
(3, 'Admin', 'HALLAL', 'user3.demo@mystock.ma', 'd033e22ae348aeb5660fc2140aec35850c4da997', 3, 1, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/3.png', 8),
(4, 'Stock', 'HALLAL', 'user4.demo@mystock.ma', 'ed487e1e87c675af89db011b2903f20f99b11c7d', 4, 1, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/3.png', 9),
(5, NULL, NULL, 'user5.demo@mystock.ma', NULL, NULL, 1, NULL, NULL, 'Y', '2016-04-24 14:00:39', NULL, NULL, NULL, 'icons/3.png', 10),
(6, NULL, NULL, 'user6.demo@mystock.ma', NULL, NULL, 1, NULL, NULL, 'Y', '2016-04-24 14:00:39', NULL, NULL, NULL, 'icons/3.png', 11),
(7, NULL, NULL, 'user1.demo1@mystock.ma', NULL, NULL, 2, NULL, NULL, 'Y', '2016-04-24 14:00:39', NULL, NULL, NULL, 'icons/3.png', 12),
(8, NULL, NULL, 'user1.demo2@mystock.ma', NULL, NULL, 3, NULL, NULL, 'Y', '2016-04-24 14:00:39', NULL, NULL, NULL, 'icons/3.png', 13),
(9, NULL, NULL, 'user1.demo3@mystock.ma', NULL, NULL, 4, NULL, NULL, 'Y', '2016-04-24 14:00:39', NULL, NULL, NULL, 'icons/3.png', 14),
(10, 'f', NULL, 'user1.demo4@mystock.ma', NULL, NULL, 5, NULL, NULL, 'Y', '2016-04-24 14:00:39', NULL, NULL, NULL, 'icons/3.png', 15),
(11, 'first', 'last', 'user1.sagetch@mystock.ma', 'd033e22ae348aeb5660fc2140aec35850c4da997', 1, 6, NULL, NULL, 'Y', '2016-04-24 14:19:23', NULL, NULL, NULL, 'icons/3.png', 30);

--
-- Déclencheurs `clt_user`
--
DELIMITER //
CREATE TRIGGER `trg_bi_clt_user` BEFORE INSERT ON `clt_user`
 FOR EACH ROW begin

    -- date creation 
    
    set  new.date_creation = now();

    -- generate entity id pour ctl_client

    insert into cta_entity (party_type_id) values (2);
    set new.entity_id = last_insert_id();  
    
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `clt_user_category`
--

CREATE TABLE `clt_user_category` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_user_category`
--

INSERT INTO `clt_user_category` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'WebMaster', NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Super Admin', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Admin', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'Stock', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_user_client`
--

CREATE TABLE `clt_user_client` (
`ID` bigint(20) NOT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  `CLIENT_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_user_client`
--

INSERT INTO `clt_user_client` (`ID`, `USER_ID`, `CLIENT_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 1, 2, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_user_group`
--

CREATE TABLE `clt_user_group` (
`ID` bigint(20) NOT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  `INF_GROUP_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_user_group`
--

INSERT INTO `clt_user_group` (`ID`, `USER_ID`, `INF_GROUP_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(2, 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_user_module`
--

CREATE TABLE `clt_user_module` (
`ID` bigint(20) NOT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  `MODULE_ID` bigint(20) DEFAULT NULL,
  `FULL CONTROL` char(1) CHARACTER SET latin1 DEFAULT 'N',
  `MODULE_ID_TO_MANAGER` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_user_module`
--

INSERT INTO `clt_user_module` (`ID`, `USER_ID`, `MODULE_ID`, `FULL CONTROL`, `MODULE_ID_TO_MANAGER`, `ACTIVE`, `SORT_KEY`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(40, 11, 11, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_user_status`
--

CREATE TABLE `clt_user_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_user_status`
--

INSERT INTO `clt_user_status` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Actif 1', NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_EMAIL_TYPE`
--

CREATE TABLE `CTA_EMAIL_TYPE` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_EMAIL_TYPE`
--

INSERT INTO `CTA_EMAIL_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'E-mail professionnel', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'E-mail personnel', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_ENTITY`
--

CREATE TABLE `CTA_ENTITY` (
`ID` bigint(20) NOT NULL,
  `PARTY_TYPE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_ENTITY`
--

INSERT INTO `CTA_ENTITY` (`ID`, `PARTY_TYPE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, NULL, NULL, NULL, NULL),
(2, 1, NULL, NULL, NULL, NULL),
(3, 1, NULL, NULL, NULL, NULL),
(4, 1, NULL, NULL, NULL, NULL),
(5, 1, NULL, NULL, NULL, NULL),
(6, NULL, NULL, NULL, NULL, NULL),
(7, NULL, NULL, NULL, NULL, NULL),
(8, NULL, NULL, NULL, NULL, NULL),
(9, NULL, NULL, NULL, NULL, NULL),
(10, NULL, NULL, NULL, NULL, NULL),
(11, NULL, NULL, NULL, NULL, NULL),
(12, NULL, NULL, NULL, NULL, NULL),
(13, NULL, NULL, NULL, NULL, NULL),
(14, NULL, NULL, NULL, NULL, NULL),
(15, NULL, NULL, NULL, NULL, NULL),
(16, 1, NULL, NULL, NULL, NULL),
(30, 2, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_ENTITY_EMAIL`
--

CREATE TABLE `CTA_ENTITY_EMAIL` (
`ID` bigint(20) NOT NULL,
  `EMAIL_ADDRESS` tinytext,
  `IS_FOR_NOTIFICATION` char(1) DEFAULT NULL,
  `ENTITY_ID` bigint(20) DEFAULT NULL,
  `PRIORITY` bigint(20) DEFAULT NULL,
  `IS_PRINCIPAL` char(1) DEFAULT NULL,
  `EMAIL_TYPE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_ENTITY_EMAIL`
--

INSERT INTO `CTA_ENTITY_EMAIL` (`ID`, `EMAIL_ADDRESS`, `IS_FOR_NOTIFICATION`, `ENTITY_ID`, `PRIORITY`, `IS_PRINCIPAL`, `EMAIL_TYPE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(4, 'ok@aaa.fr', NULL, 3, 1, NULL, 2, NULL, NULL, NULL, NULL),
(5, 'aaaa', NULL, 2, 12, NULL, 1, NULL, NULL, NULL, NULL),
(6, 'w<xw<xw<x<w', NULL, 1, 1, NULL, 2, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_ENTITY_FAX`
--

CREATE TABLE `CTA_ENTITY_FAX` (
`ID` bigint(20) NOT NULL,
  `COUNTRY_CODE` tinytext,
  `NUMBER` tinytext,
  `PRIORITY` bigint(20) DEFAULT NULL,
  `ENTITY_ID` bigint(20) DEFAULT NULL,
  `FAX_TYPE_ID` bigint(20) DEFAULT NULL,
  `IS_PRINCIPAL` char(1) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_ENTITY_FAX`
--

INSERT INTO `CTA_ENTITY_FAX` (`ID`, `COUNTRY_CODE`, `NUMBER`, `PRIORITY`, `ENTITY_ID`, `FAX_TYPE_ID`, `IS_PRINCIPAL`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(2, '111', '2222', 33, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '111', '222', 33, 1, 1, NULL, NULL, NULL, NULL, NULL),
(4, '121', '123456789', 1, 3, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_ENTITY_LOCATION`
--

CREATE TABLE `CTA_ENTITY_LOCATION` (
`ID` bigint(20) NOT NULL,
  `ADDRESS_LINE_1` tinytext,
  `ADDRESS_LINE_2` tinytext,
  `ADDRESS_LINE_3` tinytext,
  `INF_COUNTRY_ID` bigint(20) DEFAULT NULL,
  `INF_CITY_ID` bigint(20) DEFAULT NULL,
  `POSTAL_CODE` tinytext,
  `PRIORITY` bigint(20) DEFAULT NULL,
  `ENTITY_ID` bigint(20) DEFAULT NULL,
  `LOCATION_TYPE_ID` bigint(20) DEFAULT NULL,
  `IS_PRINCIPAL` char(1) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_ENTITY_LOCATION`
--

INSERT INTO `CTA_ENTITY_LOCATION` (`ID`, `ADDRESS_LINE_1`, `ADDRESS_LINE_2`, `ADDRESS_LINE_3`, `INF_COUNTRY_ID`, `INF_CITY_ID`, `POSTAL_CODE`, `PRIORITY`, `ENTITY_ID`, `LOCATION_TYPE_ID`, `IS_PRINCIPAL`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(3, 'line1', 'line2', 'line2', 1, 1, '1', 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(4, 'aaa', 'bbb', 'ccc', 1, 2, '1000', 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(5, 'syabwaya sqdqsdqs ', 'kjkljkjkj', 'kljsqd dqs', 1, 1, '4000', 1, 1, 2, NULL, NULL, NULL, NULL, NULL),
(7, 'fsd', 'fdsfsd', 'fds', 1, 2, '11', 1, 2, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_ENTITY_PHONE`
--

CREATE TABLE `CTA_ENTITY_PHONE` (
`ID` bigint(20) NOT NULL,
  `COUNTRY_CODE` tinytext,
  `NUMBER` tinytext,
  `ENTITY_ID` bigint(20) DEFAULT NULL,
  `PHONE_TYPE_ID` bigint(20) DEFAULT NULL,
  `PRIORITY` bigint(20) DEFAULT NULL,
  `IS_PRINCIPAL` char(1) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_ENTITY_PHONE`
--

INSERT INTO `CTA_ENTITY_PHONE` (`ID`, `COUNTRY_CODE`, `NUMBER`, `ENTITY_ID`, `PHONE_TYPE_ID`, `PRIORITY`, `IS_PRINCIPAL`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(5, '+212', '1', 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(6, '121', '456789', 3, 1, 1, NULL, NULL, NULL, NULL, NULL),
(7, '+212', '2', 1, 1, 2, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_ENTITY_TYPE`
--

CREATE TABLE `CTA_ENTITY_TYPE` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_ENTITY_TYPE`
--

INSERT INTO `CTA_ENTITY_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Client / Tenant', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'Utilisateur', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Fournisseur', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'CLient / Customer', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_ENTITY_WEB`
--

CREATE TABLE `CTA_ENTITY_WEB` (
`ID` bigint(20) NOT NULL,
  `URL` tinytext,
  `PRIORITY` bigint(20) DEFAULT NULL,
  `ENTITY_ID` bigint(20) DEFAULT NULL,
  `WEB_TYPE_ID` bigint(20) DEFAULT NULL,
  `IS_PRINCIPAL` char(1) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_ENTITY_WEB`
--

INSERT INTO `CTA_ENTITY_WEB` (`ID`, `URL`, `PRIORITY`, `ENTITY_ID`, `WEB_TYPE_ID`, `IS_PRINCIPAL`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(14, 'aaadsqdqs', 111, 1, 1, NULL, NULL, NULL, NULL, NULL),
(15, 'google.com', 1, 3, 1, NULL, NULL, NULL, NULL, NULL),
(16, 'sdsqdsq', 1, 1, 1, NULL, NULL, NULL, NULL, NULL),
(17, 'sqdsqdqs', 1, 1, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_FAX_TYPE`
--

CREATE TABLE `CTA_FAX_TYPE` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_FAX_TYPE`
--

INSERT INTO `CTA_FAX_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Télécopieur professionnel', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'Télécopieur personnel', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(3, 'Télécopieur temporaire', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_LOCATION_TYPE`
--

CREATE TABLE `CTA_LOCATION_TYPE` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_LOCATION_TYPE`
--

INSERT INTO `CTA_LOCATION_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Adresse du siège social', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'Adresse de correspondance', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(3, 'Adresse professionnelle', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(4, 'Adresse personnelle', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_PHONE_TYPE`
--

CREATE TABLE `CTA_PHONE_TYPE` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_PHONE_TYPE`
--

INSERT INTO `CTA_PHONE_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Téléphone professionnel', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'Téléphone personnel', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(3, 'Téléphone mobile', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(4, 'Téléphone standard', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CTA_WEB_TYPE`
--

CREATE TABLE `CTA_WEB_TYPE` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `CTA_WEB_TYPE`
--

INSERT INTO `CTA_WEB_TYPE` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Site web professionnel', NULL, 1, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'Site web personnel', NULL, 2, 'Y', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_basic_parameter`
--

CREATE TABLE `inf_basic_parameter` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext CHARACTER SET latin1,
  `DESCRIPTION` text CHARACTER SET latin1,
  `VALUE` text,
  `BASIC_PARAMETER_TYPE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_basic_parameter`
--

INSERT INTO `inf_basic_parameter` (`ID`, `NAME`, `DESCRIPTION`, `VALUE`, `BASIC_PARAMETER_TYPE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Le liens publique de l''application', 'Le liens publique de l''application', 'http://localhost:8083/MyStock', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Les droits de la société', 'Les droits de la société', 'Copyright © 2016', 1, 'Y', NULL, NULL, NULL, NULL),
(3, 'Le nom de la société', 'Le nom de la société', 'Saas212', 1, 'Y', NULL, NULL, NULL, NULL),
(4, 'Rafraichir tous les composants', 'Cette variable pour reloader les ressources pour chaque requêtes HTTP', 'http://localhost:8083/MyStock/servlet/refresh?username=user&password=pass', 1, 'Y', NULL, NULL, NULL, NULL),
(5, 'Le site officiel de projet', 'Le site officielle de projet', 'http://www.saas212.com', 1, 'Y', NULL, NULL, NULL, NULL),
(6, 'Le lien pour obtenir d''un compte', 'Le lien pour obtenir d''un compte', 'http://www.saas212.com/ObtenirCompte', 1, 'Y', NULL, NULL, NULL, NULL),
(7, 'Version de produit', 'Version de produit', '1.0 Beta', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_basic_parameter_type`
--

CREATE TABLE `inf_basic_parameter_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_basic_parameter_type`
--

INSERT INTO `inf_basic_parameter_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'basic config', 'basic config', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'type 1', 'esc type 1', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_city`
--

CREATE TABLE `inf_city` (
`ID` bigint(20) NOT NULL,
  `CODE` tinytext CHARACTER SET latin1,
  `NAME` tinytext,
  `DESCRIPTION` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `COUNTRY_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_city`
--

INSERT INTO `inf_city` (`ID`, `CODE`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `COUNTRY_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, NULL, 'Rabat', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(2, NULL, 'Casablanca', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(3, NULL, 'Rabat2', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(4, NULL, '111city1', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_country`
--

CREATE TABLE `inf_country` (
`ID` bigint(20) NOT NULL,
  `CODE` tinytext CHARACTER SET latin1,
  `NAME` tinytext,
  `DESCRIPTION` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_country`
--

INSERT INTO `inf_country` (`ID`, `CODE`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, NULL, 'Maroc', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, NULL, 'Maroc2', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, NULL, 'pay12', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_currency`
--

CREATE TABLE `inf_currency` (
`ID` bigint(20) NOT NULL,
  `CODE` tinytext,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `SYMBOL` tinytext,
  `FORMAT` tinytext,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `inf_group`
--

CREATE TABLE `inf_group` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_group`
--

INSERT INTO `inf_group` (`ID`, `NAME`, `DESCRIPTION`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'group 1', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_item`
--

CREATE TABLE `inf_item` (
  `CODE` varchar(255) NOT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` varchar(45) DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_item`
--

INSERT INTO `inf_item` (`CODE`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
('basicParameter', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1.basicParameterType', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1.description', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1.id', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1.name', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1.value', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2.basicParameterType', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2.description', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2.id', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2.name', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2.value', 'Y', NULL, NULL, NULL, NULL),
('blValidation', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.customerName', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.description', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.id', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.sumQuantity', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.sumTotalPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s2', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s2.customer', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s2.description', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.color', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.size', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.color', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.price', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.quantity', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.reference', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.size', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.totalPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s5', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s5.sumQuantity', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s5.sumTotal', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s6', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s1', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s1.customer', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s1.description', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.color', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.quantity', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.size', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.color', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.price', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.size', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.totalPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s4', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s4.sumQuantity', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s4.sumTotal', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s5', 'Y', NULL, NULL, NULL, NULL),
('contactManagement', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1.addressLine', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1.ctaLocationLine1', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1.ctaLocationLine2', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1.ctaLocationLine3', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1.ctaLocationPostalCode', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1.ctaLocationPriority', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1.infCityName', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1.infCountryName', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1.locationTypeName', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1.postalCode', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s1.priority', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s2', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s2.countryCode', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s2.ctaPhoneCountryCode', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s2.ctaPhoneNumber', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s2.ctaPhonePriority', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s2.number', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s2.phoneTypeName', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s2.priority', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s3', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s3.countryCode', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s3.ctaFaxCountryCode', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s3.ctaFaxNumber', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s3.ctaFaxPriority', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s3.faxTypeName', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s3.number', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s3.priority', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s4', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s4.ctaWebPriority', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s4.ctaWebUrl', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s4.externalUrl', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s4.priority', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s4.webTypeName', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s5', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s5.ctaEmailAdresse', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s5.ctaEmailPriority', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s5.emailAddress', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s5.emailTypeName', 'Y', NULL, NULL, NULL, NULL),
('contactManagement.s5.priority', 'Y', NULL, NULL, NULL, NULL),
('customer', 'Y', NULL, NULL, NULL, NULL),
('customer.s1', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.active', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.adress', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.category', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.city', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.company', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.country', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.email', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.firstName', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.fixedPhone', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.fullLabel', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.lastName', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.note', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.secondaryAddress', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.shortLabel', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.type', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.webSite', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.zipCode', 'Y', NULL, NULL, NULL, NULL),
('customer.s2', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.adress', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.city', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.country', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.email', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.firstName', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.fixedPhone', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.lastName', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.type', 'Y', NULL, NULL, NULL, NULL),
('globals', 'Y', NULL, NULL, NULL, NULL),
('globals.forms', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.add', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.cancel', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.clean', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.confirmClean', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.confirmDelete', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.delete', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.edit', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.no', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.save', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.search', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.validate', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.yes', 'Y', NULL, NULL, NULL, NULL),
('globals.list', 'Y', NULL, NULL, NULL, NULL),
('globals.list.activate', 'Y', NULL, NULL, NULL, NULL),
('globals.list.confirmDelete', 'Y', NULL, NULL, NULL, NULL),
('globals.list.delete', 'Y', NULL, NULL, NULL, NULL),
('globals.list.edit', 'Y', NULL, NULL, NULL, NULL),
('globals.list.option', 'Y', NULL, NULL, NULL, NULL),
('globals.list.validate', 'Y', NULL, NULL, NULL, NULL),
('globals.list.vide', 'Y', NULL, NULL, NULL, NULL),
('inventaire', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.active', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.designation', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.priceBuy', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.priceSale', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.productColorName', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.productSizeName', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.quantity', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.quantityCount', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.receptionValidCount', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.unitPriceBuyMax', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.unitPriceBuyMin', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.unitPriceBuyMoyenne', 'Y', NULL, NULL, NULL, NULL),
('login', 'Y', NULL, NULL, NULL, NULL),
('login.s1', 'Y', NULL, NULL, NULL, NULL),
('login.s1.forgetPassword', 'Y', NULL, NULL, NULL, NULL),
('login.s1.getAccount', 'Y', NULL, NULL, NULL, NULL),
('login.s1.login', 'Y', NULL, NULL, NULL, NULL),
('login.s1.password', 'Y', NULL, NULL, NULL, NULL),
('login.s1.sessionActive', 'Y', NULL, NULL, NULL, NULL),
('login.s1.username', 'Y', NULL, NULL, NULL, NULL),
('module', 'Y', NULL, NULL, NULL, NULL),
('module.s1', 'Y', NULL, NULL, NULL, NULL),
('module.s1.access', 'Y', NULL, NULL, NULL, NULL),
('module.s1.logout', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1.description', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1.id', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1.moduleParameterType', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1.name', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1.value', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2.description', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2.id', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2.moduleParameterType', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2.name', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2.value', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s1', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s1.company', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s1.email', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s1.name', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s2', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s2.descriptionModule', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s2.moduleTypeName', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s2.nameModule', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3.client', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3.defaultValue', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3.parameterModuleName', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3.parameterModuleTypeName', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3.value', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s4', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s4.client', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s4.parameterModule', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s4.parameterModuleDefaultValue', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s4.value', 'Y', NULL, NULL, NULL, NULL),
('p003', 'Y', NULL, NULL, NULL, NULL),
('p003.s1', 'Y', NULL, NULL, NULL, NULL),
('p003.s1.amount', 'Y', NULL, NULL, NULL, NULL),
('p003.s1.description', 'Y', NULL, NULL, NULL, NULL),
('p003.s1.expenseType', 'Y', NULL, NULL, NULL, NULL),
('p003.s1.expenseTypeName', 'Y', NULL, NULL, NULL, NULL),
('p003.s1.name', 'Y', NULL, NULL, NULL, NULL),
('p003.s2', 'Y', NULL, NULL, NULL, NULL),
('p003.s2.amount', 'Y', NULL, NULL, NULL, NULL),
('p003.s2.description', 'Y', NULL, NULL, NULL, NULL),
('p003.s2.name', 'Y', NULL, NULL, NULL, NULL),
('p003.s2.type', 'Y', NULL, NULL, NULL, NULL),
('p004', 'Y', NULL, NULL, NULL, NULL),
('p004.s1', 'Y', NULL, NULL, NULL, NULL),
('p004.s1.active', 'Y', NULL, NULL, NULL, NULL),
('p004.s1.companyName', 'Y', NULL, NULL, NULL, NULL),
('p004.s1.email', 'Y', NULL, NULL, NULL, NULL),
('p004.s1.name', 'Y', NULL, NULL, NULL, NULL),
('p004.s1.phone', 'Y', NULL, NULL, NULL, NULL),
('p004.s1.type', 'Y', NULL, NULL, NULL, NULL),
('p004.s2', 'Y', NULL, NULL, NULL, NULL),
('p004.s2.active', 'Y', NULL, NULL, NULL, NULL),
('p004.s2.category', 'Y', NULL, NULL, NULL, NULL),
('p004.s2.companyName', 'Y', NULL, NULL, NULL, NULL),
('p004.s2.firstName', 'Y', NULL, NULL, NULL, NULL),
('p004.s2.fullLabel', 'Y', NULL, NULL, NULL, NULL),
('p004.s2.lastName', 'Y', NULL, NULL, NULL, NULL),
('p004.s2.nature', 'Y', NULL, NULL, NULL, NULL),
('p004.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p004.s2.representative', 'Y', NULL, NULL, NULL, NULL),
('p004.s2.shortLabel', 'Y', NULL, NULL, NULL, NULL),
('p004.s2.type', 'Y', NULL, NULL, NULL, NULL),
('p005.s1', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.searchColor', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.searchDepartment', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.searchDesignation', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.searchFamily', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.searchQte', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.searchReference', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.searchSize', 'Y', NULL, NULL, NULL, NULL),
('p005.s1.searchType', 'Y', NULL, NULL, NULL, NULL),
('p005.s2', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.designation', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.priceBuy', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.priceSale', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.productFamilyName', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.productGroupName', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.productTypeName', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.quantity', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p005.s2.threshold', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.active', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.additionalInformation', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.basicInformation', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.color', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.family', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.group', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.note', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.priceBuy', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.priceSale', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.reorganization', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.size', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.threshold', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.type', 'Y', NULL, NULL, NULL, NULL),
('p005.s3.unit', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.code', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.deadline', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.deposit', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.souche', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p007', 'Y', NULL, NULL, NULL, NULL),
('p007.s1', 'Y', NULL, NULL, NULL, NULL),
('p007.s1.active', 'Y', NULL, NULL, NULL, NULL),
('p007.s1.companyName', 'Y', NULL, NULL, NULL, NULL),
('p007.s1.email', 'Y', NULL, NULL, NULL, NULL),
('p007.s1.name', 'Y', NULL, NULL, NULL, NULL),
('p007.s1.phone', 'Y', NULL, NULL, NULL, NULL),
('p007.s1.type', 'Y', NULL, NULL, NULL, NULL),
('p007.s2', 'Y', NULL, NULL, NULL, NULL),
('p007.s2.active', 'Y', NULL, NULL, NULL, NULL),
('p007.s2.category', 'Y', NULL, NULL, NULL, NULL),
('p007.s2.companyName', 'Y', NULL, NULL, NULL, NULL),
('p007.s2.firstName', 'Y', NULL, NULL, NULL, NULL),
('p007.s2.fullLabel', 'Y', NULL, NULL, NULL, NULL),
('p007.s2.lastName', 'Y', NULL, NULL, NULL, NULL),
('p007.s2.nature', 'Y', NULL, NULL, NULL, NULL),
('p007.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p007.s2.representative', 'Y', NULL, NULL, NULL, NULL),
('p007.s2.shortLabel', 'Y', NULL, NULL, NULL, NULL),
('p007.s2.type', 'Y', NULL, NULL, NULL, NULL),
('p008', 'Y', NULL, NULL, NULL, NULL),
('p008.s1', 'Y', NULL, NULL, NULL, NULL),
('p008.s1.designation', 'Y', NULL, NULL, NULL, NULL),
('p008.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p008.s2.designation', 'Y', NULL, NULL, NULL, NULL),
('p008.s2.quantity', 'Y', NULL, NULL, NULL, NULL),
('p008.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p008.s2.threshold', 'Y', NULL, NULL, NULL, NULL),
('p008.s2.totalCommanded', 'Y', NULL, NULL, NULL, NULL),
('p008.s2.totalRecieved', 'Y', NULL, NULL, NULL, NULL),
('p009', 'Y', NULL, NULL, NULL, NULL),
('p009.s1', 'Y', NULL, NULL, NULL, NULL),
('p009.s1.designation', 'Y', NULL, NULL, NULL, NULL),
('p009.s1.orderSupplierReference', 'Y', NULL, NULL, NULL, NULL),
('p009.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p009.s1.supplier', 'Y', NULL, NULL, NULL, NULL),
('p009.s2', 'Y', NULL, NULL, NULL, NULL),
('p009.s2.designation', 'Y', NULL, NULL, NULL, NULL),
('p009.s2.orderSupplierReference', 'Y', NULL, NULL, NULL, NULL),
('p009.s2.qteCommanded', 'Y', NULL, NULL, NULL, NULL),
('p009.s2.qteRecieved', 'Y', NULL, NULL, NULL, NULL),
('p009.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p009.s2.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p012.s1', 'Y', NULL, NULL, NULL, NULL),
('p012.s1.productTotalQuantity', 'Y', NULL, NULL, NULL, NULL),
('p012.s2', 'Y', NULL, NULL, NULL, NULL),
('p012.s2.orderSupplierInProgressCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s2.orderSupplierRefusedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s2.orderSupplierRejectedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s2.orderSupplierTransmittedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s2.orderSupplierValidatedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s3', 'Y', NULL, NULL, NULL, NULL),
('p012.s3.receptionInProgressCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s3.receptionRefusedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s3.receptionRejectedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s3.receptionTransmittedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s3.receptionValidatedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s4', 'Y', NULL, NULL, NULL, NULL),
('p012.s4.orderdRefusedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s4.orderdRejectedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s4.orderInProgressCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s4.orderTransmittedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s4.orderValidatedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s5', 'Y', NULL, NULL, NULL, NULL),
('p012.s5.returnReceiptInProgressCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s5.returnReceiptRefusedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s5.returnReceiptRejectedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s5.returnReceiptTransmittedCount', 'Y', NULL, NULL, NULL, NULL),
('p012.s5.returnReceiptValidatedCount', 'Y', NULL, NULL, NULL, NULL),
('p032', 'Y', NULL, NULL, NULL, NULL),
('p032.s1', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.addReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.deleteReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.editReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p032.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p032.s2', 'Y', NULL, NULL, NULL, NULL),
('p032.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p032.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p032.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p032.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p032.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p032.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p032.s2.validate', 'Y', NULL, NULL, NULL, NULL),
('p032.s3', 'Y', NULL, NULL, NULL, NULL),
('p032.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p032.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p032.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p032.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p032.s3.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p032.s4', 'Y', NULL, NULL, NULL, NULL),
('p032.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p032.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p032.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p032.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p032.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p032.s5', 'Y', NULL, NULL, NULL, NULL),
('p032.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p032.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p032.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p032.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p032.s5.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p033', 'Y', NULL, NULL, NULL, NULL),
('p033.s1', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.addReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.deleteReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.editReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p033.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p033.s2', 'Y', NULL, NULL, NULL, NULL),
('p033.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p033.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p033.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p033.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p033.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p033.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p033.s3', 'Y', NULL, NULL, NULL, NULL),
('p033.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p033.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p033.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p033.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p033.s3.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p033.s4', 'Y', NULL, NULL, NULL, NULL),
('p033.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p033.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p033.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p033.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p033.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p033.s5', 'Y', NULL, NULL, NULL, NULL),
('p033.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p033.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p033.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p033.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p033.s5.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p034', 'Y', NULL, NULL, NULL, NULL),
('p034.s1', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.addReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.deleteReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.editReturnReceipt', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p034.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p034.s2', 'Y', NULL, NULL, NULL, NULL),
('p034.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p034.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p034.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p034.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p034.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p034.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p034.s3', 'Y', NULL, NULL, NULL, NULL),
('p034.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p034.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p034.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p034.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p034.s3.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p034.s4', 'Y', NULL, NULL, NULL, NULL),
('p034.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p034.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p034.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p034.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p034.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p034.s5', 'Y', NULL, NULL, NULL, NULL),
('p034.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p034.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p034.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p034.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p034.s5.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p035', 'Y', NULL, NULL, NULL, NULL),
('p035.s1', 'Y', NULL, NULL, NULL, NULL),
('p035.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p035.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p035.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p035.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p035.s1.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p035.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p035.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p035.s2', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.commande', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p035.s2.validate', 'Y', NULL, NULL, NULL, NULL),
('p035.s3', 'Y', NULL, NULL, NULL, NULL),
('p035.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p035.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p035.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p035.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p035.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p035.s4', 'Y', NULL, NULL, NULL, NULL),
('p035.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p035.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p035.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p035.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p035.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p035.s5', 'Y', NULL, NULL, NULL, NULL),
('p035.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p035.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p035.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p035.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p035.s5.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p036', 'Y', NULL, NULL, NULL, NULL),
('p036.s1', 'Y', NULL, NULL, NULL, NULL),
('p036.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p036.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p036.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p036.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p036.s1.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p036.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p036.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p036.s2', 'Y', NULL, NULL, NULL, NULL),
('p036.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p036.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p036.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p036.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p036.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p036.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p036.s2.validate', 'Y', NULL, NULL, NULL, NULL),
('p036.s3', 'Y', NULL, NULL, NULL, NULL),
('p036.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p036.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p036.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p036.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p036.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p036.s4', 'Y', NULL, NULL, NULL, NULL),
('p036.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p036.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p036.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p036.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p036.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p036.s5', 'Y', NULL, NULL, NULL, NULL),
('p036.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p036.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p036.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p036.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p036.s5.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p037', 'Y', NULL, NULL, NULL, NULL),
('p037.s1', 'Y', NULL, NULL, NULL, NULL),
('p037.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p037.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p037.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p037.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p037.s1.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p037.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p037.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p037.s2', 'Y', NULL, NULL, NULL, NULL),
('p037.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p037.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p037.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p037.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p037.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p037.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p037.s2.validate', 'Y', NULL, NULL, NULL, NULL),
('p037.s3', 'Y', NULL, NULL, NULL, NULL),
('p037.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p037.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p037.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p037.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p037.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p037.s4', 'Y', NULL, NULL, NULL, NULL),
('p037.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p037.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p037.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p037.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p037.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p037.s5', 'Y', NULL, NULL, NULL, NULL),
('p037.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p037.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p037.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p037.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p037.s5.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p038', 'Y', NULL, NULL, NULL, NULL),
('p038.s1', 'Y', NULL, NULL, NULL, NULL),
('p038.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p038.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p038.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p038.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p038.s1.supplierCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p038.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p038.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p038.s2', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.commande', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('p038.s2.validate', 'Y', NULL, NULL, NULL, NULL),
('p038.s3', 'Y', NULL, NULL, NULL, NULL),
('p038.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p038.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p038.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p038.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p038.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p038.s3.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p038.s4', 'Y', NULL, NULL, NULL, NULL),
('p038.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p038.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p038.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p038.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p038.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p038.s5', 'Y', NULL, NULL, NULL, NULL),
('p038.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p038.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p038.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p038.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p038.s5.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('p038.s5.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('p041', 'Y', NULL, NULL, NULL, NULL),
('p041.s1', 'Y', NULL, NULL, NULL, NULL),
('p041.s1.customerCompanyName', 'Y', NULL, NULL, NULL, NULL),
('p041.s1.noSeq', 'Y', NULL, NULL, NULL, NULL),
('p041.s1.option', 'Y', NULL, NULL, NULL, NULL),
('p041.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('p041.s1.sumProduct', 'Y', NULL, NULL, NULL, NULL),
('p041.s1.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('p041.s1.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('p041.s2', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.commande', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.customer', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.return', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.save', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.submit', 'Y', NULL, NULL, NULL, NULL),
('p041.s2.validate', 'Y', NULL, NULL, NULL, NULL),
('p041.s3', 'Y', NULL, NULL, NULL, NULL),
('p041.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('p041.s3.negotiatePriceSale', 'Y', NULL, NULL, NULL, NULL),
('p041.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('p041.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('p041.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('p041.s4', 'Y', NULL, NULL, NULL, NULL),
('p041.s4.amountTva', 'Y', NULL, NULL, NULL, NULL),
('p041.s4.countProducts', 'Y', NULL, NULL, NULL, NULL),
('p041.s4.totalPu', 'Y', NULL, NULL, NULL, NULL),
('p041.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('p041.s4.valueTva', 'Y', NULL, NULL, NULL, NULL),
('p041.s5', 'Y', NULL, NULL, NULL, NULL),
('p041.s5.designation', 'Y', NULL, NULL, NULL, NULL),
('p041.s5.negotiatePriceSale', 'Y', NULL, NULL, NULL, NULL),
('p041.s5.quantity', 'Y', NULL, NULL, NULL, NULL),
('p041.s5.reference', 'Y', NULL, NULL, NULL, NULL),
('p041.s5.totalHt', 'Y', NULL, NULL, NULL, NULL),
('page1.s1.name', 'Y', NULL, NULL, NULL, NULL),
('pageParameter', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.description', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.id', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.name', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.page', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.pageParameterType', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.value', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.description', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.id', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.name', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.page', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.pageParameterType', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.value', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s1', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s1.company', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s1.email', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s1.name', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s2', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s2.descriptionModule', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s2.moduleTypeName', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s2.nameModule', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s3', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s3.client', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s3.pageDescription', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s3.pageName', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s3.pageTypeName', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s4', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s4.client', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s4.pageParameter', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s4.pageParameterDefaultValue', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s4.value', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s5', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s5.client', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s5.pageParameter', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s5.pageParameterDefaultValue', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s5.value', 'Y', NULL, NULL, NULL, NULL),
('parameter', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1.description', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1.id', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1.name', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1.parameterType', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1.value', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2.description', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2.id', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2.name', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2.parameterType', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2.value', 'Y', NULL, NULL, NULL, NULL),
('parameterClient', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s1', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s1.company', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s1.email', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s1.name', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2.client', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2.defaultValue', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2.parameterName', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2.parameterTypeName', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2.value', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s3', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s3.client', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s3.parameter', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s3.parameterDefaultValue', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s3.value', 'Y', NULL, NULL, NULL, NULL),
('pdf001', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s1', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s2', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s2.seq', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s2.totalHt', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s2.TotalQuantity', 'Y', NULL, NULL, NULL, NULL),
('pdf001.s2.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('pdf002', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s1', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s1.status', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s1.supplier', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s1.the', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s2', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s2.designation', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s2.quantity', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s2.totalHt', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s2.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s3.amountTva', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s3.totalPu', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s3.valueTva', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s4', 'Y', NULL, NULL, NULL, NULL),
('pdf002.s4.note', 'Y', NULL, NULL, NULL, NULL),
('pdf003', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s1', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s2', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s2.seq', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s2.totalHt', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s2.TotalQuantity', 'Y', NULL, NULL, NULL, NULL),
('pdf003.s2.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('pdf004', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s1', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s1.status', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s1.supplier', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s1.the', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s2', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s2.designation', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s2.quantity', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s2.totalHt', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s2.unitPriceSale', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s3.amountTva', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s3.totalPu', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s3.valueTva', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s4', 'Y', NULL, NULL, NULL, NULL),
('pdf004.s4.note', 'Y', NULL, NULL, NULL, NULL),
('pdf005', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s1', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s2', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s2.seq', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s2.totalHt', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s2.TotalQuantity', 'Y', NULL, NULL, NULL, NULL),
('pdf005.s2.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('pdf006', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s1', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s1.status', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s1.supplier', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s1.the', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s2', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s2.designation', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s2.quantity', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s2.totalHt', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s2.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s3.amountTva', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s3.totalPu', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s3.valueTva', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s4', 'Y', NULL, NULL, NULL, NULL),
('pdf006.s4.note', 'Y', NULL, NULL, NULL, NULL),
('product', 'Y', NULL, NULL, NULL, NULL),
('profile.s1', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.adress', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.email', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.firstName', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.fixedPhone', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.lastName', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.password', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.replayRassword', 'Y', NULL, NULL, NULL, NULL),
('reception', 'Y', NULL, NULL, NULL, NULL),
('reception.s1', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.active', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.amount', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.deadline', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.id', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.name', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.souche', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.supplierId', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.valid', 'Y', NULL, NULL, NULL, NULL),
('reception.s2', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.amount', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.deadline', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.deposit', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.description', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.id', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.name', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.sizes', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.souche', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.valid', 'Y', NULL, NULL, NULL, NULL),
('reception.s3', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.id', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.remise', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.tva', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('reception.s4', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.designation', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.id', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.quantity', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.remise', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.totalHt', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.tva', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('reception.s5', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.quantityTotal', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.remiseAvg', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.tvaAvg', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.unitPriceBuyTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.active', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.amount', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.deadline', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.id', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.name', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.souche', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.supplierId', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.amount', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.deadline', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.deposit', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.description', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.id', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.name', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.souche', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.id', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.remise', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.tva', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.quantityTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.remiseAvg', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.tvaAvg', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.unitPriceBuyTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.active', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.amount', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.deadline', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.id', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.name', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.souche', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.supplierId', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.amount', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.deadline', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.deposit', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.description', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.id', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.name', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.souche', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.id', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.remise', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.tva', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.quantityTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.remiseAvg', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.tvaAvg', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.unitPriceBuyTotal', 'Y', NULL, NULL, NULL, NULL),
('rofile.s1.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('supplier', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.dataTypeChar', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.dataTypeDateTime', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.dataTypeDouble', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.dataTypeInteger', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeCodePostale', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeDate', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeDateTime', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeEmail', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeFax', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypePhone', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeTime', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.maxlenght', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.maxWord', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.required', 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_language`
--

CREATE TABLE `inf_language` (
`ID` bigint(20) NOT NULL,
  `CODE` tinytext,
  `NAME` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_language`
--

INSERT INTO `inf_language` (`ID`, `CODE`, `NAME`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'fr', 'Français de France', 1, 'Y', '2014-09-30 00:00:00', 1, '2014-09-16 00:00:00', NULL),
(2, 'en', 'Anglais', NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_lovs`
--

CREATE TABLE `inf_lovs` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `TABLE_PREFIX` tinytext,
  `TABLE` tinytext,
  `VIEW` tinytext,
  `ITEM_CODE` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ENTITY` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_lovs`
--

INSERT INTO `inf_lovs` (`ID`, `NAME`, `DESCRIPTION`, `TABLE_PREFIX`, `TABLE`, `VIEW`, `ITEM_CODE`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `CLT_MODULE_ID`, `ENTITY`) VALUES
(1, 'Les types de fournisseurs', NULL, 'sm', 'sm_supplier_type', 'v_sm_supplier_type', 'itemCode', 1, 'Y', NULL, NULL, NULL, NULL, 1, 'SmSupplierType'),
(2, 'La couleurs', NULL, 'sm', 'sm_expense_type', 'v_sm_expense_type', 'itemCode', 2, 'Y', NULL, NULL, NULL, NULL, 1, 'SmProductColor'),
(3, 'Les Pays', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'InfCountry'),
(4, 'Les villes', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'InfCity');

-- --------------------------------------------------------

--
-- Structure de la table `INF_PACKAGE`
--

CREATE TABLE `INF_PACKAGE` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `INF_PACKAGE`
--

INSERT INTO `INF_PACKAGE` (`ID`, `NAME`, `DESCRIPTION`) VALUES
(1, 'Pack Basique', 'Pack Basique');

-- --------------------------------------------------------

--
-- Structure de la table `inf_prefix`
--

CREATE TABLE `inf_prefix` (
`ID` bigint(20) NOT NULL,
  `CODE` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_prefix`
--

INSERT INTO `inf_prefix` (`ID`, `CODE`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'default', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_privilege`
--

CREATE TABLE `inf_privilege` (
`ID` bigint(20) NOT NULL,
  `ITEM_CODE` varchar(255) DEFAULT NULL,
  `ROLE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_privilege`
--

INSERT INTO `inf_privilege` (`ID`, `ITEM_CODE`, `ROLE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'login', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'login.s1', 1, 'Y', NULL, NULL, NULL, NULL),
(3, 'login.s1.username', 1, 'Y', NULL, NULL, NULL, NULL),
(4, 'login.s1.password', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_role`
--

CREATE TABLE `inf_role` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_role`
--

INSERT INTO `inf_role` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Role 1', 'Role 1', 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_role_group`
--

CREATE TABLE `inf_role_group` (
`ID` bigint(20) NOT NULL,
  `ROLE_ID` bigint(20) DEFAULT NULL,
  `GROUP_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_role_group`
--

INSERT INTO `inf_role_group` (`ID`, `ROLE_ID`, `GROUP_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_text`
--

CREATE TABLE `inf_text` (
`ID` bigint(20) NOT NULL,
  `PREFIX` bigint(20) DEFAULT NULL,
  `ITEM_CODE` varchar(255) DEFAULT NULL,
  `VALUE` longtext,
  `TEXT_TYPE_ID` bigint(20) DEFAULT NULL,
  `LANGUAGE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1056 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_text`
--

INSERT INTO `inf_text` (`ID`, `PREFIX`, `ITEM_CODE`, `VALUE`, `TEXT_TYPE_ID`, `LANGUAGE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(2, 1, 'module.s1.access', 'Mot de passe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(3, 1, 'login.s1.username', 'E-mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(4, 1, 'login.s1.username', 'username', 6, 1, 'Y', NULL, NULL, NULL, NULL),
(5, 1, 'login.s1.login', 'Se connecter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(10, 1, 'page1.s1.name', 'Résumé Général', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(11, 1, 'validation.v1.dataTypeInteger', '{0} doit être de type integer', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(12, 1, 'validation.v1.dataTypeDouble', '{0} doit être de type double', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(13, 1, 'validation.v1.maxWord', '{0} ne doit pas dépasser {1} mots', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(14, 1, 'validation.v1.formatTypeDate', '{0} doit être de type date', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(15, 1, 'validation.v1.dataTypeChar', '{0} doit être de type caractère', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(16, 1, 'validation.v1.dataTypeDateTime', '{0} doit être de type datetime', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(17, 1, 'validation.v1.formatTypeEmail', '{0} doit être de type email', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(18, 1, 'validation.v1.formatTypePhone', '{0} doit être de type phone', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(19, 1, 'validation.v1.maxlenght', '{0} ne doit pas dépasser {1} char', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(20, 1, 'validation.v1.required', '{0} est obligatoire', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(26, 1, 'p003.s1', 'La liste des dépenses', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(27, 1, 'p003.s1.name', 'Libelle', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(28, 1, 'p003.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(29, 1, 'p003.s1.amount', 'Montant', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(30, 1, 'p003.s1.expenseTypeName', 'Type de charge', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(34, 1, 'p003.s2', 'Ajouter / Modifier un dépense', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(36, 1, 'p003.s2.type', 'Type de charge', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(37, 1, 'p003.s2.name', 'Libelle', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(38, 1, 'p003.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(39, 1, 'p003.s2.amount', 'Montant', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(53, 1, 'p005.s1.searchReference', 'Réference', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(54, 1, 'p005.s1.searchDesignation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(55, 1, 'p005.s1.searchFamily', 'Famille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(56, 1, 'p005.s1.searchQte', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(57, 1, 'p005.s1.searchType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(58, 1, 'p005.s1.searchDepartment', 'Département', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(59, 1, 'p005.s1.searchSize', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(60, 1, 'p005.s1.searchColor', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(63, 1, 'p005.s1', 'Gestion de produits : Recherche Multi-critères', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(67, 1, 'p005.s2.productFamilyName', 'Famille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(70, 1, 'p005.s2.priceSale', 'Prix de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(71, 1, 'p005.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(72, 1, 'p005.s2.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(73, 1, 'p005.s2.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(74, 1, 'p005.s2', 'Résultat de recherche', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(90, 1, 'globals.list.delete', 'Supprimer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(91, 1, 'globals.list.edit', 'Modifier', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(92, 1, 'globals.list.option', 'Option', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(93, 1, 'globals.list.activate', 'Activer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(94, 1, 'globals.list.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(95, 1, 'globals.list.vide', 'la liste est vide.', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(96, 1, 'globals.list.confirmDelete', 'Êtes-vous sûr de vouloir supprimer ?', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(97, 1, 'globals.forms.add', 'Ajouter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(98, 1, 'globals.forms.edit', 'Modifier', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(99, 1, 'globals.forms.cancel', 'Annuler', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(100, 1, 'globals.forms.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(101, 1, 'globals.forms.delete', 'Supprimer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(102, 1, 'globals.forms.confirmDelete', 'Êtes-vous sûr de vouloir supprimer ?', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(103, 1, 'reception.s1', 'La liste des réceptions en cours', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(104, 1, 'reception.s1.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(105, 1, 'reception.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(106, 1, 'reception.s1.supplierId', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(107, 1, 'reception.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(108, 1, 'reception.s1.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(109, 1, 'reception.s1.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(110, 1, 'reception.s1.amount', 'Le montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(111, 1, 'reception.s1.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(112, 1, 'globals.forms.yes', 'Oui', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(113, 1, 'globals.forms.no', 'Non', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(146, 1, 'reception.s2', 'Ajouter une réception ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(147, 1, 'reception.s2.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(148, 1, 'reception.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(149, 1, 'reception.s2.supplier', 'Fournisseur ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(150, 1, 'reception.s2.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(151, 1, 'reception.s2.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(152, 1, 'reception.s2.amount', 'Montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(153, 1, 'reception.s2.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(155, 1, 'reception.s2.deposit', 'Depôt', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(156, 1, 'reception.s2.description', 'Description ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(159, 1, 'product', 'La gestion des produits ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(161, 1, 'p003.s1.expenseTypeName', 'Type de charge', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(162, 1, 'customer.s2', 'La liste des clients', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(163, 1, 'customer.s2.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(164, 1, 'customer.s1', 'Ajouter / Modifier un client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(165, 1, 'customer.s2.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(166, 1, 'customer.s2.country', 'Pays', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(167, 1, 'customer.s2.city', 'Ville', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(168, 1, 'customer.s2.email', 'E-Mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(169, 1, 'customer.s2.adress', 'Adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(170, 1, 'customer.s2.fixedPhone', 'Télé Fixe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(171, 1, 'customer.s2.cellPhone', 'Télé Portable', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(172, 1, 'customer.s1.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(173, 1, 'customer.s1.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(174, 1, 'customer.s1.country', 'Pays', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(175, 1, 'customer.s1.city', 'Ville', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(176, 1, 'customer.s1.cellPhone', 'Télé Portable', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(177, 1, 'customer.s1.fixedPhone', 'Télé Fixe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(178, 1, 'customer.s1.adress', 'Adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(179, 1, 'customer.s1.email', 'E-Mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(180, 1, 'customer.s1.type', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(186, 1, 'customer.s1.active', 'Active ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(188, 1, 'p005.s2.priceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(202, 1, 'reception.s3', 'Ajouter des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(203, 1, 'reception.s3.id', 'Code d''article ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(204, 1, 'reception.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(205, 1, 'reception.s3.quantity', 'Qté ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(206, 1, 'reception.s3.unitPriceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(207, 1, 'reception.s3.remise', 'Remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(208, 1, 'reception.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(209, 1, 'reception.s3.tva', 'Tva', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(210, 1, 'reception.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(211, 1, 'globals.forms.confirmClean', 'Etes-vous sur de vouloir vider le formulaire ?', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(212, 1, 'globals.forms.clean', 'vider le fourmulaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(213, 1, 'reception.s4', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(214, 1, 'reception.s4.id', 'Code d''article ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(215, 1, 'reception.s4.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(216, 1, 'reception.s4.quantity', 'Qté ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(217, 1, 'reception.s4.unitPriceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(218, 1, 'reception.s4.remise', 'Remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(219, 1, 'reception.s4.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(220, 1, 'reception.s4.tva', 'Tva', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(221, 1, 'reception.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(222, 1, 'page1.s1.name', 'name msg', 2, 1, 'Y', NULL, NULL, NULL, NULL),
(223, 1, 'page1.s1.name', 'name help', 4, 1, 'Y', NULL, NULL, NULL, NULL),
(224, 1, 'globals.forms.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(225, 1, 'globals.forms.validate', 'Validate', 3, 2, 'Y', NULL, NULL, NULL, NULL),
(226, 1, 'receptionValidation.s1', 'La liste des réceptions qui sont prêt pour la validation', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(227, 1, 'receptionValidation.s1.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(228, 1, 'receptionValidation.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(229, 1, 'receptionValidation.s1.supplierId', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(230, 1, 'receptionValidation.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(231, 1, 'receptionValidation.s1.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(232, 1, 'receptionValidation.s1.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(233, 1, 'receptionValidation.s1.amount', 'Le montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(234, 1, 'receptionValidation.s1.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(235, 1, 'receptionValidation.s2', 'Détails de réception ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(236, 1, 'receptionValidation.s2.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(237, 1, 'receptionValidation.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(238, 1, 'receptionValidation.s2.supplier', 'Fournisseur ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(239, 1, 'receptionValidation.s2.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(240, 1, 'receptionValidation.s2.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(241, 1, 'receptionValidation.s2.amount', 'Montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(242, 1, 'receptionValidation.s2.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(243, 1, 'receptionValidation.s2.deposit', 'Depôt', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(244, 1, 'receptionValidation.s2.description', 'Description ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(245, 1, 'receptionValidation.s3', 'Ajouter des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(246, 1, 'receptionValidation.s3.id', 'Code d''article ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(247, 1, 'receptionValidation.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(248, 1, 'receptionValidation.s3.quantity', 'Qté ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(249, 1, 'receptionValidation.s3.unitPriceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(250, 1, 'receptionValidation.s3.remise', 'Remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(251, 1, 'receptionValidation.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(252, 1, 'receptionValidation.s3.tva', 'Tva', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(253, 1, 'receptionValidation.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(254, 1, 'receptionHistory.s1', 'Hisortique des réceptions', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(255, 1, 'receptionHistory.s1.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(256, 1, 'receptionHistory.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(257, 1, 'receptionHistory.s1.supplierId', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(258, 1, 'receptionHistory.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(259, 1, 'receptionHistory.s1.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(260, 1, 'receptionHistory.s1.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(261, 1, 'receptionHistory.s1.amount', 'Le montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(262, 1, 'receptionHistory.s1.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(263, 1, 'receptionHistory.s2', 'Détails de réception ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(264, 1, 'receptionHistory.s2.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(265, 1, 'receptionHistory.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(266, 1, 'receptionHistory.s2.supplier', 'Fournisseur ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(267, 1, 'receptionHistory.s2.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(268, 1, 'receptionHistory.s2.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(269, 1, 'receptionHistory.s2.amount', 'Montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(270, 1, 'receptionHistory.s2.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(271, 1, 'receptionHistory.s2.deposit', 'Depôt', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(272, 1, 'receptionHistory.s2.description', 'Description ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(273, 1, 'receptionHistory.s3', 'Ajouter des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(274, 1, 'receptionHistory.s3.id', 'Code d''article ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(275, 1, 'receptionHistory.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(276, 1, 'receptionHistory.s3.quantity', 'Qté ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(277, 1, 'receptionHistory.s3.unitPriceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(278, 1, 'receptionHistory.s3.remise', 'Remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(279, 1, 'receptionHistory.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(280, 1, 'receptionHistory.s3.tva', 'Tva', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(281, 1, 'receptionHistory.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(282, 1, 'receptionValidation.s4', 'Résumé', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(283, 1, 'receptionValidation.s4.quantityTotal', 'La quantité totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(284, 1, 'receptionValidation.s4.unitPriceBuyTotal', 'Le prix unitaire totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(285, 1, 'receptionValidation.s4.remiseAvg', 'La moyenne de remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(286, 1, 'receptionValidation.s4.tvaAvg', 'La monyenne de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(287, 1, 'receptionValidation.s4.totalHtTotal', 'hore taxt total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(288, 1, 'receptionValidation.s4.totalTtcTotal', 'TTC total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(289, 1, 'receptionHistory.s4', 'Résumé Général', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(290, 1, 'receptionHistory.s4.quantityTotal', 'La quantité totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(291, 1, 'receptionHistory.s4.unitPriceBuyTotal', 'Le prix unitaire totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(292, 1, 'receptionHistory.s4.remiseAvg', 'La moyenne de remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(293, 1, 'receptionHistory.s4.tvaAvg', 'La monyenne de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(294, 1, 'receptionHistory.s4.totalHtTotal', 'hore taxt total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(295, 1, 'receptionHistory.s4.totalTtcTotal', 'TTC total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(303, 1, 'reception.s5', 'Résumé Général', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(304, 1, 'reception.s5.quantityTotal', 'La quantité totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(305, 1, 'reception.s5.unitPriceBuyTotal', 'Le prix unitaire totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(306, 1, 'reception.s5.remiseAvg', 'La moyenne de remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(307, 1, 'reception.s5.tvaAvg', 'La monyenne de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(308, 1, 'reception.s5.totalHtTotal', 'hore taxt total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(309, 1, 'reception.s5.totalTtcTotal', 'TTC total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(310, 1, 'reception.s4.totalHt', 'Total HT', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(311, 1, 'reception.s4.totalTtc', 'Total TTC', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(312, 1, 'inventaire.s1', 'Inventaire générale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(313, 1, 'inventaire.s1.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(314, 1, 'inventaire.s1.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(315, 1, 'inventaire.s1.quantity', 'Quantité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(316, 1, 'inventaire.s1.priceBuy', 'Prix de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(317, 1, 'inventaire.s1.priceSale', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(318, 1, 'inventaire.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(319, 1, 'inventaire.s1.receptionValidCount', 'Nombre de réception', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(320, 1, 'inventaire.s1.unitPriceBuyMax', 'Max de prix de vent', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(321, 1, 'inventaire.s1.unitPriceBuyMoyenne', 'Moyenne de prix de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(322, 1, 'inventaire.s1.unitPriceBuyMin', 'Min de prix de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(323, 1, 'inventaire.s1.quantityCount', 'Total des quantité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(334, 1, 'login.s1.forgetPassword', 'Mot de passe oublié.', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(335, 1, 'login.s1.getAccount', 'Obtenir d''un compte', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(336, 1, 'login.s1.sessionActive', 'Garder ma session active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(337, 1, 'login.s1', 'Authentification', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(338, 1, 'module.s1.access', 'Accès au module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(339, 1, 'module.s1.logout', 'Déconnexion', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(340, 1, 'bonLivraison.s1', 'Nouveau bon de livraison', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(341, 1, 'bonLivraison.s1.customer', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(342, 1, 'bonLivraison.s1.description', 'Note', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(343, 1, 'bonLivraison.s2', 'Ajouter un produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(344, 1, 'bonLivraison.s2.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(345, 1, 'bonLivraison.s2.color', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(346, 1, 'bonLivraison.s2.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(347, 1, 'bonLivraison.s2.unitPriceBuy', 'Prix', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(348, 1, 'bonLivraison.s2.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(349, 1, 'bonLivraison.s2.totalTtc', 'Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(350, 1, 'bonLivraison.s3', 'La liste des articles', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(351, 1, 'bonLivraison.s3.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(352, 1, 'bonLivraison.s3.color', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(353, 1, 'bonLivraison.s3.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(354, 1, 'bonLivraison.s3.price', 'Prix Unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(355, 1, 'bonLivraison.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(356, 1, 'bonLivraison.s3.totalPriceBuy', 'Prix Totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(357, 1, 'bonLivraison.s4', 'Résumé Général', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(358, 1, 'bonLivraison.s5', 'Mode de paiement', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(359, 1, 'bonLivraison.s4.sumQuantity', 'Quantité Totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(360, 1, 'bonLivraison.s4.sumTotal', 'Montant Totale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(361, 1, 'blValidation.s1.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(362, 1, 'blValidation.s1.customerName', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(363, 1, 'blValidation.s1.description', 'Note', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(364, 1, 'blValidation.s1.sumTotalPriceBuy', 'Prix Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(365, 1, 'blValidation.s1', 'La liste des ventes attentes', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(366, 1, 'blValidation.s2', 'Modifer un vente', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(367, 1, 'blValidation.s2.customer', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(368, 1, 'blValidation.s2.description', 'Note', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(369, 1, 'blValidation.s3.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(370, 1, 'blValidation.s3.color', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(371, 1, 'blValidation.s3.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(372, 1, 'blValidation.s3.unitPriceBuy', 'Prix', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(373, 1, 'blValidation.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(374, 1, 'blValidation.s3.totalTtc', 'Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(375, 1, 'blValidation.s4', 'La liste des articles', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(376, 1, 'blValidation.s4.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(377, 1, 'blValidation.s4.color', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(378, 1, 'blValidation.s4.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(379, 1, 'blValidation.s4.price', 'Prix', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(380, 1, 'blValidation.s4.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(381, 1, 'blValidation.s4.totalPriceBuy', 'Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(382, 1, 'blValidation.s5', 'Résumé Général', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(383, 1, 'blValidation.s5.sumQuantity', 'Qté Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(384, 1, 'blValidation.s5.sumTotal', 'Montant Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(385, 1, 'blValidation.s5', 'Mode de paiement', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(386, 1, 'blValidation.s3', 'Ajouter un article', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(387, 1, 'blValidation.s1.sumQuantity', 'Qté Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(388, 1, 'blValidation.s6', 'Mode de paiement', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(389, 1, 'reception.s2.sizes', 'Taille de Réception', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(390, 1, 'inventaire.s1.productSizeName', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(391, 1, 'inventaire.s1.productColorName', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(393, 1, 'profile.s1.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(394, 1, 'profile.s1.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(395, 1, 'profile.s1.email', 'E-mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(396, 1, 'profile.s1.password', 'Mot de passe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(397, 1, 'profile.s1.replayRassword', 'Re mot de passe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(398, 1, 'profile.s1.cellPhone', 'Télé Mobile', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(399, 1, 'profile.s1.fixedPhone', 'Télé Fixe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(400, 1, 'profile.s1.adress', 'Adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(401, 1, 'profile.s1', 'Modifier Mon Profile', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(402, 1, 'basicParameter.s1', 'Basic Parameter', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(403, 1, 'basicParameter.s1.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(404, 1, 'basicParameter.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(405, 1, 'basicParameter.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(406, 1, 'basicParameter.s1.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(407, 1, 'basicParameter.s1.basicParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(408, 1, 'basicParameter.s2', 'Ajouter / Editer basic Parameter', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(409, 1, 'basicParameter.s2.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(410, 1, 'basicParameter.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(411, 1, 'basicParameter.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(412, 1, 'basicParameter.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(413, 1, 'basicParameter.s2.basicParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(414, 1, 'basicParameter.s1', 'Basic Parameter', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(415, 1, 'basicParameter.s1.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(416, 1, 'basicParameter.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(417, 1, 'basicParameter.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(418, 1, 'basicParameter.s1.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(419, 1, 'basicParameter.s1.basicParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(420, 1, 'basicParameter.s2', 'Ajouter / Editer basic Parameter', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(421, 1, 'basicParameter.s2.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(422, 1, 'basicParameter.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(423, 1, 'basicParameter.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(424, 1, 'basicParameter.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(425, 1, 'basicParameter.s2.basicParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(426, 1, 'moduleParameter.s1', 'Parameter du module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(427, 1, 'moduleParameter.s1.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(428, 1, 'moduleParameter.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(429, 1, 'moduleParameter.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(430, 1, 'moduleParameter.s1.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(431, 1, 'moduleParameter.s1.moduleParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(432, 1, 'moduleParameter.s2', 'Ajouter / Editer Parameter du module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(433, 1, 'moduleParameter.s2.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(434, 1, 'moduleParameter.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(435, 1, 'moduleParameter.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(436, 1, 'moduleParameter.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(437, 1, 'moduleParameter.s2.moduleParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(438, 1, 'parameterClient.s1', 'Liste du client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(439, 1, 'parameterClient.s1.name', 'Nom et prenom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(440, 1, 'parameterClient.s1.company', 'Societer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(441, 1, 'parameterClient.s1.email', 'Email', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(442, 1, 'parameterClient.s2', 'Parameter du client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(443, 1, 'parameterClient.s2.client', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(444, 1, 'parameterClient.s2.parameterName', 'Nom de parameter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(445, 1, 'parameterClient.s2.parameterTypeName', 'Type de parameter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(446, 1, 'parameterClient.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(447, 1, 'parameterClient.s2.defaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(448, 1, 'parameterClient.s3', 'Ajouter / Editer Parameter du client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(449, 1, 'parameterClient.s3.client', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(450, 1, 'parameterClient.s3.parameter', 'Nom de parameter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(451, 1, 'parameterClient.s3.parameterDefaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(452, 1, 'parameterClient.s3.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(453, 1, 'parameter.s1', 'Paramètres', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(454, 1, 'parameter.s1.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(455, 1, 'parameter.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(456, 1, 'parameter.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(457, 1, 'parameter.s1.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(458, 1, 'parameter.s1.parameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(459, 1, 'parameter.s2', 'Ajouter / Editer paramètres', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(460, 1, 'parameter.s2.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(461, 1, 'parameter.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(462, 1, 'parameter.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(463, 1, 'parameter.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(464, 1, 'parameter.s2.parameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(465, 1, 'pageParameterModule.s1', 'Liste du client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(466, 1, 'pageParameterModule.s1.name', 'Nom et prenom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(467, 1, 'pageParameterModule.s1.company', 'Societer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(468, 1, 'pageParameterModule.s1.email', 'Email', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(469, 1, 'pageParameterModule.s2', 'Liste du module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(470, 1, 'pageParameterModule.s2.nameModule', 'Nom du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(471, 1, 'pageParameterModule.s2.descriptionModule', 'Description du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(472, 1, 'pageParameterModule.s2.moduleTypeName', 'Type du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(473, 1, 'pageParameterModule.s3', 'Liste du page', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(474, 1, 'pageParameterModule.s3.client', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(475, 1, 'pageParameterModule.s3.pageName', 'Nom du page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(476, 1, 'pageParameterModule.s3.pageDescription', 'Description du page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(477, 1, 'pageParameterModule.s3.pageTypeName', 'Nom du type page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(478, 1, 'pageParameterModule.s4', 'Ajouter / Editer Parameter du page/module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(479, 1, 'pageParameterModule.s4.pageParameter', 'Nom de parameter du page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(480, 1, 'pageParameterModule.s4.pageParameterDefaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(481, 1, 'pageParameterModule.s4.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(482, 1, 'pageParameterModule.s5', 'Ajouter / Editer Parameter du page/module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(483, 1, 'pageParameterModule.s5.pageParameter', 'Nom de parameter du page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(484, 1, 'pageParameterModule.s5.pageParameterDefaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(485, 1, 'pageParameterModule.s5.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(486, 1, 'pageParameter.s1', 'Parameter du page', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(487, 1, 'pageParameter.s1.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(488, 1, 'pageParameter.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(489, 1, 'pageParameter.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(490, 1, 'pageParameter.s1.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(491, 1, 'pageParameter.s1.pageParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(492, 1, 'pageParameter.s1.page', 'Page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(493, 1, 'pageParameter.s2', 'Ajouter / Editer Parameter du page', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(494, 1, 'pageParameter.s2.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(495, 1, 'pageParameter.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(496, 1, 'pageParameter.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(497, 1, 'pageParameter.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(498, 1, 'pageParameter.s2.pageParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(499, 1, 'pageParameter.s2.page', 'Page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(500, 1, 'moduleParameterClient.s1', 'Liste du client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(501, 1, 'moduleParameterClient.s1.name', 'Nom et prenom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(502, 1, 'moduleParameterClient.s1.company', 'Societer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(503, 1, 'moduleParameterClient.s1.email', 'Email', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(504, 1, 'moduleParameterClient.s2', 'Liste du module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(505, 1, 'moduleParameterClient.s2.nameModule', 'Nom du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(506, 1, 'moduleParameterClient.s2.descriptionModule', 'Description du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(507, 1, 'moduleParameterClient.s2.moduleTypeName', 'Type du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(508, 1, 'moduleParameterClient.s3', 'Parameter du module/client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(509, 1, 'moduleParameterClient.s3.client', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(510, 1, 'moduleParameterClient.s3.parameterModuleName', 'Nom de parameter du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(511, 1, 'moduleParameterClient.s3.parameterModuleTypeName', 'Type de parameter du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(512, 1, 'moduleParameterClient.s3.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(513, 1, 'moduleParameterClient.s3.defaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(514, 1, 'moduleParameterClient.s4', 'Ajouter / Editer Parameter du module/client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(515, 1, 'moduleParameterClient.s4.client', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(516, 1, 'moduleParameterClient.s4.parameterModule', 'Nom de parameter du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(517, 1, 'moduleParameterClient.s4.parameterModuleDefaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(518, 1, 'moduleParameterClient.s4.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(519, 1, 'validation.v1.formatTypeFax', '{0} doit être de type fax', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(520, 1, 'validation.v1.formatTypeCodePostale', '{0} doit être de type code postale', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(521, 1, 'validation.v1.formatTypeTime', '{0} doit être de type time', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(522, 1, 'validation.v1.formatTypeDateTime', '{0} doit être de type datetime', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(523, 1, 'validation.v1.formatTypePattern', '{0} doit être un {1}', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(525, 1, 'p005.s2.productGroupName', 'Groupe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(526, 1, 'p005.s2.productTypeName', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(527, 1, 'customer.s1.secondaryAddress ', 'Adresse 2', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(528, 1, 'customer.s1.category ', 'Catégorie', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(529, 1, 'customer.s1.company', 'Société', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(530, 1, 'customer.s1.zipCode', 'Code postale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(531, 1, 'customer.s1.shortLabel', 'Libelle court', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(532, 1, 'customer.s1.fullLabel', 'Libelle complet', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(533, 1, 'customer.s1.webSite', 'Site web', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(534, 1, 'customer.s1.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(541, 1, 'p006.s2.code', 'Code réception', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(542, 1, 'p006.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(543, 1, 'p006.s2.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(544, 1, 'p006.s2.deadline', 'Date d''écheance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(545, 1, 'p006.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(546, 1, 'p006.s2.deposit', 'Dépôt', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(547, 1, 'p032.s1', 'La liste des commandes en cours', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(548, 1, 'p032.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(549, 1, 'p032.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(550, 1, 'p032.s1.supplierCompanyName', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(551, 1, 'p032.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(552, 1, 'p032.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(553, 1, 'p032.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(556, 1, 'p032.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(557, 1, 'p032.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(560, 1, 'p032.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(561, 1, 'p032.s2', 'Ajouter / Editer une commande', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(562, 1, 'p032.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(565, 1, 'p032.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(566, 1, 'p032.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(567, 1, 'p032.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(568, 1, 'p032.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(569, 1, 'p032.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(570, 1, 'p032.s3.unitPriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(571, 1, 'p032.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(572, 1, 'p032.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(573, 1, 'p032.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(574, 1, 'p032.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(575, 1, 'p032.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(576, 1, 'p032.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(577, 1, 'p032.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(578, 1, 'p032.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(579, 1, 'p032.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(580, 1, 'p032.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(581, 1, 'p032.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(582, 1, 'p032.s5.unitPriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(583, 1, 'p032.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(584, 1, 'p032.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(585, 1, 'pdf003.s1', 'Bon de réception', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(586, 1, 'pdf003.s1', 'La liste des réceptions en cours', 2, 1, 'Y', NULL, NULL, NULL, NULL),
(587, 1, 'pdf003.s2.seq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(588, 1, 'pdf003.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(589, 1, 'pdf003.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(590, 1, 'pdf003.s2.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(591, 1, 'pdf003.s2.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(592, 1, 'pdf003.s2.TotalQuantity', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(593, 1, 'pdf004.s1', 'Bon de réception', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(594, 1, 'pdf004.s1.reference', 'Référence ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(595, 1, 'pdf004.s1.status', 'Statut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(596, 1, 'pdf004.s1.supplier', ' Fournisseur ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(597, 1, 'pdf004.s1.the', 'Le : ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(598, 1, 'pdf004.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(599, 1, 'pdf004.s2.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(600, 1, 'pdf004.s2.unitPriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(601, 1, 'pdf004.s2.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(602, 1, 'pdf004.s2.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(603, 1, 'pdf004.s3.quantity', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(604, 1, 'pdf004.s3.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(605, 1, 'pdf004.s3.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(606, 1, 'pdf004.s3.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(607, 1, 'pdf004.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(608, 1, 'pdf004.s4.note', ' Note interne ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(609, 1, 'p005.s2.threshold', 'Seuil d''alerte', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(610, 1, 'p033.s1', 'La liste des commandes soumise', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(611, 1, 'p033.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(612, 1, 'p033.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(613, 1, 'p033.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(614, 1, 'p033.s1.supplierCompanyName', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(615, 1, 'p033.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(616, 1, 'p033.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(617, 1, 'p033.s2', 'Ajouter / Editer une commande', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(618, 1, 'p033.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(619, 1, 'p033.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(620, 1, 'p033.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(621, 1, 'p033.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(622, 1, 'p033.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(623, 1, 'p033.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(624, 1, 'p033.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(625, 1, 'p033.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(626, 1, 'p033.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(627, 1, 'p033.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(628, 1, 'p033.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(629, 1, 'p033.s3.unitPriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(630, 1, 'p033.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(631, 1, 'p033.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(632, 1, 'p033.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(633, 1, 'p033.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(634, 1, 'p033.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(635, 1, 'p033.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(636, 1, 'p033.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(637, 1, 'p033.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(638, 1, 'p033.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(639, 1, 'p033.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(640, 1, 'p033.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(641, 1, 'p033.s5.unitPriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(642, 1, 'p034.s1', 'La liste des commandes validée', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(643, 1, 'p034.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(644, 1, 'p034.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(645, 1, 'p034.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(646, 1, 'p034.s1.supplierCompanyName', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(647, 1, 'p034.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(648, 1, 'p034.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(649, 1, 'p034.s2', 'Ajouter / Editer une commande', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(650, 1, 'p034.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(651, 1, 'p034.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(652, 1, 'p034.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(653, 1, 'p034.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(654, 1, 'p034.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(655, 1, 'p034.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(656, 1, 'p034.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(657, 1, 'p034.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(658, 1, 'p034.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(659, 1, 'p034.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(660, 1, 'p034.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(661, 1, 'p034.s3.unitPriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(662, 1, 'p034.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(663, 1, 'p034.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(664, 1, 'p034.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(665, 1, 'p034.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(666, 1, 'p034.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(667, 1, 'p034.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(668, 1, 'p034.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(669, 1, 'p034.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(670, 1, 'p034.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(671, 1, 'p034.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(672, 1, 'p034.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(673, 1, 'p034.s5.unitPriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(674, 1, 'p032.s2.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(675, 1, 'p035.s1', 'La liste des réceptions en cours', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(676, 1, 'p035.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(677, 1, 'p035.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(678, 1, 'p035.s1.supplierCompanyName', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(679, 1, 'p035.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(680, 1, 'p035.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(681, 1, 'p035.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(682, 1, 'p035.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(683, 1, 'p035.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(684, 1, 'p035.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(685, 1, 'p035.s2', 'Ajouter / Editer une réception', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(686, 1, 'p035.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(687, 1, 'p035.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(688, 1, 'p035.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(689, 1, 'p035.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(690, 1, 'p035.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(691, 1, 'p035.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(692, 1, 'p035.s3.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(693, 1, 'p035.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(694, 1, 'p035.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(695, 1, 'p035.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(696, 1, 'p035.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(697, 1, 'p035.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(698, 1, 'p035.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(699, 1, 'p035.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(700, 1, 'p035.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(701, 1, 'p035.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(702, 1, 'p035.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(703, 1, 'p035.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(704, 1, 'p035.s5.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(705, 1, 'p035.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(706, 1, 'p035.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(707, 1, 'p035.s2.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(708, 1, 'pdf001.s1', 'Bon de commande fournisseur', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(709, 1, 'pdf001.s1', 'La liste des commande en cours', 2, 1, 'Y', NULL, NULL, NULL, NULL),
(710, 1, 'pdf001.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(711, 1, 'pdf001.s2.seq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(712, 1, 'pdf001.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(713, 1, 'pdf001.s2.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(714, 1, 'pdf001.s2.TotalQuantity', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(715, 1, 'pdf001.s2.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(716, 1, 'pdf002.s1', 'Bon de commande fournisseur', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(717, 1, 'pdf002.s1.reference', 'Référence ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(718, 1, 'pdf002.s1.status', 'Statut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(719, 1, 'pdf002.s1.supplier', ' Fournisseur ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(720, 1, 'pdf002.s1.the', 'Le : ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(721, 1, 'pdf002.s2.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(722, 1, 'pdf002.s2.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(723, 1, 'pdf002.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(724, 1, 'pdf002.s2.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(725, 1, 'pdf002.s2.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL);
INSERT INTO `inf_text` (`ID`, `PREFIX`, `ITEM_CODE`, `VALUE`, `TEXT_TYPE_ID`, `LANGUAGE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(726, 1, 'pdf002.s3.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(727, 1, 'pdf002.s3.quantity', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(728, 1, 'pdf002.s3.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(729, 1, 'pdf002.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(730, 1, 'pdf002.s3.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(731, 1, 'pdf002.s4.note', ' Note interne ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(732, 1, 'p036.s1', 'La liste des réceptions soumise', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(733, 1, 'p036.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(734, 1, 'p036.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(735, 1, 'p036.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(736, 1, 'p036.s1.supplierCompanyName', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(737, 1, 'p036.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(738, 1, 'p036.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(739, 1, 'p036.s2', 'Ajouter / Editer une réception', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(740, 1, 'p036.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(741, 1, 'p036.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(742, 1, 'p036.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(743, 1, 'p036.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(744, 1, 'p036.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(745, 1, 'p036.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(746, 1, 'p036.s2.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(747, 1, 'p036.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(748, 1, 'p036.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(749, 1, 'p036.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(750, 1, 'p036.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(751, 1, 'p036.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(752, 1, 'p036.s3.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(753, 1, 'p036.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(754, 1, 'p036.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(755, 1, 'p036.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(756, 1, 'p036.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(757, 1, 'p036.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(758, 1, 'p036.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(759, 1, 'p036.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(760, 1, 'p036.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(761, 1, 'p036.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(762, 1, 'p036.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(763, 1, 'p036.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(764, 1, 'p036.s5.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(765, 1, 'p037.s1', 'La liste des réceptions validée', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(766, 1, 'p037.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(767, 1, 'p037.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(768, 1, 'p037.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(769, 1, 'p037.s1.supplierCompanyName', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(770, 1, 'p037.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(771, 1, 'p037.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(772, 1, 'p037.s2', 'Ajouter / Editer une réception', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(773, 1, 'p037.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(774, 1, 'p037.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(775, 1, 'p037.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(776, 1, 'p037.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(777, 1, 'p037.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(778, 1, 'p037.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(779, 1, 'p037.s2.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(780, 1, 'p037.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(781, 1, 'p037.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(782, 1, 'p037.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(783, 1, 'p037.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(784, 1, 'p037.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(785, 1, 'p037.s3.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(786, 1, 'p037.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(787, 1, 'p037.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(788, 1, 'p037.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(789, 1, 'p037.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(790, 1, 'p037.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(791, 1, 'p037.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(792, 1, 'p037.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(793, 1, 'p037.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(794, 1, 'p037.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(795, 1, 'p037.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(796, 1, 'p037.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(797, 1, 'p037.s5.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(798, 1, 'p035.s2.commande', 'Commande', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(799, 1, 'p038.s1', 'La liste des ventes en cours', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(800, 1, 'p038.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(801, 1, 'p038.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(802, 1, 'p038.s1.supplierCompanyName', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(803, 1, 'p038.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(804, 1, 'p038.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(805, 1, 'p038.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(806, 1, 'p038.s2.supplier', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(807, 1, 'p038.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(808, 1, 'p038.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(809, 1, 'p038.s2', 'Ajouter / Editer un vente', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(810, 1, 'p038.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(811, 1, 'p038.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(812, 1, 'p038.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(813, 1, 'p038.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(814, 1, 'p038.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(815, 1, 'p038.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(816, 1, 'p038.s3.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(817, 1, 'p038.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(818, 1, 'p038.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(819, 1, 'p038.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(820, 1, 'p038.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(821, 1, 'p038.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(822, 1, 'p038.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(823, 1, 'p038.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(824, 1, 'p038.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(825, 1, 'p038.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(826, 1, 'p038.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(827, 1, 'p038.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(828, 1, 'p038.s5.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(829, 1, 'p038.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(830, 1, 'p038.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(831, 1, 'p038.s2.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(832, 1, 'p038.s2.commande', 'Commande', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(833, 1, 'p038.s3.unitPriceSale', 'Prix vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(834, 1, 'p038.s5.unitPriceSale', 'Prix vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(835, 1, 'pdf005.s1', 'Bon de vente', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(836, 1, 'pdf005.s1', 'La liste des ventes en cours', 2, 1, 'Y', NULL, NULL, NULL, NULL),
(837, 1, 'pdf005.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(838, 1, 'pdf005.s2.seq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(839, 1, 'pdf005.s2.supplier', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(840, 1, 'pdf005.s2.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(841, 1, 'pdf005.s2.TotalQuantity', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(842, 1, 'pdf005.s2.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(843, 1, 'pdf006.s1', 'Bon de vente', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(844, 1, 'pdf006.s1.reference', 'Référence ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(845, 1, 'pdf006.s1.status', 'Statut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(846, 1, 'pdf006.s1.supplier', ' Client ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(847, 1, 'pdf006.s1.the', 'Le : ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(848, 1, 'pdf006.s2.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(849, 1, 'pdf006.s2.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(850, 1, 'pdf006.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(851, 1, 'pdf006.s2.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(852, 1, 'pdf006.s2.unitPriceBuy', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(853, 1, 'pdf006.s3.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(854, 1, 'pdf006.s3.quantity', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(855, 1, 'pdf006.s3.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(856, 1, 'pdf006.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(857, 1, 'pdf006.s3.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(858, 1, 'pdf006.s4.note', ' Note interne ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(859, 1, 'p041.s1', 'La liste des bons de retour en cours', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(860, 1, 'p041.s1.noSeq', 'Séq', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(861, 1, 'p041.s1.sumProduct', 'Nbr des produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(862, 1, 'p041.s1.customerCompanyName', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(863, 1, 'p041.s1.totalHtTotal', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(864, 1, 'p041.s1.totalTtcTotal', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(865, 1, 'p041.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(866, 1, 'p041.s2.customer', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(867, 1, 'p041.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(868, 1, 'p041.s2.return', 'Retour à la liste', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(869, 1, 'p041.s2', 'Ajouter / Editer un bon de retour', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(870, 1, 'p041.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(871, 1, 'p041.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(872, 1, 'p041.s2.submit', 'Soumitter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(873, 1, 'p041.s3', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(874, 1, 'p041.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(875, 1, 'p041.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(876, 1, 'p041.s3.negotiatePriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(877, 1, 'p041.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(878, 1, 'p041.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(879, 1, 'p041.s4', 'Résumé globale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(880, 1, 'p041.s4.countProducts', 'Nombre de produits', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(881, 1, 'p041.s4.totalPu', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(882, 1, 'p041.s4.valueTva', 'La valeur de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(883, 1, 'p041.s4.amountTva', 'Le montant de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(884, 1, 'p041.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(885, 1, 'p041.s5', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(886, 1, 'p041.s5.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(887, 1, 'p041.s5.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(888, 1, 'p041.s5.negotiatePriceSale', 'Prix unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(889, 1, 'p041.s5.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(890, 1, 'p041.s5.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(891, 1, 'p041.s2.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(892, 1, 'p041.s2.commande', 'Commande', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(893, 1, 'login.s1.password', 'Mot de passe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(909, 1, 'contactManagement.s1', 'Adresses', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(910, 1, 'contactManagement.s2', 'Téléphones', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(911, 1, 'contactManagement.s3', 'Faxes', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(912, 1, 'contactManagement.s4', 'Sites web', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(913, 1, 'contactManagement.s5', 'Emails', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(914, 1, 'contactManagement.s1.locationTypeName', 'Type d''adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(915, 1, 'contactManagement.s1.addressLine', 'Adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(916, 1, 'contactManagement.s1.postalCode', 'Code postal', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(917, 1, 'contactManagement.s1.infCountryName', 'Pays', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(918, 1, 'contactManagement.s1.infCityName', 'Ville', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(919, 1, 'contactManagement.s1.priority', 'Priorité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(920, 1, 'contactManagement.s1.ctaLocationPostalCode', 'Code postal', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(921, 1, 'contactManagement.s1.ctaLocationPriority', 'Priorité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(922, 1, 'contactManagement.s1.ctaLocationLine1', 'Ligne 1 ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(923, 1, 'contactManagement.s1.ctaLocationLine2', 'Ligne 2 ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(924, 1, 'contactManagement.s1.ctaLocationLine3', 'Ligne 3 ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(925, 1, 'contactManagement.s2.phoneTypeName', 'Type d''adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(926, 1, 'contactManagement.s2.countryCode', 'Indicatif', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(927, 1, 'contactManagement.s2.number', 'Numéro', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(928, 1, 'contactManagement.s2.priority', 'Priorité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(929, 1, 'contactManagement.s2.ctaPhoneCountryCode', 'Indicatif', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(930, 1, 'contactManagement.s2.ctaPhoneNumber', 'Numéro', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(931, 1, 'contactManagement.s2.ctaPhonePriority', 'Priorité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(932, 1, 'contactManagement.s3.faxTypeName', 'Type de faxe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(933, 1, 'contactManagement.s3.countryCode', 'Indicatif', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(934, 1, 'contactManagement.s3.number', 'Numéro', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(935, 1, 'contactManagement.s3.priority', 'Priorité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(936, 1, 'contactManagement.s3.ctaFaxCountryCode', 'Indicatif', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(937, 1, 'contactManagement.s3.ctaFaxNumber', 'Numéro', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(938, 1, 'contactManagement.s3.ctaFaxPriority', 'Priorité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(939, 1, 'contactManagement.s4.webTypeName', 'Type de sites web', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(940, 1, 'contactManagement.s4.externalUrl', 'Le lien', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(941, 1, 'contactManagement.s4.priority', 'Priorité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(942, 1, 'contactManagement.s4.ctaWebUrl', 'Le lien', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(943, 1, 'contactManagement.s4.ctaWebPriority', 'Priorité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(944, 1, 'contactManagement.s5.emailTypeName', 'Type d''email', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(945, 1, 'contactManagement.s5.emailAddress', 'E-mail adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(946, 1, 'contactManagement.s5.priority', 'Priorité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(947, 1, 'contactManagement.s5.ctaEmailAdresse', 'E-mail adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(948, 1, 'contactManagement.s5.ctaEmailPriority', 'Priorité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(949, 1, 'p009.s1', 'La liste des produits qui sont en arrivage', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(950, 1, 'p009.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(951, 1, 'p009.s1.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(952, 1, 'p009.s1.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(953, 1, 'p009.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(954, 1, 'p009.s2.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(955, 1, 'p009.s2.qteCommanded', 'Qté commandée', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(956, 1, 'p009.s2.qteRecieved', 'Qté reçue', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(957, 1, 'p009.s2.supplierCompanyName', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(958, 1, 'globals.forms.search', 'Rechercher', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(959, 1, 'p009.s2.orderSupplierReference', 'Réf de commande', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(960, 1, 'p009.s1.orderSupplierReference', 'Réf de commande', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(961, 1, 'p009.s2.orderSupplierReference', 'Réf de commande', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(962, 1, 'p009.s1.orderSupplierReference', 'Réf de commande', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(963, 1, 'p008.s1', 'Alerte de stocks', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(964, 1, 'p008.s1.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(965, 1, 'p008.s1.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(966, 1, 'p008.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(967, 1, 'p008.s2.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(968, 1, 'p008.s2.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(969, 1, 'p008.s2.threshold', 'Seuil d''alerte', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(970, 1, 'p008.s2.totalCommanded', 'Qté commandée', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(971, 1, 'p008.s2.totalRecieved', 'Qté reçue', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(972, 1, 'p005.s2.priceSale', 'Prix de vente ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(973, 1, 'p005.s2.threshold', 'Seuil d''alerte', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(974, 1, 'p005.s3.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(975, 1, 'p005.s3.additionalInformation', 'Information supplémentaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(976, 1, 'p005.s3.basicInformation', 'Informations de base', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(977, 1, 'p005.s3.color', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(978, 1, 'p005.s3.designation', 'Désignation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(979, 1, 'p005.s3.family', 'Famille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(980, 1, 'p005.s3.group', 'Groupe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(981, 1, 'p005.s3.note', 'Note', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(982, 1, 'p005.s3.priceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(983, 1, 'p005.s3.priceSale', 'Prix de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(984, 1, 'p005.s3.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(985, 1, 'p005.s3.reorganization', 'Réorganisation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(986, 1, 'p005.s3.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(987, 1, 'p005.s3.threshold', 'Seuil d''alerte', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(988, 1, 'p005.s3.type', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(989, 1, 'p005.s3.unit', 'Unité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(990, 1, 'p012.s1', 'Produits : Information générale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(991, 1, 'p012.s1.productTotalQuantity', 'La quantité totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(992, 1, 'p012.s2', 'Commande : Information générale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(993, 1, 'p012.s2.orderSupplierInProgressCount', 'Opérations en cours', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(994, 1, 'p012.s2.orderSupplierTransmittedCount', 'Opérations soumise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(995, 1, 'p012.s2.orderSupplierValidatedCount', 'Opérations validées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(996, 1, 'p012.s3', 'Réceptions : Information générale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(997, 1, 'p012.s3.receptionInProgressCount', 'Opérations en cours', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(998, 1, 'p012.s3.receptionTransmittedCount', 'Opérations soumise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(999, 1, 'p012.s3.receptionValidatedCount', 'Opérations validées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1000, 1, 'p012.s4.orderInProgressCount', 'Opérations en cours', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1001, 1, 'p012.s4.orderTransmittedCount', 'Opérations soumise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1002, 1, 'p012.s4.orderValidatedCount', 'Opérations validées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1003, 1, 'p012.s5', 'Bon de retour : Information générale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(1004, 1, 'p012.s5.returnReceiptInProgressCount', 'Opérations en cours', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1005, 1, 'p012.s5.returnReceiptTransmittedCount', 'Opérations soumise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1006, 1, 'p012.s5.returnReceiptValidatedCount', 'Opérations validées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1008, 1, 'p012.s4', 'Ventes : Information générale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(1009, 1, 'p004.s1', 'Liste des fournisseurs', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(1010, 1, 'p004.s1.companyName', 'Entreprise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1011, 1, 'p004.s1.name', 'Nom complet', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1012, 1, 'p004.s1.phone', 'Télé\n', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1013, 1, 'p004.s1.email', 'E-mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1014, 1, 'p004.s1.type', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1015, 1, 'p004.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1017, 1, 'p004.s2', 'Ajouter / Modifier Fournisseur ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(1018, 1, 'p004.s2.nature', 'Nature', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1019, 1, 'p004.s2.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1020, 1, 'p004.s2.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1021, 1, 'p004.s2.companyName', 'Nom de l''entreprise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1022, 1, 'p004.s2.representative', 'Représentant', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1023, 1, 'p004.s2.shortLabel', 'Libellé court', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1024, 1, 'p004.s2.fullLabel', 'Libellé complète', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1025, 1, 'p004.s2.category', 'Catégorie', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1026, 1, 'p004.s2.type', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1027, 1, 'p004.s2.note', 'Note', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1028, 1, 'p004.s2.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1029, 1, 'p007.s1', 'Liste des clients', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(1030, 1, 'p007.s1.companyName', 'Entreprise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1031, 1, 'p007.s1.name', 'Nom complet', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1032, 1, 'p007.s1.phone', 'Télé\n', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1033, 1, 'p007.s1.email', 'E-mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1034, 1, 'p007.s1.type', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1035, 1, 'p007.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1036, 1, 'p007.s2', 'Ajouter / Modifier Client ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(1037, 1, 'p007.s2.nature', 'Nature', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1038, 1, 'p007.s2.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1039, 1, 'p007.s2.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1040, 1, 'p007.s2.companyName', 'Nom de l''entreprise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1041, 1, 'p007.s2.representative', 'Représentant', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1042, 1, 'p007.s2.shortLabel', 'Libellé court', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1043, 1, 'p007.s2.fullLabel', 'Libellé complète', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1044, 1, 'p007.s2.category', 'Catégorie', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1045, 1, 'p007.s2.type', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1046, 1, 'p007.s2.note', 'Note', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1047, 1, 'p007.s2.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1048, 1, 'p012.s2.orderSupplierRefusedCount', 'Opérations réfusées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1049, 1, 'p012.s2.orderSupplierRejectedCount', 'Opérations rejetées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1050, 1, 'p012.s3.receptionRefusedCount', 'Opérations réfusées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1051, 1, 'p012.s3.receptionRejectedCount', 'Opérations rejetées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1052, 1, 'p012.s4.orderdRefusedCount', 'Opérations réfusées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1053, 1, 'p012.s4.orderdRejectedCount', 'Opérations rejetées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1054, 1, 'p012.s5.returnReceiptRefusedCount', 'Opérations réfusées', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(1055, 1, 'p012.s5.returnReceiptRejectedCount', 'Opérations rejetées', 3, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_text_type`
--

CREATE TABLE `inf_text_type` (
`ID` bigint(20) NOT NULL,
  `CODE` tinytext CHARACTER SET latin1,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_text_type`
--

INSERT INTO `inf_text_type` (`ID`, `CODE`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'title', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'message', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'label', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'help', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(5, 'comment', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(6, 'placeholder', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(7, 'error', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_attribute_validation`
--

CREATE TABLE `pm_attribute_validation` (
`ID` bigint(20) NOT NULL,
  `PAGE_ATTRIBUTE_ID` bigint(20) DEFAULT NULL,
  `VALIDATION_VALIDATION_ID` bigint(20) DEFAULT NULL,
  `PARAMS` mediumtext CHARACTER SET latin1,
  `CUSTOM_ERROR` text CHARACTER SET latin1,
  `MODEL_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pm_category`
--

CREATE TABLE `pm_category` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT NULL,
  `CATEGORY_TYPE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `IMAGE_PATH` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_category`
--

INSERT INTO `pm_category` (`ID`, `NAME`, `DESCRIPTION`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `ACTIVE`, `CATEGORY_TYPE_ID`, `SORT_KEY`, `IMAGE_PATH`) VALUES
(1, 'Fichiers', 'Fichiers', NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(2, 'Entrées', 'Entrées', NULL, NULL, NULL, NULL, 'Y', 1, 2, 'icons/1.png'),
(3, 'Sorties', 'Sorties', NULL, NULL, NULL, NULL, 'Y', 1, 3, 'icons/1.png'),
(4, 'Divers', 'Divers', '2014-11-13 00:00:00', NULL, NULL, NULL, 'Y', 1, 4, 'icons/1.png'),
(5, 'Statistiques', 'Rapport', NULL, NULL, NULL, NULL, 'Y', 1, 6, 'icons/1.png'),
(6, 'Recourcie', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 7, 'icons/1.png'),
(7, 'Generale', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(8, 'Ressource', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 2, 'icons/1.png'),
(9, 'Module', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 3, 'icons/1.png'),
(10, 'Parametrage', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 4, 'icons/1.png'),
(11, 'Generale', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(12, 'Ressource', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(13, 'Modules', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(14, 'Paramètrage', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(15, 'Paramètrage', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(16, 'Commandes', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png'),
(17, 'Retour - Avoir', NULL, NULL, NULL, NULL, NULL, 'Y', 1, 1, 'icons/1.png');

-- --------------------------------------------------------

--
-- Structure de la table `pm_category_type`
--

CREATE TABLE `pm_category_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext CHARACTER SET latin1,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_category_type`
--

INSERT INTO `pm_category_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Header', 'DFFF', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Sub Header', ' FGGE', 2, 'Y', NULL, NULL, NULL, NULL),
(3, 'Principal', ' FERT', 3, 'Y', NULL, NULL, NULL, NULL),
(4, 'Footer', 'FFZF', 5, 'Y', NULL, NULL, NULL, NULL),
(5, 'Footer', '  GGDZ', 4, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_component`
--

CREATE TABLE `pm_component` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` tinytext,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` tinytext,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_component`
--

INSERT INTO `pm_component` (`ID`, `NAME`, `DESCRIPTION`, `ACTIVE`, `SORT_KEY`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'inpuText', NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'textarea', NULL, 'Y', 2, NULL, NULL, NULL, NULL),
(3, 'comobox', NULL, 'Y', 3, NULL, NULL, NULL, NULL),
(4, 'table', NULL, 'Y', 4, NULL, NULL, NULL, NULL),
(5, 'column', NULL, 'Y', 5, NULL, NULL, NULL, NULL),
(6, 'other', NULL, 'Y', 6, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_composition`
--

CREATE TABLE `pm_composition` (
`ID` bigint(20) NOT NULL,
  `GROUP_ID` bigint(20) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `MENU_ID` bigint(20) DEFAULT NULL,
  `PAGE_ID` bigint(20) DEFAULT NULL,
  `INDEX_SHOW` char(1) DEFAULT NULL,
  `GROUP_SORT` bigint(20) DEFAULT NULL,
  `MENU_SORT` bigint(20) DEFAULT NULL,
  `CATEGORY_SORT` bigint(20) DEFAULT NULL,
  `PAGE_SORT` bigint(20) DEFAULT NULL,
  `INDEX_SORT` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `MODEL_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=219 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_composition`
--

INSERT INTO `pm_composition` (`ID`, `GROUP_ID`, `CATEGORY_ID`, `MENU_ID`, `PAGE_ID`, `INDEX_SHOW`, `GROUP_SORT`, `MENU_SORT`, `CATEGORY_SORT`, `PAGE_SORT`, `INDEX_SORT`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `MODEL_ID`) VALUES
(1, 3, 1, 1, 5, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(2, 3, 1, 2, 12, '', 1, 3, 1, 2, 0, 'Y', NULL, NULL, NULL, NULL, 1),
(4, 3, 1, 4, 4, '', 1, 4, 1, 6, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(5, 3, 2, 5, 35, NULL, 1, 2, 3, 7, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(6, 3, 2, 6, 36, NULL, 1, 3, 3, 8, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(7, 3, 2, 7, 37, NULL, 1, 4, 3, 9, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(8, 3, 1, 8, 7, NULL, 1, 5, 1, 10, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(9, 3, 3, 9, 38, NULL, 1, 15, 4, 11, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(10, 3, 3, 10, 39, NULL, 1, 16, 4, 12, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(11, 3, 3, 11, 40, NULL, 1, 17, 4, 13, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(15, 3, 5, 15, 14, NULL, 1, 21, 6, 15, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(16, 2, 6, 16, 5, NULL, 1, 22, 22, 16, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(17, 2, 6, 17, 8, NULL, 1, 23, 23, 17, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(18, 2, 6, 18, 9, NULL, 1, 24, 24, 18, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(19, 2, 6, 19, 35, NULL, 1, 25, 25, 19, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(20, 2, 6, 20, 38, NULL, 1, 29, 29, 24, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(72, 3, 11, 26, 28, NULL, 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 3),
(188, 3, 7, 21, 1, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 2),
(190, 3, 8, 22, 1, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 2),
(193, 3, 9, 23, 31, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 2),
(196, 3, 9, 24, 27, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 2),
(197, 3, 12, 27, 30, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 3),
(198, 3, 10, 25, 26, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 2),
(199, 3, 13, 28, 29, NULL, 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 3),
(200, 3, 14, 29, 25, NULL, 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 3),
(201, 3, 15, 30, 21, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 4),
(202, 3, 15, 31, 22, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 4),
(203, 3, 15, 32, 23, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 4),
(204, 3, 15, 33, 24, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 4),
(206, 3, 10, 35, 19, NULL, 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 2),
(207, 1, 1, 36, 20, NULL, 1, 30, 30, 25, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(208, 1, 1, 36, 20, NULL, 1, 23, 23, 23, NULL, 'Y', NULL, NULL, NULL, NULL, 2),
(209, 1, 1, 36, 20, NULL, 1, 24, 24, 24, NULL, 'Y', NULL, NULL, NULL, NULL, 3),
(210, 1, 1, 36, 20, NULL, 1, 25, 25, 25, NULL, 'Y', NULL, NULL, NULL, NULL, 4),
(211, 3, 16, 38, 32, NULL, 1, 3, 2, 3, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(212, 3, 16, 39, 33, NULL, 1, 4, 2, 4, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(213, 3, 16, 40, 34, NULL, 1, 5, 2, 5, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(214, 3, 17, 41, 41, NULL, 1, 26, 5, 20, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(215, 3, 17, 42, 42, NULL, 1, 27, 5, 22, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(216, 3, 17, 43, 43, NULL, 1, 28, 5, 23, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(217, 3, 1, 44, 8, NULL, 1, 2, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(218, 3, 2, 45, 9, NULL, 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `pm_data_type`
--

CREATE TABLE `pm_data_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `PARAM_NUMBER` bigint(20) DEFAULT '1',
  `PARAMS` tinytext,
  `ERROR_MESSAGE` tinytext,
  `HELP` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_data_type`
--

INSERT INTO `pm_data_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `PARAM_NUMBER`, `PARAMS`, `ERROR_MESSAGE`, `HELP`) VALUES
(1, 'Texte', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 1, 'String', 'validation.v1.dataTypeString', NULL),
(2, 'Caractère', NULL, 2, 'Y', NULL, NULL, NULL, NULL, 1, 'Char', 'validation.v1.dataTypeChar', NULL),
(3, 'Nombre entière', NULL, 3, 'Y', NULL, NULL, NULL, NULL, 1, 'Integer', 'validation.v1.dataTypeInteger', NULL),
(4, 'Nombre réel', NULL, 4, 'Y', NULL, NULL, NULL, NULL, 1, 'Double', 'validation.v1.dataTypeDouble', NULL),
(5, 'datetime', NULL, 5, 'Y', NULL, NULL, NULL, NULL, 1, 'DateTime', 'validation.v1.dataTypeDateTime', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_format_type`
--

CREATE TABLE `pm_format_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `PARAM_NUMBER` bigint(20) DEFAULT '1',
  `PARAMS` tinytext,
  `ERROR_MESSAGE` tinytext,
  `HELP` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_format_type`
--

INSERT INTO `pm_format_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `PARAM_NUMBER`, `PARAMS`, `ERROR_MESSAGE`, `HELP`) VALUES
(1, 'None', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'none', 'validation.v1.formatTypeNone', NULL),
(2, 'Email', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'mail', 'validation.v1.formatTypeEmail', NULL),
(3, 'Phone', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'phone', 'validation.v1.formatTypePhone', NULL),
(4, 'Fax', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'fax', 'validation.v1.formatTypeFax', NULL),
(5, 'Code postale', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'codePostale', 'validation.v1.formatTypeCodePostale', NULL),
(6, 'Date', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'date', 'validation.v1.formatTypeDate', NULL),
(7, 'Time', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'time', 'validation.v1.formatTypeTime', NULL),
(8, 'DateTime', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'datetime', 'validation.v1.formatTypeDateTime', NULL),
(9, 'Pattern', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, NULL, 'validation.v1.formatTypePattern', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_group`
--

CREATE TABLE `pm_group` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `GROUP_TYPE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_group`
--

INSERT INTO `pm_group` (`ID`, `NAME`, `DESCRIPTION`, `GROUP_TYPE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Header', 'header', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'sub Header', 'sub Header', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(3, 'Menu', 'Menu', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(4, 'Footer', 'Footer', 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_group_type`
--

CREATE TABLE `pm_group_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_group_type`
--

INSERT INTO `pm_group_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Simple', 'Simple', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Avancée', 'Avancée', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_menu`
--

CREATE TABLE `pm_menu` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `MENU_TYPE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `IMAGE_PATH` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_menu`
--

INSERT INTO `pm_menu` (`ID`, `NAME`, `DESCRIPTION`, `MENU_TYPE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `IMAGE_PATH`) VALUES
(1, 'Produit', 'Page de TEST', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/product.png'),
(2, 'Statut de stock', 'Gestion des produits', 1, 2, 'Y', NULL, NULL, NULL, NULL, 'icons/status.png'),
(3, 'Inventaire', 'Gestion des dépenses', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(4, 'Gestion des Fournisseurs', 'Gestion des fournisseurs   ', 1, NULL, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(5, 'Reception', 'Gestion des réception', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(6, 'Validation des réceptions', 'Réception', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(7, 'Historique des réceptions', 'Dépense', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(8, 'Gestion des Clients', 'Fournisseurs', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(9, 'Nouveau Bon laivraison', 'Produits', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(10, 'Validation des BL', 'Réception', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(11, 'Historiques des BL', 'Client', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(12, 'Changer un produit', 'Gestion des clients', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(13, 'Dépenses', 'Validation des réceptions', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/coins.png'),
(14, 'Avance de produit', 'Historique des réceptions', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(15, 'Rapports et statistiques', 'Promotion des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/chart.png'),
(16, 'Produit', 'Produit', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/onebit_63.png'),
(17, 'Seuil d''alerte', 'Seuil d''alerte', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/onebit_19.png'),
(18, 'En arrivage', 'En arrivage', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/onebit_18.png'),
(19, 'Réception', 'Réception', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/onebit_85.png'),
(20, 'Ventes', 'Ventes', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/onebit_66.png'),
(21, 'Information générale', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(22, 'Rôle d''utilisateur', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(23, 'Proprietes des elements', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(24, 'Configuration des pages', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(25, 'Configuration générale', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(26, 'Information', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(27, 'Utilisateur', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(28, 'Modules', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(29, 'Parametrage', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(30, 'Les paramèetres Basique', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(31, 'Les paramèetres Client', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(32, 'Les paramèetres module', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(33, 'Les paramèetres de page', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(34, 'Promotions de produits', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(35, 'Liste des valeurs', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(36, 'Mon Profile', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/4.png'),
(37, 'to del', 'Configuration des modules', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(38, 'Commandes', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(39, 'Validation commande', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(40, 'Historique commande', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(41, 'Bon de retour', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(42, 'Validation Bon de retour', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(43, 'Historique bon de retour', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png'),
(44, 'Alert produit', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/warning.png'),
(45, 'En arrivage', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL, 'icons/2.png');

-- --------------------------------------------------------

--
-- Structure de la table `pm_menu_type`
--

CREATE TABLE `pm_menu_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_menu_type`
--

INSERT INTO `pm_menu_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'menu1 typ', 'Menu 1 type ', 1, 'Y', NULL, NULL, NULL, NULL),
(4, 'menu type 2', ' mznu type 2', 2, 'Y', NULL, NULL, NULL, NULL),
(6, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_model`
--

CREATE TABLE `pm_model` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `INF_PACKAGE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(20) DEFAULT 'Y'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_model`
--

INSERT INTO `pm_model` (`ID`, `NAME`, `DESCRIPTION`, `INF_PACKAGE_ID`, `ACTIVE`) VALUES
(1, 'Modél de Stock', 'Modél de Stock', 1, 'Y'),
(2, 'Model d''admin de stock', 'Model d''admin de stock', 1, 'Y'),
(3, 'Modél de super admin', 'Modél de super admin', 1, 'Y'),
(4, 'Modél de webmaster', 'Modél de webmaster', 1, 'Y');

-- --------------------------------------------------------

--
-- Structure de la table `pm_model_status`
--

CREATE TABLE `pm_model_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pm_page`
--

CREATE TABLE `pm_page` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `PAGE_TYPE_ID` bigint(20) DEFAULT NULL,
  `IN_DEV` char(1) DEFAULT 'N',
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1009 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_page`
--

INSERT INTO `pm_page` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `PAGE_TYPE_ID`, `IN_DEV`, `ACTIVE`, `USER_CREATION`, `DATE_CREATION`, `USER_UPDATE`, `DATE_UPDATE`) VALUES
(1, 'page Test', 'page test1', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(2, 'Page add admin ', 'Page add admin', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(3, 'Page Expense', 'Page pour la gestion des dépense', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(4, 'Page Supplier', 'Page pour la gestion des fournisseurs', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(5, 'Page produit', 'Page pour la gestion des produits', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(6, 'Page de réception', 'Page pour la gestion des réceptions et BL', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(7, 'Page de custmer', 'customer', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(8, 'Alert produit', 'Alert produit', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(9, 'En arrivage', 'En arrivage', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(10, 'Page de promotion des prodtuis', 'promo produit', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(11, 'Inventaire', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(12, 'statut de stock : capital', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(13, 'Bon de livraison Client', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(14, 'Valdation de Bon de livraison Client', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(15, 'Historique de Bon de livraison Client', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(16, 'les rapports cotidiennet', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(17, 'Changé un produit', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(18, 'avance de produit', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(19, 'la liste des valeurs', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(20, 'Mon profil', NULL, 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(21, 'Les paramètres basiques', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(22, 'Paramètres', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(23, 'Parameter du module', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(24, 'Parameter du page', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(25, 'Parameter du client', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(26, 'Parameter du module/client', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(27, 'Parameter du page/module', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(28, 'Gestion des clients', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(29, 'Gestion des modules', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(30, 'Gestion des utilisateurs', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(31, 'Proprietes des elements', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(32, 'Commande fournisseur', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(33, 'Validation commande fournisseur', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(34, 'Historique commande fournisseur', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(35, 'Receptions en cours', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(36, 'Validation des receptions', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(37, 'Historique des validations', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(38, 'Ventes en cours', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(39, 'Validation des ventes', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(40, 'Historique des ventes', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(41, 'Avoir en cours', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(42, 'Validation Bon de retour', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(43, 'Historique Bon de retour', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(1001, 'Afficher la liste des commandes ', NULL, NULL, 2, 'N', 'Y', NULL, NULL, NULL, NULL),
(1002, 'Afficher le détails d''une commande', NULL, NULL, 2, 'N', 'Y', NULL, NULL, NULL, NULL),
(1003, 'Afficher la liste des réceptions', NULL, NULL, 2, 'N', 'Y', NULL, NULL, NULL, NULL),
(1004, 'Afficher le détails d''une récéption', NULL, NULL, 2, 'N', 'Y', NULL, NULL, NULL, NULL),
(1005, 'Afficher la liste des ventes ', NULL, NULL, 2, 'N', 'Y', NULL, NULL, NULL, NULL),
(1006, 'Afficher le détails d''une vente', NULL, NULL, 2, 'N', 'Y', NULL, NULL, NULL, NULL),
(1007, 'Afficher la liste des avoirs', NULL, NULL, 2, 'N', 'Y', NULL, NULL, NULL, NULL),
(1008, 'Afficher le détails d''une avoir', NULL, NULL, 2, 'N', 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_page_attribute`
--

CREATE TABLE `pm_page_attribute` (
`ID` bigint(20) NOT NULL,
  `PAGE_ID` bigint(20) DEFAULT NULL,
  `INF_ITEM_CODE` varchar(255) DEFAULT NULL,
  `PM_COMPONENT_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `IS_REQUIRED` char(1) DEFAULT 'N',
  `IS_READONLY` char(1) DEFAULT 'N',
  `IS_HIDDEN` char(1) DEFAULT 'N',
  `DATA_TYPE_ID` bigint(20) DEFAULT NULL,
  `FORMAT_TYPE_ID` bigint(20) DEFAULT NULL,
  `MAX_LENGHT` bigint(20) DEFAULT NULL,
  `MAX_WORD` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `DATE_UPDATE` varchar(45) DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=143 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_page_attribute`
--

INSERT INTO `pm_page_attribute` (`ID`, `PAGE_ID`, `INF_ITEM_CODE`, `PM_COMPONENT_ID`, `SORT_KEY`, `ACTIVE`, `IS_REQUIRED`, `IS_READONLY`, `IS_HIDDEN`, `DATA_TYPE_ID`, `FORMAT_TYPE_ID`, `MAX_LENGHT`, `MAX_WORD`, `DATE_CREATION`, `DATE_UPDATE`, `USER_CREATION`, `USER_UPDATE`) VALUES
(141, 4, 'p004.s2.note', 1, 1, 'Y', 'Y', 'N', 'N', 1, 1, 20, 4, NULL, NULL, NULL, NULL),
(142, 4, 'p004.s2.category', 1, 2, 'Y', '', '', 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_page_attribute_model`
--

CREATE TABLE `pm_page_attribute_model` (
`ID` bigint(20) NOT NULL,
  `PAGE_ATTRIBUTE_ID` bigint(20) DEFAULT NULL,
  `VALUE` longtext,
  `MODEL_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `IS_REQUIRED` char(1) DEFAULT 'N',
  `IS_READONLY` char(1) DEFAULT 'N',
  `IS_HIDDEN` char(1) DEFAULT 'N',
  `DATA_TYPE_ID` bigint(20) DEFAULT NULL,
  `FORMAT_TYPE_ID` bigint(20) DEFAULT NULL,
  `MAX_LENGHT` bigint(20) DEFAULT NULL,
  `MAX_WORD` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pm_page_attribute_module`
--

CREATE TABLE `pm_page_attribute_module` (
`ID` bigint(20) NOT NULL,
  `PAGE_ATTRIBUTE_ID` bigint(20) DEFAULT NULL,
  `VALUE` longtext,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `IS_REQUIRED` char(1) DEFAULT 'N',
  `IS_READONLY` char(1) DEFAULT 'N',
  `IS_HIDDEN` char(1) DEFAULT 'N',
  `DATA_TYPE_ID` bigint(20) DEFAULT NULL,
  `FORMAT_TYPE_ID` bigint(20) DEFAULT NULL,
  `MAX_LENGHT` bigint(20) DEFAULT NULL,
  `MAX_WORD` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pm_page_parameter`
--

CREATE TABLE `pm_page_parameter` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext CHARACTER SET latin1,
  `DESCRIPTION` text CHARACTER SET latin1,
  `DEFAULT_VALUE` text,
  `PAGE_ID` bigint(20) DEFAULT NULL,
  `PAGE_PARAMETER_TYPE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_page_parameter`
--

INSERT INTO `pm_page_parameter` (`ID`, `NAME`, `DESCRIPTION`, `DEFAULT_VALUE`, `PAGE_ID`, `PAGE_PARAMETER_TYPE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'titre de l''application', 'title of application', 'My Stock Management', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'visiblité de form add edit ', NULL, 'no', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(4, '1', '2', '3', 20, 1, 'Y', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_page_parameter_model`
--

CREATE TABLE `pm_page_parameter_model` (
`ID` bigint(20) NOT NULL,
  `PAGE_PARAMETER_ID` bigint(20) DEFAULT NULL,
  `VALUE` longtext,
  `MODEL_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pm_page_parameter_module`
--

CREATE TABLE `pm_page_parameter_module` (
`ID` bigint(20) NOT NULL,
  `PAGE_PARAMETER_ID` bigint(20) DEFAULT NULL,
  `VALUE` longtext,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pm_page_parameter_type`
--

CREATE TABLE `pm_page_parameter_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_page_parameter_type`
--

INSERT INTO `pm_page_parameter_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'pm_page_parameter_type', 'pm_page_parameter_type', NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_page_type`
--

CREATE TABLE `pm_page_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_page_type`
--

INSERT INTO `pm_page_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Pages', '0   --> 999', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Pdf', '1001 --> 1999', 2, 'Y', NULL, NULL, NULL, NULL),
(3, 'Widget', '2001 --> 2999', 3, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_validation_type`
--

CREATE TABLE `pm_validation_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `PARAM_NUMBER` bigint(20) DEFAULT NULL,
  `ERROR_MESSAGE` tinytext,
  `HELP` text CHARACTER SET latin1,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_validation_type`
--

INSERT INTO `pm_validation_type` (`ID`, `NAME`, `DESCRIPTION`, `PARAM_NUMBER`, `ERROR_MESSAGE`, `HELP`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'required', 'required', 0, 'validation.v1.required', 'validation.v1.required', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'maxlenght', 'maxlenght', 1, 'validation.v1.maxlenght', 'validation.v1.maxlenght', 2, 'Y', NULL, NULL, NULL, NULL),
(3, 'maxWord', 'maxWord', 1, 'validation.v1.maxWord', 'validation.v1.maxWord', 3, 'Y', NULL, NULL, NULL, NULL),
(4, 'dataType', 'dataType : ne sera pas changé pas un locataire', 1, 'validation.v1.dataType', 'validation.v1.dataType', 4, 'Y', NULL, NULL, NULL, NULL),
(5, 'formatType', 'formatType', 1, 'validation.v1.formatType', 'validation.v1.formatType', 5, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_advanced`
--

CREATE TABLE `sm_advanced` (
`ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `CUSTOMER_ID` bigint(20) DEFAULT NULL,
  `ADVANCED_STATUS_ID` bigint(20) DEFAULT NULL,
  `AMOUNT` double DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_advanced_status`
--

CREATE TABLE `sm_advanced_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_advanced_status`
--

INSERT INTO `sm_advanced_status` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'En cours de saisie', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'Transmis', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Validé', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'Refuser', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(5, 'Rejeter', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_bank_type`
--

CREATE TABLE `sm_bank_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_check`
--

CREATE TABLE `sm_check` (
`ID` bigint(20) NOT NULL,
  `BANK_ID` bigint(20) DEFAULT NULL,
  `NAME_PERSON` tinytext CHARACTER SET latin1,
  `NUMBER` tinytext CHARACTER SET latin1,
  `ACCOUNT` tinytext CHARACTER SET latin1,
  `PHONE` tinytext CHARACTER SET latin1,
  `PAYABLE_IN` tinytext CHARACTER SET latin1,
  `AMOUNT` double DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_customer`
--

CREATE TABLE `sm_customer` (
`ID` bigint(20) NOT NULL,
  `FIRST_NAME` tinytext CHARACTER SET latin1,
  `LAST_NAME` tinytext CHARACTER SET latin1,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `CUSTOMER_TYPE_ID` bigint(20) DEFAULT NULL,
  `CUSTOMER_CATEGORY_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `SHORT_LABEL` tinytext,
  `FULL_LABEL` text,
  `NOTE` text,
  `COMPANY_NAME` tinytext,
  `REPRESENTATIVE` tinytext,
  `IDENTIFICATION` tinytext,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `CUSTOMER_NATURE_ID` bigint(20) DEFAULT '1',
  `ENTITY_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_customer`
--

INSERT INTO `sm_customer` (`ID`, `FIRST_NAME`, `LAST_NAME`, `CLT_MODULE_ID`, `CUSTOMER_TYPE_ID`, `CUSTOMER_CATEGORY_ID`, `SORT_KEY`, `ACTIVE`, `SHORT_LABEL`, `FULL_LABEL`, `NOTE`, `COMPANY_NAME`, `REPRESENTATIVE`, `IDENTIFICATION`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `CUSTOMER_NATURE_ID`, `ENTITY_ID`) VALUES
(4, 'AHmed', 'ML', 1, 1, NULL, 1, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 1, 1),
(5, 'Abdessamad2', 'HALLAL2', 1, 1, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 1, 1),
(6, 'Abdessamad3', 'HALLAL3', 1, 1, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 1, 1),
(10, 'Abdessamad', 'HALLAL', 1, 1, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1),
(11, ' Prénom ', 'Nom ', 1, 1, 2, NULL, 'Y', ' Libelle court ', ' Libelle complet ', ' Note interne ', NULL, NULL, NULL, NULL, 2, NULL, 2, 1, 1),
(16, '1', '2', 1, 1, 1, NULL, 'N', '10', '11', '13', NULL, NULL, NULL, NULL, 1, NULL, 1, 1, 1),
(17, 'dsdqs', '', 1, NULL, NULL, NULL, '', '', '', '', NULL, NULL, NULL, NULL, 1, NULL, NULL, 1, 1),
(18, 'qsdqsdsqdqsdsq111', '', 1, NULL, NULL, NULL, '', '', '', '', '', NULL, NULL, '2016-04-16 00:15:55', 1, NULL, NULL, 1, 1),
(19, 'sds', 'qdqsd', 1, 1, 2, NULL, 'Y', 'qsdsq', 'dsqdqs', 'qsdqs', 'dqs', 'dsqd', NULL, '2016-04-20 23:38:05', 1, NULL, NULL, 2, 1),
(20, 'dsdsq', 'dqsdqs', 1, 1, 2, NULL, 'Y', '', 'dqsdqs', 'dsqdqs', '', '', NULL, '2016-04-20 23:42:15', 1, NULL, NULL, 2, 1),
(21, 'prénom', 'nom', 1, 1, 1, NULL, 'Y', '', 'liv', '', '', '', NULL, '2016-04-20 23:43:54', 1, NULL, NULL, 2, 1),
(22, '', '', 1, NULL, NULL, NULL, '', '', '', '', '', '', NULL, '2016-04-21 00:22:29', 1, NULL, NULL, 1, 1),
(23, '', '', 1, NULL, NULL, NULL, '', '', '', '', '', '', NULL, '2016-04-21 00:22:34', 1, NULL, NULL, 2, 1);

--
-- Déclencheurs `sm_customer`
--
DELIMITER //
CREATE TRIGGER `trg_bi_sm_customer` BEFORE INSERT ON `sm_customer`
 FOR EACH ROW begin

    -- date creation 
    
    set  new.date_creation = now();

    -- generate entity id pour ctl_client

    insert into cta_entity (party_type_id) values (3);
    set new.entity_id = last_insert_id();  
    
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `sm_customer_category`
--

CREATE TABLE `sm_customer_category` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_customer_category`
--

INSERT INTO `sm_customer_category` (`ID`, `NAME`, `DESCRIPTION`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Category 1', NULL, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'Category 2', NULL, 1, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_customer_type`
--

CREATE TABLE `sm_customer_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_customer_type`
--

INSERT INTO `sm_customer_type` (`ID`, `NAME`, `DESCRIPTION`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Particulier', 'Particulier', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Société', 'Société', 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_deposit`
--

CREATE TABLE `sm_deposit` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_deposit`
--

INSERT INTO `sm_deposit` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'depôt 1', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'depôt', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_expense`
--

CREATE TABLE `sm_expense` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `AMOUNT` double DEFAULT NULL,
  `EXPENSE_TYPE_ID` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_expense`
--

INSERT INTO `sm_expense` (`ID`, `NAME`, `DESCRIPTION`, `AMOUNT`, `EXPENSE_TYPE_ID`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(23, 'Libelle1', 'Description1', 11, 1, 1, NULL, 'Y', NULL, 1, NULL, NULL),
(29, 'le petit déjounée ', 'avec ... ', 50, 1, 1, NULL, 'Y', NULL, 1, NULL, 1),
(31, 'aaa', 'bb', 3344, 1, 1, NULL, 'Y', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_expense_type`
--

CREATE TABLE `sm_expense_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_expense_type`
--

INSERT INTO `sm_expense_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Générale', 'Générale', 1, 3, 'Y', NULL, NULL, NULL, NULL),
(62, 'Femme de menage', 'Femme de menage', 2, 3, 'Y', NULL, NULL, NULL, NULL),
(63, 'Transport', 'Transport', 3, 3, 'Y', NULL, NULL, NULL, NULL),
(64, 'Eléctrisité', 'Eléctrisité', 4, 3, 'Y', NULL, NULL, NULL, NULL),
(65, 'Restaurant', 'Restaurant', 5, 3, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_JUSTIFICATION_STATUS`
--

CREATE TABLE `SM_JUSTIFICATION_STATUS` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `JUSTIFICATION_STATUS_CATEGORY_ID` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `SM_JUSTIFICATION_STATUS_CATEGORY`
--

CREATE TABLE `SM_JUSTIFICATION_STATUS_CATEGORY` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_JUSTIFICATION_STATUS_CATEGORY`
--

INSERT INTO `SM_JUSTIFICATION_STATUS_CATEGORY` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'sm_order_supplier', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'sm_reception', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'sm_order', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'sm_return_receipt', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_ORDER`
--

CREATE TABLE `SM_ORDER` (
`ID` bigint(20) NOT NULL,
  `NO_SEQ` bigint(20) DEFAULT NULL,
  `REFERENCE` tinytext,
  `CUSTOMER_ID` bigint(20) DEFAULT NULL,
  `PAYMENT_METHOD_ID` bigint(20) DEFAULT NULL,
  `CHECK_ID` bigint(20) DEFAULT NULL,
  `DELIVERY` tinytext,
  `NOTE` text,
  `ORDER_STATUS_ID` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `SM_ORDER`
--

INSERT INTO `SM_ORDER` (`ID`, `NO_SEQ`, `REFERENCE`, `CUSTOMER_ID`, `PAYMENT_METHOD_ID`, `CHECK_ID`, `DELIVERY`, `NOTE`, `ORDER_STATUS_ID`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(44, 1, 'V1', 4, NULL, NULL, NULL, 'eeee', 3, 1, NULL, '2015-12-24 22:28:45', NULL, NULL, NULL),
(45, 2, 'V2', 5, NULL, NULL, NULL, 'ddd', 2, 1, NULL, '2015-12-24 22:30:27', NULL, NULL, NULL),
(46, 3, 'V3', 6, NULL, NULL, NULL, 'dsqdsq', 5, 1, NULL, '2015-12-24 23:09:58', NULL, NULL, NULL),
(47, 4, 'V4', 10, NULL, NULL, NULL, 'note1', 3, 1, NULL, '2015-12-24 23:37:30', NULL, NULL, NULL),
(48, 5, 'V5', 10, NULL, NULL, NULL, 'AEE', 4, 1, NULL, '2015-12-24 23:41:49', NULL, NULL, NULL),
(49, 6, 'V6', NULL, NULL, NULL, NULL, '', 1, 1, NULL, '2015-12-24 23:49:17', NULL, NULL, NULL),
(50, 7, 'V7', 5, NULL, NULL, NULL, 'gggg', 1, 1, NULL, '2015-12-24 23:52:11', NULL, NULL, NULL),
(51, 8, 'V8', NULL, NULL, NULL, NULL, '', 4, 1, NULL, '2015-12-24 23:52:57', NULL, NULL, NULL),
(52, 9, 'V9', 6, NULL, NULL, NULL, 'abc111', 1, 1, NULL, '2015-12-24 23:54:06', NULL, NULL, NULL),
(53, 10, 'V10', 5, NULL, NULL, NULL, 'centre', 5, 1, NULL, '2015-12-24 23:59:09', NULL, NULL, NULL),
(54, 11, 'V11', 6, NULL, NULL, NULL, 'centre d''appelle', 3, 1, NULL, '2015-12-27 18:55:51', NULL, NULL, NULL);

--
-- Déclencheurs `SM_ORDER`
--
DELIMITER //
CREATE TRIGGER `TRG_BI_SM_ORDER` BEFORE INSERT ON `sm_order`
 FOR EACH ROW BEGIN

    SET NEW.DATE_CREATION = NOW();
    
    IF EXISTS(SELECT * FROM SM_ORDER WHERE CLT_MODULE_ID = NEW.CLT_MODULE_ID) THEN
		SET NEW.NO_SEQ = (SELECT MAX(NO_SEQ) + 1 FROM SM_ORDER WHERE CLT_MODULE_ID = NEW.CLT_MODULE_ID);
    else
		SET NEW.NO_SEQ = 1;
	END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `sm_order_line`
--

CREATE TABLE `sm_order_line` (
`ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `DESIGNATION` text,
  `ORDER_ID` bigint(20) DEFAULT NULL,
  `PROMOTION_ID` bigint(20) DEFAULT NULL,
  `NEGOTIATE_PRICE_SALE` double DEFAULT NULL,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_order_line`
--

INSERT INTO `sm_order_line` (`ID`, `PRODUCT_ID`, `DESIGNATION`, `ORDER_ID`, `PROMOTION_ID`, `NEGOTIATE_PRICE_SALE`, `QUANTITY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(4, NULL, 'Pc portable1', 44, NULL, 100, 1, NULL, NULL, NULL, NULL, NULL),
(5, 23, 'Note Portable', 44, NULL, 500, 1, NULL, NULL, NULL, NULL, NULL),
(6, 23, 'Sumsung duos', 45, NULL, 300, 1, NULL, NULL, NULL, NULL, NULL),
(7, 2, 'Pc portable1', 46, NULL, 100, 1, NULL, NULL, NULL, NULL, NULL),
(8, NULL, 'Nokia', 47, NULL, 200, 1, NULL, NULL, NULL, NULL, NULL),
(10, NULL, 'Sumsung duos', 48, NULL, 300, 1, NULL, NULL, NULL, NULL, NULL),
(11, NULL, 'Nokia', 49, NULL, 200, 1, NULL, NULL, NULL, NULL, NULL),
(13, 2, 'Pc portable1', 50, NULL, 100, 1, NULL, NULL, NULL, NULL, NULL),
(14, 18, 'Mac OS', 51, NULL, 300, 1, NULL, NULL, NULL, NULL, NULL),
(15, 18, 'Mac OS', 52, NULL, 300, 1, NULL, NULL, NULL, NULL, NULL),
(16, 2, 'Pc portable1', 53, NULL, 100, 1, NULL, NULL, NULL, NULL, NULL),
(17, 18, 'Mac OS', 53, NULL, 300, 1, NULL, NULL, NULL, NULL, NULL),
(18, 3, 'TEST 11111', 53, NULL, 4567, 222, NULL, NULL, NULL, NULL, NULL),
(19, 4, 'Sumsung duos', 53, NULL, 300, 22, NULL, NULL, NULL, NULL, NULL),
(20, 4, 'Note Portable', 53, NULL, 111.33, 222, NULL, NULL, NULL, NULL, NULL),
(21, 2, 'Pc portable1', 53, NULL, 100, 4567, NULL, NULL, NULL, NULL, NULL),
(22, 2, 'Pc portable1', 52, NULL, 100, 1, NULL, NULL, NULL, NULL, NULL),
(23, 18, 'Mac OS', 47, NULL, 300, 1, NULL, NULL, NULL, NULL, NULL),
(24, 23, 'Note Portable', 48, NULL, 500, 1, NULL, NULL, NULL, NULL, NULL),
(25, 4, 'Sumsung duos', 46, NULL, 300, 1, NULL, NULL, NULL, NULL, NULL),
(26, 2, 'Pc portable1', 46, NULL, 100, 1, NULL, NULL, NULL, NULL, NULL),
(27, 2, 'Pc portable1', 54, NULL, 100, 9, NULL, NULL, NULL, NULL, NULL),
(28, 3, 'Nokia', 54, NULL, 200, 1, NULL, NULL, NULL, NULL, NULL),
(29, 3, 'Nokia', 51, NULL, 200, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_order_status`
--

CREATE TABLE `sm_order_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_order_status`
--

INSERT INTO `sm_order_status` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'En cours de saisie', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'Transmis', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Validé', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'Refuser', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(5, 'Rejeter', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_ORDER_STATUS_LOG`
--

CREATE TABLE `SM_ORDER_STATUS_LOG` (
`ID` bigint(20) NOT NULL,
  `ORDER_ID` bigint(20) DEFAULT NULL,
  `ORDER_STATUS_ID` bigint(20) DEFAULT NULL,
  `JUSTIFICATION_STATUS_ID` bigint(20) DEFAULT NULL,
  `REASON_TITLE` tinytext,
  `REASON_CONTENT` text,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_order_supplier`
--

CREATE TABLE `sm_order_supplier` (
`ID` bigint(20) NOT NULL,
  `NO_SEQ` bigint(20) DEFAULT NULL,
  `REFERENCE` tinytext,
  `NOTE` text,
  `ORDER_SUPPLIER_STATUS_ID` bigint(20) DEFAULT NULL,
  `SUPPLIER_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_order_supplier`
--

INSERT INTO `sm_order_supplier` (`ID`, `NO_SEQ`, `REFERENCE`, `NOTE`, `ORDER_SUPPLIER_STATUS_ID`, `SUPPLIER_ID`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(6, 3, '111', '222', 3, 16, NULL, 1, '2015-11-22 15:46:52', NULL, NULL, NULL),
(7, 4, 'kaka', 'baba', 3, 1, NULL, 1, '2015-11-22 15:49:07', NULL, NULL, NULL),
(9, 6, 'R0012', 'aaaaa', 3, 1, NULL, 1, '2015-11-28 14:47:48', NULL, NULL, NULL),
(12, 7, 'aa', '', 1, 15, NULL, 1, '2015-11-29 00:06:57', NULL, NULL, NULL),
(19, 8, 'C8', 'dddd', 1, 1, NULL, 1, '2015-11-29 00:26:10', NULL, NULL, NULL),
(20, 9, 'C9', '', 1, NULL, NULL, 1, '2015-11-29 00:27:32', NULL, NULL, NULL),
(23, 10, 'C10', 'ddsqdsq', 3, 1, NULL, 1, '2015-11-29 13:45:02', NULL, NULL, NULL),
(24, 11, 'TEST', 'onte', 3, 1, NULL, 1, '2015-11-29 14:50:22', NULL, NULL, NULL),
(26, 13, 'C13', '', 3, 15, NULL, 1, '2015-12-27 18:56:24', NULL, NULL, NULL),
(27, 14, 'C14', 'note 1', 1, 1, NULL, 1, '2016-03-08 19:10:14', NULL, NULL, NULL),
(28, 15, 'C15', '', 1, NULL, NULL, 1, '2016-03-09 21:46:28', NULL, NULL, NULL),
(29, 16, 'VJ26BT', '', 1, 15, NULL, 1, '2016-04-15 23:23:30', NULL, NULL, NULL),
(30, 17, 'C17', '', 1, 15, NULL, 1, '2016-04-15 23:50:31', NULL, NULL, NULL);

--
-- Déclencheurs `sm_order_supplier`
--
DELIMITER //
CREATE TRIGGER `TRG_BI_SM_ORDER_SUPPLIER` BEFORE INSERT ON `sm_order_supplier`
 FOR EACH ROW BEGIN

    SET NEW.DATE_CREATION = NOW();
    
    IF EXISTS(SELECT * FROM SM_ORDER_SUPPLIER WHERE CLT_MODULE_ID = NEW.CLT_MODULE_ID) THEN
		SET NEW.NO_SEQ = (SELECT MAX(NO_SEQ) + 1 FROM SM_ORDER_SUPPLIER WHERE CLT_MODULE_ID = NEW.CLT_MODULE_ID);
    else
		SET NEW.NO_SEQ = 1;
	END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `sm_order_supplier_line`
--

CREATE TABLE `sm_order_supplier_line` (
`ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `DESIGNATION` text,
  `UNIT_PRICE_SALE` double DEFAULT NULL,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `TVA` double DEFAULT NULL,
  `REMISE` double DEFAULT NULL,
  `PROMOTION_ID` bigint(20) DEFAULT NULL,
  `ORDER_SUPPLIER_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_order_supplier_line`
--

INSERT INTO `sm_order_supplier_line` (`ID`, `PRODUCT_ID`, `DESIGNATION`, `UNIT_PRICE_SALE`, `QUANTITY`, `TVA`, `REMISE`, `PROMOTION_ID`, `ORDER_SUPPLIER_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(13, 18, 'Mac OS', 300, 32, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL),
(14, 3, 'Sumsung duos1199', 300, 32, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL),
(15, 3, 'Note Portable111', 900, 32, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL),
(19, 3, 'Nokia', 200, 32, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL),
(20, 3, 'Nokia1', 200, 112, NULL, NULL, NULL, 7, NULL, NULL, NULL, NULL, NULL),
(31, 3, 'Nokia', 200, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 19, 'HP Portable', 400, 1, NULL, NULL, NULL, 19, NULL, NULL, NULL, NULL, NULL),
(38, 3, 'Note Portable', 500, 60, NULL, NULL, NULL, 23, NULL, NULL, NULL, NULL, NULL),
(39, 2, 'Pc portable1', 100, 9, NULL, NULL, NULL, 24, NULL, NULL, NULL, NULL, NULL),
(41, 2, 'Pc portable1', 100, 7, NULL, NULL, NULL, 26, NULL, NULL, NULL, NULL, NULL),
(42, 3, 'Nokia', 200, 180, NULL, NULL, NULL, 26, NULL, NULL, NULL, NULL, NULL),
(43, 2, 'Pc portable1', 100, 3, NULL, NULL, NULL, 27, NULL, NULL, NULL, NULL, NULL),
(44, 2, 'Sumsung duos', 300, 4, NULL, NULL, NULL, 27, NULL, NULL, NULL, NULL, NULL),
(45, 23, 'du Text', 500, 1, NULL, NULL, NULL, 27, NULL, NULL, NULL, NULL, NULL),
(46, NULL, 'Sumsung duos', 300, 1, NULL, NULL, NULL, 28, NULL, NULL, NULL, NULL, NULL),
(47, 19, 'HP Portable', 400, 1, NULL, NULL, NULL, 9, NULL, NULL, NULL, NULL, NULL),
(48, 19, 'HP Portable', 400, 1, NULL, NULL, NULL, 29, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_order_supplier_status`
--

CREATE TABLE `sm_order_supplier_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_order_supplier_status`
--

INSERT INTO `sm_order_supplier_status` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'En cours', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'Submit', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Valider', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'Refuser', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(5, 'Rejeter', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_ORDER_SUPPLIER_STATUS_LOG`
--

CREATE TABLE `SM_ORDER_SUPPLIER_STATUS_LOG` (
`ID` bigint(20) NOT NULL,
  `ORDER_SUPPLIER_ID` bigint(20) DEFAULT NULL,
  `ORDER_SUPPLIER_STATUS_ID` bigint(20) DEFAULT NULL,
  `JUSTIFICATION_STATUS_ID` bigint(20) DEFAULT NULL,
  `REASON_TITLE` tinytext,
  `REASON_CONTENT` text,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_payment_method`
--

CREATE TABLE `sm_payment_method` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_payment_method`
--

INSERT INTO `sm_payment_method` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Chèque', NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'Prélèvement', NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Espèces', NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'Carte bancaire', NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(5, 'Traite', NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(6, 'Autre', NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_product`
--

CREATE TABLE `sm_product` (
`ID` bigint(20) NOT NULL,
  `NO_SEQ` bigint(20) DEFAULT NULL,
  `REFERENCE` tinytext CHARACTER SET latin1,
  `DESIGNATION` tinytext CHARACTER SET latin1,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `PRICE_SALE` double DEFAULT NULL,
  `PRICE_BUY` double DEFAULT NULL,
  `PRODUCT_GROUP_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_FAMILY_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_SIZE_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_COLOR_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_STATUS_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_TYPE_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_DEPARTMENT_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_UNIT_ID` bigint(20) DEFAULT NULL,
  `THRESHOLD` bigint(20) DEFAULT NULL,
  `NOTE` text,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_product`
--

INSERT INTO `sm_product` (`ID`, `NO_SEQ`, `REFERENCE`, `DESIGNATION`, `QUANTITY`, `PRICE_SALE`, `PRICE_BUY`, `PRODUCT_GROUP_ID`, `PRODUCT_FAMILY_ID`, `PRODUCT_SIZE_ID`, `PRODUCT_COLOR_ID`, `PRODUCT_STATUS_ID`, `PRODUCT_TYPE_ID`, `PRODUCT_DEPARTMENT_ID`, `PRODUCT_UNIT_ID`, `THRESHOLD`, `NOTE`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(2, 0, 'P001', 'Pc portable1', 223, 100, 111, 3, 4, 47, 1, 1, 1, NULL, NULL, 9000, 'Note interne', 'N', 1, NULL, 2, NULL, 2),
(3, 0, 'P002', 'Nokia', 200, 200, 222, 2, 3, 50, 2, 1, 2, NULL, NULL, 9000, 'Note interne', 'Y', 1, NULL, 2, NULL, NULL),
(4, 0, 'P003', 'Sumsung duos', -2, 300, 333, 1, 2, 47, 8, 1, 2, NULL, NULL, 9000, 'Intene', 'Y', 1, NULL, 2, NULL, 1),
(18, 2, 'P004', 'Mac OS', 0, 300, 444, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9000, '', '', 1, '2015-11-13 23:17:10', 1, NULL, NULL),
(19, 3, 'P005', 'HP Portable', 12, 400, 555, 2, 3, NULL, NULL, NULL, 1, NULL, NULL, 90000, '', 'Y', 1, '2015-11-14 13:00:09', 1, NULL, 1),
(23, 7, 'P006', 'Note Portable', -1, 500, 666, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9000, '', '', 1, '2015-11-14 13:43:19', 1, NULL, NULL),
(24, 1, NULL, NULL, 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9000, NULL, 'Y', 2, '2016-04-07 22:54:55', NULL, NULL, NULL),
(25, 1, NULL, NULL, 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 96000, NULL, 'Y', 2, '2016-04-07 22:54:55', NULL, NULL, NULL),
(26, 1, NULL, NULL, 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9000, NULL, 'Y', 2, '2016-04-07 22:54:55', NULL, NULL, NULL),
(27, 1, NULL, NULL, 200, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 9000, NULL, 'Y', 3, '2016-04-07 22:54:55', NULL, NULL, NULL),
(28, 8, 'OGQV8N', 'aaaa', NULL, 111, 222, 2, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, '', 'Y', 1, '2016-04-15 22:29:27', 1, NULL, NULL),
(30, 10, 'CT2YNN', 'autooo', NULL, 12.15, 12.11, 1, 1, 47, 11, NULL, 1, NULL, NULL, 900, 'note1', 'Y', 1, '2016-04-17 13:21:46', 1, NULL, NULL),
(31, 11, 'RWLTLQ', 'aa', NULL, 11, 22, 1, 1, 47, 1, NULL, 2, NULL, NULL, 11, 'ccc', 'Y', 1, '2016-04-17 13:47:48', 1, NULL, NULL),
(33, 13, 'dsqdsq', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'Y', 1, '2016-04-22 01:24:00', 1, NULL, NULL),
(34, 14, 'P14', ' Désignation ', NULL, 11, 22, 3, 4, 48, 9, NULL, 3, NULL, NULL, 23, 'note', 'Y', 1, '2016-04-22 01:25:09', 1, NULL, 1);

--
-- Déclencheurs `sm_product`
--
DELIMITER //
CREATE TRIGGER `TRG_BI_SM_PRODUCT` BEFORE INSERT ON `sm_product`
 FOR EACH ROW BEGIN

    SET NEW.DATE_CREATION = NOW();
    
    IF EXISTS(SELECT * FROM sm_product WHERE CLT_MODULE_ID = NEW.CLT_MODULE_ID) THEN
		SET NEW.NO_SEQ = (SELECT MAX(NO_SEQ) + 1 FROM sm_product WHERE CLT_MODULE_ID = NEW.CLT_MODULE_ID);
    else
		SET NEW.NO_SEQ = 1;
	END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `sm_product_color`
--

CREATE TABLE `sm_product_color` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `HEX` tinytext CHARACTER SET latin1,
  `RGB` tinytext CHARACTER SET latin1,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_product_color`
--

INSERT INTO `sm_product_color` (`ID`, `NAME`, `HEX`, `RGB`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `CLT_MODULE_ID`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Blanc1', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(2, 'Bleu', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(3, 'Brun', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(4, 'Gris', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(5, 'Noir', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(6, 'Or', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(7, 'Orange', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(8, 'Rose', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(9, 'Rouge', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(10, 'Vert', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(11, 'Violet', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(12, 'New Color11', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(13, 'bbb', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_product_department`
--

CREATE TABLE `sm_product_department` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` varchar(45) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_product_department`
--

INSERT INTO `sm_product_department` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'departement 1', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_product_family`
--

CREATE TABLE `sm_product_family` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `PRODUCT_GROUP_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_product_family`
--

INSERT INTO `sm_product_family` (`ID`, `NAME`, `DESCRIPTION`, `PRODUCT_GROUP_ID`, `SORT_KEY`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'PC', 'family 2', 1, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'Electro', NULL, 1, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(3, 'TV', NULL, 2, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(4, 'Tools', NULL, 3, NULL, 'Y', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_product_group`
--

CREATE TABLE `sm_product_group` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_product_group`
--

INSERT INTO `sm_product_group` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Groupe 01', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Groupe 02', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(3, 'Groupe 03', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_product_size`
--

CREATE TABLE `sm_product_size` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_product_size`
--

INSERT INTO `sm_product_size` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(47, '37', NULL, NULL, 1, 'Y', NULL, 1, NULL, NULL),
(48, '38', NULL, NULL, 1, 'Y', NULL, 1, NULL, NULL),
(49, '39', NULL, NULL, 1, 'Y', NULL, 1, NULL, NULL),
(50, '40', NULL, NULL, 1, 'Y', NULL, 1, NULL, NULL),
(51, '32', NULL, NULL, 1, 'Y', NULL, 1, NULL, NULL),
(52, '36', NULL, NULL, 1, 'Y', NULL, 1, NULL, NULL),
(53, '41', NULL, NULL, 1, 'Y', NULL, 1, NULL, NULL),
(54, '67', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(55, '78', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(56, '89', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(57, '98', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(58, '17', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(59, '23', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(60, '35', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(61, '42', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_product_status`
--

CREATE TABLE `sm_product_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_product_status`
--

INSERT INTO `sm_product_status` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Statuts product 1', 'desc', 1, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'Statuts product 2', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_product_type`
--

CREATE TABLE `sm_product_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` varchar(45) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_product_type`
--

INSERT INTO `sm_product_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Type 01', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Type 02', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(3, 'Type 03', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_product_unit`
--

CREATE TABLE `sm_product_unit` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` varchar(45) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_product_unit`
--

INSERT INTO `sm_product_unit` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'lettre', NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_promotion`
--

CREATE TABLE `sm_promotion` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `PROMOTION_TYPE_ID` bigint(20) DEFAULT NULL,
  `PRODUCT` bigint(20) DEFAULT NULL,
  `VALUE` double DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_promotion_type`
--

CREATE TABLE `sm_promotion_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_reception`
--

CREATE TABLE `sm_reception` (
`ID` bigint(20) NOT NULL,
  `NO_SEQ` bigint(20) DEFAULT NULL,
  `REFERENCE` tinytext,
  `SOUCHE` tinytext,
  `SUPPLIER_ID` bigint(20) DEFAULT NULL,
  `DEPOSIT_ID` bigint(20) DEFAULT NULL,
  `RECEPTION_STATUS_ID` bigint(20) DEFAULT NULL,
  `DEADLINE` datetime DEFAULT NULL,
  `NOTE` text,
  `TVA` double DEFAULT NULL,
  `DELIVERY` tinytext,
  `ORDER_SUPPLIER_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `EXPIRATION_DATE` datetime DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_reception`
--

INSERT INTO `sm_reception` (`ID`, `NO_SEQ`, `REFERENCE`, `SOUCHE`, `SUPPLIER_ID`, `DEPOSIT_ID`, `RECEPTION_STATUS_ID`, `DEADLINE`, `NOTE`, `TVA`, `DELIVERY`, `ORDER_SUPPLIER_ID`, `ACTIVE`, `EXPIRATION_DATE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(43, 1, 'R1', NULL, 1, NULL, 3, NULL, 'centre', NULL, NULL, 9, NULL, NULL, 1, '2015-12-13 17:59:36', NULL, NULL, NULL),
(44, 2, 'R2', NULL, NULL, NULL, 3, NULL, '', NULL, NULL, 9, NULL, NULL, 1, '2015-12-13 18:32:47', NULL, NULL, NULL),
(45, 3, 'R3', NULL, 16, NULL, 3, NULL, '', NULL, NULL, 9, NULL, NULL, 1, '2015-12-13 22:28:11', NULL, NULL, NULL),
(46, 4, 'R4', NULL, 16, NULL, 3, NULL, 'centre', NULL, NULL, 9, NULL, NULL, 1, '2015-12-27 18:54:30', NULL, NULL, NULL),
(47, 5, 'R5', NULL, 15, NULL, 3, NULL, 'Ok merci', NULL, NULL, 9, NULL, NULL, 1, '2015-12-27 18:57:14', NULL, NULL, NULL),
(48, 6, 'B97C1K', NULL, 1, NULL, 3, NULL, ' Note interne  11', NULL, NULL, 7, NULL, NULL, 1, '2016-01-21 23:52:16', NULL, NULL, NULL),
(49, 7, '54TKNU', NULL, 1, NULL, 3, NULL, 'note', NULL, NULL, NULL, NULL, NULL, 1, '2016-02-09 22:17:24', NULL, NULL, NULL),
(50, 8, 'UDFPV6', NULL, 1, NULL, 2, NULL, 'fdsf', NULL, NULL, NULL, NULL, NULL, 1, '2016-03-06 19:38:26', NULL, NULL, NULL),
(51, 9, 'R9', NULL, 1, NULL, 3, NULL, 'note', NULL, NULL, NULL, NULL, NULL, 1, '2016-03-08 19:43:51', NULL, NULL, NULL),
(53, 10, 'R10', NULL, NULL, NULL, 1, NULL, '', NULL, NULL, NULL, NULL, NULL, 1, '2016-03-13 00:49:36', NULL, NULL, NULL),
(54, 1, 'QSRFG', NULL, 1, NULL, 3, NULL, NULL, NULL, NULL, 24, 'Y', NULL, NULL, '2016-04-07 23:01:36', NULL, NULL, NULL),
(55, 1, 'qsdfqdf', NULL, 1, NULL, 3, NULL, NULL, NULL, NULL, 26, 'Y', NULL, NULL, '2016-04-07 23:02:21', NULL, NULL, NULL);

--
-- Déclencheurs `sm_reception`
--
DELIMITER //
CREATE TRIGGER `TRG_BI_SM_RECEPTION` BEFORE INSERT ON `sm_reception`
 FOR EACH ROW BEGIN

    SET NEW.DATE_CREATION = NOW();
    
    IF EXISTS(SELECT * FROM sm_reception WHERE CLT_MODULE_ID = NEW.CLT_MODULE_ID) THEN
		SET NEW.NO_SEQ = (SELECT MAX(NO_SEQ) + 1 FROM sm_reception WHERE CLT_MODULE_ID = NEW.CLT_MODULE_ID);
    else
		SET NEW.NO_SEQ = 1;
	END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `sm_reception_line`
--

CREATE TABLE `sm_reception_line` (
`ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `DESIGNATION` tinytext,
  `UNIT_PRICE_BUY` double DEFAULT NULL,
  `REMISE` double DEFAULT NULL,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `TVA` double DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `RECEPTION_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_reception_line`
--

INSERT INTO `sm_reception_line` (`ID`, `PRODUCT_ID`, `DESIGNATION`, `UNIT_PRICE_BUY`, `REMISE`, `QUANTITY`, `TVA`, `ACTIVE`, `RECEPTION_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 2, 'Pc portable1', 100, NULL, 1, NULL, NULL, 43, NULL, NULL, NULL, NULL),
(2, 3, 'Nokia', 200, NULL, 1, NULL, NULL, 43, NULL, NULL, NULL, NULL),
(3, 4, 'Sumsung duos', 300, NULL, 1, NULL, NULL, 43, NULL, NULL, NULL, NULL),
(4, 18, 'Mac OS', 300, NULL, 1, NULL, NULL, 43, NULL, NULL, NULL, NULL),
(5, 19, 'HP Portable', 400, NULL, 1, NULL, NULL, 43, NULL, NULL, NULL, NULL),
(6, 23, 'Note Portable', 500, NULL, 1, NULL, NULL, 43, NULL, NULL, NULL, NULL),
(11, 2, 'Pc portable1', 100, NULL, 9, NULL, NULL, 45, NULL, NULL, NULL, NULL),
(12, 2, 'Pc portable1', 100, NULL, 9, NULL, NULL, 46, NULL, NULL, NULL, NULL),
(13, 3, 'Nokia', 200, NULL, 1, NULL, NULL, 46, NULL, NULL, NULL, NULL),
(14, 2, 'Pc portable1', 100, NULL, 7, NULL, NULL, 47, NULL, NULL, NULL, NULL),
(15, 3, 'Nokia', 200, NULL, 180, NULL, NULL, 47, NULL, NULL, NULL, NULL),
(16, 2, 'Pc portable1', 100, NULL, 7, NULL, NULL, 48, NULL, NULL, NULL, NULL),
(17, 3, 'Nokia', 200, NULL, 7, NULL, NULL, 48, NULL, NULL, NULL, NULL),
(18, 2, 'Pc portable1', 100, NULL, 9, NULL, NULL, 49, NULL, NULL, NULL, NULL),
(19, NULL, 'Pc portable1', 100, NULL, 1, NULL, NULL, 49, NULL, NULL, NULL, NULL),
(20, NULL, 'Nokia', 200, NULL, 1, NULL, NULL, 49, NULL, NULL, NULL, NULL),
(21, NULL, 'Nokia', 200, NULL, 1, NULL, NULL, 49, NULL, NULL, NULL, NULL),
(22, NULL, 'Nokia', 200, NULL, 1, NULL, NULL, 49, NULL, NULL, NULL, NULL),
(23, NULL, 'Pc portable1', 100, NULL, 1, NULL, NULL, 50, NULL, NULL, NULL, NULL),
(24, NULL, 'Pc portable1', 100, NULL, 1, NULL, NULL, 49, NULL, NULL, NULL, NULL),
(25, 2, 'Pc portable1', 100, NULL, 9, NULL, NULL, 44, NULL, NULL, NULL, NULL),
(26, 2, 'Pc portable1', 100, NULL, 1, NULL, NULL, 44, NULL, NULL, NULL, NULL),
(27, 19, 'HP Portable', 400, NULL, 1, NULL, NULL, 44, NULL, NULL, NULL, NULL),
(28, 2, 'Pc portable1', 100, NULL, 7, NULL, NULL, 51, NULL, NULL, NULL, NULL),
(30, 19, 'HP Portable', 400, NULL, 1, NULL, NULL, 51, NULL, NULL, NULL, NULL),
(33, 3, 'Nokia', 200, NULL, 1, NULL, NULL, 53, NULL, NULL, NULL, NULL),
(34, 2, NULL, NULL, NULL, 2, NULL, 'Y', 54, NULL, NULL, NULL, NULL),
(35, 2, NULL, NULL, NULL, 3, NULL, 'Y', 55, NULL, NULL, NULL, NULL),
(36, 4, 'Sumsung duos', 300, NULL, 1, NULL, NULL, 50, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_reception_products`
--

CREATE TABLE `sm_reception_products` (
`ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `DESIGNATION` tinytext,
  `UNIT_PRICE_BUY` double DEFAULT NULL,
  `REMISE` double DEFAULT NULL,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `TVA` double DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `RECEPTION_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_reception_status`
--

CREATE TABLE `sm_reception_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_reception_status`
--

INSERT INTO `sm_reception_status` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'En cours de saisie', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'Transmis', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Validé', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'Refuser', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(5, 'Rejeter', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_RECEPTION_STATUS_LOG`
--

CREATE TABLE `SM_RECEPTION_STATUS_LOG` (
`ID` bigint(20) NOT NULL,
  `RECEPTION_ID` bigint(20) DEFAULT NULL,
  `RECEPTION_STATUS_ID` bigint(20) DEFAULT NULL,
  `JUSTIFICATION_STATUS_ID` bigint(20) DEFAULT NULL,
  `REASON_TITLE` tinytext,
  `REASON_CONTENT` text,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_return_receipt`
--

CREATE TABLE `sm_return_receipt` (
`ID` bigint(20) NOT NULL,
  `NO_SEQ` bigint(20) DEFAULT NULL,
  `REFERENCE` tinytext,
  `CUSTOMER_ID` bigint(20) DEFAULT NULL,
  `ORDER_ID` bigint(20) DEFAULT NULL,
  `RETURN_RECEIPT_STATUS_ID` bigint(20) DEFAULT NULL,
  `NOTE` text,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_return_receipt`
--

INSERT INTO `sm_return_receipt` (`ID`, `NO_SEQ`, `REFERENCE`, `CUSTOMER_ID`, `ORDER_ID`, `RETURN_RECEIPT_STATUS_ID`, `NOTE`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(5, 1, 'ref1', 1, NULL, 1, 'Note12', 'Y', 1, '2015-11-21 02:46:35', NULL, NULL, NULL),
(6, 1, NULL, 1, NULL, 2, NULL, 'Y', 1, '2015-11-21 02:46:35', NULL, NULL, NULL),
(7, 2, 'R2', 1, NULL, 1, 'aaaaaa', NULL, 1, '2015-12-27 21:39:07', NULL, NULL, NULL),
(8, 3, 'R3', 6, NULL, 1, 'note123', NULL, 1, '2015-12-27 22:52:59', NULL, NULL, NULL),
(9, 4, 'XZ6Q31', 6, NULL, 1, 'OKSDFG67', NULL, 1, '2015-12-27 23:01:12', NULL, NULL, NULL),
(10, 5, 'R5', 6, NULL, 2, 'aaaa', NULL, 1, '2016-01-16 21:53:30', NULL, NULL, NULL),
(11, 6, 'R6', NULL, NULL, 2, '', NULL, 1, '2016-04-15 23:02:35', NULL, NULL, NULL);

--
-- Déclencheurs `sm_return_receipt`
--
DELIMITER //
CREATE TRIGGER `TRG_BI_SM_RETURN_RECEIPT` BEFORE INSERT ON `sm_return_receipt`
 FOR EACH ROW BEGIN

    SET NEW.DATE_CREATION = NOW();
    
    IF EXISTS(SELECT * FROM sm_return_receipt WHERE CLT_MODULE_ID = NEW.CLT_MODULE_ID) THEN
		SET NEW.NO_SEQ = (SELECT MAX(NO_SEQ) + 1 FROM sm_return_receipt WHERE CLT_MODULE_ID = NEW.CLT_MODULE_ID);
    else
		SET NEW.NO_SEQ = 1;
	END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `sm_return_receipt_line`
--

CREATE TABLE `sm_return_receipt_line` (
`ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `DESIGNATION` text,
  `RETURN_RECEIPT_ID` bigint(20) DEFAULT NULL,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `PROMOTION_ID` bigint(20) DEFAULT NULL,
  `NEGOTIATE_PRICE_SALE` double DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_return_receipt_line`
--

INSERT INTO `sm_return_receipt_line` (`ID`, `PRODUCT_ID`, `DESIGNATION`, `RETURN_RECEIPT_ID`, `QUANTITY`, `PROMOTION_ID`, `NEGOTIATE_PRICE_SALE`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(3, 2, 'Pc portable1', 7, 1, NULL, 100, NULL, NULL, NULL, NULL, NULL),
(4, 18, 'Mac OS', 7, 1, NULL, 300, NULL, NULL, NULL, NULL, NULL),
(5, 3, 'TEST 11111', 7, 222, NULL, 4567, NULL, NULL, NULL, NULL, NULL),
(6, 4, 'Sumsung duos', 7, 22, NULL, 300, NULL, NULL, NULL, NULL, NULL),
(7, 4, 'Note Portable', 7, 222, NULL, 111.33, NULL, NULL, NULL, NULL, NULL),
(8, 2, 'Pc portable1', 7, 4567, NULL, 100, NULL, NULL, NULL, NULL, NULL),
(9, 2, 'Pc portable1', 5, 11, NULL, 100, NULL, NULL, NULL, NULL, NULL),
(11, 23, 'Note Portable', 8, 1, NULL, 500, NULL, NULL, NULL, NULL, NULL),
(12, 2, 'Pc portable1', 8, 1, NULL, 100, NULL, NULL, NULL, NULL, NULL),
(13, 2, 'Pc portable1', 9, 1, NULL, 100, NULL, NULL, NULL, NULL, NULL),
(14, 23, 'Note Portable', 10, 1, NULL, 500, NULL, NULL, NULL, NULL, NULL),
(15, 2, 'Pc portable1', 10, 1, NULL, 100, NULL, NULL, NULL, NULL, NULL),
(16, 19, 'HP Portable', 10, 1, NULL, 400, NULL, NULL, NULL, NULL, NULL),
(17, 23, 'Note Portable', 11, 1, NULL, 500, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_return_receipt_status`
--

CREATE TABLE `sm_return_receipt_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_return_receipt_status`
--

INSERT INTO `sm_return_receipt_status` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'en cours', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'submit', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'Validé', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'Refuser', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(5, 'Rejeter', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `SM_RETURN_RECEIPT_STATUS_LOG`
--

CREATE TABLE `SM_RETURN_RECEIPT_STATUS_LOG` (
`ID` bigint(20) NOT NULL,
  `RETURN_RECEIPT_ID` bigint(20) DEFAULT NULL,
  `RETURN_RECEIPT_STATUS_ID` bigint(20) DEFAULT NULL,
  `JUSTIFICATION_STATUS_ID` bigint(20) DEFAULT NULL,
  `REASON_TITLE` tinytext,
  `REASON_CONTENT` text,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_supplier`
--

CREATE TABLE `sm_supplier` (
`ID` bigint(20) NOT NULL,
  `FIRST_NAME` tinytext CHARACTER SET latin1,
  `LAST_NAME` tinytext CHARACTER SET latin1,
  `COMPANY_NAME` tinytext,
  `REPRESENTATIVE` tinytext,
  `SHORT_LABEL` tinytext,
  `FULL_LABEL` text,
  `NOTE` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `SUPPLIER_TYPE_ID` bigint(20) DEFAULT NULL,
  `SUPPLIER_CATEGORY_ID` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ENTITY_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `SUPPLIER_NATURE_ID` bigint(20) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_supplier`
--

INSERT INTO `sm_supplier` (`ID`, `FIRST_NAME`, `LAST_NAME`, `COMPANY_NAME`, `REPRESENTATIVE`, `SHORT_LABEL`, `FULL_LABEL`, `NOTE`, `SORT_KEY`, `ACTIVE`, `SUPPLIER_TYPE_ID`, `SUPPLIER_CATEGORY_ID`, `CLT_MODULE_ID`, `ENTITY_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `SUPPLIER_NATURE_ID`) VALUES
(1, 'sybaway@gmail.com', 'sybaway@gmail.com', 'sybaway@gmail.com', 'sybaway@gmail.com', 'sybaway@gmail.com', 'sybaway@gmail.com', '', NULL, 'Y', 1, NULL, 1, 1, NULL, 1, NULL, 1, 2),
(15, '', '', 'JIA DA LI SHOES', NULL, NULL, NULL, NULL, NULL, 'Y', 1, NULL, 1, 1, NULL, 1, NULL, 1, 1),
(16, 'Ahmed', 'Ali', 'INF sos', NULL, NULL, NULL, NULL, NULL, 'Y', 1, NULL, 1, 1, NULL, 1, NULL, NULL, 1),
(19, '1', '2', '3', NULL, '10', '11', '13', NULL, 'N', 1, 1, 1, 1, NULL, 1, NULL, 1, 1),
(20, '', '', '', NULL, '', '', '', NULL, '', NULL, NULL, 1, 1, NULL, 1, NULL, NULL, 1),
(21, '', '', '', NULL, '', '', '', NULL, '', NULL, NULL, 1, 1, NULL, 1, NULL, 1, 1),
(22, 'dqsdqsdsq', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, 1, '2016-04-12 13:00:46', NULL, NULL, NULL, 1),
(23, '', '', 'aaaa', NULL, 'dddd', 'eee', 'note 1', NULL, 'Y', 1, 1, 1, 1, '2016-04-20 23:15:15', 1, NULL, NULL, 1),
(24, '', '', '', '', '', '', '', NULL, '', NULL, NULL, 1, 1, '2016-04-24 13:34:33', 1, NULL, NULL, 1);

--
-- Déclencheurs `sm_supplier`
--
DELIMITER //
CREATE TRIGGER `trg_bi_sm_supplier` BEFORE INSERT ON `sm_supplier`
 FOR EACH ROW begin

    -- date creation 
    
    set  new.date_creation = now();

    -- generate entity id pour ctl_client

    insert into cta_entity (party_type_id) values (4);
    set new.entity_id = last_insert_id();  
    
end
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `sm_supplier_category`
--

CREATE TABLE `sm_supplier_category` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` varchar(45) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_supplier_category`
--

INSERT INTO `sm_supplier_category` (`ID`, `NAME`, `DESCRIPTION`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'categorie One', NULL, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'categorie Tow', NULL, 1, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_supplier_type`
--

CREATE TABLE `sm_supplier_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_supplier_type`
--

INSERT INTO `sm_supplier_type` (`ID`, `NAME`, `DESCRIPTION`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Société', 'Société', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Particulier', 'Particulier', 1, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tmp_clt_user_module`
--

CREATE TABLE `tmp_clt_user_module` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `USER_ID` bigint(20) DEFAULT NULL,
  `MODULE_ID` bigint(20) DEFAULT NULL,
  `FULL CONTROL` char(1) CHARACTER SET latin1 DEFAULT 'N',
  `MODULE_ID_TO_MANAGER` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;

--
-- Contenu de la table `tmp_clt_user_module`
--

INSERT INTO `tmp_clt_user_module` (`ID`, `USER_ID`, `MODULE_ID`, `FULL CONTROL`, `MODULE_ID_TO_MANAGER`, `ACTIVE`, `SORT_KEY`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(12, 2, 3, 'N', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(14, 2, 1, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(27, 1, 1, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(29, 1, 3, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(31, 1, 5, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(36, 3, 1, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(37, 3, 3, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(38, 3, 5, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(39, 4, 1, 'N', NULL, 'Y', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tmp_inf_item`
--

CREATE TABLE `tmp_inf_item` (
  `CODE` varchar(255) NOT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` varchar(45) DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `tmp_inf_item`
--

INSERT INTO `tmp_inf_item` (`CODE`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
('basicParameter', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1.basicParameterType', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1.description', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1.id', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1.name', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s1.value', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2.basicParameterType', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2.description', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2.id', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2.name', 'Y', NULL, NULL, NULL, NULL),
('basicParameter.s2.value', 'Y', NULL, NULL, NULL, NULL),
('blValidation', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.customerName', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.description', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.id', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.sumQuantity', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.sumTotalPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s2', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s2.customer', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s2.description', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.color', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.size', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.color', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.price', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.quantity', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.reference', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.size', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.totalPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s5', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s5.sumQuantity', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s5.sumTotal', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s6', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s1', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s1.customer', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s1.description', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.color', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.quantity', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.size', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.color', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.price', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.size', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.totalPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s4', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s4.sumQuantity', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s4.sumTotal', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s5', 'Y', NULL, NULL, NULL, NULL),
('customer', 'Y', NULL, NULL, NULL, NULL),
('customer.s1', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.active', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.adress', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.category', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.city', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.company', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.country', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.email', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.firstName', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.fixedPhone', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.fullLabel', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.lastName', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.note', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.secondaryAddress', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.shortLabel', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.type', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.webSite', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.zipCode', 'Y', NULL, NULL, NULL, NULL),
('customer.s2', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.adress', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.city', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.country', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.email', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.firstName', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.fixedPhone', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.lastName', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.type', 'Y', NULL, NULL, NULL, NULL),
('globals', 'Y', NULL, NULL, NULL, NULL),
('globals.forms', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.add', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.cancel', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.clean', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.confirmClean', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.confirmDelete', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.delete', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.edit', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.no', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.save', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.validate', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.yes', 'Y', NULL, NULL, NULL, NULL),
('globals.list', 'Y', NULL, NULL, NULL, NULL),
('globals.list.activate', 'Y', NULL, NULL, NULL, NULL),
('globals.list.confirmDelete', 'Y', NULL, NULL, NULL, NULL),
('globals.list.delete', 'Y', NULL, NULL, NULL, NULL),
('globals.list.edit', 'Y', NULL, NULL, NULL, NULL),
('globals.list.option', 'Y', NULL, NULL, NULL, NULL),
('globals.list.validate', 'Y', NULL, NULL, NULL, NULL),
('globals.list.vide', 'Y', NULL, NULL, NULL, NULL),
('inventaire', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.active', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.designation', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.priceBuy', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.priceSale', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.productColorName', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.productSizeName', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.quantity', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.quantityCount', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.receptionValidCount', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.unitPriceBuyMax', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.unitPriceBuyMin', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.unitPriceBuyMoyenne', 'Y', NULL, NULL, NULL, NULL),
('login', 'Y', NULL, NULL, NULL, NULL),
('login.s1', 'Y', NULL, NULL, NULL, NULL),
('login.s1.forgetPassword', 'Y', NULL, NULL, NULL, NULL),
('login.s1.getAccount', 'Y', NULL, NULL, NULL, NULL),
('login.s1.login', 'Y', NULL, NULL, NULL, NULL),
('login.s1.password', 'Y', NULL, NULL, NULL, NULL),
('login.s1.sessionActive', 'Y', NULL, NULL, NULL, NULL),
('login.s1.username', 'Y', NULL, NULL, NULL, NULL),
('module', 'Y', NULL, NULL, NULL, NULL),
('module.s1', 'Y', NULL, NULL, NULL, NULL),
('module.s1.access', 'Y', NULL, NULL, NULL, NULL),
('module.s1.logout', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1.description', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1.id', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1.moduleParameterType', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1.name', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s1.value', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2.description', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2.id', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2.moduleParameterType', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2.name', 'Y', NULL, NULL, NULL, NULL),
('moduleParameter.s2.value', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s1', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s1.company', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s1.email', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s1.name', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s2', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s2.descriptionModule', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s2.moduleTypeName', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s2.nameModule', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3.client', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3.defaultValue', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3.parameterModuleName', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3.parameterModuleTypeName', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s3.value', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s4', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s4.client', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s4.parameterModule', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s4.parameterModuleDefaultValue', 'Y', NULL, NULL, NULL, NULL),
('moduleParameterClient.s4.value', 'Y', NULL, NULL, NULL, NULL),
('p003', 'Y', NULL, NULL, NULL, NULL),
('p003.s1', 'Y', NULL, NULL, NULL, NULL),
('p003.s1.amount', 'Y', NULL, NULL, NULL, NULL),
('p003.s1.description', 'Y', NULL, NULL, NULL, NULL),
('p003.s1.expenseType', 'Y', NULL, NULL, NULL, NULL),
('p003.s1.expenseTypeName', 'Y', NULL, NULL, NULL, NULL),
('p003.s1.name', 'Y', NULL, NULL, NULL, NULL),
('p003.s2', 'Y', NULL, NULL, NULL, NULL),
('p003.s2.amount', 'Y', NULL, NULL, NULL, NULL),
('p003.s2.description', 'Y', NULL, NULL, NULL, NULL),
('p003.s2.name', 'Y', NULL, NULL, NULL, NULL),
('p003.s2.type', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.code', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.deadline', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.deposit', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.note', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.souche', 'Y', NULL, NULL, NULL, NULL),
('p006.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('page1.s1.name', 'Y', NULL, NULL, NULL, NULL),
('pageParameter', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.description', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.id', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.name', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.page', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.pageParameterType', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s1.value', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.description', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.id', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.name', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.page', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.pageParameterType', 'Y', NULL, NULL, NULL, NULL),
('pageParameter.s2.value', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s1', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s1.company', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s1.email', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s1.name', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s2', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s2.descriptionModule', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s2.moduleTypeName', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s2.nameModule', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s3', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s3.client', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s3.pageDescription', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s3.pageName', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s3.pageTypeName', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s4', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s4.client', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s4.pageParameter', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s4.pageParameterDefaultValue', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s4.value', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s5', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s5.client', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s5.pageParameter', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s5.pageParameterDefaultValue', 'Y', NULL, NULL, NULL, NULL),
('pageParameterModule.s5.value', 'Y', NULL, NULL, NULL, NULL),
('parameter', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1.description', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1.id', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1.name', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1.parameterType', 'Y', NULL, NULL, NULL, NULL),
('parameter.s1.value', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2.description', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2.id', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2.name', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2.parameterType', 'Y', NULL, NULL, NULL, NULL),
('parameter.s2.value', 'Y', NULL, NULL, NULL, NULL),
('parameterClient', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s1', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s1.company', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s1.email', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s1.name', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2.client', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2.defaultValue', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2.parameterName', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2.parameterTypeName', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s2.value', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s3', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s3.client', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s3.parameter', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s3.parameterDefaultValue', 'Y', NULL, NULL, NULL, NULL),
('parameterClient.s3.value', 'Y', NULL, NULL, NULL, NULL),
('product', 'Y', NULL, NULL, NULL, NULL),
('product.s1', 'Y', NULL, NULL, NULL, NULL),
('product.s1.color', 'Y', NULL, NULL, NULL, NULL),
('product.s1.designation', 'Y', NULL, NULL, NULL, NULL),
('product.s1.family', 'Y', NULL, NULL, NULL, NULL),
('product.s1.priceBuy', 'Y', NULL, NULL, NULL, NULL),
('product.s1.pricesale', 'Y', NULL, NULL, NULL, NULL),
('product.s1.quantity', 'Y', NULL, NULL, NULL, NULL),
('product.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('product.s1.size', 'Y', NULL, NULL, NULL, NULL),
('product.s1.status', 'Y', NULL, NULL, NULL, NULL),
('product.s1.title', 'Y', NULL, NULL, NULL, NULL),
('product.s2', 'Y', NULL, NULL, NULL, NULL),
('product.s2.active', 'Y', NULL, NULL, NULL, NULL),
('product.s2.color', 'Y', NULL, NULL, NULL, NULL),
('product.s2.designation', 'Y', NULL, NULL, NULL, NULL),
('product.s2.family', 'Y', NULL, NULL, NULL, NULL),
('product.s2.group', 'Y', NULL, NULL, NULL, NULL),
('product.s2.note', 'Y', NULL, NULL, NULL, NULL),
('product.s2.priceBuy', 'Y', NULL, NULL, NULL, NULL),
('product.s2.pricesale', 'Y', NULL, NULL, NULL, NULL),
('product.s2.quantity', 'Y', NULL, NULL, NULL, NULL),
('product.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('product.s2.size', 'Y', NULL, NULL, NULL, NULL),
('product.s2.status', 'Y', NULL, NULL, NULL, NULL),
('product.s2.type', 'Y', NULL, NULL, NULL, NULL),
('profile.s1', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.adress', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.email', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.firstName', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.fixedPhone', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.lastName', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.password', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.replayRassword', 'Y', NULL, NULL, NULL, NULL),
('reception', 'Y', NULL, NULL, NULL, NULL),
('reception.s1', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.active', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.amount', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.deadline', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.id', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.name', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.souche', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.supplierId', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.valid', 'Y', NULL, NULL, NULL, NULL),
('reception.s2', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.amount', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.deadline', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.deposit', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.description', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.id', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.name', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.sizes', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.souche', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.valid', 'Y', NULL, NULL, NULL, NULL),
('reception.s3', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.id', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.remise', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.tva', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('reception.s4', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.designation', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.id', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.quantity', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.remise', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.totalHt', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.tva', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('reception.s5', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.quantityTotal', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.remiseAvg', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.tvaAvg', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.unitPriceBuyTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.active', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.amount', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.deadline', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.id', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.name', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.souche', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.supplierId', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.amount', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.deadline', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.deposit', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.description', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.id', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.name', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.souche', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.id', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.remise', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.tva', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.quantityTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.remiseAvg', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.tvaAvg', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.unitPriceBuyTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.active', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.amount', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.deadline', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.id', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.name', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.souche', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.supplierId', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.amount', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.deadline', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.deposit', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.description', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.id', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.name', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.souche', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.id', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.remise', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.tva', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.quantityTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.remiseAvg', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.tvaAvg', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.unitPriceBuyTotal', 'Y', NULL, NULL, NULL, NULL),
('rofile.s1.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('stockState', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.productNotValid', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.productValid', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.quantityCount', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.receptionInprogressCount', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.receptionValidCount', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.unitPriceBuyMax', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.unitPriceBuyMin', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.unitPriceBuyMoyenne', 'Y', NULL, NULL, NULL, NULL),
('supplier', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.active', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.adress', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.category', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.city', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.companyName', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.country', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.email', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.firstName', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.fixedPhone', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.fullLabel', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.lastName', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.note', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.secondaryAddress', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.shortLabel', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.type', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.webSite', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.zipCode', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.adress', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.city', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.companyName', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.country', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.email', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.firstName', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.fixedPhone', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.lastName', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.postCode', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.type', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.dataTypeChar', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.dataTypeDateTime', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.dataTypeDouble', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.dataTypeInteger', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeCodePostale', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeDate', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeDateTime', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeEmail', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeFax', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypePhone', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.formatTypeTime', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.maxlenght', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.maxWord', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.required', 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tmp_inf_text`
--

CREATE TABLE `tmp_inf_text` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `PREFIX` bigint(20) DEFAULT NULL,
  `ITEM_CODE` varchar(255) DEFAULT NULL,
  `VALUE` longtext,
  `TEXT_TYPE_ID` bigint(20) DEFAULT NULL,
  `LANGUAGE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `tmp_inf_text`
--

INSERT INTO `tmp_inf_text` (`ID`, `PREFIX`, `ITEM_CODE`, `VALUE`, `TEXT_TYPE_ID`, `LANGUAGE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(2, 1, 'login.s1.password', 'Mot de passe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(3, 1, 'login.s1.username', 'E-mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(4, 1, 'login.s1.username', 'username', 6, 1, 'Y', NULL, NULL, NULL, NULL),
(5, 1, 'login.s1.login', 'Se connecter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(10, 1, 'page1.s1.name', 'Résumé Général', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(11, 1, 'validation.v1.dataTypeInteger', '{0} doit être de type integer', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(12, 1, 'validation.v1.dataTypeDouble', '{0} doit être de type double', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(13, 1, 'validation.v1.maxWord', '{0} ne doit pas dépasser {1} mots', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(14, 1, 'validation.v1.formatTypeDate', '{0} doit être de type date', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(15, 1, 'validation.v1.dataTypeChar', '{0} doit être de type caractère', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(16, 1, 'validation.v1.dataTypeDateTime', '{0} doit être de type datetime', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(17, 1, 'validation.v1.formatTypeEmail', '{0} doit être de type email', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(18, 1, 'validation.v1.formatTypePhone', '{0} doit être de type phone', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(19, 1, 'validation.v1.maxlenght', '{0} ne doit pas dépasser {1} char', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(20, 1, 'validation.v1.required', '{0} est obligatoire', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(26, 1, 'p003.s1', 'La liste des dépenses', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(27, 1, 'p003.s1.name', 'Libelle', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(28, 1, 'p003.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(29, 1, 'p003.s1.amount', 'Montant', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(30, 1, 'p003.s1.expenseTypeName', 'Type de charge', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(34, 1, 'p003.s2', 'Ajouter / Modifier un dépense', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(36, 1, 'p003.s2.type', 'Type de charge', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(37, 1, 'p003.s2.name', 'Libelle', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(38, 1, 'p003.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(39, 1, 'p003.s2.amount', 'Montant', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(41, 1, 'supplier.s1', 'Ajouter / Modifier un fournisseur', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(43, 1, 'supplier.s2', 'Liste des fournisseurs', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(45, 1, 'supplier.s2.country', 'Pays', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(46, 1, 'supplier.s2.city', 'Ville', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(47, 1, 'supplier.s2.email', 'E-Mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(49, 1, 'supplier.s2.fixedPhone', 'Télé Fixe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(50, 1, 'supplier.s2.cellPhone', 'Télé Portable', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(51, 1, 'supplier.s2.companyName', 'Nom de Société ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(52, 1, 'product.s1', 'List Produits :', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(53, 1, 'product.s1.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(54, 1, 'product.s1.quantity', 'quantity', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(55, 1, 'product.s1.reference', 'reference', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(56, 1, 'product.s1.pricesale', 'pricesale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(57, 1, 'product.s1.status', 'status', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(58, 1, 'product.s1.size', 'size', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(59, 1, 'product.s1.family', 'family', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(60, 1, 'product.s1.color', 'color', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(61, 1, 'product.s1.edit', 'edit', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(62, 1, 'product.s1.delete', 'delete', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(63, 1, 'product.s1', 'List Produit : ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(64, 1, 'product.s2.cancel', 'Annuler', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(65, 1, 'product.s2.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(66, 1, 'product.s2.color', 'couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(67, 1, 'product.s2.family', 'Famille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(68, 1, 'product.s2.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(69, 1, 'product.s2.status', 'Status', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(70, 1, 'product.s2.pricesale', 'Prix de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(71, 1, 'product.s2.reference', 'Référence', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(72, 1, 'product.s2.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(73, 1, 'product.s2.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(74, 1, 'product.s2', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(75, 1, 'supplier.s1.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(76, 1, 'supplier.s1.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(77, 1, 'supplier.s1.country', 'Pays', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(78, 1, 'supplier.s1.city', 'Ville', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(79, 1, 'supplier.s1.companyName', 'Nom de Société ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(80, 1, 'supplier.s1.cellPhone', 'Télé Portable', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(81, 1, 'supplier.s1.fixedPhone', 'Télé Fixe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(82, 1, 'supplier.s1.adress', 'Adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(83, 1, 'supplier.s1.email', 'E-Mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(84, 1, 'supplier.s1.type', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(89, 1, 'supplier.s2.type', 'Type ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(90, 1, 'globals.list.delete', 'Supprimer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(91, 1, 'globals.list.edit', 'Modifier', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(92, 1, 'globals.list.option', 'Option', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(93, 1, 'globals.list.activate', 'Activer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(94, 1, 'globals.list.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(95, 1, 'globals.list.vide', 'la liste est vide.', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(96, 1, 'globals.list.confirmDelete', 'Êtes-vous sûr de vouloir supprimer ?', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(97, 1, 'globals.forms.add', 'Ajouter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(98, 1, 'globals.forms.edit', 'Modifier', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(99, 1, 'globals.forms.cancel', 'Annuler', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(100, 1, 'globals.forms.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(101, 1, 'globals.forms.delete', 'Supprimer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(102, 1, 'globals.forms.confirmDelete', 'Êtes-vous sûr de vouloir supprimer ?', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(103, 1, 'reception.s1', 'La liste des réceptions en cours', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(104, 1, 'reception.s1.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(105, 1, 'reception.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(106, 1, 'reception.s1.supplierId', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(107, 1, 'reception.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(108, 1, 'reception.s1.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(109, 1, 'reception.s1.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(110, 1, 'reception.s1.amount', 'Le montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(111, 1, 'reception.s1.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(112, 1, 'globals.forms.yes', 'Oui', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(113, 1, 'globals.forms.no', 'Non', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(146, 1, 'reception.s2', 'Ajouter une réception ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(147, 1, 'reception.s2.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(148, 1, 'reception.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(149, 1, 'reception.s2.supplier', 'Fournisseur ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(150, 1, 'reception.s2.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(151, 1, 'reception.s2.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(152, 1, 'reception.s2.amount', 'Montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(153, 1, 'reception.s2.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(155, 1, 'reception.s2.deposit', 'Depôt', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(156, 1, 'reception.s2.description', 'Description ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(158, 1, 'supplier.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(159, 1, 'product', 'La gestion des produits ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(160, 1, 'product.s2.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(161, 1, 'p003.s1.expenseTypeName', 'Type de charge', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(162, 1, 'customer.s2', 'La liste des clients', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(163, 1, 'customer.s2.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(164, 1, 'customer.s1', 'Ajouter / Modifier un client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(165, 1, 'customer.s2.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(166, 1, 'customer.s2.country', 'Pays', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(167, 1, 'customer.s2.city', 'Ville', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(168, 1, 'customer.s2.email', 'E-Mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(169, 1, 'customer.s2.adress', 'Adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(170, 1, 'customer.s2.fixedPhone', 'Télé Fixe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(171, 1, 'customer.s2.cellPhone', 'Télé Portable', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(172, 1, 'customer.s1.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(173, 1, 'customer.s1.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(174, 1, 'customer.s1.country', 'Pays', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(175, 1, 'customer.s1.city', 'Ville', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(176, 1, 'customer.s1.cellPhone', 'Télé Portable', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(177, 1, 'customer.s1.fixedPhone', 'Télé Fixe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(178, 1, 'customer.s1.adress', 'Adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(179, 1, 'customer.s1.email', 'E-Mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(180, 1, 'customer.s1.type', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(186, 1, 'customer.s1.active', 'Active ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(187, 1, 'product.s1.priceBuy', 'price by', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(188, 1, 'product.s2.priceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(189, 1, 'supplier.s1.zipCode', 'Code postale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(190, 1, 'supplier.s2.postCode', 'Code postale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(202, 1, 'reception.s3', 'Ajouter des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(203, 1, 'reception.s3.id', 'Code d''article ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(204, 1, 'reception.s3.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(205, 1, 'reception.s3.quantity', 'Qté ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(206, 1, 'reception.s3.unitPriceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(207, 1, 'reception.s3.remise', 'Remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(208, 1, 'reception.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(209, 1, 'reception.s3.tva', 'Tva', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(210, 1, 'reception.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(211, 1, 'globals.forms.confirmClean', 'Etes-vous sur de vouloir vider le formulaire ?', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(212, 1, 'globals.forms.clean', 'vider le fourmulaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(213, 1, 'reception.s4', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(214, 1, 'reception.s4.id', 'Code d''article ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(215, 1, 'reception.s4.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(216, 1, 'reception.s4.quantity', 'Qté ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(217, 1, 'reception.s4.unitPriceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(218, 1, 'reception.s4.remise', 'Remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(219, 1, 'reception.s4.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(220, 1, 'reception.s4.tva', 'Tva', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(221, 1, 'reception.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(222, 1, 'page1.s1.name', 'name msg', 2, 1, 'Y', NULL, NULL, NULL, NULL),
(223, 1, 'page1.s1.name', 'name help', 4, 1, 'Y', NULL, NULL, NULL, NULL),
(224, 1, 'globals.forms.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(225, 1, 'globals.forms.validate', 'Validate', 3, 2, 'Y', NULL, NULL, NULL, NULL),
(226, 1, 'receptionValidation.s1', 'La liste des réceptions qui sont prêt pour la validation', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(227, 1, 'receptionValidation.s1.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(228, 1, 'receptionValidation.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(229, 1, 'receptionValidation.s1.supplierId', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(230, 1, 'receptionValidation.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(231, 1, 'receptionValidation.s1.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(232, 1, 'receptionValidation.s1.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(233, 1, 'receptionValidation.s1.amount', 'Le montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(234, 1, 'receptionValidation.s1.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(235, 1, 'receptionValidation.s2', 'Détails de réception ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(236, 1, 'receptionValidation.s2.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(237, 1, 'receptionValidation.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(238, 1, 'receptionValidation.s2.supplier', 'Fournisseur ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(239, 1, 'receptionValidation.s2.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(240, 1, 'receptionValidation.s2.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(241, 1, 'receptionValidation.s2.amount', 'Montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(242, 1, 'receptionValidation.s2.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(243, 1, 'receptionValidation.s2.deposit', 'Depôt', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(244, 1, 'receptionValidation.s2.description', 'Description ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(245, 1, 'receptionValidation.s3', 'Ajouter des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(246, 1, 'receptionValidation.s3.id', 'Code d''article ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(247, 1, 'receptionValidation.s3.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(248, 1, 'receptionValidation.s3.quantity', 'Qté ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(249, 1, 'receptionValidation.s3.unitPriceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(250, 1, 'receptionValidation.s3.remise', 'Remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(251, 1, 'receptionValidation.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(252, 1, 'receptionValidation.s3.tva', 'Tva', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(253, 1, 'receptionValidation.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(254, 1, 'receptionHistory.s1', 'Hisortique des réceptions', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(255, 1, 'receptionHistory.s1.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(256, 1, 'receptionHistory.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(257, 1, 'receptionHistory.s1.supplierId', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(258, 1, 'receptionHistory.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(259, 1, 'receptionHistory.s1.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(260, 1, 'receptionHistory.s1.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(261, 1, 'receptionHistory.s1.amount', 'Le montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(262, 1, 'receptionHistory.s1.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(263, 1, 'receptionHistory.s2', 'Détails de réception ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(264, 1, 'receptionHistory.s2.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(265, 1, 'receptionHistory.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(266, 1, 'receptionHistory.s2.supplier', 'Fournisseur ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(267, 1, 'receptionHistory.s2.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(268, 1, 'receptionHistory.s2.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(269, 1, 'receptionHistory.s2.amount', 'Montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(270, 1, 'receptionHistory.s2.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(271, 1, 'receptionHistory.s2.deposit', 'Depôt', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(272, 1, 'receptionHistory.s2.description', 'Description ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(273, 1, 'receptionHistory.s3', 'Ajouter des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(274, 1, 'receptionHistory.s3.id', 'Code d''article ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(275, 1, 'receptionHistory.s3.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(276, 1, 'receptionHistory.s3.quantity', 'Qté ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(277, 1, 'receptionHistory.s3.unitPriceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(278, 1, 'receptionHistory.s3.remise', 'Remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(279, 1, 'receptionHistory.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(280, 1, 'receptionHistory.s3.tva', 'Tva', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(281, 1, 'receptionHistory.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(282, 1, 'receptionValidation.s4', 'Résumé', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(283, 1, 'receptionValidation.s4.quantityTotal', 'La quantité totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(284, 1, 'receptionValidation.s4.unitPriceBuyTotal', 'Le prix unitaire totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(285, 1, 'receptionValidation.s4.remiseAvg', 'La moyenne de remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(286, 1, 'receptionValidation.s4.tvaAvg', 'La monyenne de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(287, 1, 'receptionValidation.s4.totalHtTotal', 'hore taxt total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(288, 1, 'receptionValidation.s4.totalTtcTotal', 'TTC total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(289, 1, 'receptionHistory.s4', 'Résumé Général', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(290, 1, 'receptionHistory.s4.quantityTotal', 'La quantité totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(291, 1, 'receptionHistory.s4.unitPriceBuyTotal', 'Le prix unitaire totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(292, 1, 'receptionHistory.s4.remiseAvg', 'La moyenne de remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(293, 1, 'receptionHistory.s4.tvaAvg', 'La monyenne de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(294, 1, 'receptionHistory.s4.totalHtTotal', 'hore taxt total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(295, 1, 'receptionHistory.s4.totalTtcTotal', 'TTC total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(303, 1, 'reception.s5', 'Résumé Général', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(304, 1, 'reception.s5.quantityTotal', 'La quantité totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(305, 1, 'reception.s5.unitPriceBuyTotal', 'Le prix unitaire totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(306, 1, 'reception.s5.remiseAvg', 'La moyenne de remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(307, 1, 'reception.s5.tvaAvg', 'La monyenne de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(308, 1, 'reception.s5.totalHtTotal', 'hore taxt total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(309, 1, 'reception.s5.totalTtcTotal', 'TTC total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(310, 1, 'reception.s4.totalHt', 'Total HT', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(311, 1, 'reception.s4.totalTtc', 'Total TTC', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(312, 1, 'inventaire.s1', 'Inventaire générale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(313, 1, 'inventaire.s1.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(314, 1, 'inventaire.s1.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(315, 1, 'inventaire.s1.quantity', 'Quantité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(316, 1, 'inventaire.s1.priceBuy', 'Prix de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(317, 1, 'inventaire.s1.priceSale', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(318, 1, 'inventaire.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(319, 1, 'inventaire.s1.receptionValidCount', 'Nombre de réception', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(320, 1, 'inventaire.s1.unitPriceBuyMax', 'Max de prix de vent', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(321, 1, 'inventaire.s1.unitPriceBuyMoyenne', 'Moyenne de prix de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(322, 1, 'inventaire.s1.unitPriceBuyMin', 'Min de prix de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(323, 1, 'inventaire.s1.quantityCount', 'Total des quantité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(324, 1, 'stockState', 'Statut de stock', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(325, 1, 'stockState.s1', 'Statut de stock', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(326, 1, 'stockState.s1.receptionValidCount', 'Le nombre des réceptions valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(327, 1, 'stockState.s1.unitPriceBuyMax', 'le max de prix unitaire de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(328, 1, 'stockState.s1.unitPriceBuyMoyenne', 'la moyenne de prix unitaire de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(329, 1, 'stockState.s1.unitPriceBuyMin', 'Le min de prix de vent unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(330, 1, 'stockState.s1.quantityCount', 'Total des quantités', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(331, 1, 'stockState.s1.productValid', 'Le nombre de produit valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(332, 1, 'stockState.s1.productNotValid', 'Le nombre de produit non valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(333, 1, 'stockState.s1.receptionInprogressCount', 'Le nombre des réceptions qui sont en cours', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(334, 1, 'login.s1.forgetPassword', 'Mot de passe oublié.', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(335, 1, 'login.s1.getAccount', 'Obtenir d''un compte', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(336, 1, 'login.s1.sessionActive', 'Garder ma session active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(337, 1, 'login.s1', 'Authentification', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(338, 1, 'module.s1.access', 'Accès au module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(339, 1, 'module.s1.logout', 'Déconnexion', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(340, 1, 'bonLivraison.s1', 'Nouveau bon de livraison', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(341, 1, 'bonLivraison.s1.customer', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(342, 1, 'bonLivraison.s1.description', 'Note', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(343, 1, 'bonLivraison.s2', 'Ajouter un produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(344, 1, 'bonLivraison.s2.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(345, 1, 'bonLivraison.s2.color', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(346, 1, 'bonLivraison.s2.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(347, 1, 'bonLivraison.s2.unitPriceBuy', 'Prix', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(348, 1, 'bonLivraison.s2.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(349, 1, 'bonLivraison.s2.totalTtc', 'Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(350, 1, 'bonLivraison.s3', 'La liste des articles', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(351, 1, 'bonLivraison.s3.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(352, 1, 'bonLivraison.s3.color', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(353, 1, 'bonLivraison.s3.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(354, 1, 'bonLivraison.s3.price', 'Prix Unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(355, 1, 'bonLivraison.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(356, 1, 'bonLivraison.s3.totalPriceBuy', 'Prix Totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(357, 1, 'bonLivraison.s4', 'Résumé Général', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(358, 1, 'bonLivraison.s5', 'Mode de paiement', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(359, 1, 'bonLivraison.s4.sumQuantity', 'Quantité Totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(360, 1, 'bonLivraison.s4.sumTotal', 'Montant Totale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(361, 1, 'blValidation.s1.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(362, 1, 'blValidation.s1.customerName', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(363, 1, 'blValidation.s1.description', 'Note', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(364, 1, 'blValidation.s1.sumTotalPriceBuy', 'Prix Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(365, 1, 'blValidation.s1', 'La liste des ventes attentes', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(366, 1, 'blValidation.s2', 'Modifer un vente', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(367, 1, 'blValidation.s2.customer', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(368, 1, 'blValidation.s2.description', 'Note', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(369, 1, 'blValidation.s3.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(370, 1, 'blValidation.s3.color', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(371, 1, 'blValidation.s3.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(372, 1, 'blValidation.s3.unitPriceBuy', 'Prix', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(373, 1, 'blValidation.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(374, 1, 'blValidation.s3.totalTtc', 'Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(375, 1, 'blValidation.s4', 'La liste des articles', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(376, 1, 'blValidation.s4.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(377, 1, 'blValidation.s4.color', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(378, 1, 'blValidation.s4.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(379, 1, 'blValidation.s4.price', 'Prix', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(380, 1, 'blValidation.s4.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(381, 1, 'blValidation.s4.totalPriceBuy', 'Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(382, 1, 'blValidation.s5', 'Résumé Général', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(383, 1, 'blValidation.s5.sumQuantity', 'Qté Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(384, 1, 'blValidation.s5.sumTotal', 'Montant Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(385, 1, 'blValidation.s5', 'Mode de paiement', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(386, 1, 'blValidation.s3', 'Ajouter un article', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(387, 1, 'blValidation.s1.sumQuantity', 'Qté Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(388, 1, 'blValidation.s6', 'Mode de paiement', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(389, 1, 'reception.s2.sizes', 'Taille de Réception', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(390, 1, 'inventaire.s1.productSizeName', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(391, 1, 'inventaire.s1.productColorName', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(393, 1, 'profile.s1.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(394, 1, 'profile.s1.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(395, 1, 'profile.s1.email', 'E-mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(396, 1, 'profile.s1.password', 'Mot de passe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(397, 1, 'profile.s1.replayRassword', 'Re mot de passe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(398, 1, 'profile.s1.cellPhone', 'Télé Mobile', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(399, 1, 'profile.s1.fixedPhone', 'Télé Fixe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(400, 1, 'profile.s1.adress', 'Adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(401, 1, 'profile.s1', 'Modifier Mon Profile', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(402, 1, 'basicParameter.s1', 'Basic Parameter', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(403, 1, 'basicParameter.s1.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(404, 1, 'basicParameter.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(405, 1, 'basicParameter.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(406, 1, 'basicParameter.s1.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(407, 1, 'basicParameter.s1.basicParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(408, 1, 'basicParameter.s2', 'Ajouter / Editer basic Parameter', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(409, 1, 'basicParameter.s2.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(410, 1, 'basicParameter.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(411, 1, 'basicParameter.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(412, 1, 'basicParameter.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(413, 1, 'basicParameter.s2.basicParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(414, 1, 'basicParameter.s1', 'Basic Parameter', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(415, 1, 'basicParameter.s1.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(416, 1, 'basicParameter.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(417, 1, 'basicParameter.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(418, 1, 'basicParameter.s1.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(419, 1, 'basicParameter.s1.basicParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(420, 1, 'basicParameter.s2', 'Ajouter / Editer basic Parameter', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(421, 1, 'basicParameter.s2.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(422, 1, 'basicParameter.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(423, 1, 'basicParameter.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(424, 1, 'basicParameter.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(425, 1, 'basicParameter.s2.basicParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(426, 1, 'moduleParameter.s1', 'Parameter du module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(427, 1, 'moduleParameter.s1.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(428, 1, 'moduleParameter.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(429, 1, 'moduleParameter.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(430, 1, 'moduleParameter.s1.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(431, 1, 'moduleParameter.s1.moduleParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(432, 1, 'moduleParameter.s2', 'Ajouter / Editer Parameter du module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(433, 1, 'moduleParameter.s2.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(434, 1, 'moduleParameter.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(435, 1, 'moduleParameter.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(436, 1, 'moduleParameter.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(437, 1, 'moduleParameter.s2.moduleParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(438, 1, 'parameterClient.s1', 'Liste du client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(439, 1, 'parameterClient.s1.name', 'Nom et prenom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(440, 1, 'parameterClient.s1.company', 'Societer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(441, 1, 'parameterClient.s1.email', 'Email', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(442, 1, 'parameterClient.s2', 'Parameter du client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(443, 1, 'parameterClient.s2.client', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(444, 1, 'parameterClient.s2.parameterName', 'Nom de parameter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(445, 1, 'parameterClient.s2.parameterTypeName', 'Type de parameter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(446, 1, 'parameterClient.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(447, 1, 'parameterClient.s2.defaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(448, 1, 'parameterClient.s3', 'Ajouter / Editer Parameter du client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(449, 1, 'parameterClient.s3.client', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(450, 1, 'parameterClient.s3.parameter', 'Nom de parameter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(451, 1, 'parameterClient.s3.parameterDefaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(452, 1, 'parameterClient.s3.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(453, 1, 'parameter.s1', 'Paramètres', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(454, 1, 'parameter.s1.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(455, 1, 'parameter.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(456, 1, 'parameter.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(457, 1, 'parameter.s1.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(458, 1, 'parameter.s1.parameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(459, 1, 'parameter.s2', 'Ajouter / Editer paramètres', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(460, 1, 'parameter.s2.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(461, 1, 'parameter.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(462, 1, 'parameter.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(463, 1, 'parameter.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(464, 1, 'parameter.s2.parameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(465, 1, 'pageParameterModule.s1', 'Liste du client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(466, 1, 'pageParameterModule.s1.name', 'Nom et prenom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(467, 1, 'pageParameterModule.s1.company', 'Societer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(468, 1, 'pageParameterModule.s1.email', 'Email', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(469, 1, 'pageParameterModule.s2', 'Liste du module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(470, 1, 'pageParameterModule.s2.nameModule', 'Nom du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(471, 1, 'pageParameterModule.s2.descriptionModule', 'Description du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(472, 1, 'pageParameterModule.s2.moduleTypeName', 'Type du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(473, 1, 'pageParameterModule.s3', 'Liste du page', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(474, 1, 'pageParameterModule.s3.client', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(475, 1, 'pageParameterModule.s3.pageName', 'Nom du page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(476, 1, 'pageParameterModule.s3.pageDescription', 'Description du page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(477, 1, 'pageParameterModule.s3.pageTypeName', 'Nom du type page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(478, 1, 'pageParameterModule.s4', 'Ajouter / Editer Parameter du page/module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(479, 1, 'pageParameterModule.s4.pageParameter', 'Nom de parameter du page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(480, 1, 'pageParameterModule.s4.pageParameterDefaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(481, 1, 'pageParameterModule.s4.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(482, 1, 'pageParameterModule.s5', 'Ajouter / Editer Parameter du page/module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(483, 1, 'pageParameterModule.s5.pageParameter', 'Nom de parameter du page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(484, 1, 'pageParameterModule.s5.pageParameterDefaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(485, 1, 'pageParameterModule.s5.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(486, 1, 'pageParameter.s1', 'Parameter du page', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(487, 1, 'pageParameter.s1.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(488, 1, 'pageParameter.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(489, 1, 'pageParameter.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(490, 1, 'pageParameter.s1.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(491, 1, 'pageParameter.s1.pageParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(492, 1, 'pageParameter.s1.page', 'Page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(493, 1, 'pageParameter.s2', 'Ajouter / Editer Parameter du page', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(494, 1, 'pageParameter.s2.id', 'ID', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(495, 1, 'pageParameter.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(496, 1, 'pageParameter.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(497, 1, 'pageParameter.s2.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(498, 1, 'pageParameter.s2.pageParameterType', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(499, 1, 'pageParameter.s2.page', 'Page', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(500, 1, 'moduleParameterClient.s1', 'Liste du client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(501, 1, 'moduleParameterClient.s1.name', 'Nom et prenom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(502, 1, 'moduleParameterClient.s1.company', 'Societer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(503, 1, 'moduleParameterClient.s1.email', 'Email', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(504, 1, 'moduleParameterClient.s2', 'Liste du module', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(505, 1, 'moduleParameterClient.s2.nameModule', 'Nom du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(506, 1, 'moduleParameterClient.s2.descriptionModule', 'Description du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(507, 1, 'moduleParameterClient.s2.moduleTypeName', 'Type du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(508, 1, 'moduleParameterClient.s3', 'Parameter du module/client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(509, 1, 'moduleParameterClient.s3.client', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(510, 1, 'moduleParameterClient.s3.parameterModuleName', 'Nom de parameter du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(511, 1, 'moduleParameterClient.s3.parameterModuleTypeName', 'Type de parameter du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(512, 1, 'moduleParameterClient.s3.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(513, 1, 'moduleParameterClient.s3.defaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(514, 1, 'moduleParameterClient.s4', 'Ajouter / Editer Parameter du module/client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(515, 1, 'moduleParameterClient.s4.client', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(516, 1, 'moduleParameterClient.s4.parameterModule', 'Nom de parameter du module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(517, 1, 'moduleParameterClient.s4.parameterModuleDefaultValue', 'Valeur par défaut', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(518, 1, 'moduleParameterClient.s4.value', 'Valuer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(519, 1, 'validation.v1.formatTypeFax', '{0} doit être de type fax', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(520, 1, 'validation.v1.formatTypeCodePostale', '{0} doit être de type code postale', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(521, 1, 'validation.v1.formatTypeTime', '{0} doit être de type time', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(522, 1, 'validation.v1.formatTypeDateTime', '{0} doit être de type datetime', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(523, 1, 'validation.v1.formatTypePattern', '{0} doit être un {1}', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(524, 1, 'product.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(525, 1, 'product.s2.group', 'Groupe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(526, 1, 'product.s2.type', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(527, 1, 'customer.s1.secondaryAddress ', 'Adresse 2', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(528, 1, 'customer.s1.category ', 'Catégorie', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(529, 1, 'customer.s1.company', 'Société', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(530, 1, 'customer.s1.zipCode', 'Code postale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(531, 1, 'customer.s1.shortLabel', 'Libelle court', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(532, 1, 'customer.s1.fullLabel', 'Libelle complet', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(533, 1, 'customer.s1.webSite', 'Site web', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(534, 1, 'customer.s1.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(535, 1, 'supplier.s1.secondaryAddress', 'Adresse 2', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(536, 1, 'supplier.s1.shortLabel', 'Libelle court', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(537, 1, 'supplier.s1.fullLabel', 'Libelle complet', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(538, 1, 'supplier.s1.webSite', 'Site web', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(539, 1, 'supplier.s1.category', 'Catégorie', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(540, 1, 'supplier.s1.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(541, 1, 'p006.s2.code', 'Code réception', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(542, 1, 'p006.s2.supplier', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(543, 1, 'p006.s2.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(544, 1, 'p006.s2.deadline', 'Date d''écheance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(545, 1, 'p006.s2.note', 'Note interne', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(546, 1, 'p006.s2.deposit', 'Dépôt', 3, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tmp_pm_attribute_exclud`
--

CREATE TABLE `tmp_pm_attribute_exclud` (
`ID` bigint(20) NOT NULL,
  `PAGE_ID` bigint(20) DEFAULT NULL,
  `INF_ITEM_CODE` tinytext,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `MODEL_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `tmp_pm_attribute_exclud`
--

INSERT INTO `tmp_pm_attribute_exclud` (`ID`, `PAGE_ID`, `INF_ITEM_CODE`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `MODEL_ID`) VALUES
(1, 3, 'ok', 1, 'Y', NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `tmp_pm_attribute_validation`
--

CREATE TABLE `tmp_pm_attribute_validation` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `PAGE_ATTRIBUTE_ID` bigint(20) DEFAULT NULL,
  `VALIDATION_VALIDATION_ID` bigint(20) DEFAULT NULL,
  `PARAMS` mediumtext CHARACTER SET latin1,
  `CUSTOM_ERROR` text CHARACTER SET latin1,
  `MODEL_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `tmp_pm_attribute_validation`
--

INSERT INTO `tmp_pm_attribute_validation` (`ID`, `PAGE_ATTRIBUTE_ID`, `VALIDATION_VALIDATION_ID`, `PARAMS`, `CUSTOM_ERROR`, `MODEL_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, 10, '', 'error', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 1, 7, '', NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(3, 1, 9, '3', NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(5, 3, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(7, 3, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(8, 3, 2, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(10, 3, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(11, 4, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(14, 4, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(15, 4, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(16, 4, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(18, 4, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(19, 4, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(20, 4, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(21, 4, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(22, 6, 10, '', 'error', 1, 'Y', NULL, NULL, NULL, NULL),
(24, 6, 10, '', '', 1, 'Y', NULL, NULL, NULL, NULL),
(31, 5, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(32, 5, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(33, 5, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(34, 5, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(35, 5, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(36, 5, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(37, 5, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(38, 5, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(39, 5, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(40, 5, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(41, 5, 1, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(42, 5, 2, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(43, 3, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(44, 5, 2, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(45, 5, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(46, 4, 1, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(50, 7, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(51, 7, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(52, 7, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(53, 7, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(54, 7, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(55, 13, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(56, 13, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(57, 1, 10, '', 'error', 2, 'Y', NULL, NULL, NULL, NULL),
(58, 1, 7, '', NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(59, 1, 9, '3', NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(60, 3, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(61, 3, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(62, 3, 2, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(63, 3, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(64, 4, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(65, 4, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(66, 4, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(67, 4, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(68, 4, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(69, 4, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(70, 4, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(71, 4, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(72, 6, 10, '', 'error', 2, 'Y', NULL, NULL, NULL, NULL),
(73, 6, 10, '', '', 2, 'Y', NULL, NULL, NULL, NULL),
(74, 5, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(75, 5, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(76, 5, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(77, 5, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(78, 5, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(79, 5, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(80, 5, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(81, 5, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(82, 5, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(83, 5, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(84, 5, 1, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(85, 5, 2, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(86, 3, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(87, 5, 2, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(88, 5, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(89, 4, 1, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(90, 7, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(91, 7, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(92, 7, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(93, 7, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(94, 7, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(95, 13, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(96, 13, 10, NULL, NULL, 2, 'Y', NULL, NULL, NULL, NULL),
(120, 1, 10, '', 'error', NULL, 'Y', NULL, NULL, NULL, NULL),
(121, 1, 7, '', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(122, 1, 9, '3', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(123, 3, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(124, 3, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(125, 3, 2, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(126, 3, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(127, 4, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(128, 4, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(129, 4, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(130, 4, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(131, 4, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(132, 4, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(133, 4, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(134, 4, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(135, 6, 10, '', 'error', NULL, 'Y', NULL, NULL, NULL, NULL),
(136, 6, 10, '', '', NULL, 'Y', NULL, NULL, NULL, NULL),
(137, 5, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(138, 5, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(139, 5, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(140, 5, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(141, 5, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(142, 5, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(143, 5, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(144, 5, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(145, 5, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(146, 5, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(147, 5, 1, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(148, 5, 2, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(149, 3, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(150, 5, 2, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(151, 5, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(152, 4, 1, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(153, 7, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(154, 7, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(155, 7, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(156, 7, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(157, 7, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(158, 13, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(159, 13, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(183, 1, 10, '', 'error', 3, 'Y', NULL, NULL, NULL, NULL),
(184, 1, 7, '', NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(185, 1, 9, '3', NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(186, 3, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(187, 3, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(188, 3, 2, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(189, 3, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(190, 4, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(191, 4, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(192, 4, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(193, 4, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(194, 4, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(195, 4, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(196, 4, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(197, 4, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(198, 6, 10, '', 'error', 3, 'Y', NULL, NULL, NULL, NULL),
(199, 6, 10, '', '', 3, 'Y', NULL, NULL, NULL, NULL),
(200, 5, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(201, 5, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(202, 5, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(203, 5, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(204, 5, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(205, 5, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(206, 5, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(207, 5, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(208, 5, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(209, 5, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(210, 5, 1, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(211, 5, 2, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(212, 3, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(213, 5, 2, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(214, 5, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(215, 4, 1, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(216, 7, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(217, 7, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(218, 7, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(219, 7, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(220, 7, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(221, 13, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(222, 13, 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(246, 1, 10, '', 'error', NULL, 'Y', NULL, NULL, NULL, NULL),
(247, 1, 7, '', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(248, 1, 9, '3', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(249, 3, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(250, 3, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(251, 3, 2, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(252, 3, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(253, 4, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(254, 4, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(255, 4, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(256, 4, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(257, 4, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(258, 4, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(259, 4, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(260, 4, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(261, 6, 10, '', 'error', NULL, 'Y', NULL, NULL, NULL, NULL),
(262, 6, 10, '', '', NULL, 'Y', NULL, NULL, NULL, NULL),
(263, 5, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(264, 5, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(265, 5, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(266, 5, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(267, 5, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(268, 5, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(269, 5, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(270, 5, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(271, 5, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(272, 5, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(273, 5, 1, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(274, 5, 2, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(275, 3, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(276, 5, 2, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(277, 5, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(278, 4, 1, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(279, 7, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(280, 7, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(281, 7, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(282, 7, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(283, 7, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(284, 13, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(285, 13, 10, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(286, 4, 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tmp_pm_composition`
--

CREATE TABLE `tmp_pm_composition` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `GROUP_ID` bigint(20) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `MENU_ID` bigint(20) DEFAULT NULL,
  `PAGE_ID` bigint(20) DEFAULT NULL,
  `INDEX_SHOW` char(1) DEFAULT NULL,
  `GROUP_SORT` bigint(20) DEFAULT NULL,
  `MENU_SORT` bigint(20) DEFAULT NULL,
  `CATEGORY_SORT` bigint(20) DEFAULT NULL,
  `PAGE_SORT` bigint(20) DEFAULT NULL,
  `INDEX_SORT` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `MODEL_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `tmp_pm_composition`
--

INSERT INTO `tmp_pm_composition` (`ID`, `CLT_MODULE_ID`, `GROUP_ID`, `CATEGORY_ID`, `MENU_ID`, `PAGE_ID`, `INDEX_SHOW`, `GROUP_SORT`, `MENU_SORT`, `CATEGORY_SORT`, `PAGE_SORT`, `INDEX_SORT`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `MODEL_ID`) VALUES
(1, 1, 3, 1, 1, 5, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(2, 1, 3, 1, 2, 12, '', 1, 3, 1, 2, 0, 'Y', NULL, NULL, NULL, NULL, 1),
(4, 1, 3, 1, 4, 4, '', 1, 4, 1, 6, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(5, 1, 3, 2, 5, 35, NULL, 1, 2, 3, 7, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(6, 1, 3, 2, 6, 36, NULL, 1, 3, 3, 8, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(7, 1, 3, 2, 7, 37, NULL, 1, 4, 3, 9, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(8, 1, 3, 1, 8, 7, NULL, 1, 5, 1, 10, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(9, 1, 3, 3, 9, 38, NULL, 1, 15, 4, 11, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(10, 1, 3, 3, 10, 39, NULL, 1, 16, 4, 12, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(11, 1, 3, 3, 11, 40, NULL, 1, 17, 4, 13, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(15, 1, 3, 5, 15, 14, NULL, 1, 21, 6, 15, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(16, 1, 2, 6, 16, 4, NULL, 1, 22, 22, 16, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(17, 1, 2, 6, 17, 25, NULL, 1, 23, 23, 17, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(18, 1, 2, 6, 18, 7, NULL, 1, 24, 24, 18, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(19, 1, 2, 6, 19, 13, NULL, 1, 25, 25, 19, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(20, 1, 2, 6, 20, 16, NULL, 1, 29, 29, 24, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(72, 5, 3, 11, 26, 28, NULL, 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 3),
(188, 3, 3, 7, 21, 1, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 2),
(190, 3, 3, 8, 22, 1, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 2),
(193, 3, 3, 9, 23, 31, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 2),
(196, 3, 3, 9, 24, 27, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 2),
(197, 5, 3, 12, 27, 30, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 3),
(198, 3, 3, 10, 25, 26, '', 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 2),
(199, 5, 3, 13, 28, 29, NULL, 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 3),
(200, 5, 3, 14, 29, 25, NULL, 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 3),
(201, 10, 3, 15, 30, 21, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 4),
(202, 10, 3, 15, 31, 22, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 4),
(203, 10, 3, 15, 32, 23, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 4),
(204, 10, 3, 15, 33, 24, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 4),
(206, 3, 3, 10, 35, 19, NULL, 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 2),
(207, 1, 1, 1, 36, 20, NULL, 1, 30, 30, 25, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(208, 3, 1, 1, 36, 20, NULL, 1, 23, 23, 23, NULL, 'Y', NULL, NULL, NULL, NULL, 2),
(209, 5, 1, 1, 36, 20, NULL, 1, 24, 24, 24, NULL, 'Y', NULL, NULL, NULL, NULL, 3),
(210, 10, 1, 1, 36, 20, NULL, 1, 25, 25, 25, NULL, 'Y', NULL, NULL, NULL, NULL, 4),
(211, 1, 3, 16, 38, 32, NULL, 1, 3, 2, 3, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(212, 1, 3, 16, 39, 33, NULL, 1, 4, 2, 4, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(213, 1, 3, 16, 40, 34, NULL, 1, 5, 2, 5, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(214, 1, 3, 17, 41, 41, NULL, 1, 26, 5, 20, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(215, 1, 3, 17, 42, 42, NULL, 1, 27, 5, 22, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(216, 1, 3, 17, 43, 43, NULL, 1, 28, 5, 23, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(217, 1, 3, 1, 44, 8, NULL, 1, 2, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(218, 1, 3, 2, 45, 9, NULL, 1, 1, 1, 1, NULL, 'Y', NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `tmp_pm_model_attribute`
--

CREATE TABLE `tmp_pm_model_attribute` (
`ID` bigint(20) NOT NULL,
  `PM_PAGE_ATTRIBUTE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `IS_REQUIRED` char(1) DEFAULT 'N',
  `IS_READONLY` char(1) DEFAULT 'N',
  `DATA_TYPE_ID` bigint(20) DEFAULT NULL,
  `FORMAT_TYPE_ID` bigint(20) DEFAULT NULL,
  `MAX_LENGHT` bigint(20) DEFAULT NULL,
  `MAX_WORD` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `DATE_UPDATE` varchar(45) DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `MODEL_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tmp_pm_model_pdf_attribute`
--

CREATE TABLE `tmp_pm_model_pdf_attribute` (
`ID` bigint(20) NOT NULL,
  `PM_PDF_ATTRIBUTE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATA_TYPE_ID` bigint(20) DEFAULT NULL,
  `FORMAT_TYPE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `DATE_UPDATE` varchar(45) DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `MODEL_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tmp_pm_page`
--

CREATE TABLE `tmp_pm_page` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `NAME` tinytext,
  `DESCRIPTION` text,
  `LABEL` tinytext CHARACTER SET latin1,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `PAGE_TYPE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `PACKAGE_JAVA` tinytext,
  `FOLDER_JSF` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;

--
-- Contenu de la table `tmp_pm_page`
--

INSERT INTO `tmp_pm_page` (`ID`, `NAME`, `DESCRIPTION`, `LABEL`, `SORT_KEY`, `PAGE_TYPE_ID`, `ACTIVE`, `USER_CREATION`, `DATE_CREATION`, `USER_UPDATE`, `DATE_UPDATE`, `PACKAGE_JAVA`, `FOLDER_JSF`) VALUES
(1, 'page Test', 'page test1', 'page1', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.page1', 'stock/page1'),
(2, 'Page add admin ', 'Page add admin', 'addAdmin', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.admin.addAdmin', 'admin/addAdmin'),
(3, 'Page Expense', 'Page pour la gestion des dépense', 'expense', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.expense', 'stock/expense'),
(4, 'Page Supplier', 'Page pour la gestion des fournisseurs', 'supplier', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.supplier', 'stock/supplier'),
(5, 'Page produit', 'Page pour la gestion des produits', 'product', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.product', 'stock/product'),
(6, 'Page de réception', 'Page pour la gestion des réceptions et BL', 'reception', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.reception', 'stock/reception'),
(7, 'Page de custmer', 'customer', 'customer', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.customer', 'stock/customer'),
(8, 'Page de validation des réceptions', 'Page de validation des réceptions', 'receptionValidation', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.receptionValidation', 'stock/receptionValidation'),
(9, 'page de historique de reception', 'page de historique de reception', 'receptionHistory', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.receptionHistory', 'stock/receptionHistory'),
(10, 'Page de promotion des prodtuis', 'promo produit', 'encours', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.encours', 'stock/encours'),
(11, 'Inventaire', NULL, 'inventaire', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.inventaire', 'stock/inventaire'),
(12, 'statut de stock : capital', NULL, 'stockState', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.stockState', 'stock/stockState'),
(13, 'Bon de livraison Client', NULL, 'bonLivraison', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.bonLivraison', 'stock/bonLivraison'),
(14, 'Valdation de Bon de livraison Client', NULL, 'blValidation', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.blValidation', 'stock/blValidation'),
(15, 'Historique de Bon de livraison Client', NULL, 'blHistory', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.blHistory', 'stock/blHistory'),
(16, 'les rapports cotidiennet', NULL, 'rapDaily', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.rapDaily', 'stock/rapDaily'),
(17, 'Changé un produit', NULL, 'encours', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.encours', 'stock/encours'),
(18, 'avance de produit', NULL, 'encours', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.encours', 'stock/encours'),
(19, 'la liste des valeurs', NULL, 'lovs', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.admin.lovs', 'admin/lovs'),
(20, 'Mon profil', NULL, 'profile', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.admin.profile', 'admin/profile'),
(21, 'Les paramètres basiques', NULL, 'basicParameter', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.webmaster.basicParameter', 'webMaster/basicParameter'),
(22, 'Paramètres', NULL, 'parameter', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.webmaster.parameter', 'webMaster/parameter'),
(23, 'Parameter du module', NULL, 'moduleParameter', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.webmaster.moduleParameter', 'webMaster/moduleParameter'),
(24, 'Parameter du page', NULL, 'pageParameter', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.webmaster.pageParameter', 'webMaster/pageParameter'),
(25, 'Parameter du client', NULL, 'parameterClient', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.webmaster.parameterClient', 'webMaster/parameterClient'),
(26, 'Parameter du module/client', NULL, 'moduleParameterClient', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.webmaster.moduleParameterClient', 'webMaster/moduleParameterClient'),
(27, 'Parameter du page/module', NULL, 'pageParameterModule', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.webmaster.pageParameterModule', 'webMaster/pageParameterModule');

-- --------------------------------------------------------

--
-- Structure de la table `tmp_pm_page_attribute`
--

CREATE TABLE `tmp_pm_page_attribute` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `PAGE_ID` bigint(20) DEFAULT NULL,
  `INF_ITEM_CODE` varchar(255) DEFAULT NULL,
  `PM_COMPONENT_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `IS_REQUIRED` char(1) DEFAULT 'N',
  `IS_READONLY` char(1) DEFAULT 'N',
  `IS_HIDDEN` char(1) DEFAULT 'N',
  `DATA_TYPE_ID` bigint(20) DEFAULT NULL,
  `FORMAT_TYPE_ID` bigint(20) DEFAULT NULL,
  `MAX_LENGHT` bigint(20) DEFAULT NULL,
  `MAX_WORD` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `DATE_UPDATE` varchar(45) DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;

--
-- Contenu de la table `tmp_pm_page_attribute`
--

INSERT INTO `tmp_pm_page_attribute` (`ID`, `PAGE_ID`, `INF_ITEM_CODE`, `PM_COMPONENT_ID`, `SORT_KEY`, `ACTIVE`, `IS_REQUIRED`, `IS_READONLY`, `IS_HIDDEN`, `DATA_TYPE_ID`, `FORMAT_TYPE_ID`, `MAX_LENGHT`, `MAX_WORD`, `DATE_CREATION`, `DATE_UPDATE`, `USER_CREATION`, `USER_UPDATE`) VALUES
(1, 3, 'p003.s2.name', 1, 1, 'Y', 'Y', 'N', 'N', NULL, 2, NULL, 0, NULL, NULL, NULL, NULL),
(2, 3, 'p003.s1.name', 5, 2, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 3, 'p003.s1.description', 5, 3, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 3, 'p003.s1.type', 5, 1, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 3, 'p003.s1.amount', 5, 4, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 3, 'p003.s1', 4, 1, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 3, 'p003.s1.edit', 5, 6, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 3, 'p003.s1.delete', 5, 7, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 3, 'p003.s2.type', 3, 1, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL),
(11, 3, 'p003.s2.description', 2, 3, 'Y', 'Y', 'Y', 'N', NULL, NULL, 100, 30, NULL, NULL, NULL, NULL),
(12, 3, 'p003.s2.amount', 1, 4, 'Y', 'Y', 'N', 'N', 3, 2, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 6, 'reception.s1', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 6, 'reception.s2.name', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 6, 'reception.s2.description', 1, 3, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 6, 'reception.s2.supplier', 1, 4, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 6, 'reception.s2.deadline', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 6, 'reception.s2.souche', 1, 6, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 6, 'reception.s2.amount', 1, 7, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 6, 'reception.s2.deposit', 1, 8, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 6, 'reception.s1.id', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 6, 'reception.s1.name', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 6, 'reception.s1.souche', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 6, 'reception.s1.supplierId', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 6, 'reception.s1.amount', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 6, 'reception.s1.deadline', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 6, 'reception.s1.option', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 6, 'reception.s1.edit', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 6, 'reception.s1.delete', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 4, 'supplier.s1', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 4, 'supplier.s1.country', 1, 3, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 4, 'supplier.s1.city', 1, 4, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 4, 'supplier.s1.companyName', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 4, 'supplier.s1.cellPhone', 1, 6, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 4, 'supplier.s1.fixedPhone', 1, 7, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, 4, 'supplier.s1.adress', 1, 8, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 4, 'supplier.s1.email', 1, 9, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 4, 'supplier.s1.type', 1, 10, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, 4, 'supplier.s1.edit', 1, 11, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 4, 'supplier.s1.delete', 1, 12, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 4, 'supplier.s1.add', 1, 13, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 4, 'supplier.s2.firstName', 1, 14, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 4, 'supplier.s2.lastName', 1, 15, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 4, 'supplier.s2.type', 1, 16, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 4, 'supplier.s2.country', 1, 17, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 4, 'supplier.s2.city', 1, 18, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 4, 'supplier.s2.companyName', 1, 19, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, 4, 'supplier.s2.cellPhone', 1, 20, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 4, 'supplier.s2.fixedPhone', 1, 21, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 4, 'supplier.s2.adress', 1, 22, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 4, 'supplier.s2.email', 1, 23, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 4, 'product.s2.ative', 1, 24, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 4, 'supplier.s2.save', 1, 25, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 4, 'supplier.s1.option', 1, 25, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 5, 'p005.s1', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 5, 'p005.s1.designation', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 5, 'p005.s1.quantity', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 5, 'p005.s1.reference', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 5, 'p005.s1.pricesale', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 5, 'p005.s1.size', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 5, 'p005.s1.status', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 5, 'p005.s1.family', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 5, 'p005.s1.color', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 5, 'p005.s1.option', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 5, 'p005.s1.edit', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 5, 'p005.s1.add', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 5, 'p005.s1.delete', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(73, 5, 'p005.s2.designation', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, 5, 'p005.s2.quantity', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(75, 5, 'p005.s2.reference', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(76, 5, 'p005.s2.priceSale', 1, 0, 'Y', 'N', 'N', 'N', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(77, 5, 'p005.s2.active', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(78, 5, 'p005.s2.status', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(79, 5, 'p005.s2.size', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(80, 5, 'p005.s2.family', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(81, 5, 'p005.s2.color', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(82, 5, 'p005.s2.save', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(83, 5, 'p005.s1.priceBuy', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(84, 5, 'p005.s2.priceBuy', 1, 0, 'Y', 'N', 'N', 'N', 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(85, 4, 'supplier.s1.postCode', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(86, 4, 'supplier.s2.postCode', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(87, 13, 'bonLivraison.s1.description', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(88, 13, 'bonLivraison.s1.customer', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(89, 6, 'reception.s2.sizes', 1, 0, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(90, 21, 'basicParameter.s1', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(91, 21, 'basicParameter.s2.name', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(92, 21, 'basicParameter.s2.description', 1, 3, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(93, 21, 'basicParameter.s2.value', 1, 4, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(94, 21, 'basicParameter.s2.basicParameterType', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(95, 21, 'basicParameter.s1', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(96, 21, 'basicParameter.s2.name', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(97, 21, 'basicParameter.s2.description', 1, 3, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(98, 21, 'basicParameter.s2.value', 1, 4, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(99, 21, 'basicParameter.s2.basicParameterType', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(100, 23, 'moduleParameter.s1', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(101, 23, 'moduleParameter.s2.name', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(102, 23, 'moduleParameter.s2.description', 1, 3, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(103, 23, 'moduleParameter.s2.value', 1, 4, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(104, 23, 'moduleParameter.s2.moduleParameterType', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(105, 25, 'parameterClient.s1', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(106, 25, 'parameterClient.s2', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(107, 25, 'parameterClient.s3.client', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(108, 25, 'parameterClient.s3.parameter', 1, 3, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(109, 25, 'parameterClient.s3.value', 1, 4, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(110, 22, 'parameter.s1', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(111, 22, 'parameter.s2.name', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(112, 22, 'parameter.s2.description', 1, 3, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(113, 22, 'parameter.s2.value', 1, 4, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(114, 22, 'parameter.s2.parameterType', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(115, 27, 'pageParameterModule.s1', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(116, 27, 'pageParameterModule.s2', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(117, 27, 'pageParameterModule.s3', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(118, 27, 'pageParameterModule.s4', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(119, 27, 'pageParameterModule.s5.module', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(120, 27, 'pageParameterModule.s5.pageParameter', 1, 3, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(121, 27, 'pageParameterModule.s5.value', 1, 4, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(122, 24, 'pageParameter.s1', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(123, 24, 'pageParameter.s2.name', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(124, 24, 'pageParameter.s2.description', 1, 3, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(125, 24, 'pageParameter.s2.value', 1, 4, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(126, 24, 'pageParameter.s2.pageParameterType', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(127, 24, 'pageParameter.s2.page', 1, 5, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(128, 26, 'moduleParameterClient.s1', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(129, 26, 'moduleParameterClient.s2', 1, 1, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(130, 26, 'moduleParameterClient.s3', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(131, 26, 'moduleParameterClient.s4.module', 1, 2, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(132, 26, 'moduleParameterClient.s4.parameterModule', 1, 3, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(133, 26, 'moduleParameterClient.s4.value', 1, 4, 'Y', 'N', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(134, 6, 'p006.s2.code', 1, 1, 'Y', 'N', 'Y', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(135, 6, 'p006.s2.deadline', 1, 1, 'Y', 'N', 'N', 'N', 5, 8, NULL, NULL, NULL, NULL, NULL, NULL),
(136, 5, 'p005.s2.reference', NULL, NULL, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(137, 5, 'p005.s2.designation', NULL, NULL, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(138, 5, 'p005.s2.priceBuy', NULL, NULL, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(139, 5, 'p005.s2.family', NULL, NULL, 'Y', 'Y', 'N', 'N', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(140, 4, 'p004.s2.firstName', 1, 1, 'Y', 'Y', 'N', 'N', 1, 2, 100, 200, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tmp_pm_pdf`
--

CREATE TABLE `tmp_pm_pdf` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `PDF_TYPE_ID` bigint(20) DEFAULT NULL,
  `IN_DEV` char(1) DEFAULT 'N',
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `tmp_pm_pdf`
--

INSERT INTO `tmp_pm_pdf` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `PDF_TYPE_ID`, `IN_DEV`, `ACTIVE`, `USER_CREATION`, `DATE_CREATION`, `USER_UPDATE`, `DATE_UPDATE`) VALUES
(1, 'Afficher la liste des commandes ', NULL, 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(2, 'Afficher le détails d''une commande', NULL, 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(3, 'Afficher la liste des réceptions', '1', NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(4, 'Afficher le détails d''une récéption', '1', 1, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(5, 'Afficher la liste des ventes ', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(6, 'Afficher le détails d''une vente', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(7, 'Afficher la liste des avoirs', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL),
(8, 'Afficher le détails d''une avoir', NULL, NULL, 1, 'N', 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tmp_pm_pdf_attribute`
--

CREATE TABLE `tmp_pm_pdf_attribute` (
`ID` bigint(20) NOT NULL,
  `PDF_ID` bigint(20) DEFAULT NULL,
  `INF_ITEM_CODE` varchar(255) DEFAULT NULL,
  `PM_COMPONENT_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `IS_HIDDEN` char(1) DEFAULT 'N',
  `DATA_TYPE_ID` bigint(20) DEFAULT NULL,
  `FORMAT_TYPE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `DATE_UPDATE` varchar(45) DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tmp_pm_pdf_attribute_exclud`
--

CREATE TABLE `tmp_pm_pdf_attribute_exclud` (
`ID` bigint(20) NOT NULL,
  `PDF_ID` bigint(20) DEFAULT NULL,
  `INF_ITEM_CODE` tinytext,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `MODEL_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tmp_pm_pdf_parameter`
--

CREATE TABLE `tmp_pm_pdf_parameter` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext CHARACTER SET latin1,
  `DESCRIPTION` text CHARACTER SET latin1,
  `DEFAULT_VALUE` text,
  `PDF_ID` bigint(20) DEFAULT NULL,
  `PDF_PARAMETER_TYPE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tmp_pm_pdf_parameter_module`
--

CREATE TABLE `tmp_pm_pdf_parameter_module` (
`ID` bigint(20) NOT NULL,
  `PDF_PARAMETER_ID` bigint(20) DEFAULT NULL,
  `VALUE` longtext,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `MODEL_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tmp_pm_pdf_parameter_type`
--

CREATE TABLE `tmp_pm_pdf_parameter_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `tmp_pm_pdf_type`
--

CREATE TABLE `tmp_pm_pdf_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `tmp_pm_pdf_type`
--

INSERT INTO `tmp_pm_pdf_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'type simple', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tmp_pm_validation_type`
--

CREATE TABLE `tmp_pm_validation_type` (
  `ID` bigint(20) NOT NULL DEFAULT '0',
  `NAME` tinytext,
  `DESCRIPTION` text,
  `PARAM_NUMBER` bigint(20) DEFAULT NULL,
  `ERROR_MESSAGE` tinytext,
  `HELP` text CHARACTER SET latin1,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

;

--
-- Contenu de la table `tmp_pm_validation_type`
--

INSERT INTO `tmp_pm_validation_type` (`ID`, `NAME`, `DESCRIPTION`, `PARAM_NUMBER`, `ERROR_MESSAGE`, `HELP`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'integer', 'integer', 0, 'validation.v1.integer', 'validation.v1.integer', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'double', 'double', 0, 'validation.v1.double', 'validation.v1.double', 2, 'Y', NULL, NULL, NULL, NULL),
(3, 'char', 'char', 0, 'validation.v1.char', 'validation.v1.char', 3, 'Y', NULL, NULL, NULL, NULL),
(4, 'date', 'date', 0, 'validation.v1.date', 'validation.v1.date', 4, 'Y', NULL, NULL, NULL, NULL),
(5, 'time', 'time', 0, 'validation.v1.time', 'validation.v1.time', 5, 'Y', NULL, NULL, NULL, NULL),
(6, 'datetime', 'datetime', 0, 'validation.v1.datetime', 'validation.v1.datetime', 6, 'Y', NULL, NULL, NULL, NULL),
(7, 'email', 'email', 0, 'validation.v1.email', 'validation.v1.email', 7, 'Y', NULL, NULL, NULL, NULL),
(8, 'phone', 'phone', 0, 'validation.v1.phone', 'validation.v1.phone', 8, 'Y', NULL, NULL, NULL, NULL),
(9, 'maxlenght', 'maxlenght', 0, 'validation.v1.maxlenght', 'validation.v1.maxlenght', 9, 'Y', NULL, NULL, NULL, NULL),
(10, 'required', 'required', 0, 'validation.v1.required', 'validation.v1.required', 10, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_clt_client`
--
CREATE TABLE `v_clt_client` (
`ID` bigint(20)
,`CODE` varchar(255)
,`FIRST_NAME` tinytext
,`LAST_NAME` tinytext
,`COMPANY_NAME` tinytext
,`CLIENT_STATUS_ID` bigint(20)
,`DEFAULT_LANGUAGE_ID` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`ENTITY_ID` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_clt_client_details`
--
CREATE TABLE `v_clt_client_details` (
`ID` bigint(20)
,`CODE` varchar(255)
,`FIRST_NAME` tinytext
,`LAST_NAME` tinytext
,`COMPANY_NAME` tinytext
,`CLIENT_STATUS_ID` bigint(20)
,`DEFAULT_LANGUAGE_ID` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`ENTITY_ID` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_clt_client_language`
--
CREATE TABLE `v_clt_client_language` (
`code` varchar(41)
,`client_id` bigint(20)
,`inf_language_id` bigint(20)
,`inf_language_code` tinytext
,`inf_prefix_id` bigint(20)
,`inf_prefix_code` tinytext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_clt_module`
--
CREATE TABLE `v_clt_module` (
`ID` bigint(20)
,`module_name` tinytext
,`module_description` text
,`module_type_name` tinytext
,`client_id` bigint(20)
,`active` char(1)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_clt_module_details`
--
CREATE TABLE `v_clt_module_details` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` text
,`CLIENT_ID` bigint(20)
,`MODULE_STATUS_ID` bigint(20)
,`MODULE_TYPE_ID` bigint(20)
,`PARENT_MODULE_ID` bigint(20)
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_clt_module_parameter`
--
CREATE TABLE `v_clt_module_parameter` (
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_clt_module_parameter_client`
--
CREATE TABLE `v_clt_module_parameter_client` (
`CODE` varchar(41)
,`MODULE_ID` bigint(20)
,`PARAMETER_ID` bigint(20)
,`PARAMETER_TYPE_NAME` tinytext
,`ACTIVE` varchar(1)
,`PARAMETER_NAME` tinytext
,`VALUE` longtext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_clt_module_parameter_type`
--
CREATE TABLE `v_clt_module_parameter_type` (
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_clt_module_type`
--
CREATE TABLE `v_clt_module_type` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` tinytext
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_clt_parameter`
--
CREATE TABLE `v_clt_parameter` (
`id` bigint(20)
,`name` tinytext
,`description` tinytext
,`DEFAULT_VALUE` text
,`PARAMETER_TYPE_ID` bigint(20)
,`clt_parameter_type_name` tinytext
,`active` char(1)
,`can_be_delete` varchar(5)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_clt_parameters`
--
CREATE TABLE `v_clt_parameters` (
`code` varchar(41)
,`id` bigint(20)
,`parameter_type_id` bigint(20)
,`clinet_id` bigint(20)
,`val` longtext
,`value` longtext
,`default_value` text
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_clt_parameter_client`
--
CREATE TABLE `v_clt_parameter_client` (
`CODE` varchar(41)
,`CLIENT_ID` bigint(20)
,`PARAMETER_ID` bigint(20)
,`PARAMETER_TYPE_NAME` tinytext
,`ACTIVE` varchar(1)
,`PARAMETER_NAME` tinytext
,`VALUE` longtext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_clt_parameter_type`
--
CREATE TABLE `v_clt_parameter_type` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` text
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_clt_user`
--
CREATE TABLE `v_clt_user` (
`ID` bigint(20)
,`FIRST_NAME` tinytext
,`LAST_NAME` tinytext
,`USERNAME` tinytext
,`PASSWORD` tinytext
,`CATEGORY_ID` bigint(20)
,`CLIENT_ID` bigint(20)
,`USER_STATUS_ID` bigint(20)
,`DEFAULT_LANGUAGE_ID` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`IMAGE_PATH` tinytext
,`ENTITY_ID` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_clt_user_category`
--
CREATE TABLE `v_clt_user_category` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` text
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_clt_user_client`
--
CREATE TABLE `v_clt_user_client` (
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_clt_user_details`
--
CREATE TABLE `v_clt_user_details` (
`ID` bigint(20)
,`FIRST_NAME` tinytext
,`LAST_NAME` tinytext
,`USERNAME` tinytext
,`PASSWORD` tinytext
,`CATEGORY_ID` bigint(20)
,`CLIENT_ID` bigint(20)
,`USER_STATUS_ID` bigint(20)
,`DEFAULT_LANGUAGE_ID` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`IMAGE_PATH` tinytext
,`ENTITY_ID` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_clt_user_module`
--
CREATE TABLE `v_clt_user_module` (
`ID` bigint(20)
,`USER_ID` bigint(20)
,`MODULE_ID` bigint(20)
,`FULL CONTROL` char(1)
,`MODULE_ID_TO_MANAGER` bigint(20)
,`ACTIVE` char(1)
,`SORT_KEY` bigint(20)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`MODULE_TYPE_ID` bigint(20)
,`MODULE_NAME` tinytext
,`MODULE_IMAGE_PATH` tinytext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_compos_cat`
--
CREATE TABLE `v_compos_cat` (
`model_id` bigint(20)
,`group_id` bigint(20)
,`category_sort` bigint(20)
,`category_id` bigint(20)
,`active` char(1)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_cta_email_type`
--
CREATE TABLE `v_cta_email_type` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` text
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`CLT_MODULE_ID` bigint(20)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_cta_entity_contact`
--
CREATE TABLE `v_cta_entity_contact` (
`ID` bigint(20)
,`ENTITY_PHONE` varchar(510)
,`ENTITY_EMAIL` varchar(255)
,`ENTITY_FAX` varchar(510)
,`ENTITY_LOCATION_ADDRESS` text
,`ENTITY_LOCATION_CITY_NAME` varchar(255)
,`ENTITY_LOCATION_COUNTRY_NAME` varchar(255)
,`ENTITY_WEB` varchar(255)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_cta_entity_email`
--
CREATE TABLE `v_cta_entity_email` (
`ID` bigint(20)
,`EMAIL_ADDRESS` tinytext
,`IS_FOR_NOTIFICATION` char(1)
,`ENTITY_ID` bigint(20)
,`PRIORITY` bigint(20)
,`EMAIL_TYPE_ID` bigint(20)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`EMAIL_TYPE_NAME` tinytext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_cta_entity_fax`
--
CREATE TABLE `v_cta_entity_fax` (
`ID` bigint(20)
,`COUNTRY_CODE` tinytext
,`NUMBER` tinytext
,`PRIORITY` bigint(20)
,`ENTITY_ID` bigint(20)
,`FAX_TYPE_ID` bigint(20)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`FAX_TYPE_NAME` tinytext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_cta_entity_location`
--
CREATE TABLE `v_cta_entity_location` (
`ID` bigint(20)
,`ADDRESS_LINE_1` tinytext
,`ADDRESS_LINE_2` tinytext
,`ADDRESS_LINE_3` tinytext
,`INF_COUNTRY_ID` bigint(20)
,`INF_CITY_ID` bigint(20)
,`POSTAL_CODE` tinytext
,`PRIORITY` bigint(20)
,`ENTITY_ID` bigint(20)
,`LOCATION_TYPE_ID` bigint(20)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`LOCATION_TYPE_NAME` tinytext
,`INF_COUNTRY_NAME` tinytext
,`INF_CITY_NAME` tinytext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_cta_entity_phone`
--
CREATE TABLE `v_cta_entity_phone` (
`ID` bigint(20)
,`COUNTRY_CODE` tinytext
,`NUMBER` tinytext
,`ENTITY_ID` bigint(20)
,`PHONE_TYPE_ID` bigint(20)
,`PRIORITY` bigint(20)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`PHONE_TYPE_NAME` tinytext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_cta_entity_web`
--
CREATE TABLE `v_cta_entity_web` (
`ID` bigint(20)
,`URL` tinytext
,`PRIORITY` bigint(20)
,`ENTITY_ID` bigint(20)
,`WEB_TYPE_ID` bigint(20)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`WEB_TYPE_NAME` tinytext
,`EXTERNAL_URL` tinytext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_cta_fax_type`
--
CREATE TABLE `v_cta_fax_type` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` text
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`CLT_MODULE_ID` bigint(20)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_cta_location_type`
--
CREATE TABLE `v_cta_location_type` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` text
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`CLT_MODULE_ID` bigint(20)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_cta_phone_type`
--
CREATE TABLE `v_cta_phone_type` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` text
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`CLT_MODULE_ID` bigint(20)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_cta_web_type`
--
CREATE TABLE `v_cta_web_type` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` text
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`CLT_MODULE_ID` bigint(20)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_inf_basic_parameter`
--
CREATE TABLE `v_inf_basic_parameter` (
`id` bigint(20)
,`name` tinytext
,`description` text
,`value` text
,`basic_parameter_type_id` bigint(20)
,`basic_parameter_type_name` tinytext
,`active` char(1)
,`CAN_BE_DELETE` int(1)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_inf_basic_parameter_type`
--
CREATE TABLE `v_inf_basic_parameter_type` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` text
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`CAN_BE_DELETE` binary(0)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_inf_city`
--
CREATE TABLE `v_inf_city` (
`ID` bigint(20)
,`CODE` tinytext
,`NAME` tinytext
,`DESCRIPTION` tinytext
,`SORT_KEY` bigint(20)
,`COUNTRY_ID` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_inf_country`
--
CREATE TABLE `v_inf_country` (
`ID` bigint(20)
,`CODE` tinytext
,`NAME` tinytext
,`DESCRIPTION` tinytext
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_inf_lovs`
--
CREATE TABLE `v_inf_lovs` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` text
,`TABLE_PREFIX` tinytext
,`TABLE` tinytext
,`VIEW` tinytext
,`ITEM_CODE` tinytext
,`SORT_KEY` bigint(20)
,`ACTIVE` char(20)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`CLT_MODULE_ID` bigint(20)
,`ENTITY` tinytext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_inf_meta_model`
--
CREATE TABLE `v_inf_meta_model` (
`CODE` text
,`PREFIX_ID` bigint(20)
,`PREFIX_CODE` tinytext
,`ITEM_CODE` varchar(255)
,`VALUE` longtext
,`TEXT_TYPE_ID` bigint(20)
,`TEXT_TYPE_CODE` tinytext
,`LANGUAGE_ID` bigint(20)
,`LANGUAGE_CODE` tinytext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_inf_privileges`
--
CREATE TABLE `v_inf_privileges` (
`CODE` varchar(297)
,`ITEM_CODE` varchar(255)
,`ROLE_ID` bigint(20)
,`ROLE_NAME` tinytext
,`GROUP_ID` bigint(20)
,`GROUP_NAME` tinytext
,`USER_ID` bigint(20)
,`MODULE_ID` bigint(20)
,`CLIENT_ID` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_attribute_date_type`
--
CREATE TABLE `v_pm_attribute_date_type` (
`code` varchar(259)
,`inf_item_code` varchar(255)
,`page_id` bigint(20)
,`clt_module_id` int(1)
,`validation_id` bigint(20)
,`params` tinytext
,`param_number` bigint(20)
,`help` tinytext
,`custom_error` binary(0)
,`error_message` tinytext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_attribute_exclud`
--
CREATE TABLE `v_pm_attribute_exclud` (
`code` varchar(257)
,`inf_item_code` varchar(255)
,`page_id` bigint(20)
,`clt_module_id` int(1)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_attribute_format_type`
--
CREATE TABLE `v_pm_attribute_format_type` (
`code` varchar(259)
,`inf_item_code` varchar(255)
,`page_id` bigint(20)
,`clt_module_id` int(1)
,`validation_id` bigint(20)
,`params` tinytext
,`param_number` bigint(20)
,`help` tinytext
,`custom_error` binary(0)
,`error_message` tinytext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_attribute_hidden`
--
CREATE TABLE `v_pm_attribute_hidden` (
`code` varchar(257)
,`inf_item_code` varchar(255)
,`page_id` bigint(20)
,`clt_module_id` int(1)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_attribute_max_length`
--
CREATE TABLE `v_pm_attribute_max_length` (
`code` varchar(257)
,`inf_item_code` varchar(255)
,`page_id` bigint(20)
,`clt_module_id` int(1)
,`validation_id` bigint(20)
,`params` bigint(20)
,`param_number` bigint(20)
,`help` text
,`custom_error` binary(0)
,`error_message` tinytext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_attribute_max_word`
--
CREATE TABLE `v_pm_attribute_max_word` (
`code` varchar(257)
,`inf_item_code` varchar(255)
,`page_id` bigint(20)
,`clt_module_id` int(1)
,`validation_id` bigint(20)
,`params` bigint(20)
,`param_number` bigint(20)
,`help` text
,`custom_error` binary(0)
,`error_message` tinytext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_attribute_readonly`
--
CREATE TABLE `v_pm_attribute_readonly` (
`code` varchar(257)
,`inf_item_code` varchar(255)
,`page_id` bigint(20)
,`clt_module_id` int(1)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_attribute_required`
--
CREATE TABLE `v_pm_attribute_required` (
`code` varchar(257)
,`inf_item_code` varchar(255)
,`page_id` bigint(20)
,`clt_module_id` int(1)
,`validation_id` bigint(20)
,`params` binary(0)
,`param_number` bigint(20)
,`help` text
,`custom_error` binary(0)
,`error_message` tinytext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_attribute_validation`
--
CREATE TABLE `v_pm_attribute_validation` (
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_categorys_dto`
--
CREATE TABLE `v_pm_categorys_dto` (
`code` varchar(62)
,`id` bigint(20)
,`name` tinytext
,`image_path` tinytext
,`description` text
,`category_type_id` bigint(20)
,`category_sort` bigint(20)
,`model_id` bigint(20)
,`group_id` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_composition`
--
CREATE TABLE `v_pm_composition` (
`id` bigint(20)
,`model_id` bigint(20)
,`group_id` bigint(20)
,`category_id` bigint(20)
,`menu_id` bigint(20)
,`page_id` bigint(20)
,`index_show` char(1)
,`index_sort` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_compos_details`
--
CREATE TABLE `v_pm_compos_details` (
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_groups_dto`
--
CREATE TABLE `v_pm_groups_dto` (
`code` varchar(41)
,`id` bigint(20)
,`name` tinytext
,`description` text
,`group_type_id` bigint(20)
,`sort_key` bigint(20)
,`model_id` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_menus_dto`
--
CREATE TABLE `v_pm_menus_dto` (
`code` varchar(83)
,`id` bigint(20)
,`name` tinytext
,`image_path` tinytext
,`description` text
,`menu_type_id` bigint(20)
,`menu_sort` bigint(20)
,`model_id` bigint(20)
,`group_id` bigint(20)
,`category_id` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_model_dto`
--
CREATE TABLE `v_pm_model_dto` (
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_page`
--
CREATE TABLE `v_pm_page` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` text
,`SORT_KEY` bigint(20)
,`PAGE_TYPE_ID` bigint(20)
,`IN_DEV` char(1)
,`ACTIVE` char(1)
,`USER_CREATION` bigint(20)
,`DATE_CREATION` datetime
,`USER_UPDATE` bigint(20)
,`DATE_UPDATE` datetime
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_pages_dto`
--
CREATE TABLE `v_pm_pages_dto` (
`code` varchar(104)
,`id` bigint(20)
,`name` tinytext
,`description` text
,`in_dev` char(1)
,`page_type_id` bigint(20)
,`page_sort` bigint(20)
,`model_id` bigint(20)
,`group_id` bigint(20)
,`category_id` bigint(20)
,`menu_id` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_page_attribute`
--
CREATE TABLE `v_pm_page_attribute` (
`ID` bigint(20)
,`PAGE_ID` bigint(20)
,`INF_ITEM_CODE` varchar(255)
,`PM_COMPONENT_ID` bigint(20)
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`IS_REQUIRED` char(1)
,`IS_READONLY` char(1)
,`IS_HIDDEN` char(1)
,`DATA_TYPE_ID` bigint(20)
,`FORMAT_TYPE_ID` bigint(20)
,`MAX_LENGHT` bigint(20)
,`MAX_WORD` bigint(20)
,`DATE_CREATION` datetime
,`DATE_UPDATE` varchar(45)
,`USER_CREATION` bigint(20)
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_page_attribute_module`
--
CREATE TABLE `v_pm_page_attribute_module` (
`CODE` varchar(41)
,`CLT_MODULE_ID` bigint(20)
,`PAGE_ATTRIBUTE_ID` bigint(20)
,`ACTIVE` varchar(1)
,`PAGE_ID` bigint(20)
,`INF_ITEM_CODE` varchar(255)
,`PM_COMPONENT_ID` bigint(20)
,`IS_REQUIRED` varchar(1)
,`IS_READONLY` varchar(1)
,`IS_HIDDEN` varchar(1)
,`DATA_TYPE_ID` bigint(20)
,`FORMAT_TYPE_ID` bigint(20)
,`MAX_LENGHT` bigint(20)
,`MAX_WORD` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_page_module`
--
CREATE TABLE `v_pm_page_module` (
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_page_parameter`
--
CREATE TABLE `v_pm_page_parameter` (
`id` bigint(20)
,`name` tinytext
,`description` text
,`DEFAULT_VALUE` text
,`PAGE_PARAMETER_TYPE_ID` bigint(20)
,`pm_page_parameter_type_name` tinytext
,`PAGE_ID` bigint(20)
,`pm_page_name` tinytext
,`active` char(1)
,`can_be_delete` varchar(5)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_page_parameter_module`
--
CREATE TABLE `v_pm_page_parameter_module` (
`CODE` varchar(41)
,`CLT_MODULE_ID` bigint(20)
,`PAGE_PARAMETER_ID` bigint(20)
,`PAGE_PARAMETER_TYPE_NAME` tinytext
,`ACTIVE` varchar(1)
,`PAGE_PARAMETER_NAME` tinytext
,`VALUE` longtext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_page_parameter_type`
--
CREATE TABLE `v_pm_page_parameter_type` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` text
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_pdf`
--
CREATE TABLE `v_pm_pdf` (
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_pdf_parameter_module`
--
CREATE TABLE `v_pm_pdf_parameter_module` (
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_pm_validation`
--
CREATE TABLE `v_pm_validation` (
`code` varchar(259)
,`page_id` bigint(20)
,`item_code` varchar(255)
,`validation_id` bigint(20)
,`params` tinytext
,`param_number` bigint(20)
,`help` text
,`custom_error` binary(0)
,`clt_module_id` int(11)
,`error_message` tinytext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_color_by_reference`
--
CREATE TABLE `v_sm_color_by_reference` (
`CODE` varchar(297)
,`REFERENCE` tinytext
,`PRODUCT_COLOR_ID` bigint(20)
,`PRODUCT_COLOR_NAME` tinytext
,`CLT_MODULE_ID` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_customer`
--
CREATE TABLE `v_sm_customer` (
`ID` bigint(20)
,`FIRST_NAME` tinytext
,`LAST_NAME` tinytext
,`CLT_MODULE_ID` bigint(20)
,`CUSTOMER_TYPE_ID` bigint(20)
,`CUSTOMER_CATEGORY_ID` bigint(20)
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`SHORT_LABEL` tinytext
,`FULL_LABEL` text
,`NOTE` text
,`COMPANY_NAME` tinytext
,`REPRESENTATIVE` tinytext
,`IDENTIFICATION` tinytext
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`CUSTOMER_NATURE_ID` bigint(20)
,`ENTITY_ID` bigint(20)
,`CUSTOMER_TYPE_NAME` tinytext
,`CUSTOMER_CATEGORY_NAME` tinytext
,`ENTITY_PHONE` varchar(510)
,`ENTITY_EMAIL` varchar(255)
,`ENTITY_FAX` varchar(510)
,`ENTITY_WEB` varchar(255)
,`ENTITY_LOCATION_ADDRESS` text
,`ENTITY_LOCATION_CITY_NAME` varchar(255)
,`ENTITY_LOCATION_COUNTRY_NAME` varchar(255)
,`CAN_BE_BELETE` varchar(1)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_customer_category`
--
CREATE TABLE `v_sm_customer_category` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` text
,`CLT_MODULE_ID` bigint(20)
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_customer_type`
--
CREATE TABLE `v_sm_customer_type` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` text
,`CLT_MODULE_ID` bigint(20)
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_deposit`
--
CREATE TABLE `v_sm_deposit` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` text
,`SORT_KEY` bigint(20)
,`CLT_MODULE_ID` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_expense`
--
CREATE TABLE `v_sm_expense` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` text
,`AMOUNT` double
,`EXPENSE_TYPE_ID` bigint(20)
,`CLT_MODULE_ID` bigint(20)
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`EXPENSE_TYPE_NAME` tinytext
,`CLT_MODULE_NAME` tinytext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_expense_type`
--
CREATE TABLE `v_sm_expense_type` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` text
,`ACTIVE` char(1)
,`SORT_KEY` bigint(20)
,`CLT_MODULE_ID` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_inventaire`
--
CREATE TABLE `v_sm_inventaire` (
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_lovs`
--
CREATE TABLE `v_sm_lovs` (
`code` varchar(40)
,`table_code` varchar(17)
,`id` bigint(20)
,`name` text
,`description` text
,`active` char(1)
,`sort_key` bigint(20)
,`clt_module_id` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_order`
--
CREATE TABLE `v_sm_order` (
`ID` bigint(20)
,`NO_SEQ` bigint(20)
,`REFERENCE` tinytext
,`CUSTOMER_ID` bigint(20)
,`PAYMENT_METHOD_ID` bigint(20)
,`CHECK_ID` bigint(20)
,`DELIVERY` tinytext
,`NOTE` text
,`ORDER_STATUS_ID` bigint(20)
,`CLT_MODULE_ID` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`customer_company_name` tinytext
,`total_hors_taxe` double
,`total_ttc` double
,`sum_product` decimal(41,0)
,`order_status_name` tinytext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_order_details`
--
CREATE TABLE `v_sm_order_details` (
`CLT_MODULE_ID` bigint(20)
,`ORDER_SUPPLIER_IN_PROGRESS_COUNT` bigint(21)
,`ORDER_SUPPLIER_TRANSMITTED_COUNT` bigint(21)
,`ORDER_SUPPLIER_VALIDATED_COUNT` bigint(21)
,`ORDER_SUPPLIER_REFUSED_COUNT` bigint(21)
,`ORDER_SUPPLIER_REJECTED_COUNT` bigint(21)
,`RECEPTION_IN_PROGRESS_COUNT` bigint(21)
,`RECEPTION_TRANSMITTED_COUNT` bigint(21)
,`RECEPTION_VALIDATED_COUNT` bigint(21)
,`RECEPTION_REFUSED_COUNT` bigint(21)
,`RECEPTION_REJECTED_COUNT` bigint(21)
,`ORDER_IN_PROGRESS_COUNT` bigint(21)
,`ORDER_TRANSMITTED_COUNT` bigint(21)
,`ORDER_VALIDATED_COUNT` bigint(21)
,`ORDERD_REFUSED_COUNT` bigint(21)
,`ORDERD_REJECTED_COUNT` bigint(21)
,`RETURN_RECEIPT_IN_PROGRESS_COUNT` bigint(21)
,`RETURN_RECEIPT_TRANSMITTED_COUNT` bigint(21)
,`RETURN_RECEIPT_VALIDATED_COUNT` bigint(21)
,`RETURN_RECEIPT_REFUSED_COUNT` bigint(21)
,`RETURN_RECEIPT_REJECTED_COUNT` bigint(21)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_order_line`
--
CREATE TABLE `v_sm_order_line` (
`ID` bigint(20)
,`PRODUCT_ID` bigint(20)
,`DESIGNATION` text
,`ORDER_ID` bigint(20)
,`PROMOTION_ID` bigint(20)
,`NEGOTIATE_PRICE_SALE` double
,`QUANTITY` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`reference` tinytext
,`TOTAL_NEGOTIATE_PRICE_SALE` double
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_order_summary`
--
CREATE TABLE `v_sm_order_summary` (
`order_id` bigint(20)
,`summary_total_quantity` decimal(41,0)
,`summary_total_ht` double
,`summary_tva_amount` double
,`summary_total_ttc` double
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_order_supplier`
--
CREATE TABLE `v_sm_order_supplier` (
`ID` bigint(20)
,`NO_SEQ` bigint(20)
,`REFERENCE` tinytext
,`NOTE` text
,`ORDER_SUPPLIER_STATUS_ID` bigint(20)
,`SUPPLIER_ID` bigint(20)
,`ACTIVE` char(1)
,`CLT_MODULE_ID` bigint(20)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`SUPPLIER_COMPANY_NAME` tinytext
,`TOTAL_HORS_TAXE` double
,`TOTAL_TTC` double
,`SUM_PRODUCT` decimal(41,0)
,`order_supplier_status_name` tinytext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_order_supplier_line`
--
CREATE TABLE `v_sm_order_supplier_line` (
`ID` bigint(20)
,`PRODUCT_ID` bigint(20)
,`DESIGNATION` text
,`UNIT_PRICE_SALE` double
,`QUANTITY` bigint(20)
,`TVA` double
,`REMISE` double
,`PROMOTION_ID` bigint(20)
,`ORDER_SUPPLIER_ID` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`reference` tinytext
,`TOTAL_PRICE_SALE` double
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_order_supplier_not_received`
--
CREATE TABLE `v_sm_order_supplier_not_received` (
`ID` bigint(20)
,`NO_SEQ` bigint(20)
,`REFERENCE` tinytext
,`NOTE` text
,`ORDER_SUPPLIER_STATUS_ID` bigint(20)
,`SUPPLIER_ID` bigint(20)
,`ACTIVE` char(1)
,`CLT_MODULE_ID` bigint(20)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`SUPPLIER_FIRST_NAME` tinytext
,`SUPPLIER_LAST_NAME` tinytext
,`QTE_COMMANDED` double
,`QTE_RECIEVED` double
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_order_supplier_summary`
--
CREATE TABLE `v_sm_order_supplier_summary` (
`order_supplier_id` bigint(20)
,`summaryTotalQuantity` decimal(41,0)
,`summaryTotalHt` double
,`summaryTvaAmount` double
,`summaryTotalTtc` double
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_product`
--
CREATE TABLE `v_sm_product` (
`id` bigint(20)
,`no_seq` bigint(20)
,`reference` tinytext
,`designation` tinytext
,`quantity` bigint(20)
,`threshold` bigint(20)
,`price_sale` double
,`price_buy` double
,`note` text
,`active` char(1)
,`clt_module_id` bigint(20)
,`product_group_id` bigint(20)
,`product_group_name` tinytext
,`product_family_id` bigint(20)
,`product_family_name` tinytext
,`product_size_id` bigint(20)
,`product_size_name` tinytext
,`product_status_id` bigint(20)
,`product_status_name` tinytext
,`product_type_id` bigint(20)
,`product_type_name` tinytext
,`product_color_id` bigint(20)
,`product_color_name` tinytext
,`product_department_id` bigint(20)
,`product_department_name` tinytext
,`product_unit_id` bigint(20)
,`product_unit_name` tinytext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_product_alert`
--
CREATE TABLE `v_sm_product_alert` (
`ID` bigint(20)
,`REFERENCE` tinytext
,`DESIGNATION` tinytext
,`QUANTITY` bigint(20)
,`THRESHOLD` bigint(20)
,`CLT_MODULE_ID` bigint(20)
,`TOTAL_COMMANDED` double
,`TOTAL_RECIEVED` double
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_product_color`
--
CREATE TABLE `v_sm_product_color` (
`ID` bigint(20)
,`NAME` tinytext
,`HEX` tinytext
,`RGB` tinytext
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`CLT_MODULE_ID` bigint(20)
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_product_department`
--
CREATE TABLE `v_sm_product_department` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` varchar(45)
,`SORT_KEY` bigint(20)
,`CLT_MODULE_ID` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_product_details_group`
--
CREATE TABLE `v_sm_product_details_group` (
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_product_family`
--
CREATE TABLE `v_sm_product_family` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` text
,`PRODUCT_GROUP_ID` bigint(20)
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`CLT_MODULE_ID` bigint(20)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_product_group`
--
CREATE TABLE `v_sm_product_group` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` text
,`SORT_KEY` bigint(20)
,`CLT_MODULE_ID` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_product_not_received`
--
CREATE TABLE `v_sm_product_not_received` (
`CODE` varchar(41)
,`ORDER_SUPPLIER_ID` bigint(20)
,`ORDER_SUPPLIER_REFERENCE` tinytext
,`CLT_MODULE_ID` bigint(20)
,`PRODUCT_REFERENCE` tinytext
,`PRODUCT_DESIGNATION` tinytext
,`SUPPLIER_ID` bigint(20)
,`SUPPLIER_FIRST_NAME` tinytext
,`SUPPLIER_LAST_NAME` tinytext
,`SUPPLIER_COMPANY_NAME` tinytext
,`PRODUCT_ID` bigint(20)
,`QTE_COMMANDED` varchar(20)
,`QTE_RECIEVED` double
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_product_size`
--
CREATE TABLE `v_sm_product_size` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` text
,`SORT_KEY` bigint(20)
,`CLT_MODULE_ID` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_product_status`
--
CREATE TABLE `v_sm_product_status` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` text
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`CLT_MODULE_ID` bigint(20)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_product_type`
--
CREATE TABLE `v_sm_product_type` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` varchar(45)
,`SORT_KEY` bigint(20)
,`CLT_MODULE_ID` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_product_unit`
--
CREATE TABLE `v_sm_product_unit` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` varchar(45)
,`SORT_KEY` bigint(20)
,`CLT_MODULE_ID` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_rap_daily`
--
CREATE TABLE `v_sm_rap_daily` (
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_reception`
--
CREATE TABLE `v_sm_reception` (
`ID` bigint(20)
,`NO_SEQ` bigint(20)
,`SOUCHE` tinytext
,`SUPPLIER_ID` bigint(20)
,`DEPOSIT_ID` bigint(20)
,`RECEPTION_STATUS_ID` bigint(20)
,`DEADLINE` datetime
,`NOTE` text
,`TVA` double
,`DELIVERY` tinytext
,`ORDER_SUPPLIER_ID` bigint(20)
,`ACTIVE` char(1)
,`EXPIRATION_DATE` datetime
,`CLT_MODULE_ID` bigint(20)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`REFERENCE` tinytext
,`supplier_company_name` tinytext
,`total_hors_taxe` double
,`total_ttc` double
,`sum_product` decimal(41,0)
,`order_supplier_status_name` tinytext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_reception_line`
--
CREATE TABLE `v_sm_reception_line` (
`ID` bigint(20)
,`PRODUCT_ID` bigint(20)
,`DESIGNATION` tinytext
,`UNIT_PRICE_BUY` double
,`REMISE` double
,`QUANTITY` bigint(20)
,`TVA` double
,`ACTIVE` char(1)
,`RECEPTION_ID` bigint(20)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`reference` tinytext
,`TOTAL_PRICE_BUY` double
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_reception_products`
--
CREATE TABLE `v_sm_reception_products` (
`ID` bigint(20)
,`PRODUCT_ID` bigint(20)
,`DESIGNATION` tinytext
,`UNIT_PRICE_BUY` double(19,2)
,`REMISE` double
,`QUANTITY` bigint(20)
,`TVA` double
,`ACTIVE` char(1)
,`RECEPTION_ID` bigint(20)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`reference` tinytext
,`total_ht` double(19,2)
,`total_ttc` double(19,2)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_reception_summary`
--
CREATE TABLE `v_sm_reception_summary` (
`reception_id` bigint(20)
,`summary_total_quantity` decimal(41,0)
,`summary_total_ht` double
,`summary_tva_amount` double
,`summary_total_ttc` double
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_return_receipt`
--
CREATE TABLE `v_sm_return_receipt` (
`ID` bigint(20)
,`NO_SEQ` bigint(20)
,`REFERENCE` tinytext
,`CUSTOMER_ID` bigint(20)
,`ORDER_ID` bigint(20)
,`RETURN_RECEIPT_STATUS_ID` bigint(20)
,`NOTE` text
,`ACTIVE` char(1)
,`CLT_MODULE_ID` bigint(20)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`customer_company_name` tinytext
,`total_hors_taxe` double
,`total_ttc` double
,`sum_product` decimal(41,0)
,`sm_return_receipt_status_name` tinytext
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_return_receipt_line`
--
CREATE TABLE `v_sm_return_receipt_line` (
`ID` bigint(20)
,`PRODUCT_ID` bigint(20)
,`DESIGNATION` text
,`RETURN_RECEIPT_ID` bigint(20)
,`QUANTITY` bigint(20)
,`PROMOTION_ID` bigint(20)
,`NEGOTIATE_PRICE_SALE` double
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`reference` tinytext
,`total_price_sale` double
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_return_receipt_summary`
--
CREATE TABLE `v_sm_return_receipt_summary` (
`return_receipt_id` bigint(20)
,`summary_total_quantity` decimal(41,0)
,`summary_total_ht` double
,`summary_tva_amount` double
,`summary_total_ttc` double
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_size_by_reference`
--
CREATE TABLE `v_sm_size_by_reference` (
`CODE` varchar(297)
,`REFERENCE` tinytext
,`PRODUCT_SIZE_ID` bigint(20)
,`PRODUCT_SIZE_NAME` tinytext
,`CLT_MODULE_ID` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_supplier`
--
CREATE TABLE `v_sm_supplier` (
`ID` bigint(20)
,`FIRST_NAME` tinytext
,`LAST_NAME` tinytext
,`COMPANY_NAME` tinytext
,`REPRESENTATIVE` tinytext
,`SHORT_LABEL` tinytext
,`FULL_LABEL` text
,`NOTE` text
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`SUPPLIER_TYPE_ID` bigint(20)
,`SUPPLIER_CATEGORY_ID` bigint(20)
,`CLT_MODULE_ID` bigint(20)
,`ENTITY_ID` bigint(20)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
,`SUPPLIER_NATURE_ID` bigint(20)
,`SUPPLIER_TYPE_NAME` tinytext
,`SUPPLIER_CATEGORY_NAME` tinytext
,`ENTITY_PHONE` varchar(510)
,`ENTITY_EMAIL` varchar(255)
,`ENTITY_FAX` varchar(510)
,`ENTITY_WEB` varchar(255)
,`ENTITY_LOCATION_ADDRESS` text
,`ENTITY_LOCATION_CITY_NAME` varchar(255)
,`ENTITY_LOCATION_COUNTRY_NAME` varchar(255)
,`CAN_BE_BELETE` varchar(1)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_supplier_category`
--
CREATE TABLE `v_sm_supplier_category` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` varchar(45)
,`CLT_MODULE_ID` bigint(20)
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_sm_supplier_type`
--
CREATE TABLE `v_sm_supplier_type` (
`ID` bigint(20)
,`NAME` tinytext
,`DESCRIPTION` text
,`CLT_MODULE_ID` bigint(20)
,`SORT_KEY` bigint(20)
,`ACTIVE` char(1)
,`DATE_CREATION` datetime
,`USER_CREATION` bigint(20)
,`DATE_UPDATE` datetime
,`USER_UPDATE` bigint(20)
);
-- --------------------------------------------------------

--
-- Structure de la vue `v_clt_client`
--
DROP TABLE IF EXISTS `v_clt_client`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_clt_client` AS select `clt_client`.`ID` AS `ID`,`clt_client`.`CODE` AS `CODE`,`clt_client`.`FIRST_NAME` AS `FIRST_NAME`,`clt_client`.`LAST_NAME` AS `LAST_NAME`,`clt_client`.`COMPANY_NAME` AS `COMPANY_NAME`,`clt_client`.`CLIENT_STATUS_ID` AS `CLIENT_STATUS_ID`,`clt_client`.`DEFAULT_LANGUAGE_ID` AS `DEFAULT_LANGUAGE_ID`,`clt_client`.`ACTIVE` AS `ACTIVE`,`clt_client`.`DATE_CREATION` AS `DATE_CREATION`,`clt_client`.`USER_CREATION` AS `USER_CREATION`,`clt_client`.`DATE_UPDATE` AS `DATE_UPDATE`,`clt_client`.`USER_UPDATE` AS `USER_UPDATE`,`clt_client`.`ENTITY_ID` AS `ENTITY_ID` from `clt_client`;

-- --------------------------------------------------------

--
-- Structure de la vue `v_clt_client_details`
--
DROP TABLE IF EXISTS `v_clt_client_details`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_clt_client_details` AS select `clt_c`.`ID` AS `ID`,`clt_c`.`CODE` AS `CODE`,`clt_c`.`FIRST_NAME` AS `FIRST_NAME`,`clt_c`.`LAST_NAME` AS `LAST_NAME`,`clt_c`.`COMPANY_NAME` AS `COMPANY_NAME`,`clt_c`.`CLIENT_STATUS_ID` AS `CLIENT_STATUS_ID`,`clt_c`.`DEFAULT_LANGUAGE_ID` AS `DEFAULT_LANGUAGE_ID`,`clt_c`.`ACTIVE` AS `ACTIVE`,`clt_c`.`DATE_CREATION` AS `DATE_CREATION`,`clt_c`.`USER_CREATION` AS `USER_CREATION`,`clt_c`.`DATE_UPDATE` AS `DATE_UPDATE`,`clt_c`.`USER_UPDATE` AS `USER_UPDATE`,`clt_c`.`ENTITY_ID` AS `ENTITY_ID` from `clt_client` `clt_c`;

-- --------------------------------------------------------

--
-- Structure de la vue `v_clt_client_language`
--
DROP TABLE IF EXISTS `v_clt_client_language`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_clt_client_language` AS select concat_ws('.',`clt_cl`.`CLIENT_ID`,`inf_l`.`ID`) AS `code`,`clt_cl`.`CLIENT_ID` AS `client_id`,`inf_l`.`ID` AS `inf_language_id`,`inf_l`.`CODE` AS `inf_language_code`,`inf_p`.`ID` AS `inf_prefix_id`,`inf_p`.`CODE` AS `inf_prefix_code` from ((`clt_client_language` `clt_cl` join `inf_prefix` `inf_p`) join `inf_language` `inf_l`) where ((`clt_cl`.`LANGUAGE_ID` = `inf_l`.`ID`) and (`clt_cl`.`INF_PREFIX_ID` = `inf_p`.`ID`) and (ucase(`clt_cl`.`ACTIVE`) = convert(ucase('y') using latin1)) and (ucase(`inf_p`.`ACTIVE`) = convert(ucase('y') using latin1)) and (ucase(`inf_l`.`ACTIVE`) = convert(ucase('y') using latin1)));

-- --------------------------------------------------------

--
-- Structure de la vue `v_clt_module`
--
DROP TABLE IF EXISTS `v_clt_module`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_clt_module` AS select `cm`.`ID` AS `ID`,`cm`.`NAME` AS `module_name`,`cm`.`DESCRIPTION` AS `module_description`,`cmt`.`NAME` AS `module_type_name`,`cm`.`CLIENT_ID` AS `client_id`,`cm`.`ACTIVE` AS `active` from (`clt_module` `cm` join `clt_module_type` `cmt`) where (`cm`.`MODULE_TYPE_ID` = `cmt`.`ID`);

-- --------------------------------------------------------

--
-- Structure de la vue `v_clt_module_details`
--
DROP TABLE IF EXISTS `v_clt_module_details`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_clt_module_details` AS select `clt_m`.`ID` AS `ID`,`clt_m`.`NAME` AS `NAME`,`clt_m`.`DESCRIPTION` AS `DESCRIPTION`,`clt_m`.`CLIENT_ID` AS `CLIENT_ID`,`clt_m`.`MODULE_STATUS_ID` AS `MODULE_STATUS_ID`,`clt_m`.`MODULE_TYPE_ID` AS `MODULE_TYPE_ID`,`clt_m`.`PARENT_MODULE_ID` AS `PARENT_MODULE_ID`,`clt_m`.`SORT_KEY` AS `SORT_KEY`,`clt_m`.`ACTIVE` AS `ACTIVE`,`clt_m`.`DATE_CREATION` AS `DATE_CREATION`,`clt_m`.`USER_CREATION` AS `USER_CREATION`,`clt_m`.`DATE_UPDATE` AS `DATE_UPDATE`,`clt_m`.`USER_UPDATE` AS `USER_UPDATE` from `clt_module` `clt_m`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_clt_module_parameter`
--
DROP TABLE IF EXISTS `v_clt_module_parameter`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_clt_module_parameter` AS select `clt_mod_par`.`ID` AS `id`,`clt_mod_par`.`NAME` AS `name`,`clt_mod_par`.`DESCRIPTION` AS `description`,`clt_mod_par`.`DEFAULT_VALUE` AS `DEFAULT_VALUE`,`clt_mod_par`.`MODULE_PARAMETER_TYPE_ID` AS `MODULE_PARAMETER_TYPE_ID`,`clt_mod_par_type`.`NAME` AS `clt_module_parameter_type_name`,`clt_mod_par`.`ACTIVE` AS `active`,'false' AS `can_be_delete` from (`clt_module_parameter` `clt_mod_par` join `clt_module_parameter_type` `clt_mod_par_type`) where (`clt_mod_par`.`MODULE_PARAMETER_TYPE_ID` = `clt_mod_par_type`.`ID`);

-- --------------------------------------------------------

--
-- Structure de la vue `v_clt_module_parameter_client`
--
DROP TABLE IF EXISTS `v_clt_module_parameter_client`;

CREATE ALGORITHM=UNDEFINED DEFINER=`mystock`@`%` SQL SECURITY DEFINER VIEW `v_clt_module_parameter_client` AS (select concat_ws('.',`CLT_M`.`ID`,`CLT_MP`.`ID`) AS `CODE`,`CLT_M`.`ID` AS `MODULE_ID`,`CLT_MP`.`ID` AS `PARAMETER_ID`,`CLT_MPT`.`NAME` AS `PARAMETER_TYPE_NAME`,'Y' AS `ACTIVE`,`CLT_MP`.`NAME` AS `PARAMETER_NAME`,coalesce(coalesce((select `CLT_MPMODULE`.`VALUE` from `clt_model_parameter_module` `CLT_MPMODULE` where ((`CLT_MPMODULE`.`MODULE_ID` = `CLT_M`.`ID`) and (`CLT_MPMODULE`.`MODEL_PARAMETER_ID` = `CLT_MP`.`ID`) and (ucase(`CLT_MPMODULE`.`ACTIVE`) = 'Y'))),(select `CLT_MPMODEL`.`VALUE` from `clt_model_parameter_model` `CLT_MPMODEL` where ((`CLT_MPMODEL`.`PM_MODEL_ID` = `CLT_M`.`PM_MODEL_ID`) and (`CLT_MPMODEL`.`MODEL_PARAMETER_ID` = `CLT_MP`.`ID`) and (ucase(`CLT_MPMODEL`.`ACTIVE`) = 'Y')))),`CLT_MP`.`DEFAULT_VALUE`) AS `VALUE` from ((`clt_module` `CLT_M` join `clt_model_parameter` `CLT_MP`) join `clt_model_parameter_type` `CLT_MPT`) where (`CLT_MP`.`MODEL_PARAMETER_TYPE_ID` = `CLT_MPT`.`ID`));

-- --------------------------------------------------------

--
-- Structure de la vue `v_clt_module_parameter_type`
--
DROP TABLE IF EXISTS `v_clt_module_parameter_type`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_clt_module_parameter_type` AS select `clt_module_parameter_type`.`ID` AS `ID`,`clt_module_parameter_type`.`NAME` AS `NAME`,`clt_module_parameter_type`.`DESCRIPTION` AS `DESCRIPTION`,`clt_module_parameter_type`.`SORT_KEY` AS `SORT_KEY`,`clt_module_parameter_type`.`ACTIVE` AS `ACTIVE`,`clt_module_parameter_type`.`DATE_CREATION` AS `DATE_CREATION`,`clt_module_parameter_type`.`USER_CREATION` AS `USER_CREATION`,`clt_module_parameter_type`.`DATE_UPDATE` AS `DATE_UPDATE`,`clt_module_parameter_type`.`USER_UPDATE` AS `USER_UPDATE` from `clt_module_parameter_type`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_clt_module_type`
--
DROP TABLE IF EXISTS `v_clt_module_type`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_clt_module_type` AS select `clt_module_type`.`ID` AS `ID`,`clt_module_type`.`NAME` AS `NAME`,`clt_module_type`.`DESCRIPTION` AS `DESCRIPTION`,`clt_module_type`.`SORT_KEY` AS `SORT_KEY`,`clt_module_type`.`ACTIVE` AS `ACTIVE`,`clt_module_type`.`DATE_CREATION` AS `DATE_CREATION`,`clt_module_type`.`USER_CREATION` AS `USER_CREATION`,`clt_module_type`.`DATE_UPDATE` AS `DATE_UPDATE`,`clt_module_type`.`USER_UPDATE` AS `USER_UPDATE` from `clt_module_type`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_clt_parameter`
--
DROP TABLE IF EXISTS `v_clt_parameter`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_clt_parameter` AS select `clt_par`.`ID` AS `id`,`clt_par`.`NAME` AS `name`,`clt_par`.`DESCRIPTION` AS `description`,`clt_par`.`DEFAULT_VALUE` AS `DEFAULT_VALUE`,`clt_par`.`PARAMETER_TYPE_ID` AS `PARAMETER_TYPE_ID`,`clt_par_type`.`NAME` AS `clt_parameter_type_name`,`clt_par`.`ACTIVE` AS `active`,'false' AS `can_be_delete` from (`clt_parameter` `clt_par` join `clt_parameter_type` `clt_par_type`) where (`clt_par`.`PARAMETER_TYPE_ID` = `clt_par_type`.`ID`);

-- --------------------------------------------------------

--
-- Structure de la vue `v_clt_parameters`
--
DROP TABLE IF EXISTS `v_clt_parameters`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_clt_parameters` AS select concat_ws('.',`clt_pc`.`CLINET_ID`,`clt_p`.`ID`) AS `code`,`clt_p`.`ID` AS `id`,`clt_p`.`PARAMETER_TYPE_ID` AS `parameter_type_id`,`clt_pc`.`CLINET_ID` AS `clinet_id`,(case when (`clt_pc`.`VALUE` = '') then `clt_p`.`DEFAULT_VALUE` when isnull(`clt_p`.`DEFAULT_VALUE`) then `clt_pc`.`VALUE` else `clt_pc`.`VALUE` end) AS `val`,`clt_pc`.`VALUE` AS `value`,`clt_p`.`DEFAULT_VALUE` AS `default_value` from ((`clt_parameter` `clt_p` join `clt_parameter_type` `clt_pt`) join `clt_parameter_client` `clt_pc`) where ((`clt_p`.`PARAMETER_TYPE_ID` = `clt_pt`.`ID`) and (`clt_p`.`ID` = `clt_pc`.`PARAMETER_ID`));

-- --------------------------------------------------------

--
-- Structure de la vue `v_clt_parameter_client`
--
DROP TABLE IF EXISTS `v_clt_parameter_client`;

CREATE ALGORITHM=UNDEFINED DEFINER=`mystock`@`%` SQL SECURITY DEFINER VIEW `v_clt_parameter_client` AS (select concat_ws('.',`CLT_C`.`ID`,`CLT_P`.`ID`) AS `CODE`,`CLT_C`.`ID` AS `CLIENT_ID`,`CLT_P`.`ID` AS `PARAMETER_ID`,`CLT_PT`.`NAME` AS `PARAMETER_TYPE_NAME`,'Y' AS `ACTIVE`,`CLT_P`.`NAME` AS `PARAMETER_NAME`,coalesce((select `CLT_PC`.`VALUE` from `clt_parameter_client` `CLT_PC` where ((`CLT_PC`.`CLINET_ID` = `CLT_C`.`ID`) and (`CLT_PC`.`PARAMETER_ID` = `CLT_P`.`ID`) and (ucase(`CLT_PC`.`ACTIVE`) = 'Y'))),`CLT_P`.`DEFAULT_VALUE`) AS `VALUE` from ((`clt_client` `CLT_C` join `clt_parameter` `CLT_P`) join `clt_parameter_type` `CLT_PT`) where (`CLT_P`.`PARAMETER_TYPE_ID` = `CLT_PT`.`ID`));

-- --------------------------------------------------------

--
-- Structure de la vue `v_clt_parameter_type`
--
DROP TABLE IF EXISTS `v_clt_parameter_type`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_clt_parameter_type` AS select `clt_parameter_type`.`ID` AS `ID`,`clt_parameter_type`.`NAME` AS `NAME`,`clt_parameter_type`.`DESCRIPTION` AS `DESCRIPTION`,`clt_parameter_type`.`SORT_KEY` AS `SORT_KEY`,`clt_parameter_type`.`ACTIVE` AS `ACTIVE`,`clt_parameter_type`.`DATE_CREATION` AS `DATE_CREATION`,`clt_parameter_type`.`USER_CREATION` AS `USER_CREATION`,`clt_parameter_type`.`DATE_UPDATE` AS `DATE_UPDATE`,`clt_parameter_type`.`USER_UPDATE` AS `USER_UPDATE` from `clt_parameter_type`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_clt_user`
--
DROP TABLE IF EXISTS `v_clt_user`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_clt_user` AS select `clt_user`.`ID` AS `ID`,`clt_user`.`FIRST_NAME` AS `FIRST_NAME`,`clt_user`.`LAST_NAME` AS `LAST_NAME`,`clt_user`.`USERNAME` AS `USERNAME`,`clt_user`.`PASSWORD` AS `PASSWORD`,`clt_user`.`CATEGORY_ID` AS `CATEGORY_ID`,`clt_user`.`CLIENT_ID` AS `CLIENT_ID`,`clt_user`.`USER_STATUS_ID` AS `USER_STATUS_ID`,`clt_user`.`DEFAULT_LANGUAGE_ID` AS `DEFAULT_LANGUAGE_ID`,`clt_user`.`ACTIVE` AS `ACTIVE`,`clt_user`.`DATE_CREATION` AS `DATE_CREATION`,`clt_user`.`USER_CREATION` AS `USER_CREATION`,`clt_user`.`DATE_UPDATE` AS `DATE_UPDATE`,`clt_user`.`USER_UPDATE` AS `USER_UPDATE`,`clt_user`.`IMAGE_PATH` AS `IMAGE_PATH`,`clt_user`.`ENTITY_ID` AS `ENTITY_ID` from `clt_user`;

-- --------------------------------------------------------

--
-- Structure de la vue `v_clt_user_category`
--
DROP TABLE IF EXISTS `v_clt_user_category`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_clt_user_category` AS select `clt_user_category`.`ID` AS `ID`,`clt_user_category`.`NAME` AS `NAME`,`clt_user_category`.`DESCRIPTION` AS `DESCRIPTION`,`clt_user_category`.`SORT_KEY` AS `SORT_KEY`,`clt_user_category`.`ACTIVE` AS `ACTIVE`,`clt_user_category`.`DATE_CREATION` AS `DATE_CREATION`,`clt_user_category`.`USER_CREATION` AS `USER_CREATION`,`clt_user_category`.`DATE_UPDATE` AS `DATE_UPDATE`,`clt_user_category`.`USER_UPDATE` AS `USER_UPDATE` from `clt_user_category`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_clt_user_client`
--
DROP TABLE IF EXISTS `v_clt_user_client`;
-- utilisé(#1356 - View 'mystock.v_clt_user_client' references invalid table(s) or column(s) or function(s) or definer/invoker of view lack rights to use them)

-- --------------------------------------------------------

--
-- Structure de la vue `v_clt_user_details`
--
DROP TABLE IF EXISTS `v_clt_user_details`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_clt_user_details` AS select `clt_user`.`ID` AS `ID`,`clt_user`.`FIRST_NAME` AS `FIRST_NAME`,`clt_user`.`LAST_NAME` AS `LAST_NAME`,`clt_user`.`USERNAME` AS `USERNAME`,`clt_user`.`PASSWORD` AS `PASSWORD`,`clt_user`.`CATEGORY_ID` AS `CATEGORY_ID`,`clt_user`.`CLIENT_ID` AS `CLIENT_ID`,`clt_user`.`USER_STATUS_ID` AS `USER_STATUS_ID`,`clt_user`.`DEFAULT_LANGUAGE_ID` AS `DEFAULT_LANGUAGE_ID`,`clt_user`.`ACTIVE` AS `ACTIVE`,`clt_user`.`DATE_CREATION` AS `DATE_CREATION`,`clt_user`.`USER_CREATION` AS `USER_CREATION`,`clt_user`.`DATE_UPDATE` AS `DATE_UPDATE`,`clt_user`.`USER_UPDATE` AS `USER_UPDATE`,`clt_user`.`IMAGE_PATH` AS `IMAGE_PATH`,`clt_user`.`ENTITY_ID` AS `ENTITY_ID` from `clt_user`;

-- --------------------------------------------------------

--
-- Structure de la vue `v_clt_user_module`
--
DROP TABLE IF EXISTS `v_clt_user_module`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_clt_user_module` AS select `clt_um`.`ID` AS `ID`,`clt_um`.`USER_ID` AS `USER_ID`,`clt_um`.`MODULE_ID` AS `MODULE_ID`,`clt_um`.`FULL CONTROL` AS `FULL CONTROL`,`clt_um`.`MODULE_ID_TO_MANAGER` AS `MODULE_ID_TO_MANAGER`,`clt_um`.`ACTIVE` AS `ACTIVE`,`clt_um`.`SORT_KEY` AS `SORT_KEY`,`clt_um`.`DATE_CREATION` AS `DATE_CREATION`,`clt_um`.`USER_CREATION` AS `USER_CREATION`,`clt_um`.`DATE_UPDATE` AS `DATE_UPDATE`,`clt_um`.`USER_UPDATE` AS `USER_UPDATE`,`clt_m`.`MODULE_TYPE_ID` AS `MODULE_TYPE_ID`,`clt_m`.`NAME` AS `MODULE_NAME`,`clt_m`.`IMAGE_PATH` AS `MODULE_IMAGE_PATH` from ((`clt_user_module` `clt_um` join `clt_module` `clt_m`) join `clt_module_type` `clt_mt`) where ((ucase(`clt_um`.`ACTIVE`) = convert(ucase('y') using latin1)) and (`clt_um`.`MODULE_ID` = `clt_m`.`ID`) and (`clt_m`.`MODULE_TYPE_ID` = `clt_mt`.`ID`)) order by `clt_um`.`SORT_KEY`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_compos_cat`
--
DROP TABLE IF EXISTS `v_compos_cat`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_compos_cat` AS select `pm_composition`.`MODEL_ID` AS `model_id`,`pm_composition`.`GROUP_ID` AS `group_id`,min(`pm_composition`.`CATEGORY_SORT`) AS `category_sort`,`pm_composition`.`CATEGORY_ID` AS `category_id`,`pm_composition`.`ACTIVE` AS `active` from `pm_composition` group by `pm_composition`.`MODEL_ID`,`pm_composition`.`GROUP_ID`,`pm_composition`.`CATEGORY_ID`,`pm_composition`.`ACTIVE` order by min(`pm_composition`.`CATEGORY_SORT`);

-- --------------------------------------------------------

--
-- Structure de la vue `v_cta_email_type`
--
DROP TABLE IF EXISTS `v_cta_email_type`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_cta_email_type` AS select `cta_email_type`.`ID` AS `ID`,`cta_email_type`.`NAME` AS `NAME`,`cta_email_type`.`DESCRIPTION` AS `DESCRIPTION`,`cta_email_type`.`SORT_KEY` AS `SORT_KEY`,`cta_email_type`.`ACTIVE` AS `ACTIVE`,`cta_email_type`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`cta_email_type`.`DATE_CREATION` AS `DATE_CREATION`,`cta_email_type`.`USER_CREATION` AS `USER_CREATION`,`cta_email_type`.`DATE_UPDATE` AS `DATE_UPDATE`,`cta_email_type`.`USER_UPDATE` AS `USER_UPDATE` from `cta_email_type`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_cta_entity_contact`
--
DROP TABLE IF EXISTS `v_cta_entity_contact`;

CREATE ALGORITHM=UNDEFINED DEFINER=`mystock`@`%` SQL SECURITY DEFINER VIEW `v_cta_entity_contact` AS (select `CTA_E`.`ID` AS `ID`,(select concat_ws('',`CTA_EP`.`COUNTRY_CODE`,`CTA_EP`.`NUMBER`) from `cta_entity_phone` `CTA_EP` where (`CTA_E`.`ID` = `CTA_EP`.`ENTITY_ID`) order by `CTA_EP`.`IS_PRINCIPAL` desc,`CTA_EP`.`PRIORITY` limit 1) AS `ENTITY_PHONE`,(select `CTA_EE`.`EMAIL_ADDRESS` from `cta_entity_email` `CTA_EE` where (`CTA_E`.`ID` = `CTA_EE`.`ENTITY_ID`) order by `CTA_EE`.`IS_PRINCIPAL` desc,`CTA_EE`.`PRIORITY` limit 1) AS `ENTITY_EMAIL`,(select concat_ws('',`CTA_EF`.`COUNTRY_CODE`,`CTA_EF`.`NUMBER`) from `cta_entity_fax` `CTA_EF` where (`CTA_E`.`ID` = `CTA_EF`.`ENTITY_ID`) order by `CTA_EF`.`IS_PRINCIPAL` desc,`CTA_EF`.`PRIORITY` limit 1) AS `ENTITY_FAX`,(select concat_ws(' ',`CTA_EL`.`ADDRESS_LINE_1`,`CTA_EL`.`ADDRESS_LINE_2`,`CTA_EL`.`ADDRESS_LINE_3`) from `cta_entity_location` `CTA_EL` where (`CTA_E`.`ID` = `CTA_EL`.`ENTITY_ID`) order by `CTA_EL`.`IS_PRINCIPAL` desc,`CTA_EL`.`PRIORITY` limit 1) AS `ENTITY_LOCATION_ADDRESS`,(select `INF_CI`.`NAME` from (`cta_entity_location` `CTA_EL` left join `inf_city` `INF_CI` on((`INF_CI`.`ID` = `CTA_EL`.`INF_CITY_ID`))) where (`CTA_E`.`ID` = `CTA_EL`.`ENTITY_ID`) order by `CTA_EL`.`IS_PRINCIPAL` desc,`CTA_EL`.`PRIORITY` limit 1) AS `ENTITY_LOCATION_CITY_NAME`,(select `INF_CO`.`NAME` from (`cta_entity_location` `CTA_EL` left join `inf_country` `INF_CO` on((`INF_CO`.`ID` = `CTA_EL`.`INF_COUNTRY_ID`))) where (`CTA_E`.`ID` = `CTA_EL`.`ENTITY_ID`) order by `CTA_EL`.`IS_PRINCIPAL` desc,`CTA_EL`.`PRIORITY` limit 1) AS `ENTITY_LOCATION_COUNTRY_NAME`,(select `CTA_EW`.`URL` from `cta_entity_web` `CTA_EW` where (`CTA_E`.`ID` = `CTA_EW`.`ENTITY_ID`) order by `CTA_EW`.`IS_PRINCIPAL` desc,`CTA_EW`.`PRIORITY` limit 1) AS `ENTITY_WEB` from `cta_entity` `CTA_E`);

-- --------------------------------------------------------

--
-- Structure de la vue `v_cta_entity_email`
--
DROP TABLE IF EXISTS `v_cta_entity_email`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_cta_entity_email` AS select `cta_et`.`ID` AS `ID`,`cta_et`.`EMAIL_ADDRESS` AS `EMAIL_ADDRESS`,`cta_et`.`IS_FOR_NOTIFICATION` AS `IS_FOR_NOTIFICATION`,`cta_et`.`ENTITY_ID` AS `ENTITY_ID`,`cta_et`.`PRIORITY` AS `PRIORITY`,`cta_et`.`EMAIL_TYPE_ID` AS `EMAIL_TYPE_ID`,`cta_et`.`DATE_CREATION` AS `DATE_CREATION`,`cta_et`.`USER_CREATION` AS `USER_CREATION`,`cta_et`.`DATE_UPDATE` AS `DATE_UPDATE`,`cta_et`.`USER_UPDATE` AS `USER_UPDATE`,`cta_ee`.`NAME` AS `EMAIL_TYPE_NAME` from (`cta_entity_email` `cta_et` join `cta_email_type` `cta_ee` on((`cta_et`.`EMAIL_TYPE_ID` = `cta_ee`.`ID`)));

-- --------------------------------------------------------

--
-- Structure de la vue `v_cta_entity_fax`
--
DROP TABLE IF EXISTS `v_cta_entity_fax`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_cta_entity_fax` AS select `cta_ef`.`ID` AS `ID`,`cta_ef`.`COUNTRY_CODE` AS `COUNTRY_CODE`,`cta_ef`.`NUMBER` AS `NUMBER`,`cta_ef`.`PRIORITY` AS `PRIORITY`,`cta_ef`.`ENTITY_ID` AS `ENTITY_ID`,`cta_ef`.`FAX_TYPE_ID` AS `FAX_TYPE_ID`,`cta_ef`.`DATE_CREATION` AS `DATE_CREATION`,`cta_ef`.`USER_CREATION` AS `USER_CREATION`,`cta_ef`.`DATE_UPDATE` AS `DATE_UPDATE`,`cta_ef`.`USER_UPDATE` AS `USER_UPDATE`,`cta_ft`.`NAME` AS `FAX_TYPE_NAME` from (`cta_entity_fax` `cta_ef` join `cta_fax_type` `cta_ft` on((`cta_ef`.`FAX_TYPE_ID` = `cta_ft`.`ID`)));

-- --------------------------------------------------------

--
-- Structure de la vue `v_cta_entity_location`
--
DROP TABLE IF EXISTS `v_cta_entity_location`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_cta_entity_location` AS select `cta_el`.`ID` AS `ID`,`cta_el`.`ADDRESS_LINE_1` AS `ADDRESS_LINE_1`,`cta_el`.`ADDRESS_LINE_2` AS `ADDRESS_LINE_2`,`cta_el`.`ADDRESS_LINE_3` AS `ADDRESS_LINE_3`,`cta_el`.`INF_COUNTRY_ID` AS `INF_COUNTRY_ID`,`cta_el`.`INF_CITY_ID` AS `INF_CITY_ID`,`cta_el`.`POSTAL_CODE` AS `POSTAL_CODE`,`cta_el`.`PRIORITY` AS `PRIORITY`,`cta_el`.`ENTITY_ID` AS `ENTITY_ID`,`cta_el`.`LOCATION_TYPE_ID` AS `LOCATION_TYPE_ID`,`cta_el`.`DATE_CREATION` AS `DATE_CREATION`,`cta_el`.`USER_CREATION` AS `USER_CREATION`,`cta_el`.`DATE_UPDATE` AS `DATE_UPDATE`,`cta_el`.`USER_UPDATE` AS `USER_UPDATE`,`cta_lt`.`NAME` AS `LOCATION_TYPE_NAME`,`inf_c`.`NAME` AS `INF_COUNTRY_NAME`,`inf_ci`.`NAME` AS `INF_CITY_NAME` from (((`cta_entity_location` `cta_el` left join `cta_location_type` `cta_lt` on((`cta_el`.`LOCATION_TYPE_ID` = `cta_lt`.`ID`))) left join `inf_country` `inf_c` on((`cta_el`.`INF_COUNTRY_ID` = `inf_c`.`ID`))) left join `inf_city` `inf_ci` on((`cta_el`.`INF_CITY_ID` = `inf_ci`.`ID`)));

-- --------------------------------------------------------

--
-- Structure de la vue `v_cta_entity_phone`
--
DROP TABLE IF EXISTS `v_cta_entity_phone`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_cta_entity_phone` AS select `cta_ep`.`ID` AS `ID`,`cta_ep`.`COUNTRY_CODE` AS `COUNTRY_CODE`,`cta_ep`.`NUMBER` AS `NUMBER`,`cta_ep`.`ENTITY_ID` AS `ENTITY_ID`,`cta_ep`.`PHONE_TYPE_ID` AS `PHONE_TYPE_ID`,`cta_ep`.`PRIORITY` AS `PRIORITY`,`cta_ep`.`DATE_CREATION` AS `DATE_CREATION`,`cta_ep`.`USER_CREATION` AS `USER_CREATION`,`cta_ep`.`DATE_UPDATE` AS `DATE_UPDATE`,`cta_ep`.`USER_UPDATE` AS `USER_UPDATE`,`cta_pt`.`NAME` AS `PHONE_TYPE_NAME` from (`cta_entity_phone` `cta_ep` left join `cta_phone_type` `cta_pt` on((`cta_ep`.`PHONE_TYPE_ID` = `cta_pt`.`ID`)));

-- --------------------------------------------------------

--
-- Structure de la vue `v_cta_entity_web`
--
DROP TABLE IF EXISTS `v_cta_entity_web`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_cta_entity_web` AS select `cta_a`.`ID` AS `ID`,`cta_a`.`URL` AS `URL`,`cta_a`.`PRIORITY` AS `PRIORITY`,`cta_a`.`ENTITY_ID` AS `ENTITY_ID`,`cta_a`.`WEB_TYPE_ID` AS `WEB_TYPE_ID`,`cta_a`.`DATE_CREATION` AS `DATE_CREATION`,`cta_a`.`USER_CREATION` AS `USER_CREATION`,`cta_a`.`DATE_UPDATE` AS `DATE_UPDATE`,`cta_a`.`USER_UPDATE` AS `USER_UPDATE`,`cta_wt`.`NAME` AS `WEB_TYPE_NAME`,`cta_a`.`URL` AS `EXTERNAL_URL` from (`cta_entity_web` `cta_a` join `cta_web_type` `cta_wt`) where (`cta_a`.`WEB_TYPE_ID` = `cta_wt`.`ID`);

-- --------------------------------------------------------

--
-- Structure de la vue `v_cta_fax_type`
--
DROP TABLE IF EXISTS `v_cta_fax_type`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_cta_fax_type` AS select `cta_fax_type`.`ID` AS `ID`,`cta_fax_type`.`NAME` AS `NAME`,`cta_fax_type`.`DESCRIPTION` AS `DESCRIPTION`,`cta_fax_type`.`SORT_KEY` AS `SORT_KEY`,`cta_fax_type`.`ACTIVE` AS `ACTIVE`,`cta_fax_type`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`cta_fax_type`.`DATE_CREATION` AS `DATE_CREATION`,`cta_fax_type`.`USER_CREATION` AS `USER_CREATION`,`cta_fax_type`.`DATE_UPDATE` AS `DATE_UPDATE`,`cta_fax_type`.`USER_UPDATE` AS `USER_UPDATE` from `cta_fax_type`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_cta_location_type`
--
DROP TABLE IF EXISTS `v_cta_location_type`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_cta_location_type` AS select `cta_location_type`.`ID` AS `ID`,`cta_location_type`.`NAME` AS `NAME`,`cta_location_type`.`DESCRIPTION` AS `DESCRIPTION`,`cta_location_type`.`SORT_KEY` AS `SORT_KEY`,`cta_location_type`.`ACTIVE` AS `ACTIVE`,`cta_location_type`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`cta_location_type`.`DATE_CREATION` AS `DATE_CREATION`,`cta_location_type`.`USER_CREATION` AS `USER_CREATION`,`cta_location_type`.`DATE_UPDATE` AS `DATE_UPDATE`,`cta_location_type`.`USER_UPDATE` AS `USER_UPDATE` from `cta_location_type`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_cta_phone_type`
--
DROP TABLE IF EXISTS `v_cta_phone_type`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_cta_phone_type` AS select `cta_phone_type`.`ID` AS `ID`,`cta_phone_type`.`NAME` AS `NAME`,`cta_phone_type`.`DESCRIPTION` AS `DESCRIPTION`,`cta_phone_type`.`SORT_KEY` AS `SORT_KEY`,`cta_phone_type`.`ACTIVE` AS `ACTIVE`,`cta_phone_type`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`cta_phone_type`.`DATE_CREATION` AS `DATE_CREATION`,`cta_phone_type`.`USER_CREATION` AS `USER_CREATION`,`cta_phone_type`.`DATE_UPDATE` AS `DATE_UPDATE`,`cta_phone_type`.`USER_UPDATE` AS `USER_UPDATE` from `cta_phone_type`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_cta_web_type`
--
DROP TABLE IF EXISTS `v_cta_web_type`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_cta_web_type` AS select `cta_web_type`.`ID` AS `ID`,`cta_web_type`.`NAME` AS `NAME`,`cta_web_type`.`DESCRIPTION` AS `DESCRIPTION`,`cta_web_type`.`SORT_KEY` AS `SORT_KEY`,`cta_web_type`.`ACTIVE` AS `ACTIVE`,`cta_web_type`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`cta_web_type`.`DATE_CREATION` AS `DATE_CREATION`,`cta_web_type`.`USER_CREATION` AS `USER_CREATION`,`cta_web_type`.`DATE_UPDATE` AS `DATE_UPDATE`,`cta_web_type`.`USER_UPDATE` AS `USER_UPDATE` from `cta_web_type`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_inf_basic_parameter`
--
DROP TABLE IF EXISTS `v_inf_basic_parameter`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_inf_basic_parameter` AS select `inf_cb`.`ID` AS `id`,`inf_cb`.`NAME` AS `name`,`inf_cb`.`DESCRIPTION` AS `description`,`inf_cb`.`VALUE` AS `value`,`inf_cb`.`BASIC_PARAMETER_TYPE_ID` AS `basic_parameter_type_id`,`inf_cbt`.`NAME` AS `basic_parameter_type_name`,`inf_cb`.`ACTIVE` AS `active`,1 AS `CAN_BE_DELETE` from (`inf_basic_parameter` `inf_cb` join `inf_basic_parameter_type` `inf_cbt`) where (`inf_cb`.`BASIC_PARAMETER_TYPE_ID` = `inf_cbt`.`ID`);

-- --------------------------------------------------------

--
-- Structure de la vue `v_inf_basic_parameter_type`
--
DROP TABLE IF EXISTS `v_inf_basic_parameter_type`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_inf_basic_parameter_type` AS select `inf_basic_parameter_type`.`ID` AS `ID`,`inf_basic_parameter_type`.`NAME` AS `NAME`,`inf_basic_parameter_type`.`DESCRIPTION` AS `DESCRIPTION`,`inf_basic_parameter_type`.`SORT_KEY` AS `SORT_KEY`,`inf_basic_parameter_type`.`ACTIVE` AS `ACTIVE`,`inf_basic_parameter_type`.`DATE_CREATION` AS `DATE_CREATION`,`inf_basic_parameter_type`.`USER_CREATION` AS `USER_CREATION`,`inf_basic_parameter_type`.`DATE_UPDATE` AS `DATE_UPDATE`,`inf_basic_parameter_type`.`USER_UPDATE` AS `USER_UPDATE`,NULL AS `CAN_BE_DELETE` from `inf_basic_parameter_type`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_inf_city`
--
DROP TABLE IF EXISTS `v_inf_city`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_inf_city` AS select `inf_city`.`ID` AS `ID`,`inf_city`.`CODE` AS `CODE`,`inf_city`.`NAME` AS `NAME`,`inf_city`.`DESCRIPTION` AS `DESCRIPTION`,`inf_city`.`SORT_KEY` AS `SORT_KEY`,`inf_city`.`COUNTRY_ID` AS `COUNTRY_ID`,`inf_city`.`ACTIVE` AS `ACTIVE`,`inf_city`.`DATE_CREATION` AS `DATE_CREATION`,`inf_city`.`USER_CREATION` AS `USER_CREATION`,`inf_city`.`DATE_UPDATE` AS `DATE_UPDATE`,`inf_city`.`USER_UPDATE` AS `USER_UPDATE` from `inf_city`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_inf_country`
--
DROP TABLE IF EXISTS `v_inf_country`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_inf_country` AS select `inf_country`.`ID` AS `ID`,`inf_country`.`CODE` AS `CODE`,`inf_country`.`NAME` AS `NAME`,`inf_country`.`DESCRIPTION` AS `DESCRIPTION`,`inf_country`.`SORT_KEY` AS `SORT_KEY`,`inf_country`.`ACTIVE` AS `ACTIVE`,`inf_country`.`DATE_CREATION` AS `DATE_CREATION`,`inf_country`.`USER_CREATION` AS `USER_CREATION`,`inf_country`.`DATE_UPDATE` AS `DATE_UPDATE`,`inf_country`.`USER_UPDATE` AS `USER_UPDATE` from `inf_country`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_inf_lovs`
--
DROP TABLE IF EXISTS `v_inf_lovs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_inf_lovs` AS select `inf_lovs`.`ID` AS `ID`,`inf_lovs`.`NAME` AS `NAME`,`inf_lovs`.`DESCRIPTION` AS `DESCRIPTION`,`inf_lovs`.`TABLE_PREFIX` AS `TABLE_PREFIX`,`inf_lovs`.`TABLE` AS `TABLE`,`inf_lovs`.`VIEW` AS `VIEW`,`inf_lovs`.`ITEM_CODE` AS `ITEM_CODE`,`inf_lovs`.`SORT_KEY` AS `SORT_KEY`,`inf_lovs`.`ACTIVE` AS `ACTIVE`,`inf_lovs`.`DATE_CREATION` AS `DATE_CREATION`,`inf_lovs`.`USER_CREATION` AS `USER_CREATION`,`inf_lovs`.`DATE_UPDATE` AS `DATE_UPDATE`,`inf_lovs`.`USER_UPDATE` AS `USER_UPDATE`,`inf_lovs`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`inf_lovs`.`ENTITY` AS `ENTITY` from `inf_lovs`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_inf_meta_model`
--
DROP TABLE IF EXISTS `v_inf_meta_model`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_inf_meta_model` AS select concat_ws('.',`INF_P`.`CODE`,`INF_L`.`CODE`,`INF_I`.`CODE`,convert(`INF_TT`.`CODE` using utf8)) AS `CODE`,`INF_P`.`ID` AS `PREFIX_ID`,`INF_P`.`CODE` AS `PREFIX_CODE`,`INF_I`.`CODE` AS `ITEM_CODE`,`INF_T`.`VALUE` AS `VALUE`,`INF_TT`.`ID` AS `TEXT_TYPE_ID`,`INF_TT`.`CODE` AS `TEXT_TYPE_CODE`,`INF_L`.`ID` AS `LANGUAGE_ID`,`INF_L`.`CODE` AS `LANGUAGE_CODE` from ((((`inf_prefix` `INF_P` join `inf_language` `INF_L`) join `inf_text_type` `INF_TT`) join `inf_item` `INF_I`) join `inf_text` `INF_T`) where ((`INF_T`.`PREFIX` = `INF_P`.`ID`) and (`INF_T`.`LANGUAGE_ID` = `INF_L`.`ID`) and (`INF_T`.`TEXT_TYPE_ID` = `INF_TT`.`ID`) and (`INF_T`.`ITEM_CODE` = `INF_I`.`CODE`) and (ucase(`INF_P`.`ACTIVE`) = 'Y') and (ucase(`INF_L`.`ACTIVE`) = 'Y') and (ucase(`INF_TT`.`ACTIVE`) = 'Y') and (ucase(`INF_I`.`ACTIVE`) = 'Y') and (ucase(`INF_T`.`ACTIVE`) = 'Y'));

-- --------------------------------------------------------

--
-- Structure de la vue `v_inf_privileges`
--
DROP TABLE IF EXISTS `v_inf_privileges`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_inf_privileges` AS select concat_ws('.',`clt_um`.`MODULE_ID`,`clt_u`.`ID`,`inf_i`.`CODE`) AS `CODE`,`inf_i`.`CODE` AS `ITEM_CODE`,`inf_r`.`ID` AS `ROLE_ID`,`inf_r`.`NAME` AS `ROLE_NAME`,`inf_g`.`ID` AS `GROUP_ID`,`inf_g`.`NAME` AS `GROUP_NAME`,`clt_u`.`ID` AS `USER_ID`,`clt_um`.`MODULE_ID` AS `MODULE_ID`,`clt_u`.`CLIENT_ID` AS `CLIENT_ID` from (((((((`inf_item` `inf_i` join `inf_privilege` `inf_p`) join `inf_role` `inf_r`) join `inf_role_group` `inf_rg`) join `inf_group` `inf_g`) join `clt_user_group` `clt_ug`) join `clt_user` `clt_u`) join `clt_user_module` `clt_um`) where ((`inf_i`.`CODE` = `inf_p`.`ITEM_CODE`) and (`inf_p`.`ROLE_ID` = `inf_r`.`ID`) and (`inf_r`.`ID` = `inf_rg`.`ROLE_ID`) and (`inf_rg`.`GROUP_ID` = `inf_g`.`ID`) and (`inf_g`.`ID` = `clt_ug`.`INF_GROUP_ID`) and (`clt_ug`.`USER_ID` = `clt_u`.`ID`) and (`clt_u`.`ID` = `clt_um`.`USER_ID`) and (`clt_um`.`MODULE_ID` = `inf_r`.`CLT_MODULE_ID`) and (`clt_um`.`MODULE_ID` = `inf_g`.`CLT_MODULE_ID`) and (ucase(`inf_i`.`ACTIVE`) = 'Y') and (ucase(`inf_p`.`ACTIVE`) = 'Y') and (ucase(`inf_r`.`ACTIVE`) = 'Y') and (ucase(`inf_rg`.`ACTIVE`) = 'Y') and (ucase(`inf_g`.`ACTIVE`) = 'Y') and (ucase(`clt_ug`.`ACTIVE`) = 'Y') and (ucase(`clt_u`.`ACTIVE`) = 'Y') and (ucase(`clt_um`.`ACTIVE`) = 'Y'));

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_attribute_date_type`
--
DROP TABLE IF EXISTS `v_pm_attribute_date_type`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pm_attribute_date_type` AS select concat_ws('.',3,`ppa`.`INF_ITEM_CODE`,4) AS `code`,`ppa`.`INF_ITEM_CODE` AS `inf_item_code`,`ppa`.`PAGE_ID` AS `page_id`,3 AS `clt_module_id`,`pvt`.`ID` AS `validation_id`,`pdt`.`PARAMS` AS `params`,`pdt`.`PARAM_NUMBER` AS `param_number`,`pdt`.`HELP` AS `help`,NULL AS `custom_error`,`pdt`.`ERROR_MESSAGE` AS `error_message` from ((`pm_page_attribute` `ppa` join `pm_data_type` `pdt`) join `pm_validation_type` `pvt`) where ((`ppa`.`DATA_TYPE_ID` = `pdt`.`ID`) and (ucase(`ppa`.`ACTIVE`) = convert(ucase('y') using latin1)) and (ucase(`pdt`.`ACTIVE`) = ucase('y')) and (`pvt`.`ID` = 4));

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_attribute_exclud`
--
DROP TABLE IF EXISTS `v_pm_attribute_exclud`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pm_attribute_exclud` AS select concat_ws('.',3,`pm_page_attribute`.`INF_ITEM_CODE`) AS `code`,`pm_page_attribute`.`INF_ITEM_CODE` AS `inf_item_code`,`pm_page_attribute`.`PAGE_ID` AS `page_id`,3 AS `clt_module_id` from `pm_page_attribute` where (ucase(`pm_page_attribute`.`ACTIVE`) = convert(ucase('y') using latin1));

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_attribute_format_type`
--
DROP TABLE IF EXISTS `v_pm_attribute_format_type`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pm_attribute_format_type` AS select concat_ws('.',3,`ppa`.`INF_ITEM_CODE`,5) AS `code`,`ppa`.`INF_ITEM_CODE` AS `inf_item_code`,`ppa`.`PAGE_ID` AS `page_id`,3 AS `clt_module_id`,`pvt`.`ID` AS `validation_id`,`pft`.`PARAMS` AS `params`,`pft`.`PARAM_NUMBER` AS `param_number`,`pft`.`HELP` AS `help`,NULL AS `custom_error`,`pft`.`ERROR_MESSAGE` AS `error_message` from ((`pm_page_attribute` `ppa` join `pm_format_type` `pft`) join `pm_validation_type` `pvt`) where ((`ppa`.`FORMAT_TYPE_ID` = `pft`.`ID`) and (ucase(`ppa`.`ACTIVE`) = convert(ucase('y') using latin1)) and (ucase(`pft`.`ACTIVE`) = ucase('y')) and (`pvt`.`ID` = 5));

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_attribute_hidden`
--
DROP TABLE IF EXISTS `v_pm_attribute_hidden`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pm_attribute_hidden` AS select concat_ws('.',1,`pm_page_attribute`.`INF_ITEM_CODE`) AS `code`,`pm_page_attribute`.`INF_ITEM_CODE` AS `inf_item_code`,`pm_page_attribute`.`PAGE_ID` AS `page_id`,1 AS `clt_module_id` from `pm_page_attribute` where ((ucase(`pm_page_attribute`.`IS_HIDDEN`) = ucase('y')) and (ucase(`pm_page_attribute`.`ACTIVE`) = convert(ucase('y') using latin1)));

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_attribute_max_length`
--
DROP TABLE IF EXISTS `v_pm_attribute_max_length`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pm_attribute_max_length` AS select concat_ws('.',1,`ppa`.`INF_ITEM_CODE`) AS `code`,`ppa`.`INF_ITEM_CODE` AS `inf_item_code`,`ppa`.`PAGE_ID` AS `page_id`,1 AS `clt_module_id`,`pvt`.`ID` AS `validation_id`,`ppa`.`MAX_LENGHT` AS `params`,`pvt`.`PARAM_NUMBER` AS `param_number`,`pvt`.`HELP` AS `help`,NULL AS `custom_error`,`pvt`.`ERROR_MESSAGE` AS `error_message` from (`pm_page_attribute` `ppa` join `pm_validation_type` `pvt`) where ((`ppa`.`MAX_LENGHT` is not null) and (`ppa`.`MAX_LENGHT` > 0) and (ucase(`ppa`.`ACTIVE`) = convert(ucase('y') using latin1)) and (ucase(`pvt`.`ACTIVE`) = convert(ucase('y') using latin1)) and (`pvt`.`ID` = 2));

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_attribute_max_word`
--
DROP TABLE IF EXISTS `v_pm_attribute_max_word`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pm_attribute_max_word` AS select concat_ws('.',1,`ppa`.`INF_ITEM_CODE`) AS `code`,`ppa`.`INF_ITEM_CODE` AS `inf_item_code`,`ppa`.`PAGE_ID` AS `page_id`,1 AS `clt_module_id`,`pvt`.`ID` AS `validation_id`,`ppa`.`MAX_WORD` AS `params`,`pvt`.`PARAM_NUMBER` AS `param_number`,`pvt`.`HELP` AS `help`,NULL AS `custom_error`,`pvt`.`ERROR_MESSAGE` AS `error_message` from (`pm_page_attribute` `ppa` join `pm_validation_type` `pvt`) where ((`ppa`.`MAX_WORD` is not null) and (`ppa`.`MAX_WORD` > 0) and (ucase(`ppa`.`ACTIVE`) = convert(ucase('y') using latin1)) and (ucase(`pvt`.`ACTIVE`) = convert(ucase('y') using latin1)) and (`pvt`.`ID` = 3));

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_attribute_readonly`
--
DROP TABLE IF EXISTS `v_pm_attribute_readonly`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pm_attribute_readonly` AS select concat_ws('.',1,`ppa`.`INF_ITEM_CODE`) AS `code`,`ppa`.`INF_ITEM_CODE` AS `inf_item_code`,`ppa`.`PAGE_ID` AS `page_id`,1 AS `clt_module_id` from `pm_page_attribute` `ppa` where ((ucase(`ppa`.`IS_READONLY`) = ucase('y')) and (ucase(`ppa`.`ACTIVE`) = convert(ucase('y') using latin1)));

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_attribute_required`
--
DROP TABLE IF EXISTS `v_pm_attribute_required`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pm_attribute_required` AS select concat_ws('.',1,`ppa`.`INF_ITEM_CODE`) AS `code`,`ppa`.`INF_ITEM_CODE` AS `inf_item_code`,`ppa`.`PAGE_ID` AS `page_id`,1 AS `clt_module_id`,`pvt`.`ID` AS `validation_id`,NULL AS `params`,`pvt`.`PARAM_NUMBER` AS `param_number`,`pvt`.`HELP` AS `help`,NULL AS `custom_error`,`pvt`.`ERROR_MESSAGE` AS `error_message` from (`pm_page_attribute` `ppa` join `pm_validation_type` `pvt`) where ((ucase(`ppa`.`IS_REQUIRED`) = ucase('y')) and (ucase(`ppa`.`ACTIVE`) = convert(ucase('y') using latin1)) and (ucase(`pvt`.`ACTIVE`) = convert(ucase('y') using latin1)) and (`pvt`.`ID` = 1));

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_attribute_validation`
--
DROP TABLE IF EXISTS `v_pm_attribute_validation`;
-- utilisé(#1356 - View 'mystock.v_pm_attribute_validation' references invalid table(s) or column(s) or function(s) or definer/invoker of view lack rights to use them)

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_categorys_dto`
--
DROP TABLE IF EXISTS `v_pm_categorys_dto`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pm_categorys_dto` AS select concat_ws('.',`pm_c`.`model_id`,`pm_c`.`group_id`,`pm_g`.`ID`) AS `code`,`pm_g`.`ID` AS `id`,`pm_g`.`NAME` AS `name`,`pm_g`.`IMAGE_PATH` AS `image_path`,`pm_g`.`DESCRIPTION` AS `description`,`pm_g`.`CATEGORY_TYPE_ID` AS `category_type_id`,`pm_c`.`category_sort` AS `category_sort`,`pm_c`.`model_id` AS `model_id`,`pm_c`.`group_id` AS `group_id` from (((`pm_category` `pm_g` join `pm_category_type` `mp_gt`) join `v_compos_cat` `pm_c`) join `pm_model` `pm_m`) where ((`pm_g`.`CATEGORY_TYPE_ID` = `mp_gt`.`ID`) and (`pm_g`.`ID` = `pm_c`.`category_id`) and (`pm_c`.`model_id` = `pm_m`.`ID`) and (ucase(`pm_g`.`ACTIVE`) = 'Y') and (ucase(`mp_gt`.`ACTIVE`) = 'Y') and (ucase(`pm_c`.`active`) = 'Y') and (ucase(`pm_m`.`ACTIVE`) = 'Y')) group by `pm_g`.`ID`,`pm_g`.`IMAGE_PATH`,`pm_g`.`NAME`,`pm_g`.`DESCRIPTION`,`pm_g`.`CATEGORY_TYPE_ID`,`pm_c`.`category_sort`,`pm_c`.`model_id`,`pm_c`.`group_id` order by `pm_c`.`category_sort`;

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_composition`
--
DROP TABLE IF EXISTS `v_pm_composition`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pm_composition` AS select `pm_composition`.`ID` AS `id`,`pm_composition`.`MODEL_ID` AS `model_id`,`pm_composition`.`GROUP_ID` AS `group_id`,`pm_composition`.`CATEGORY_ID` AS `category_id`,`pm_composition`.`MENU_ID` AS `menu_id`,`pm_composition`.`PAGE_ID` AS `page_id`,`pm_composition`.`INDEX_SHOW` AS `index_show`,`pm_composition`.`INDEX_SORT` AS `index_sort` from `pm_composition` where (ucase(`pm_composition`.`ACTIVE`) = convert(ucase('y') using latin1)) order by `pm_composition`.`MODEL_ID`,`pm_composition`.`GROUP_SORT`,`pm_composition`.`CATEGORY_SORT`,`pm_composition`.`MENU_SORT`,`pm_composition`.`PAGE_SORT`;

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_compos_details`
--
DROP TABLE IF EXISTS `v_pm_compos_details`;
-- utilisé(#1356 - View 'mystock.v_pm_compos_details' references invalid table(s) or column(s) or function(s) or definer/invoker of view lack rights to use them)

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_groups_dto`
--
DROP TABLE IF EXISTS `v_pm_groups_dto`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pm_groups_dto` AS select concat_ws('.',`pm_c`.`MODEL_ID`,`pm_g`.`ID`) AS `code`,`pm_g`.`ID` AS `id`,`pm_g`.`NAME` AS `name`,`pm_g`.`DESCRIPTION` AS `description`,`pm_g`.`GROUP_TYPE_ID` AS `group_type_id`,`pm_g`.`SORT_KEY` AS `sort_key`,`pm_c`.`MODEL_ID` AS `model_id` from (((`pm_group` `pm_g` join `pm_group_type` `mp_gt`) join `pm_composition` `pm_c`) join `pm_model` `pm_m`) where ((`pm_g`.`GROUP_TYPE_ID` = `mp_gt`.`ID`) and (`pm_g`.`ID` = `pm_c`.`GROUP_ID`) and (`pm_c`.`MODEL_ID` = `pm_m`.`ID`) and (ucase(`pm_g`.`ACTIVE`) = 'y') and (ucase(`mp_gt`.`ACTIVE`) = 'y') and (ucase(`pm_c`.`ACTIVE`) = 'y') and (ucase(`pm_m`.`ACTIVE`) = 'y')) group by `pm_g`.`ID`,`pm_g`.`NAME`,`pm_g`.`DESCRIPTION`,`pm_g`.`GROUP_TYPE_ID`,`pm_g`.`SORT_KEY`,`pm_c`.`MODEL_ID`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_menus_dto`
--
DROP TABLE IF EXISTS `v_pm_menus_dto`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pm_menus_dto` AS select concat_ws('.',`pm_c`.`MODEL_ID`,`pm_c`.`GROUP_ID`,`pm_c`.`CATEGORY_ID`,`pm_m`.`ID`) AS `code`,`pm_m`.`ID` AS `id`,`pm_m`.`NAME` AS `name`,`pm_m`.`IMAGE_PATH` AS `image_path`,`pm_m`.`DESCRIPTION` AS `description`,`pm_m`.`MENU_TYPE_ID` AS `menu_type_id`,`pm_c`.`MENU_SORT` AS `menu_sort`,`pm_c`.`MODEL_ID` AS `model_id`,`pm_c`.`GROUP_ID` AS `group_id`,`pm_c`.`CATEGORY_ID` AS `category_id` from (((`pm_menu` `pm_m` join `pm_menu_type` `mp_mt`) join `pm_composition` `pm_c`) join `pm_model` `pm_mo`) where ((`pm_m`.`MENU_TYPE_ID` = `mp_mt`.`ID`) and (`pm_m`.`ID` = `pm_c`.`MENU_ID`) and (`pm_c`.`MODEL_ID` = `pm_mo`.`ID`) and (ucase(`pm_m`.`ACTIVE`) = 'y') and (ucase(`mp_mt`.`ACTIVE`) = 'y') and (ucase(`pm_c`.`ACTIVE`) = 'y') and (ucase(`pm_mo`.`ACTIVE`) = 'y')) group by `pm_m`.`ID`,`pm_m`.`NAME`,`pm_m`.`IMAGE_PATH`,`pm_m`.`DESCRIPTION`,`pm_m`.`MENU_TYPE_ID`,`pm_c`.`MENU_SORT`,`pm_c`.`MODEL_ID`,`pm_c`.`GROUP_ID`,`pm_c`.`CATEGORY_ID`;

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_model_dto`
--
DROP TABLE IF EXISTS `v_pm_model_dto`;
-- utilisé(#1356 - View 'mystock.v_pm_model_dto' references invalid table(s) or column(s) or function(s) or definer/invoker of view lack rights to use them)

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_page`
--
DROP TABLE IF EXISTS `v_pm_page`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pm_page` AS select `pm_page`.`ID` AS `ID`,`pm_page`.`NAME` AS `NAME`,`pm_page`.`DESCRIPTION` AS `DESCRIPTION`,`pm_page`.`SORT_KEY` AS `SORT_KEY`,`pm_page`.`PAGE_TYPE_ID` AS `PAGE_TYPE_ID`,`pm_page`.`IN_DEV` AS `IN_DEV`,`pm_page`.`ACTIVE` AS `ACTIVE`,`pm_page`.`USER_CREATION` AS `USER_CREATION`,`pm_page`.`DATE_CREATION` AS `DATE_CREATION`,`pm_page`.`USER_UPDATE` AS `USER_UPDATE`,`pm_page`.`DATE_UPDATE` AS `DATE_UPDATE` from `pm_page`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_pages_dto`
--
DROP TABLE IF EXISTS `v_pm_pages_dto`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pm_pages_dto` AS select concat_ws('.',`pm_c`.`MODEL_ID`,`pm_c`.`GROUP_ID`,`pm_c`.`CATEGORY_ID`,`pm_c`.`MENU_ID`,`pm_p`.`ID`) AS `code`,`pm_p`.`ID` AS `id`,`pm_p`.`NAME` AS `name`,`pm_p`.`DESCRIPTION` AS `description`,`pm_p`.`IN_DEV` AS `in_dev`,`pm_p`.`PAGE_TYPE_ID` AS `page_type_id`,`pm_c`.`PAGE_SORT` AS `page_sort`,`pm_c`.`MODEL_ID` AS `model_id`,`pm_c`.`GROUP_ID` AS `group_id`,`pm_c`.`CATEGORY_ID` AS `category_id`,`pm_c`.`MENU_ID` AS `menu_id` from (((`pm_page` `pm_p` join `pm_page_type` `mp_pt`) join `pm_composition` `pm_c`) join `pm_model` `pm_m`) where ((`pm_p`.`PAGE_TYPE_ID` = `mp_pt`.`ID`) and (`pm_p`.`ID` = `pm_c`.`PAGE_ID`) and (`pm_c`.`MODEL_ID` = `pm_m`.`ID`) and (ucase(`pm_p`.`ACTIVE`) = 'y') and (ucase(`mp_pt`.`ACTIVE`) = 'y') and (ucase(`pm_c`.`ACTIVE`) = 'y') and (ucase(`pm_m`.`ACTIVE`) = 'y')) group by `pm_p`.`ID`,`pm_p`.`NAME`,`pm_p`.`DESCRIPTION`,`pm_p`.`IN_DEV`,`pm_p`.`PAGE_TYPE_ID`,`pm_c`.`PAGE_SORT`,`pm_c`.`MODEL_ID`,`pm_c`.`GROUP_ID`,`pm_c`.`CATEGORY_ID`,`pm_c`.`MENU_ID`;

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_page_attribute`
--
DROP TABLE IF EXISTS `v_pm_page_attribute`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pm_page_attribute` AS select `pm_page_attribute`.`ID` AS `ID`,`pm_page_attribute`.`PAGE_ID` AS `PAGE_ID`,`pm_page_attribute`.`INF_ITEM_CODE` AS `INF_ITEM_CODE`,`pm_page_attribute`.`PM_COMPONENT_ID` AS `PM_COMPONENT_ID`,`pm_page_attribute`.`SORT_KEY` AS `SORT_KEY`,`pm_page_attribute`.`ACTIVE` AS `ACTIVE`,`pm_page_attribute`.`IS_REQUIRED` AS `IS_REQUIRED`,`pm_page_attribute`.`IS_READONLY` AS `IS_READONLY`,`pm_page_attribute`.`IS_HIDDEN` AS `IS_HIDDEN`,`pm_page_attribute`.`DATA_TYPE_ID` AS `DATA_TYPE_ID`,`pm_page_attribute`.`FORMAT_TYPE_ID` AS `FORMAT_TYPE_ID`,`pm_page_attribute`.`MAX_LENGHT` AS `MAX_LENGHT`,`pm_page_attribute`.`MAX_WORD` AS `MAX_WORD`,`pm_page_attribute`.`DATE_CREATION` AS `DATE_CREATION`,`pm_page_attribute`.`DATE_UPDATE` AS `DATE_UPDATE`,`pm_page_attribute`.`USER_CREATION` AS `USER_CREATION`,`pm_page_attribute`.`USER_UPDATE` AS `USER_UPDATE` from `pm_page_attribute`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_page_attribute_module`
--
DROP TABLE IF EXISTS `v_pm_page_attribute_module`;

CREATE ALGORITHM=UNDEFINED DEFINER=`mystock`@`%` SQL SECURITY DEFINER VIEW `v_pm_page_attribute_module` AS (select concat_ws('.',`CLT_M`.`ID`,`PM_PA`.`ID`) AS `CODE`,`CLT_M`.`ID` AS `CLT_MODULE_ID`,`PM_PA`.`ID` AS `PAGE_ATTRIBUTE_ID`,'Y' AS `ACTIVE`,`PM_PA`.`PAGE_ID` AS `PAGE_ID`,`PM_PA`.`INF_ITEM_CODE` AS `INF_ITEM_CODE`,`PM_PA`.`PM_COMPONENT_ID` AS `PM_COMPONENT_ID`,coalesce(coalesce((select `PM_PAMODULE`.`IS_REQUIRED` from `pm_page_attribute_module` `PM_PAMODULE` where ((`PM_PAMODULE`.`CLT_MODULE_ID` = `CLT_M`.`ID`) and (`PM_PAMODULE`.`PAGE_ATTRIBUTE_ID` = `PM_PA`.`ID`) and (ucase(`PM_PAMODULE`.`ACTIVE`) = 'Y'))),(select `PM_PAMODEL`.`IS_REQUIRED` from `pm_page_attribute_model` `PM_PAMODEL` where ((`PM_PAMODEL`.`MODEL_ID` = `CLT_M`.`PM_MODEL_ID`) and (`PM_PAMODEL`.`PAGE_ATTRIBUTE_ID` = `PM_PA`.`ID`) and (ucase(`PM_PAMODEL`.`ACTIVE`) = 'Y')))),`PM_PA`.`IS_REQUIRED`) AS `IS_REQUIRED`,coalesce(coalesce((select `PM_PAMODULE`.`IS_READONLY` from `pm_page_attribute_module` `PM_PAMODULE` where ((`PM_PAMODULE`.`CLT_MODULE_ID` = `CLT_M`.`ID`) and (`PM_PAMODULE`.`PAGE_ATTRIBUTE_ID` = `PM_PA`.`ID`) and (ucase(`PM_PAMODULE`.`ACTIVE`) = 'Y'))),(select `PM_PAMODEL`.`IS_READONLY` from `pm_page_attribute_model` `PM_PAMODEL` where ((`PM_PAMODEL`.`MODEL_ID` = `CLT_M`.`PM_MODEL_ID`) and (`PM_PAMODEL`.`PAGE_ATTRIBUTE_ID` = `PM_PA`.`ID`) and (ucase(`PM_PAMODEL`.`ACTIVE`) = 'Y')))),`PM_PA`.`IS_READONLY`) AS `IS_READONLY`,coalesce(coalesce((select `PM_PAMODULE`.`IS_HIDDEN` from `pm_page_attribute_module` `PM_PAMODULE` where ((`PM_PAMODULE`.`CLT_MODULE_ID` = `CLT_M`.`ID`) and (`PM_PAMODULE`.`PAGE_ATTRIBUTE_ID` = `PM_PA`.`ID`) and (ucase(`PM_PAMODULE`.`ACTIVE`) = 'Y'))),(select `PM_PAMODEL`.`IS_HIDDEN` from `pm_page_attribute_model` `PM_PAMODEL` where ((`PM_PAMODEL`.`MODEL_ID` = `CLT_M`.`PM_MODEL_ID`) and (`PM_PAMODEL`.`PAGE_ATTRIBUTE_ID` = `PM_PA`.`ID`) and (ucase(`PM_PAMODEL`.`ACTIVE`) = 'Y')))),`PM_PA`.`IS_HIDDEN`) AS `IS_HIDDEN`,coalesce(coalesce((select `PM_PAMODULE`.`DATA_TYPE_ID` from `pm_page_attribute_module` `PM_PAMODULE` where ((`PM_PAMODULE`.`CLT_MODULE_ID` = `CLT_M`.`ID`) and (`PM_PAMODULE`.`PAGE_ATTRIBUTE_ID` = `PM_PA`.`ID`) and (ucase(`PM_PAMODULE`.`ACTIVE`) = 'Y'))),(select `PM_PAMODEL`.`DATA_TYPE_ID` from `pm_page_attribute_model` `PM_PAMODEL` where ((`PM_PAMODEL`.`MODEL_ID` = `CLT_M`.`PM_MODEL_ID`) and (`PM_PAMODEL`.`PAGE_ATTRIBUTE_ID` = `PM_PA`.`ID`) and (ucase(`PM_PAMODEL`.`ACTIVE`) = 'Y')))),`PM_PA`.`DATA_TYPE_ID`) AS `DATA_TYPE_ID`,coalesce(coalesce((select `PM_PAMODULE`.`FORMAT_TYPE_ID` from `pm_page_attribute_module` `PM_PAMODULE` where ((`PM_PAMODULE`.`CLT_MODULE_ID` = `CLT_M`.`ID`) and (`PM_PAMODULE`.`PAGE_ATTRIBUTE_ID` = `PM_PA`.`ID`) and (ucase(`PM_PAMODULE`.`ACTIVE`) = 'Y'))),(select `PM_PAMODEL`.`FORMAT_TYPE_ID` from `pm_page_attribute_model` `PM_PAMODEL` where ((`PM_PAMODEL`.`MODEL_ID` = `CLT_M`.`PM_MODEL_ID`) and (`PM_PAMODEL`.`PAGE_ATTRIBUTE_ID` = `PM_PA`.`ID`) and (ucase(`PM_PAMODEL`.`ACTIVE`) = 'Y')))),`PM_PA`.`FORMAT_TYPE_ID`) AS `FORMAT_TYPE_ID`,coalesce(coalesce((select `PM_PAMODULE`.`MAX_LENGHT` from `pm_page_attribute_module` `PM_PAMODULE` where ((`PM_PAMODULE`.`CLT_MODULE_ID` = `CLT_M`.`ID`) and (`PM_PAMODULE`.`PAGE_ATTRIBUTE_ID` = `PM_PA`.`ID`) and (ucase(`PM_PAMODULE`.`ACTIVE`) = 'Y'))),(select `PM_PAMODEL`.`MAX_LENGHT` from `pm_page_attribute_model` `PM_PAMODEL` where ((`PM_PAMODEL`.`MODEL_ID` = `CLT_M`.`PM_MODEL_ID`) and (`PM_PAMODEL`.`PAGE_ATTRIBUTE_ID` = `PM_PA`.`ID`) and (ucase(`PM_PAMODEL`.`ACTIVE`) = 'Y')))),`PM_PA`.`MAX_LENGHT`) AS `MAX_LENGHT`,coalesce(coalesce((select `PM_PAMODULE`.`MAX_WORD` from `pm_page_attribute_module` `PM_PAMODULE` where ((`PM_PAMODULE`.`CLT_MODULE_ID` = `CLT_M`.`ID`) and (`PM_PAMODULE`.`PAGE_ATTRIBUTE_ID` = `PM_PA`.`ID`) and (ucase(`PM_PAMODULE`.`ACTIVE`) = 'Y'))),(select `PM_PAMODEL`.`MAX_WORD` from `pm_page_attribute_model` `PM_PAMODEL` where ((`PM_PAMODEL`.`MODEL_ID` = `CLT_M`.`PM_MODEL_ID`) and (`PM_PAMODEL`.`PAGE_ATTRIBUTE_ID` = `PM_PA`.`ID`) and (ucase(`PM_PAMODEL`.`ACTIVE`) = 'Y')))),`PM_PA`.`MAX_WORD`) AS `MAX_WORD` from (`clt_module` `CLT_M` join `pm_page_attribute` `PM_PA`));

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_page_module`
--
DROP TABLE IF EXISTS `v_pm_page_module`;
-- utilisé(#1356 - View 'mystock.v_pm_page_module' references invalid table(s) or column(s) or function(s) or definer/invoker of view lack rights to use them)

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_page_parameter`
--
DROP TABLE IF EXISTS `v_pm_page_parameter`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pm_page_parameter` AS select `pm_pg_par`.`ID` AS `id`,`pm_pg_par`.`NAME` AS `name`,`pm_pg_par`.`DESCRIPTION` AS `description`,`pm_pg_par`.`DEFAULT_VALUE` AS `DEFAULT_VALUE`,`pm_pg_par`.`PAGE_PARAMETER_TYPE_ID` AS `PAGE_PARAMETER_TYPE_ID`,`pm_pg_par_type`.`NAME` AS `pm_page_parameter_type_name`,`pm_pg_par`.`PAGE_ID` AS `PAGE_ID`,`pp`.`NAME` AS `pm_page_name`,`pm_pg_par`.`ACTIVE` AS `active`,'false' AS `can_be_delete` from ((`pm_page_parameter` `pm_pg_par` join `pm_page_parameter_type` `pm_pg_par_type`) join `pm_page` `pp`) where ((`pm_pg_par`.`PAGE_PARAMETER_TYPE_ID` = `pm_pg_par_type`.`ID`) and (`pm_pg_par`.`PAGE_ID` = `pp`.`ID`));

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_page_parameter_module`
--
DROP TABLE IF EXISTS `v_pm_page_parameter_module`;

CREATE ALGORITHM=UNDEFINED DEFINER=`mystock`@`%` SQL SECURITY DEFINER VIEW `v_pm_page_parameter_module` AS (select concat_ws('.',`CLT_M`.`ID`,`PM_PP`.`ID`) AS `CODE`,`CLT_M`.`ID` AS `CLT_MODULE_ID`,`PM_PP`.`ID` AS `PAGE_PARAMETER_ID`,`PM_PPT`.`NAME` AS `PAGE_PARAMETER_TYPE_NAME`,'Y' AS `ACTIVE`,`PM_PP`.`NAME` AS `PAGE_PARAMETER_NAME`,coalesce(coalesce((select `PM_PPMODULE`.`VALUE` from `pm_page_parameter_module` `PM_PPMODULE` where ((`PM_PPMODULE`.`CLT_MODULE_ID` = `CLT_M`.`ID`) and (`PM_PPMODULE`.`PAGE_PARAMETER_ID` = `PM_PP`.`ID`) and (ucase(`PM_PPMODULE`.`ACTIVE`) = 'Y'))),(select `PM_PPMODEL`.`VALUE` from `pm_page_parameter_model` `PM_PPMODEL` where ((`PM_PPMODEL`.`MODEL_ID` = `CLT_M`.`PM_MODEL_ID`) and (`PM_PPMODEL`.`PAGE_PARAMETER_ID` = `PM_PP`.`ID`) and (ucase(`PM_PPMODEL`.`ACTIVE`) = 'Y')))),`PM_PP`.`DEFAULT_VALUE`) AS `VALUE` from ((`clt_module` `CLT_M` join `pm_page_parameter` `PM_PP`) join `pm_page_parameter_type` `PM_PPT`) where (`PM_PP`.`PAGE_PARAMETER_TYPE_ID` = `PM_PPT`.`ID`));

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_page_parameter_type`
--
DROP TABLE IF EXISTS `v_pm_page_parameter_type`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pm_page_parameter_type` AS select `pm_page_parameter_type`.`ID` AS `ID`,`pm_page_parameter_type`.`NAME` AS `NAME`,`pm_page_parameter_type`.`DESCRIPTION` AS `DESCRIPTION`,`pm_page_parameter_type`.`SORT_KEY` AS `SORT_KEY`,`pm_page_parameter_type`.`ACTIVE` AS `ACTIVE`,`pm_page_parameter_type`.`DATE_CREATION` AS `DATE_CREATION`,`pm_page_parameter_type`.`USER_CREATION` AS `USER_CREATION`,`pm_page_parameter_type`.`DATE_UPDATE` AS `DATE_UPDATE`,`pm_page_parameter_type`.`USER_UPDATE` AS `USER_UPDATE` from `pm_page_parameter_type`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_pdf`
--
DROP TABLE IF EXISTS `v_pm_pdf`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pm_pdf` AS select `pm_p`.`ID` AS `ID`,`pm_p`.`NAME` AS `NAME`,`pm_p`.`DESCRIPTION` AS `DESCRIPTION`,`pm_p`.`SORT_KEY` AS `SORT_KEY`,`pm_p`.`PDF_TYPE_ID` AS `PDF_TYPE_ID`,`pm_p`.`IN_DEV` AS `IN_DEV`,`pm_p`.`ACTIVE` AS `ACTIVE`,`pm_p`.`USER_CREATION` AS `USER_CREATION`,`pm_p`.`DATE_CREATION` AS `DATE_CREATION`,`pm_p`.`USER_UPDATE` AS `USER_UPDATE`,`pm_p`.`DATE_UPDATE` AS `DATE_UPDATE`,`pm_pt`.`NAME` AS `pdf_type_name` from (`pm_pdf` `pm_p` left join `pm_pdf_type` `pm_pt` on((`pm_p`.`PDF_TYPE_ID` = `pm_pt`.`ID`)))

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_pdf_parameter_module`
--
DROP TABLE IF EXISTS `v_pm_pdf_parameter_module`;

CREATE ALGORITHM=UNDEFINED DEFINER=`mystock`@`%` SQL SECURITY DEFINER VIEW `v_pm_pdf_parameter_module` AS (select concat_ws('.',`clt_m`.`ID`,`pm_pp`.`ID`) AS `CODE`,`clt_m`.`ID` AS `CLT_MODULE_ID`,`pm_pp`.`ID` AS `PDF_PARAMETER_ID`,`pm_ppt`.`NAME` AS `PDF_PARAMETER_TYPE_NAME`,'Y' AS `ACTIVE`,`pm_pp`.`NAME` AS `PDF_PARAMETER_NAME`,coalesce((select `pm_ppm`.`VALUE` from `pm_pdf_parameter_module` `PM_PPM` where ((`pm_ppm`.`CLT_MODULE_ID` = `clt_m`.`ID`) and (`pm_ppm`.`PDF_PARAMETER_ID` = `pm_pp`.`ID`) and (ucase(`pm_ppm`.`ACTIVE`) = 'Y'))),`pm_pp`.`DEFAULT_VALUE`) AS `VALUE` from ((`clt_module` `CLT_M` join `pm_pdf_parameter` `PM_PP`) join `pm_pdf_parameter_type` `PM_PPT`) where (`pm_pp`.`PDF_PARAMETER_TYPE_ID` = `pm_ppt`.`ID`));

-- --------------------------------------------------------

--
-- Structure de la vue `v_pm_validation`
--
DROP TABLE IF EXISTS `v_pm_validation`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_pm_validation` AS select `v_pm_attribute_required`.`code` AS `code`,`v_pm_attribute_required`.`page_id` AS `page_id`,`v_pm_attribute_required`.`inf_item_code` AS `item_code`,`v_pm_attribute_required`.`validation_id` AS `validation_id`,`v_pm_attribute_required`.`params` AS `params`,`v_pm_attribute_required`.`param_number` AS `param_number`,`v_pm_attribute_required`.`help` AS `help`,`v_pm_attribute_required`.`custom_error` AS `custom_error`,`v_pm_attribute_required`.`clt_module_id` AS `clt_module_id`,`v_pm_attribute_required`.`error_message` AS `error_message` from `v_pm_attribute_required` union select `v_pm_attribute_max_length`.`code` AS `code`,`v_pm_attribute_max_length`.`page_id` AS `page_id`,`v_pm_attribute_max_length`.`inf_item_code` AS `item_code`,`v_pm_attribute_max_length`.`validation_id` AS `validation_id`,`v_pm_attribute_max_length`.`params` AS `params`,`v_pm_attribute_max_length`.`param_number` AS `param_number`,`v_pm_attribute_max_length`.`help` AS `help`,`v_pm_attribute_max_length`.`custom_error` AS `custom_error`,`v_pm_attribute_max_length`.`clt_module_id` AS `clt_module_id`,`v_pm_attribute_max_length`.`error_message` AS `error_message` from `v_pm_attribute_max_length` union select `v_pm_attribute_max_word`.`code` AS `code`,`v_pm_attribute_max_word`.`page_id` AS `page_id`,`v_pm_attribute_max_word`.`inf_item_code` AS `item_code`,`v_pm_attribute_max_word`.`validation_id` AS `validation_id`,`v_pm_attribute_max_word`.`params` AS `params`,`v_pm_attribute_max_word`.`param_number` AS `param_number`,`v_pm_attribute_max_word`.`help` AS `help`,`v_pm_attribute_max_word`.`custom_error` AS `custom_error`,`v_pm_attribute_max_word`.`clt_module_id` AS `clt_module_id`,`v_pm_attribute_max_word`.`error_message` AS `error_message` from `v_pm_attribute_max_word` union select `v_pm_attribute_date_type`.`code` AS `code`,`v_pm_attribute_date_type`.`page_id` AS `page_id`,`v_pm_attribute_date_type`.`inf_item_code` AS `item_code`,`v_pm_attribute_date_type`.`validation_id` AS `validation_id`,`v_pm_attribute_date_type`.`params` AS `params`,`v_pm_attribute_date_type`.`param_number` AS `param_number`,`v_pm_attribute_date_type`.`help` AS `help`,`v_pm_attribute_date_type`.`custom_error` AS `custom_error`,`v_pm_attribute_date_type`.`clt_module_id` AS `clt_module_id`,`v_pm_attribute_date_type`.`error_message` AS `error_message` from `v_pm_attribute_date_type` union select `v_pm_attribute_format_type`.`code` AS `code`,`v_pm_attribute_format_type`.`page_id` AS `page_id`,`v_pm_attribute_format_type`.`inf_item_code` AS `item_code`,`v_pm_attribute_format_type`.`validation_id` AS `validation_id`,`v_pm_attribute_format_type`.`params` AS `params`,`v_pm_attribute_format_type`.`param_number` AS `param_number`,`v_pm_attribute_format_type`.`help` AS `help`,`v_pm_attribute_format_type`.`custom_error` AS `custom_error`,`v_pm_attribute_format_type`.`clt_module_id` AS `clt_module_id`,`v_pm_attribute_format_type`.`error_message` AS `error_message` from `v_pm_attribute_format_type`;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_color_by_reference`
--
DROP TABLE IF EXISTS `v_sm_color_by_reference`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_color_by_reference` AS select concat_ws('.',`sm_p`.`CLT_MODULE_ID`,`sm_p`.`REFERENCE`,`sm_pc`.`ID`) AS `CODE`,`sm_p`.`REFERENCE` AS `REFERENCE`,`sm_pc`.`ID` AS `PRODUCT_COLOR_ID`,`sm_pc`.`NAME` AS `PRODUCT_COLOR_NAME`,`sm_p`.`CLT_MODULE_ID` AS `CLT_MODULE_ID` from (`sm_product` `sm_p` join `sm_product_color` `sm_pc`) where (`sm_p`.`PRODUCT_COLOR_ID` = `sm_pc`.`ID`) group by `sm_p`.`REFERENCE`,`sm_pc`.`ID`,`sm_pc`.`NAME`,`sm_p`.`CLT_MODULE_ID`;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_customer`
--
DROP TABLE IF EXISTS `v_sm_customer`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_customer` AS select `SM_C`.`ID` AS `ID`,`SM_C`.`FIRST_NAME` AS `FIRST_NAME`,`SM_C`.`LAST_NAME` AS `LAST_NAME`,`SM_C`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`SM_C`.`CUSTOMER_TYPE_ID` AS `CUSTOMER_TYPE_ID`,`SM_C`.`CUSTOMER_CATEGORY_ID` AS `CUSTOMER_CATEGORY_ID`,`SM_C`.`SORT_KEY` AS `SORT_KEY`,`SM_C`.`ACTIVE` AS `ACTIVE`,`SM_C`.`SHORT_LABEL` AS `SHORT_LABEL`,`SM_C`.`FULL_LABEL` AS `FULL_LABEL`,`SM_C`.`NOTE` AS `NOTE`,`SM_C`.`COMPANY_NAME` AS `COMPANY_NAME`,`SM_C`.`REPRESENTATIVE` AS `REPRESENTATIVE`,`SM_C`.`IDENTIFICATION` AS `IDENTIFICATION`,`SM_C`.`DATE_CREATION` AS `DATE_CREATION`,`SM_C`.`USER_CREATION` AS `USER_CREATION`,`SM_C`.`DATE_UPDATE` AS `DATE_UPDATE`,`SM_C`.`USER_UPDATE` AS `USER_UPDATE`,`SM_C`.`CUSTOMER_NATURE_ID` AS `CUSTOMER_NATURE_ID`,`SM_C`.`ENTITY_ID` AS `ENTITY_ID`,`SM_CT`.`NAME` AS `CUSTOMER_TYPE_NAME`,`SM_CC`.`NAME` AS `CUSTOMER_CATEGORY_NAME`,`vcta_ec`.`ENTITY_PHONE` AS `ENTITY_PHONE`,`vcta_ec`.`ENTITY_EMAIL` AS `ENTITY_EMAIL`,`vcta_ec`.`ENTITY_FAX` AS `ENTITY_FAX`,`vcta_ec`.`ENTITY_WEB` AS `ENTITY_WEB`,`vcta_ec`.`ENTITY_LOCATION_ADDRESS` AS `ENTITY_LOCATION_ADDRESS`,`vcta_ec`.`ENTITY_LOCATION_CITY_NAME` AS `ENTITY_LOCATION_CITY_NAME`,`vcta_ec`.`ENTITY_LOCATION_COUNTRY_NAME` AS `ENTITY_LOCATION_COUNTRY_NAME`,(select (case when (count(0) > 0) then 'N' else 'Y' end) from `sm_order` `O` where (`O`.`CUSTOMER_ID` = `SM_C`.`ID`)) AS `CAN_BE_BELETE` from (((`sm_customer` `SM_C` left join `sm_customer_type` `SM_CT` on((`SM_C`.`CUSTOMER_TYPE_ID` = `SM_CT`.`ID`))) left join `sm_customer_category` `SM_CC` on((`SM_C`.`CUSTOMER_CATEGORY_ID` = `SM_CC`.`ID`))) left join `v_cta_entity_contact` `VCTA_EC` on((`SM_C`.`ENTITY_ID` = `vcta_ec`.`ID`)))

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_customer_category`
--
DROP TABLE IF EXISTS `v_sm_customer_category`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_customer_category` AS select `sm_customer_category`.`ID` AS `ID`,`sm_customer_category`.`NAME` AS `NAME`,`sm_customer_category`.`DESCRIPTION` AS `DESCRIPTION`,`sm_customer_category`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`sm_customer_category`.`SORT_KEY` AS `SORT_KEY`,`sm_customer_category`.`ACTIVE` AS `ACTIVE`,`sm_customer_category`.`DATE_CREATION` AS `DATE_CREATION`,`sm_customer_category`.`USER_CREATION` AS `USER_CREATION`,`sm_customer_category`.`DATE_UPDATE` AS `DATE_UPDATE`,`sm_customer_category`.`USER_UPDATE` AS `USER_UPDATE` from `sm_customer_category`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_customer_type`
--
DROP TABLE IF EXISTS `v_sm_customer_type`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_customer_type` AS select `sm_customer_type`.`ID` AS `ID`,`sm_customer_type`.`NAME` AS `NAME`,`sm_customer_type`.`DESCRIPTION` AS `DESCRIPTION`,`sm_customer_type`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`sm_customer_type`.`SORT_KEY` AS `SORT_KEY`,`sm_customer_type`.`ACTIVE` AS `ACTIVE`,`sm_customer_type`.`DATE_CREATION` AS `DATE_CREATION`,`sm_customer_type`.`USER_CREATION` AS `USER_CREATION`,`sm_customer_type`.`DATE_UPDATE` AS `DATE_UPDATE`,`sm_customer_type`.`USER_UPDATE` AS `USER_UPDATE` from `sm_customer_type`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_deposit`
--
DROP TABLE IF EXISTS `v_sm_deposit`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_deposit` AS select `sm_deposit`.`ID` AS `ID`,`sm_deposit`.`NAME` AS `NAME`,`sm_deposit`.`DESCRIPTION` AS `DESCRIPTION`,`sm_deposit`.`SORT_KEY` AS `SORT_KEY`,`sm_deposit`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`sm_deposit`.`ACTIVE` AS `ACTIVE`,`sm_deposit`.`DATE_CREATION` AS `DATE_CREATION`,`sm_deposit`.`USER_CREATION` AS `USER_CREATION`,`sm_deposit`.`DATE_UPDATE` AS `DATE_UPDATE`,`sm_deposit`.`USER_UPDATE` AS `USER_UPDATE` from `sm_deposit`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_expense`
--
DROP TABLE IF EXISTS `v_sm_expense`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_expense` AS select `t`.`ID` AS `ID`,`t`.`NAME` AS `NAME`,`t`.`DESCRIPTION` AS `DESCRIPTION`,`t`.`AMOUNT` AS `AMOUNT`,`t`.`EXPENSE_TYPE_ID` AS `EXPENSE_TYPE_ID`,`t`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`t`.`SORT_KEY` AS `SORT_KEY`,`t`.`ACTIVE` AS `ACTIVE`,`t`.`DATE_CREATION` AS `DATE_CREATION`,`t`.`USER_CREATION` AS `USER_CREATION`,`t`.`DATE_UPDATE` AS `DATE_UPDATE`,`t`.`USER_UPDATE` AS `USER_UPDATE`,`s`.`NAME` AS `EXPENSE_TYPE_NAME`,`c`.`NAME` AS `CLT_MODULE_NAME` from ((`sm_expense` `t` join `sm_expense_type` `s`) join `clt_module` `c`) where ((`t`.`EXPENSE_TYPE_ID` = `s`.`ID`) and (`t`.`CLT_MODULE_ID` = `c`.`ID`))

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_expense_type`
--
DROP TABLE IF EXISTS `v_sm_expense_type`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_expense_type` AS select `sm_expense_type`.`ID` AS `ID`,`sm_expense_type`.`NAME` AS `NAME`,`sm_expense_type`.`DESCRIPTION` AS `DESCRIPTION`,`sm_expense_type`.`ACTIVE` AS `ACTIVE`,`sm_expense_type`.`SORT_KEY` AS `SORT_KEY`,`sm_expense_type`.`CLT_MODULE_ID` AS `CLT_MODULE_ID` from `sm_expense_type`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_inventaire`
--
DROP TABLE IF EXISTS `v_sm_inventaire`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_inventaire` AS select `sm_p`.`ID` AS `id`,`sm_p`.`REFERENCE` AS `reference`,`sm_pc`.`NAME` AS `product_color_name`,`sm_ps`.`NAME` AS `product_size_name`,`sm_p`.`DESIGNATION` AS `designation`,`sm_p`.`QUANTITY` AS `quantity`,`sm_p`.`PRICE_BUY` AS `price_buy`,`sm_p`.`PRICE_SALE` AS `price_sale`,`sm_p`.`ACTIVE` AS `active`,count(`sm_pd`.`RECEPTION_ID`) AS `reception_valid_count`,max(`sm_pd`.`UNIT_PRICE_BUY`) AS `unit_price_buy_max`,avg(`sm_pd`.`UNIT_PRICE_BUY`) AS `unit_price_buy_moyenne`,min(`sm_pd`.`UNIT_PRICE_BUY`) AS `unit_price_buy_min`,count(`sm_pd`.`QUANTITY`) AS `quantity_count` from (((`sm_product` `sm_p` join `sm_product_details` `sm_pd`) join `sm_product_color` `sm_pc`) join `sm_product_size` `sm_ps`) where ((`sm_p`.`ID` = `sm_pd`.`PRODUCT_ID`) and (`sm_p`.`PRODUCT_COLOR_ID` = `sm_pc`.`ID`) and (`sm_p`.`PRODUCT_SIZE_ID` = `sm_ps`.`ID`) and (ucase(`sm_pd`.`ACTIVE`) like convert(ucase('y') using latin1))) group by `sm_p`.`REFERENCE`,`sm_p`.`DESIGNATION`,`sm_p`.`QUANTITY`,`sm_p`.`PRICE_BUY`,`sm_p`.`PRICE_SALE`,`sm_p`.`ACTIVE`;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_lovs`
--
DROP TABLE IF EXISTS `v_sm_lovs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_lovs` AS select concat_ws('.',`v_sm_product_status`.`ID`,'sm_product_status') AS `code`,'sm_product_status' AS `table_code`,`v_sm_product_status`.`ID` AS `id`,`v_sm_product_status`.`NAME` AS `name`,`v_sm_product_status`.`DESCRIPTION` AS `description`,`v_sm_product_status`.`ACTIVE` AS `active`,`v_sm_product_status`.`SORT_KEY` AS `sort_key`,`v_sm_product_status`.`CLT_MODULE_ID` AS `clt_module_id` from `v_sm_product_status` union select concat_ws('.',`v_sm_expense_type`.`ID`,'sm_expense_type') AS `code`,'sm_expense_type' AS `table_code`,`v_sm_expense_type`.`ID` AS `id`,`v_sm_expense_type`.`NAME` AS `name`,`v_sm_expense_type`.`DESCRIPTION` AS `description`,`v_sm_expense_type`.`ACTIVE` AS `active`,`v_sm_expense_type`.`SORT_KEY` AS `sort_key`,`v_sm_expense_type`.`CLT_MODULE_ID` AS `clt_module_id` from `v_sm_expense_type` union select concat_ws('.',`v_sm_supplier_type`.`ID`,'m_supplier_type') AS `code`,'sm_supplier_type' AS `table_code`,`v_sm_supplier_type`.`ID` AS `id`,`v_sm_supplier_type`.`NAME` AS `name`,`v_sm_supplier_type`.`DESCRIPTION` AS `description`,`v_sm_supplier_type`.`ACTIVE` AS `active`,`v_sm_supplier_type`.`SORT_KEY` AS `sort_key`,`v_sm_supplier_type`.`CLT_MODULE_ID` AS `clt_module_id` from `v_sm_supplier_type` union select concat_ws('.',`v_inf_country`.`ID`,'v_inf_country') AS `code`,'inf_country' AS `table_code`,`v_inf_country`.`ID` AS `id`,`v_inf_country`.`NAME` AS `name`,`v_inf_country`.`DESCRIPTION` AS `description`,`v_inf_country`.`ACTIVE` AS `active`,`v_inf_country`.`SORT_KEY` AS `sort_key`,NULL AS `clt_module_id` from `v_inf_country` union select concat_ws('.',`v_inf_city`.`ID`,'v_inf_city') AS `code`,'inf_city' AS `table_code`,`v_inf_city`.`ID` AS `id`,`v_inf_city`.`NAME` AS `name`,`v_inf_city`.`DESCRIPTION` AS `description`,`v_inf_city`.`ACTIVE` AS `active`,`v_inf_city`.`SORT_KEY` AS `sort_key`,NULL AS `clt_module_id` from `v_inf_city` union select concat_ws('.',`v_sm_product_size`.`ID`,'v_sm_product_size') AS `code`,'sm_product_size' AS `table_code`,`v_sm_product_size`.`ID` AS `id`,`v_sm_product_size`.`NAME` AS `name`,`v_sm_product_size`.`DESCRIPTION` AS `description`,`v_sm_product_size`.`ACTIVE` AS `active`,`v_sm_product_size`.`SORT_KEY` AS `sort_key`,`v_sm_product_size`.`CLT_MODULE_ID` AS `clt_module_id` from `v_sm_product_size` union select concat_ws('.',`v_sm_product_family`.`ID`,'v_sm_product_family') AS `code`,'sm_product_family' AS `table_code`,`v_sm_product_family`.`ID` AS `id`,`v_sm_product_family`.`NAME` AS `name`,`v_sm_product_family`.`DESCRIPTION` AS `description`,`v_sm_product_family`.`ACTIVE` AS `active`,`v_sm_product_family`.`SORT_KEY` AS `sort_key`,`v_sm_product_family`.`CLT_MODULE_ID` AS `clt_module_id` from `v_sm_product_family` union select concat_ws('.',`v_sm_product_color`.`ID`,'v_sm_product_color') AS `code`,'sm_product_color' AS `table_code`,`v_sm_product_color`.`ID` AS `id`,`v_sm_product_color`.`NAME` AS `name`,NULL AS `description`,`v_sm_product_color`.`ACTIVE` AS `active`,`v_sm_product_color`.`SORT_KEY` AS `sort_key`,`v_sm_product_color`.`CLT_MODULE_ID` AS `clt_module_id` from `v_sm_product_color` union select concat_ws('.',`v_sm_supplier`.`ID`,'v_sm_supplier') AS `code`,'sm_supplier' AS `table_code`,`v_sm_supplier`.`ID` AS `id`,concat_ws(', ',`v_sm_supplier`.`FIRST_NAME`,ucase(`v_sm_supplier`.`LAST_NAME`)) AS `name`,NULL AS `description`,`v_sm_supplier`.`ACTIVE` AS `active`,`v_sm_supplier`.`SORT_KEY` AS `sort_key`,`v_sm_supplier`.`CLT_MODULE_ID` AS `clt_module_id` from `v_sm_supplier` union select concat_ws('.',`v_sm_deposit`.`ID`,'v_sm_deposit') AS `code`,'sm_deposit' AS `table_code`,`v_sm_deposit`.`ID` AS `id`,`v_sm_deposit`.`NAME` AS `name`,NULL AS `description`,`v_sm_deposit`.`ACTIVE` AS `active`,`v_sm_deposit`.`SORT_KEY` AS `sort_key`,`v_sm_deposit`.`CLT_MODULE_ID` AS `clt_module_id` from `v_sm_deposit` union select concat_ws('.',`v_sm_customer_type`.`ID`,'v_sm_customer_type') AS `code`,'sm_customer_type' AS `table_code`,`v_sm_customer_type`.`ID` AS `id`,`v_sm_customer_type`.`NAME` AS `name`,NULL AS `description`,`v_sm_customer_type`.`ACTIVE` AS `active`,`v_sm_customer_type`.`SORT_KEY` AS `sort_key`,`v_sm_customer_type`.`CLT_MODULE_ID` AS `clt_module_id` from `v_sm_customer_type`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_order`
--
DROP TABLE IF EXISTS `v_sm_order`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_order` AS select `sm_o`.`ID` AS `ID`,`sm_o`.`NO_SEQ` AS `NO_SEQ`,`sm_o`.`REFERENCE` AS `REFERENCE`,`sm_o`.`CUSTOMER_ID` AS `CUSTOMER_ID`,`sm_o`.`PAYMENT_METHOD_ID` AS `PAYMENT_METHOD_ID`,`sm_o`.`CHECK_ID` AS `CHECK_ID`,`sm_o`.`DELIVERY` AS `DELIVERY`,`sm_o`.`NOTE` AS `NOTE`,`sm_o`.`ORDER_STATUS_ID` AS `ORDER_STATUS_ID`,`sm_o`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`sm_o`.`ACTIVE` AS `ACTIVE`,`sm_o`.`DATE_CREATION` AS `DATE_CREATION`,`sm_o`.`USER_CREATION` AS `USER_CREATION`,`sm_o`.`DATE_UPDATE` AS `DATE_UPDATE`,`sm_o`.`USER_UPDATE` AS `USER_UPDATE`,`sm_c`.`COMPANY_NAME` AS `customer_company_name`,`vsm_os`.`summary_total_ht` AS `total_hors_taxe`,`vsm_os`.`summary_total_ttc` AS `total_ttc`,`vsm_os`.`summary_total_quantity` AS `sum_product`,`sm_os`.`NAME` AS `order_status_name` from (((`sm_order` `sm_o` left join `sm_customer` `sm_c` on((`sm_o`.`CUSTOMER_ID` = `sm_c`.`ID`))) left join `v_sm_order_summary` `vsm_os` on((`sm_o`.`ID` = `vsm_os`.`order_id`))) left join `sm_order_status` `sm_os` on((`sm_os`.`ID` = `sm_o`.`ORDER_STATUS_ID`)));

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_order_details`
--
DROP TABLE IF EXISTS `v_sm_order_details`;

CREATE ALGORITHM=UNDEFINED DEFINER=`mystock`@`%` SQL SECURITY DEFINER VIEW `v_sm_order_details` AS (select distinct `CLT_M`.`ID` AS `CLT_MODULE_ID`,(select count(`SM_OS`.`ID`) from `sm_order_supplier` `SM_OS` where ((`SM_OS`.`ORDER_SUPPLIER_STATUS_ID` = 1) and (`SM_OS`.`CLT_MODULE_ID` = `CLT_M`.`ID`))) AS `ORDER_SUPPLIER_IN_PROGRESS_COUNT`,(select count(`SM_OS`.`ID`) from `sm_order_supplier` `SM_OS` where ((`SM_OS`.`ORDER_SUPPLIER_STATUS_ID` = 2) and (`SM_OS`.`CLT_MODULE_ID` = `CLT_M`.`ID`))) AS `ORDER_SUPPLIER_TRANSMITTED_COUNT`,(select count(`SM_OS`.`ID`) from `sm_order_supplier` `SM_OS` where ((`SM_OS`.`ORDER_SUPPLIER_STATUS_ID` = 3) and (`SM_OS`.`CLT_MODULE_ID` = `CLT_M`.`ID`))) AS `ORDER_SUPPLIER_VALIDATED_COUNT`,(select count(`SM_OS`.`ID`) from `sm_order_supplier` `SM_OS` where ((`SM_OS`.`ORDER_SUPPLIER_STATUS_ID` = 4) and (`SM_OS`.`CLT_MODULE_ID` = `CLT_M`.`ID`))) AS `ORDER_SUPPLIER_REFUSED_COUNT`,(select count(`SM_OS`.`ID`) from `sm_order_supplier` `SM_OS` where ((`SM_OS`.`ORDER_SUPPLIER_STATUS_ID` = 5) and (`SM_OS`.`CLT_MODULE_ID` = `CLT_M`.`ID`))) AS `ORDER_SUPPLIER_REJECTED_COUNT`,(select count(`SM_R`.`ID`) from `sm_reception` `SM_R` where ((`SM_R`.`RECEPTION_STATUS_ID` = 1) and (`SM_R`.`CLT_MODULE_ID` = `CLT_M`.`ID`))) AS `RECEPTION_IN_PROGRESS_COUNT`,(select count(`SM_R`.`ID`) from `sm_reception` `SM_R` where ((`SM_R`.`RECEPTION_STATUS_ID` = 2) and (`SM_R`.`CLT_MODULE_ID` = `CLT_M`.`ID`))) AS `RECEPTION_TRANSMITTED_COUNT`,(select count(`SM_R`.`ID`) from `sm_reception` `SM_R` where ((`SM_R`.`RECEPTION_STATUS_ID` = 3) and (`SM_R`.`CLT_MODULE_ID` = `CLT_M`.`ID`))) AS `RECEPTION_VALIDATED_COUNT`,(select count(`SM_R`.`ID`) from `sm_reception` `SM_R` where ((`SM_R`.`RECEPTION_STATUS_ID` = 4) and (`SM_R`.`CLT_MODULE_ID` = `CLT_M`.`ID`))) AS `RECEPTION_REFUSED_COUNT`,(select count(`SM_R`.`ID`) from `sm_reception` `SM_R` where ((`SM_R`.`RECEPTION_STATUS_ID` = 5) and (`SM_R`.`CLT_MODULE_ID` = `CLT_M`.`ID`))) AS `RECEPTION_REJECTED_COUNT`,(select count(`SM_O`.`ID`) from `sm_order` `SM_O` where ((`SM_O`.`ORDER_STATUS_ID` = 1) and (`SM_O`.`CLT_MODULE_ID` = `CLT_M`.`ID`))) AS `ORDER_IN_PROGRESS_COUNT`,(select count(`SM_O`.`ID`) from `sm_order` `SM_O` where ((`SM_O`.`ORDER_STATUS_ID` = 2) and (`SM_O`.`CLT_MODULE_ID` = `CLT_M`.`ID`))) AS `ORDER_TRANSMITTED_COUNT`,(select count(`SM_O`.`ID`) from `sm_order` `SM_O` where ((`SM_O`.`ORDER_STATUS_ID` = 3) and (`SM_O`.`CLT_MODULE_ID` = `CLT_M`.`ID`))) AS `ORDER_VALIDATED_COUNT`,(select count(`SM_O`.`ID`) from `sm_order` `SM_O` where ((`SM_O`.`ORDER_STATUS_ID` = 4) and (`SM_O`.`CLT_MODULE_ID` = `CLT_M`.`ID`))) AS `ORDERD_REFUSED_COUNT`,(select count(`SM_O`.`ID`) from `sm_order` `SM_O` where ((`SM_O`.`ORDER_STATUS_ID` = 5) and (`SM_O`.`CLT_MODULE_ID` = `CLT_M`.`ID`))) AS `ORDERD_REJECTED_COUNT`,(select count(`SM_RR`.`ID`) from `sm_return_receipt` `SM_RR` where ((`SM_RR`.`RETURN_RECEIPT_STATUS_ID` = 1) and (`SM_RR`.`CLT_MODULE_ID` = `CLT_M`.`ID`))) AS `RETURN_RECEIPT_IN_PROGRESS_COUNT`,(select count(`SM_RR`.`ID`) from `sm_return_receipt` `SM_RR` where ((`SM_RR`.`RETURN_RECEIPT_STATUS_ID` = 2) and (`SM_RR`.`CLT_MODULE_ID` = `CLT_M`.`ID`))) AS `RETURN_RECEIPT_TRANSMITTED_COUNT`,(select count(`SM_RR`.`ID`) from `sm_return_receipt` `SM_RR` where ((`SM_RR`.`RETURN_RECEIPT_STATUS_ID` = 3) and (`SM_RR`.`CLT_MODULE_ID` = `CLT_M`.`ID`))) AS `RETURN_RECEIPT_VALIDATED_COUNT`,(select count(`SM_RR`.`ID`) from `sm_return_receipt` `SM_RR` where ((`SM_RR`.`RETURN_RECEIPT_STATUS_ID` = 4) and (`SM_RR`.`CLT_MODULE_ID` = `CLT_M`.`ID`))) AS `RETURN_RECEIPT_REFUSED_COUNT`,(select count(`SM_RR`.`ID`) from `sm_return_receipt` `SM_RR` where ((`SM_RR`.`RETURN_RECEIPT_STATUS_ID` = 5) and (`SM_RR`.`CLT_MODULE_ID` = `CLT_M`.`ID`))) AS `RETURN_RECEIPT_REJECTED_COUNT` from `clt_module` `CLT_M`);

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_order_line`
--
DROP TABLE IF EXISTS `v_sm_order_line`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_order_line` AS select `sm_ol`.`ID` AS `ID`,`sm_ol`.`PRODUCT_ID` AS `PRODUCT_ID`,`sm_ol`.`DESIGNATION` AS `DESIGNATION`,`sm_ol`.`ORDER_ID` AS `ORDER_ID`,`sm_ol`.`PROMOTION_ID` AS `PROMOTION_ID`,`sm_ol`.`NEGOTIATE_PRICE_SALE` AS `NEGOTIATE_PRICE_SALE`,`sm_ol`.`QUANTITY` AS `QUANTITY`,`sm_ol`.`ACTIVE` AS `ACTIVE`,`sm_ol`.`DATE_CREATION` AS `DATE_CREATION`,`sm_ol`.`USER_CREATION` AS `USER_CREATION`,`sm_ol`.`DATE_UPDATE` AS `DATE_UPDATE`,`sm_ol`.`USER_UPDATE` AS `USER_UPDATE`,`sm_p`.`REFERENCE` AS `reference`,(`sm_ol`.`QUANTITY` * `sm_ol`.`NEGOTIATE_PRICE_SALE`) AS `TOTAL_NEGOTIATE_PRICE_SALE` from (`sm_order_line` `sm_ol` join `sm_product` `sm_p`) where (`sm_ol`.`PRODUCT_ID` = `sm_p`.`ID`);

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_order_summary`
--
DROP TABLE IF EXISTS `v_sm_order_summary`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_order_summary` AS select `sm_o`.`ID` AS `order_id`,sum(`sm_ol`.`QUANTITY`) AS `summary_total_quantity`,sum(`sm_ol`.`NEGOTIATE_PRICE_SALE`) AS `summary_total_ht`,((sum(`sm_ol`.`NEGOTIATE_PRICE_SALE`) * 1.2) - sum(`sm_ol`.`NEGOTIATE_PRICE_SALE`)) AS `summary_tva_amount`,(sum(`sm_ol`.`NEGOTIATE_PRICE_SALE`) * 1.2) AS `summary_total_ttc` from (`sm_order` `sm_o` left join `sm_order_line` `sm_ol` on((`sm_o`.`ID` = `sm_ol`.`ORDER_ID`))) group by `sm_o`.`ID`;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_order_supplier`
--
DROP TABLE IF EXISTS `v_sm_order_supplier`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_order_supplier` AS select `sm_os`.`ID` AS `ID`,`sm_os`.`NO_SEQ` AS `NO_SEQ`,`sm_os`.`REFERENCE` AS `REFERENCE`,`sm_os`.`NOTE` AS `NOTE`,`sm_os`.`ORDER_SUPPLIER_STATUS_ID` AS `ORDER_SUPPLIER_STATUS_ID`,`sm_os`.`SUPPLIER_ID` AS `SUPPLIER_ID`,`sm_os`.`ACTIVE` AS `ACTIVE`,`sm_os`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`sm_os`.`DATE_CREATION` AS `DATE_CREATION`,`sm_os`.`USER_CREATION` AS `USER_CREATION`,`sm_os`.`DATE_UPDATE` AS `DATE_UPDATE`,`sm_os`.`USER_UPDATE` AS `USER_UPDATE`,`sm_s`.`COMPANY_NAME` AS `SUPPLIER_COMPANY_NAME`,`vsm_oss`.`summaryTotalHt` AS `TOTAL_HORS_TAXE`,`vsm_oss`.`summaryTotalTtc` AS `TOTAL_TTC`,`vsm_oss`.`summaryTotalQuantity` AS `SUM_PRODUCT`,`sm_osst`.`NAME` AS `order_supplier_status_name` from (((`sm_order_supplier` `sm_os` left join `sm_supplier` `sm_s` on((`sm_os`.`SUPPLIER_ID` = `sm_s`.`ID`))) left join `v_sm_order_supplier_summary` `vsm_oss` on((`sm_os`.`ID` = `vsm_oss`.`order_supplier_id`))) left join `sm_order_supplier_status` `sm_osst` on((`sm_osst`.`ID` = `sm_os`.`ORDER_SUPPLIER_STATUS_ID`)));

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_order_supplier_line`
--
DROP TABLE IF EXISTS `v_sm_order_supplier_line`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_order_supplier_line` AS select `sm_osl`.`ID` AS `ID`,`sm_osl`.`PRODUCT_ID` AS `PRODUCT_ID`,`sm_osl`.`DESIGNATION` AS `DESIGNATION`,`sm_osl`.`UNIT_PRICE_SALE` AS `UNIT_PRICE_SALE`,`sm_osl`.`QUANTITY` AS `QUANTITY`,`sm_osl`.`TVA` AS `TVA`,`sm_osl`.`REMISE` AS `REMISE`,`sm_osl`.`PROMOTION_ID` AS `PROMOTION_ID`,`sm_osl`.`ORDER_SUPPLIER_ID` AS `ORDER_SUPPLIER_ID`,`sm_osl`.`ACTIVE` AS `ACTIVE`,`sm_osl`.`DATE_CREATION` AS `DATE_CREATION`,`sm_osl`.`USER_CREATION` AS `USER_CREATION`,`sm_osl`.`DATE_UPDATE` AS `DATE_UPDATE`,`sm_osl`.`USER_UPDATE` AS `USER_UPDATE`,`sm_p`.`REFERENCE` AS `reference`,(`sm_osl`.`QUANTITY` * `sm_osl`.`UNIT_PRICE_SALE`) AS `TOTAL_PRICE_SALE` from (`sm_order_supplier_line` `sm_osl` join `sm_product` `sm_p`) where (`sm_osl`.`PRODUCT_ID` = `sm_p`.`ID`);

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_order_supplier_not_received`
--
DROP TABLE IF EXISTS `v_sm_order_supplier_not_received`;

CREATE ALGORITHM=UNDEFINED DEFINER=`mystock`@`%` SQL SECURITY DEFINER VIEW `v_sm_order_supplier_not_received` AS (select `SM_OS`.`ID` AS `ID`,`SM_OS`.`NO_SEQ` AS `NO_SEQ`,`SM_OS`.`REFERENCE` AS `REFERENCE`,`SM_OS`.`NOTE` AS `NOTE`,`SM_OS`.`ORDER_SUPPLIER_STATUS_ID` AS `ORDER_SUPPLIER_STATUS_ID`,`SM_OS`.`SUPPLIER_ID` AS `SUPPLIER_ID`,`SM_OS`.`ACTIVE` AS `ACTIVE`,`SM_OS`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`SM_OS`.`DATE_CREATION` AS `DATE_CREATION`,`SM_OS`.`USER_CREATION` AS `USER_CREATION`,`SM_OS`.`DATE_UPDATE` AS `DATE_UPDATE`,`SM_OS`.`USER_UPDATE` AS `USER_UPDATE`,`SM_S`.`FIRST_NAME` AS `SUPPLIER_FIRST_NAME`,`SM_S`.`LAST_NAME` AS `SUPPLIER_LAST_NAME`,(coalesce(sum(`SM_OSL`.`QUANTITY`),'0') + 0) AS `QTE_COMMANDED`,((select coalesce(sum(`SM_RL`.`QUANTITY`),'0') AS `QTE_RECIEVED` from (`sm_reception_line` `SM_RL` join `sm_reception` `SM_R`) where ((`SM_RL`.`RECEPTION_ID` = `SM_R`.`ID`) and (`SM_R`.`ORDER_SUPPLIER_ID` = `SM_OSL`.`ORDER_SUPPLIER_ID`) and (`SM_R`.`RECEPTION_STATUS_ID` = 3))) + 0) AS `QTE_RECIEVED` from ((`sm_order_supplier` `SM_OS` join `sm_order_supplier_line` `SM_OSL`) join `sm_supplier` `SM_S`) where ((`SM_OS`.`ID` = `SM_OSL`.`ORDER_SUPPLIER_ID`) and (`SM_OS`.`ORDER_SUPPLIER_STATUS_ID` = 3) and (`SM_S`.`ID` = `SM_OS`.`SUPPLIER_ID`)) group by `SM_OSL`.`ORDER_SUPPLIER_ID` order by `SM_OSL`.`ORDER_SUPPLIER_ID`);

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_order_supplier_summary`
--
DROP TABLE IF EXISTS `v_sm_order_supplier_summary`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_order_supplier_summary` AS select `sm_os`.`ID` AS `order_supplier_id`,sum(`sm_osl`.`QUANTITY`) AS `summaryTotalQuantity`,sum(`sm_osl`.`UNIT_PRICE_SALE`) AS `summaryTotalHt`,((sum(`sm_osl`.`UNIT_PRICE_SALE`) * 1.2) - sum(`sm_osl`.`UNIT_PRICE_SALE`)) AS `summaryTvaAmount`,(sum(`sm_osl`.`UNIT_PRICE_SALE`) * 1.2) AS `summaryTotalTtc` from (`sm_order_supplier` `sm_os` left join `sm_order_supplier_line` `sm_osl` on((`sm_os`.`ID` = `sm_osl`.`ORDER_SUPPLIER_ID`))) group by `sm_os`.`ID`;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_product`
--
DROP TABLE IF EXISTS `v_sm_product`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_product` AS select `sm_p`.`ID` AS `id`,`sm_p`.`NO_SEQ` AS `no_seq`,`sm_p`.`REFERENCE` AS `reference`,`sm_p`.`DESIGNATION` AS `designation`,`sm_p`.`QUANTITY` AS `quantity`,`sm_p`.`THRESHOLD` AS `threshold`,`sm_p`.`PRICE_SALE` AS `price_sale`,`sm_p`.`PRICE_BUY` AS `price_buy`,`sm_p`.`NOTE` AS `note`,`sm_p`.`ACTIVE` AS `active`,`sm_p`.`CLT_MODULE_ID` AS `clt_module_id`,`sm_p`.`PRODUCT_GROUP_ID` AS `product_group_id`,`sm_pg`.`NAME` AS `product_group_name`,`sm_p`.`PRODUCT_FAMILY_ID` AS `product_family_id`,`sm_pf`.`NAME` AS `product_family_name`,`sm_p`.`PRODUCT_SIZE_ID` AS `product_size_id`,`sm_pz`.`NAME` AS `product_size_name`,`sm_p`.`PRODUCT_STATUS_ID` AS `product_status_id`,`sm_ps`.`NAME` AS `product_status_name`,`sm_p`.`PRODUCT_TYPE_ID` AS `product_type_id`,`sm_pt`.`NAME` AS `product_type_name`,`sm_p`.`PRODUCT_COLOR_ID` AS `product_color_id`,`sm_pc`.`NAME` AS `product_color_name`,`sm_p`.`PRODUCT_DEPARTMENT_ID` AS `product_department_id`,`sm_pd`.`NAME` AS `product_department_name`,`sm_p`.`PRODUCT_UNIT_ID` AS `product_unit_id`,`sm_pu`.`NAME` AS `product_unit_name` from ((((((((`sm_product` `sm_p` left join `sm_product_color` `sm_pc` on((`sm_p`.`PRODUCT_COLOR_ID` = `sm_pc`.`ID`))) left join `sm_product_family` `sm_pf` on((`sm_p`.`PRODUCT_FAMILY_ID` = `sm_pf`.`ID`))) left join `sm_product_group` `sm_pg` on((`sm_p`.`PRODUCT_GROUP_ID` = `sm_pg`.`ID`))) left join `sm_product_size` `sm_pz` on((`sm_p`.`PRODUCT_SIZE_ID` = `sm_pz`.`ID`))) left join `sm_product_status` `sm_ps` on((`sm_p`.`PRODUCT_STATUS_ID` = `sm_ps`.`ID`))) left join `sm_product_type` `sm_pt` on((`sm_p`.`PRODUCT_TYPE_ID` = `sm_pt`.`ID`))) left join `sm_product_department` `sm_pd` on((`sm_p`.`PRODUCT_DEPARTMENT_ID` = `sm_pd`.`ID`))) left join `sm_product_unit` `sm_pu` on((`sm_p`.`PRODUCT_UNIT_ID` = `sm_pu`.`ID`)));

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_product_alert`
--
DROP TABLE IF EXISTS `v_sm_product_alert`;

CREATE ALGORITHM=UNDEFINED DEFINER=`mystock`@`%` SQL SECURITY DEFINER VIEW `v_sm_product_alert` AS (select `SM_P`.`ID` AS `ID`,`SM_P`.`REFERENCE` AS `REFERENCE`,`SM_P`.`DESIGNATION` AS `DESIGNATION`,`SM_P`.`QUANTITY` AS `QUANTITY`,`SM_P`.`THRESHOLD` AS `THRESHOLD`,`SM_P`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,coalesce(sum(`vsm_pnr`.`QTE_COMMANDED`),0) AS `TOTAL_COMMANDED`,coalesce(sum(`vsm_pnr`.`QTE_RECIEVED`),0) AS `TOTAL_RECIEVED` from (`sm_product` `SM_P` left join `v_sm_product_not_received` `VSM_PNR` on((`vsm_pnr`.`PRODUCT_ID` = `SM_P`.`ID`))) where (`SM_P`.`QUANTITY` <= `SM_P`.`THRESHOLD`) group by `SM_P`.`ID`);

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_product_color`
--
DROP TABLE IF EXISTS `v_sm_product_color`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_product_color` AS select `sm_product_color`.`ID` AS `ID`,`sm_product_color`.`NAME` AS `NAME`,`sm_product_color`.`HEX` AS `HEX`,`sm_product_color`.`RGB` AS `RGB`,`sm_product_color`.`SORT_KEY` AS `SORT_KEY`,`sm_product_color`.`ACTIVE` AS `ACTIVE`,`sm_product_color`.`DATE_CREATION` AS `DATE_CREATION`,`sm_product_color`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`sm_product_color`.`USER_CREATION` AS `USER_CREATION`,`sm_product_color`.`DATE_UPDATE` AS `DATE_UPDATE`,`sm_product_color`.`USER_UPDATE` AS `USER_UPDATE` from `sm_product_color`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_product_department`
--
DROP TABLE IF EXISTS `v_sm_product_department`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_product_department` AS select `sm_product_department`.`ID` AS `ID`,`sm_product_department`.`NAME` AS `NAME`,`sm_product_department`.`DESCRIPTION` AS `DESCRIPTION`,`sm_product_department`.`SORT_KEY` AS `SORT_KEY`,`sm_product_department`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`sm_product_department`.`ACTIVE` AS `ACTIVE`,`sm_product_department`.`DATE_CREATION` AS `DATE_CREATION`,`sm_product_department`.`USER_CREATION` AS `USER_CREATION`,`sm_product_department`.`DATE_UPDATE` AS `DATE_UPDATE`,`sm_product_department`.`USER_UPDATE` AS `USER_UPDATE` from `sm_product_department`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_product_details_group`
--
DROP TABLE IF EXISTS `v_sm_product_details_group`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_product_details_group` AS select `sm_p`.`ID` AS `product_id`,group_concat(`sm_pd`.`ID` separator ',') AS `ids`,`sm_p`.`REFERENCE` AS `reference`,`sm_pc`.`NAME` AS `product_color_name`,group_concat(`sm_ps`.`NAME` separator ',') AS `product_size_names`,group_concat(`sm_pd`.`QUANTITY` separator ',') AS `quantitys`,sum(`sm_pd`.`QUANTITY`) AS `sum_quantity`,`sm_pd`.`UNIT_PRICE_BUY` AS `unit_price_buy`,(sum(`sm_pd`.`QUANTITY`) * `sm_pd`.`UNIT_PRICE_BUY`) AS `sum_unit_price_buy`,`sm_pd`.`RECEPTION_ID` AS `reception_id`,`sm_pc`.`ID` AS `product_color_id` from (((`sm_product` `sm_p` join `sm_product_details` `sm_pd`) join `sm_product_color` `sm_pc`) join `sm_product_size` `sm_ps`) where ((`sm_p`.`ID` = `sm_pd`.`PRODUCT_ID`) and (`sm_p`.`PRODUCT_COLOR_ID` = `sm_pc`.`ID`) and (`sm_p`.`PRODUCT_SIZE_ID` = `sm_ps`.`ID`)) group by `sm_p`.`REFERENCE`,`sm_pc`.`NAME`,`sm_pd`.`UNIT_PRICE_BUY`,`sm_pd`.`RECEPTION_ID`;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_product_family`
--
DROP TABLE IF EXISTS `v_sm_product_family`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_product_family` AS select `sm_product_family`.`ID` AS `ID`,`sm_product_family`.`NAME` AS `NAME`,`sm_product_family`.`DESCRIPTION` AS `DESCRIPTION`,`sm_product_family`.`PRODUCT_GROUP_ID` AS `PRODUCT_GROUP_ID`,`sm_product_family`.`SORT_KEY` AS `SORT_KEY`,`sm_product_family`.`ACTIVE` AS `ACTIVE`,`sm_product_family`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`sm_product_family`.`DATE_CREATION` AS `DATE_CREATION`,`sm_product_family`.`USER_CREATION` AS `USER_CREATION`,`sm_product_family`.`DATE_UPDATE` AS `DATE_UPDATE`,`sm_product_family`.`USER_UPDATE` AS `USER_UPDATE` from `sm_product_family`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_product_group`
--
DROP TABLE IF EXISTS `v_sm_product_group`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_product_group` AS select `sm_product_group`.`ID` AS `ID`,`sm_product_group`.`NAME` AS `NAME`,`sm_product_group`.`DESCRIPTION` AS `DESCRIPTION`,`sm_product_group`.`SORT_KEY` AS `SORT_KEY`,`sm_product_group`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`sm_product_group`.`ACTIVE` AS `ACTIVE`,`sm_product_group`.`DATE_CREATION` AS `DATE_CREATION`,`sm_product_group`.`USER_CREATION` AS `USER_CREATION`,`sm_product_group`.`DATE_UPDATE` AS `DATE_UPDATE`,`sm_product_group`.`USER_UPDATE` AS `USER_UPDATE` from `sm_product_group`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_product_not_received`
--
DROP TABLE IF EXISTS `v_sm_product_not_received`;

CREATE ALGORITHM=UNDEFINED DEFINER=`mystock`@`%` SQL SECURITY DEFINER VIEW `v_sm_product_not_received` AS (select concat_ws('.',`SM_OS`.`ID`,`SM_OSL`.`ID`) AS `CODE`,`SM_OS`.`ID` AS `ORDER_SUPPLIER_ID`,`SM_OS`.`REFERENCE` AS `ORDER_SUPPLIER_REFERENCE`,`SM_P`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`SM_P`.`REFERENCE` AS `PRODUCT_REFERENCE`,`SM_P`.`DESIGNATION` AS `PRODUCT_DESIGNATION`,`SM_S`.`ID` AS `SUPPLIER_ID`,`SM_S`.`FIRST_NAME` AS `SUPPLIER_FIRST_NAME`,`SM_S`.`LAST_NAME` AS `SUPPLIER_LAST_NAME`,`SM_S`.`COMPANY_NAME` AS `SUPPLIER_COMPANY_NAME`,`SM_OSL`.`PRODUCT_ID` AS `PRODUCT_ID`,coalesce(`SM_OSL`.`QUANTITY`,'0') AS `QTE_COMMANDED`,((select coalesce(sum(`SM_RL`.`QUANTITY`),'0') AS `QTE_RECIEVED` from (`sm_reception_line` `SM_RL` join `sm_reception` `SM_R`) where ((`SM_RL`.`RECEPTION_ID` = `SM_R`.`ID`) and (`SM_R`.`ORDER_SUPPLIER_ID` = `SM_OSL`.`ORDER_SUPPLIER_ID`) and (`SM_RL`.`PRODUCT_ID` = `SM_OSL`.`PRODUCT_ID`) and (`SM_R`.`RECEPTION_STATUS_ID` = 3))) + 0) AS `QTE_RECIEVED` from (((`sm_order_supplier` `SM_OS` join `sm_order_supplier_line` `SM_OSL`) join `sm_product` `SM_P`) join `sm_supplier` `SM_S`) where ((`SM_OSL`.`PRODUCT_ID` = `SM_P`.`ID`) and (`SM_OS`.`ID` = `SM_OSL`.`ORDER_SUPPLIER_ID`) and (`SM_OS`.`ORDER_SUPPLIER_STATUS_ID` = 3) and (`SM_S`.`ID` = `SM_OS`.`SUPPLIER_ID`)) having (`QTE_COMMANDED` > `QTE_RECIEVED`) order by `SM_OSL`.`ORDER_SUPPLIER_ID`);

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_product_size`
--
DROP TABLE IF EXISTS `v_sm_product_size`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_product_size` AS select `sm_product_size`.`ID` AS `ID`,`sm_product_size`.`NAME` AS `NAME`,`sm_product_size`.`DESCRIPTION` AS `DESCRIPTION`,`sm_product_size`.`SORT_KEY` AS `SORT_KEY`,`sm_product_size`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`sm_product_size`.`ACTIVE` AS `ACTIVE`,`sm_product_size`.`DATE_CREATION` AS `DATE_CREATION`,`sm_product_size`.`USER_CREATION` AS `USER_CREATION`,`sm_product_size`.`DATE_UPDATE` AS `DATE_UPDATE`,`sm_product_size`.`USER_UPDATE` AS `USER_UPDATE` from `sm_product_size`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_product_status`
--
DROP TABLE IF EXISTS `v_sm_product_status`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_product_status` AS select `sm_product_status`.`ID` AS `ID`,`sm_product_status`.`NAME` AS `NAME`,`sm_product_status`.`DESCRIPTION` AS `DESCRIPTION`,`sm_product_status`.`SORT_KEY` AS `SORT_KEY`,`sm_product_status`.`ACTIVE` AS `ACTIVE`,`sm_product_status`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`sm_product_status`.`DATE_CREATION` AS `DATE_CREATION`,`sm_product_status`.`USER_CREATION` AS `USER_CREATION`,`sm_product_status`.`DATE_UPDATE` AS `DATE_UPDATE`,`sm_product_status`.`USER_UPDATE` AS `USER_UPDATE` from `sm_product_status`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_product_type`
--
DROP TABLE IF EXISTS `v_sm_product_type`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_product_type` AS select `sm_product_type`.`ID` AS `ID`,`sm_product_type`.`NAME` AS `NAME`,`sm_product_type`.`DESCRIPTION` AS `DESCRIPTION`,`sm_product_type`.`SORT_KEY` AS `SORT_KEY`,`sm_product_type`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`sm_product_type`.`ACTIVE` AS `ACTIVE`,`sm_product_type`.`DATE_CREATION` AS `DATE_CREATION`,`sm_product_type`.`USER_CREATION` AS `USER_CREATION`,`sm_product_type`.`DATE_UPDATE` AS `DATE_UPDATE`,`sm_product_type`.`USER_UPDATE` AS `USER_UPDATE` from `sm_product_type`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_product_unit`
--
DROP TABLE IF EXISTS `v_sm_product_unit`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_product_unit` AS select `sm_product_unit`.`ID` AS `ID`,`sm_product_unit`.`NAME` AS `NAME`,`sm_product_unit`.`DESCRIPTION` AS `DESCRIPTION`,`sm_product_unit`.`SORT_KEY` AS `SORT_KEY`,`sm_product_unit`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`sm_product_unit`.`ACTIVE` AS `ACTIVE`,`sm_product_unit`.`DATE_CREATION` AS `DATE_CREATION`,`sm_product_unit`.`USER_CREATION` AS `USER_CREATION`,`sm_product_unit`.`DATE_UPDATE` AS `DATE_UPDATE`,`sm_product_unit`.`USER_UPDATE` AS `USER_UPDATE` from `sm_product_unit`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_rap_daily`
--
DROP TABLE IF EXISTS `v_sm_rap_daily`;
-- utilisé(#1356 - View 'mystock.v_sm_rap_daily' references invalid table(s) or column(s) or function(s) or definer/invoker of view lack rights to use them)

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_reception`
--
DROP TABLE IF EXISTS `v_sm_reception`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_reception` AS select `sm_r`.`ID` AS `ID`,`sm_r`.`NO_SEQ` AS `NO_SEQ`,`sm_r`.`SOUCHE` AS `SOUCHE`,`sm_r`.`SUPPLIER_ID` AS `SUPPLIER_ID`,`sm_r`.`DEPOSIT_ID` AS `DEPOSIT_ID`,`sm_r`.`RECEPTION_STATUS_ID` AS `RECEPTION_STATUS_ID`,`sm_r`.`DEADLINE` AS `DEADLINE`,`sm_r`.`NOTE` AS `NOTE`,`sm_r`.`TVA` AS `TVA`,`sm_r`.`DELIVERY` AS `DELIVERY`,`sm_r`.`ORDER_SUPPLIER_ID` AS `ORDER_SUPPLIER_ID`,`sm_r`.`ACTIVE` AS `ACTIVE`,`sm_r`.`EXPIRATION_DATE` AS `EXPIRATION_DATE`,`sm_r`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`sm_r`.`DATE_CREATION` AS `DATE_CREATION`,`sm_r`.`USER_CREATION` AS `USER_CREATION`,`sm_r`.`DATE_UPDATE` AS `DATE_UPDATE`,`sm_r`.`USER_UPDATE` AS `USER_UPDATE`,`sm_r`.`REFERENCE` AS `REFERENCE`,`sm_s`.`COMPANY_NAME` AS `supplier_company_name`,`vsm_rs`.`summary_total_ht` AS `total_hors_taxe`,`vsm_rs`.`summary_total_ttc` AS `total_ttc`,`vsm_rs`.`summary_total_quantity` AS `sum_product`,`sm_rs`.`NAME` AS `order_supplier_status_name` from (((`sm_reception` `sm_r` left join `sm_supplier` `sm_s` on((`sm_r`.`SUPPLIER_ID` = `sm_s`.`ID`))) left join `v_sm_reception_summary` `vsm_rs` on((`sm_r`.`ID` = `vsm_rs`.`reception_id`))) left join `sm_reception_status` `sm_rs` on((`sm_rs`.`ID` = `sm_r`.`RECEPTION_STATUS_ID`)));

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_reception_line`
--
DROP TABLE IF EXISTS `v_sm_reception_line`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_reception_line` AS select `sm_rl`.`ID` AS `ID`,`sm_rl`.`PRODUCT_ID` AS `PRODUCT_ID`,`sm_rl`.`DESIGNATION` AS `DESIGNATION`,`sm_rl`.`UNIT_PRICE_BUY` AS `UNIT_PRICE_BUY`,`sm_rl`.`REMISE` AS `REMISE`,`sm_rl`.`QUANTITY` AS `QUANTITY`,`sm_rl`.`TVA` AS `TVA`,`sm_rl`.`ACTIVE` AS `ACTIVE`,`sm_rl`.`RECEPTION_ID` AS `RECEPTION_ID`,`sm_rl`.`DATE_CREATION` AS `DATE_CREATION`,`sm_rl`.`USER_CREATION` AS `USER_CREATION`,`sm_rl`.`DATE_UPDATE` AS `DATE_UPDATE`,`sm_rl`.`USER_UPDATE` AS `USER_UPDATE`,`sm_p`.`REFERENCE` AS `reference`,(`sm_rl`.`QUANTITY` * `sm_rl`.`UNIT_PRICE_BUY`) AS `TOTAL_PRICE_BUY` from (`sm_reception_line` `sm_rl` join `sm_product` `sm_p`) where (`sm_rl`.`PRODUCT_ID` = `sm_p`.`ID`);

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_reception_products`
--
DROP TABLE IF EXISTS `v_sm_reception_products`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_reception_products` AS select `sm_rp`.`ID` AS `ID`,`sm_rp`.`PRODUCT_ID` AS `PRODUCT_ID`,`sm_rp`.`DESIGNATION` AS `DESIGNATION`,round(`sm_rp`.`UNIT_PRICE_BUY`,2) AS `UNIT_PRICE_BUY`,`sm_rp`.`REMISE` AS `REMISE`,`sm_rp`.`QUANTITY` AS `QUANTITY`,`sm_rp`.`TVA` AS `TVA`,`sm_rp`.`ACTIVE` AS `ACTIVE`,`sm_rp`.`RECEPTION_ID` AS `RECEPTION_ID`,`sm_rp`.`DATE_CREATION` AS `DATE_CREATION`,`sm_rp`.`USER_CREATION` AS `USER_CREATION`,`sm_rp`.`DATE_UPDATE` AS `DATE_UPDATE`,`sm_rp`.`USER_UPDATE` AS `USER_UPDATE`,`sm_p`.`REFERENCE` AS `reference`,round(((`sm_rp`.`UNIT_PRICE_BUY` * `sm_rp`.`QUANTITY`) - ((`sm_rp`.`UNIT_PRICE_BUY` * `sm_rp`.`QUANTITY`) * (`sm_rp`.`REMISE` / 100))),2) AS `total_ht`,round((((`sm_rp`.`UNIT_PRICE_BUY` * `sm_rp`.`QUANTITY`) - ((`sm_rp`.`UNIT_PRICE_BUY` * `sm_rp`.`QUANTITY`) * (`sm_rp`.`REMISE` / 100))) * 1.2),2) AS `total_ttc` from (`sm_reception_products` `sm_rp` left join `sm_product` `sm_p` on((`sm_rp`.`PRODUCT_ID` = `sm_p`.`ID`)));

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_reception_summary`
--
DROP TABLE IF EXISTS `v_sm_reception_summary`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_reception_summary` AS select `sm_r`.`ID` AS `reception_id`,sum(`sm_rl`.`QUANTITY`) AS `summary_total_quantity`,sum(`sm_rl`.`UNIT_PRICE_BUY`) AS `summary_total_ht`,((sum(`sm_rl`.`UNIT_PRICE_BUY`) * 1.2) - sum(`sm_rl`.`UNIT_PRICE_BUY`)) AS `summary_tva_amount`,(sum(`sm_rl`.`UNIT_PRICE_BUY`) * 1.2) AS `summary_total_ttc` from (`sm_reception` `sm_r` left join `sm_reception_line` `sm_rl` on((`sm_r`.`ID` = `sm_rl`.`RECEPTION_ID`))) group by `sm_r`.`ID`;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_return_receipt`
--
DROP TABLE IF EXISTS `v_sm_return_receipt`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_return_receipt` AS select `sm_rr`.`ID` AS `ID`,`sm_rr`.`NO_SEQ` AS `NO_SEQ`,`sm_rr`.`REFERENCE` AS `REFERENCE`,`sm_rr`.`CUSTOMER_ID` AS `CUSTOMER_ID`,`sm_rr`.`ORDER_ID` AS `ORDER_ID`,`sm_rr`.`RETURN_RECEIPT_STATUS_ID` AS `RETURN_RECEIPT_STATUS_ID`,`sm_rr`.`NOTE` AS `NOTE`,`sm_rr`.`ACTIVE` AS `ACTIVE`,`sm_rr`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`sm_rr`.`DATE_CREATION` AS `DATE_CREATION`,`sm_rr`.`USER_CREATION` AS `USER_CREATION`,`sm_rr`.`DATE_UPDATE` AS `DATE_UPDATE`,`sm_rr`.`USER_UPDATE` AS `USER_UPDATE`,`sm_c`.`COMPANY_NAME` AS `customer_company_name`,`vsm_rrs`.`summary_total_ht` AS `total_hors_taxe`,`vsm_rrs`.`summary_total_ttc` AS `total_ttc`,`vsm_rrs`.`summary_total_quantity` AS `sum_product`,`sm_rrs`.`NAME` AS `sm_return_receipt_status_name` from (((`sm_return_receipt` `sm_rr` left join `sm_customer` `sm_c` on((`sm_rr`.`CUSTOMER_ID` = `sm_c`.`ID`))) left join `v_sm_return_receipt_summary` `vsm_rrs` on((`sm_rr`.`ID` = `vsm_rrs`.`return_receipt_id`))) left join `sm_return_receipt_status` `sm_rrs` on((`sm_rr`.`RETURN_RECEIPT_STATUS_ID` = `sm_rrs`.`ID`)));

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_return_receipt_line`
--
DROP TABLE IF EXISTS `v_sm_return_receipt_line`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_return_receipt_line` AS select `sm_rrl`.`ID` AS `ID`,`sm_rrl`.`PRODUCT_ID` AS `PRODUCT_ID`,`sm_rrl`.`DESIGNATION` AS `DESIGNATION`,`sm_rrl`.`RETURN_RECEIPT_ID` AS `RETURN_RECEIPT_ID`,`sm_rrl`.`QUANTITY` AS `QUANTITY`,`sm_rrl`.`PROMOTION_ID` AS `PROMOTION_ID`,`sm_rrl`.`NEGOTIATE_PRICE_SALE` AS `NEGOTIATE_PRICE_SALE`,`sm_rrl`.`ACTIVE` AS `ACTIVE`,`sm_rrl`.`DATE_CREATION` AS `DATE_CREATION`,`sm_rrl`.`USER_CREATION` AS `USER_CREATION`,`sm_rrl`.`DATE_UPDATE` AS `DATE_UPDATE`,`sm_rrl`.`USER_UPDATE` AS `USER_UPDATE`,`sm_p`.`REFERENCE` AS `reference`,(`sm_rrl`.`QUANTITY` * `sm_rrl`.`NEGOTIATE_PRICE_SALE`) AS `total_price_sale` from (`sm_return_receipt_line` `sm_rrl` join `sm_product` `sm_p`) where (`sm_rrl`.`PRODUCT_ID` = `sm_p`.`ID`);

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_return_receipt_summary`
--
DROP TABLE IF EXISTS `v_sm_return_receipt_summary`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_return_receipt_summary` AS select `sm_rr`.`ID` AS `return_receipt_id`,sum(`sm_rrl`.`QUANTITY`) AS `summary_total_quantity`,sum(`sm_rrl`.`NEGOTIATE_PRICE_SALE`) AS `summary_total_ht`,((sum(`sm_rrl`.`NEGOTIATE_PRICE_SALE`) * 1.2) - sum(`sm_rrl`.`NEGOTIATE_PRICE_SALE`)) AS `summary_tva_amount`,(sum(`sm_rrl`.`NEGOTIATE_PRICE_SALE`) * 1.2) AS `summary_total_ttc` from (`sm_return_receipt` `sm_rr` left join `sm_return_receipt_line` `sm_rrl` on((`sm_rr`.`ID` = `sm_rrl`.`RETURN_RECEIPT_ID`))) group by `sm_rr`.`ID`;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_size_by_reference`
--
DROP TABLE IF EXISTS `v_sm_size_by_reference`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_size_by_reference` AS select concat_ws('.',`sm_p`.`CLT_MODULE_ID`,`sm_p`.`REFERENCE`,`sm_ps`.`ID`) AS `CODE`,`sm_p`.`REFERENCE` AS `REFERENCE`,`sm_ps`.`ID` AS `PRODUCT_SIZE_ID`,`sm_ps`.`NAME` AS `PRODUCT_SIZE_NAME`,`sm_p`.`CLT_MODULE_ID` AS `CLT_MODULE_ID` from (`sm_product` `sm_p` join `sm_product_size` `sm_ps`) where (`sm_p`.`PRODUCT_SIZE_ID` = `sm_ps`.`ID`) group by `sm_p`.`REFERENCE`,`sm_ps`.`ID`,`sm_ps`.`NAME`,`sm_p`.`CLT_MODULE_ID`;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_supplier`
--
DROP TABLE IF EXISTS `v_sm_supplier`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_supplier` AS select `SM_S`.`ID` AS `ID`,`SM_S`.`FIRST_NAME` AS `FIRST_NAME`,`SM_S`.`LAST_NAME` AS `LAST_NAME`,`SM_S`.`COMPANY_NAME` AS `COMPANY_NAME`,`SM_S`.`REPRESENTATIVE` AS `REPRESENTATIVE`,`SM_S`.`SHORT_LABEL` AS `SHORT_LABEL`,`SM_S`.`FULL_LABEL` AS `FULL_LABEL`,`SM_S`.`NOTE` AS `NOTE`,`SM_S`.`SORT_KEY` AS `SORT_KEY`,`SM_S`.`ACTIVE` AS `ACTIVE`,`SM_S`.`SUPPLIER_TYPE_ID` AS `SUPPLIER_TYPE_ID`,`SM_S`.`SUPPLIER_CATEGORY_ID` AS `SUPPLIER_CATEGORY_ID`,`SM_S`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`SM_S`.`ENTITY_ID` AS `ENTITY_ID`,`SM_S`.`DATE_CREATION` AS `DATE_CREATION`,`SM_S`.`USER_CREATION` AS `USER_CREATION`,`SM_S`.`DATE_UPDATE` AS `DATE_UPDATE`,`SM_S`.`USER_UPDATE` AS `USER_UPDATE`,`SM_S`.`SUPPLIER_NATURE_ID` AS `SUPPLIER_NATURE_ID`,`SM_ST`.`NAME` AS `SUPPLIER_TYPE_NAME`,`SM_SC`.`NAME` AS `SUPPLIER_CATEGORY_NAME`,`vcta_ec`.`ENTITY_PHONE` AS `ENTITY_PHONE`,`vcta_ec`.`ENTITY_EMAIL` AS `ENTITY_EMAIL`,`vcta_ec`.`ENTITY_FAX` AS `ENTITY_FAX`,`vcta_ec`.`ENTITY_WEB` AS `ENTITY_WEB`,`vcta_ec`.`ENTITY_LOCATION_ADDRESS` AS `ENTITY_LOCATION_ADDRESS`,`vcta_ec`.`ENTITY_LOCATION_CITY_NAME` AS `ENTITY_LOCATION_CITY_NAME`,`vcta_ec`.`ENTITY_LOCATION_COUNTRY_NAME` AS `ENTITY_LOCATION_COUNTRY_NAME`,(select (case when (count(0) > 0) then 'N' else 'Y' end) from `sm_reception` `D` where (`D`.`SUPPLIER_ID` = `SM_S`.`ID`)) AS `CAN_BE_BELETE` from (((`sm_supplier` `SM_S` left join `sm_supplier_type` `SM_ST` on((`SM_S`.`SUPPLIER_TYPE_ID` = `SM_ST`.`ID`))) left join `sm_supplier_category` `SM_SC` on((`SM_S`.`SUPPLIER_CATEGORY_ID` = `SM_SC`.`ID`))) left join `v_cta_entity_contact` `VCTA_EC` on((`SM_S`.`ENTITY_ID` = `vcta_ec`.`ID`)))

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_supplier_category`
--
DROP TABLE IF EXISTS `v_sm_supplier_category`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_supplier_category` AS select `sm_supplier_category`.`ID` AS `ID`,`sm_supplier_category`.`NAME` AS `NAME`,`sm_supplier_category`.`DESCRIPTION` AS `DESCRIPTION`,`sm_supplier_category`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`sm_supplier_category`.`SORT_KEY` AS `SORT_KEY`,`sm_supplier_category`.`ACTIVE` AS `ACTIVE`,`sm_supplier_category`.`DATE_CREATION` AS `DATE_CREATION`,`sm_supplier_category`.`USER_CREATION` AS `USER_CREATION`,`sm_supplier_category`.`DATE_UPDATE` AS `DATE_UPDATE`,`sm_supplier_category`.`USER_UPDATE` AS `USER_UPDATE` from `sm_supplier_category`

;

-- --------------------------------------------------------

--
-- Structure de la vue `v_sm_supplier_type`
--
DROP TABLE IF EXISTS `v_sm_supplier_type`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_sm_supplier_type` AS select `sm_supplier_type`.`ID` AS `ID`,`sm_supplier_type`.`NAME` AS `NAME`,`sm_supplier_type`.`DESCRIPTION` AS `DESCRIPTION`,`sm_supplier_type`.`CLT_MODULE_ID` AS `CLT_MODULE_ID`,`sm_supplier_type`.`SORT_KEY` AS `SORT_KEY`,`sm_supplier_type`.`ACTIVE` AS `ACTIVE`,`sm_supplier_type`.`DATE_CREATION` AS `DATE_CREATION`,`sm_supplier_type`.`USER_CREATION` AS `USER_CREATION`,`sm_supplier_type`.`DATE_UPDATE` AS `DATE_UPDATE`,`sm_supplier_type`.`USER_UPDATE` AS `USER_UPDATE` from `sm_supplier_type`

;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `CLT_CLIENT`
--
ALTER TABLE `CLT_CLIENT`
 ADD PRIMARY KEY (`ID`), ADD UNIQUE KEY `CODE_UNIQUE` (`CODE`), ADD KEY `fk_CLT_CLIENT_CLT_CLIENT_STATUS1_idx` (`CLIENT_STATUS_ID`), ADD KEY `FK4_CLT_CLIENT_idx` (`ENTITY_ID`);

--
-- Index pour la table `CLT_CLIENT_LANGUAGE`
--
ALTER TABLE `CLT_CLIENT_LANGUAGE`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_CLT_CLIENT_LANGUAGE_CLT_CLIENT1_idx` (`CLIENT_ID`), ADD KEY `fk_CLT_CLIENT_LANGUAGE_INF_LANGUAGE1_idx` (`LANGUAGE_ID`), ADD KEY `fk_CLT_CLIENT_LANGUAGE_INF_PREFIX_ID` (`INF_PREFIX_ID`);

--
-- Index pour la table `CLT_CLIENT_STATUS`
--
ALTER TABLE `CLT_CLIENT_STATUS`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `CLT_MODEL_PARAMETER`
--
ALTER TABLE `CLT_MODEL_PARAMETER`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `CLT_MODEL_PARAMETER_MODEL`
--
ALTER TABLE `CLT_MODEL_PARAMETER_MODEL`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `CLT_MODEL_PARAMETER_MODULE`
--
ALTER TABLE `CLT_MODEL_PARAMETER_MODULE`
 ADD PRIMARY KEY (`ID`), ADD KEY `idx_clt_module_parameter_client` (`MODEL_PARAMETER_ID`), ADD KEY `idx_clt_module_parameter_client_0` (`MODULE_ID`);

--
-- Index pour la table `CLT_MODEL_PARAMETER_TYPE`
--
ALTER TABLE `CLT_MODEL_PARAMETER_TYPE`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `CLT_MODULE`
--
ALTER TABLE `CLT_MODULE`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_CLT_FOLDER_CLT_CLIENT1_idx` (`CLIENT_ID`), ADD KEY `fk_CLT_MODULE_CLT_MODULE_STATUS1_idx` (`MODULE_STATUS_ID`), ADD KEY `fk_CLT_MODULE_TYPE1_idx` (`MODULE_TYPE_ID`);

--
-- Index pour la table `clt_module_status`
--
ALTER TABLE `clt_module_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `clt_module_type`
--
ALTER TABLE `clt_module_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `clt_parameter`
--
ALTER TABLE `clt_parameter`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_CLT_PARAMETER_CLT_TYPE_PARAMETER1_idx` (`PARAMETER_TYPE_ID`);

--
-- Index pour la table `clt_parameter_client`
--
ALTER TABLE `clt_parameter_client`
 ADD PRIMARY KEY (`ID`), ADD KEY `idx_clt_parameter_client` (`PARAMETER_ID`), ADD KEY `idx_clt_parameter_client_0` (`CLINET_ID`);

--
-- Index pour la table `clt_parameter_type`
--
ALTER TABLE `clt_parameter_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `clt_structure`
--
ALTER TABLE `clt_structure`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `clt_structure_role`
--
ALTER TABLE `clt_structure_role`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `clt_structure_type`
--
ALTER TABLE `clt_structure_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `clt_user`
--
ALTER TABLE `clt_user`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_CLT_USER_CLT_CLIENT1_idx` (`CLIENT_ID`), ADD KEY `fk_CLT_USER_CLT_CATEGORY_USER1_idx` (`CATEGORY_ID`), ADD KEY `idx_clt_user` (`USER_STATUS_ID`), ADD KEY `FK6_CLT_USER_idx` (`ENTITY_ID`);

--
-- Index pour la table `clt_user_category`
--
ALTER TABLE `clt_user_category`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `clt_user_client`
--
ALTER TABLE `clt_user_client`
 ADD PRIMARY KEY (`ID`), ADD KEY `FK_USER_CLIENT_ID_idx` (`CLIENT_ID`), ADD KEY `FK2_USER_CLIENT_idx` (`USER_ID`);

--
-- Index pour la table `clt_user_group`
--
ALTER TABLE `clt_user_group`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_CLT_USER_GROUP_CLT_USER1_idx` (`USER_ID`), ADD KEY `fk_CLT_USER_GROUP_INF_GROUP1_idx` (`INF_GROUP_ID`);

--
-- Index pour la table `clt_user_module`
--
ALTER TABLE `clt_user_module`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_CLT_USER_FOLDER_CLT_USER1_idx` (`USER_ID`), ADD KEY `fk_CLT_USER_FOLDER_CLT_FOLDER1_idx` (`MODULE_ID`);

--
-- Index pour la table `clt_user_status`
--
ALTER TABLE `clt_user_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `CTA_EMAIL_TYPE`
--
ALTER TABLE `CTA_EMAIL_TYPE`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `CTA_ENTITY`
--
ALTER TABLE `CTA_ENTITY`
 ADD PRIMARY KEY (`ID`), ADD KEY `FK1_PARTY_TYPE_ID_idx` (`PARTY_TYPE_ID`);

--
-- Index pour la table `CTA_ENTITY_EMAIL`
--
ALTER TABLE `CTA_ENTITY_EMAIL`
 ADD PRIMARY KEY (`ID`), ADD KEY `FK1_PARTY_ID_idx` (`ENTITY_ID`), ADD KEY `FK2_EMAIL_TYPE_idx` (`EMAIL_TYPE_ID`);

--
-- Index pour la table `CTA_ENTITY_FAX`
--
ALTER TABLE `CTA_ENTITY_FAX`
 ADD PRIMARY KEY (`ID`), ADD KEY `FK1_PARTY_ID_idx` (`ENTITY_ID`), ADD KEY `FK2_FAX_TYPE_idx` (`FAX_TYPE_ID`);

--
-- Index pour la table `CTA_ENTITY_LOCATION`
--
ALTER TABLE `CTA_ENTITY_LOCATION`
 ADD PRIMARY KEY (`ID`), ADD KEY `FK44_PARTY_ID_idx` (`ENTITY_ID`), ADD KEY `FK1_LOCATION_TYPE_idx` (`LOCATION_TYPE_ID`);

--
-- Index pour la table `CTA_ENTITY_PHONE`
--
ALTER TABLE `CTA_ENTITY_PHONE`
 ADD PRIMARY KEY (`ID`), ADD KEY `FK9_PARTY_ID_idx` (`ENTITY_ID`), ADD KEY `FK1_PHONE_TYPE_idx` (`PHONE_TYPE_ID`);

--
-- Index pour la table `CTA_ENTITY_TYPE`
--
ALTER TABLE `CTA_ENTITY_TYPE`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `CTA_ENTITY_WEB`
--
ALTER TABLE `CTA_ENTITY_WEB`
 ADD PRIMARY KEY (`ID`), ADD KEY `FK10_PARTY_ID_idx` (`ENTITY_ID`), ADD KEY `FK1_WEB_TYPE_idx` (`WEB_TYPE_ID`);

--
-- Index pour la table `CTA_FAX_TYPE`
--
ALTER TABLE `CTA_FAX_TYPE`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `CTA_LOCATION_TYPE`
--
ALTER TABLE `CTA_LOCATION_TYPE`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `CTA_PHONE_TYPE`
--
ALTER TABLE `CTA_PHONE_TYPE`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `CTA_WEB_TYPE`
--
ALTER TABLE `CTA_WEB_TYPE`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_basic_parameter`
--
ALTER TABLE `inf_basic_parameter`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_basic_parameter_type`
--
ALTER TABLE `inf_basic_parameter_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_city`
--
ALTER TABLE `inf_city`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_INF_CITY_INF_COUNTRY1_idx` (`COUNTRY_ID`);

--
-- Index pour la table `inf_country`
--
ALTER TABLE `inf_country`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_currency`
--
ALTER TABLE `inf_currency`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_group`
--
ALTER TABLE `inf_group`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_INF_GROUP_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `inf_item`
--
ALTER TABLE `inf_item`
 ADD PRIMARY KEY (`CODE`);

--
-- Index pour la table `inf_language`
--
ALTER TABLE `inf_language`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_lovs`
--
ALTER TABLE `inf_lovs`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `INF_PACKAGE`
--
ALTER TABLE `INF_PACKAGE`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_prefix`
--
ALTER TABLE `inf_prefix`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_privilege`
--
ALTER TABLE `inf_privilege`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_INF_PRIVILEGE_INF_ITEM1_idx` (`ITEM_CODE`), ADD KEY `fk_INF_PRIVILEGE_INF_ROLE1_idx` (`ROLE_ID`);

--
-- Index pour la table `inf_role`
--
ALTER TABLE `inf_role`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_role_group`
--
ALTER TABLE `inf_role_group`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_INF_ROLE_GROUP_INF_GROUP_idx` (`GROUP_ID`), ADD KEY `fk_INF_ROLE_GROUP_INF_ROLE1_idx` (`ROLE_ID`);

--
-- Index pour la table `inf_text`
--
ALTER TABLE `inf_text`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_INF_TEXT_INF_PREFIX1_idx` (`PREFIX`), ADD KEY `fk_inf_text` (`ITEM_CODE`);

--
-- Index pour la table `inf_text_type`
--
ALTER TABLE `inf_text_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_attribute_validation`
--
ALTER TABLE `pm_attribute_validation`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_VALID_SIMPLE_PAGE_PM_VALID_SIMPLE1_idx` (`VALIDATION_VALIDATION_ID`), ADD KEY `fk1_pm_attribute_validation` (`MODEL_ID`);

--
-- Index pour la table `pm_category`
--
ALTER TABLE `pm_category`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_MENU_PM_MENU_TYPE1_idx` (`CATEGORY_TYPE_ID`);

--
-- Index pour la table `pm_category_type`
--
ALTER TABLE `pm_category_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_component`
--
ALTER TABLE `pm_component`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_composition`
--
ALTER TABLE `pm_composition`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_COMPOSITION_PM_MENU1_idx` (`MENU_ID`), ADD KEY `fk_PM_COMPOSITION_PM_PAGE1_idx` (`PAGE_ID`), ADD KEY `FK2` (`GROUP_ID`), ADD KEY `FK3` (`CATEGORY_ID`), ADD KEY `fk1_pm_composition` (`MODEL_ID`);

--
-- Index pour la table `pm_data_type`
--
ALTER TABLE `pm_data_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_format_type`
--
ALTER TABLE `pm_format_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_group`
--
ALTER TABLE `pm_group`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_GROUP_PM_GROUP_TYPE1_idx` (`GROUP_TYPE_ID`);

--
-- Index pour la table `pm_group_type`
--
ALTER TABLE `pm_group_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_menu`
--
ALTER TABLE `pm_menu`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_SECTION_PM_SECTION_TYPE1_idx` (`MENU_TYPE_ID`);

--
-- Index pour la table `pm_menu_type`
--
ALTER TABLE `pm_menu_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_model`
--
ALTER TABLE `pm_model`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk1_pm_model_idx` (`INF_PACKAGE_ID`);

--
-- Index pour la table `pm_model_status`
--
ALTER TABLE `pm_model_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_page`
--
ALTER TABLE `pm_page`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_PAGE_PM_TYPE_PAGE1_idx` (`PAGE_TYPE_ID`);

--
-- Index pour la table `pm_page_attribute`
--
ALTER TABLE `pm_page_attribute`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_PAGE_ITEM_PM_PAGE1_idx` (`PAGE_ID`), ADD KEY `fk_PM_PAGE_ITEM_INF_ITEM1_idx` (`INF_ITEM_CODE`), ADD KEY `fk_PM_PAGE_ITEM_PM_COMPONENTE1_idx` (`PM_COMPONENT_ID`), ADD KEY `fk_data_type1_idx` (`DATA_TYPE_ID`), ADD KEY `fk_foramt_type1_idx` (`FORMAT_TYPE_ID`);

--
-- Index pour la table `pm_page_attribute_model`
--
ALTER TABLE `pm_page_attribute_model`
 ADD PRIMARY KEY (`ID`), ADD KEY `idx_pm_page_attribute_model` (`PAGE_ATTRIBUTE_ID`);

--
-- Index pour la table `pm_page_attribute_module`
--
ALTER TABLE `pm_page_attribute_module`
 ADD PRIMARY KEY (`ID`), ADD KEY `idx_pm_page_attribute_module` (`PAGE_ATTRIBUTE_ID`);

--
-- Index pour la table `pm_page_parameter`
--
ALTER TABLE `pm_page_parameter`
 ADD PRIMARY KEY (`ID`), ADD KEY `FK1_PAGE_PAPRAMETER_idx` (`PAGE_PARAMETER_TYPE_ID`), ADD KEY `idx_pm_page_parameter` (`PAGE_ID`);

--
-- Index pour la table `pm_page_parameter_model`
--
ALTER TABLE `pm_page_parameter_model`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_page_parameter_module`
--
ALTER TABLE `pm_page_parameter_module`
 ADD PRIMARY KEY (`ID`), ADD KEY `idx_pm_page_parameter_module` (`PAGE_PARAMETER_ID`);

--
-- Index pour la table `pm_page_parameter_type`
--
ALTER TABLE `pm_page_parameter_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_page_type`
--
ALTER TABLE `pm_page_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_validation_type`
--
ALTER TABLE `pm_validation_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_advanced`
--
ALTER TABLE `sm_advanced`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_ADVANCED_SM_ADVANCED_STATUS1_idx` (`ADVANCED_STATUS_ID`), ADD KEY `fk_SM_ADVANCED_SM_CUSTOMER1_idx` (`CUSTOMER_ID`), ADD KEY `fk_SM_ADVANCED_SM_PRODUCT1_idx` (`PRODUCT_ID`);

--
-- Index pour la table `sm_advanced_status`
--
ALTER TABLE `sm_advanced_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_bank_type`
--
ALTER TABLE `sm_bank_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_check`
--
ALTER TABLE `sm_check`
 ADD PRIMARY KEY (`ID`), ADD UNIQUE KEY `pk_sm_check_0` (`BANK_ID`);

--
-- Index pour la table `sm_customer`
--
ALTER TABLE `sm_customer`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_CUSTOMER_CLT_MODULE1_idx` (`CLT_MODULE_ID`), ADD KEY `idx_sm_customer` (`CUSTOMER_TYPE_ID`), ADD KEY `FK3_SM_CUSTOMER_idx` (`ENTITY_ID`);

--
-- Index pour la table `sm_customer_category`
--
ALTER TABLE `sm_customer_category`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_customer_type`
--
ALTER TABLE `sm_customer_type`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_CUSTOMER_TYPE_CLT_FOLDER1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `sm_deposit`
--
ALTER TABLE `sm_deposit`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_expense`
--
ALTER TABLE `sm_expense`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_EXPENSE_SM_EXPENSE_TYPE1_idx` (`EXPENSE_TYPE_ID`), ADD KEY `fk_SM_EXPENSE_CLT_FOLDER1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `sm_expense_type`
--
ALTER TABLE `sm_expense_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `SM_JUSTIFICATION_STATUS`
--
ALTER TABLE `SM_JUSTIFICATION_STATUS`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `SM_JUSTIFICATION_STATUS_CATEGORY`
--
ALTER TABLE `SM_JUSTIFICATION_STATUS_CATEGORY`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `SM_ORDER`
--
ALTER TABLE `SM_ORDER`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_ORDER_SM_ORDER_STATUS1_idx` (`ORDER_STATUS_ID`), ADD KEY `fk_SM_ORDER_SM_PAYMENT_METHOD1_idx` (`PAYMENT_METHOD_ID`), ADD KEY `fk_SM_ORDER_SM_CUSTOMER1_idx` (`CUSTOMER_ID`), ADD KEY `fk_SM_ORDER_SM_CHECK1_idx` (`CHECK_ID`);

--
-- Index pour la table `sm_order_line`
--
ALTER TABLE `sm_order_line`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_ORDER_LINE_SM_PRODUCT1_idx` (`PRODUCT_ID`), ADD KEY `fk_SM_ORDER_LINE_SM_ORDER1_idx` (`ORDER_ID`);

--
-- Index pour la table `sm_order_status`
--
ALTER TABLE `sm_order_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `SM_ORDER_STATUS_LOG`
--
ALTER TABLE `SM_ORDER_STATUS_LOG`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_order_supplier`
--
ALTER TABLE `sm_order_supplier`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk1_sm_order_supplier_idx` (`ORDER_SUPPLIER_STATUS_ID`);

--
-- Index pour la table `sm_order_supplier_line`
--
ALTER TABLE `sm_order_supplier_line`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk1_sm_order_supplier_line_idx` (`ORDER_SUPPLIER_ID`), ADD KEY `fk2_sm_order_supplier_line_idx` (`PRODUCT_ID`);

--
-- Index pour la table `sm_order_supplier_status`
--
ALTER TABLE `sm_order_supplier_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `SM_ORDER_SUPPLIER_STATUS_LOG`
--
ALTER TABLE `SM_ORDER_SUPPLIER_STATUS_LOG`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_payment_method`
--
ALTER TABLE `sm_payment_method`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_PAYMENT_METHOD_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `sm_product`
--
ALTER TABLE `sm_product`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_PRODUCT_SM_PRODUCT_STATUS1_idx` (`PRODUCT_STATUS_ID`), ADD KEY `fk_SM_PRODUCT_SM_PRODUCT_FAMILY1_idx` (`PRODUCT_FAMILY_ID`), ADD KEY `fk_SM_PRODUCT_SM_PRODUCT_SIZE1_idx` (`PRODUCT_SIZE_ID`), ADD KEY `fk_SM_PRODUCT_SM_PRODUCT_COLOR1_idx` (`PRODUCT_COLOR_ID`);

--
-- Index pour la table `sm_product_color`
--
ALTER TABLE `sm_product_color`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_PRODUCT_COLOR_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `sm_product_department`
--
ALTER TABLE `sm_product_department`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_product_family`
--
ALTER TABLE `sm_product_family`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_PRODUCT_FAMILY_SM_PRODUCT_GROUP1_idx` (`PRODUCT_GROUP_ID`), ADD KEY `fk_SM_PRODUCT_FAMILY_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `sm_product_group`
--
ALTER TABLE `sm_product_group`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_PRODUCT_GROUP_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `sm_product_size`
--
ALTER TABLE `sm_product_size`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_product_status`
--
ALTER TABLE `sm_product_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_product_type`
--
ALTER TABLE `sm_product_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_product_unit`
--
ALTER TABLE `sm_product_unit`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_promotion`
--
ALTER TABLE `sm_promotion`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_PROMOTION_SM_PROMOTION_TYPE1_idx` (`PROMOTION_TYPE_ID`), ADD KEY `fk_SM_PROMOTION_SM_PRODUCT1_idx` (`PRODUCT`);

--
-- Index pour la table `sm_promotion_type`
--
ALTER TABLE `sm_promotion_type`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_PROMOTION_TYPE_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `sm_reception`
--
ALTER TABLE `sm_reception`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SPM_RECEPTION_STATUS_idx` (`RECEPTION_STATUS_ID`), ADD KEY `fk1_sm_reception_idx` (`SUPPLIER_ID`), ADD KEY `fk3_sm_reception_idx` (`ORDER_SUPPLIER_ID`);

--
-- Index pour la table `sm_reception_line`
--
ALTER TABLE `sm_reception_line`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk2_sm_reception_line_idx` (`RECEPTION_ID`), ADD KEY `fk1_sm_reception_line_idx` (`PRODUCT_ID`);

--
-- Index pour la table `sm_reception_products`
--
ALTER TABLE `sm_reception_products`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk1_reception_id_idx` (`RECEPTION_ID`);

--
-- Index pour la table `sm_reception_status`
--
ALTER TABLE `sm_reception_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `SM_RECEPTION_STATUS_LOG`
--
ALTER TABLE `SM_RECEPTION_STATUS_LOG`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_return_receipt`
--
ALTER TABLE `sm_return_receipt`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk1_sm_return_receipt_idx` (`RETURN_RECEIPT_STATUS_ID`), ADD KEY `fk2_sm_return_receipt_idx` (`ORDER_ID`);

--
-- Index pour la table `sm_return_receipt_line`
--
ALTER TABLE `sm_return_receipt_line`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk1_ sm_return_receipt_line_idx` (`RETURN_RECEIPT_ID`), ADD KEY `fk2_ sm_return_receipt_line_idx` (`PRODUCT_ID`);

--
-- Index pour la table `sm_return_receipt_status`
--
ALTER TABLE `sm_return_receipt_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `SM_RETURN_RECEIPT_STATUS_LOG`
--
ALTER TABLE `SM_RETURN_RECEIPT_STATUS_LOG`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_supplier`
--
ALTER TABLE `sm_supplier`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_SUPPLIER_SM_SUPPLIER_TYPE1_idx` (`SUPPLIER_TYPE_ID`), ADD KEY `FK2_SUPPLIER_idx` (`CLT_MODULE_ID`), ADD KEY `FK3_SUPPLIER_idx` (`ENTITY_ID`);

--
-- Index pour la table `sm_supplier_category`
--
ALTER TABLE `sm_supplier_category`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_supplier_type`
--
ALTER TABLE `sm_supplier_type`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_SUPPLIER_TYPE_CLT_FOLDER1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `tmp_pm_attribute_exclud`
--
ALTER TABLE `tmp_pm_attribute_exclud`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_COMPOS_EXECLUD_PM_PAGE_ITEM1_idx` (`PAGE_ID`), ADD KEY `fk1_pm_attribute_exclud` (`MODEL_ID`);

--
-- Index pour la table `tmp_pm_model_attribute`
--
ALTER TABLE `tmp_pm_model_attribute`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_PAGE_ATTRIBUTE1_idx` (`PM_PAGE_ATTRIBUTE_ID`), ADD KEY `fk_DATA_TYPE2_idx` (`DATA_TYPE_ID`), ADD KEY `FORMAT_TYPE_ID2_idx` (`FORMAT_TYPE_ID`), ADD KEY `fk1_pm_model_attribute` (`MODEL_ID`);

--
-- Index pour la table `tmp_pm_model_pdf_attribute`
--
ALTER TABLE `tmp_pm_model_pdf_attribute`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk1_pm_model_pdf_attribute` (`MODEL_ID`);

--
-- Index pour la table `tmp_pm_pdf`
--
ALTER TABLE `tmp_pm_pdf`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `tmp_pm_pdf_attribute`
--
ALTER TABLE `tmp_pm_pdf_attribute`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `tmp_pm_pdf_attribute_exclud`
--
ALTER TABLE `tmp_pm_pdf_attribute_exclud`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk1_pm_pdf_attribute_exclud` (`MODEL_ID`);

--
-- Index pour la table `tmp_pm_pdf_parameter`
--
ALTER TABLE `tmp_pm_pdf_parameter`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `tmp_pm_pdf_parameter_module`
--
ALTER TABLE `tmp_pm_pdf_parameter_module`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk1_pm_pdf_parameter_model` (`MODEL_ID`);

--
-- Index pour la table `tmp_pm_pdf_parameter_type`
--
ALTER TABLE `tmp_pm_pdf_parameter_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `tmp_pm_pdf_type`
--
ALTER TABLE `tmp_pm_pdf_type`
 ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `CLT_CLIENT`
--
ALTER TABLE `CLT_CLIENT`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `CLT_CLIENT_LANGUAGE`
--
ALTER TABLE `CLT_CLIENT_LANGUAGE`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `CLT_CLIENT_STATUS`
--
ALTER TABLE `CLT_CLIENT_STATUS`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `CLT_MODEL_PARAMETER`
--
ALTER TABLE `CLT_MODEL_PARAMETER`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `CLT_MODEL_PARAMETER_MODEL`
--
ALTER TABLE `CLT_MODEL_PARAMETER_MODEL`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `CLT_MODEL_PARAMETER_MODULE`
--
ALTER TABLE `CLT_MODEL_PARAMETER_MODULE`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `CLT_MODEL_PARAMETER_TYPE`
--
ALTER TABLE `CLT_MODEL_PARAMETER_TYPE`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `CLT_MODULE`
--
ALTER TABLE `CLT_MODULE`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `clt_module_status`
--
ALTER TABLE `clt_module_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `clt_module_type`
--
ALTER TABLE `clt_module_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `clt_parameter`
--
ALTER TABLE `clt_parameter`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `clt_parameter_client`
--
ALTER TABLE `clt_parameter_client`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `clt_structure`
--
ALTER TABLE `clt_structure`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `clt_structure_role`
--
ALTER TABLE `clt_structure_role`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `clt_structure_type`
--
ALTER TABLE `clt_structure_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `clt_user`
--
ALTER TABLE `clt_user`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `clt_user_category`
--
ALTER TABLE `clt_user_category`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `clt_user_client`
--
ALTER TABLE `clt_user_client`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `clt_user_group`
--
ALTER TABLE `clt_user_group`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `clt_user_module`
--
ALTER TABLE `clt_user_module`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT pour la table `clt_user_status`
--
ALTER TABLE `clt_user_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `CTA_EMAIL_TYPE`
--
ALTER TABLE `CTA_EMAIL_TYPE`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `CTA_ENTITY`
--
ALTER TABLE `CTA_ENTITY`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT pour la table `CTA_ENTITY_EMAIL`
--
ALTER TABLE `CTA_ENTITY_EMAIL`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `CTA_ENTITY_FAX`
--
ALTER TABLE `CTA_ENTITY_FAX`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `CTA_ENTITY_LOCATION`
--
ALTER TABLE `CTA_ENTITY_LOCATION`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `CTA_ENTITY_PHONE`
--
ALTER TABLE `CTA_ENTITY_PHONE`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `CTA_ENTITY_TYPE`
--
ALTER TABLE `CTA_ENTITY_TYPE`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `CTA_ENTITY_WEB`
--
ALTER TABLE `CTA_ENTITY_WEB`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `CTA_FAX_TYPE`
--
ALTER TABLE `CTA_FAX_TYPE`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `CTA_LOCATION_TYPE`
--
ALTER TABLE `CTA_LOCATION_TYPE`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `CTA_PHONE_TYPE`
--
ALTER TABLE `CTA_PHONE_TYPE`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `CTA_WEB_TYPE`
--
ALTER TABLE `CTA_WEB_TYPE`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `inf_basic_parameter`
--
ALTER TABLE `inf_basic_parameter`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `inf_basic_parameter_type`
--
ALTER TABLE `inf_basic_parameter_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `inf_city`
--
ALTER TABLE `inf_city`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `inf_country`
--
ALTER TABLE `inf_country`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `inf_currency`
--
ALTER TABLE `inf_currency`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `inf_group`
--
ALTER TABLE `inf_group`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `inf_language`
--
ALTER TABLE `inf_language`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `inf_lovs`
--
ALTER TABLE `inf_lovs`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `inf_prefix`
--
ALTER TABLE `inf_prefix`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `inf_privilege`
--
ALTER TABLE `inf_privilege`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `inf_role`
--
ALTER TABLE `inf_role`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `inf_role_group`
--
ALTER TABLE `inf_role_group`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `inf_text`
--
ALTER TABLE `inf_text`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1056;
--
-- AUTO_INCREMENT pour la table `inf_text_type`
--
ALTER TABLE `inf_text_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `pm_attribute_validation`
--
ALTER TABLE `pm_attribute_validation`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pm_category`
--
ALTER TABLE `pm_category`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `pm_category_type`
--
ALTER TABLE `pm_category_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `pm_component`
--
ALTER TABLE `pm_component`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `pm_composition`
--
ALTER TABLE `pm_composition`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=219;
--
-- AUTO_INCREMENT pour la table `pm_data_type`
--
ALTER TABLE `pm_data_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `pm_format_type`
--
ALTER TABLE `pm_format_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT pour la table `pm_group`
--
ALTER TABLE `pm_group`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `pm_group_type`
--
ALTER TABLE `pm_group_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `pm_menu`
--
ALTER TABLE `pm_menu`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT pour la table `pm_menu_type`
--
ALTER TABLE `pm_menu_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `pm_model`
--
ALTER TABLE `pm_model`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `pm_model_status`
--
ALTER TABLE `pm_model_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pm_page`
--
ALTER TABLE `pm_page`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1009;
--
-- AUTO_INCREMENT pour la table `pm_page_attribute`
--
ALTER TABLE `pm_page_attribute`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=143;
--
-- AUTO_INCREMENT pour la table `pm_page_attribute_model`
--
ALTER TABLE `pm_page_attribute_model`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pm_page_attribute_module`
--
ALTER TABLE `pm_page_attribute_module`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pm_page_parameter`
--
ALTER TABLE `pm_page_parameter`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `pm_page_parameter_model`
--
ALTER TABLE `pm_page_parameter_model`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pm_page_parameter_module`
--
ALTER TABLE `pm_page_parameter_module`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `pm_page_parameter_type`
--
ALTER TABLE `pm_page_parameter_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `pm_page_type`
--
ALTER TABLE `pm_page_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `pm_validation_type`
--
ALTER TABLE `pm_validation_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `sm_advanced`
--
ALTER TABLE `sm_advanced`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_advanced_status`
--
ALTER TABLE `sm_advanced_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `sm_bank_type`
--
ALTER TABLE `sm_bank_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_check`
--
ALTER TABLE `sm_check`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_customer`
--
ALTER TABLE `sm_customer`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT pour la table `sm_customer_category`
--
ALTER TABLE `sm_customer_category`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `sm_customer_type`
--
ALTER TABLE `sm_customer_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `sm_deposit`
--
ALTER TABLE `sm_deposit`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `sm_expense`
--
ALTER TABLE `sm_expense`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT pour la table `sm_expense_type`
--
ALTER TABLE `sm_expense_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT pour la table `SM_JUSTIFICATION_STATUS`
--
ALTER TABLE `SM_JUSTIFICATION_STATUS`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `SM_JUSTIFICATION_STATUS_CATEGORY`
--
ALTER TABLE `SM_JUSTIFICATION_STATUS_CATEGORY`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `SM_ORDER`
--
ALTER TABLE `SM_ORDER`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT pour la table `sm_order_line`
--
ALTER TABLE `sm_order_line`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT pour la table `sm_order_status`
--
ALTER TABLE `sm_order_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `SM_ORDER_STATUS_LOG`
--
ALTER TABLE `SM_ORDER_STATUS_LOG`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_order_supplier`
--
ALTER TABLE `sm_order_supplier`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT pour la table `sm_order_supplier_line`
--
ALTER TABLE `sm_order_supplier_line`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT pour la table `sm_order_supplier_status`
--
ALTER TABLE `sm_order_supplier_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `SM_ORDER_SUPPLIER_STATUS_LOG`
--
ALTER TABLE `SM_ORDER_SUPPLIER_STATUS_LOG`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_payment_method`
--
ALTER TABLE `sm_payment_method`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `sm_product`
--
ALTER TABLE `sm_product`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT pour la table `sm_product_color`
--
ALTER TABLE `sm_product_color`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `sm_product_department`
--
ALTER TABLE `sm_product_department`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `sm_product_family`
--
ALTER TABLE `sm_product_family`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `sm_product_group`
--
ALTER TABLE `sm_product_group`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `sm_product_size`
--
ALTER TABLE `sm_product_size`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT pour la table `sm_product_status`
--
ALTER TABLE `sm_product_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `sm_product_type`
--
ALTER TABLE `sm_product_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `sm_product_unit`
--
ALTER TABLE `sm_product_unit`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `sm_promotion`
--
ALTER TABLE `sm_promotion`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_promotion_type`
--
ALTER TABLE `sm_promotion_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_reception`
--
ALTER TABLE `sm_reception`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT pour la table `sm_reception_line`
--
ALTER TABLE `sm_reception_line`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT pour la table `sm_reception_products`
--
ALTER TABLE `sm_reception_products`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_reception_status`
--
ALTER TABLE `sm_reception_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `SM_RECEPTION_STATUS_LOG`
--
ALTER TABLE `SM_RECEPTION_STATUS_LOG`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_return_receipt`
--
ALTER TABLE `sm_return_receipt`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `sm_return_receipt_line`
--
ALTER TABLE `sm_return_receipt_line`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `sm_return_receipt_status`
--
ALTER TABLE `sm_return_receipt_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `SM_RETURN_RECEIPT_STATUS_LOG`
--
ALTER TABLE `SM_RETURN_RECEIPT_STATUS_LOG`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_supplier`
--
ALTER TABLE `sm_supplier`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT pour la table `sm_supplier_category`
--
ALTER TABLE `sm_supplier_category`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `sm_supplier_type`
--
ALTER TABLE `sm_supplier_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `tmp_pm_attribute_exclud`
--
ALTER TABLE `tmp_pm_attribute_exclud`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `tmp_pm_model_attribute`
--
ALTER TABLE `tmp_pm_model_attribute`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `tmp_pm_model_pdf_attribute`
--
ALTER TABLE `tmp_pm_model_pdf_attribute`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `tmp_pm_pdf`
--
ALTER TABLE `tmp_pm_pdf`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `tmp_pm_pdf_attribute`
--
ALTER TABLE `tmp_pm_pdf_attribute`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `tmp_pm_pdf_attribute_exclud`
--
ALTER TABLE `tmp_pm_pdf_attribute_exclud`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `tmp_pm_pdf_parameter`
--
ALTER TABLE `tmp_pm_pdf_parameter`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `tmp_pm_pdf_parameter_module`
--
ALTER TABLE `tmp_pm_pdf_parameter_module`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `tmp_pm_pdf_parameter_type`
--
ALTER TABLE `tmp_pm_pdf_parameter_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `tmp_pm_pdf_type`
--
ALTER TABLE `tmp_pm_pdf_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `CLT_CLIENT`
--
ALTER TABLE `CLT_CLIENT`
ADD CONSTRAINT `FK1_CLT_CLIENT` FOREIGN KEY (`CLIENT_STATUS_ID`) REFERENCES `clt_client_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK4_CLT_CLIENT` FOREIGN KEY (`ENTITY_ID`) REFERENCES `CTA_ENTITY` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `CLT_CLIENT_LANGUAGE`
--
ALTER TABLE `CLT_CLIENT_LANGUAGE`
ADD CONSTRAINT `fk_CLT_CLIENT_LANGUAGE_CLT_CLIENT1` FOREIGN KEY (`CLIENT_ID`) REFERENCES `clt_client` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_CLIENT_LANGUAGE_INF_PREFIX_ID` FOREIGN KEY (`INF_PREFIX_ID`) REFERENCES `inf_prefix` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_CLIENT_LANGUAGE_LANGUAGE_ID` FOREIGN KEY (`LANGUAGE_ID`) REFERENCES `inf_language` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CLT_MODEL_PARAMETER_MODULE`
--
ALTER TABLE `CLT_MODEL_PARAMETER_MODULE`
ADD CONSTRAINT `fk_clt_module_parameter_client` FOREIGN KEY (`MODEL_PARAMETER_ID`) REFERENCES `clt_model_parameter` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_clt_module_parameter_client_0` FOREIGN KEY (`MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CLT_MODULE`
--
ALTER TABLE `CLT_MODULE`
ADD CONSTRAINT `fk_CLT_MODULE_CLT_CLIENT1` FOREIGN KEY (`CLIENT_ID`) REFERENCES `clt_client` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_MODULE_CLT_MODULE_STATUS1` FOREIGN KEY (`MODULE_STATUS_ID`) REFERENCES `clt_module_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_MODULE_TYPE1` FOREIGN KEY (`MODULE_TYPE_ID`) REFERENCES `clt_module_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `clt_parameter`
--
ALTER TABLE `clt_parameter`
ADD CONSTRAINT `fk_clt_parameter` FOREIGN KEY (`PARAMETER_TYPE_ID`) REFERENCES `clt_parameter_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `clt_parameter_client`
--
ALTER TABLE `clt_parameter_client`
ADD CONSTRAINT `fk_clt_parameter_client` FOREIGN KEY (`PARAMETER_ID`) REFERENCES `clt_parameter` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_clt_parameter_client_0` FOREIGN KEY (`CLINET_ID`) REFERENCES `clt_client` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `clt_user`
--
ALTER TABLE `clt_user`
ADD CONSTRAINT `FK1_CLT_USER` FOREIGN KEY (`USER_STATUS_ID`) REFERENCES `clt_user_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK2_CLT_USER` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `clt_user_category` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK3_CLT_USER` FOREIGN KEY (`CLIENT_ID`) REFERENCES `clt_client` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK6_CLT_USER` FOREIGN KEY (`ENTITY_ID`) REFERENCES `CTA_ENTITY` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `clt_user_client`
--
ALTER TABLE `clt_user_client`
ADD CONSTRAINT `FK1_USER_CLIENT` FOREIGN KEY (`CLIENT_ID`) REFERENCES `clt_client` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK2_USER_CLIENT` FOREIGN KEY (`USER_ID`) REFERENCES `clt_user` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `clt_user_group`
--
ALTER TABLE `clt_user_group`
ADD CONSTRAINT `fk_CLT_USER_GROUP_CLT_USER1` FOREIGN KEY (`USER_ID`) REFERENCES `clt_user` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_USER_GROUP_INF_GROUP1` FOREIGN KEY (`INF_GROUP_ID`) REFERENCES `inf_group` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `clt_user_module`
--
ALTER TABLE `clt_user_module`
ADD CONSTRAINT `fk_CLT_USER_FOLDER_CLT_FOLDER1` FOREIGN KEY (`MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_USER_FOLDER_CLT_USER1` FOREIGN KEY (`USER_ID`) REFERENCES `clt_user` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CTA_ENTITY`
--
ALTER TABLE `CTA_ENTITY`
ADD CONSTRAINT `FK1_PARTY_TYPE_ID` FOREIGN KEY (`PARTY_TYPE_ID`) REFERENCES `cta_entity_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CTA_ENTITY_EMAIL`
--
ALTER TABLE `CTA_ENTITY_EMAIL`
ADD CONSTRAINT `FK1_CTA_ENTITY_EMAIL` FOREIGN KEY (`ENTITY_ID`) REFERENCES `cta_entity` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK2_CTA_ENTITY_EMAIL` FOREIGN KEY (`EMAIL_TYPE_ID`) REFERENCES `cta_email_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CTA_ENTITY_FAX`
--
ALTER TABLE `CTA_ENTITY_FAX`
ADD CONSTRAINT `FK1_CTA_ENTITY_FAX` FOREIGN KEY (`FAX_TYPE_ID`) REFERENCES `cta_fax_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK2_CTA_ENTITY_FAX` FOREIGN KEY (`ENTITY_ID`) REFERENCES `cta_entity` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `CTA_ENTITY_LOCATION`
--
ALTER TABLE `CTA_ENTITY_LOCATION`
ADD CONSTRAINT `FK_1_CTA_ENTITY_LOCATION` FOREIGN KEY (`ENTITY_ID`) REFERENCES `cta_entity` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `FK_2_ CTA_ENTITY_LOCATION` FOREIGN KEY (`LOCATION_TYPE_ID`) REFERENCES `CTA_LOCATION_TYPE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `CTA_ENTITY_PHONE`
--
ALTER TABLE `CTA_ENTITY_PHONE`
ADD CONSTRAINT `FK1_CTA_ENTITY_PHONE` FOREIGN KEY (`PHONE_TYPE_ID`) REFERENCES `cta_phone_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK2_CTA_ENTITY_PHONE` FOREIGN KEY (`ENTITY_ID`) REFERENCES `cta_entity` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `CTA_ENTITY_WEB`
--
ALTER TABLE `CTA_ENTITY_WEB`
ADD CONSTRAINT `FK1_CTA_ENTITY_WEB` FOREIGN KEY (`WEB_TYPE_ID`) REFERENCES `CTA_WEB_TYPE` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK2_CTA_ENTITY_WEB` FOREIGN KEY (`ENTITY_ID`) REFERENCES `CTA_ENTITY` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `inf_city`
--
ALTER TABLE `inf_city`
ADD CONSTRAINT `fk_INF_CITY_INF_COUNTRY1` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `inf_country` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `inf_group`
--
ALTER TABLE `inf_group`
ADD CONSTRAINT `fk_INF_GROUP_CLT_MODULE1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `inf_privilege`
--
ALTER TABLE `inf_privilege`
ADD CONSTRAINT `fk_inf_privilege` FOREIGN KEY (`ITEM_CODE`) REFERENCES `inf_item` (`CODE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_INF_PRIVILEGE_INF_ROLE1` FOREIGN KEY (`ROLE_ID`) REFERENCES `inf_role` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `inf_role_group`
--
ALTER TABLE `inf_role_group`
ADD CONSTRAINT `fk_INF_ROLE_GROUP_INF_GROUP` FOREIGN KEY (`GROUP_ID`) REFERENCES `inf_group` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_INF_ROLE_GROUP_INF_ROLE1` FOREIGN KEY (`ROLE_ID`) REFERENCES `inf_role` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_attribute_validation`
--
ALTER TABLE `pm_attribute_validation`
ADD CONSTRAINT `fk1_pm_attribute_validation` FOREIGN KEY (`MODEL_ID`) REFERENCES `pm_model` (`ID`);

--
-- Contraintes pour la table `pm_category`
--
ALTER TABLE `pm_category`
ADD CONSTRAINT `fk_pm_category` FOREIGN KEY (`CATEGORY_TYPE_ID`) REFERENCES `pm_category_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_composition`
--
ALTER TABLE `pm_composition`
ADD CONSTRAINT `fk1_pm_composition` FOREIGN KEY (`MODEL_ID`) REFERENCES `pm_model` (`ID`),
ADD CONSTRAINT `FK2` FOREIGN KEY (`GROUP_ID`) REFERENCES `pm_group` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK3` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `pm_category` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK4` FOREIGN KEY (`MENU_ID`) REFERENCES `pm_menu` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK5` FOREIGN KEY (`PAGE_ID`) REFERENCES `pm_page` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_group`
--
ALTER TABLE `pm_group`
ADD CONSTRAINT `fk_pm_group` FOREIGN KEY (`GROUP_TYPE_ID`) REFERENCES `pm_group_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_menu`
--
ALTER TABLE `pm_menu`
ADD CONSTRAINT `fk_PM_SECTION_PM_SECTION_TYPE1` FOREIGN KEY (`MENU_TYPE_ID`) REFERENCES `pm_menu_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_model`
--
ALTER TABLE `pm_model`
ADD CONSTRAINT `fk1_pm_model` FOREIGN KEY (`INF_PACKAGE_ID`) REFERENCES `inf_package` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_page`
--
ALTER TABLE `pm_page`
ADD CONSTRAINT `fk_PM_PAGE_PM_TYPE_PAGE1` FOREIGN KEY (`PAGE_TYPE_ID`) REFERENCES `pm_page_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_page_attribute`
--
ALTER TABLE `pm_page_attribute`
ADD CONSTRAINT `fk_data_type1` FOREIGN KEY (`DATA_TYPE_ID`) REFERENCES `pm_data_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_foramt_type1` FOREIGN KEY (`FORMAT_TYPE_ID`) REFERENCES `pm_format_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_PM_PAGE_ITEM_PM_COMPONENTE1` FOREIGN KEY (`PM_COMPONENT_ID`) REFERENCES `pm_component` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_PM_PAGE_ITEM_PM_PAGE1` FOREIGN KEY (`PAGE_ID`) REFERENCES `pm_page` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_page_attribute_model`
--
ALTER TABLE `pm_page_attribute_model`
ADD CONSTRAINT `fk_pm_page_attribute_model` FOREIGN KEY (`PAGE_ATTRIBUTE_ID`) REFERENCES `pm_page_attribute` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_page_attribute_module`
--
ALTER TABLE `pm_page_attribute_module`
ADD CONSTRAINT `fk_pm_page_attribute_module` FOREIGN KEY (`PAGE_ATTRIBUTE_ID`) REFERENCES `pm_page_attribute` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_page_parameter`
--
ALTER TABLE `pm_page_parameter`
ADD CONSTRAINT `FK1_PAGE_PAPRAMETER` FOREIGN KEY (`PAGE_PARAMETER_TYPE_ID`) REFERENCES `pm_page_parameter_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_pm_page_parameter` FOREIGN KEY (`PAGE_ID`) REFERENCES `pm_page` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_page_parameter_module`
--
ALTER TABLE `pm_page_parameter_module`
ADD CONSTRAINT `fk_pm_page_parameter_module` FOREIGN KEY (`PAGE_PARAMETER_ID`) REFERENCES `pm_page_parameter` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_advanced`
--
ALTER TABLE `sm_advanced`
ADD CONSTRAINT `fk_SM_ADVANCED_SM_ADVANCED_STATUS1` FOREIGN KEY (`ADVANCED_STATUS_ID`) REFERENCES `sm_advanced_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_ADVANCED_SM_CUSTOMER1` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `sm_customer` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_ADVANCED_SM_PRODUCT1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sm_product` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_customer`
--
ALTER TABLE `sm_customer`
ADD CONSTRAINT `FK1_SM_CUSTOMER` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK2_SM_CUSTOMER` FOREIGN KEY (`CUSTOMER_TYPE_ID`) REFERENCES `sm_customer_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK3_SM_CUSTOMER` FOREIGN KEY (`ENTITY_ID`) REFERENCES `CTA_ENTITY` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `sm_customer_type`
--
ALTER TABLE `sm_customer_type`
ADD CONSTRAINT `fk_SM_CUSTOMER_TYPE_CLT_FOLDER1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_expense`
--
ALTER TABLE `sm_expense`
ADD CONSTRAINT `fk_SM_EXPENSE_CLT_FOLDER1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_EXPENSE_SM_EXPENSE_TYPE1` FOREIGN KEY (`EXPENSE_TYPE_ID`) REFERENCES `sm_expense_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `SM_ORDER`
--
ALTER TABLE `SM_ORDER`
ADD CONSTRAINT `fk_SM_ORDER_SM_CHECK1` FOREIGN KEY (`CHECK_ID`) REFERENCES `sm_check` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_ORDER_SM_CUSTOMER1` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `sm_customer` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_ORDER_SM_ORDER_STATUS1` FOREIGN KEY (`ORDER_STATUS_ID`) REFERENCES `sm_order_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_ORDER_SM_PAYMENT_METHOD1` FOREIGN KEY (`PAYMENT_METHOD_ID`) REFERENCES `sm_payment_method` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_order_line`
--
ALTER TABLE `sm_order_line`
ADD CONSTRAINT `fk_SM_ORDER_LINE_SM_ORDER1` FOREIGN KEY (`ORDER_ID`) REFERENCES `sm_order` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_ORDER_LINE_SM_PRODUCT1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sm_product` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_order_supplier`
--
ALTER TABLE `sm_order_supplier`
ADD CONSTRAINT `fk1_sm_order_supplier` FOREIGN KEY (`ORDER_SUPPLIER_STATUS_ID`) REFERENCES `sm_order_supplier_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_order_supplier_line`
--
ALTER TABLE `sm_order_supplier_line`
ADD CONSTRAINT `fk1_sm_order_supplier_line` FOREIGN KEY (`ORDER_SUPPLIER_ID`) REFERENCES `sm_order_supplier` (`ID`) ON DELETE CASCADE ON UPDATE SET NULL,
ADD CONSTRAINT `fk2_sm_order_supplier_line` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sm_product` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_payment_method`
--
ALTER TABLE `sm_payment_method`
ADD CONSTRAINT `fk_SM_PAYMENT_METHOD_CLT_MODULE1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_product`
--
ALTER TABLE `sm_product`
ADD CONSTRAINT `fk_SM_PRODUCT_SM_PRODUCT_COLOR1` FOREIGN KEY (`PRODUCT_COLOR_ID`) REFERENCES `sm_product_color` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_PRODUCT_SM_PRODUCT_FAMILY1` FOREIGN KEY (`PRODUCT_FAMILY_ID`) REFERENCES `sm_product_family` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_sm_product_sm_product_size1` FOREIGN KEY (`PRODUCT_SIZE_ID`) REFERENCES `sm_product_size` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_PRODUCT_SM_PRODUCT_STATUS1` FOREIGN KEY (`PRODUCT_STATUS_ID`) REFERENCES `sm_product_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_product_color`
--
ALTER TABLE `sm_product_color`
ADD CONSTRAINT `fk_SM_PRODUCT_COLOR_CLT_MODULE1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_product_family`
--
ALTER TABLE `sm_product_family`
ADD CONSTRAINT `fk_SM_PRODUCT_FAMILY_CLT_MODULE1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_PRODUCT_FAMILY_SM_PRODUCT_GROUP1` FOREIGN KEY (`PRODUCT_GROUP_ID`) REFERENCES `sm_product_group` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_product_group`
--
ALTER TABLE `sm_product_group`
ADD CONSTRAINT `fk_SM_PRODUCT_GROUP_CLT_MODULE1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_promotion`
--
ALTER TABLE `sm_promotion`
ADD CONSTRAINT `fk_SM_PROMOTION_SM_PRODUCT1` FOREIGN KEY (`PRODUCT`) REFERENCES `sm_product` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_PROMOTION_SM_PROMOTION_TYPE1` FOREIGN KEY (`PROMOTION_TYPE_ID`) REFERENCES `sm_promotion_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_promotion_type`
--
ALTER TABLE `sm_promotion_type`
ADD CONSTRAINT `fk_SM_PROMOTION_TYPE_CLT_MODULE1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_reception`
--
ALTER TABLE `sm_reception`
ADD CONSTRAINT `fk1_sm_reception` FOREIGN KEY (`SUPPLIER_ID`) REFERENCES `sm_supplier` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk2_sm_reception` FOREIGN KEY (`RECEPTION_STATUS_ID`) REFERENCES `sm_reception_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk3_sm_reception` FOREIGN KEY (`ORDER_SUPPLIER_ID`) REFERENCES `sm_order_supplier` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_reception_line`
--
ALTER TABLE `sm_reception_line`
ADD CONSTRAINT `fk1_sm_reception_line` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sm_product` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk2_sm_reception_line` FOREIGN KEY (`RECEPTION_ID`) REFERENCES `sm_reception` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_reception_products`
--
ALTER TABLE `sm_reception_products`
ADD CONSTRAINT `fk1_sm_reception_id` FOREIGN KEY (`RECEPTION_ID`) REFERENCES `sm_reception` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_return_receipt`
--
ALTER TABLE `sm_return_receipt`
ADD CONSTRAINT `fk1_sm_return_receipt` FOREIGN KEY (`RETURN_RECEIPT_STATUS_ID`) REFERENCES `sm_return_receipt_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk2_sm_return_receipt` FOREIGN KEY (`ORDER_ID`) REFERENCES `sm_order` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_return_receipt_line`
--
ALTER TABLE `sm_return_receipt_line`
ADD CONSTRAINT `fk1_ sm_return_receipt_line` FOREIGN KEY (`RETURN_RECEIPT_ID`) REFERENCES `sm_return_receipt` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk2_ sm_return_receipt_line` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sm_product` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_supplier`
--
ALTER TABLE `sm_supplier`
ADD CONSTRAINT `FK1_SUPPLIER` FOREIGN KEY (`SUPPLIER_TYPE_ID`) REFERENCES `sm_supplier_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK2_SUPPLIER` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK3_SUPPLIER` FOREIGN KEY (`ENTITY_ID`) REFERENCES `CTA_ENTITY` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `sm_supplier_type`
--
ALTER TABLE `sm_supplier_type`
ADD CONSTRAINT `fk_SM_SUPPLIER_TYPE_CLT_FOLDER1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `tmp_pm_attribute_exclud`
--
ALTER TABLE `tmp_pm_attribute_exclud`
ADD CONSTRAINT `fk1_pm_attribute_exclud` FOREIGN KEY (`MODEL_ID`) REFERENCES `pm_model` (`ID`),
ADD CONSTRAINT `fk_pm_attribute_exclud` FOREIGN KEY (`PAGE_ID`) REFERENCES `pm_page` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `tmp_pm_model_attribute`
--
ALTER TABLE `tmp_pm_model_attribute`
ADD CONSTRAINT `fk1_pm_model_attribute` FOREIGN KEY (`MODEL_ID`) REFERENCES `pm_model` (`ID`),
ADD CONSTRAINT `fk_DATA_TYPE2` FOREIGN KEY (`DATA_TYPE_ID`) REFERENCES `pm_data_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_PM_PAGE_ATTRIBUTE1` FOREIGN KEY (`PM_PAGE_ATTRIBUTE_ID`) REFERENCES `pm_page_attribute` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FORMAT_TYPE_ID2` FOREIGN KEY (`FORMAT_TYPE_ID`) REFERENCES `pm_format_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `tmp_pm_model_pdf_attribute`
--
ALTER TABLE `tmp_pm_model_pdf_attribute`
ADD CONSTRAINT `fk1_pm_model_pdf_attribute` FOREIGN KEY (`MODEL_ID`) REFERENCES `pm_model` (`ID`);

--
-- Contraintes pour la table `tmp_pm_pdf_attribute_exclud`
--
ALTER TABLE `tmp_pm_pdf_attribute_exclud`
ADD CONSTRAINT `fk1_pm_pdf_attribute_exclud` FOREIGN KEY (`MODEL_ID`) REFERENCES `pm_model` (`ID`);

--
-- Contraintes pour la table `tmp_pm_pdf_parameter_module`
--
ALTER TABLE `tmp_pm_pdf_parameter_module`
ADD CONSTRAINT `fk1_pm_pdf_parameter_model` FOREIGN KEY (`MODEL_ID`) REFERENCES `pm_model` (`ID`);
