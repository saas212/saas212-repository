
-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Client :  localhost:3306
-- Généré le :  Ven 24 Avril 2015 à 22:53
-- Version du serveur :  5.5.38
-- Version de PHP :  5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `mystock`
--
CREATE DATABASE IF NOT EXISTS `mystock` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `mystock`;

-- --------------------------------------------------------

--
-- Structure de la table `clt_client`
--

CREATE TABLE `clt_client` (
`ID` bigint(20) NOT NULL,
  `FIRST_NAME` tinytext,
  `LAST_NAME` tinytext,
  `COMPANY_NAME` tinytext CHARACTER SET latin1,
  `ADRESS` tinytext CHARACTER SET latin1,
  `EMAIL` tinytext CHARACTER SET latin1,
  `CELL_PHONE` tinytext CHARACTER SET latin1,
  `FIXED_PHONE` tinytext CHARACTER SET latin1,
  `CURRENCY` tinytext CHARACTER SET latin1,
  `INF_COUNTRY_ID` bigint(20) DEFAULT NULL,
  `INF_CITY_ID` bigint(20) DEFAULT NULL,
  `CLIENT_STATUS_ID` bigint(20) DEFAULT NULL,
  `DEFAULT_LANGUAGE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_client`
--

INSERT INTO `clt_client` (`ID`, `FIRST_NAME`, `LAST_NAME`, `COMPANY_NAME`, `ADRESS`, `EMAIL`, `CELL_PHONE`, `FIXED_PHONE`, `CURRENCY`, `INF_COUNTRY_ID`, `INF_CITY_ID`, `CLIENT_STATUS_ID`, `DEFAULT_LANGUAGE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Jalal', 'TELEMSANI', 'SAJ ELEGENT', '33 passage sumica casablanca 20080 Centre ville', 'sajelegent@gmail.com', NULL, '+212 5 22 20 66 49', 'DH', 1, 2, NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

--
-- Déclencheurs `clt_client`
--
DELIMITER //
CREATE TRIGGER `TRIG_BI_CLIENT` BEFORE INSERT ON `clt_client`
 FOR EACH ROW BEGIN

    -- VERFIED ACTIVE
    
    IF NEW.ACTIVE IS NULL THEN
    
      SET NEW.ACTIVE = 'Y';
    
    END IF;
    
    IF NEW.ACTIVE IS NOT NULL THEN
      
      IF upper(NEW.ACTIVE) <> 'Y' and upper(NEW.ACTIVE) <> 'N' THEN
      
          SET  NEW.ACTIVE = 'Y';
      
      end if ;
      
    END IF;
    -- DATE CREATION 
    
    SET  NEW.DATE_CREATION = now();

END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `clt_client_language`
--

CREATE TABLE `clt_client_language` (
`ID` bigint(20) NOT NULL,
  `CLIENT_ID` bigint(20) DEFAULT NULL,
  `LANGUAGE_ID` bigint(20) DEFAULT NULL,
  `INF_PREFIX_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_client_language`
--

INSERT INTO `clt_client_language` (`ID`, `CLIENT_ID`, `LANGUAGE_ID`, `INF_PREFIX_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_client_status`
--

CREATE TABLE `clt_client_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_client_status`
--

INSERT INTO `clt_client_status` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Active', NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_module`
--

CREATE TABLE `clt_module` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `CLIENT_ID` bigint(20) DEFAULT NULL,
  `MODULE_STATUS_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_module`
--

INSERT INTO `clt_module` (`ID`, `NAME`, `DESCRIPTION`, `CLIENT_ID`, `MODULE_STATUS_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Depôt', '', 1, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Electro', '', 1, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(3, 'Medina 1', '', 1, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(4, 'Medina 2', '', 1, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(5, 'Sans titre', '', 1, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(6, 'En cours', NULL, 1, 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_module_parameter`
--

CREATE TABLE `clt_module_parameter` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext CHARACTER SET latin1,
  `DESCRIPTION` text CHARACTER SET latin1,
  `DEFAULT_VALUE` text,
  `PAGE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `clt_module_parameter_client`
--

CREATE TABLE `clt_module_parameter_client` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `clt_module_parameter_type`
--

CREATE TABLE `clt_module_parameter_type` (
  `ID` bigint(20) NOT NULL,
  `PAGE_PARAMETER_ID` bigint(20) DEFAULT NULL,
  `VALUE` longtext,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `clt_module_status`
--

CREATE TABLE `clt_module_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_module_status`
--

INSERT INTO `clt_module_status` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'ACTIF', NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_parameter`
--

CREATE TABLE `clt_parameter` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext CHARACTER SET latin1,
  `DEFAULT_VALUE` text,
  `PARAMETER_TYPE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `DESCRIPTION` tinytext CHARACTER SET latin1
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_parameter`
--

INSERT INTO `clt_parameter` (`ID`, `NAME`, `DEFAULT_VALUE`, `PARAMETER_TYPE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `DESCRIPTION`) VALUES
(1, 'Titre de l''application ', 'defaitl val 1', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(2, 'Package et jsp de page en cours', 'defaitl val 2', 1, 'Y', NULL, NULL, NULL, NULL, NULL),
(3, 'param 3', 'default va 3', 1, 'Y', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_parameter_client`
--

CREATE TABLE `clt_parameter_client` (
`ID` bigint(20) NOT NULL,
  `VALUE` longtext,
  `CLINET_ID` bigint(20) DEFAULT NULL,
  `PARAMETER_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_parameter_client`
--

INSERT INTO `clt_parameter_client` (`ID`, `VALUE`, `CLINET_ID`, `PARAMETER_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'My Stock Management', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'ma.mystock.web.beans.stock.encours,stock/encours', 1, 2, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_parameter_type`
--

CREATE TABLE `clt_parameter_type` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_parameter_type`
--

INSERT INTO `clt_parameter_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'type1', 'desc type 11', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_user`
--

CREATE TABLE `clt_user` (
`ID` bigint(20) NOT NULL,
  `FIRST_NAME` tinytext CHARACTER SET latin1,
  `LAST_NAME` tinytext CHARACTER SET latin1,
  `USERNAME` tinytext CHARACTER SET latin1,
  `PASSWORD` tinytext CHARACTER SET latin1,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `CLIENT_ID` bigint(20) DEFAULT NULL,
  `INF_COUNTRY_ID` bigint(20) DEFAULT NULL,
  `INF_CITY_ID` bigint(20) DEFAULT NULL,
  `USER_STATUS_ID` bigint(20) DEFAULT NULL,
  `DEFAULT_LANGUAGE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `SUPER_ADMIN` char(1) CHARACTER SET latin1 DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `CELL_PHONE` varchar(45) DEFAULT NULL,
  `FIXED_PHONE` varchar(45) DEFAULT NULL,
  `ADRESS` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_user`
--

INSERT INTO `clt_user` (`ID`, `FIRST_NAME`, `LAST_NAME`, `USERNAME`, `PASSWORD`, `CATEGORY_ID`, `CLIENT_ID`, `INF_COUNTRY_ID`, `INF_CITY_ID`, `USER_STATUS_ID`, `DEFAULT_LANGUAGE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `SUPER_ADMIN`, `CLT_MODULE_ID`, `CELL_PHONE`, `FIXED_PHONE`, `ADRESS`, `SORT_KEY`) VALUES
(1, 'Abdessamad HALLAL', 'HALLAL', 'admin', '8308651804facb7b9af8ffc53a33a22d6a1c8ac2', 1, 1, 1, 2, 1, 0, 'Y', NULL, NULL, NULL, NULL, NULL, 1, 'aaa', 'fff', 'rrrrG', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_user_category`
--

CREATE TABLE `clt_user_category` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_user_category`
--

INSERT INTO `clt_user_category` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Category 1', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_user_group`
--

CREATE TABLE `clt_user_group` (
`ID` bigint(20) NOT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  `INF_GROUP_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_user_group`
--

INSERT INTO `clt_user_group` (`ID`, `USER_ID`, `INF_GROUP_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(2, 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_user_module`
--

CREATE TABLE `clt_user_module` (
`ID` bigint(20) NOT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  `MODULE_ID` bigint(20) DEFAULT NULL,
  `SUPER_ADMIN` char(1) CHARACTER SET latin1 DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_user_module`
--

INSERT INTO `clt_user_module` (`ID`, `USER_ID`, `MODULE_ID`, `SUPER_ADMIN`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, 1, 'O', 'Y', NULL, NULL, NULL, NULL),
(2, 1, 2, 'O', 'Y', NULL, NULL, NULL, NULL),
(3, 1, 3, 'O', 'Y', NULL, NULL, NULL, NULL),
(4, 1, 4, 'O', 'Y', NULL, NULL, NULL, NULL),
(5, 1, 5, 'O', 'Y', NULL, NULL, NULL, NULL),
(6, 1, 6, 'O', 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `clt_user_status`
--

CREATE TABLE `clt_user_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `clt_user_status`
--

INSERT INTO `clt_user_status` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Actif 1', NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_basic_parameter`
--

CREATE TABLE `inf_basic_parameter` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext CHARACTER SET latin1,
  `DESCRIPTION` text CHARACTER SET latin1,
  `VALUE` text,
  `BASIC_PARAMETER_TYPE_ID` bigint(20) NOT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_basic_parameter`
--

INSERT INTO `inf_basic_parameter` (`ID`, `NAME`, `DESCRIPTION`, `VALUE`, `BASIC_PARAMETER_TYPE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'URL de l''application', 'url de l''application utilisé pour récupérer les images et styles et les fichiers javascript', 'http://localhost:8083/MyStock', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Variable de css, js et img', 'ce variable pour reladoer les fichiers pour chaque requûets HTTP', 'Y', 1, 'Y', NULL, NULL, NULL, NULL),
(3, 'default prefixe', 'le préfixe qui sera pardéfaut', 'default', 1, 'Y', NULL, NULL, NULL, NULL),
(4, 'langue par défaut', 'le code du langue', 'fr', 1, 'Y', NULL, NULL, NULL, NULL),
(5, 'copyright', 'le nom de la société', 'My Systems', 1, 'Y', NULL, NULL, NULL, NULL),
(6, 'Authentification', 'le titre de page d''authentification', 'Authentification', 1, 'Y', NULL, NULL, NULL, NULL),
(7, 'Le nom de l''application', NULL, 'My Stock Management', 1, 'Y', NULL, NULL, NULL, NULL),
(8, 'Les droits', 'les droits qui s''affichent dans le footer ', 'Copyright © 2015 ', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_basic_parameter_type`
--

CREATE TABLE `inf_basic_parameter_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_basic_parameter_type`
--

INSERT INTO `inf_basic_parameter_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'basic config', 'basic config', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'type 1', 'esc type 1', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_city`
--

CREATE TABLE `inf_city` (
`ID` bigint(20) NOT NULL,
  `CODE` tinytext CHARACTER SET latin1,
  `NAME` tinytext,
  `DESCRIPTION` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `COUNTRY_ID` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_city`
--

INSERT INTO `inf_city` (`ID`, `CODE`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `COUNTRY_ID`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, NULL, 'Rabat', NULL, NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, NULL, 'Casablanca', NULL, NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(3, NULL, 'Rabat2', NULL, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(4, NULL, '111city1', NULL, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_country`
--

CREATE TABLE `inf_country` (
`ID` bigint(20) NOT NULL,
  `CODE` tinytext CHARACTER SET latin1,
  `NAME` tinytext,
  `DESCRIPTION` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_country`
--

INSERT INTO `inf_country` (`ID`, `CODE`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `CLT_MODULE_ID`) VALUES
(1, NULL, 'Maroc', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(2, NULL, 'Maroc2', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1),
(3, NULL, 'pay12', NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `inf_group`
--

CREATE TABLE `inf_group` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_group`
--

INSERT INTO `inf_group` (`ID`, `NAME`, `DESCRIPTION`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'group 1', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_item`
--

CREATE TABLE `inf_item` (
  `CODE` varchar(255) NOT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` varchar(45) DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_item`
--

INSERT INTO `inf_item` (`CODE`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
('blValidation', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.customerName', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.description', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.id', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.sumQuantity', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s1.sumTotalPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s2', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s2.customer', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s2.description', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.color', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.size', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.color', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.price', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.quantity', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.reference', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.size', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s4.totalPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s5', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s5.sumQuantity', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s5.sumTotal', 'Y', NULL, NULL, NULL, NULL),
('blValidation.s6', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s1', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s1.customer', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s1.description', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.color', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.quantity', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.size', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s2.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.color', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.price', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.reference', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.size', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s3.totalPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s4', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s4.sumQuantity', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s4.sumTotal', 'Y', NULL, NULL, NULL, NULL),
('bonLivraison.s5', 'Y', NULL, NULL, NULL, NULL),
('customer', 'Y', NULL, NULL, NULL, NULL),
('customer.s1', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.active', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.adress', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.city', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.country', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.email', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.firstName', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.fixedPhone', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.lastName', 'Y', NULL, NULL, NULL, NULL),
('customer.s1.type', 'Y', NULL, NULL, NULL, NULL),
('customer.s2', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.adress', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.city', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.country', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.email', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.firstName', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.fixedPhone', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.lastName', 'Y', NULL, NULL, NULL, NULL),
('customer.s2.type', 'Y', NULL, NULL, NULL, NULL),
('expense', 'Y', NULL, NULL, NULL, NULL),
('expense.s1', 'Y', NULL, NULL, NULL, NULL),
('expense.s1.amount', 'Y', NULL, NULL, NULL, NULL),
('expense.s1.description', 'Y', NULL, NULL, NULL, NULL),
('expense.s1.expenseType', 'Y', NULL, NULL, NULL, NULL),
('expense.s1.expenseTypeName', 'Y', NULL, NULL, NULL, NULL),
('expense.s1.name', 'Y', NULL, NULL, NULL, NULL),
('expense.s2', 'Y', NULL, NULL, NULL, NULL),
('expense.s2.amount', 'Y', NULL, NULL, NULL, NULL),
('expense.s2.description', 'Y', NULL, NULL, NULL, NULL),
('expense.s2.name', 'Y', NULL, NULL, NULL, NULL),
('expense.s2.type', 'Y', NULL, NULL, NULL, NULL),
('globals', 'Y', NULL, NULL, NULL, NULL),
('globals.forms', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.add', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.cancel', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.clean', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.confirmClean', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.confirmDelete', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.delete', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.edit', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.no', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.save', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.validate', 'Y', NULL, NULL, NULL, NULL),
('globals.forms.yes', 'Y', NULL, NULL, NULL, NULL),
('globals.list', 'Y', NULL, NULL, NULL, NULL),
('globals.list.activate', 'Y', NULL, NULL, NULL, NULL),
('globals.list.confirmDelete', 'Y', NULL, NULL, NULL, NULL),
('globals.list.delete', 'Y', NULL, NULL, NULL, NULL),
('globals.list.edit', 'Y', NULL, NULL, NULL, NULL),
('globals.list.option', 'Y', NULL, NULL, NULL, NULL),
('globals.list.validate', 'Y', NULL, NULL, NULL, NULL),
('globals.list.vide', 'Y', NULL, NULL, NULL, NULL),
('inventaire', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.active', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.designation', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.priceBuy', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.priceSale', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.productColorName', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.productSizeName', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.quantity', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.quantityCount', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.receptionValidCount', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.unitPriceBuyMax', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.unitPriceBuyMin', 'Y', NULL, NULL, NULL, NULL),
('inventaire.s1.unitPriceBuyMoyenne', 'Y', NULL, NULL, NULL, NULL),
('login', 'Y', NULL, NULL, NULL, NULL),
('login.s1', 'Y', NULL, NULL, NULL, NULL),
('login.s1.forgetPassword', 'Y', NULL, NULL, NULL, NULL),
('login.s1.getAccount', 'Y', NULL, NULL, NULL, NULL),
('login.s1.login', 'Y', NULL, NULL, NULL, NULL),
('login.s1.password', 'Y', NULL, NULL, NULL, NULL),
('login.s1.sessionActive', 'Y', NULL, NULL, NULL, NULL),
('login.s1.username', 'Y', NULL, NULL, NULL, NULL),
('module', 'Y', NULL, NULL, NULL, NULL),
('module.s1', 'Y', NULL, NULL, NULL, NULL),
('module.s1.access', 'Y', NULL, NULL, NULL, NULL),
('module.s1.logout', 'Y', NULL, NULL, NULL, NULL),
('page1.s1.name', 'Y', NULL, NULL, NULL, NULL),
('product', 'Y', NULL, NULL, NULL, NULL),
('product.s1', 'Y', NULL, NULL, NULL, NULL),
('product.s1.color', 'Y', NULL, NULL, NULL, NULL),
('product.s1.designation', 'Y', NULL, NULL, NULL, NULL),
('product.s1.family', 'Y', NULL, NULL, NULL, NULL),
('product.s1.priceBuy', 'Y', NULL, NULL, NULL, NULL),
('product.s1.pricesale', 'Y', NULL, NULL, NULL, NULL),
('product.s1.quantity', 'Y', NULL, NULL, NULL, NULL),
('product.s1.reference', 'Y', NULL, NULL, NULL, NULL),
('product.s1.size', 'Y', NULL, NULL, NULL, NULL),
('product.s1.status', 'Y', NULL, NULL, NULL, NULL),
('product.s1.title', 'Y', NULL, NULL, NULL, NULL),
('product.s2', 'Y', NULL, NULL, NULL, NULL),
('product.s2.active', 'Y', NULL, NULL, NULL, NULL),
('product.s2.color', 'Y', NULL, NULL, NULL, NULL),
('product.s2.designation', 'Y', NULL, NULL, NULL, NULL),
('product.s2.family', 'Y', NULL, NULL, NULL, NULL),
('product.s2.priceBuy', 'Y', NULL, NULL, NULL, NULL),
('product.s2.pricesale', 'Y', NULL, NULL, NULL, NULL),
('product.s2.quantity', 'Y', NULL, NULL, NULL, NULL),
('product.s2.reference', 'Y', NULL, NULL, NULL, NULL),
('product.s2.size', 'Y', NULL, NULL, NULL, NULL),
('product.s2.status', 'Y', NULL, NULL, NULL, NULL),
('profile.s1', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.adress', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.email', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.firstName', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.fixedPhone', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.lastName', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.password', 'Y', NULL, NULL, NULL, NULL),
('profile.s1.replayRassword', 'Y', NULL, NULL, NULL, NULL),
('reception', 'Y', NULL, NULL, NULL, NULL),
('reception.s1', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.active', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.amount', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.deadline', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.id', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.name', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.souche', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.supplierId', 'Y', NULL, NULL, NULL, NULL),
('reception.s1.valid', 'Y', NULL, NULL, NULL, NULL),
('reception.s2', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.amount', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.deadline', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.deposit', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.description', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.id', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.name', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.sizes', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.souche', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('reception.s2.valid', 'Y', NULL, NULL, NULL, NULL),
('reception.s3', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.id', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.remise', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.tva', 'Y', NULL, NULL, NULL, NULL),
('reception.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('reception.s4', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.designation', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.id', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.quantity', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.remise', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.totalHt', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.tva', 'Y', NULL, NULL, NULL, NULL),
('reception.s4.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('reception.s5', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.quantityTotal', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.remiseAvg', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.tvaAvg', 'Y', NULL, NULL, NULL, NULL),
('reception.s5.unitPriceBuyTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.active', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.amount', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.deadline', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.id', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.name', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.souche', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s1.supplierId', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.amount', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.deadline', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.deposit', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.description', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.id', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.name', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.souche', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.id', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.remise', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.tva', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.quantityTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.remiseAvg', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.tvaAvg', 'Y', NULL, NULL, NULL, NULL),
('receptionHistory.s4.unitPriceBuyTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.active', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.amount', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.deadline', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.id', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.name', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.souche', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s1.supplierId', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.amount', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.deadline', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.deposit', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.description', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.id', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.name', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.souche', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s2.supplier', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.designation', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.id', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.quantity', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.remise', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.totalHt', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.totalTtc', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.tva', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s3.unitPriceBuy', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.quantityTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.remiseAvg', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.totalHtTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.totalTtcTotal', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.tvaAvg', 'Y', NULL, NULL, NULL, NULL),
('receptionValidation.s4.unitPriceBuyTotal', 'Y', NULL, NULL, NULL, NULL),
('rofile.s1.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('stockState', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.productNotValid', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.productValid', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.quantityCount', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.receptionInprogressCount', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.receptionValidCount', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.unitPriceBuyMax', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.unitPriceBuyMin', 'Y', NULL, NULL, NULL, NULL),
('stockState.s1.unitPriceBuyMoyenne', 'Y', NULL, NULL, NULL, NULL),
('supplier', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.active', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.adress', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.city', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.companyName', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.country', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.email', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.firstName', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.fixedPhone', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.lastName', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.postCode', 'Y', NULL, NULL, NULL, NULL),
('supplier.s1.type', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.adress', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.cellPhone', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.city', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.companyName', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.country', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.email', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.firstName', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.fixedPhone', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.lastName', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.postCode', 'Y', NULL, NULL, NULL, NULL),
('supplier.s2.type', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.char', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.date', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.datetime', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.double', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.email', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.integer', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.maxlenght', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.phone', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.required', 'Y', NULL, NULL, NULL, NULL),
('validation.v1.time', 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_language`
--

CREATE TABLE `inf_language` (
`ID` bigint(20) NOT NULL,
  `CODE` tinytext,
  `NAME` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_language`
--

INSERT INTO `inf_language` (`ID`, `CODE`, `NAME`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'fr', 'Français de France', 1, 'Y', '2014-09-30 00:00:00', 1, '2014-09-16 00:00:00', NULL),
(2, 'en', 'Anglais', NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_lovs`
--

CREATE TABLE `inf_lovs` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `TABLE_PREFIX` tinytext,
  `TABLE` tinytext,
  `VIEW` tinytext,
  `ITEM_CODE` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ENTITY` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_lovs`
--

INSERT INTO `inf_lovs` (`ID`, `NAME`, `DESCRIPTION`, `TABLE_PREFIX`, `TABLE`, `VIEW`, `ITEM_CODE`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `CLT_MODULE_ID`, `ENTITY`) VALUES
(1, 'Les types de fournisseurs', NULL, 'sm', 'sm_supplier_type', 'v_sm_supplier_type', 'itemCode', 1, 'Y', NULL, NULL, NULL, NULL, 1, 'SmSupplierType'),
(2, 'La couleurs', NULL, 'sm', 'sm_expense_type', 'v_sm_expense_type', 'itemCode', 2, 'Y', NULL, NULL, NULL, NULL, 1, 'SmProductColor'),
(3, 'Les Pays', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'InfCountry'),
(4, 'Les villes', NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL, 1, 'InfCity');

-- --------------------------------------------------------

--
-- Structure de la table `inf_prefix`
--

CREATE TABLE `inf_prefix` (
`ID` bigint(20) NOT NULL,
  `CODE` tinytext,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_prefix`
--

INSERT INTO `inf_prefix` (`ID`, `CODE`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'default', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_privilege`
--

CREATE TABLE `inf_privilege` (
`ID` bigint(20) NOT NULL,
  `ITEM_CODE` varchar(255) DEFAULT NULL,
  `ROLE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_privilege`
--

INSERT INTO `inf_privilege` (`ID`, `ITEM_CODE`, `ROLE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'login', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'login.s1', 1, 'Y', NULL, NULL, NULL, NULL),
(3, 'login.s1.username', 1, 'Y', NULL, NULL, NULL, NULL),
(4, 'login.s1.password', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_role`
--

CREATE TABLE `inf_role` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_role`
--

INSERT INTO `inf_role` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Role 1', 'Role 1', 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_role_group`
--

CREATE TABLE `inf_role_group` (
`ID` bigint(20) NOT NULL,
  `ROLE_ID` bigint(20) DEFAULT NULL,
  `GROUP_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_role_group`
--

INSERT INTO `inf_role_group` (`ID`, `ROLE_ID`, `GROUP_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_text`
--

CREATE TABLE `inf_text` (
`ID` bigint(20) NOT NULL,
  `PREFIX` bigint(20) DEFAULT NULL,
  `ITEM_CODE` varchar(255) DEFAULT NULL,
  `VALUE` longtext,
  `TEXT_TYPE_ID` bigint(20) DEFAULT NULL,
  `LANGUAGE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=402 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_text`
--

INSERT INTO `inf_text` (`ID`, `PREFIX`, `ITEM_CODE`, `VALUE`, `TEXT_TYPE_ID`, `LANGUAGE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(2, 1, 'login.s1.password', 'Mot de passe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(3, 1, 'login.s1.username', 'E-mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(4, 1, 'login.s1.username', 'username', 6, 1, 'Y', NULL, NULL, NULL, NULL),
(5, 1, 'login.s1.login', 'Se connecter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(10, 1, 'page1.s1.name', 'Résumé Général', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(11, 1, 'validation.v1.integer', '{0} doit être de type integer', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(12, 1, 'validation.v1.double', '{0} doit être de type double', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(13, 1, 'validation.v1.char', '{0} doit être de type char', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(14, 1, 'validation.v1.date', '{0} doit être de type date', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(15, 1, 'validation.v1.time', '{0} doit être de type time', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(16, 1, 'validation.v1.datetime', '{0} doit être de type datetime', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(17, 1, 'validation.v1.email', '{0} doit être de type email', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(18, 1, 'validation.v1.phone', '{0} doit être de type phone', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(19, 1, 'validation.v1.maxlenght', '{0} ne doit pas dépasser {1} char', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(20, 1, 'validation.v1.required', '{0} est obligatoire', 7, 1, 'Y', NULL, NULL, NULL, NULL),
(26, 1, 'expense.s1', 'La liste des dépenses', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(27, 1, 'expense.s1.name', 'Libelle', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(28, 1, 'expense.s1.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(29, 1, 'expense.s1.amount', 'Montant', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(30, 1, 'expense.s1.expenseTypeName', 'Type de charge', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(34, 1, 'expense.s2', 'Ajouter / Modifier un dépense', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(36, 1, 'expense.s2.type', 'Type de charge', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(37, 1, 'expense.s2.name', 'Libelle', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(38, 1, 'expense.s2.description', 'Description', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(39, 1, 'expense.s2.amount', 'Montant', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(41, 1, 'supplier.s1', 'Ajouter / Modifier un fournisseur', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(43, 1, 'supplier.s2', 'Liste des fournisseurs', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(45, 1, 'supplier.s2.country', 'Pays', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(46, 1, 'supplier.s2.city', 'Ville', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(47, 1, 'supplier.s2.email', 'E-Mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(49, 1, 'supplier.s2.fixedPhone', 'Télé Fixe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(50, 1, 'supplier.s2.cellPhone', 'Télé Portable', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(51, 1, 'supplier.s2.companyName', 'Nom de Société ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(52, 1, 'product.s1', 'List Produits :', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(53, 1, 'product.s1.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(54, 1, 'product.s1.quantity', 'quantity', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(55, 1, 'product.s1.reference', 'reference', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(56, 1, 'product.s1.pricesale', 'pricesale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(57, 1, 'product.s1.status', 'status', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(58, 1, 'product.s1.size', 'size', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(59, 1, 'product.s1.family', 'family', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(60, 1, 'product.s1.color', 'color', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(61, 1, 'product.s1.edit', 'edit', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(62, 1, 'product.s1.delete', 'delete', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(63, 1, 'product.s1', 'List Produit : ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(64, 1, 'product.s2.cancel', 'cancel', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(65, 1, 'product.s2.save', 'save', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(66, 1, 'product.s2.color', 'product Color', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(67, 1, 'product.s2.family', 'product Family', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(68, 1, 'product.s2.size', 'product Size', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(69, 1, 'product.s2.status', 'product Status', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(70, 1, 'product.s2.pricesale', 'pricesale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(71, 1, 'product.s2.reference', 'reference', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(72, 1, 'product.s2.quantity', 'quantity', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(73, 1, 'product.s2.designation', 'designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(74, 1, 'product.s2', 'Ajouter un nouveau produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(75, 1, 'supplier.s1.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(76, 1, 'supplier.s1.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(77, 1, 'supplier.s1.country', 'Pays', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(78, 1, 'supplier.s1.city', 'Ville', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(79, 1, 'supplier.s1.companyName', 'Nom de Société ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(80, 1, 'supplier.s1.cellPhone', 'Télé Portable', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(81, 1, 'supplier.s1.fixedPhone', 'Télé Fixe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(82, 1, 'supplier.s1.adress', 'Adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(83, 1, 'supplier.s1.email', 'E-Mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(84, 1, 'supplier.s1.type', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(89, 1, 'supplier.s2.type', 'Type ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(90, 1, 'globals.list.delete', 'Supprimer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(91, 1, 'globals.list.edit', 'Modifier', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(92, 1, 'globals.list.option', 'Option', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(93, 1, 'globals.list.activate', 'Activer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(94, 1, 'globals.list.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(95, 1, 'globals.list.vide', 'la liste est vide.', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(96, 1, 'globals.list.confirmDelete', 'Êtes-vous sûr de vouloir supprimer ?', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(97, 1, 'globals.forms.add', 'Ajouter', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(98, 1, 'globals.forms.edit', 'Modifier', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(99, 1, 'globals.forms.cancel', 'Annuler', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(100, 1, 'globals.forms.save', 'Sauvegarder', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(101, 1, 'globals.forms.delete', 'Supprimer', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(102, 1, 'globals.forms.confirmDelete', 'Êtes-vous sûr de vouloir supprimer ?', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(103, 1, 'reception.s1', 'La liste des réceptions en cours', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(104, 1, 'reception.s1.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(105, 1, 'reception.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(106, 1, 'reception.s1.supplierId', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(107, 1, 'reception.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(108, 1, 'reception.s1.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(109, 1, 'reception.s1.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(110, 1, 'reception.s1.amount', 'Le montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(111, 1, 'reception.s1.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(112, 1, 'globals.forms.yes', 'Oui', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(113, 1, 'globals.forms.no', 'Non', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(146, 1, 'reception.s2', 'Ajouter une réception ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(147, 1, 'reception.s2.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(148, 1, 'reception.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(149, 1, 'reception.s2.supplier', 'Fournisseur ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(150, 1, 'reception.s2.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(151, 1, 'reception.s2.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(152, 1, 'reception.s2.amount', 'Montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(153, 1, 'reception.s2.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(155, 1, 'reception.s2.deposit', 'Depôt', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(156, 1, 'reception.s2.description', 'Description ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(158, 1, 'supplier.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(159, 1, 'product', 'La gestion des produits ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(160, 1, 'product.s2.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(161, 1, 'expense.s1.expenseTypeName', 'Type de charge', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(162, 1, 'customer.s2', 'La liste des clients', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(163, 1, 'customer.s2.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(164, 1, 'customer.s1', 'Ajouter / Modifier un client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(165, 1, 'customer.s2.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(166, 1, 'customer.s2.country', 'Pays', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(167, 1, 'customer.s2.city', 'Ville', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(168, 1, 'customer.s2.email', 'E-Mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(169, 1, 'customer.s2.adress', 'Adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(170, 1, 'customer.s2.fixedPhone', 'Télé Fixe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(171, 1, 'customer.s2.cellPhone', 'Télé Portable', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(172, 1, 'customer.s1.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(173, 1, 'customer.s1.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(174, 1, 'customer.s1.country', 'Pays', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(175, 1, 'customer.s1.city', 'Ville', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(176, 1, 'customer.s1.cellPhone', 'Télé Portable', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(177, 1, 'customer.s1.fixedPhone', 'Télé Fixe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(178, 1, 'customer.s1.adress', 'Adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(179, 1, 'customer.s1.email', 'E-Mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(180, 1, 'customer.s1.type', 'Type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(186, 1, 'customer.s1.active', 'Active ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(187, 1, 'product.s1.priceBuy', 'price by', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(188, 1, 'product.s2.priceBuy', 'price by', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(189, 1, 'supplier.s1.postCode', 'Code postale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(190, 1, 'supplier.s2.postCode', 'Code postale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(202, 1, 'reception.s3', 'Ajouter des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(203, 1, 'reception.s3.id', 'Code d''article ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(204, 1, 'reception.s3.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(205, 1, 'reception.s3.quantity', 'Qté ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(206, 1, 'reception.s3.unitPriceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(207, 1, 'reception.s3.remise', 'Remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(208, 1, 'reception.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(209, 1, 'reception.s3.tva', 'Tva', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(210, 1, 'reception.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(211, 1, 'globals.forms.confirmClean', 'Etes-vous sur de vouloir vider le formulaire ?', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(212, 1, 'globals.forms.clean', 'vider le fourmulaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(213, 1, 'reception.s4', 'La liste des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(214, 1, 'reception.s4.id', 'Code d''article ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(215, 1, 'reception.s4.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(216, 1, 'reception.s4.quantity', 'Qté ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(217, 1, 'reception.s4.unitPriceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(218, 1, 'reception.s4.remise', 'Remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(219, 1, 'reception.s4.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(220, 1, 'reception.s4.tva', 'Tva', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(221, 1, 'reception.s4.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(222, 1, 'page1.s1.name', 'name msg', 2, 1, 'Y', NULL, NULL, NULL, NULL),
(223, 1, 'page1.s1.name', 'name help', 4, 1, 'Y', NULL, NULL, NULL, NULL),
(224, 1, 'globals.forms.validate', 'Valider', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(225, 1, 'globals.forms.validate', 'Validate', 3, 2, 'Y', NULL, NULL, NULL, NULL),
(226, 1, 'receptionValidation.s1', 'La liste des réceptions qui sont prêt pour la validation', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(227, 1, 'receptionValidation.s1.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(228, 1, 'receptionValidation.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(229, 1, 'receptionValidation.s1.supplierId', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(230, 1, 'receptionValidation.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(231, 1, 'receptionValidation.s1.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(232, 1, 'receptionValidation.s1.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(233, 1, 'receptionValidation.s1.amount', 'Le montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(234, 1, 'receptionValidation.s1.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(235, 1, 'receptionValidation.s2', 'Détails de réception ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(236, 1, 'receptionValidation.s2.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(237, 1, 'receptionValidation.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(238, 1, 'receptionValidation.s2.supplier', 'Fournisseur ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(239, 1, 'receptionValidation.s2.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(240, 1, 'receptionValidation.s2.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(241, 1, 'receptionValidation.s2.amount', 'Montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(242, 1, 'receptionValidation.s2.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(243, 1, 'receptionValidation.s2.deposit', 'Depôt', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(244, 1, 'receptionValidation.s2.description', 'Description ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(245, 1, 'receptionValidation.s3', 'Ajouter des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(246, 1, 'receptionValidation.s3.id', 'Code d''article ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(247, 1, 'receptionValidation.s3.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(248, 1, 'receptionValidation.s3.quantity', 'Qté ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(249, 1, 'receptionValidation.s3.unitPriceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(250, 1, 'receptionValidation.s3.remise', 'Remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(251, 1, 'receptionValidation.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(252, 1, 'receptionValidation.s3.tva', 'Tva', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(253, 1, 'receptionValidation.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(254, 1, 'receptionHistory.s1', 'Hisortique des réceptions', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(255, 1, 'receptionHistory.s1.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(256, 1, 'receptionHistory.s1.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(257, 1, 'receptionHistory.s1.supplierId', 'Fournisseur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(258, 1, 'receptionHistory.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(259, 1, 'receptionHistory.s1.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(260, 1, 'receptionHistory.s1.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(261, 1, 'receptionHistory.s1.amount', 'Le montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(262, 1, 'receptionHistory.s1.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(263, 1, 'receptionHistory.s2', 'Détails de réception ', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(264, 1, 'receptionHistory.s2.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(265, 1, 'receptionHistory.s2.name', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(266, 1, 'receptionHistory.s2.supplier', 'Fournisseur ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(267, 1, 'receptionHistory.s2.deadline', 'Échéance', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(268, 1, 'receptionHistory.s2.souche', 'La souche', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(269, 1, 'receptionHistory.s2.amount', 'Montant totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(270, 1, 'receptionHistory.s2.valid', 'Valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(271, 1, 'receptionHistory.s2.deposit', 'Depôt', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(272, 1, 'receptionHistory.s2.description', 'Description ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(273, 1, 'receptionHistory.s3', 'Ajouter des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(274, 1, 'receptionHistory.s3.id', 'Code d''article ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(275, 1, 'receptionHistory.s3.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(276, 1, 'receptionHistory.s3.quantity', 'Qté ', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(277, 1, 'receptionHistory.s3.unitPriceBuy', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(278, 1, 'receptionHistory.s3.remise', 'Remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(279, 1, 'receptionHistory.s3.totalHt', 'Total HT', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(280, 1, 'receptionHistory.s3.tva', 'Tva', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(281, 1, 'receptionHistory.s3.totalTtc', 'Total TTC', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(282, 1, 'receptionValidation.s4', 'Résumé', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(283, 1, 'receptionValidation.s4.quantityTotal', 'La quantité totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(284, 1, 'receptionValidation.s4.unitPriceBuyTotal', 'Le prix unitaire totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(285, 1, 'receptionValidation.s4.remiseAvg', 'La moyenne de remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(286, 1, 'receptionValidation.s4.tvaAvg', 'La monyenne de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(287, 1, 'receptionValidation.s4.totalHtTotal', 'hore taxt total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(288, 1, 'receptionValidation.s4.totalTtcTotal', 'TTC total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(289, 1, 'receptionHistory.s4', 'Résumé Général', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(290, 1, 'receptionHistory.s4.quantityTotal', 'La quantité totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(291, 1, 'receptionHistory.s4.unitPriceBuyTotal', 'Le prix unitaire totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(292, 1, 'receptionHistory.s4.remiseAvg', 'La moyenne de remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(293, 1, 'receptionHistory.s4.tvaAvg', 'La monyenne de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(294, 1, 'receptionHistory.s4.totalHtTotal', 'hore taxt total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(295, 1, 'receptionHistory.s4.totalTtcTotal', 'TTC total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(303, 1, 'reception.s5', 'Résumé Général', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(304, 1, 'reception.s5.quantityTotal', 'La quantité totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(305, 1, 'reception.s5.unitPriceBuyTotal', 'Le prix unitaire totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(306, 1, 'reception.s5.remiseAvg', 'La moyenne de remise', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(307, 1, 'reception.s5.tvaAvg', 'La monyenne de TVA', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(308, 1, 'reception.s5.totalHtTotal', 'hore taxt total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(309, 1, 'reception.s5.totalTtcTotal', 'TTC total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(310, 1, 'reception.s4.totalHt', 'Total HT', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(311, 1, 'reception.s4.totalTtc', 'Total TTC', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(312, 1, 'inventaire.s1', 'Inventaire générale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(313, 1, 'inventaire.s1.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(314, 1, 'inventaire.s1.designation', 'Designation', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(315, 1, 'inventaire.s1.quantity', 'Quantité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(316, 1, 'inventaire.s1.priceBuy', 'Prix de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(317, 1, 'inventaire.s1.priceSale', 'Prix d''achat', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(318, 1, 'inventaire.s1.active', 'Active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(319, 1, 'inventaire.s1.receptionValidCount', 'Nombre de réception', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(320, 1, 'inventaire.s1.unitPriceBuyMax', 'Max de prix de vent', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(321, 1, 'inventaire.s1.unitPriceBuyMoyenne', 'Moyenne de prix de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(322, 1, 'inventaire.s1.unitPriceBuyMin', 'Min de prix de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(323, 1, 'inventaire.s1.quantityCount', 'Total des quantité', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(324, 1, 'stockState', 'Statut de stock', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(325, 1, 'stockState.s1', 'Statut de stock', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(326, 1, 'stockState.s1.receptionValidCount', 'Le nombre des réceptions valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(327, 1, 'stockState.s1.unitPriceBuyMax', 'le max de prix unitaire de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(328, 1, 'stockState.s1.unitPriceBuyMoyenne', 'la moyenne de prix unitaire de vente', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(329, 1, 'stockState.s1.unitPriceBuyMin', 'Le min de prix de vent unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(330, 1, 'stockState.s1.quantityCount', 'Total des quantités', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(331, 1, 'stockState.s1.productValid', 'Le nombre de produit valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(332, 1, 'stockState.s1.productNotValid', 'Le nombre de produit non valide', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(333, 1, 'stockState.s1.receptionInprogressCount', 'Le nombre des réceptions qui sont en cours', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(334, 1, 'login.s1.forgetPassword', 'Mot de passe oublié.', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(335, 1, 'login.s1.getAccount', 'Obtenir d''un compte', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(336, 1, 'login.s1.sessionActive', 'Garder ma session active', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(337, 1, 'login.s1', 'Authentification', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(338, 1, 'module.s1.access', 'Accès au module', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(339, 1, 'module.s1.logout', 'Déconnexion', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(340, 1, 'bonLivraison.s1', 'Nouveau bon de livraison', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(341, 1, 'bonLivraison.s1.customer', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(342, 1, 'bonLivraison.s1.description', 'Note', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(343, 1, 'bonLivraison.s2', 'Ajouter un produit', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(344, 1, 'bonLivraison.s2.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(345, 1, 'bonLivraison.s2.color', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(346, 1, 'bonLivraison.s2.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(347, 1, 'bonLivraison.s2.unitPriceBuy', 'Prix', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(348, 1, 'bonLivraison.s2.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(349, 1, 'bonLivraison.s2.totalTtc', 'Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(350, 1, 'bonLivraison.s3', 'La liste des articles', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(351, 1, 'bonLivraison.s3.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(352, 1, 'bonLivraison.s3.color', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(353, 1, 'bonLivraison.s3.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(354, 1, 'bonLivraison.s3.price', 'Prix Unitaire', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(355, 1, 'bonLivraison.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(356, 1, 'bonLivraison.s3.totalPriceBuy', 'Prix Totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(357, 1, 'bonLivraison.s4', 'Résumé Général', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(358, 1, 'bonLivraison.s5', 'Mode de paiement', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(359, 1, 'bonLivraison.s4.sumQuantity', 'Quantité Totale', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(360, 1, 'bonLivraison.s4.sumTotal', 'Montant Totale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(361, 1, 'blValidation.s1.id', 'Code', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(362, 1, 'blValidation.s1.customerName', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(363, 1, 'blValidation.s1.description', 'Note', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(364, 1, 'blValidation.s1.sumTotalPriceBuy', 'Prix Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(365, 1, 'blValidation.s1', 'La liste des ventes attentes', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(366, 1, 'blValidation.s2', 'Modifer un vente', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(367, 1, 'blValidation.s2.customer', 'Client', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(368, 1, 'blValidation.s2.description', 'Note', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(369, 1, 'blValidation.s3.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(370, 1, 'blValidation.s3.color', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(371, 1, 'blValidation.s3.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(372, 1, 'blValidation.s3.unitPriceBuy', 'Prix', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(373, 1, 'blValidation.s3.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(374, 1, 'blValidation.s3.totalTtc', 'Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(375, 1, 'blValidation.s4', 'La liste des articles', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(376, 1, 'blValidation.s4.reference', 'Réf', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(377, 1, 'blValidation.s4.color', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(378, 1, 'blValidation.s4.size', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(379, 1, 'blValidation.s4.price', 'Prix', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(380, 1, 'blValidation.s4.quantity', 'Qté', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(381, 1, 'blValidation.s4.totalPriceBuy', 'Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(382, 1, 'blValidation.s5', 'Résumé Général', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(383, 1, 'blValidation.s5.sumQuantity', 'Qté Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(384, 1, 'blValidation.s5.sumTotal', 'Montant Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(385, 1, 'blValidation.s5', 'Mode de paiement', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(386, 1, 'blValidation.s3', 'Ajouter un article', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(387, 1, 'blValidation.s1.sumQuantity', 'Qté Total', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(388, 1, 'blValidation.s6', 'Mode de paiement', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(389, 1, 'reception.s2.sizes', 'Taille de Réception', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(390, 1, 'inventaire.s1.productSizeName', 'Taille', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(391, 1, 'inventaire.s1.productColorName', 'Couleur', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(393, 1, 'profile.s1.firstName', 'Prénom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(394, 1, 'profile.s1.lastName', 'Nom', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(395, 1, 'profile.s1.email', 'E-mail', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(396, 1, 'profile.s1.password', 'Mot de passe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(397, 1, 'profile.s1.replayRassword', 'Re mot de passe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(398, 1, 'profile.s1.cellPhone', 'Télé Mobile', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(399, 1, 'profile.s1.fixedPhone', 'Télé Fixe', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(400, 1, 'profile.s1.adress', 'Adresse', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(401, 1, 'profile.s1', 'Modifier Mon Profile', 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `inf_text_type`
--

CREATE TABLE `inf_text_type` (
`ID` bigint(20) NOT NULL,
  `CODE` tinytext CHARACTER SET latin1,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `inf_text_type`
--

INSERT INTO `inf_text_type` (`ID`, `CODE`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'title', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(2, 'message', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'label', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'help', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(5, 'comment', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(6, 'placeholder', NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(7, 'error', NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_attribute_exclud`
--

CREATE TABLE `pm_attribute_exclud` (
`ID` bigint(20) NOT NULL,
  `PAGE_ID` bigint(20) DEFAULT NULL,
  `INF_ITEM_CODE` tinytext,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_attribute_exclud`
--

INSERT INTO `pm_attribute_exclud` (`ID`, `PAGE_ID`, `INF_ITEM_CODE`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 3, 'ok', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_attribute_validation`
--

CREATE TABLE `pm_attribute_validation` (
`ID` bigint(20) NOT NULL,
  `PAGE_ID` bigint(20) DEFAULT NULL,
  `INF_ITEM_CODE` tinytext,
  `VALIDATION_ID` bigint(20) DEFAULT NULL,
  `PARAMS` mediumtext CHARACTER SET latin1,
  `CUSTOM_ERROR` text CHARACTER SET latin1,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=286 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_attribute_validation`
--

INSERT INTO `pm_attribute_validation` (`ID`, `PAGE_ID`, `INF_ITEM_CODE`, `VALIDATION_ID`, `PARAMS`, `CUSTOM_ERROR`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, 'page1.s1.name', 10, '', 'error', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 1, 'page1.s1.name', 7, '', NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(3, 1, 'page1.s1.name', 9, '3', NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(5, 3, 'expense.s2.description', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(7, 3, 'expense.s2.amount', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(8, 3, 'expense.s2.amount', 2, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(10, 3, 'expense.s2.type', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(11, 4, 'supplier.s1.type', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(14, 4, 'supplier.s1.country', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(15, 4, 'supplier.s1.city', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(16, 4, 'supplier.s1.companyName', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(18, 4, 'supplier.s1.fixedPhone', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(19, 4, 'supplier.s1.adress', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(20, 4, 'supplier.s1.email', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(21, 4, 'supplier.s1.active', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(22, 6, 'reception.s2.name', 10, '', 'error', 1, 'Y', NULL, NULL, NULL, NULL),
(24, 6, 'reception.s2.supplier', 10, '', '', 1, 'Y', NULL, NULL, NULL, NULL),
(31, 5, 'product.s2.designation', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(32, 5, 'product.s2.quantity', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(33, 5, 'product.s2.reference', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(34, 5, 'product.s2.pricesale', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(35, 5, 'product.s2.active', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(36, 5, 'product.s2.status', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(37, 5, 'product.s2.size', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(38, 5, 'product.s2.family', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(39, 5, 'product.s2.color', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(40, 5, 'product.s2.save', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(41, 5, 'product.s2.quantity', 1, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(42, 5, 'product.s2.pricesale', 2, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(43, 3, 'expense.s2.name', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(44, 5, 'product.s2.priceBuy', 2, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(45, 5, 'product.s2.priceBuy', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(46, 4, 'supplier.s1.postCode', 1, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(50, 7, 'customer.s1.adress', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(51, 7, 'customer.s1.country', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(52, 7, 'customer.s1.city', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(53, 7, 'customer.s1.type', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(54, 7, 'customer.s1.active', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(55, 13, 'bonLivraison.s1.customer', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(56, 13, 'bonLivraison.s1.description', 10, NULL, NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(57, 1, 'page1.s1.name', 10, '', 'error', 3, 'Y', NULL, NULL, NULL, NULL),
(58, 1, 'page1.s1.name', 7, '', NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(59, 1, 'page1.s1.name', 9, '3', NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(60, 3, 'expense.s2.description', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(61, 3, 'expense.s2.amount', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(62, 3, 'expense.s2.amount', 2, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(63, 3, 'expense.s2.type', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(64, 4, 'supplier.s1.type', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(65, 4, 'supplier.s1.country', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(66, 4, 'supplier.s1.city', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(67, 4, 'supplier.s1.companyName', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(68, 4, 'supplier.s1.fixedPhone', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(69, 4, 'supplier.s1.adress', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(70, 4, 'supplier.s1.email', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(71, 4, 'supplier.s1.active', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(72, 6, 'reception.s2.name', 10, '', 'error', 3, 'Y', NULL, NULL, NULL, NULL),
(73, 6, 'reception.s2.supplier', 10, '', '', 3, 'Y', NULL, NULL, NULL, NULL),
(74, 5, 'product.s2.designation', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(75, 5, 'product.s2.quantity', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(76, 5, 'product.s2.reference', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(77, 5, 'product.s2.pricesale', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(78, 5, 'product.s2.active', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(79, 5, 'product.s2.status', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(80, 5, 'product.s2.size', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(81, 5, 'product.s2.family', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(82, 5, 'product.s2.color', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(83, 5, 'product.s2.save', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(84, 5, 'product.s2.quantity', 1, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(85, 5, 'product.s2.pricesale', 2, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(86, 3, 'expense.s2.name', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(87, 5, 'product.s2.priceBuy', 2, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(88, 5, 'product.s2.priceBuy', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(89, 4, 'supplier.s1.postCode', 1, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(90, 7, 'customer.s1.adress', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(91, 7, 'customer.s1.country', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(92, 7, 'customer.s1.city', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(93, 7, 'customer.s1.type', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(94, 7, 'customer.s1.active', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(95, 13, 'bonLivraison.s1.customer', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(96, 13, 'bonLivraison.s1.description', 10, NULL, NULL, 3, 'Y', NULL, NULL, NULL, NULL),
(120, 1, 'page1.s1.name', 10, '', 'error', 4, 'Y', NULL, NULL, NULL, NULL),
(121, 1, 'page1.s1.name', 7, '', NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(122, 1, 'page1.s1.name', 9, '3', NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(123, 3, 'expense.s2.description', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(124, 3, 'expense.s2.amount', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(125, 3, 'expense.s2.amount', 2, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(126, 3, 'expense.s2.type', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(127, 4, 'supplier.s1.type', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(128, 4, 'supplier.s1.country', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(129, 4, 'supplier.s1.city', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(130, 4, 'supplier.s1.companyName', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(131, 4, 'supplier.s1.fixedPhone', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(132, 4, 'supplier.s1.adress', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(133, 4, 'supplier.s1.email', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(134, 4, 'supplier.s1.active', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(135, 6, 'reception.s2.name', 10, '', 'error', 4, 'Y', NULL, NULL, NULL, NULL),
(136, 6, 'reception.s2.supplier', 10, '', '', 4, 'Y', NULL, NULL, NULL, NULL),
(137, 5, 'product.s2.designation', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(138, 5, 'product.s2.quantity', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(139, 5, 'product.s2.reference', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(140, 5, 'product.s2.pricesale', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(141, 5, 'product.s2.active', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(142, 5, 'product.s2.status', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(143, 5, 'product.s2.size', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(144, 5, 'product.s2.family', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(145, 5, 'product.s2.color', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(146, 5, 'product.s2.save', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(147, 5, 'product.s2.quantity', 1, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(148, 5, 'product.s2.pricesale', 2, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(149, 3, 'expense.s2.name', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(150, 5, 'product.s2.priceBuy', 2, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(151, 5, 'product.s2.priceBuy', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(152, 4, 'supplier.s1.postCode', 1, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(153, 7, 'customer.s1.adress', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(154, 7, 'customer.s1.country', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(155, 7, 'customer.s1.city', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(156, 7, 'customer.s1.type', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(157, 7, 'customer.s1.active', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(158, 13, 'bonLivraison.s1.customer', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(159, 13, 'bonLivraison.s1.description', 10, NULL, NULL, 4, 'Y', NULL, NULL, NULL, NULL),
(183, 1, 'page1.s1.name', 10, '', 'error', 5, 'Y', NULL, NULL, NULL, NULL),
(184, 1, 'page1.s1.name', 7, '', NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(185, 1, 'page1.s1.name', 9, '3', NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(186, 3, 'expense.s2.description', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(187, 3, 'expense.s2.amount', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(188, 3, 'expense.s2.amount', 2, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(189, 3, 'expense.s2.type', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(190, 4, 'supplier.s1.type', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(191, 4, 'supplier.s1.country', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(192, 4, 'supplier.s1.city', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(193, 4, 'supplier.s1.companyName', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(194, 4, 'supplier.s1.fixedPhone', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(195, 4, 'supplier.s1.adress', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(196, 4, 'supplier.s1.email', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(197, 4, 'supplier.s1.active', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(198, 6, 'reception.s2.name', 10, '', 'error', 5, 'Y', NULL, NULL, NULL, NULL),
(199, 6, 'reception.s2.supplier', 10, '', '', 5, 'Y', NULL, NULL, NULL, NULL),
(200, 5, 'product.s2.designation', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(201, 5, 'product.s2.quantity', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(202, 5, 'product.s2.reference', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(203, 5, 'product.s2.pricesale', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(204, 5, 'product.s2.active', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(205, 5, 'product.s2.status', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(206, 5, 'product.s2.size', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(207, 5, 'product.s2.family', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(208, 5, 'product.s2.color', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(209, 5, 'product.s2.save', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(210, 5, 'product.s2.quantity', 1, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(211, 5, 'product.s2.pricesale', 2, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(212, 3, 'expense.s2.name', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(213, 5, 'product.s2.priceBuy', 2, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(214, 5, 'product.s2.priceBuy', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(215, 4, 'supplier.s1.postCode', 1, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(216, 7, 'customer.s1.adress', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(217, 7, 'customer.s1.country', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(218, 7, 'customer.s1.city', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(219, 7, 'customer.s1.type', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(220, 7, 'customer.s1.active', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(221, 13, 'bonLivraison.s1.customer', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(222, 13, 'bonLivraison.s1.description', 10, NULL, NULL, 5, 'Y', NULL, NULL, NULL, NULL),
(246, 1, 'page1.s1.name', 10, '', 'error', 6, 'Y', NULL, NULL, NULL, NULL),
(247, 1, 'page1.s1.name', 7, '', NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(248, 1, 'page1.s1.name', 9, '3', NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(249, 3, 'expense.s2.description', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(250, 3, 'expense.s2.amount', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(251, 3, 'expense.s2.amount', 2, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(252, 3, 'expense.s2.type', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(253, 4, 'supplier.s1.type', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(254, 4, 'supplier.s1.country', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(255, 4, 'supplier.s1.city', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(256, 4, 'supplier.s1.companyName', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(257, 4, 'supplier.s1.fixedPhone', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(258, 4, 'supplier.s1.adress', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(259, 4, 'supplier.s1.email', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(260, 4, 'supplier.s1.active', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(261, 6, 'reception.s2.name', 10, '', 'error', 6, 'Y', NULL, NULL, NULL, NULL),
(262, 6, 'reception.s2.supplier', 10, '', '', 6, 'Y', NULL, NULL, NULL, NULL),
(263, 5, 'product.s2.designation', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(264, 5, 'product.s2.quantity', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(265, 5, 'product.s2.reference', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(266, 5, 'product.s2.pricesale', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(267, 5, 'product.s2.active', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(268, 5, 'product.s2.status', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(269, 5, 'product.s2.size', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(270, 5, 'product.s2.family', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(271, 5, 'product.s2.color', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(272, 5, 'product.s2.save', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(273, 5, 'product.s2.quantity', 1, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(274, 5, 'product.s2.pricesale', 2, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(275, 3, 'expense.s2.name', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(276, 5, 'product.s2.priceBuy', 2, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(277, 5, 'product.s2.priceBuy', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(278, 4, 'supplier.s1.postCode', 1, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(279, 7, 'customer.s1.adress', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(280, 7, 'customer.s1.country', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(281, 7, 'customer.s1.city', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(282, 7, 'customer.s1.type', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(283, 7, 'customer.s1.active', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(284, 13, 'bonLivraison.s1.customer', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL),
(285, 13, 'bonLivraison.s1.description', 10, NULL, NULL, 6, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_category`
--

CREATE TABLE `pm_category` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT NULL,
  `CATEGORY_TYPE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_category`
--

INSERT INTO `pm_category` (`ID`, `NAME`, `DESCRIPTION`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `ACTIVE`, `CATEGORY_TYPE_ID`, `SORT_KEY`) VALUES
(1, 'Fichiers', 'Fichiers', NULL, NULL, NULL, NULL, 'Y', 1, 1),
(2, 'Entrées : Fournisseur', 'Entrées', NULL, NULL, NULL, NULL, 'Y', 1, 2),
(3, 'Sorties : Client', 'Sorties', NULL, NULL, NULL, NULL, 'Y', 1, 3),
(4, 'Edition', 'Edition', '2014-11-13 00:00:00', NULL, NULL, NULL, 'Y', 1, 4),
(5, 'Divers', 'Divers', NULL, NULL, NULL, NULL, 'Y', 1, 6),
(6, 'Rapport', 'Rapport', NULL, NULL, NULL, NULL, 'Y', 1, 4);

-- --------------------------------------------------------

--
-- Structure de la table `pm_category_type`
--

CREATE TABLE `pm_category_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext CHARACTER SET latin1,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_category_type`
--

INSERT INTO `pm_category_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Header', 'DFFF', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Sub Header', ' FGGE', 2, 'Y', NULL, NULL, NULL, NULL),
(3, 'Principal', ' FERT', 3, 'Y', NULL, NULL, NULL, NULL),
(4, 'Footer', 'FFZF', 5, 'Y', NULL, NULL, NULL, NULL),
(5, 'Footer', '  GGDZ', 4, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_component`
--

CREATE TABLE `pm_component` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` tinytext,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` tinytext,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_component`
--

INSERT INTO `pm_component` (`ID`, `NAME`, `DESCRIPTION`, `ACTIVE`, `SORT_KEY`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'inpuText', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(2, 'textarea', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(3, 'comobox', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(4, 'table', NULL, 'Y', NULL, NULL, NULL, NULL, NULL),
(5, 'column', NULL, 'Y', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_composition`
--

CREATE TABLE `pm_composition` (
`ID` bigint(20) NOT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `GROUP_ID` bigint(20) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `MENU_ID` bigint(20) DEFAULT NULL,
  `PAGE_ID` bigint(20) DEFAULT NULL,
  `INDEX_SHOW` char(1) DEFAULT NULL,
  `GROUP_SORT` bigint(20) DEFAULT NULL,
  `MENU_SORT` bigint(20) DEFAULT NULL,
  `CATEGORY_SORT` bigint(20) DEFAULT NULL,
  `PAGE_SORT` bigint(20) DEFAULT NULL,
  `INDEX_SORT` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=188 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_composition`
--

INSERT INTO `pm_composition` (`ID`, `CLT_MODULE_ID`, `GROUP_ID`, `CATEGORY_ID`, `MENU_ID`, `PAGE_ID`, `INDEX_SHOW`, `GROUP_SORT`, `MENU_SORT`, `CATEGORY_SORT`, `PAGE_SORT`, `INDEX_SORT`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(31, 1, 3, 2, 8, 6, '', 1, 1, 2, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(32, 1, 3, 4, 6, 3, '', 1, 1, 4, 1, 0, 'Y', NULL, NULL, NULL, NULL),
(33, 1, 3, 2, 7, 4, '', 1, 1, 2, 1, 0, 'Y', NULL, NULL, NULL, NULL),
(34, 1, 2, 5, 12, 3, '', 1, 6, 6, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(39, 1, 2, 2, 13, 4, NULL, 1, 2, 2, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(41, 1, 2, 1, 14, 5, NULL, NULL, 1, 1, NULL, NULL, 'N', NULL, NULL, NULL, NULL),
(42, 1, 2, 1, 15, 6, NULL, NULL, 3, 1, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(44, 1, 3, 3, 18, 7, NULL, 1, 1, 2, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(48, 1, 3, 2, 20, 9, NULL, 1, 1, 2, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(60, 1, 3, 1, 22, 11, NULL, NULL, NULL, 1, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(61, 1, 3, 1, 23, 12, NULL, NULL, NULL, 1, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(62, 1, 3, 3, 24, 13, NULL, NULL, NULL, 3, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(63, 1, 3, 3, 25, 14, NULL, NULL, NULL, 3, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(64, 1, 3, 3, 26, 15, NULL, NULL, NULL, 3, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(65, 1, 3, 6, 27, 16, NULL, NULL, 2, 5, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(66, 1, 3, 4, 28, 17, NULL, NULL, NULL, 4, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(67, 1, 3, 4, 29, 18, NULL, NULL, NULL, 4, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(68, 1, 3, 5, 32, 19, NULL, NULL, NULL, 6, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(69, 1, 3, 1, 2, 5, NULL, NULL, 3, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL),
(70, 1, 2, 1, 30, 13, NULL, NULL, 5, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(71, 1, 2, 1, 17, 7, NULL, NULL, 4, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(72, 1, 3, 5, 31, 20, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(73, 3, 3, 2, 8, 6, '', 1, 1, 2, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(74, 3, 3, 4, 6, 3, '', 1, 1, 4, 1, 0, 'Y', NULL, NULL, NULL, NULL),
(75, 3, 3, 2, 7, 4, '', 1, 1, 2, 1, 0, 'Y', NULL, NULL, NULL, NULL),
(76, 3, 2, 5, 12, 3, '', 1, 6, 6, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(77, 3, 2, 2, 13, 4, NULL, 1, 2, 2, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(78, 3, 2, 1, 14, 5, NULL, NULL, 1, 1, NULL, NULL, 'N', NULL, NULL, NULL, NULL),
(79, 3, 2, 1, 15, 6, NULL, NULL, 3, 1, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(80, 3, 3, 3, 18, 7, NULL, 1, 1, 2, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(81, 3, 3, 2, 20, 9, NULL, 1, 1, 2, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(82, 3, 3, 1, 22, 11, NULL, NULL, NULL, 1, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(83, 3, 3, 1, 23, 12, NULL, NULL, NULL, 1, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(84, 3, 3, 3, 24, 13, NULL, NULL, NULL, 3, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(85, 3, 3, 3, 25, 14, NULL, NULL, NULL, 3, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(86, 3, 3, 3, 26, 15, NULL, NULL, NULL, 3, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(87, 3, 3, 6, 27, 16, NULL, NULL, 2, 5, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(88, 3, 3, 4, 28, 17, NULL, NULL, NULL, 4, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(89, 3, 3, 4, 29, 18, NULL, NULL, NULL, 4, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(90, 3, 3, 5, 32, 19, NULL, NULL, NULL, 6, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(91, 3, 3, 1, 2, 5, NULL, NULL, 3, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL),
(92, 3, 2, 1, 30, 13, NULL, NULL, 5, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(93, 3, 2, 1, 17, 7, NULL, NULL, 4, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(94, 3, 3, 5, 31, 20, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(104, 4, 3, 2, 8, 6, '', 1, 1, 2, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(105, 4, 3, 4, 6, 3, '', 1, 1, 4, 1, 0, 'Y', NULL, NULL, NULL, NULL),
(106, 4, 3, 2, 7, 4, '', 1, 1, 2, 1, 0, 'Y', NULL, NULL, NULL, NULL),
(107, 4, 2, 5, 12, 3, '', 1, 6, 6, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(108, 4, 2, 2, 13, 4, NULL, 1, 2, 2, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(109, 4, 2, 1, 14, 5, NULL, NULL, 1, 1, NULL, NULL, 'N', NULL, NULL, NULL, NULL),
(110, 4, 2, 1, 15, 6, NULL, NULL, 3, 1, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(111, 4, 3, 3, 18, 7, NULL, 1, 1, 2, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(112, 4, 3, 2, 20, 9, NULL, 1, 1, 2, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(113, 4, 3, 1, 22, 11, NULL, NULL, NULL, 1, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(114, 4, 3, 1, 23, 12, NULL, NULL, NULL, 1, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(115, 4, 3, 3, 24, 13, NULL, NULL, NULL, 3, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(116, 4, 3, 3, 25, 14, NULL, NULL, NULL, 3, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(117, 4, 3, 3, 26, 15, NULL, NULL, NULL, 3, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(118, 4, 3, 6, 27, 16, NULL, NULL, 2, 5, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(119, 4, 3, 4, 28, 17, NULL, NULL, NULL, 4, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(120, 4, 3, 4, 29, 18, NULL, NULL, NULL, 4, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(121, 4, 3, 5, 32, 19, NULL, NULL, NULL, 6, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(122, 4, 3, 1, 2, 5, NULL, NULL, 3, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL),
(123, 4, 2, 1, 30, 13, NULL, NULL, 5, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(124, 4, 2, 1, 17, 7, NULL, NULL, 4, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(125, 4, 3, 5, 31, 20, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(135, 5, 3, 2, 8, 6, '', 1, 1, 2, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(136, 5, 3, 4, 6, 3, '', 1, 1, 4, 1, 0, 'Y', NULL, NULL, NULL, NULL),
(137, 5, 3, 2, 7, 4, '', 1, 1, 2, 1, 0, 'Y', NULL, NULL, NULL, NULL),
(138, 5, 2, 5, 12, 3, '', 1, 6, 6, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(139, 5, 2, 2, 13, 4, NULL, 1, 2, 2, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(140, 5, 2, 1, 14, 5, NULL, NULL, 1, 1, NULL, NULL, 'N', NULL, NULL, NULL, NULL),
(141, 5, 2, 1, 15, 6, NULL, NULL, 3, 1, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(142, 5, 3, 3, 18, 7, NULL, 1, 1, 2, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(143, 5, 3, 2, 20, 9, NULL, 1, 1, 2, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(144, 5, 3, 1, 22, 11, NULL, NULL, NULL, 1, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(145, 5, 3, 1, 23, 12, NULL, NULL, NULL, 1, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(146, 5, 3, 3, 24, 13, NULL, NULL, NULL, 3, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(147, 5, 3, 3, 25, 14, NULL, NULL, NULL, 3, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(148, 5, 3, 3, 26, 15, NULL, NULL, NULL, 3, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(149, 5, 3, 6, 27, 16, NULL, NULL, 2, 5, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(150, 5, 3, 4, 28, 17, NULL, NULL, NULL, 4, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(151, 5, 3, 4, 29, 18, NULL, NULL, NULL, 4, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(152, 5, 3, 5, 32, 19, NULL, NULL, NULL, 6, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(153, 5, 3, 1, 2, 5, NULL, NULL, 3, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL),
(154, 5, 2, 1, 30, 13, NULL, NULL, 5, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(155, 5, 2, 1, 17, 7, NULL, NULL, 4, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(156, 5, 3, 5, 31, 20, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(166, 6, 3, 2, 8, 6, '', 1, 1, 2, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(167, 6, 3, 4, 6, 3, '', 1, 1, 4, 1, 0, 'Y', NULL, NULL, NULL, NULL),
(168, 6, 3, 2, 7, 4, '', 1, 1, 2, 1, 0, 'Y', NULL, NULL, NULL, NULL),
(169, 6, 2, 5, 12, 3, '', 1, 6, 6, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(170, 6, 2, 2, 13, 4, NULL, 1, 2, 2, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(171, 6, 2, 1, 14, 5, NULL, NULL, 1, 1, NULL, NULL, 'N', NULL, NULL, NULL, NULL),
(172, 6, 2, 1, 15, 6, NULL, NULL, 3, 1, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(173, 6, 3, 3, 18, 7, NULL, 1, 1, 2, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(174, 6, 3, 2, 20, 9, NULL, 1, 1, 2, 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(175, 6, 3, 1, 22, 11, NULL, NULL, NULL, 1, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(176, 6, 3, 1, 23, 12, NULL, NULL, NULL, 1, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(177, 6, 3, 3, 24, 13, NULL, NULL, NULL, 3, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(178, 6, 3, 3, 25, 14, NULL, NULL, NULL, 3, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(179, 6, 3, 3, 26, 15, NULL, NULL, NULL, 3, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(180, 6, 3, 6, 27, 16, NULL, NULL, 2, 5, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(181, 6, 3, 4, 28, 17, NULL, NULL, NULL, 4, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(182, 6, 3, 4, 29, 18, NULL, NULL, NULL, 4, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(183, 6, 3, 5, 32, 19, NULL, NULL, NULL, 6, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(184, 6, 3, 1, 2, 5, NULL, NULL, 3, NULL, NULL, NULL, 'N', NULL, NULL, NULL, NULL),
(185, 6, 2, 1, 30, 13, NULL, NULL, 5, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(186, 6, 2, 1, 17, 7, NULL, NULL, 4, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL),
(187, 6, 3, 5, 31, 20, NULL, NULL, NULL, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_group`
--

CREATE TABLE `pm_group` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `GROUP_TYPE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_group`
--

INSERT INTO `pm_group` (`ID`, `NAME`, `DESCRIPTION`, `GROUP_TYPE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Header', 'header', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'sub Header', 'sub Header', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(3, 'Menu', 'Menu', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(4, 'Footer', 'Footer', 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_group_type`
--

CREATE TABLE `pm_group_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_group_type`
--

INSERT INTO `pm_group_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Simple', 'Simple', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Avancée', 'Avancée', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_menu`
--

CREATE TABLE `pm_menu` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `MENU_TYPE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_menu`
--

INSERT INTO `pm_menu` (`ID`, `NAME`, `DESCRIPTION`, `MENU_TYPE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Page de TEST', 'Page de TEST', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Gestion des produits', 'sec 2 ', 1, 2, 'Y', NULL, NULL, NULL, NULL),
(6, 'Gestion des dépenses', 'General', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(7, 'Gestion des fournisseurs   ', 'Menu : add admin', 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(8, 'Gestion des réception', 'Gestion des dépenses', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(11, 'Réception', 'Réception', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(12, 'Dépense', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(13, 'Fournisseurs', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(14, 'Produits', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(15, 'Réception', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(17, 'Client', 'Client', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(18, 'Gestion des clients', 'Gestion des clients', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(19, 'Validation des réceptions', 'Validation des réceptions', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(20, 'Historique des réceptions', 'Historique des réceptions', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(21, 'Promotion des produits', 'Promotion des produits', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(22, 'Inventaire', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(23, 'Statut de stock', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(24, 'Nouveau Vente', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(25, 'Vente en attente', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(26, 'Historique des ventes', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(27, 'Rapports cotidiennet', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(28, 'Changer un produit', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(29, 'Avance de produit', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(30, 'Nou. Vente', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(31, 'Mon profil', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL),
(32, 'Liste des valeurs', NULL, 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_menu_type`
--

CREATE TABLE `pm_menu_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_menu_type`
--

INSERT INTO `pm_menu_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'menu1 typ', 'Menu 1 type ', 1, 'Y', NULL, NULL, NULL, NULL),
(4, 'menu type 2', ' mznu type 2', 2, 'Y', NULL, NULL, NULL, NULL),
(6, NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_page`
--

CREATE TABLE `pm_page` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `LABEL` tinytext CHARACTER SET latin1,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `PAGE_TYPE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `PACKAGE_JAVA` tinytext,
  `FOLDER_JSF` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_page`
--

INSERT INTO `pm_page` (`ID`, `NAME`, `DESCRIPTION`, `LABEL`, `SORT_KEY`, `PAGE_TYPE_ID`, `ACTIVE`, `USER_CREATION`, `DATE_CREATION`, `USER_UPDATE`, `DATE_UPDATE`, `PACKAGE_JAVA`, `FOLDER_JSF`) VALUES
(1, 'page Test', 'page test1', 'page1', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.page1', 'stock/page1'),
(2, 'Page add admin ', 'Page add admin', 'addAdmin', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.admin.addAdmin', 'admin/addAdmin'),
(3, 'Page Expense', 'Page pour la gestion des dépense', 'expense', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.expense', 'stock/expense'),
(4, 'Page Supplier', 'Page pour la gestion des fournisseurs', 'supplier', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.supplier', 'stock/supplier'),
(5, 'Page produit', 'Page pour la gestion des produits', 'product', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.product', 'stock/product'),
(6, 'Page de réception', 'Page pour la gestion des réceptions et BL', 'reception', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.reception', 'stock/reception'),
(7, 'Page de custmer', 'customer', 'customer', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.customer', 'stock/customer'),
(8, 'Page de validation des réceptions', 'Page de validation des réceptions', 'receptionValidation', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.receptionValidation', 'stock/receptionValidation'),
(9, 'page de historique de reception', 'page de historique de reception', 'receptionHistory', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.receptionHistory', 'stock/receptionHistory'),
(10, 'Page de promotion des prodtuis', 'promo produit', 'encours', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.encours', 'stock/encours'),
(11, 'Inventaire', NULL, 'inventaire', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.inventaire', 'stock/inventaire'),
(12, 'statut de stock : capital', NULL, 'stockState', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.stockState', 'stock/stockState'),
(13, 'Bon de livraison Client', NULL, 'bonLivraison', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.bonLivraison', 'stock/bonLivraison'),
(14, 'Valdation de Bon de livraison Client', NULL, 'blValidation', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.blValidation', 'stock/blValidation'),
(15, 'Historique de Bon de livraison Client', NULL, 'blHistory', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.blHistory', 'stock/blHistory'),
(16, 'les rapports cotidiennet', NULL, 'rapDaily', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.rapDaily', 'stock/rapDaily'),
(17, 'Changé un produit', NULL, 'encours', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.encours', 'stock/encours'),
(18, 'avance de produit', NULL, 'encours', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.stock.encours', 'stock/encours'),
(19, 'la liste des valeurs', NULL, 'lovs', NULL, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.admin.lovs', 'admin/lovs'),
(20, 'Mon profil', NULL, 'profile', 1, 1, 'Y', NULL, NULL, NULL, NULL, 'ma.mystock.web.beans.admin.profile', 'admin/profile'),
(21, 'Les paramètres basiques', NULL, 'basicParameter', NULL, 1, 'Y', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_page_attribute`
--

CREATE TABLE `pm_page_attribute` (
`ID` bigint(20) NOT NULL,
  `PAGE_ID` bigint(20) DEFAULT NULL,
  `INF_ITEM_CODE` varchar(255) DEFAULT NULL,
  `PM_COMPONENT_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` varchar(45) DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_page_attribute`
--

INSERT INTO `pm_page_attribute` (`ID`, `PAGE_ID`, `INF_ITEM_CODE`, `PM_COMPONENT_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, 'page1.s1.name', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 3, 'expense.s1.name', 5, 2, 'Y', NULL, NULL, NULL, NULL),
(3, 3, 'expense.s1.description', 5, 3, 'Y', NULL, NULL, NULL, NULL),
(4, 3, 'expense.s1.type', 5, 5, 'Y', NULL, NULL, NULL, NULL),
(5, 3, 'expense.s1.amount', 5, 4, 'Y', NULL, NULL, NULL, NULL),
(6, 3, 'expense.s1', 4, 1, 'Y', NULL, NULL, NULL, NULL),
(7, 3, 'expense.s1.edit', 5, 6, 'Y', NULL, NULL, NULL, NULL),
(8, 3, 'expense.s1.delete', 5, 7, 'Y', NULL, NULL, NULL, NULL),
(9, 3, 'expense.s2.type', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(10, 3, 'expense.s2.name', 1, 2, 'Y', NULL, NULL, NULL, NULL),
(11, 3, 'expense.s2.description', 2, 3, 'Y', NULL, NULL, NULL, NULL),
(12, 3, 'expense.s2.amount', 1, 4, 'Y', NULL, NULL, NULL, NULL),
(13, 6, 'reception.s1', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(14, 6, 'reception.s2.name', 1, 2, 'Y', NULL, NULL, NULL, NULL),
(15, 6, 'reception.s2.description', 1, 3, 'Y', NULL, NULL, NULL, NULL),
(16, 6, 'reception.s2.supplier', 1, 4, 'Y', NULL, NULL, NULL, NULL),
(17, 6, 'reception.s2.deadline', 1, 5, 'Y', NULL, NULL, NULL, NULL),
(18, 6, 'reception.s2.souche', 1, 6, 'Y', NULL, NULL, NULL, NULL),
(19, 6, 'reception.s2.amount', 1, 7, 'Y', NULL, NULL, NULL, NULL),
(20, 6, 'reception.s2.deposit', 1, 8, 'Y', NULL, NULL, NULL, NULL),
(22, 6, 'reception.s1.id', 1, 5, 'Y', NULL, NULL, NULL, NULL),
(23, 6, 'reception.s1.name', 1, 5, 'Y', NULL, NULL, NULL, NULL),
(24, 6, 'reception.s1.souche', 1, 5, 'Y', NULL, NULL, NULL, NULL),
(25, 6, 'reception.s1.supplierId', 1, 5, 'Y', NULL, NULL, NULL, NULL),
(26, 6, 'reception.s1.amount', 1, 5, 'Y', NULL, NULL, NULL, NULL),
(28, 6, 'reception.s1.deadline', 1, 5, 'Y', NULL, NULL, NULL, NULL),
(29, 6, 'reception.s1.option', 1, 5, 'Y', NULL, NULL, NULL, NULL),
(30, 6, 'reception.s1.edit', 1, 5, 'Y', NULL, NULL, NULL, NULL),
(31, 6, 'reception.s1.delete', 1, 5, 'Y', NULL, NULL, NULL, NULL),
(33, 4, 'supplier.s1', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(36, 4, 'supplier.s1.country', 1, 3, 'Y', NULL, NULL, NULL, NULL),
(37, 4, 'supplier.s1.city', 1, 4, 'Y', NULL, NULL, NULL, NULL),
(38, 4, 'supplier.s1.companyName', 1, 5, 'Y', NULL, NULL, NULL, NULL),
(39, 4, 'supplier.s1.cellPhone', 1, 6, 'Y', NULL, NULL, NULL, NULL),
(40, 4, 'supplier.s1.fixedPhone', 1, 7, 'Y', NULL, NULL, NULL, NULL),
(41, 4, 'supplier.s1.adress', 1, 8, 'Y', NULL, NULL, NULL, NULL),
(42, 4, 'supplier.s1.email', 1, 9, 'Y', NULL, NULL, NULL, NULL),
(43, 4, 'supplier.s1.type', 1, 10, 'Y', NULL, NULL, NULL, NULL),
(44, 4, 'supplier.s1.edit', 1, 11, 'Y', NULL, NULL, NULL, NULL),
(45, 4, 'supplier.s1.delete', 1, 12, 'Y', NULL, NULL, NULL, NULL),
(46, 4, 'supplier.s1.add', 1, 13, 'Y', NULL, NULL, NULL, NULL),
(47, 4, 'supplier.s2.firstName', 1, 14, 'Y', NULL, NULL, NULL, NULL),
(48, 4, 'supplier.s2.lastName', 1, 15, 'Y', NULL, NULL, NULL, NULL),
(49, 4, 'supplier.s2.type', 1, 16, 'Y', NULL, NULL, NULL, NULL),
(50, 4, 'supplier.s2.country', 1, 17, 'Y', NULL, NULL, NULL, NULL),
(51, 4, 'supplier.s2.city', 1, 18, 'Y', NULL, NULL, NULL, NULL),
(52, 4, 'supplier.s2.companyName', 1, 19, 'Y', NULL, NULL, NULL, NULL),
(53, 4, 'supplier.s2.cellPhone', 1, 20, 'Y', NULL, NULL, NULL, NULL),
(54, 4, 'supplier.s2.fixedPhone', 1, 21, 'Y', NULL, NULL, NULL, NULL),
(55, 4, 'supplier.s2.adress', 1, 22, 'Y', NULL, NULL, NULL, NULL),
(56, 4, 'supplier.s2.email', 1, 23, 'Y', NULL, NULL, NULL, NULL),
(57, 4, 'product.s2.ative', 1, 24, 'Y', NULL, NULL, NULL, NULL),
(58, 4, 'supplier.s2.save', 1, 25, 'Y', NULL, NULL, NULL, NULL),
(59, 4, 'supplier.s1.option', 1, 25, 'Y', NULL, NULL, NULL, NULL),
(60, 5, 'product.s1', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(61, 5, 'product.s1.designation', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(62, 5, 'product.s1.quantity', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(63, 5, 'product.s1.reference', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(64, 5, 'product.s1.pricesale', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(65, 5, 'product.s1.size', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(66, 5, 'product.s1.status', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(67, 5, 'product.s1.family', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(68, 5, 'product.s1.color', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(69, 5, 'product.s1.option', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(70, 5, 'product.s1.edit', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(71, 5, 'product.s1.add', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(72, 5, 'product.s1.delete', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(73, 5, 'product.s2.designation', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(74, 5, 'product.s2.quantity', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(75, 5, 'product.s2.reference', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(76, 5, 'product.s2.pricesale', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(77, 5, 'product.s2.active', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(78, 5, 'product.s2.status', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(79, 5, 'product.s2.size', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(80, 5, 'product.s2.family', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(81, 5, 'product.s2.color', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(82, 5, 'product.s2.save', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(83, 5, 'product.s1.priceBuy', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(84, 5, 'product.s2.priceBuy', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(85, 4, 'supplier.s1.postCode', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(86, 4, 'supplier.s2.postCode', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(87, 13, 'bonLivraison.s1.description', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(88, 13, 'bonLivraison.s1.customer', 1, 0, 'Y', NULL, NULL, NULL, NULL),
(89, 6, 'reception.s2.sizes', 1, 0, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_page_parameter`
--

CREATE TABLE `pm_page_parameter` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext CHARACTER SET latin1,
  `DESCRIPTION` text CHARACTER SET latin1,
  `DEFAULT_VALUE` text,
  `PAGE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_page_parameter`
--

INSERT INTO `pm_page_parameter` (`ID`, `NAME`, `DESCRIPTION`, `DEFAULT_VALUE`, `PAGE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'titre de l''application', 'title of application', 'My Stock Management', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'visiblité de form add edit ', NULL, 'no', 3, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_page_parameter_module`
--

CREATE TABLE `pm_page_parameter_module` (
`ID` bigint(20) NOT NULL,
  `PAGE_PARAMETER_ID` bigint(20) DEFAULT NULL,
  `VALUE` longtext,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_page_parameter_module`
--

INSERT INTO `pm_page_parameter_module` (`ID`, `PAGE_PARAMETER_ID`, `VALUE`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 1, 'yes', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 2, 'yes', 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_page_parameter_type`
--

CREATE TABLE `pm_page_parameter_type` (
  `ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `pm_page_type`
--

CREATE TABLE `pm_page_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_page_type`
--

INSERT INTO `pm_page_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'TYpe 1', NULL, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'type autre ', 'desc', 2, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `pm_validation_type`
--

CREATE TABLE `pm_validation_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `PARAM_NUMBER` bigint(20) DEFAULT NULL,
  `ERROR_MESSAGE` tinytext,
  `HELP` text CHARACTER SET latin1,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `pm_validation_type`
--

INSERT INTO `pm_validation_type` (`ID`, `NAME`, `DESCRIPTION`, `PARAM_NUMBER`, `ERROR_MESSAGE`, `HELP`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'integer', 'integer', 0, 'validation.v1.integer', 'validation.v1.integer', 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'double', 'double', 0, 'validation.v1.double', 'validation.v1.double', 2, 'Y', NULL, NULL, NULL, NULL),
(3, 'char', 'char', 0, 'validation.v1.char', 'validation.v1.char', 3, 'Y', NULL, NULL, NULL, NULL),
(4, 'date', 'date', 0, 'validation.v1.date', 'validation.v1.date', 4, 'Y', NULL, NULL, NULL, NULL),
(5, 'time', 'time', 0, 'validation.v1.time', 'validation.v1.time', 5, 'Y', NULL, NULL, NULL, NULL),
(6, 'datetime', 'datetime', 0, 'validation.v1.datetime', 'validation.v1.datetime', 6, 'Y', NULL, NULL, NULL, NULL),
(7, 'email', 'email', 0, 'validation.v1.email', 'validation.v1.email', 7, 'Y', NULL, NULL, NULL, NULL),
(8, 'phone', 'phone', 0, 'validation.v1.phone', 'validation.v1.phone', 8, 'Y', NULL, NULL, NULL, NULL),
(9, 'maxlenght', 'maxlenght', 0, 'validation.v1.maxlenght', 'validation.v1.maxlenght', 9, 'Y', NULL, NULL, NULL, NULL),
(10, 'required', 'required', 0, 'validation.v1.required', 'validation.v1.required', 10, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_advanced`
--

CREATE TABLE `sm_advanced` (
`ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `CUSTOMER_ID` bigint(20) DEFAULT NULL,
  `ADVANCED_STATUS_ID` bigint(20) DEFAULT NULL,
  `AMOUNT` double DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_advanced_status`
--

CREATE TABLE `sm_advanced_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_bank_type`
--

CREATE TABLE `sm_bank_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_check`
--

CREATE TABLE `sm_check` (
`ID` bigint(20) NOT NULL,
  `BANK_ID` bigint(20) DEFAULT NULL,
  `NAME_PERSON` tinytext CHARACTER SET latin1,
  `NUMBER` tinytext CHARACTER SET latin1,
  `ACCOUNT` tinytext CHARACTER SET latin1,
  `PHONE` tinytext CHARACTER SET latin1,
  `PAYABLE_IN` tinytext CHARACTER SET latin1,
  `AMOUNT` double DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_customer`
--

CREATE TABLE `sm_customer` (
`ID` bigint(20) NOT NULL,
  `FIRST_NAME` tinytext CHARACTER SET latin1,
  `LAST_NAME` tinytext CHARACTER SET latin1,
  `INF_COUNTRY_ID` bigint(20) DEFAULT NULL,
  `INF_CITY_ID` bigint(20) DEFAULT NULL,
  `CELL_PHONE` tinytext CHARACTER SET latin1,
  `FIXED_PHONE` tinytext CHARACTER SET latin1,
  `ADRESS` tinytext CHARACTER SET latin1,
  `EMAIL` tinytext CHARACTER SET latin1,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `CUSTOMER_TYPE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_customer`
--

INSERT INTO `sm_customer` (`ID`, `FIRST_NAME`, `LAST_NAME`, `INF_COUNTRY_ID`, `INF_CITY_ID`, `CELL_PHONE`, `FIXED_PHONE`, `ADRESS`, `EMAIL`, `CLT_MODULE_ID`, `CUSTOMER_TYPE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(4, 'AHmed', 'ML', 1, 1, '+212 6 66 93 98', '+212 6 66 93 98', 'SYBA 87', 'abdessamad.hallal@evision.ca', 1, 1, 1, 'Y', NULL, 1, NULL, 1),
(5, 'Abdessamad2', 'HALLAL2', 1, 1, '+212 5 76 87 87', '+212 5 76 87 87', 'SYBA 5 N 879', 'abdessamad@hallal.com', 1, 1, NULL, 'Y', NULL, 1, NULL, 1),
(6, 'Abdessamad3', 'HALLAL3', 1, 1, '+212 5 76 87 87', '+212 5 76 87 87', 'SYBA 5 N 879', 'abdessamad@hallal.com', 1, 1, NULL, 'Y', NULL, 1, NULL, 1),
(7, 'a', 'b', 1, NULL, 'd', 'e', '', 'c', 1, NULL, NULL, 'Y', NULL, 1, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `sm_customer_type`
--

CREATE TABLE `sm_customer_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_customer_type`
--

INSERT INTO `sm_customer_type` (`ID`, `NAME`, `DESCRIPTION`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Particulier', 'Particulier', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(2, 'Société', 'Société', 1, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_deposit`
--

CREATE TABLE `sm_deposit` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_deposit`
--

INSERT INTO `sm_deposit` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'depôt 1', NULL, NULL, NULL, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_expense`
--

CREATE TABLE `sm_expense` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `AMOUNT` double DEFAULT NULL,
  `EXPENSE_TYPE_ID` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_expense`
--

INSERT INTO `sm_expense` (`ID`, `NAME`, `DESCRIPTION`, `AMOUNT`, `EXPENSE_TYPE_ID`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(22, 'Café d''Abdessamad', 'lors la réunion de produit My Stock', 7.5, 1, 1, NULL, 'Y', NULL, 1, NULL, NULL),
(23, 'Libelle1', 'Description1', 11, 1, 2, NULL, 'Y', NULL, 1, NULL, NULL),
(24, 'Libelle2', 'Description2', 345, 64, 1, NULL, 'Y', NULL, 1, NULL, NULL),
(25, 'lliil', '-', 120, 63, 1, NULL, 'Y', NULL, 1, NULL, NULL),
(26, 'dsq', 'dqsd', 33, 1, 1, NULL, 'Y', NULL, 1, NULL, NULL),
(27, '12', '12', 12, 1, 1, NULL, 'Y', NULL, 1, NULL, NULL),
(28, '', '', 200, 62, 1, NULL, 'Y', NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_expense_type`
--

CREATE TABLE `sm_expense_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_expense_type`
--

INSERT INTO `sm_expense_type` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Générale', 'Générale', 1, 1, 'Y', NULL, NULL, NULL, NULL),
(62, 'Femme de menage', 'Femme de menage', 2, 1, 'Y', NULL, NULL, NULL, NULL),
(63, 'Transport', 'Transport', 3, 1, 'Y', NULL, NULL, NULL, NULL),
(64, 'Eléctrisité', 'Eléctrisité', 4, 1, 'Y', NULL, NULL, NULL, NULL),
(65, 'Restaurant', 'Restaurant', 5, 1, 'Y', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_order`
--

CREATE TABLE `sm_order` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `ORDER_STATUS_ID` bigint(20) DEFAULT NULL,
  `PAYMENT_METHOD_ID` bigint(20) DEFAULT NULL,
  `CUSTOMER_ID` bigint(20) DEFAULT NULL,
  `CHECK_ID` bigint(20) DEFAULT NULL,
  `VALID` char(1) CHARACTER SET latin1 DEFAULT NULL,
  `OLD_ORDER_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_order`
--

INSERT INTO `sm_order` (`ID`, `NAME`, `DESCRIPTION`, `ORDER_STATUS_ID`, `PAYMENT_METHOD_ID`, `CUSTOMER_ID`, `CHECK_ID`, `VALID`, `OLD_ORDER_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `CLT_MODULE_ID`) VALUES
(26, NULL, 'note7', NULL, NULL, 5, NULL, 'Y', NULL, NULL, '2015-02-20 13:17:10', 1, NULL, NULL, 1),
(27, NULL, '12', NULL, NULL, 5, NULL, 'Y', NULL, NULL, '2015-02-20 13:17:10', 1, NULL, NULL, 1),
(28, NULL, 'ref', NULL, NULL, 4, NULL, 'Y', NULL, NULL, '2015-02-20 13:17:10', 1, NULL, NULL, 1),
(29, NULL, 'r2', NULL, NULL, 5, NULL, 'Y', NULL, NULL, '2015-02-20 13:17:10', 1, NULL, NULL, 1),
(30, NULL, '..', NULL, NULL, 6, NULL, 'Y', NULL, NULL, '2015-02-20 13:17:10', 1, NULL, 1, 1),
(31, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '2015-02-20 13:17:10', NULL, NULL, NULL, 1),
(33, NULL, 're', NULL, NULL, 4, NULL, 'Y', NULL, NULL, NULL, 1, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `sm_order_line`
--

CREATE TABLE `sm_order_line` (
`ID` bigint(20) NOT NULL,
  `DESCRIPTION` tinytext,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `DESIGNATION` text,
  `ORDER_ID` bigint(20) DEFAULT NULL,
  `PROMOTION_ID` bigint(20) DEFAULT NULL,
  `NEGOTIATE_PRICE` double DEFAULT NULL,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `REFERENCE` tinytext,
  `PRODUCT_SIZE_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_COLOR_ID` bigint(20) DEFAULT NULL,
  `UNIT_PRICE_BUY` double DEFAULT NULL,
  `TOTAL_PRICE_BUY` double DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_order_line`
--

INSERT INTO `sm_order_line` (`ID`, `DESCRIPTION`, `PRODUCT_ID`, `DESIGNATION`, `ORDER_ID`, `PROMOTION_ID`, `NEGOTIATE_PRICE`, `QUANTITY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `REFERENCE`, `PRODUCT_SIZE_ID`, `PRODUCT_COLOR_ID`, `UNIT_PRICE_BUY`, `TOTAL_PRICE_BUY`) VALUES
(43, NULL, 60, NULL, 26, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 'r1', 48, 1, 300, 300),
(44, NULL, 61, NULL, 27, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 'r1', 49, 1, 300, 300),
(45, NULL, 59, NULL, 28, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 'r1', 47, 1, 300, 300),
(46, NULL, 60, NULL, 29, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 'r1', 48, 1, 300, 300),
(47, NULL, 68, NULL, 30, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, '6039', 48, 1, 5000, 5000),
(48, NULL, 68, NULL, 30, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, '6039', 48, 1, 150, 300),
(50, NULL, 59, NULL, 33, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, 'r1', 47, 1, 100, 200);

-- --------------------------------------------------------

--
-- Structure de la table `sm_order_status`
--

CREATE TABLE `sm_order_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_payment_method`
--

CREATE TABLE `sm_payment_method` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_product`
--

CREATE TABLE `sm_product` (
`ID` bigint(20) NOT NULL,
  `DESIGNATION` tinytext CHARACTER SET latin1,
  `PRODUCT_STATUS_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_FAMILY_ID` bigint(20) DEFAULT NULL,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `REFERENCE` tinytext CHARACTER SET latin1,
  `PRODUCT_SIZE_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_COLOR_ID` bigint(20) DEFAULT NULL,
  `PRICE_SALE` double DEFAULT NULL,
  `PRICE_BUY` double DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_product`
--

INSERT INTO `sm_product` (`ID`, `DESIGNATION`, `PRODUCT_STATUS_ID`, `PRODUCT_FAMILY_ID`, `QUANTITY`, `REFERENCE`, `PRODUCT_SIZE_ID`, `PRODUCT_COLOR_ID`, `PRICE_SALE`, `PRICE_BUY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `CLT_MODULE_ID`) VALUES
(59, NULL, NULL, NULL, 0, 'r1', 47, 1, NULL, 300, 'Y', NULL, 1, NULL, NULL, 1),
(60, NULL, NULL, NULL, 2, 'r1', 48, 1, NULL, 300, 'Y', NULL, 1, NULL, NULL, 1),
(61, NULL, NULL, NULL, 2, 'r1', 49, 1, NULL, 300, 'Y', NULL, 1, NULL, NULL, 1),
(62, NULL, NULL, NULL, 4, 'r2', 47, 1, NULL, 678, 'Y', NULL, 1, NULL, NULL, 1),
(63, NULL, NULL, NULL, 5, 'r2', 50, 1, NULL, 678, 'Y', NULL, 1, NULL, NULL, 1),
(64, NULL, NULL, NULL, 12, 'r1', 51, 1, 234, 234, 'Y', NULL, 1, NULL, NULL, 1),
(65, NULL, NULL, NULL, 12, 'r1', 51, 1, 14, 14, 'Y', NULL, 1, NULL, NULL, 1),
(66, NULL, NULL, NULL, 1, '6039', 52, 1, 170, 170, 'Y', NULL, 1, NULL, NULL, 1),
(67, NULL, NULL, NULL, 2, '6039', 47, 1, 170, 170, 'Y', NULL, 1, NULL, NULL, 1),
(68, NULL, NULL, NULL, -1, '6039', 48, 1, 170, 170, 'Y', NULL, 1, NULL, NULL, 1),
(69, NULL, NULL, NULL, 1, '6039', 49, 1, 170, 170, 'Y', NULL, 1, NULL, NULL, 1),
(70, NULL, NULL, NULL, 1, '6039', 50, 1, 170, 170, 'Y', NULL, 1, NULL, NULL, 1),
(71, NULL, NULL, NULL, 1, '6039', 53, 1, 170, 170, 'Y', NULL, 1, NULL, NULL, 1),
(72, NULL, NULL, NULL, 1, '169A', 52, 1, 299, 299, 'Y', NULL, 1, NULL, NULL, 1),
(73, NULL, NULL, NULL, 1, '169A', 47, 1, 299, 299, 'Y', NULL, 1, NULL, NULL, 1),
(74, NULL, NULL, NULL, 1, '169A', 48, 1, 299, 299, 'Y', NULL, 1, NULL, NULL, 1),
(75, NULL, NULL, NULL, 1, '169A', 49, 1, 299, 299, 'Y', NULL, 1, NULL, NULL, 1),
(76, NULL, NULL, NULL, 1, '169A', 50, 1, 299, 299, 'Y', NULL, 1, NULL, NULL, 1),
(77, NULL, NULL, NULL, 1, '169A', 53, 1, 299, 299, 'Y', NULL, 1, NULL, NULL, 1),
(78, NULL, NULL, NULL, 1, 'r2', 54, 3, 1, 1, 'Y', NULL, 1, NULL, 1, 1),
(79, NULL, NULL, NULL, 2, 'r2', 55, 3, 1, 1, 'Y', NULL, 1, NULL, 1, 1),
(80, NULL, NULL, NULL, 3, 'r2', 56, 3, 1, 1, 'Y', NULL, 1, NULL, 1, 1),
(81, NULL, NULL, NULL, 4, 'r2', 56, 3, 1, 1, 'Y', NULL, 1, NULL, 1, 1),
(82, NULL, NULL, NULL, 4, 'r2', 57, 3, 1, 1, 'Y', NULL, 1, NULL, 1, 1),
(83, NULL, NULL, NULL, 4, 'r2', 57, 3, 1, 1, 'Y', NULL, 1, NULL, 1, 1),
(84, NULL, NULL, NULL, 21, 'R12', 54, 3, 1, 1, 'Y', NULL, 1, NULL, 1, 1),
(85, NULL, NULL, NULL, 1, 'R12', 55, 3, 1, 1, 'Y', NULL, 1, NULL, 1, 1),
(86, NULL, NULL, NULL, 1, 'R12', 56, 3, 1, 1, 'Y', NULL, 1, NULL, 1, 1),
(87, NULL, NULL, NULL, 1, 'R12', 56, 3, 1, 1, 'Y', NULL, 1, NULL, 1, 1),
(88, NULL, NULL, NULL, 1, 'R12', 57, 3, 1, 1, 'Y', NULL, 1, NULL, 1, 1),
(89, NULL, NULL, NULL, 1, 'R12', 57, 3, 1, 1, 'Y', NULL, 1, NULL, 1, 1),
(90, NULL, NULL, NULL, 1, 'r', 54, 1, 22, 22, 'Y', NULL, 1, NULL, 1, 1),
(91, NULL, NULL, NULL, 1, 'r', 55, 1, 22, 22, 'Y', NULL, 1, NULL, 1, 1),
(92, NULL, NULL, NULL, 1, 'r', 56, 1, 22, 22, 'Y', NULL, 1, NULL, 1, 1),
(93, NULL, NULL, NULL, 1, 'r', 56, 1, 22, 22, 'Y', NULL, 1, NULL, 1, 1),
(94, NULL, NULL, NULL, 1, 'r', 57, 1, 22, 22, 'Y', NULL, 1, NULL, 1, 1),
(95, NULL, NULL, NULL, 11, 'r', 57, 1, 22, 22, 'Y', NULL, 1, NULL, 1, 1),
(96, NULL, NULL, NULL, 3, '666-603', 58, 2, 55, 55, 'Y', NULL, 1, NULL, 1, 1),
(97, NULL, NULL, NULL, 7, '666-603', 59, 2, 55, 55, 'Y', NULL, 1, NULL, 1, 1),
(98, NULL, NULL, NULL, 0, 'rrrrr1', 54, NULL, 0, 0, 'Y', NULL, 1, NULL, NULL, 1),
(99, NULL, NULL, NULL, 0, 'rrrrr1', 55, NULL, 0, 0, 'Y', NULL, 1, NULL, NULL, 1),
(100, NULL, NULL, NULL, 0, 'rrrrr1', 56, NULL, 0, 0, 'Y', NULL, 1, NULL, NULL, 1),
(101, NULL, NULL, NULL, 0, 'rrrrr1', 56, NULL, 0, 0, 'Y', NULL, 1, NULL, NULL, 1),
(102, NULL, NULL, NULL, 0, 'rrrrr1', 57, NULL, 0, 0, 'Y', NULL, 1, NULL, NULL, 1),
(103, NULL, NULL, NULL, 0, 'rrrrr1', 57, NULL, 0, 0, 'Y', NULL, 1, NULL, NULL, 1),
(104, NULL, NULL, NULL, 1, '11', 54, 1, 1, 1, 'Y', NULL, 1, NULL, 1, 1),
(105, NULL, NULL, NULL, 2, '11', 55, 1, 1, 1, 'Y', NULL, 1, NULL, 1, 1),
(106, NULL, NULL, NULL, 1, '11', 56, 1, 1, 1, 'Y', NULL, 1, NULL, 1, 1),
(107, NULL, NULL, NULL, 0, '11', 56, 1, 1, 1, 'Y', NULL, 1, NULL, 1, 1),
(108, NULL, NULL, NULL, 0, '11', 57, 1, 1, 1, 'Y', NULL, 1, NULL, 1, 1),
(109, NULL, NULL, NULL, 0, '11', 57, 1, 1, 1, 'Y', NULL, 1, NULL, 1, 1),
(110, NULL, NULL, NULL, 1, 'kk', 54, 1, 22, 22, 'Y', NULL, 1, NULL, 1, 1),
(111, NULL, NULL, NULL, 1, 'kk', 55, 1, 22, 22, 'Y', NULL, 1, NULL, 1, 1),
(112, NULL, NULL, NULL, 1, 'kk', 56, 1, 22, 22, 'Y', NULL, 1, NULL, 1, 1),
(113, NULL, NULL, NULL, 1, 'kk', 56, 1, 22, 22, 'Y', NULL, 1, NULL, 1, 1),
(114, NULL, NULL, NULL, 1, 'kk', 57, 1, 22, 22, 'Y', NULL, 1, NULL, 1, 1),
(115, NULL, NULL, NULL, 1, 'kk', 57, 1, 22, 22, 'Y', NULL, 1, NULL, 1, 1),
(116, NULL, NULL, NULL, 5, 'ref3', 60, 2, 5, 5, 'Y', NULL, 1, NULL, 1, 1),
(117, NULL, NULL, NULL, 5, 'ref3', 52, 2, 5, 5, 'Y', NULL, 1, NULL, 1, 1),
(118, NULL, NULL, NULL, 5, 'ref3', 47, 2, 5, 5, 'Y', NULL, 1, NULL, 1, 1),
(119, NULL, NULL, NULL, 3, 'ref2', 60, 1, 2, 2, 'Y', NULL, 1, NULL, 1, 1),
(120, NULL, NULL, NULL, 1, 'ref2', 52, 1, 2, 2, 'Y', NULL, 1, NULL, 1, 1),
(121, NULL, NULL, NULL, 1, 'ref2', 47, 1, 2, 2, 'Y', NULL, 1, NULL, 1, 1),
(122, NULL, NULL, NULL, 0, '', 60, NULL, 0, 0, 'Y', NULL, 1, NULL, NULL, 1),
(123, NULL, NULL, NULL, 0, '', 52, NULL, 0, 0, 'Y', NULL, 1, NULL, NULL, 1),
(124, NULL, NULL, NULL, 0, '', 47, NULL, 0, 0, 'Y', NULL, 1, NULL, NULL, 1),
(125, NULL, NULL, NULL, 23, 'r1', 47, 4, 23, 23, 'Y', NULL, 1, NULL, 1, 1),
(126, NULL, NULL, NULL, 33, 'r1', 52, 4, 23, 23, 'Y', NULL, 1, NULL, 1, 1),
(127, NULL, NULL, NULL, 4, 'r1', 60, 4, 23, 23, 'Y', NULL, 1, NULL, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `sm_product_color`
--

CREATE TABLE `sm_product_color` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `HEX` tinytext CHARACTER SET latin1,
  `RGB` tinytext CHARACTER SET latin1,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_product_color`
--

INSERT INTO `sm_product_color` (`ID`, `NAME`, `HEX`, `RGB`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `CLT_MODULE_ID`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Blanc1', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(2, 'Bleu', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(3, 'Brun', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(4, 'Gris', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(5, 'Noir', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(6, 'Or', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(7, 'Orange', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(8, 'Rose', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(9, 'Rouge', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(10, 'Vert', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(11, 'Violet', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(12, 'New Color11', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL),
(13, 'bbb', NULL, NULL, NULL, 'Y', NULL, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_product_details`
--

CREATE TABLE `sm_product_details` (
`ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `DESIGNATION` tinytext,
  `UNIT_PRICE_BUY` double DEFAULT NULL,
  `REMISE` double DEFAULT NULL,
  `QUANTITY` bigint(20) DEFAULT NULL,
  `TVA` double DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `RECEPTION_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_product_details`
--

INSERT INTO `sm_product_details` (`ID`, `PRODUCT_ID`, `DESIGNATION`, `UNIT_PRICE_BUY`, `REMISE`, `QUANTITY`, `TVA`, `ACTIVE`, `RECEPTION_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `CLT_MODULE_ID`) VALUES
(132, 59, NULL, 300, NULL, 2, NULL, 'Y', 20, NULL, NULL, NULL, NULL, 1),
(133, 60, NULL, 300, NULL, 2, NULL, 'Y', 20, NULL, NULL, NULL, NULL, 1),
(134, 61, NULL, 300, NULL, 2, NULL, 'Y', 20, NULL, NULL, NULL, NULL, 1),
(135, 62, NULL, 678, NULL, 4, NULL, 'Y', 21, NULL, NULL, NULL, NULL, 1),
(136, 63, NULL, 678, NULL, 5, NULL, 'Y', 21, NULL, NULL, NULL, NULL, 1),
(138, 65, NULL, 14, NULL, 12, NULL, 'Y', 22, NULL, NULL, NULL, NULL, 1),
(139, 66, NULL, 170, NULL, 1, NULL, 'Y', 25, NULL, NULL, NULL, NULL, 1),
(140, 67, NULL, 170, NULL, 2, NULL, 'Y', 25, NULL, NULL, NULL, NULL, 1),
(141, 68, NULL, 170, NULL, 2, NULL, 'Y', 25, NULL, NULL, NULL, NULL, 1),
(142, 69, NULL, 170, NULL, 1, NULL, 'Y', 25, NULL, NULL, NULL, NULL, 1),
(143, 70, NULL, 170, NULL, 1, NULL, 'Y', 25, NULL, NULL, NULL, NULL, 1),
(144, 71, NULL, 170, NULL, 1, NULL, 'Y', 25, NULL, NULL, NULL, NULL, 1),
(145, 72, NULL, 299, NULL, 1, NULL, 'Y', 25, NULL, NULL, NULL, NULL, 1),
(146, 73, NULL, 299, NULL, 1, NULL, 'Y', 25, NULL, NULL, NULL, NULL, 1),
(147, 74, NULL, 299, NULL, 1, NULL, 'Y', 25, NULL, NULL, NULL, NULL, 1),
(148, 75, NULL, 299, NULL, 1, NULL, 'Y', 25, NULL, NULL, NULL, NULL, 1),
(149, 76, NULL, 299, NULL, 1, NULL, 'Y', 25, NULL, NULL, NULL, NULL, 1),
(150, 77, NULL, 299, NULL, 1, NULL, 'Y', 25, NULL, NULL, NULL, NULL, 1),
(151, 78, NULL, 1, NULL, 1, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(152, 79, NULL, 1, NULL, 2, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(153, 80, NULL, 1, NULL, 3, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(154, 81, NULL, 1, NULL, 4, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(155, 82, NULL, 1, NULL, 4, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(156, 83, NULL, 1, NULL, 4, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(157, 84, NULL, 1, NULL, 21, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(158, 85, NULL, 1, NULL, 1, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(159, 86, NULL, 1, NULL, 1, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(160, 87, NULL, 1, NULL, 1, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(161, 88, NULL, 1, NULL, 1, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(162, 89, NULL, 1, NULL, 1, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(163, 90, NULL, 22, NULL, 1, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(164, 91, NULL, 22, NULL, 1, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(165, 92, NULL, 22, NULL, 1, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(166, 93, NULL, 22, NULL, 1, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(167, 94, NULL, 22, NULL, 1, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(168, 95, NULL, 22, NULL, 11, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(169, 96, NULL, 55, NULL, 3, NULL, 'Y', 26, NULL, NULL, NULL, NULL, 1),
(170, 97, NULL, 55, NULL, 7, NULL, 'Y', 26, NULL, NULL, NULL, NULL, 1),
(171, 98, NULL, 0, NULL, 0, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(172, 99, NULL, 0, NULL, 0, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(173, 100, NULL, 0, NULL, 0, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(174, 101, NULL, 0, NULL, 0, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(175, 102, NULL, 0, NULL, 0, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(176, 103, NULL, 0, NULL, 0, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(177, 104, NULL, 1, NULL, 1, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(178, 105, NULL, 1, NULL, 2, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(179, 106, NULL, 1, NULL, 1, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(180, 107, NULL, 1, NULL, 0, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(181, 108, NULL, 1, NULL, 0, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(182, 109, NULL, 1, NULL, 0, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(183, 110, NULL, 22, NULL, 1, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(184, 111, NULL, 22, NULL, 1, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(185, 112, NULL, 22, NULL, 1, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(186, 113, NULL, 22, NULL, 1, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(187, 114, NULL, 22, NULL, 1, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(188, 115, NULL, 22, NULL, 1, NULL, 'Y', 27, NULL, NULL, NULL, NULL, 1),
(189, 116, NULL, 5, NULL, 5, NULL, 'Y', 29, NULL, NULL, NULL, NULL, 1),
(190, 117, NULL, 5, NULL, 5, NULL, 'Y', 29, NULL, NULL, NULL, NULL, 1),
(191, 118, NULL, 5, NULL, 5, NULL, 'Y', 29, NULL, NULL, NULL, NULL, 1),
(192, 119, NULL, 2, NULL, 3, NULL, 'Y', 29, NULL, NULL, NULL, NULL, 1),
(193, 120, NULL, 2, NULL, 1, NULL, 'Y', 29, NULL, NULL, NULL, NULL, 1),
(194, 121, NULL, 2, NULL, 1, NULL, 'Y', 29, NULL, NULL, NULL, NULL, 1),
(195, 122, NULL, 0, NULL, 0, NULL, 'Y', 29, NULL, NULL, NULL, NULL, 1),
(196, 123, NULL, 0, NULL, 0, NULL, 'Y', 29, NULL, NULL, NULL, NULL, 1),
(197, 124, NULL, 0, NULL, 0, NULL, 'Y', 29, NULL, NULL, NULL, NULL, 1),
(198, 125, NULL, 23, NULL, 23, NULL, 'Y', 32, NULL, NULL, NULL, NULL, 1),
(199, 126, NULL, 23, NULL, 33, NULL, 'Y', 32, NULL, NULL, NULL, NULL, 1),
(200, 127, NULL, 23, NULL, 4, NULL, 'Y', 32, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Structure de la table `sm_product_family`
--

CREATE TABLE `sm_product_family` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `PRODUCT_GROUP_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_product_family`
--

INSERT INTO `sm_product_family` (`ID`, `NAME`, `DESCRIPTION`, `PRODUCT_GROUP_ID`, `SORT_KEY`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'PC', 'family 2', NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(2, 'Electro', NULL, NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(3, 'TV', NULL, NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL),
(4, 'Tools', NULL, NULL, NULL, 'Y', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_product_group`
--

CREATE TABLE `sm_product_group` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_product_size`
--

CREATE TABLE `sm_product_size` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_product_size`
--

INSERT INTO `sm_product_size` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `CLT_MODULE_ID`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(47, '37', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(48, '38', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(49, '39', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(50, '40', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(51, 'Qté', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(52, '36', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(53, '41', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(54, '67', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(55, '78', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(56, '89', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(57, '98', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(58, '17', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(59, '23', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL),
(60, '35', NULL, NULL, 1, NULL, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_product_status`
--

CREATE TABLE `sm_product_status` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_product_status`
--

INSERT INTO `sm_product_status` (`ID`, `NAME`, `DESCRIPTION`, `SORT_KEY`, `ACTIVE`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'status1', 'desc', 1, 'Y', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sm_promotion`
--

CREATE TABLE `sm_promotion` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `PROMOTION_TYPE_ID` bigint(20) DEFAULT NULL,
  `PRODUCT` bigint(20) DEFAULT NULL,
  `VALUE` double DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `END_DATE` datetime DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_promotion_type`
--

CREATE TABLE `sm_promotion_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `sm_reception`
--

CREATE TABLE `sm_reception` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `SUPPLIER_ID` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DEADLINE` datetime DEFAULT NULL,
  `SOUCHE` tinytext,
  `AMOUNT` double DEFAULT NULL,
  `DEPOSIT_ID` bigint(20) DEFAULT NULL,
  `VALID` char(1) DEFAULT 'N',
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `SIZES` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_reception`
--

INSERT INTO `sm_reception` (`ID`, `NAME`, `DESCRIPTION`, `SUPPLIER_ID`, `ACTIVE`, `DEADLINE`, `SOUCHE`, `AMOUNT`, `DEPOSIT_ID`, `VALID`, `CLT_MODULE_ID`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `SIZES`) VALUES
(18, 'R1', 'Description de R1', 1, NULL, '2015-02-20 13:17:10', '', NULL, NULL, 'Y', 1, NULL, 1, NULL, 1, '37,38,50'),
(19, 'R1', '', 1, NULL, '2015-02-20 15:25:26', '', NULL, NULL, 'Y', 1, NULL, 1, NULL, 1, ''),
(20, 'R1', '', 1, NULL, '2015-02-20 15:38:26', '', NULL, NULL, 'Y', 1, NULL, 1, NULL, 1, '37,38,39'),
(21, 'R2', '', 1, NULL, '2015-02-20 15:39:21', '', NULL, NULL, 'Y', 1, NULL, 1, NULL, 1, '37,40'),
(22, 'R1', '', 1, NULL, '2015-02-20 17:46:01', '', NULL, NULL, 'Y', 1, NULL, 1, NULL, 1, ''),
(25, 'R1', '', 15, NULL, '2015-02-21 12:04:00', '', NULL, NULL, 'Y', 1, NULL, 1, NULL, 1, '36,37,38,39,40,41'),
(26, 'R1', '', 1, NULL, NULL, '', NULL, NULL, 'N', 1, NULL, 1, NULL, NULL, '17,23'),
(27, 'R02', '', 1, NULL, NULL, '', NULL, NULL, 'N', 1, NULL, 1, NULL, 1, '67,78,89,89,98,98'),
(29, 'R11', '', 1, NULL, NULL, '', NULL, NULL, 'N', 1, NULL, 1, NULL, 1, '35,36,37'),
(30, '', '', NULL, NULL, NULL, '', NULL, NULL, 'N', 1, NULL, 1, NULL, NULL, ''),
(31, '', '', NULL, NULL, NULL, '', NULL, NULL, 'N', 1, NULL, 1, NULL, NULL, ''),
(32, 'R2', 'aaa', 1, NULL, NULL, 'S1', NULL, NULL, 'N', 1, NULL, 1, NULL, NULL, '35,36,37');

-- --------------------------------------------------------

--
-- Structure de la table `sm_supplier`
--

CREATE TABLE `sm_supplier` (
`ID` bigint(20) NOT NULL,
  `SUPPLIER_TYPE_ID` bigint(20) DEFAULT NULL,
  `FIRST_NAME` tinytext CHARACTER SET latin1,
  `LAST_NAME` tinytext CHARACTER SET latin1,
  `INF_COUNTRY_ID` bigint(20) DEFAULT NULL,
  `INF_CITY_ID` bigint(20) DEFAULT NULL,
  `COMPANY_NAME` tinytext,
  `CELL_PHONE` tinytext CHARACTER SET latin1,
  `FIXED_PHONE` tinytext CHARACTER SET latin1,
  `ADRESS` tinytext CHARACTER SET latin1,
  `EMAIL` tinytext CHARACTER SET latin1,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL,
  `POST_CODE` tinytext
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_supplier`
--

INSERT INTO `sm_supplier` (`ID`, `SUPPLIER_TYPE_ID`, `FIRST_NAME`, `LAST_NAME`, `INF_COUNTRY_ID`, `INF_CITY_ID`, `COMPANY_NAME`, `CELL_PHONE`, `FIXED_PHONE`, `ADRESS`, `EMAIL`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`, `POST_CODE`) VALUES
(1, 1, 'Abdessamad', 'HALLAL', 1, 1, 'Sybaway Inc', '+212 6 66 39 18 32', '+212 5 37 60 98 98', 'Kasser Elbher Bloc D 70', 'abdessamad.hallal@gmail.com', 1, NULL, 'Y', NULL, 1, NULL, 1, '40000'),
(15, 1, '', '', 1, 1, 'JIA DA LI SHOES', '', '-', '-', '-', 1, NULL, 'Y', NULL, 1, NULL, 1, '');

-- --------------------------------------------------------

--
-- Structure de la table `sm_supplier_type`
--

CREATE TABLE `sm_supplier_type` (
`ID` bigint(20) NOT NULL,
  `NAME` tinytext,
  `DESCRIPTION` text,
  `CLT_MODULE_ID` bigint(20) DEFAULT NULL,
  `SORT_KEY` bigint(20) DEFAULT NULL,
  `ACTIVE` char(1) CHARACTER SET latin1 DEFAULT 'Y',
  `DATE_CREATION` datetime DEFAULT NULL,
  `USER_CREATION` bigint(20) DEFAULT NULL,
  `DATE_UPDATE` datetime DEFAULT NULL,
  `USER_UPDATE` bigint(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `sm_supplier_type`
--

INSERT INTO `sm_supplier_type` (`ID`, `NAME`, `DESCRIPTION`, `CLT_MODULE_ID`, `SORT_KEY`, `ACTIVE`, `DATE_CREATION`, `USER_CREATION`, `DATE_UPDATE`, `USER_UPDATE`) VALUES
(1, 'Type par défaut12', '12', 1, 1, 'N', NULL, NULL, NULL, NULL),
(2, 'nom1', 'description1', 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(3, 'aa', 'bb', 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(4, 'tt', 'err', 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(5, 'tr', 'tr', 1, NULL, 'Y', NULL, NULL, NULL, NULL),
(6, 'type1111', 'type222', 1, NULL, 'Y', NULL, NULL, NULL, NULL);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `clt_client`
--
ALTER TABLE `clt_client`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_CLT_CLIENT_INF_COUNTRY1_idx` (`INF_COUNTRY_ID`), ADD KEY `fk_CLT_CLIENT_INF_CITY1_idx` (`INF_CITY_ID`), ADD KEY `fk_CLT_CLIENT_CLT_CLIENT_STATUS1_idx` (`CLIENT_STATUS_ID`);

--
-- Index pour la table `clt_client_language`
--
ALTER TABLE `clt_client_language`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_CLT_CLIENT_LANGUAGE_CLT_CLIENT1_idx` (`CLIENT_ID`), ADD KEY `fk_CLT_CLIENT_LANGUAGE_INF_LANGUAGE1_idx` (`LANGUAGE_ID`), ADD KEY `fk_CLT_CLIENT_LANGUAGE_INF_PREFIX_ID` (`INF_PREFIX_ID`);

--
-- Index pour la table `clt_client_status`
--
ALTER TABLE `clt_client_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `clt_module`
--
ALTER TABLE `clt_module`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_CLT_FOLDER_CLT_CLIENT1_idx` (`CLIENT_ID`), ADD KEY `fk_CLT_MODULE_CLT_MODULE_STATUS1_idx` (`MODULE_STATUS_ID`);

--
-- Index pour la table `clt_module_parameter`
--
ALTER TABLE `clt_module_parameter`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `clt_module_parameter_client`
--
ALTER TABLE `clt_module_parameter_client`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `clt_module_parameter_type`
--
ALTER TABLE `clt_module_parameter_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `clt_module_status`
--
ALTER TABLE `clt_module_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `clt_parameter`
--
ALTER TABLE `clt_parameter`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_CLT_PARAMETER_CLT_TYPE_PARAMETER1_idx` (`PARAMETER_TYPE_ID`);

--
-- Index pour la table `clt_parameter_client`
--
ALTER TABLE `clt_parameter_client`
 ADD PRIMARY KEY (`ID`), ADD KEY `idx_clt_parameter_client` (`PARAMETER_ID`), ADD KEY `idx_clt_parameter_client_0` (`CLINET_ID`);

--
-- Index pour la table `clt_parameter_type`
--
ALTER TABLE `clt_parameter_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `clt_user`
--
ALTER TABLE `clt_user`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_CLT_USER_CLT_CLIENT1_idx` (`CLIENT_ID`), ADD KEY `fk_CLT_USER_CLT_CATEGORY_USER1_idx` (`CATEGORY_ID`), ADD KEY `fk_CLT_USER_INF_COUNTRY1_idx` (`INF_COUNTRY_ID`), ADD KEY `fk_CLT_USER_INF_CITY1_idx` (`INF_CITY_ID`), ADD KEY `idx_clt_user` (`USER_STATUS_ID`);

--
-- Index pour la table `clt_user_category`
--
ALTER TABLE `clt_user_category`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_CLT_USER_CATEGORY_CLT_MODULE1_idx` (`MODULE_ID`);

--
-- Index pour la table `clt_user_group`
--
ALTER TABLE `clt_user_group`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_CLT_USER_GROUP_CLT_USER1_idx` (`USER_ID`), ADD KEY `fk_CLT_USER_GROUP_INF_GROUP1_idx` (`INF_GROUP_ID`);

--
-- Index pour la table `clt_user_module`
--
ALTER TABLE `clt_user_module`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_CLT_USER_FOLDER_CLT_USER1_idx` (`USER_ID`), ADD KEY `fk_CLT_USER_FOLDER_CLT_FOLDER1_idx` (`MODULE_ID`);

--
-- Index pour la table `clt_user_status`
--
ALTER TABLE `clt_user_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_basic_parameter`
--
ALTER TABLE `inf_basic_parameter`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_basic_parameter_type`
--
ALTER TABLE `inf_basic_parameter_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_city`
--
ALTER TABLE `inf_city`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_INF_CITY_INF_COUNTRY1_idx` (`COUNTRY_ID`);

--
-- Index pour la table `inf_country`
--
ALTER TABLE `inf_country`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_group`
--
ALTER TABLE `inf_group`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_INF_GROUP_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `inf_item`
--
ALTER TABLE `inf_item`
 ADD PRIMARY KEY (`CODE`);

--
-- Index pour la table `inf_language`
--
ALTER TABLE `inf_language`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_lovs`
--
ALTER TABLE `inf_lovs`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_prefix`
--
ALTER TABLE `inf_prefix`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_privilege`
--
ALTER TABLE `inf_privilege`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_INF_PRIVILEGE_INF_ITEM1_idx` (`ITEM_CODE`), ADD KEY `fk_INF_PRIVILEGE_INF_ROLE1_idx` (`ROLE_ID`);

--
-- Index pour la table `inf_role`
--
ALTER TABLE `inf_role`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `inf_role_group`
--
ALTER TABLE `inf_role_group`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_INF_ROLE_GROUP_INF_GROUP_idx` (`GROUP_ID`), ADD KEY `fk_INF_ROLE_GROUP_INF_ROLE1_idx` (`ROLE_ID`);

--
-- Index pour la table `inf_text`
--
ALTER TABLE `inf_text`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_INF_TEXT_INF_PREFIX1_idx` (`PREFIX`), ADD KEY `fk_inf_text` (`ITEM_CODE`);

--
-- Index pour la table `inf_text_type`
--
ALTER TABLE `inf_text_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_attribute_exclud`
--
ALTER TABLE `pm_attribute_exclud`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_COMPOS_EXECLUD_PM_PAGE_ITEM1_idx` (`PAGE_ID`);

--
-- Index pour la table `pm_attribute_validation`
--
ALTER TABLE `pm_attribute_validation`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_VALID_SIMPLE_PAGE_PM_VALID_SIMPLE1_idx` (`VALIDATION_ID`), ADD KEY `fk_PM_VALID_SIMPLE_PAGE_PM_PAGE1_idx` (`PAGE_ID`);

--
-- Index pour la table `pm_category`
--
ALTER TABLE `pm_category`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_MENU_PM_MENU_TYPE1_idx` (`CATEGORY_TYPE_ID`);

--
-- Index pour la table `pm_category_type`
--
ALTER TABLE `pm_category_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_component`
--
ALTER TABLE `pm_component`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_composition`
--
ALTER TABLE `pm_composition`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_COMPOSITION_PM_MENU1_idx` (`MENU_ID`), ADD KEY `fk_PM_COMPOSITION_PM_PAGE1_idx` (`PAGE_ID`), ADD KEY `fk_PM_COMPOSITION_CLT_FOLDER1_idx` (`CLT_MODULE_ID`), ADD KEY `FK2` (`GROUP_ID`), ADD KEY `FK3` (`CATEGORY_ID`);

--
-- Index pour la table `pm_group`
--
ALTER TABLE `pm_group`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_GROUP_PM_GROUP_TYPE1_idx` (`GROUP_TYPE_ID`);

--
-- Index pour la table `pm_group_type`
--
ALTER TABLE `pm_group_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_menu`
--
ALTER TABLE `pm_menu`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_SECTION_PM_SECTION_TYPE1_idx` (`MENU_TYPE_ID`);

--
-- Index pour la table `pm_menu_type`
--
ALTER TABLE `pm_menu_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_page`
--
ALTER TABLE `pm_page`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_PAGE_PM_TYPE_PAGE1_idx` (`PAGE_TYPE_ID`);

--
-- Index pour la table `pm_page_attribute`
--
ALTER TABLE `pm_page_attribute`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_PM_PAGE_ITEM_PM_PAGE1_idx` (`PAGE_ID`), ADD KEY `fk_PM_PAGE_ITEM_INF_ITEM1_idx` (`INF_ITEM_CODE`), ADD KEY `fk_PM_PAGE_ITEM_PM_COMPONENTE1_idx` (`PM_COMPONENT_ID`);

--
-- Index pour la table `pm_page_parameter`
--
ALTER TABLE `pm_page_parameter`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_page_parameter_module`
--
ALTER TABLE `pm_page_parameter_module`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_page_parameter_type`
--
ALTER TABLE `pm_page_parameter_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_page_type`
--
ALTER TABLE `pm_page_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `pm_validation_type`
--
ALTER TABLE `pm_validation_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_advanced`
--
ALTER TABLE `sm_advanced`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_ADVANCED_SM_ADVANCED_STATUS1_idx` (`ADVANCED_STATUS_ID`), ADD KEY `fk_SM_ADVANCED_SM_CUSTOMER1_idx` (`CUSTOMER_ID`), ADD KEY `fk_SM_ADVANCED_SM_PRODUCT1_idx` (`PRODUCT_ID`);

--
-- Index pour la table `sm_advanced_status`
--
ALTER TABLE `sm_advanced_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_bank_type`
--
ALTER TABLE `sm_bank_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_check`
--
ALTER TABLE `sm_check`
 ADD PRIMARY KEY (`ID`), ADD UNIQUE KEY `pk_sm_check_0` (`BANK_ID`);

--
-- Index pour la table `sm_customer`
--
ALTER TABLE `sm_customer`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_CUSTOMER_CLT_MODULE1_idx` (`CLT_MODULE_ID`), ADD KEY `idx_sm_customer` (`CUSTOMER_TYPE_ID`);

--
-- Index pour la table `sm_customer_type`
--
ALTER TABLE `sm_customer_type`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_CUSTOMER_TYPE_CLT_FOLDER1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `sm_deposit`
--
ALTER TABLE `sm_deposit`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_expense`
--
ALTER TABLE `sm_expense`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_EXPENSE_SM_EXPENSE_TYPE1_idx` (`EXPENSE_TYPE_ID`), ADD KEY `fk_SM_EXPENSE_CLT_FOLDER1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `sm_expense_type`
--
ALTER TABLE `sm_expense_type`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_order`
--
ALTER TABLE `sm_order`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_ORDER_SM_ORDER_STATUS1_idx` (`ORDER_STATUS_ID`), ADD KEY `fk_SM_ORDER_SM_PAYMENT_METHOD1_idx` (`PAYMENT_METHOD_ID`), ADD KEY `fk_SM_ORDER_SM_CUSTOMER1_idx` (`CUSTOMER_ID`), ADD KEY `fk_SM_ORDER_SM_CHECK1_idx` (`CHECK_ID`);

--
-- Index pour la table `sm_order_line`
--
ALTER TABLE `sm_order_line`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_ORDER_LINE_SM_PRODUCT1_idx` (`PRODUCT_ID`), ADD KEY `fk_SM_ORDER_LINE_SM_ORDER1_idx` (`ORDER_ID`);

--
-- Index pour la table `sm_order_status`
--
ALTER TABLE `sm_order_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_payment_method`
--
ALTER TABLE `sm_payment_method`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_PAYMENT_METHOD_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `sm_product`
--
ALTER TABLE `sm_product`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_PRODUCT_SM_PRODUCT_STATUS1_idx` (`PRODUCT_STATUS_ID`), ADD KEY `fk_SM_PRODUCT_SM_PRODUCT_FAMILY1_idx` (`PRODUCT_FAMILY_ID`), ADD KEY `fk_SM_PRODUCT_SM_PRODUCT_SIZE1_idx` (`PRODUCT_SIZE_ID`), ADD KEY `fk_SM_PRODUCT_SM_PRODUCT_COLOR1_idx` (`PRODUCT_COLOR_ID`);

--
-- Index pour la table `sm_product_color`
--
ALTER TABLE `sm_product_color`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_PRODUCT_COLOR_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `sm_product_details`
--
ALTER TABLE `sm_product_details`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_PRODUCT_DETAILS_SM_RECEPTION1_idx` (`RECEPTION_ID`), ADD KEY `fk_SM_PRODUCT_DETAILS_SM_PRODUCT1_idx` (`PRODUCT_ID`);

--
-- Index pour la table `sm_product_family`
--
ALTER TABLE `sm_product_family`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_PRODUCT_FAMILY_SM_PRODUCT_GROUP1_idx` (`PRODUCT_GROUP_ID`), ADD KEY `fk_SM_PRODUCT_FAMILY_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `sm_product_group`
--
ALTER TABLE `sm_product_group`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_PRODUCT_GROUP_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `sm_product_size`
--
ALTER TABLE `sm_product_size`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_product_status`
--
ALTER TABLE `sm_product_status`
 ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `sm_promotion`
--
ALTER TABLE `sm_promotion`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_PROMOTION_SM_PROMOTION_TYPE1_idx` (`PROMOTION_TYPE_ID`), ADD KEY `fk_SM_PROMOTION_SM_PRODUCT1_idx` (`PRODUCT`);

--
-- Index pour la table `sm_promotion_type`
--
ALTER TABLE `sm_promotion_type`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_PROMOTION_TYPE_CLT_MODULE1_idx` (`CLT_MODULE_ID`);

--
-- Index pour la table `sm_reception`
--
ALTER TABLE `sm_reception`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_RECEPTION_SM_SUPPLIER1_idx` (`SUPPLIER_ID`);

--
-- Index pour la table `sm_supplier`
--
ALTER TABLE `sm_supplier`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_SUPPLIER_SM_SUPPLIER_TYPE1_idx` (`SUPPLIER_TYPE_ID`);

--
-- Index pour la table `sm_supplier_type`
--
ALTER TABLE `sm_supplier_type`
 ADD PRIMARY KEY (`ID`), ADD KEY `fk_SM_SUPPLIER_TYPE_CLT_FOLDER1_idx` (`CLT_MODULE_ID`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `clt_client`
--
ALTER TABLE `clt_client`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `clt_client_language`
--
ALTER TABLE `clt_client_language`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `clt_client_status`
--
ALTER TABLE `clt_client_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `clt_module`
--
ALTER TABLE `clt_module`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `clt_module_parameter`
--
ALTER TABLE `clt_module_parameter`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `clt_module_parameter_client`
--
ALTER TABLE `clt_module_parameter_client`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `clt_module_status`
--
ALTER TABLE `clt_module_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `clt_parameter`
--
ALTER TABLE `clt_parameter`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `clt_parameter_client`
--
ALTER TABLE `clt_parameter_client`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `clt_user`
--
ALTER TABLE `clt_user`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `clt_user_category`
--
ALTER TABLE `clt_user_category`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `clt_user_group`
--
ALTER TABLE `clt_user_group`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `clt_user_module`
--
ALTER TABLE `clt_user_module`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `clt_user_status`
--
ALTER TABLE `clt_user_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `inf_basic_parameter`
--
ALTER TABLE `inf_basic_parameter`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `inf_basic_parameter_type`
--
ALTER TABLE `inf_basic_parameter_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `inf_city`
--
ALTER TABLE `inf_city`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `inf_country`
--
ALTER TABLE `inf_country`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `inf_group`
--
ALTER TABLE `inf_group`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `inf_language`
--
ALTER TABLE `inf_language`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `inf_lovs`
--
ALTER TABLE `inf_lovs`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `inf_prefix`
--
ALTER TABLE `inf_prefix`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `inf_privilege`
--
ALTER TABLE `inf_privilege`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `inf_role`
--
ALTER TABLE `inf_role`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `inf_role_group`
--
ALTER TABLE `inf_role_group`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `inf_text`
--
ALTER TABLE `inf_text`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=402;
--
-- AUTO_INCREMENT pour la table `inf_text_type`
--
ALTER TABLE `inf_text_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `pm_attribute_exclud`
--
ALTER TABLE `pm_attribute_exclud`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `pm_attribute_validation`
--
ALTER TABLE `pm_attribute_validation`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=286;
--
-- AUTO_INCREMENT pour la table `pm_category`
--
ALTER TABLE `pm_category`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `pm_category_type`
--
ALTER TABLE `pm_category_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `pm_component`
--
ALTER TABLE `pm_component`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `pm_composition`
--
ALTER TABLE `pm_composition`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=188;
--
-- AUTO_INCREMENT pour la table `pm_group`
--
ALTER TABLE `pm_group`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `pm_group_type`
--
ALTER TABLE `pm_group_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `pm_menu`
--
ALTER TABLE `pm_menu`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT pour la table `pm_menu_type`
--
ALTER TABLE `pm_menu_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `pm_page`
--
ALTER TABLE `pm_page`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT pour la table `pm_page_attribute`
--
ALTER TABLE `pm_page_attribute`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT pour la table `pm_page_parameter`
--
ALTER TABLE `pm_page_parameter`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `pm_page_parameter_module`
--
ALTER TABLE `pm_page_parameter_module`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `pm_page_type`
--
ALTER TABLE `pm_page_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `pm_validation_type`
--
ALTER TABLE `pm_validation_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `sm_advanced`
--
ALTER TABLE `sm_advanced`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_advanced_status`
--
ALTER TABLE `sm_advanced_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_bank_type`
--
ALTER TABLE `sm_bank_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_check`
--
ALTER TABLE `sm_check`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_customer`
--
ALTER TABLE `sm_customer`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `sm_customer_type`
--
ALTER TABLE `sm_customer_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `sm_deposit`
--
ALTER TABLE `sm_deposit`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `sm_expense`
--
ALTER TABLE `sm_expense`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT pour la table `sm_expense_type`
--
ALTER TABLE `sm_expense_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT pour la table `sm_order`
--
ALTER TABLE `sm_order`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT pour la table `sm_order_line`
--
ALTER TABLE `sm_order_line`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT pour la table `sm_order_status`
--
ALTER TABLE `sm_order_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_payment_method`
--
ALTER TABLE `sm_payment_method`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_product`
--
ALTER TABLE `sm_product`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=128;
--
-- AUTO_INCREMENT pour la table `sm_product_color`
--
ALTER TABLE `sm_product_color`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `sm_product_details`
--
ALTER TABLE `sm_product_details`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=201;
--
-- AUTO_INCREMENT pour la table `sm_product_family`
--
ALTER TABLE `sm_product_family`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `sm_product_group`
--
ALTER TABLE `sm_product_group`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_product_size`
--
ALTER TABLE `sm_product_size`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT pour la table `sm_product_status`
--
ALTER TABLE `sm_product_status`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `sm_promotion`
--
ALTER TABLE `sm_promotion`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_promotion_type`
--
ALTER TABLE `sm_promotion_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `sm_reception`
--
ALTER TABLE `sm_reception`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT pour la table `sm_supplier`
--
ALTER TABLE `sm_supplier`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `sm_supplier_type`
--
ALTER TABLE `sm_supplier_type`
MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `clt_client`
--
ALTER TABLE `clt_client`
ADD CONSTRAINT `fk_CLT_CLIENT_CLT_CLIENT_STATUS1` FOREIGN KEY (`CLIENT_STATUS_ID`) REFERENCES `clt_client_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_CLIENT_INF_CITY1` FOREIGN KEY (`INF_CITY_ID`) REFERENCES `inf_city` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_CLIENT_INF_COUNTRY1` FOREIGN KEY (`INF_COUNTRY_ID`) REFERENCES `inf_country` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `clt_client_language`
--
ALTER TABLE `clt_client_language`
ADD CONSTRAINT `fk_CLT_CLIENT_LANGUAGE_CLT_CLIENT1` FOREIGN KEY (`CLIENT_ID`) REFERENCES `clt_client` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_CLIENT_LANGUAGE_INF_PREFIX_ID` FOREIGN KEY (`INF_PREFIX_ID`) REFERENCES `inf_prefix` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_CLIENT_LANGUAGE_LANGUAGE_ID` FOREIGN KEY (`LANGUAGE_ID`) REFERENCES `inf_language` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `clt_module`
--
ALTER TABLE `clt_module`
ADD CONSTRAINT `fk_CLT_MODULE_CLT_CLIENT1` FOREIGN KEY (`CLIENT_ID`) REFERENCES `clt_client` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_MODULE_CLT_MODULE_STATUS1` FOREIGN KEY (`MODULE_STATUS_ID`) REFERENCES `clt_module_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `clt_parameter`
--
ALTER TABLE `clt_parameter`
ADD CONSTRAINT `fk_clt_parameter` FOREIGN KEY (`PARAMETER_TYPE_ID`) REFERENCES `clt_parameter_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `clt_parameter_client`
--
ALTER TABLE `clt_parameter_client`
ADD CONSTRAINT `fk_clt_parameter_client` FOREIGN KEY (`PARAMETER_ID`) REFERENCES `clt_parameter` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_clt_parameter_client_0` FOREIGN KEY (`CLINET_ID`) REFERENCES `clt_client` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `clt_user`
--
ALTER TABLE `clt_user`
ADD CONSTRAINT `fk_clt_user` FOREIGN KEY (`USER_STATUS_ID`) REFERENCES `clt_user_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_USER_CLT_CATEGORY_USER1` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `clt_user_category` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_USER_CLT_CLIENT1` FOREIGN KEY (`CLIENT_ID`) REFERENCES `clt_client` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_USER_INF_CITY1` FOREIGN KEY (`INF_CITY_ID`) REFERENCES `inf_city` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_USER_INF_COUNTRY1` FOREIGN KEY (`INF_COUNTRY_ID`) REFERENCES `inf_country` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `clt_user_group`
--
ALTER TABLE `clt_user_group`
ADD CONSTRAINT `fk_CLT_USER_GROUP_CLT_USER1` FOREIGN KEY (`USER_ID`) REFERENCES `clt_user` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_USER_GROUP_INF_GROUP1` FOREIGN KEY (`INF_GROUP_ID`) REFERENCES `inf_group` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `clt_user_module`
--
ALTER TABLE `clt_user_module`
ADD CONSTRAINT `fk_CLT_USER_FOLDER_CLT_FOLDER1` FOREIGN KEY (`MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_CLT_USER_FOLDER_CLT_USER1` FOREIGN KEY (`USER_ID`) REFERENCES `clt_user` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `inf_city`
--
ALTER TABLE `inf_city`
ADD CONSTRAINT `fk_INF_CITY_INF_COUNTRY1` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `inf_country` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `inf_group`
--
ALTER TABLE `inf_group`
ADD CONSTRAINT `fk_INF_GROUP_CLT_MODULE1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `inf_privilege`
--
ALTER TABLE `inf_privilege`
ADD CONSTRAINT `fk_inf_privilege` FOREIGN KEY (`ITEM_CODE`) REFERENCES `inf_item` (`CODE`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_INF_PRIVILEGE_INF_ROLE1` FOREIGN KEY (`ROLE_ID`) REFERENCES `inf_role` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `inf_role_group`
--
ALTER TABLE `inf_role_group`
ADD CONSTRAINT `fk_INF_ROLE_GROUP_INF_GROUP` FOREIGN KEY (`GROUP_ID`) REFERENCES `inf_group` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_INF_ROLE_GROUP_INF_ROLE1` FOREIGN KEY (`ROLE_ID`) REFERENCES `inf_role` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_attribute_validation`
--
ALTER TABLE `pm_attribute_validation`
ADD CONSTRAINT `fk_PM_VALID_SIMPLE_PAGE_PM_PAGE1` FOREIGN KEY (`PAGE_ID`) REFERENCES `pm_page` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_category`
--
ALTER TABLE `pm_category`
ADD CONSTRAINT `fk_pm_category` FOREIGN KEY (`CATEGORY_TYPE_ID`) REFERENCES `pm_category_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_composition`
--
ALTER TABLE `pm_composition`
ADD CONSTRAINT `FK1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE SET NULL ON UPDATE SET NULL,
ADD CONSTRAINT `FK2` FOREIGN KEY (`GROUP_ID`) REFERENCES `pm_group` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK3` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `pm_category` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK4` FOREIGN KEY (`MENU_ID`) REFERENCES `pm_menu` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `FK5` FOREIGN KEY (`PAGE_ID`) REFERENCES `pm_page` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_group`
--
ALTER TABLE `pm_group`
ADD CONSTRAINT `fk_pm_group` FOREIGN KEY (`GROUP_TYPE_ID`) REFERENCES `pm_group_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_menu`
--
ALTER TABLE `pm_menu`
ADD CONSTRAINT `fk_PM_SECTION_PM_SECTION_TYPE1` FOREIGN KEY (`MENU_TYPE_ID`) REFERENCES `pm_menu_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_page`
--
ALTER TABLE `pm_page`
ADD CONSTRAINT `fk_PM_PAGE_PM_TYPE_PAGE1` FOREIGN KEY (`PAGE_TYPE_ID`) REFERENCES `pm_page_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `pm_page_attribute`
--
ALTER TABLE `pm_page_attribute`
ADD CONSTRAINT `fk_PM_PAGE_ITEM_PM_COMPONENTE1` FOREIGN KEY (`PM_COMPONENT_ID`) REFERENCES `pm_component` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_PM_PAGE_ITEM_PM_PAGE1` FOREIGN KEY (`PAGE_ID`) REFERENCES `pm_page` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_advanced`
--
ALTER TABLE `sm_advanced`
ADD CONSTRAINT `fk_SM_ADVANCED_SM_ADVANCED_STATUS1` FOREIGN KEY (`ADVANCED_STATUS_ID`) REFERENCES `sm_advanced_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_ADVANCED_SM_CUSTOMER1` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `sm_customer` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_ADVANCED_SM_PRODUCT1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sm_product` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_customer`
--
ALTER TABLE `sm_customer`
ADD CONSTRAINT `fk_SM_CUSTOMER_CLT_MODULE1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_sm_customer_sm_customer_type1` FOREIGN KEY (`CUSTOMER_TYPE_ID`) REFERENCES `sm_customer_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_customer_type`
--
ALTER TABLE `sm_customer_type`
ADD CONSTRAINT `fk_SM_CUSTOMER_TYPE_CLT_FOLDER1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_expense`
--
ALTER TABLE `sm_expense`
ADD CONSTRAINT `fk_SM_EXPENSE_CLT_FOLDER1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_EXPENSE_SM_EXPENSE_TYPE1` FOREIGN KEY (`EXPENSE_TYPE_ID`) REFERENCES `sm_expense_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_order`
--
ALTER TABLE `sm_order`
ADD CONSTRAINT `fk_SM_ORDER_SM_CHECK1` FOREIGN KEY (`CHECK_ID`) REFERENCES `sm_check` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_ORDER_SM_CUSTOMER1` FOREIGN KEY (`CUSTOMER_ID`) REFERENCES `sm_customer` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_ORDER_SM_ORDER_STATUS1` FOREIGN KEY (`ORDER_STATUS_ID`) REFERENCES `sm_order_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_ORDER_SM_PAYMENT_METHOD1` FOREIGN KEY (`PAYMENT_METHOD_ID`) REFERENCES `sm_payment_method` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_order_line`
--
ALTER TABLE `sm_order_line`
ADD CONSTRAINT `fk_SM_ORDER_LINE_SM_ORDER1` FOREIGN KEY (`ORDER_ID`) REFERENCES `sm_order` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_ORDER_LINE_SM_PRODUCT1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sm_product` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_payment_method`
--
ALTER TABLE `sm_payment_method`
ADD CONSTRAINT `fk_SM_PAYMENT_METHOD_CLT_MODULE1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_product`
--
ALTER TABLE `sm_product`
ADD CONSTRAINT `fk_SM_PRODUCT_SM_PRODUCT_COLOR1` FOREIGN KEY (`PRODUCT_COLOR_ID`) REFERENCES `sm_product_color` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_PRODUCT_SM_PRODUCT_FAMILY1` FOREIGN KEY (`PRODUCT_FAMILY_ID`) REFERENCES `sm_product_family` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_sm_product_sm_product_size1` FOREIGN KEY (`PRODUCT_SIZE_ID`) REFERENCES `sm_product_size` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_PRODUCT_SM_PRODUCT_STATUS1` FOREIGN KEY (`PRODUCT_STATUS_ID`) REFERENCES `sm_product_status` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_product_color`
--
ALTER TABLE `sm_product_color`
ADD CONSTRAINT `fk_SM_PRODUCT_COLOR_CLT_MODULE1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_product_details`
--
ALTER TABLE `sm_product_details`
ADD CONSTRAINT `fk_SM_PRODUCT_DETAILS_SM_PRODUCT1` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `sm_product` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_PRODUCT_DETAILS_SM_RECEPTION1` FOREIGN KEY (`RECEPTION_ID`) REFERENCES `sm_reception` (`ID`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_product_family`
--
ALTER TABLE `sm_product_family`
ADD CONSTRAINT `fk_SM_PRODUCT_FAMILY_CLT_MODULE1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_PRODUCT_FAMILY_SM_PRODUCT_GROUP1` FOREIGN KEY (`PRODUCT_GROUP_ID`) REFERENCES `sm_product_group` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_product_group`
--
ALTER TABLE `sm_product_group`
ADD CONSTRAINT `fk_SM_PRODUCT_GROUP_CLT_MODULE1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_promotion`
--
ALTER TABLE `sm_promotion`
ADD CONSTRAINT `fk_SM_PROMOTION_SM_PRODUCT1` FOREIGN KEY (`PRODUCT`) REFERENCES `sm_product` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_SM_PROMOTION_SM_PROMOTION_TYPE1` FOREIGN KEY (`PROMOTION_TYPE_ID`) REFERENCES `sm_promotion_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_promotion_type`
--
ALTER TABLE `sm_promotion_type`
ADD CONSTRAINT `fk_SM_PROMOTION_TYPE_CLT_MODULE1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_reception`
--
ALTER TABLE `sm_reception`
ADD CONSTRAINT `fk_SM_RECEPTION_SM_SUPPLIER1` FOREIGN KEY (`SUPPLIER_ID`) REFERENCES `sm_supplier` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_supplier`
--
ALTER TABLE `sm_supplier`
ADD CONSTRAINT `fk_SM_SUPPLIER_SM_SUPPLIER_TYPE1` FOREIGN KEY (`SUPPLIER_TYPE_ID`) REFERENCES `sm_supplier_type` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sm_supplier_type`
--
ALTER TABLE `sm_supplier_type`
ADD CONSTRAINT `fk_SM_SUPPLIER_TYPE_CLT_FOLDER1` FOREIGN KEY (`CLT_MODULE_ID`) REFERENCES `clt_module` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;
