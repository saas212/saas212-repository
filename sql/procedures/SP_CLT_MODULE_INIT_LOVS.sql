
-- Important Ajouter la notition de 9999 pour initialiser les lovs par un models par defaut

DELIMITER //

DROP PROCEDURE SP_CLT_MODULE_INIT_LOVS //

CREATE  PROCEDURE SP_CLT_MODULE_INIT_LOVS(IN moduleid bigint(10))

BEGIN

INSERT INTO SM_BANK_TYPE (NAME,DESCRIPTION,CLT_MODULE_ID,DATE_CREATION) 
VALUES ('BANK Default Value','BANK Description Default Value',moduleid,now());

INSERT INTO SM_CUSTOMER_TYPE (NAME,DESCRIPTION,CLT_MODULE_ID,DATE_CREATION) 
VALUES ('Client Type Default Value','Client Type Description Default Value',moduleid,now());


INSERT INTO SM_EXPENSE_TYPE (NAME,DESCRIPTION,CLT_MODULE_ID,DATE_CREATION) 
VALUES ('Expense Type Default Value','Expense Type  Description Default Value',moduleid,now());


INSERT INTO SM_PRODUCT_TYPE (NAME,DESCRIPTION,CLT_MODULE_ID,DATE_CREATION) 
VALUES ('Produit type Default Value','Produit type Description Default Value',moduleid,now());


INSERT INTO SM_PROMOTION_TYPE (NAME,DESCRIPTION,CLT_MODULE_ID,DATE_CREATION) 
VALUES ('Promotion Type Default Value','Promotion Type Description Default Value',moduleid,now());


INSERT INTO SM_SUPPLIER_TYPE (NAME,DESCRIPTION,CLT_MODULE_ID,DATE_CREATION) 
VALUES ('Fournisseur Type Default Value','Fournisseur Type  Description Default Value',moduleid,now());


INSERT INTO SM_SUPPLIER_CATEGORY (NAME,DESCRIPTION,CLT_MODULE_ID,DATE_CREATION) 
VALUES ('Fournisseur catégorie Default Value','Fournisseur catégorie Description Default Value',moduleid,now());



INSERT INTO SM_CUSTOMER_CATEGORY (NAME,DESCRIPTION,CLT_MODULE_ID,DATE_CREATION) 
VALUES ('Client catégorie Default Value','Client catégorie  Description Default Value',moduleid,now());


INSERT INTO SM_PRODUCT_FAMILY (NAME,DESCRIPTION,CLT_MODULE_ID,DATE_CREATION) 
VALUES ('Famille de produit  Default Value','Famille de produit Description Default Value',moduleid,now());


INSERT INTO SM_PRODUCT_SIZE (NAME,DESCRIPTION,CLT_MODULE_ID,DATE_CREATION) 
VALUES ('Taille de produit Default Value','taille de produit  Description Default Value',moduleid,now());


INSERT INTO SM_PRODUCT_DEPARTMENT (NAME,DESCRIPTION,CLT_MODULE_ID,DATE_CREATION) 
VALUES ('Département de produit  Default Value','Département de produit  Description Default Value',moduleid,now());


INSERT INTO SM_DEPOSIT (NAME,DESCRIPTION,CLT_MODULE_ID,DATE_CREATION) 
VALUES ('Dépôt Default Value','Dépôt Description Default Value',moduleid,now());


INSERT INTO SM_PRODUCT_COLOR (NAME,CLT_MODULE_ID,DATE_CREATION) 
VALUES ('Couleur de produit  Value',moduleid,now());


INSERT INTO SM_PRODUCT_GROUP (NAME,DESCRIPTION,CLT_MODULE_ID,DATE_CREATION) 
VALUES ('Groupe Default Value','Groupe Description Default Value',moduleid,now());

INSERT INTO SM_PRODUCT_UNIT (NAME,DESCRIPTION,CLT_MODULE_ID,DATE_CREATION) 
VALUES ('Unit Default Value','Unit Description Default Value',moduleid,now());


END//
DELIMITER ;