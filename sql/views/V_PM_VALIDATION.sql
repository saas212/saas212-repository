
CREATE OR REPLACE VIEW V_PM_VALIDATION AS 

SELECT CODE, PAGE_ID, INF_ITEM_CODE ITEM_CODE,  VALIDATION_ID,  PARAMS,  PARAM_NUMBER,   HELP, 
CUSTOM_ERROR,  CLT_MODULE_ID,  ERROR_MESSAGE FROM V_PM_ATTRIBUTE_REQUIRED
UNION
SELECT CODE, PAGE_ID, INF_ITEM_CODE ITEM_CODE,  VALIDATION_ID,  PARAMS,  PARAM_NUMBER,   HELP, 
CUSTOM_ERROR,  CLT_MODULE_ID,  ERROR_MESSAGE FROM V_PM_ATTRIBUTE_MAX_LENGTH
UNION
SELECT CODE, PAGE_ID, INF_ITEM_CODE ITEM_CODE,  VALIDATION_ID,  PARAMS,  PARAM_NUMBER,   HELP, 
CUSTOM_ERROR,  CLT_MODULE_ID,  ERROR_MESSAGE FROM V_PM_ATTRIBUTE_MAX_WORD
UNION
SELECT CODE, PAGE_ID, INF_ITEM_CODE ITEM_CODE,  VALIDATION_ID,  PARAMS,  PARAM_NUMBER,   HELP, 
CUSTOM_ERROR,  CLT_MODULE_ID,  ERROR_MESSAGE FROM V_PM_ATTRIBUTE_DATE_TYPE
UNION
SELECT CODE, PAGE_ID, INF_ITEM_CODE ITEM_CODE,  VALIDATION_ID,  PARAMS,  PARAM_NUMBER,   HELP, 
CUSTOM_ERROR,  CLT_MODULE_ID,  ERROR_MESSAGE FROM V_PM_ATTRIBUTE_FORMAT_TYPE;



