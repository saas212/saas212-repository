

create or replace view v_pm_attribute_exclud as

select concat_ws('.', pm_ax.clt_module_id, pm_ax.inf_item_code)  as code,
pm_ax.page_id,  pm_ax.inf_item_code,  pm_ax.clt_module_id
from pm_attribute_exclud pm_ax
where upper(pm_ax.active) = upper('y');

select * from v_pm_attribute_exclud;