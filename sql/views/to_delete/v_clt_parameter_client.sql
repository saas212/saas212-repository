
create or replace view v_clt_parameter_client as
select clt_pc.ID ID,
	   concat_ws('.', clt_pc.CLINET_ID, clt_p.ID) as CODE,
	   null first_name,
       null last_name,
       clt_p.NAME paramater_name,
	   null parameter_type_name,
	   clt_p.DEFAULT_VALUE DEFAULT_VALUE,
	   clt_pc.value value,
       clt_pc.CLINET_ID ,
       clt_pc.PARAMETER_ID,
	   clt_pc.active,
       'false' can_be_delete

from clt_parameter_client clt_pc, clt_parameter clt_p
where clt_pc.PARAMETER_ID  = clt_p.ID;


select * from v_clt_parameter_client;

/*
create or replace view v_clt_parameter_client as
select cpc.ID ID,
	   cc.first_name first_name,
       cc.last_name last_name,
       cp.NAME paramater_name,
	   cpt.NAME parameter_type_name,
	   cp.DEFAULT_VALUE DEFAULT_VALUE,
	   cpc.value value,
       cpc.CLINET_ID ,
       cpc.PARAMETER_ID,
	   cpc.active,
       'false' can_be_delete

from clt_parameter_client cpc, clt_parameter_type cpt , clt_parameter cp,clt_client cc,clt_user cu  
where cpc.CLINET_ID = cc.ID
and cpc.PARAMETER_ID  = cp.ID 
and cp.PARAMETER_TYPE_ID = cpt.ID
and cu.CLIENT_ID = cc.id;
*/