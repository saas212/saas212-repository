
-- Note Used In Project 

create or replace view v_sm_stock_state as 

select
	
    sum(sm_pd.unit_price_buy * sm_pd.quantity) as total_price_buy,
    sum(sm_pd.quantity) as sum_quantity,
	
    
	FLOOR(RAND() * 999) as code, -- rand()
	count(sm_pd.reception_id) reception_valid_count, 
    max(sm_pd.unit_price_buy)  unit_price_buy_max,
    avg(sm_pd.unit_price_buy)  unit_price_buy_moyenne,
    min(sm_pd.unit_price_buy)  unit_price_buy_min,
    count(sm_pd.quantity) quantity_count,
	3 product_valid,
    5 product_not_valid,
    45 reception_inprogress_count,
    sm_r.CLT_MODULE_ID as clt_module_id

from sm_product sm_p, sm_product_details sm_pd, sm_reception sm_r

where sm_p.Id = sm_pd.PRODUCT_ID  and sm_r.ID = sm_pd.RECEPTION_ID

group by sm_r.CLT_MODULE_ID;

select * from v_sm_stock_state;