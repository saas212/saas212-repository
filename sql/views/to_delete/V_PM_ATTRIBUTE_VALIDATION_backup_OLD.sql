
create or replace view V_PM_ATTRIBUTE_VALIDATION as 

-- validation data type (ID : 1)
select concat_ws('.', null, pm_pa.PAGE_ID, pm_pa.inf_item_code,1) as code, pm_pa.PAGE_ID, pm_pa.inf_item_code as ITEM_CODE, 
	  1 as VALIDATION_ID, DATA_TYPE_ID as PARAMS, 1 as PARAM_NUMBER, null as HELP,null as CUSTOM_ERROR, null CLT_MODULE_ID, 'validation.v1.dataType' ERROR_MESSAGE
from pm_page_attribut pm_pa where  pm_pa.DATA_TYPE_ID is not null and upper(pm_pa.ACTIVE) = upper('Y')
union
-- validation data format (ID : 2 )
select concat_ws('.', null, pm_pa.PAGE_ID, pm_pa.inf_item_code,2) as code, pm_pa.PAGE_ID, pm_pa.inf_item_code as ITEM_CODE, 
	   2 as VALIDATION_ID, DATA_FORMAT_ID as PARAMS ,  1 as PARAM_NUMBER, null as HELP, null as CUSTOM_ERROR, null CLT_MODULE_ID, 'validation.v1.dataFormat' ERROR_MESSAGE
from pm_page_attribut pm_pa where  pm_pa.DATA_FORMAT_ID is not null  and upper(pm_pa.ACTIVE) = upper('Y')
union
-- validation max lenght (ID : 3)
select concat_ws('.', null, pm_pa.PAGE_ID, pm_pa.inf_item_code,3) as code, pm_pa.PAGE_ID, pm_pa.inf_item_code as ITEM_CODE, 
       3 as VALIDATION_ID, MAX_LENGTH as PARAMS ,  1 as PARAM_NUMBER, null as HELP,null as CUSTOM_ERROR,  null CLT_MODULE_ID , 'validation.v1.maxLength' ERROR_MESSAGE
from pm_page_attribut pm_pa where  pm_pa.MAX_LENGTH is not null  and upper(pm_pa.ACTIVE) = upper('Y')
union 
-- validation des modules 
select concat_ws('.', pm_va.CLT_MODULE_ID, pm_va.PAGE_ID, pm_va.INF_ITEM_CODE,pm_va.VALIDATION_ID) as code, pm_va.PAGE_ID, pm_va.INF_ITEM_CODE as ITEM_CODE , 
	   pm_va.VALIDATION_ID, pm_va.PARAMS, pm_vt.PARAM_NUMBER as PARAM_NUMBER, pm_vt.HELP as HELP, pm_va.CUSTOM_ERROR as CUSTOM_ERROR, pm_va.CLT_MODULE_ID, pm_vt.ERROR_MESSAGE as ERROR_MESSAGE 
from pm_attribut_validation pm_va, pm_validation_type pm_vt
where pm_va.VALIDATION_ID = pm_vt.ID
and upper(pm_va.ACTIVE) = 'Y' and upper(pm_vt.ACTIVE) = 'Y';




select * from V_PM_ATTRIBUTE_VALIDATION;