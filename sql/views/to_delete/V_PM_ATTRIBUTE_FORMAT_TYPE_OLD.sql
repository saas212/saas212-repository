
create or replace view V_PM_ATTRIBUTE_FORMAT_TYPE as 

select concat_ws('.', null, pm_pa.PAGE_ID, pm_pa.inf_item_code,pm_vt.ID) as CODE,
	   pm_pa.PAGE_ID,
	   pm_pa.inf_item_code as ITEM_CODE,
	   pm_vt.ID as VALIDATION_ID,
       pm_df.NAME as PARAMS,
       pm_vt.PARAM_NUMBER as PARAM_NUMBER,
       pm_vt.HELP as HELP,
       null as CUSTOM_ERROR,
	   null CLT_MODULE_ID,
       pm_vt.ERROR_MESSAGE ERROR_MESSAGE
       
from pm_page_attribute pm_pa, pm_data_format pm_df, pm_validation_type pm_vt

where  pm_pa.DATA_FORMAT_ID = pm_df.ID and pm_vt.ID = 2
	   and pm_pa.DATA_FORMAT_ID is not null 
	   and upper(pm_pa.ACTIVE) = upper('Y');

select * from V_PM_ATTRIBUTE_FORMAT_TYPE;

