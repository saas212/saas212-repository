
create or replace view v_pm_attribute_validation as 


select concat_ws('.', pm_va.clt_module_id, pm_va.page_id, pm_va.inf_item_code,pm_va.validation_id) as code, 

       pm_va.page_id, pm_va.inf_item_code as item_code , 
	   pm_va.validation_id, pm_va.params, pm_vt.param_number as param_number, 
       pm_vt.help as help, pm_va.custom_error as custom_error, pm_va.clt_module_id,
       pm_vt.error_message as error_message 
       
from pm_attribute_validation pm_va, pm_validation_type pm_vt

where pm_va.validation_id = pm_vt.id
and   upper(pm_va.active) = 'y' and upper(pm_vt.active) = 'y';




select * from v_pm_attribute_validation;