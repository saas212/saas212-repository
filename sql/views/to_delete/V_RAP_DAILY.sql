
create or replace view v_sm_rap_daily as

/*
	afficher depuis la table ligne de commande + jointure
	client id, module id,customer_id, ref, color, taille, price qte, total, date_vente (insértion)
    
*/

select sm_ol.id as order_line_id , clt_c.id clinet_id, clt_m.id module_id, sm_c.first_name clinet_first_name, 
sm_ol.reference,sm_pc.name product_color_name, sm_ps.name product_size_name, 
sm_ol.unit_price_buy, sm_ol.quantity, sm_ol.total_price_buy, sm_o.date_creation

from clt_client clt_c, 
	 clt_module clt_m,
     sm_customer sm_c,
     sm_order sm_o,
     sm_order_line sm_ol,
     sm_product_color sm_pc,
     sm_product_size sm_ps
     
where clt_c.id = clt_m.client_id
and   clt_m.id = sm_o.clt_module_id
and	  sm_o.id = sm_ol.order_id
and   sm_c.id =  sm_o.customer_id
and   sm_ol.product_color_id =  sm_pc.id
and   sm_ol.product_size_id =  sm_ps.id
and   upper(sm_o.valid) = upper('y');

select * from v_sm_rap_daily;