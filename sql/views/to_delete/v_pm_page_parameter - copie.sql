
create or replace view v_pm_page_parameter as 

select pm_pg_par.id,
       pm_pg_par.name,
	   pm_pg_par.description,
	   pm_pg_par.DEFAULT_VALUE,
	   pm_pg_par.PAGE_PARAMETER_TYPE_ID,
       pm_pg_par_type.NAME pm_page_parameter_type_name,
       pm_pg_par.PAGE_ID,
       pp.NAME pm_page_name,
	   pm_pg_par.active,
       'false' can_be_delete

from pm_page_parameter pm_pg_par, pm_page_parameter_type pm_pg_par_type , pm_page pp


where pm_pg_par.PAGE_PARAMETER_TYPE_ID = pm_pg_par_type.id
and pm_pg_par.PAGE_ID  = pp.ID ;
