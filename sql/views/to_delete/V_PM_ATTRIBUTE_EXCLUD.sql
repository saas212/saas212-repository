
create or replace view v_pm_attribute_exclud as 

select concat_ws('.', 3,inf_item_code) as code,  inf_item_code, page_id, 3 clt_module_id 
from pm_page_attribute 
where upper(IS_HIDDEN) = upper('y')
and upper(active) = upper('y');

select * from v_pm_attribute_exclud;
