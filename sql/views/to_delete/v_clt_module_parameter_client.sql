
create or replace view v_clt_module_parameter_client as
select cmpc.ID ID,
       cmp.NAME module_parameter_name,
	   cmpt.NAME module_parameter_type_name,
	   cmp.DEFAULT_VALUE ,
	   cmpc.value,
       cm.CLIENT_ID ,
       cm.id module_id,
       cmp.ID clt_module_parameter_id,
	   cmpc.active,
       'false' can_be_delete

from clt_module_parameter_client cmpc, clt_module_parameter cmp , clt_module_parameter_type cmpt,clt_module cm   
where cmpc.MODULE_ID = cm.ID
and cmpc.MODULE_PARAMETER_ID  = cmp.ID 
and cmp.MODULE_PARAMETER_TYPE_ID = cmpt.ID;
