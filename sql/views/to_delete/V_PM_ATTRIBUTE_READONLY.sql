
create or replace view v_pm_attribute_readonly as
    select 
        concat_ws('.', 1, ppa.inf_item_code) as code,
        ppa.inf_item_code,
        ppa.page_id,
        1 clt_module_id    
     from
        pm_page_attribute ppa
    where upper(ppa.IS_READONLY) = upper('y')
            and upper(ppa.active) = upper('y');

select * from v_pm_attribute_readonly;
/* old
create or replace view v_pm_attribute_readonly as 


select concat_ws('.', 3,inf_item_code) as code,  inf_item_code, page_id, 3 clt_module_id 
from pm_page_attribute 
where IS_READONLY = upper('y')
and upper(active) = upper('y');

select * from v_pm_attribute_readonly;




*/
