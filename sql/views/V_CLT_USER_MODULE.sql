
CREATE OR REPLACE VIEW V_CLT_USER_MODULE AS


SELECT CUM.ID,CUM.USER_ID,CUM.MODULE_ID, CM.MODULE_TYPE_ID, CUM.MODULE_ID_TO_MANAGER,  CM.NAME MODULE_NAME, CM.IMAGE_PATH MODULE_IMAGE_PATH

FROM CLT_USER_MODULE CUM, CLT_MODULE CM

WHERE UPPER(CUM.ACTIVE) = UPPER('Y') 
AND CUM.MODULE_ID = CM.ID;
