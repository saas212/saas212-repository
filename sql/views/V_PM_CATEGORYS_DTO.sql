
CREATE OR REPLACE VIEW V_COMPOS_CAT AS 


SELECT
MODEL_ID,
GROUP_ID,
MIN(CATEGORY_SORT) CATEGORY_SORT,
CATEGORY_ID AS CATEGORY_ID,
ACTIVE
FROM PM_COMPOSITION
GROUP BY MODEL_ID,
GROUP_ID,
CATEGORY_ID,
ACTIVE 
ORDER BY CATEGORY_SORT;




CREATE OR REPLACE VIEW V_PM_CATEGORYS_DTO AS

SELECT CONCAT_WS('.',PM_C.MODEL_ID,PM_C.GROUP_ID , PM_G.ID) AS CODE, PM_G.ID,PM_G.NAME,PM_G.IMAGE_PATH,PM_G.DESCRIPTION, 
       PM_G.CATEGORY_TYPE_ID, PM_C.CATEGORY_SORT, PM_C.MODEL_ID, PM_C.GROUP_ID
       
FROM PM_CATEGORY PM_G, PM_CATEGORY_TYPE MP_GT, V_COMPOS_CAT PM_C, PM_MODEL PM_M

WHERE PM_G.CATEGORY_TYPE_ID =  MP_GT.ID
AND   PM_G.ID = PM_C.CATEGORY_ID
AND   PM_C.MODEL_ID = PM_M.ID


AND UPPER(PM_G.ACTIVE) = 'Y'
AND UPPER(MP_GT.ACTIVE) = 'Y'
AND UPPER(PM_C.ACTIVE) = 'Y'
AND UPPER(PM_M.ACTIVE) = 'Y'

GROUP BY PM_G.ID,PM_G.IMAGE_PATH,PM_G.NAME,PM_G.DESCRIPTION, PM_G.CATEGORY_TYPE_ID, 
PM_C.CATEGORY_SORT, PM_C.MODEL_ID, PM_C.GROUP_ID

ORDER BY PM_C.CATEGORY_SORT;
