
CREATE OR REPLACE VIEW V_SM_ORDER_SUPPLIER_SUMMARY AS

SELECT SM_OS.ID ORDER_SUPPLIER_ID, 
	   SUM(SM_OSL.QUANTITY) SUMMARY_TOTAL_QUANTITY, 
       SUM(SM_OSL.UNIT_PRICE_SALE) SUMMARY_TOTAL_HT, 
       SUM(SM_OSL.UNIT_PRICE_SALE) * 1.2 -SUM(SM_OSL.UNIT_PRICE_SALE) SUMMARY_TVA_AMOUNT, 
       SUM(SM_OSL.UNIT_PRICE_SALE) * 1.2 SUMMARY_TOTAL_TTC
       
FROM SM_ORDER_SUPPLIER SM_OS LEFT JOIN SM_ORDER_SUPPLIER_LINE SM_OSL
ON SM_OS.ID = SM_OSL.ORDER_SUPPLIER_ID
GROUP BY SM_OS.ID;
