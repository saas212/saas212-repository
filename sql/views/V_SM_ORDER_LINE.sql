
CREATE OR REPLACE VIEW V_SM_ORDER_LINE AS 

SELECT SM_OL.*, SM_P.REFERENCE, SM_OL.QUANTITY * SM_OL.NEGOTIATE_PRICE_SALE AS  TOTAL_NEGOTIATE_PRICE_SALE

FROM SM_ORDER_LINE SM_OL, SM_PRODUCT SM_P

WHERE  SM_OL.PRODUCT_ID = SM_P.ID;
