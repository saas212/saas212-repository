

CREATE or replace VIEW V_BI_SM_ORDER_LINE_BY_DAY AS (
SELECT	CONCAT_WS('.' , CLT_MODULE_ID , DATE_FORMAT(COALESCE(SM_O.DATE_UPDATE ,SM_O.DATE_CREATION ),  '%Y-%M-%D')) PK,
 CLT_MODULE_ID,DATE_FORMAT(COALESCE(SM_O.DATE_UPDATE ,SM_O.DATE_CREATION ),  '%Y-%M-%D') OPERATION_DATE ,
DATE_FORMAT(COALESCE(SM_O.DATE_UPDATE ,SM_O.DATE_CREATION ), '%D') DAY,
DATE_FORMAT(COALESCE(SM_O.DATE_UPDATE ,SM_O.DATE_CREATION ), '%M') MONTH,
DATE_FORMAT(COALESCE(SM_O.DATE_UPDATE ,SM_O.DATE_CREATION ), '%Y') YEAR,
SUM(SM_OL.NEGOTIATE_PRICE_SALE) SUM_PRICE,SUM(SM_OL.QUANTITY) SUM_QUANTITY
FROM SM_ORDER_LINE SM_OL , SM_ORDER SM_O
WHERE SM_OL.ORDER_ID = SM_O.ID
AND SM_O.ORDER_STATUS_ID = 3
GROUP BY CLT_MODULE_ID, DATE_FORMAT(COALESCE(SM_O.DATE_UPDATE ,SM_O.DATE_CREATION ),  '%Y-%M-%D') 
);